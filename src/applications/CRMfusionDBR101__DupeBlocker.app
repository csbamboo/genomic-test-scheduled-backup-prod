<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>DupeBlocker by CRMfusion Inc.</description>
    <label>DupeBlocker</label>
    <logo>CRMfusionDBR101__DupeBlocker/CRMfusionDBR101__DupeBlockerLogo.gif</logo>
    <tab>CRMfusionDBR101__DupeBlocker_Today_VF</tab>
    <tab>CRMfusionDBR101__Scenario__c</tab>
    <tab>CRMfusionDBR101__Duplicate_Warning__c</tab>
    <tab>CRMfusionDBR101__DupeBlocker_Settings_VF</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
</CustomApplication>
