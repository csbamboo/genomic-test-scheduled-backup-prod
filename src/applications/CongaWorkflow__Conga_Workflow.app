<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>CongaWorkflow__About_Conga_Workflow</defaultLandingTab>
    <label>Conga Workflow</label>
    <logo>CongaWorkflow__Conga_Resources/CongaWorkflow__Conga_Logo_please_don_t_delete.png</logo>
    <tab>CongaWorkflow__About_Conga_Workflow</tab>
    <tab>CongaWorkflow__Workflow_Setup</tab>
    <tab>CongaWorkflow__Workflow_Formula_Builder</tab>
    <tab>CongaWorkflow__Conga_Workflow_Dashboard</tab>
</CustomApplication>
