/*************************************************************
@Name: APTS_Constants
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 06/18/2015
@Description: Static constants Class 
@UsedBy: 
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
public with sharing class APTS_Constants {
	//Agreement Record Types
	public static String AGMT_RT_US_PRIVATE = 'US Private';
	public static String AGMT_RT_US_GOV = 'US Government';
	public static String AGMT_RT_US_FACL = 'US Facilities/Hospital';

	//Price Matrix Dimenson Values
	public static String PRICE_MATRIX_PRV_DME = 'Private';
	public static String PRICE_MATRIX_GOV_DME = 'Government';

	//Related Agreement Relationships
	public static final String RELAGMT_RELATIONSHIP_AMENDMENTFOR = 'Is Amendment For';
	public static final String RELAGMT_RELATIONSHIP_AMENDEDBY = 'Is Amended By';  
	public static final String RELAGMT_RELATIONSHIP_RENEWFOR = 'Is Renewal For';
	public static final String RELAGMT_RELATIONSHIP_RENEWBY = 'Is Renewed By'; 

}