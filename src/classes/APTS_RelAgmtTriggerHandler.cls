/*************************************************************
@Name: APTS_RelAgmtTriggerHandler
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 06/18/2015
@Description: Handler class to deep clone the agreement childs and grand child in amend and renew functionality
@UsedBy: APTS_RelAgmtTrigger
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
public with sharing class APTS_RelAgmtTriggerHandler {
	
	public static void onAfterInsert(List<Apttus__APTS_Related_Agreement__c> newRecsList) {      
       copyBaseAgmtChilds(newRecsList);
    }

    private static void copyBaseAgmtChilds(List<Apttus__APTS_Related_Agreement__c> newRecsList){
    	Map<Id,Id> fromtoToagmtIdsMap = new Map<Id,Id>();
    	try{
    	//preapre a map of base agreement based amend/renew agreements
    	for(Apttus__APTS_Related_Agreement__c ra : newRecsList)
        {

            if(String.isNotBlank(ra.Apttus__Relationship_From_Type__c)
            	&& (APTS_Constants.RELAGMT_RELATIONSHIP_AMENDEDBY.equalsIgnorecase(ra.Apttus__Relationship_From_Type__c)
                || APTS_Constants.RELAGMT_RELATIONSHIP_RENEWBY.equalsIgnorecase(ra.Apttus__Relationship_From_Type__c) ))
            {
	            fromtoToagmtIdsMap.put(ra.Apttus__APTS_Contract_From__c,ra.Apttus__APTS_Contract_To__c);
	        }    
        }
        //Getting all fields of child objects
        String configAllFieldsStr = APTS_Utility.buildAllFieldsString('Apttus_Config2__ProductConfiguration__c');
        String configLineAllFieldsStr = APTS_Utility.buildAllFieldsString('Apttus_Config2__LineItem__c');
        String summaryGroupAllFieldsStr = APTS_Utility.buildAllFieldsString('Apttus_Config2__SummaryGroup__c');
        String adhocGroupAllFieldsStr = APTS_Utility.buildAllFieldsString('Apttus_Config2__AdHocGroup__c');
        String agmtLineAllFieldsStr = APTS_Utility.buildAllFieldsString('Apttus__AgreementLineItem__c');
       // String prodAttrValAllFieldsStr = APTS_Utility.buildAllFieldsString('Apttus_CMConfig__AgreementProductAttributeValue__c');
      //  String tempObjFieldStr = APTS_Utility.buildAllFieldsString('Apttus_Config2__TempObject__c');

        Set<Id> agmtIdSet = fromtoToagmtIdsMap.keySet();
        String configStatus = 'Finalized';
        //Query for agreement and its children
        String queryStr = 'Select Id,Name, RecordtypeId,Apttus__FF_Agreement_Number__c, Apttus__Version_Number__c,'+
        				'(Select '+configAllFieldsStr+' From Apttus_CMConfig__Configurations__r Where Apttus_Config2__Status__c =:configStatus),'+
        				'(Select '+agmtLineAllFieldsStr+' From Apttus__AgreementLineItems__r)'+
                        ' From Apttus__APTS_Agreement__c'+
                        ' Where Id IN : agmtIdSet';
        System.debug('queryStr--'+queryStr);
        List<Apttus__APTS_Agreement__c> agmtList = Database.query(queryStr);
        Map<Id, Apttus__APTS_Agreement__c> agmtMap = new Map<Id, Apttus__APTS_Agreement__c>(agmtList);

        //Prepare and insert the agreement childs records by copying from base agreement tot amend/renew agreemnt
        Map<Id,Apttus_Config2__ProductConfiguration__c> configMap = new Map<Id,Apttus_Config2__ProductConfiguration__c>();
        Map<Id,Apttus__AgreementLineItem__c> agmtLIMap = new Map<Id,Apttus__AgreementLineItem__c>();
        Map<Id,List<Apttus_Config2__ProductConfiguration__c>> agmtProdConfigMap = new Map<Id,List<Apttus_Config2__ProductConfiguration__c>>();
        Map<Id,List<Apttus__AgreementLineItem__c>> agmtAgmtLIMap = new Map<Id,List<Apttus__AgreementLineItem__c>>();
        Apttus__APTS_Agreement__c agmt = null;
        List<Apttus_Config2__ProductConfiguration__c> prodConfigList = null;
        Apttus_Config2__ProductConfiguration__c prodConfig= null;
        List<Apttus__AgreementLineItem__c> agmtLinesList = null;
        Apttus__AgreementLineItem__c agmtLine = null;

        if(agmtMap != null && !agmtMap.isEmpty()){
        	for(Id agmtId: agmtMap.keySet()){
        		prodConfigList = new List<Apttus_Config2__ProductConfiguration__c>();
        		agmtLinesList= new List<Apttus__AgreementLineItem__c>();
				agmt = agmtMap.get(agmtId);

				//Prodduct configuration Clone
				for(Apttus_Config2__ProductConfiguration__c config : agmt.Apttus_CMConfig__Configurations__r){
					prodConfig = config.clone(false,true,false,false);
					prodConfig.Apttus_CMConfig__AgreementId__c = fromtoToagmtIdsMap.get(agmtId);
					prodConfig.Apttus_Config2__BusinessObjectId__c = fromtoToagmtIdsMap.get(agmtId);
					prodConfig.Apttus_Config2__AncestorId__c = null;
					prodConfig.Apttus_Config2__PrimordialId__c = null;
					prodConfig.Name  = prodConfig.Name+' Amended';
					configMap.put(config.Id,prodConfig);
					prodConfigList.add(prodConfig);	
				}
				agmtProdConfigMap.put(fromtoToagmtIdsMap.get(agmtId),prodConfigList);

				//Agreement Line items Clone
				for(Apttus__AgreementLineItem__c agmtLI : agmt.Apttus__AgreementLineItems__r){
					agmtLine = agmtLI.clone(false,true,false,false);
					agmtLine.Apttus__AgreementId__c = fromtoToagmtIdsMap.get(agmtId);
					agmtLIMap.put(agmtLI.Id,agmtLine);
					agmtLinesList.add(agmtLine);	
				}
				agmtAgmtLIMap.put(fromtoToagmtIdsMap.get(agmtId),agmtLinesList);

				
        	}
        	if(!configMap.isEmpty())
        	{
        		insert configMap.values();
        	}
        	if(!agmtLIMap.isEmpty())
        	{
        		insert agmtLIMap.values();
        	}

        	//Query for Product configuration childs
        	Set<Id> configIdSet = configMap.keySet();
        	String configChildsQuery = 'Select Id, '+
				        				'(Select '+configLineAllFieldsStr+' From Apttus_Config2__LineItems__r),'+
				        				'(Select '+summaryGroupAllFieldsStr+' From Apttus_Config2__SummaryGroups__r),'+
				        				'(Select '+adhocGroupAllFieldsStr+' From Apttus_Config2__AdHocGroups__r)'+
				        				//',(Select '+tempObjFieldStr+' From Apttus_Config2__TempObjects__r)'+
				                        ' From Apttus_Config2__ProductConfiguration__c'+
				                        ' Where Id IN : configIdSet';
			System.debug('configChildsQuery--'+configChildsQuery);
			List<Apttus_Config2__ProductConfiguration__c> prodConfigObjList = Database.query(configChildsQuery);
		    Map<Id, Apttus_Config2__ProductConfiguration__c> prodConfigMap = new Map<Id, Apttus_Config2__ProductConfiguration__c>(prodConfigObjList);

		     Map<Id,List<Apttus_Config2__LineItem__c>> configlineItemMap = new Map<Id,List<Apttus_Config2__LineItem__c>>();
       		 Map<Id,List<Apttus_Config2__SummaryGroup__c>> summaryGroupMap = new Map<Id,List<Apttus_Config2__SummaryGroup__c>>();
       		 Map<Id,List<Apttus_Config2__AdHocGroup__c>> adhocGroupMap = new Map<Id,List<Apttus_Config2__AdHocGroup__c>>();
       		 Map<Id,List<Apttus_Config2__TempObject__c>> tempObjMap = new Map<Id,List<Apttus_Config2__TempObject__c>>();
		     Apttus_Config2__ProductConfiguration__c config = null;
		     Apttus_Config2__LineItem__c configLI =  null;
		     Apttus_Config2__SummaryGroup__c summGrp =  null;
		     Apttus_Config2__AdHocGroup__c adHocGrp =  null;
		     Apttus_Config2__TempObject__c tempObj = null;
		     List<Apttus_Config2__LineItem__c> configlineList = null;
		     List<Apttus_Config2__SummaryGroup__c> summGrpList = null;
		     List<Apttus_Config2__AdHocGroup__c> adHocGrpList = null;
		     List<Apttus_Config2__TempObject__c> tempObjList = null;
		     //Prepare a map of new configuration id based Child records
		    for(Id prodConfigId: prodConfigMap.keySet()){
        		configlineList = new List<Apttus_Config2__LineItem__c>();
        		summGrpList = new List<Apttus_Config2__SummaryGroup__c>();
        		adHocGrpList = new List<Apttus_Config2__AdHocGroup__c>();
        		tempObjList = new List<Apttus_Config2__TempObject__c>();
				config = prodConfigMap.get(prodConfigId);

				//map of amed/renew config Id based cloned Config lines
				for(Apttus_Config2__LineItem__c configLine : config.Apttus_Config2__LineItems__r){
					configLI = configLine.clone(false,true,false,false);
					configLI.Apttus_Config2__ConfigurationId__c = configMap.get(config.Id).Id;
					configlineList.add(configLI);	
				}
				configlineItemMap.put(configMap.get(config.Id).Id,configlineList);

				//map of amed/renew config Id based cloned Summary groups
				for(Apttus_Config2__SummaryGroup__c summGrpObj : config.Apttus_Config2__SummaryGroups__r){
					summGrp = summGrpObj.clone(false,true,false,false);
					summGrp.Apttus_Config2__ConfigurationId__c = configMap.get(config.Id).Id;
					summGrpList.add(summGrp);	
				}
				summaryGroupMap.put(configMap.get(config.Id).Id,summGrpList);

				//map of amed/renew config Id based cloned Adhoc groups
				for(Apttus_Config2__AdHocGroup__c adhocGrpObj : config.Apttus_Config2__AdHocGroups__r){
					adHocGrp = adhocGrpObj.clone(false,true,false,false);
					adHocGrp.Apttus_Config2__ConfigurationId__c = configMap.get(config.Id).Id;
					adHocGrpList.add(adHocGrp);	
				}
				adhocGroupMap.put(configMap.get(config.Id).Id,adHocGrpList);

				/*for(Apttus_Config2__TempObject__c tempObjCPQ : config.Apttus_Config2__TempObjects__r){
					tempObj = tempObjCPQ.clone(false,true,false,false);
					tempObj.Apttus_Config2__ConfigurationId__c = configMap.get(config.Id).Id;
					tempObjList.add(tempObj);	
				}
				tempObjMap.put(configMap.get(config.Id).Id,tempObjList);*/
        	}
        
       //Insert all amend/renew configuration childs
         configlineList = new List<Apttus_Config2__LineItem__c>();
         summGrpList = new List<Apttus_Config2__SummaryGroup__c>();
         adHocGrpList = new List<Apttus_Config2__AdHocGroup__c>();
         tempObjList = new List<Apttus_Config2__TempObject__c>();
      
        if(!configlineItemMap.isEmpty())
    	{
    		for(List<Apttus_Config2__LineItem__c> configLines : configlineItemMap.values()){
    			configlineList.addAll(configLines);
    		}
    		insert configlineList;
    	}
    	if(!summaryGroupMap.isEmpty())
    	{
    		for(List<Apttus_Config2__SummaryGroup__c> sumGrps : summaryGroupMap.values()){
    			summGrpList.addAll(sumGrps);
    		}
    		insert summGrpList;
    	}
    	if(!adhocGroupMap.isEmpty())
    	{
    		for(List<Apttus_Config2__AdHocGroup__c> adHocGrps : adhocGroupMap.values()){
    			adHocGrpList.addAll(adHocGrps);
    		}
    		insert adHocGrpList;
    	}
    	/*if(!tempObjMap.isEmpty())
    	{
    		for(List<Apttus_Config2__TempObject__c> tempObjs : tempObjMap.values()){
    			tempObjList.addAll(tempObjs);
    		}
    		insert tempObjList;
    	}*/

    	//Set the summary and adhoc group Ids to each line item of amend/renew configuration
    	configlineList = new List<Apttus_Config2__LineItem__c>();
    	for(Id configId : configlineItemMap.keySet()){
    		List<Apttus_Config2__AdHocGroup__c> adhocGrps = adhocGroupMap.get(configId);
    		List<Apttus_Config2__SummaryGroup__c> summaryGrps = summaryGroupMap.get(configId);	
    		Apttus_Config2__SummaryGroup__c categorySummarygroup = null;
    		//get the category Tota based Summary group for each configuration
    		for(Apttus_Config2__SummaryGroup__c sumGrp : summaryGrps){
    			if(sumGrp.Apttus_Config2__LineType__c == 'Category Total'){
    				categorySummarygroup = sumGrp;
    				System.debug('sumGrp--'+sumGrp.Id);
    				break;
    			}
    		}

    		//set summary and adhoc group ids
    		for(Apttus_Config2__LineItem__c line: configlineItemMap.get(configId)){
    			if(adhocGrps != null && !adhocGrps.isEmpty()){
    				line.Apttus_Config2__AdHocGroupId__c = adhocGrps.get(0).Id;
    			}
    			if(categorySummarygroup != null){
    				line.Apttus_Config2__SummaryGroupId__c = categorySummarygroup.Id;
    			}
    			configlineList.add(line);
    		}

    		//update the line item with new lookup ids.
    		if(!configlineList.isEmpty()){
    			update configlineList;
    		}
    	}
    }
    }catch(Exception e){
    	System.debug(e.getMessage());
    }

    }

}