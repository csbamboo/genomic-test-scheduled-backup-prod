/*************************************************************
@Name: APTS_RelAgmtTrigger
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 06/23/2015
@Description: Test class for APTS_RelAgmtTrigger
@UsedBy: 
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
@isTest
private class APTS_RelAgmtTriggerTest {
    
    @isTest static void testAmendCloneObjects() {
        Account acc = APTS_TestDataUtil.getAccount('TestAcc1','Payor','Payor Type');
        insert acc;

        //Opportunity opty = APTS_TestDataUtil.getOpportunity(acc,'Pre-Close','Delivery Assurance','Email Event');
        //insert opty;
        
        Apttus_Config2__PriceList__c plist = APTS_TestDataUtil.getPriceList();
        insert pList;

        Product2 prod1 = APTS_TestDataUtil.getProduct('Oncotype DX1', 'DX1', 'Standalone', 'Cancer');
        Product2 prod2 = APTS_TestDataUtil.getProduct('Oncotype DX2', 'DX2', 'Standalone', 'Cancer');
        insert new List<Product2>{prod1, prod2};

        Apttus_Config2__PriceListItem__c plItem1 = APTS_TestDataUtil.getPriceListItem(plist.Id, prod1.Id, 'Standard Price', 'One Time', 'Per Unit', 9485, 9000, 10000, 'Unit Price');
        Apttus_Config2__PriceListItem__c plItem2 = APTS_TestDataUtil.getPriceListItem(plist.Id, prod2.Id, 'Standard Price', 'One Time', 'Per Unit', 9485, 9000, 10000, 'Unit Price');
        insert new List<Apttus_Config2__PriceListItem__c>{plItem1, plItem2};

        Apttus_Config2__PriceDimension__c priceDimension = APTS_TestDataUtil.getPriceDimension('Private/Government', 1,'Line Item','Apttus_Config2__LineItem__c','GHI_CPQ_Private_Government__c');
        insert priceDimension;

        Apttus_Config2__PriceMatrix__c priceMat = APTS_TestDataUtil.getPriceMatrix(plItem1.id, 1,'Dimension',priceDimension.Id,'Discrete');
        insert priceMat;

        Apttus_Config2__PriceMatrixEntry__c priceMatEntry1 = APTS_TestDataUtil.getPriceMatrixEntry(priceMat.Id, 1,'Government','List Price Override',3347,3347);
        Apttus_Config2__PriceMatrixEntry__c priceMatEntry2 = APTS_TestDataUtil.getPriceMatrixEntry(priceMat.Id, 2,'Government','List Price Override',4510,4510);
        insert new List<Apttus_Config2__PriceMatrixEntry__c>{priceMatEntry1, priceMatEntry2};
        
        Id rtId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(APTS_Constants.AGMT_RT_US_PRIVATE).getRecordTypeId();

        APTS_CPQ_Discount__c cpQDisount1 = APTS_TestDataUtil.getCPQDiscount('US Private Comm', 'US Private','Commercial', '6');
        APTS_CPQ_Discount__c cpQDisount2 = APTS_TestDataUtil.getCPQDiscount('US Private_Managed Medi', 'US Private','Managed Medicaid', '6');
        insert new List<APTS_CPQ_Discount__c>{cpQDisount1, cpQDisount2};

        Apttus__APTS_Agreement__c agmtObj = APTS_TestDataUtil.createAgreement(rtId,acc.Id,'Request','Request'); 
        agmtObj.Apttus_CMConfig__PriceListId__c = pList.Id;
        insert agmtObj; 
        
        Apttus_Config2__ProductConfiguration__c pConfig = APTS_TestDataUtil.getProductConfiguration('Product Configuration', 1, agmtObj.Id, 'Agreement', 'Category', 
            pList.Id, null, 'Finalized', null, Datetime.now(), true, 'test apttus config');
            
        insert pConfig;

         // STEP VI - Create new ad hoc groups
        Apttus_Config2__AdHocGroup__c adGroupSO = APTS_TestDataUtil.createAdHocGroup('Group A',pConfig.Id,'Group A'); 
        insert adGroupSO;

        


        Apttus_Config2__AdHocGroup__c adhocgrp = new Apttus_Config2__AdHocGroup__c();
        adhocgrp.Apttus_Config2__ConfigurationId__c = pConfig.Id;
        adhocgrp.Apttus_Config2__ChargeType__c = 'Standard Price';
        adhocgrp.Apttus_Config2__Sequence__c = 1;
        adhocgrp.Apttus_Config2__GroupType__c = 'Category';
        adhocgrp.Apttus_Config2__Frequency__c = 'One Time';
        insert adhocgrp;

        Apttus_Config2__SummaryGroup__c summGrp = new Apttus_Config2__SummaryGroup__c();
        summGrp.Apttus_Config2__ConfigurationId__c = pConfig.Id;
        summGrp.Apttus_Config2__LineNumber__c = 1;
        summGrp.Apttus_Config2__ItemSequence__c = 1;
        summGrp.Apttus_Config2__GroupType__c = 'Category';
        summGrp.Apttus_Config2__LineType__c = 'Category Total';
        insert summGrp;

        Apttus_Config2__LineItem__c lineItem1 = APTS_TestDataUtil.getLineItem(pConfig.Id, null, 1, true, 1, 'Product/Service', prod1.Id, 
            true, null, null, null, null, 1, true, 'Each', 1, plist.Id, plItem1.Id, 'One Time', 'Per Unit', null, null, true, true, 9485, 9485, 
            'Per Unit', 9485, null, 9485, 'Oncotype DX1');
        
        lineItem1.Apttus_Config2__AdHocGroupId__c = adhocgrp.Id;
        lineItem1.Apttus_Config2__SummaryGroupId__c = summGrp.Id;

        Apttus_Config2__LineItem__c lineItem2 = APTS_TestDataUtil.getLineItem(pConfig.Id, null, 1, true, 1, 'Product/Service', prod2.Id, 
            true, null, null, null, null, 1, true, 'Each', 1, plist.Id, plItem2.Id, 'One Time', 'Per Unit', null, null, true, true, 9485, 9485, 
            'Per Unit', 9485, null, 9485, 'Oncotype DX12');
        lineItem2.Apttus_Config2__AdHocGroupId__c = adhocgrp.Id;
        lineItem2.Apttus_Config2__SummaryGroupId__c = summGrp.Id;

        insert new List<Apttus_Config2__LineItem__c>{lineItem1,lineItem2};


        Test.startTest(); 

        

        Apttus__AgreementLineItem__c agmtLine = new Apttus__AgreementLineItem__c();
        agmtLine.Apttus__AgreementId__c = agmtObj.id;
        agmtLine.Apttus_CMConfig__ItemSequence__c = 1;
        agmtLine.Apttus_CMConfig__LineNumber__c = 1;
        insert agmtLine;

        Apttus__APTS_Agreement__c amendAgmtObj = APTS_TestDataUtil.createAgreement(rtId,acc.Id,'Request','Request');    
        amendAgmtObj.Apttus_CMConfig__PriceListId__c = pList.Id;
        insert amendAgmtObj;

        Apttus__APTS_Related_Agreement__c relAgtmt= new Apttus__APTS_Related_Agreement__c() ;
        relAgtmt.Apttus__APTS_Contract_From__c = agmtObj.Id;
        relAgtmt.Apttus__Relationship_From_Type__c = 'Is Amended By';
        relAgtmt.Apttus__APTS_Contract_To__c = amendAgmtObj.Id;
        relAgtmt.Apttus__Relationship_To_Type__c = 'Is Amendment For';
        insert relAgtmt;
        
        APTS_Utility.buildQueryAllString('Apttus__APTS_Agreement__c');

       Test.stopTest();
    }
    
    
    
}