/*************************************************************
@Name: APTS_TestDataUtil
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 06/23/2015
@Description: Test Data util class for all test classes
@UsedBy: 
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
public class APTS_TestDataUtil {
	

	public static Account getAccount(String name,String rtName, String aType){
		
		ID rtID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(rtName).getRecordTypeID();
		Account acc = new Account(name = name, RecordTypeId = rtID,Type = aType);
		return acc;
	}

	public static Opportunity getOpportunity(Account acc, String stageName, String oType,  String oLeadSrc){
		
		Opportunity opty = new Opportunity(AccountId = acc.Id, Name = 'test Apttus Opty');
		opty.StageName = stageName;
		opty.CloseDate = Date.today().addDays(100);
		opty.Type = oType;
		opty.LeadSource = oLeadSrc;
		return opty;
	}
	
	public static Apttus_Config2__PriceList__c getPriceList(){
		
		Apttus_Config2__PriceList__c priceL = new Apttus_Config2__PriceList__c();
		priceL.Name = 'Apttus Price List';
		priceL.Apttus_Config2__EffectiveDate__c = Date.today().addDays(-300);
		priceL.Apttus_Config2__ExpirationDate__c = Date.today().addMonths(12);
		priceL.Apttus_Config2__Active__c = true;
		return priceL;
	}	

	 public static Apttus__APTS_Agreement__c createAgreement(Id reTypeId,Id accId,String statusCategory,String status){
        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c();
        agmt.RecordTypeId = reTypeId;
        if(accId != null)
          agmt.Apttus__Account__c=accId;
        agmt.Apttus__Status__c=status;
        agmt.Apttus__Status_Category__c=statusCategory;
        agmt.currencyIsoCode = 'USD';
        return agmt;
    }
	

	public static Product2 getProduct(String prdName, String pCode, String configType, String pFamily){
		
		Product2 product = new Product2(Name=prdName);
	    product.ProductCode = pCode;
	    product.Family = pFamily;
	    product.Apttus_Config2__ConfigurationType__c = configType;
	    product.Apttus_Config2__Uom__c = 'Each';
	    product.IsActive = true;
	    product.Apttus_Config2__EffectiveDate__c = Date.today().addDays(-300);
	    product.Apttus_Config2__ExpirationDate__c = Date.today().addMonths(12);
	    product.Apttus_Config2__HasAttributes__c = false;
		return product;
	}

	public static Apttus_Config2__ProductConfiguration__c getProductConfiguration(String configName,
																									                                          Integer versionNbr,
																									                                          ID bObjectId,
																									                                          String bObjectType,
																									                                          String groupType,
																									                                          ID priceListId, 
																									                                          ID ancestorId, 
																										                                      String status, 
																										                                      Datetime finalizedDate, 
																										                                      Datetime effectiveDate, 
																										                                      Boolean isTransient,
																										                                      String configDesc){
		
		Apttus_Config2__ProductConfiguration__c configSO = new Apttus_Config2__ProductConfiguration__c(Name = configName, OwnerId = UserInfo.getUserId());
	    configSO.Apttus_Config2__VersionNumber__c = versionNbr;
	    configSO.Apttus_Config2__SummaryGroupType__c = groupType;
	    configSO.Apttus_Config2__BusinessObjectId__c = bObjectId;
	    configSO.Apttus_Config2__BusinessObjectType__c = bObjectType;
	    //configSO.Apttus_QPConfig__Proposald__c = bObjectId;
	    configSO.Apttus_CMConfig__AgreementId__c = bObjectId;
	    configSO.Apttus_Config2__PriceListId__c = priceListId;
	    configSO.Apttus_Config2__AncestorId__c = ancestorId;
	    configSO.Apttus_Config2__Status__c = status;
	    configSO.Apttus_Config2__IsTransient__c = isTransient;
	    configSO.Apttus_Config2__FinalizedDate__c = finalizedDate;
	    configSO.Apttus_Config2__EffectiveDate__c = effectiveDate; 
	    configSO.Apttus_Config2__Description__c = configDesc;
	    
		return configSO;
	}

	 public static Apttus_Config2__AdHocGroup__c createAdHocGroup(String groupName,
                                                           ID configId,
                                                           String groupDesc) {
        
        // create a new ad hoc group
        Apttus_Config2__AdHocGroup__c groupSO = new Apttus_Config2__AdHocGroup__c(Name = groupName, Apttus_Config2__ConfigurationId__c = configId);
    
        // setup ad hoc group data
        
        // description
        groupSO.Apttus_Config2__Description__c = groupDesc;       
            
        return groupSO;
        
    } 
    
	
	public static Apttus_Config2__LineItem__c getLineItem(ID configId,
																	                             ID groupId,
																	                             Integer lineNumber,
																	                             Boolean isPrimaryLine,
																	                             Integer itemSeq,
																	                             String lineType,
																	                             ID productId,
																	                             Boolean customizable,
																	                             ID productOptionId,
																	                             ID optionId,
																	                             ID classId,
																	                             String classHierarchy,
																	                             Decimal quantity,
																	                             Boolean isQtyModifiable,
																	                             String uom,
																	                             Integer term,
																	                             ID priceListId,
																	                             ID plItemId,
																	                             String priceType,
																	                             String priceMethod,
																	                             String chargeType,
																	                             String frequency,
																	                             Boolean allowManualAdj,
																	                             Boolean allocateGroupAdj,
																	                             Decimal listPrice,
																	                             Decimal basePrice,
																	                             String basePriceMethod,
																	                             Decimal baseExtPrice,
																	                             Decimal optionPrice,
																	                             Decimal extPrice,
											                            						 String lineDesc){
		
		Apttus_Config2__LineItem__c lineItemSO = new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = configId);
	    lineItemSO.Apttus_Config2__LineNumber__c = lineNumber;
	    lineItemSO.Apttus_Config2__IsPrimaryLine__c = isPrimaryLine;
	    lineItemSO.Apttus_Config2__PrimaryLineNumber__c = 1;
	    lineItemSO.Apttus_Config2__ItemSequence__c = itemSeq;
	    lineItemSO.Apttus_Config2__SummaryGroupId__c = groupId;
	    lineItemSO.Apttus_Config2__LineType__c = lineType;
	    lineItemSO.Apttus_Config2__ProductId__c = productId;
	    lineItemSO.Apttus_Config2__Customizable__c = customizable;
	    lineItemSO.Apttus_Config2__ProductOptionId__c = productOptionId;
	    lineItemSO.Apttus_Config2__OptionId__c = optionId;
	    lineItemSO.Apttus_Config2__ClassificationId__c = classId;
	    lineItemSO.Apttus_Config2__ClassificationHierarchy__c = classHierarchy;
	    lineItemSO.Apttus_Config2__Quantity__c = quantity;
	    lineItemSO.Apttus_Config2__IsQuantityModifiable__c = isQtyModifiable;
	    lineItemSO.Apttus_Config2__Uom__c = uom;
	    lineItemSO.Apttus_Config2__Term__c = term;
	    lineItemSO.Apttus_Config2__PriceListId__c = priceListId;
	    lineItemSO.Apttus_Config2__PriceListItemId__c = plItemId;
	    lineItemSO.Apttus_Config2__PriceType__c = priceType;
	    lineItemSO.Apttus_Config2__PriceMethod__c = priceMethod;
	    lineItemSO.Apttus_Config2__ChargeType__c = chargeType;
	    lineItemSO.Apttus_Config2__Frequency__c = frequency;
	    lineItemSO.Apttus_Config2__AllowManualAdjustment__c = allowManualAdj;
	    lineItemSO.Apttus_Config2__AllocateGroupAdjustment__c = allocateGroupAdj;
	    lineItemSO.Apttus_Config2__ListPrice__c = listPrice;
	    lineItemSO.Apttus_Config2__BasePrice__c = basePrice;
	    lineItemSO.Apttus_Config2__BasePriceMethod__c = basePriceMethod;
	    lineItemSO.Apttus_Config2__BaseExtendedPrice__c = baseExtPrice;
	    lineItemSO.Apttus_Config2__OptionPrice__c = optionPrice;
	    lineItemSO.Apttus_Config2__ExtendedPrice__c = extPrice;
	    lineItemSO.Apttus_Config2__Description__c = lineDesc;
	    lineItemSO.Apttus_Config2__AdjustedPrice__c = 0;
	    lineItemSO.Apttus_Config2__HasAttributes__c = false;
		lineItemSO.Apttus_Config2__NetPrice__c = baseExtPrice * term;
    	return lineItemSO;
	}
				
	public static Apttus_Config2__PriceListItem__c getPriceListItem(Id plId, Id prodId,String chargeType,String priceType,String priceMethod,Decimal listPrice, Decimal minPrice, Decimal maxPrice, String minMaxPriceAppliesTo){
		
		Apttus_Config2__PriceListItem__c itemSO = new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = plId);
	    itemSO.Apttus_Config2__ProductId__c = prodId;
	    itemSO.Apttus_Config2__ChargeType__c = chargeType;
	    itemSO.Apttus_Config2__PriceType__c = priceType;
	    itemSO.Apttus_Config2__PriceMethod__c = priceMethod;
	    itemSO.Apttus_Config2__ListPrice__c = listPrice;
	    itemSO.Apttus_Config2__MinPrice__c = minPrice;
	    itemSO.Apttus_Config2__MaxPrice__c = maxPrice;
	    itemSO.Apttus_Config2__MinMaxPriceAppliesTo__c = minMaxPriceAppliesTo;
	    itemSO.Apttus_Config2__EffectiveDate__c = Date.today().addDays(-100);
	    itemSO.Apttus_Config2__ExpirationDate__c = Date.today().addDays(365);
	    return itemSO;
	}

	public static Apttus_Config2__PriceDimension__c getPriceDimension(String name, Integer seq,String contextType,String bObjAPIName,String fieldAPIName){
		
		Apttus_Config2__PriceDimension__c pDimension = new Apttus_Config2__PriceDimension__c(Name = name);
	    pDimension.Apttus_Config2__ContextType__c = contextType;
	    pDimension.Apttus_Config2__BusinessObject__c = bObjAPIName;
	    pDimension.Apttus_Config2__Datasource__c = fieldAPIName;
	    
	    return pDimension;
	}

	public static Apttus_Config2__PriceMatrix__c getPriceMatrix(Id plistId, Integer seq,String matrixType,Id dimensionId,String dimesionVal){
		
		Apttus_Config2__PriceMatrix__c pMatrix = new Apttus_Config2__PriceMatrix__c(Apttus_Config2__PriceListItemId__c = plistId);
	    pMatrix.Apttus_Config2__Sequence__c = seq;
	    pMatrix.Apttus_Config2__MatrixType__c = matrixType;
	    pMatrix.Apttus_Config2__Dimension1Id__c = dimensionId;
	    pMatrix.Apttus_Config2__Dimension1ValueType__c = dimesionVal;
	    
	    return pMatrix;
	}

	public static Apttus_Config2__PriceMatrixEntry__c getPriceMatrixEntry(Id pMatId, Integer seq,String dimeval,String adjustType,Double adjAmt,Double priceOverride){
		
		Apttus_Config2__PriceMatrixEntry__c pMatEntry = new Apttus_Config2__PriceMatrixEntry__c(Apttus_Config2__PriceMatrixId__c = pMatId);
	    pMatEntry.Apttus_Config2__Sequence__c = seq;
	    pMatEntry.Apttus_Config2__Dimension1Value__c = dimeval;
	    pMatEntry.Apttus_Config2__AdjustmentType__c = adjustType;
	    pMatEntry.Apttus_Config2__AdjustmentAmount__c = adjAmt;
	    pMatEntry.Apttus_Config2__PriceOverride__c = priceOverride;
	    
	    return pMatEntry;
	}

	public static APTS_CPQ_Discount__c getCPQDiscount(String name, String agmtRTType,String priceSchema, String priceRule){
		APTS_CPQ_Discount__c cPQDiscount = new APTS_CPQ_Discount__c();
		cPQDiscount.Name = name;
		cPQDiscount.GHI_CPQ_Agreement_Record_Type__c = agmtRTType;
		cPQDiscount.GHI_CPQ_Pricing_Schema__c = priceSchema;
		cPQDiscount.GHI_CPQ_Pricing_Rule__c  = priceRule;

		return cPQDiscount;
	}
	
}