/*************************************************************
@Name: APTS_Utility
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 06/18/2015
@Description: Utility Class for common functions
@UsedBy: 
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
public with sharing class APTS_Utility {

     //Build the Query String
    public static String buildAllFieldsString(String objName)
    {
    	Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(objName).getDescribe().fields.getMap();
 		
 		 String query = '';
          for (String fieldName : fieldMap.keySet())
          {
              query += fieldName + ',';
          }
          query = query.substring(0,query.length()-1);
        
        return query;
    }

     //Build the Query String
    public static String buildQueryAllString(String objName)
    {
    	Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(objName).getDescribe().fields.getMap();
 		
 		 String query = 'Select ';
          for (String fieldName : fieldMap.keySet())
          {
              query += fieldName + ',';
          }
          query = query.substring(0,query.length()-1);
          query = query + ' From '+ objName;
        
        return query;
    }

}