/*************************************************************
@Name: APTS_ValidationCallback
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 04/01/2015
@Description: Validation callback class to validate the cart data.
@UsedBy: Cart page
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
global class APTS_ValidationCallback implements Apttus_Config2.CustomClass.IValidationCallback2 {
/**
* Callback to validate the line items in the cart
* @param cart the cart object to validate
* @return the validation result
*/

    global Apttus_Config2.CustomClass.ValidationResult validateCart(Apttus_Config2.ProductConfiguration cart) {
    	System.debug('****validateCart called--');
        Apttus_Config2.CustomClass.ValidationResult result = new Apttus_Config2.CustomClass.ValidationResult(true);
        try{
	       Apttus_Config2__ProductConfiguration__c productConfiguration = cart.getConfigSO();
	        List <Apttus_Config2.LineItem> allLines = cart.getLineItems();
	        List<Apttus_Config2__LineItem__C> allLineItems = getLineItems(allLines);       
	        result.isSuccess=  true;
	        System.debug('productConfiguration-'+productConfiguration);
	           System.debug('allLines-'+allLines);
	        //display error message if adjustment amount is manually given in the cart page
	        String errorMessage='';
	        Map<String,Map<String,APTS_CPQ_Discount__c>> agmtRTbasedDiscountMap = getAgmtRTbasedDiscountMap();
	        System.debug('agmtRTbasedDiscountMap--'+agmtRTbasedDiscountMap);

	        //Getting Product Goverment Matrix pricing for validating with private fares
	        Set<Id> priceListItems = new Set<Id>();
	         for(Apttus_Config2__LineItem__C lineItem : allLineItems)
	        {
		        if((lineItem.GHI_CPQ_Record_Type__c == APTS_Constants.AGMT_RT_US_PRIVATE 
		        	|| lineItem.GHI_CPQ_Record_Type__c == APTS_Constants.AGMT_RT_US_FACL)
		        	&& lineItem.Apttus_Config2__PriceListItemId__c != null)
	         	{
	         		priceListItems.add(lineItem.Apttus_Config2__PriceListItemId__c);
	         	}
		     }

		     Map<Id,Double> prodGovListpricesMap = new Map<Id,Double>();
		     List<Apttus_Config2__PriceMatrix__c> priceMatrices = [Select Id, Apttus_Config2__PriceListItemId__c,
		     	(Select Id,Apttus_Config2__PriceMatrixId__c,Apttus_Config2__Dimension1Value__c, Apttus_Config2__AdjustmentAmount__c 
		     		From Apttus_Config2__MatrixEntries__r) 
				From Apttus_Config2__PriceMatrix__c 
				Where Apttus_Config2__PriceListItemId__c in : priceListItems];

			if(priceMatrices != null && !priceMatrices.isEmpty())
			{
				for(Apttus_Config2__PriceMatrix__c priceMat : priceMatrices)
				{
					List<Apttus_Config2__PriceMatrixEntry__c> priceMatentries = priceMat.Apttus_Config2__MatrixEntries__r;
					for(Apttus_Config2__PriceMatrixEntry__c priceMatEntry : priceMat.Apttus_Config2__MatrixEntries__r){
						if(priceMatEntry.Apttus_Config2__Dimension1Value__c == APTS_Constants.PRICE_MATRIX_GOV_DME)
						{
							prodGovListpricesMap.put(priceMat.Apttus_Config2__PriceListItemId__c,priceMatEntry.Apttus_Config2__AdjustmentAmount__c);
						}
					}
				}
			}
			System.debug('prodGovListpricesMap--'+prodGovListpricesMap);
			Boolean validateExists = true;
	        for(Apttus_Config2__LineItem__C lineItem : allLineItems)
	        {
	         if(lineItem.Apttus_Config2__StartDate__c == null){
	         	{
         			result.Messages.add(new apexpages.Message(Apexpages.Severity.Error,Label.APTS_LI_START_DATE_REQUIRED));
         			validateExists = false;
         		}
	         }

	         if(lineItem.GHI_CPQ_Grid_Override__c != null && lineItem.GHI_CPQ_Grid_Override__c == false
	         	&& lineItem.GHI_CPQ_Record_Type__c != null 
	         		&& lineItem.Apttus_Config2__AdjustmentType__c != null 
	         				&& lineItem.Apttus_Config2__AdjustmentAmount__c != null)
	         {
	         	Double netPrice = 0.0;
	         	Double discountPercentage = 0.0;
	         	//based on adjustment amount calculate netprice and discount percentage
 			 	if(lineItem.Apttus_Config2__AdjustmentType__c == '% Discount')
 			 	{
			 		discountPercentage = lineItem.Apttus_Config2__AdjustmentAmount__c;
			 		netPrice = lineItem.Apttus_Config2__ListPrice__c - (lineItem.Apttus_Config2__ListPrice__c*lineItem.Apttus_Config2__AdjustmentAmount__c/100);
		 		}else if(lineItem.Apttus_Config2__AdjustmentType__c == 'Discount Amount')
		 		{
		 			discountPercentage = (lineItem.Apttus_Config2__AdjustmentAmount__c/lineItem.Apttus_Config2__ListPrice__c)*100;
		 			netPrice = lineItem.Apttus_Config2__ListPrice__c - lineItem.Apttus_Config2__AdjustmentAmount__c;
		 		}else if(lineItem.Apttus_Config2__AdjustmentType__c == 'Price Override')
		 		{
		 			//if(lineItem.Apttus_Config2__AdjustmentAmount__c <= lineItem.Apttus_Config2__ListPrice__c)
		 			discountPercentage = ((lineItem.Apttus_Config2__ListPrice__c-lineItem.Apttus_Config2__AdjustmentAmount__c)/lineItem.Apttus_Config2__ListPrice__c)*100;
		 			netPrice = lineItem.Apttus_Config2__AdjustmentAmount__c;
		 		} 
	         	//	If agreement record type is 'US Private'/'US Facilities/Hospital' and for some Pricing Schemas Discount perentage should not be >6%.
	         	if(lineItem.GHI_CPQ_Record_Type__c == APTS_Constants.AGMT_RT_US_PRIVATE || lineItem.GHI_CPQ_Record_Type__c == APTS_Constants.AGMT_RT_US_FACL)
	         	{
	         		if(prodGovListpricesMap.containsKey(lineItem.Apttus_Config2__PriceListItemId__c) 
	         			&& netPrice < prodGovListpricesMap.get(lineItem.Apttus_Config2__PriceListItemId__c))
	         		{
	         			result.Messages.add(new apexpages.Message(Apexpages.Severity.Error,Label.APTS_CONT_RATE_ERR_MSG));
	         			//result.isSuccess=false;
	         			validateExists = false;
	         		}

	         		if(lineItem.GHI_CPQ_Pricing_Schema__c != null 
	         			&& agmtRTbasedDiscountMap.containsKey(lineItem.GHI_CPQ_Record_Type__c) 
	         				&& agmtRTbasedDiscountMap.get(lineItem.GHI_CPQ_Record_Type__c).containsKey(lineItem.GHI_CPQ_Pricing_Schema__c))
	         		{
	         			APTS_CPQ_Discount__c discountObj = agmtRTbasedDiscountMap.get(lineItem.GHI_CPQ_Record_Type__c).get(lineItem.GHI_CPQ_Pricing_Schema__c);
	         			System.debug('***Discount percentages-'+discountPercentage+'--'+Double.valueOf(discountObj.GHI_CPQ_Pricing_Rule__c));
	         			if(discountPercentage > Double.valueOf(discountObj.GHI_CPQ_Pricing_Rule__c))
	         			{
	         				result.Messages.add(new apexpages.Message(Apexpages.Severity.Error,Label.APTS_DISCOUNT_ERROR_MSG+' '+discountObj.GHI_CPQ_Pricing_Rule__c+'% '));
	         				//result.isSuccess=false;
	         				validateExists = false;
	         			}
	         			
	         		}
	         	}
	         	//	If agreement record type is ‘US Government’  ‘ and for some Pricing Schemas the net price should not be greater than list price
	         	else if(lineItem.GHI_CPQ_Record_Type__c == APTS_Constants.AGMT_RT_US_GOV) //US GOV recor
	         	{
	         		System.debug('***US GOV RT block--'+lineItem.GHI_CPQ_Record_Type__c);
	         		if(lineItem.GHI_CPQ_Pricing_Schema__c != null 
	         			&& agmtRTbasedDiscountMap.containsKey(lineItem.GHI_CPQ_Record_Type__c) 
	         				&& agmtRTbasedDiscountMap.get(lineItem.GHI_CPQ_Record_Type__c).containsKey(lineItem.GHI_CPQ_Pricing_Schema__c))
	         		{
	         			APTS_CPQ_Discount__c discountObj = agmtRTbasedDiscountMap.get(lineItem.GHI_CPQ_Record_Type__c).get(lineItem.GHI_CPQ_Pricing_Schema__c);
	         			if(netPrice > lineItem.Apttus_Config2__ListPrice__c)
	         			{
	         				result.Messages.add(new apexpages.Message(Apexpages.Severity.Error,Label.APTS_GOV_NET_ERR_MSG));
	         				//result.isSuccess=false;
	         				validateExists = false;
	         			}
	         			
	         		}
	         		//For 20% validation for one of the pricing schema
	         		if(lineItem.GHI_CPQ_Pricing_Schema__c != null 
	         			&& agmtRTbasedDiscountMap.containsKey(lineItem.GHI_CPQ_Record_Type__c) 
	         				&& agmtRTbasedDiscountMap.get(lineItem.GHI_CPQ_Record_Type__c).containsKey(lineItem.GHI_CPQ_Pricing_Schema__c+'_%'))
	         		{
	         			APTS_CPQ_Discount__c discountObj = agmtRTbasedDiscountMap.get(lineItem.GHI_CPQ_Record_Type__c).get(lineItem.GHI_CPQ_Pricing_Schema__c+'_%');
	         			System.debug('***Discount percentages-'+discountPercentage+'--'+Double.valueOf(discountObj.GHI_CPQ_Pricing_Rule__c));
	         			if(discountPercentage > Double.valueOf(discountObj.GHI_CPQ_Pricing_Rule__c))
	         			{
	         				result.Messages.add(new apexpages.Message(Apexpages.Severity.Error,Label.APTS_DISCOUNT_ERROR_MSG+' '+discountObj.GHI_CPQ_Pricing_Rule__c+'% '));
	         				//result.isSuccess=false;
	         				validateExists = false;
	         			}
	         			
	         		}
	         	}/*else if(lineItem.GHI_CPQ_Record_Type__c == APTS_Constants.AGMT_RT_US_FACL) //US GOV recor
	         	{
	         		System.debug('***US Facilities RT block--'+lineItem.GHI_CPQ_Record_Type__c);
	         		if(prodGovListpricesMap.containsKey(lineItem.Apttus_Config2__PriceListItemId__c) 
	         			&& netPrice < prodGovListpricesMap.get(lineItem.Apttus_Config2__PriceListItemId__c))
	         		{
	         			result.Messages.add(new apexpages.Message(Apexpages.Severity.Error,Label.APTS_CONT_RATE_ERR_MSG));
	         			//result.isSuccess=false;
	         			validateExists = false;
	         		}
	         	}*/
		         
	         }
	         
	        }
	        if(!validateExists ){
	        	result.isSuccess=false;
	        }
        
         }catch(Exception e)
        {
        	System.debug('***Validation callback Exception--'+e.getMessage());
        }
        return result;
    }
    
    
    /**
* Callback to validate the given list ramp line items
* @param cart the cart object associated with the ramp line items
* @param rampLineItems the list of ramp line items
* @return the validation result
*/
    global Apttus_Config2.CustomClass.ValidationResult validateRampLineItems(Apttus_Config2.ProductConfiguration cart, List<Apttus_Config2.LineItem> rampLineItems) {
        Apttus_Config2.CustomClass.ValidationResult result = new Apttus_Config2.CustomClass.ValidationResult(true);
        
        //Build your logic and set error message
        //result.isSuccess =  true; set if no error
        //result.isSuccess =  true; set if there is error
        //result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR,'Errro message')); 
        
        //result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter start Date post BED')); 
        return result;
        
        
    }
    
    
    /* Gets the list of product line items associated with the Battery line
* @param cart the cart object
* @return the list of line item objects
*/
    private static List<Apttus_Config2__LineItem__C> getLineItems(List<Apttus_Config2.LineItem> allLines) {
        
        List<Apttus_Config2__LineItem__C> lineItems = new List<Apttus_Config2__LineItem__C>();
        // iterate thru the cart and get the line items matching the battery code1 
        for (Apttus_Config2.LineItem lineItemMO : allLines) {
            lineItems.add(lineItemMO.getLineItemSO());
        }
        
        return lineItems;   
        
    }
    /* Gets the map og agreement record type key based map with list of pricing schema discounts */
    private static Map<String,Map<String,APTS_CPQ_Discount__c>> getAgmtRTbasedDiscountMap(){
    	Map<String,Map<String,APTS_CPQ_Discount__c>> agmtRTbasedDiscountMap = new Map<String,Map<String,APTS_CPQ_Discount__c>>(); 
    	Map<String,APTS_CPQ_Discount__c> psDiscountMap = null;
        for (APTS_CPQ_Discount__c cpqDiscount : APTS_CPQ_Discount__c.getAll().values()){			
			psDiscountMap = agmtRTbasedDiscountMap.containsKey(cpqDiscount.GHI_CPQ_Agreement_Record_Type__c)?agmtRTbasedDiscountMap.get(cpqDiscount.GHI_CPQ_Agreement_Record_Type__c): new Map<String,APTS_CPQ_Discount__c>();
			psDiscountMap.put(cpqDiscount.GHI_CPQ_Pricing_Schema__c,cpqDiscount);
			agmtRTbasedDiscountMap.put(cpqDiscount.GHI_CPQ_Agreement_Record_Type__c,psDiscountMap);		
		}
        return agmtRTbasedDiscountMap;
    }

    /**
* Callback to validate the given list of asset items
* @param cart the cart object associated with the asset items
* @param assetItems the list of asset items
* @return the validation result
*/
    global Apttus_Config2.CustomClass.ValidationResult validateAssetItems(Apttus_Config2.ProductConfiguration cart, List<Apttus_Config2__TempRenew__c> assetItems) {
        return new Apttus_Config2.CustomClass.ValidationResult(true);
        
    }
    
}