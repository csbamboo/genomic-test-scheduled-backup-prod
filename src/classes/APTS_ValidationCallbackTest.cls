/*************************************************************
@Name: APTS_ValidationCallbackTest
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 04/6/2015
@Description: Test class for APTS_ValidationCallback
@UsedBy: 
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
@isTest(SeeAllData = True)
private class APTS_ValidationCallbackTest {
    
    @isTest static void validateCartTest() {
        
       Account acc = APTS_TestDataUtil.getAccount('TestAcc1','Payor','Payor Type');
        insert acc;

        //Opportunity opty = APTS_TestDataUtil.getOpportunity(acc,'Pre-Close','Delivery Assurance','Email Event');
        //insert opty;
        
        Apttus_Config2__PriceList__c plist = APTS_TestDataUtil.getPriceList();
        insert pList;

        Product2 prod1 = APTS_TestDataUtil.getProduct('Oncotype DX1', 'DX1', 'Standalone', 'Cancer');
        Product2 prod2 = APTS_TestDataUtil.getProduct('Oncotype DX2', 'DX2', 'Standalone', 'Cancer');
        insert new List<Product2>{prod1, prod2};

        Apttus_Config2__PriceListItem__c plItem1 = APTS_TestDataUtil.getPriceListItem(plist.Id, prod1.Id, 'Standard Price', 'One Time', 'Per Unit', 9485, 9000, 10000, 'Unit Price');
        Apttus_Config2__PriceListItem__c plItem2 = APTS_TestDataUtil.getPriceListItem(plist.Id, prod2.Id, 'Standard Price', 'One Time', 'Per Unit', 9485, 9000, 10000, 'Unit Price');
        insert new List<Apttus_Config2__PriceListItem__c>{plItem1, plItem2};

        Apttus_Config2__PriceDimension__c priceDimension = APTS_TestDataUtil.getPriceDimension('Private/Government', 1,'Line Item','Apttus_Config2__LineItem__c','GHI_CPQ_Private_Government__c');
        insert priceDimension;

        Apttus_Config2__PriceMatrix__c priceMat = APTS_TestDataUtil.getPriceMatrix(plItem1.id, 1,'Dimension',priceDimension.Id,'Discrete');
        insert priceMat;

        Apttus_Config2__PriceMatrixEntry__c priceMatEntry1 = APTS_TestDataUtil.getPriceMatrixEntry(priceMat.Id, 1,'Government','List Price Override',3347,3347);
        Apttus_Config2__PriceMatrixEntry__c priceMatEntry2 = APTS_TestDataUtil.getPriceMatrixEntry(priceMat.Id, 2,'Government','List Price Override',4510,4510);
        insert new List<Apttus_Config2__PriceMatrixEntry__c>{priceMatEntry1, priceMatEntry2};
        
        Id rtId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(APTS_Constants.AGMT_RT_US_PRIVATE).getRecordTypeId();

        APTS_CPQ_Discount__c cpQDisount1 = APTS_TestDataUtil.getCPQDiscount('US Private Comm', 'US Private','Commercial', '6');
        APTS_CPQ_Discount__c cpQDisount2 = APTS_TestDataUtil.getCPQDiscount('US Private_Managed Medi', 'US Private','Managed Medicaid', '6');
        insert new List<APTS_CPQ_Discount__c>{cpQDisount1, cpQDisount2};

        Apttus__APTS_Agreement__c agmtObj = APTS_TestDataUtil.createAgreement(rtId,acc.Id,'Request','Request'); 
        agmtObj.Apttus_CMConfig__PriceListId__c = pList.Id;
        insert agmtObj; 
        
        Apttus_Config2__ProductConfiguration__c pConfig = APTS_TestDataUtil.getProductConfiguration('Product Configuration', 1, agmtObj.Id, 'Agreement', 'Category', 
            pList.Id, null, 'Finalized', null, Datetime.now(), true, 'test apttus config');
            
        insert pConfig;

         // STEP VI - Create new ad hoc groups
        Apttus_Config2__AdHocGroup__c adGroupSO = APTS_TestDataUtil.createAdHocGroup('Group A',pConfig.Id,'Group A'); 
        insert adGroupSO;

        Apttus_Config2__LineItem__c lineItem1 = APTS_TestDataUtil.getLineItem(pConfig.Id, null, 1, true, 1, 'Product/Service', prod1.Id, 
            true, null, null, null, null, 1, true, 'Each', 1, plist.Id, plItem1.Id, 'One Time', 'Per Unit', null, null, true, true, 9485, 9485, 
            'Per Unit', 9485, null, 9485, 'Oncotype DX1');

        Apttus_Config2__LineItem__c lineItem2 = APTS_TestDataUtil.getLineItem(pConfig.Id, null, 1, true, 1, 'Product/Service', prod2.Id, 
            true, null, null, null, null, 1, true, 'Each', 1, plist.Id, plItem2.Id, 'One Time', 'Per Unit', null, null, true, true, 9485, 9485, 
            'Per Unit', 9485, null, 9485, 'Oncotype DX12');

        insert new List<Apttus_Config2__LineItem__c>{lineItem1,lineItem2};

        Apttus_Config2__ConfigCustomClasses__c custSetting = new Apttus_Config2__ConfigCustomClasses__c();
        custSetting.Apttus_Config2__ValidationCallbackClass__c = 'APTS_PricingCallBack';
        custSetting.Name = 'System Properties';
        //insert custSetting;

         Test.startTest(); 

        List<Apttus_Config2__LineItem__c> lineItemList = [select id,
                                                            Apttus_Config2__PricingStatus__c,GHI_CPQ_Pricing_Schema__c,
                                                            Apttus_Config2__AdjustmentAmount__c
                                                            from Apttus_Config2__LineItem__c
                                                 where Apttus_Config2__ConfigurationId__c=:pConfig.id]; 
      
   
         for(Apttus_Config2__LineItem__c lineItem:lineItemList )
         {
          //lineItem.APTS_Year__c=3;
          lineItem.Apttus_Config2__PricingStatus__c = 'Pending';
          lineItem.GHI_CPQ_Pricing_Schema__c='Managed Medicaid';
          lineItem.Apttus_Config2__AdjustmentType__c = 'Price Override';
          lineItem.Apttus_Config2__AdjustmentAmount__c=3000;
         }
         
         update lineItemList;
        
        
        Apttus_Config2.CustomClass.ValidationResult vResult1 = Apttus_Config2.CPQWebService.validateCart(pConfig.ID);
        boolean isSuccess1 = vResult1.IsSuccess;
        system.debug('***************isSuccess1 ' + isSuccess1);

         for(Apttus_Config2__LineItem__c lineItem:lineItemList )
         {
          //lineItem.APTS_Year__c=3;
          lineItem.Apttus_Config2__PricingStatus__c = 'Pending';
          lineItem.GHI_CPQ_Pricing_Schema__c='Managed Medicaid';
          lineItem.Apttus_Config2__AdjustmentType__c = '% Discount';
          lineItem.Apttus_Config2__AdjustmentAmount__c=10;
         }
         
         update lineItemList.get(0);

        Apttus_Config2.CustomClass.ValidationResult vResult2 = Apttus_Config2.CPQWebService.validateCart(pConfig.ID);
        boolean isSuccess2 = vResult2.IsSuccess;
        system.debug('***************isSuccess2 ' + isSuccess2); 

      
        APTS_ValidationCallback controller = new APTS_ValidationCallback();
        Apttus_Config2.ProductConfiguration cart;
   
        Apttus_Config2.CustomClass.ValidationResult result1 = controller.validateAssetItems(cart, null);

        Apttus_Config2.CustomClass.ValidationResult result2 = controller.validateRampLineItems(cart, null);

       Test.stopTest();

    }

    @isTest static void validateGovProductsCart() {
        
       Account acc = APTS_TestDataUtil.getAccount('TestAcc2','Payor','Payor Type');
        insert acc;
        
        Apttus_Config2__PriceList__c plist = APTS_TestDataUtil.getPriceList();
        insert pList;

        Product2 prod1 = APTS_TestDataUtil.getProduct('Oncotype DX1', 'DX1', 'Standalone', 'Cancer');
        Product2 prod2 = APTS_TestDataUtil.getProduct('Oncotype DX2', 'DX2', 'Standalone', 'Cancer');
        insert new List<Product2>{prod1, prod2};

        Apttus_Config2__PriceListItem__c plItem1 = APTS_TestDataUtil.getPriceListItem(plist.Id, prod1.Id, 'Standard Price', 'One Time', 'Per Unit', 9485, 9000, 10000, 'Unit Price');
        Apttus_Config2__PriceListItem__c plItem2 = APTS_TestDataUtil.getPriceListItem(plist.Id, prod2.Id, 'Standard Price', 'One Time', 'Per Unit', 9485, 9000, 10000, 'Unit Price');
        insert new List<Apttus_Config2__PriceListItem__c>{plItem1, plItem2};

        Apttus_Config2__PriceDimension__c priceDimension = APTS_TestDataUtil.getPriceDimension('Private/Government', 1,'Line Item','Apttus_Config2__LineItem__c','GHI_CPQ_Private_Government__c');
        insert priceDimension;

        Apttus_Config2__PriceMatrix__c priceMat = APTS_TestDataUtil.getPriceMatrix(plItem1.id, 1,'Dimension',priceDimension.Id,'Discrete');
        insert priceMat;

        Apttus_Config2__PriceMatrixEntry__c priceMatEntry1 = APTS_TestDataUtil.getPriceMatrixEntry(priceMat.Id, 1,'Government','List Price Override',3347,3347);
        Apttus_Config2__PriceMatrixEntry__c priceMatEntry2 = APTS_TestDataUtil.getPriceMatrixEntry(priceMat.Id, 2,'Government','List Price Override',4510,4510);
        insert new List<Apttus_Config2__PriceMatrixEntry__c>{priceMatEntry1, priceMatEntry2};
        
        Id rtId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(APTS_Constants.AGMT_RT_US_GOV).getRecordTypeId();

        APTS_CPQ_Discount__c cpQDisount1 = APTS_TestDataUtil.getCPQDiscount(APTS_Constants.AGMT_RT_US_GOV+'_MedicareTest', APTS_Constants.AGMT_RT_US_GOV,'Medicare', '6');
        APTS_CPQ_Discount__c cpQDisount2 = APTS_TestDataUtil.getCPQDiscount(APTS_Constants.AGMT_RT_US_GOV+'_MedicaidTest_%', APTS_Constants.AGMT_RT_US_GOV,'Medicaid_%', '20');
        insert new List<APTS_CPQ_Discount__c>{cpQDisount1, cpQDisount2};

        Apttus__APTS_Agreement__c agmtObj = APTS_TestDataUtil.createAgreement(rtId,acc.Id,'Request','Request'); 
        agmtObj.Apttus_CMConfig__PriceListId__c = pList.Id;
        insert agmtObj; 
        
        Apttus_Config2__ProductConfiguration__c pConfig = APTS_TestDataUtil.getProductConfiguration('Product Configuration', 1, agmtObj.Id, 'Agreement', 'Category', 
            pList.Id, null, 'Finalized', null, Datetime.now(), true, 'test apttus config');
            
        insert pConfig;

         // STEP VI - Create new ad hoc groups
        Apttus_Config2__AdHocGroup__c adGroupSO = APTS_TestDataUtil.createAdHocGroup('Group A',pConfig.Id,'Group A'); 
        insert adGroupSO;

        Apttus_Config2__LineItem__c lineItem1 = APTS_TestDataUtil.getLineItem(pConfig.Id, null, 1, true, 1, 'Product/Service', prod1.Id, 
            true, null, null, null, null, 1, true, 'Each', 1, plist.Id, plItem1.Id, 'One Time', 'Per Unit', null, null, true, true, 9485, 9485, 
            'Per Unit', 9485, null, 9485, 'Oncotype DX1');

        Apttus_Config2__LineItem__c lineItem2 = APTS_TestDataUtil.getLineItem(pConfig.Id, null, 1, true, 1, 'Product/Service', prod2.Id, 
            true, null, null, null, null, 1, true, 'Each', 1, plist.Id, plItem2.Id, 'One Time', 'Per Unit', null, null, true, true, 9485, 9485, 
            'Per Unit', 9485, null, 9485, 'Oncotype DX12');

        insert new List<Apttus_Config2__LineItem__c>{lineItem1,lineItem2};

        Apttus_Config2__ConfigCustomClasses__c custSetting = new Apttus_Config2__ConfigCustomClasses__c();
        custSetting.Apttus_Config2__ValidationCallbackClass__c = 'APTS_PricingCallBack';
        custSetting.Name = 'System Properties';
        //insert custSetting;

         Test.startTest(); 

        List<Apttus_Config2__LineItem__c> lineItemList = [select id,
                                                            Apttus_Config2__PricingStatus__c,GHI_CPQ_Pricing_Schema__c,
                                                            Apttus_Config2__AdjustmentAmount__c
                                                            from Apttus_Config2__LineItem__c
                                                 where Apttus_Config2__ConfigurationId__c=:pConfig.id]; 
      
   
         for(Apttus_Config2__LineItem__c lineItem:lineItemList )
         {
          lineItem.Apttus_Config2__PricingStatus__c = 'Pending';
          lineItem.GHI_CPQ_Pricing_Schema__c='Medicaid';
          lineItem.Apttus_Config2__AdjustmentType__c = 'Price Override';
          lineItem.Apttus_Config2__AdjustmentAmount__c=15000;
         }
         
         update lineItemList;
        
        
        Apttus_Config2.CustomClass.ValidationResult vResult1 = Apttus_Config2.CPQWebService.validateCart(pConfig.ID);
        boolean isSuccess1 = vResult1.IsSuccess;
        system.debug('***************isSuccess1 ' + isSuccess1);

        Id faclRtId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(APTS_Constants.AGMT_RT_US_FACL).getRecordTypeId();
         for(Apttus_Config2__LineItem__c lineItem:lineItemList )
         {
          //lineItem.GHI_CPQ_Record_Type__c = faclRtId;
          lineItem.Apttus_Config2__PricingStatus__c = 'Pending';
          lineItem.Apttus_Config2__AdjustmentType__c = 'Price Override';
          lineItem.Apttus_Config2__AdjustmentAmount__c=3000;
         }
         
         update lineItemList.get(0);

        Apttus_Config2.CustomClass.ValidationResult vResult2 = Apttus_Config2.CPQWebService.validateCart(pConfig.ID);
        boolean isSuccess2 = vResult2.IsSuccess;
        system.debug('***************isSuccess2 ' + isSuccess2); 

         for(Apttus_Config2__LineItem__c lineItem:lineItemList )
         {
          lineItem.Apttus_Config2__PricingStatus__c = 'Pending';
          lineItem.Apttus_Config2__StartDate__c = null;
         }
         
         update lineItemList.get(0);
         Apttus_Config2.CustomClass.ValidationResult vResult3 = Apttus_Config2.CPQWebService.validateCart(pConfig.ID);
        boolean isSuccess3 = vResult3.IsSuccess;
        system.debug('***************isSuccess3 ' + isSuccess3); 

      
        APTS_ValidationCallback controller = new APTS_ValidationCallback();
        Apttus_Config2.ProductConfiguration cart;
   
        Apttus_Config2.CustomClass.ValidationResult result1 = controller.validateAssetItems(cart, null);

        Apttus_Config2.CustomClass.ValidationResult result2 = controller.validateRampLineItems(cart, null);

       Test.stopTest();

    }

    
    


}