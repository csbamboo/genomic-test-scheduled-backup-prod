/*
  @author: Amanpreet Singh Sidhu 
  @date: 21 June 2015
  @description: Test Class for OSM_WorkOrderTriggerHandler apex class
  @history: 21 June 2015 - Created (Amanpreet S Sidhu)

*/

@isTest
public class CommunitiesLandingController_Test
{
    public static testMethod void testMyController() 
    {
        /*PageReference pageRef = Page.yourPageName;
        Test.setCurrentPage(pageRef);*/
        
        CommunitiesLandingController controller = new CommunitiesLandingController();
        controller.forwardToStartPage();
    }

}