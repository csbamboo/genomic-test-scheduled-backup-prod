/**
 *  Purpose         :   Controller class for creating test data for order object
 *
 *  Created By      :   
 * 
 *  Created Date    :   04/20/2015
 * 
 *  Revision Logs   :   V_1.0 - Created
 * 
 **/
public without sharing class DataFactoryFutureHelper {
    
    public static Boolean generateXMLForOrderLineItemIsRunning = false;

    //This method is for calling webservice method for - Send Initial order to QDX button or Send Order to Lab
    public static void generateXMLForOrder(List<Order> newRecords, Map<Id, Order> oldRecordsMap) {
        if(OSM_Utilities.generateXMLForOrderRunOnce){
            return;
        }
        
        //OSM_OrderTriggerHandler.globalIsFuture = false;

        // if(OSM_Utilities.generateXMLForOrderRunOnce){
        //     return;
        // }
        
        Set<Id> validOrdr = new Set<Id>();
        
        //Loop through the new records
        for(Order odr : newRecords) {
            
            if(odr.Send_Order_to_Lab__c) {
                validOrdr.add(odr.Id);
            }
        }
        
        if(!validOrdr.isEmpty()){
            //OSM_BatchForOLIXMLProcess b = new OSM_BatchForOLIXMLProcess(validOrdr, 'Order');
            //Database.executeBatch(b, 1);
            if(system.isFuture()){
                callFutureXMLGeneration(validOrdr, 'Order');
                OSM_Utilities.generateXMLForOrderRunOnce = true;
            } else {
                callFutureXMLGenerationWithFuture(validOrdr, 'Order');
                OSM_Utilities.generateXMLForOrderRunOnce = true;
            }
        }
    }
    
    //This method is for calling webservice method for - Send Order Failure to QDX button or Send Order Update to QDX or Send Order Cancel to QDX
    public static void generateXMLForOrderLineItem(List<OrderItem> newRecords, Map<Id, OrderItem> oldRecordsMap) {
        
        if(OSM_Utilities.generateXMLForOrderLineItemRunOnce){
            return;
        }
        
        //OSM_OrderTriggerHandler.globalIsFuture = false;
        
        Set<Id> validOli = new Set<Id>();
        
        //Loop through the new records
        for(OrderItem odrItem : newRecords) {
        	
            if(odrItem.Send_Order_Failure_to_QDX__c && (oldRecordsMap == null || odrItem.Send_Order_Failure_to_QDX__c != oldRecordsMap.get(odrItem.Id).Send_Order_Failure_to_QDX__c)) {
                
                validOli.add(odrItem.Id);
            }
            else if(odrItem.Send_Order_Update_to_QDX__c && (oldRecordsMap == null || odrItem.Send_Order_Update_to_QDX__c != oldRecordsMap.get(odrItem.Id).Send_Order_Update_to_QDX__c)) {
                
                validOli.add(odrItem.Id);
            }
            else if(odrItem.Send_Order_Cancel_to_QDX__c && (oldRecordsMap == null || odrItem.Send_Order_Cancel_to_QDX__c != oldRecordsMap.get(odrItem.Id).Send_Order_Cancel_to_QDX__c)) {
                
                validOli.add(odrItem.Id);
            }
            else if(odrItem.Send_Initial_Order_to_QDX__c && (oldRecordsMap == null || odrItem.Send_Initial_Order_to_QDX__c != oldRecordsMap.get(odrItem.Id).Send_Initial_Order_to_QDX__c)) {
                
                validOli.add(odrItem.Id);
            }
            else if(odrItem.Render_and_Distribute_Data_Message__c && (oldRecordsMap == null || odrItem.Render_and_Distribute_Data_Message__c != oldRecordsMap.get(odrItem.Id).Render_and_Distribute_Data_Message__c)) {
                
                validOli.add(odrItem.Id);
            }
            else if(odrItem.Send_Order_To_Lab__c && (oldRecordsMap == null || odrItem.Send_Order_To_Lab__c != oldRecordsMap.get(odrItem.Id).Send_Order_To_Lab__c)) {
                        
                validOli.add(odrItem.Id);
            }
        }
        
        if(!validOli.isEmpty()){
            //OSM_BatchForOLIXMLProcess b = new OSM_BatchForOLIXMLProcess(validOli, 'OrderItem');
            //Database.executeBatch(b, 1);
            if(system.isFuture()){
            	callFutureXMLGeneration(validOli, 'OrderItem');
                OSM_Utilities.generateXMLForOrderLineItemRunOnce = true;
            } else {
            	callFutureXMLGenerationWithFuture(validOli, 'OrderItem');
                OSM_Utilities.generateXMLForOrderLineItemRunOnce = true;
            }
        }
        
    }
    
    //This method is for calling webservice method for -Send to Lab button or Send Order to Lab
    public static void generateXMLForWorkOrder(List<OSM_Work_Order__c> newRecords, Map<Id, OSM_Work_Order__c> oldRecordsMap) {
        
        if(OSM_Utilities.generateXMLForWorkOrderRunOnce){
            return;
        }
        
        Set<Id> validWo = new Set<Id>();
        
        //Loop through the new records
        for(OSM_Work_Order__c wrkOdr : newRecords) {
            
            if(wrkOdr.Send_Order_To_Lab__c) {
                validWo.add(wrkOdr.Id);
            }
        }
        
        if(!validWo.isEmpty()){
            //OSM_BatchForOLIXMLProcess b = new OSM_BatchForOLIXMLProcess(validWo, 'OSM_Work_Order__c');
            //Database.executeBatch(b, 1);
            if(system.isFuture()){
                callFutureXMLGeneration(validWo, 'OSM_Work_Order__c');
                OSM_Utilities.generateXMLForWorkOrderRunOnce = true;
                
            } else {
                callFutureXMLGenerationWithFuture(validWo, 'OSM_Work_Order__c');
                OSM_Utilities.generateXMLForWorkOrderRunOnce = true;
            }
        }
    }
    
    //This method is for calling webservice method for - Send Initial order to QDX button or Send Order to Lab
    public static void generateXMLForESBInboundMessage(List<ESBInboundMessage__c> newRecords, Map<Id, ESBInboundMessage__c> oldRecordsMap) {
        
        //List of ESB to be updated
        List<ESBInboundMessage__c> listESBToBeUpdated = new List<ESBInboundMessage__c>();
        
        //Loop through the new records
        for(ESBInboundMessage__c eSBIM : newRecords) {
            
            //Check for null and values is changed
            if(eSBIM.IsActive__c && eSBIM.Process_Inbound__c && (oldRecordsMap == null || (eSBIM.Process_Inbound__c != oldRecordsMap.get(eSBIM.Id).Process_Inbound__c || eSBIM.IsActive__c != oldRecordsMap.get(eSBIM.Id).IsActive__c))) {
                
                //Method for button - Process Inbound
                getOrderXmlForProcessInbound(String.valueOf(eSBIM.ESBMessageInstanceId__c), '');
                
                //Add Esb into list to be update
                listESBToBeUpdated.add(new ESBInboundMessage__c(Id = eSBIM.Id, IsActive__c = false, Process_Inbound__c = false));   
            }
        }
        
        //Checking for list size
        if(listESBToBeUpdated.size() > 0)
            update listESBToBeUpdated;
    }
    
    //@future(callout = true)
    public static void getOrderXmlForProcessInbound(String msgId, String task) {
        
        //OSM_OrderTriggerHandler.globalIsFuture = false;
        //Call webservice method
        String responseString = OSM_Messaging.processInbound(msgId, task);
    }
    
    @future(callout=true)
    public static void callFutureXMLGenerationWithFuture(Set <Id> recordIds, String objectName){
        callFutureXMLGeneration(recordIds, objectName);
    }
    
    //@future(callout=true)
    public static void callFutureXMLGeneration(Set <Id> recordIds, String objectName){
        system.debug('callFutureXMLGeneration@@@@@@@@@@@@QUERy' + Limits.getQueries());
        
        if(OSM_Utilities.callFutureXMLGenerationRunOnce){
            return;
        }
        
        OSM_Utilities.callFutureXMLGenerationRunOnce = true;
        String responseString;
        OSM_OrderTriggerHandler.globalIsFuture = false;
        
        if(objectName == 'Order'){
            List<Order> oListToUpdate = new List<Order>();
            
            for (Order oLoop : [Select Id, Send_Order_to_Lab__c From Order Where Id IN: recordIds]) {
                
                if(oLoop.Send_Order_to_Lab__c) {
                    String pName = 'OSM_OrderToLab';
                    
                    //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, null, '');
                    responseString = OSM_Messaging.MakeOrderXML(oLoop.Id, pName, '');
                    if(oLoop.Send_Order_to_Lab__c){
                        oLoop.Send_Order_to_Lab__c = false;
                        oListToUpdate.add(oLoop);
                    }
                }
                
            }
            
            //Added logging functionality - Paul Wittmeyer 8/7/2015 
            if(!oListToUpdate.isEmpty()){
                try{
                    update oListToUpdate;
                } catch(Exception e1){ Logger.debugExceptionForAsync(e1); if(e1.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oListToUpdate; } catch(Exception e2){ Logger.debugExceptionForAsync(e2); if(e2.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oListToUpdate; } catch(Exception e3){ Logger.debugExceptionForAsync(e3); if(e3.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oListToUpdate; } catch(Exception e4){ Logger.debugExceptionForAsync(e4); if(e4.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oListToUpdate; } catch(Exception e5){ Logger.debugExceptionForAsync(e5); if(e5.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oListToUpdate; }catch(exception e){ Logger.debugExceptionForAsync(e); }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        } else if(objectName == 'OrderItem'){
            List<OrderItem> oiListToUpdate = new List<OrderItem>();
            List<Case> caseListToUpdate = new List<Case>();
            
            //Query related cases
            Map<Id, List<Case>> preBillOpencaseMap = new Map<Id, List<Case>>();
            Map<Id, List<Case>> billOpencaseMap = new Map<Id, List<Case>>();
            Map<Id, List<Case>> billClosedcaseMap = new Map<Id, List<Case>>();
            
            for(Case caseList : [SELECT Id, Status, OSM_Primary_Order_Line_Item__c, OSM_BI_Sent_Date__c, RecordType.Name FROM Case WHERE OSM_Primary_Order_Line_Item__c IN: recordIds AND 
                ((RecordType.Name = 'Pre-Billing' AND Status = 'Open') OR 
                (RecordType.Name = 'Billing' AND Status = 'Open') OR 
                (RecordType.Name = 'Billing' AND Status = 'Closed'))  Order By LastModifiedDate DESC]){
                
                if(caseList.RecordType.Name == 'Pre-Billing' && caseList.Status == 'Open'){
                    if(preBillOpencaseMap.get(caseList.OSM_Primary_Order_Line_Item__c) != null){
                        preBillOpencaseMap.get(caseList.OSM_Primary_Order_Line_Item__c).add(caseList);
                    } else {
                        preBillOpencaseMap.put(caseList.OSM_Primary_Order_Line_Item__c, new List<Case>{caseList});
                    }
                }
                
                if(caseList.RecordType.Name == 'Billing' && caseList.Status == 'Open'){
                    if(billOpencaseMap.get(caseList.OSM_Primary_Order_Line_Item__c) != null){
                        billOpencaseMap.get(caseList.OSM_Primary_Order_Line_Item__c).add(caseList);
                    } else {
                        billOpencaseMap.put(caseList.OSM_Primary_Order_Line_Item__c, new List<Case>{caseList});
                    }
                }
                
                if(caseList.RecordType.Name == 'Billing' && caseList.Status == 'Closed'){
                    if(billClosedcaseMap.get(caseList.OSM_Primary_Order_Line_Item__c) != null){
                        billClosedcaseMap.get(caseList.OSM_Primary_Order_Line_Item__c).add(caseList);
                    } else {
                        billClosedcaseMap.put(caseList.OSM_Primary_Order_Line_Item__c, new List<Case>{caseList});
                    }
                }
            }
            
            
            for (OrderItem odrItem : [Select Id, Results_Disposition_Current__c , OSM_State__c, OSM_Lab_and_Report_Status__c, OSM_Billing_Status__c, OrderId, Send_Order_Cancel_to_QDX__c, Send_Order_Failure_to_QDX__c, Send_Order_Update_to_QDX__c, Render_and_Distribute_Data_Message__c, Send_Initial_Order_to_QDX__c, Send_Order_To_Lab__c FROM OrderItem WHERE Id IN: recordIds]) {
                
                /*
                //Check for null and values is changed
                if(odrItem.Send_Order_Failure_to_QDX__c && odrItem.Results_Disposition_Current__c == 'Failure') {
                    
                    String pName = 'OSM_OrderFailureToQDX';
                    
                    //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, null, '');
                    responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                    
                    String actionType = 'Failure';
                    //OSM_Messaging.MakeIntegrationTask(responseString, String.valueOf(odrItem.Id), actionType, '');
                    
                }
                if(odrItem.Send_Order_Update_to_QDX__c) {
                    
                    String pName = 'OSM_OrderUpdateToQDX';
                    
                    //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, null, '');
                    responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                    
                    String actionType = 'Update';
                    //OSM_Messaging.MakeIntegrationTask(responseString, String.valueOf(odrItem.Id), actionType, '');
                    
                }
                if(odrItem.Send_Order_Cancel_to_QDX__c && odrItem.Results_Disposition_Current__c != 'Failure') {
                    
                    String pName = 'OSM_CancelledOrderToQDX';
                    
                    //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, null, '');
                    responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                    
                    String actionType = 'Cancelled';
                    //OSM_Messaging.MakeIntegrationTask(responseString, String.valueOf(odrItem.Id), actionType, ''); commented temporarly for future testing
                    
                }
                */
                System.debug('@**#*# sendInitial: ' + odrItem.Send_Initial_Order_to_QDX__c);
                System.debug('@**#*# renderAndDist: ' + odrItem.Render_and_Distribute_Data_Message__c);
                System.debug('@**#*# sendToLab: ' + odrItem.Send_Order_To_Lab__c);
                System.debug('@**#*# sendToLab: ' + odrItem.OSM_Billing_Status__c);		
                if(odrItem.Send_Initial_Order_to_QDX__c && (odrItem.OSM_Billing_Status__c == 'Pre-Billing Investigating' || odrItem.OSM_Billing_Status__c == 'Billing Initiated'
                    || odrItem.OSM_Billing_Status__c == 'Bill Voided' || odrItem.OSM_Billing_Status__c == 'Billing Not Required')) {
                    
                    System.debug('@**#*# inside if odrItem.OSM_Billing_Status__c debug: ' + odrItem.OSM_Billing_Status__c);
                    
                    //String to hold page name
                    String pName = 'OSM_SendOrderToQDX';
                    
                    if(odrItem.OSM_Billing_Status__c == 'Pre-Billing Investigating'){
                        System.debug('Pre-Billing Investigating');
                        //List<Case> caseList = [SELECT Id, Status, OSM_Primary_Order_Line_Item__c, OSM_BI_Sent_Date__c, RecordType.Name FROM Case WHERE OSM_Primary_Order_Line_Item__c =: odrItem[0].Id AND RecordType.Name = 'Pre-Billing' AND Status = 'Open'];
                        if(preBillOpencaseMap.get(odrItem.Id) != null && !preBillOpencaseMap.get(odrItem.Id).isEmpty()){
                            
                            odrItem.OSM_Pre_Billing_Sent_Date_Current__c = system.now();
                            
                            //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, preBillOpencaseMap.get(odrItem.Id)[0].Id, odrItem.OSM_Billing_Status__c);
                            responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                            caseListToUpdate.add(new Case(Id = preBillOpencaseMap.get(odrItem.Id)[0].Id, OSM_BI_Sent_Date__c = system.now()));
                        }
                    }
                    
                    
                    if(odrItem.OSM_Billing_Status__c == 'Billing Initiated'){
                        System.debug('Billing Initiated');
                        //List<Case> caseList = [SELECT Id, Status, OSM_Primary_Order_Line_Item__c, OSM_Billing_Request_Sent_Date__c, RecordType.Name FROM Case WHERE OSM_Primary_Order_Line_Item__c =: odrItem[0].Id AND RecordType.Name = 'Billing' AND Status = 'Open'];
                        
                        if(billOpencaseMap.get(odrItem.Id) != null && !billOpencaseMap.get(odrItem.Id).isEmpty()){
                            
                            odrItem.OSM_Is_Billing__c = true;
                            
                            //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, preBillOpencaseMap.get(odrItem.Id)[0].Id, odrItem.OSM_Billing_Status__c);
                            responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                            caseListToUpdate.add(new Case(Id = billOpencaseMap.get(odrItem.Id)[0].Id, OSM_Billing_Request_Sent_Date__c = system.now()));
                        }
                    }
                    
                    
                    if(odrItem.OSM_Billing_Status__c == 'Bill Voided'){
                        System.debug('Bill Voided');
                        //List<Case> caseList = [SELECT Id, Status, OSM_Primary_Order_Line_Item__c, OSM_Billing_Request_Sent_Date__c, RecordType.Name FROM Case WHERE OSM_Primary_Order_Line_Item__c =: odrItem[0].Id AND RecordType.Name = 'Billing' AND Status = 'Closed' Order By LastModifiedDate DESC];
                        if(billClosedcaseMap.get(odrItem.Id) != null && !billClosedcaseMap.get(odrItem.Id).isEmpty()){
                            
                            odrItem.OSM_Is_Billing__c = false;
                            odrItem.OSM_Billing_Void_Sent_Date_Current__c = system.now();
                            
                            //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, preBillOpencaseMap.get(odrItem.Id)[0].Id, odrItem.OSM_Billing_Status__c);
                            responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                            caseListToUpdate.add(new Case(Id = billClosedcaseMap.get(odrItem.Id)[0].Id, OSM_Billing_Void_Sent_Date__c = system.today()));
                        } 
                    }
                    
                    if(odrItem.OSM_Billing_Status__c == 'Billing Not Required'){
                        System.debug('Billing Not Required');
                        if(odrItem.Results_Disposition_Current__c == 'Failure'){
                            pName = 'OSM_OrderFailureToQDX';
                        } else {
                            pName = 'OSM_CancelledOrderToQDX';
                        }
                        
                        //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, null, '');
                        responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                        
                    }
                        
                    
                } 
                //else {
                    //String pName = 'OSM_OrderUpdateToQDX';
                    //responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                //}
                 
                if(odrItem.Render_and_Distribute_Data_Message__c && odrItem.OSM_Lab_and_Report_Status__c == 'Distributing') {
                    
                    responseString = OSM_Messaging.sendRenderAndDistributeDataMessage(odrItem.OrderId, odrItem.Id);
                    
                }
                if(odrItem.Send_Order_To_Lab__c && odrItem.OSM_State__c == 'Closed') {
                            
                    String pName = 'OSM_OrderDataMessageLab';
                    //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, null, '');
                    responseString = OSM_Messaging.MakeOrderXML(String.valueOf(odrItem.Id), pName, '');
                    
                }
                
                //odrItem.Send_Order_Cancel_to_QDX__c = false;
                //odrItem.Send_Order_Failure_to_QDX__c = false;
                //odrItem.Send_Order_Update_to_QDX__c = false;
                
                if(odrItem.Render_and_Distribute_Data_Message__c || odrItem.Send_Initial_Order_to_QDX__c || odrItem.Send_Order_To_Lab__c){
                    odrItem.Render_and_Distribute_Data_Message__c = false;
                    odrItem.Send_Initial_Order_to_QDX__c = false;
                    odrItem.Send_Order_To_Lab__c = false;
                    oiListToUpdate.add(odrItem);
                }
            }
            
            if(!oiListToUpdate.isEmpty()){
                try{
                    update oiListToUpdate;
                } catch(Exception e1){ if(e1.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oiListToUpdate; } catch(Exception e2){ if(e2.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oiListToUpdate; } catch(Exception e3){ if(e3.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oiListToUpdate; } catch(Exception e4){ if(e4.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update oiListToUpdate; } catch(Exception e5){ if(e5.getMessage().contains('UNABLE_TO_LOCK_ROW')){ update oiListToUpdate; }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if(!caseListToUpdate.isEmpty()){
                update caseListToUpdate;
            }
        
        } else if(objectName == 'OSM_Work_Order__c'){
            List<OSM_Work_Order__c> woListToUpdate = new List<OSM_Work_Order__c>();
            
            for (OSM_Work_Order__c woLoop : [Select Id, Send_Order_To_Lab__c From OSM_Work_Order__c Where Id IN: recordIds]) {
                
                if(woLoop.Send_Order_To_Lab__c) {
                    String pName = 'OSM_OrderDataMessage';
                    
                    //DataFactoryFutureHelper.callXMLGeneration(String.valueOf(odrItem.Id), '', pName, null, '');
                    responseString = OSM_Messaging.MakeOrderXML(woLoop.Id, pName, '');
                    
                    if(woLoop.Send_Order_To_Lab__c){
                        woLoop.Send_Order_To_Lab__c = false;
                        woListToUpdate.add(woLoop);
                    }
                }
                
            }
            
            if(!woListToUpdate.isEmpty()){
                try{
                    update woListToUpdate;
                } catch(Exception e1){ if(e1.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update woListToUpdate; } catch(Exception e2){ if(e2.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update woListToUpdate; } catch(Exception e3){ if(e3.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update woListToUpdate; } catch(Exception e4){ if(e4.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update woListToUpdate; } catch(Exception e5){ if(e5.getMessage().contains('UNABLE_TO_LOCK_ROW')){ update woListToUpdate; }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
    }
     

   
}