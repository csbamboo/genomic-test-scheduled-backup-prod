/**
 *  Purpose         :   Webservice class for DataFactroyController
 *
 *  Created By      :   
 * 
 *  Created Date    :   04/21/2015
 * 
 *  Revision Logs   :   V_1.0 - Created
 * 
 **/
global class DataFactoryWebServiceHelper {

    //This method is for - Send Initial Order to QDX button
    @future(callout = true)
    webservice static void getOrderXmlForSendIntialOrderToQDX(String orderId, String pName, String contentValue) {
        
        //Call webservice method
        OSM_Messaging.MakeOrderXML(orderId, pName, contentValue);
    }
    
    //This method is for - Send Initial Order to QDX button
    @future(callout = true)
    webservice static void getOrderXmlForSendOrderToLab(String orderId, String pName, String contentValue) {
        
        //Call webservice method
        OSM_Messaging.MakeOrderXML(orderId, pName, contentValue);
    }
    
    //This method is for - Send Order Failure to QDX button
    @future(callout = true)
    webservice static void getOrderXmlForSendOrderFailureToQDX(String orderItemId, String pName, String contentValue) {
        
        //Call webservice method
        String responseString = OSM_Messaging.MakeOrderXML(orderItemId, pName, contentValue);
        
        //String to hold action type
        String actionType = 'Failure';
        
        //Call web service method
        OSM_Messaging.MakeIntegrationTask(responseString, orderItemId, actionType, '');
    }
    
    //This method is for - Send Order Update to QDX button
    @future(callout = true)
    webservice static void getOrderXmlForSendOrderUpdateToQDX(String orderItemId, String pName, String contentValue) {
        
        //Call webservice method
        String responseString = OSM_Messaging.MakeOrderXML(orderItemId, pName, contentValue);
        
        //String to hold action type
        String actionType = 'Update';
        
        //Call web service method
        OSM_Messaging.MakeIntegrationTask(responseString, orderItemId, actionType, '');
    }
    
    //This method is for - Send Order Cancel to QDX button
    @future(callout = true)
    webservice static void getOrderXmlForSendOrderCancelToQDX(String orderItemId, String pName, String contentValue) {
        
        //Call webservice method
        String responseString = OSM_Messaging.MakeOrderXML(orderItemId, pName, contentValue);
        
        //String to hold action type
        String actionType = 'Cancelled';
        
        //Call web service method
        OSM_Messaging.MakeIntegrationTask(responseString, orderItemId, actionType, '');
    }
    
    //This method is for - Send Order Cancel to QDX button
    @future(callout = true)
    webservice static void getOrderXmlForProcessInbound(String msgId, String task) {
        
        OSM_OrderTriggerHandler.globalIsFuture = false;
        //Call webservice method
        String responseString = OSM_Messaging.processInbound(msgId, task);
    }
}