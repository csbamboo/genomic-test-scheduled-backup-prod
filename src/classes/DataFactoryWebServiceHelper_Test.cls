/*
 *  @author         Raus Kenneth Ablaza
 *  @description    Test class for DataFactoryWebServiceHelper class
 *  @date           22 July 2015
 *
 */

@isTest
private class DataFactoryWebServiceHelper_Test {

    private static testMethod void testDFWSH() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;

        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        Id stdPriceBookId = Test.getStandardPricebookId();

        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;

        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        for(Integer i = 0; i < 2; i++){
            prodList.add(OSM_DataFactory.createProduct('IBC', true));
        }
        insert prodList;
        
        for(Integer i = 0; i < 2; i++){
            pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, stdPriceBookId, 3));
        }
        insert pbeList;

        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
                Pricebook2Id = stdPriceBookId, 
                EffectiveDate = System.Today(), 
                OSM_Triage_Outcome__c = 'New',
                Status = 'New'
            ));        
        }
        insert ordList;
        
        for(Integer i = 0; i < 2; i++){
            oliList.add(new OrderItem(
                OrderId = ordList[i].Id , 
                PricebookEntryId = pbeList[i].Id , 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        insert oliList;
        
        //ESBInboundMessage__c esbi = [select id from ESBInboundMessage__c limit 1];
        String xml = '';
        
        xml += '<?xml version="1.0" encoding="UTF-8"?>';
        xml += '<n1:SpecimenGroup xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:n1="http://schemas.genomichealth.com/Internal/Crm/v1/SpecimenGroup.xsd" ackRequired="true" activityId="String" correlationId="String" destination="FAX" expires="2001-12-17T09:30:47Z" messageInstanceId="String" nackRequired="true" origin="SHIPPING" timestamp="2001-12-17T09:30:47Z" xsi:schemaLocation="http://schemas.genomichealth.com/Internal/Crm/v1/DistributionEventMessage.xsd DistributionEventMessage.xsd">';
        xml += '<DistributionEvent>';
        xml += '<LabTaskId>Lab Task</LabTaskId>';
        xml += '<DistributedDate>2001-12-17T09:30:47Z</DistributedDate>';
        xml += '<ReportDate>2001-12-17T09:30:47Z</ReportDate>';
        xml += '<ReportModifiedDate>2001-12-17T09:30:47Z</ReportModifiedDate>';
        xml += '<Status>Success</Status>';
        xml += '<DistributionFailureReason>Fail</DistributionFailureReason>';
        xml += '</DistributionEvent>';
        xml += '</n1:SpecimenGroup>';
        
        Blob beforeblob = Blob.valueOf(xml);
        String paramvalue = EncodingUtil.base64Encode(beforeblob);
        
        ESBInboundMessage__c objESBInboundMessage = new ESBInboundMessage__c();
        
        objESBInboundMessage.IsActive__c = true;
        objESBInboundMessage.Process_Inbound__c = true;
        objESBInboundMessage.ESBMessageInstanceId__c = 'msgId';
        objESBInboundMessage.ESBMessage__c = paramvalue;
        Insert objESBInboundMessage;
        
        Test.startTest();
        
        DataFactoryWebServiceHelper.getOrderXmlForSendIntialOrderToQDX(String.valueOf(ordList[0].Id), 'sample name', 'sample value');
        DataFactoryWebServiceHelper.getOrderXmlForSendOrderToLab(String.valueOf(ordList[1].Id), 'sample name', 'sample value');
        DataFactoryWebServiceHelper.getOrderXmlForSendOrderFailureToQDX(String.valueOf(oliList[0].Id), 'sample name', 'sample value');
        DataFactoryWebServiceHelper.getOrderXmlForSendOrderUpdateToQDX(String.valueOf(oliList[1].Id), 'sample name', 'sample value');
        DataFactoryWebServiceHelper.getOrderXmlForSendOrderCancelToQDX(String.valueOf(oliList[1].Id), 'sample name', 'sample value');
        DataFactoryWebServiceHelper.getOrderXmlForProcessInbound('msgId', 'sample task');
        
        Test.stopTest();
    }

}