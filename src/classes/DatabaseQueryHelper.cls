/**
 *  Description     :   Genric Class to hold static variables and Quries.
 *
 *  Created By      :   
 *
 *  Created Date    :   06/18/2015 
 *  
 *  Version         :   V_1.0
 *
 *  Revision Log    :   V_1.0 - Created
**/
public with sharing class DatabaseQueryHelper{
    
    //List of order to be returned 
    public static List<Order> ORDERS = new List<Order>();
     
    //List of Order Item to be returned
    public static List<OrderItem> ORDER_ITEMS = new List<OrderItem>();
    
    //List of OrderItem to be returned
    public static List<OrderItem> ORDER_ITEMS_WOOLI = new List<OrderItem>();
        
    /**
     *  @description    :   This method is used to get the order data based on Ids.
     *
     *  @return         :   List<Order>
     *
     *  @args           :   Set<Id> orderIds
    **/
    public static List<Order> getOrderByOrderIds(Set<Id> orderIds) {
                
        //Checking for list size & Query to fetch order records based on set of Ids
        if(ORDERS  != null && ORDERS.size() == 0)
            ORDERS = [SELECT Id, status FROM Order WHERE Id IN :orderIds];
        
        //return order
        return ORDERS;
    }
    
    /**
     *  @description    :   This method is used to get the order line item date based on work orde ids.
     *
     *  @retrn          :   List<OrderItem>
     *
     *  @args           :   Set<Id> workOrderIds
    **/
    public static List<OrderItem> getOrderItemByWorkOrderIds(Set<Id> workOrderIds) {
        
         //Checking for list size & Query to fetch order records based on set of WO Ids
        if(ORDER_ITEMS  != null && ORDER_ITEMS.size() == 0)
            ORDER_ITEMS = [SELECT Id  FROM OrderItem WHERE Id IN 
                (SELECT OSM_Order_Product__c FROM OSM_Work_Order_Line_Item__c WHERE OSM_Work_Order__c IN :workOrderIds)];
        
        //return order
        return ORDER_ITEMS;
    }
    
    /**
     *  @description    :   This method is used to get the order line item date based on ids.
     *
     *  @retrn          :   List<OrderItem>
     *
     *  @args           :   Set<Id> workOrderIds
    **/
    public static List<OrderItem> getOrderItemByIds(Set<Id> oliIds) {
        
         //Checking for list size & Query to fetch order records based on set of Ids
        if(ORDER_ITEMS_WOOLI  != null && ORDER_ITEMS_WOOLI.size() == 0)
            ORDER_ITEMS_WOOLI = [SELECT Id, OSM_State__c, OSM_Lab_and_Report_Status__c, OSM_Data_Entry_Status__c FROM OrderItem WHERE Id IN : oliIds];
        
        //return order
        return ORDER_ITEMS_WOOLI;
    }
}