public with sharing class ExternalSignatureController {

  private Apttus__APTS_Agreement__c agreement;
  public String agreementId;
  
  public ExternalSignatureController (ApexPages.StandardController stdController) { 
    
    this.agreement = (Apttus__APTS_Agreement__c)stdController.getRecord();
    agreementId = ApexPages.currentPage().getParameters().get('id');  
  }

  public PageReference finalize(){
    
    agreement.Apttus__Status_Category__c = 'In Signatures';
    agreement.Apttus__Status__c = 'External Signatures';
    agreement.GHI_CLM_External_signature__c = true;
    update agreement;
    //"/apex/Apttus__SendEmail?id="&Id & "&action=Send_To_Other_Party_For_Signatures"
   PageReference pgRef = new PageReference(Page.Apttus__SendEmail.getURL() + '?id=' + agreementId + '&action=Send_To_Other_Party_For_Signatures');
    //PageReference result = new ApexPages.StandardController(agreement).view();
    
    return pgRef;
    
  }


}