/*
  @author: Amanpreet Singh Sidhu 
  @date: 01 July 2015
  @description: Test Class for ExternalSignatureController apex class
  @history: 01 July 2015 - Created (Amanpreet S Sidhu)

*/
@isTest
public class ExternalSignatureController_Test 
{
  
    static testMethod void apptusAgreement() 
    {
        Apttus__APTS_Agreement__c aggrement = new Apttus__APTS_Agreement__c();
        insert aggrement;
        
        PageReference pageRef = Page.External_Signature;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(aggrement);
        ApexPages.currentPage().getParameters().put('Id',aggrement.id);
        ExternalSignatureController cont = new ExternalSignatureController(sc);
        
        Test.startTest();
        cont.finalize();
        
        Test.stopTest();
     
    }
    
}