/**
 * An apex page controller that exposes the site forgot password functionality
 */
@IsTest public with sharing class ForgotPasswordControllerTest {
  	 @IsTest(SeeAllData=true) public static void testForgotPasswordController() {
    	// Instantiate a new controller with all parameters in the page
    	PageReference pageRef = Page.ForgotPassword;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
    	ForgotPasswordController controller = new ForgotPasswordController();
    	controller.username = 'testuser2@cloudsherpas.com.dev2';     	
    
    	System.assertEquals(controller.forgotPassword(),null);
    	Test.stopTest();
    }
}