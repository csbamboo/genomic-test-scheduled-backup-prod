@isTest
public class GHI_CLM_GetSpecimenRateList_Test {
    
    public static testmethod void  testmethod_one(){
        //Account myAccount = new Account(Name = 'Test Account', )
        //Contact testContact = new Contact(Name = 'Test', Email = 'test@apttus.com', )
        GHI_CLM_Specimen_Rate_List__c specimenRateList = new GHI_CLM_Specimen_Rate_List__c(
        														CurrencyIsoCode = 'USD',
        														GHI_CLM_Block_Rate__c = 67, 
        														GHI_CLM_Country_Region__c = 'US',
        														GHI_CLM_Slide_Rate__c = 80,
        														GHI_CLM_TAT__c = 4);
        insert specimenRateList;
        
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(
        														Name = 'Test Agreement',
            													//Apttus__Account__c = myAccount.Id,
        														Apttus__Contract_Start_Date__c =  Date.newInstance(2015,6,24), 
        														Apttus__Contract_End_Date__c =  Date.newInstance(2015,8,24),
        														CurrencyIsoCode = 'USD',
        														GHI_CLM_Agreement_SubType__c = 'DMG/ACO/IPA', 
        														GHI_CLM_Specimen_TAT__c = null,
        														GHI_CLM_Slide_Rate__c  = null,
        														GHI_CLM_Specimen_Block_Rate__c = null,
        														GHI_CLM_Country_Region__c = 'US');
        insert agreement;
        
        agreement.Apttus__Contract_End_Date__c =  Date.newInstance(2015,8,28);
        
        update agreement;
    }

}