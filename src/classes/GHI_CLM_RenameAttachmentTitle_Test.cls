@isTest
//(SeeAllData=true)

public class GHI_CLM_RenameAttachmentTitle_Test {

    public static testmethod void testmethod_one(){

        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(
                                    Name = 'Test Agreement',
                                    Apttus__Contract_Start_Date__c =  Date.newInstance(2015,6,24), 
                                    Apttus__Contract_End_Date__c =  Date.newInstance(2015,8,24),
                                    CurrencyIsoCode = 'USD',
                                    GHI_CLM_Country_Region__c = 'US');
        insert agreement;

        Apttus__APTS_Template__c myTemplate = new Apttus__APTS_Template__c(
            Name = 'Contract Summary');
        insert myTemplate;
        
        String attBody = 'Test body';
        //Attachment Records
        //List<Attachment> listAttachments = new List<Attachment>();
    	Attachment attachment1 = new Attachment(Name='Test 1',Body=Blob.valueOf(attBody), ParentId=agreement.Id);
        //listAttachments.add(attachment1);
        
        insert attachment1;

        Apttus__MergeEvent__c myMergeEvent = new Apttus__MergeEvent__c(
            Name = 'Test',
            Apttus__Action__c = 'Regenerate',
            Apttus__DocumentFormat__c = 'DOC',
            Apttus__ResultDocId__c = attachment1.Id,
            Apttus__TemplateId__c = myTemplate.Id);

        insert myMergeEvent;
    }
}