public with sharing class GHI_MergeEvent_Handler {
	public GHI_MergeEvent_Handler() {
		
	}


	//BEFORE INSERT LOGIC
	public void OnBeforeInsert(Apttus__MergeEvent__c[] newObjects){
		GHI_MergeEvent_Helper helper = new GHI_MergeEvent_Helper();
      	helper.renameAttachmentTitle(newObjects);
    }

    //AFTER INSERT LOGIC
    //public void OnAfterInsert(Apttus__MergeEvent__c[] newObjects, Map<Id, Apttus__MergeEvent__c> objectNewMap){
        //APTS_Agreement_Helper helper = new APTS_Agreement_Helper();
        //helper.createStageDataForIntegration(newObjects, ObjectNewMap);
   //	}

	//BEFORE UPDATE LOGIC
    //public void OnBeforeUpdate(Apttus__MergeEvent__c[] oldObjects, 
    //							Apttus__MergeEvent__c[] updatedObjects, Map<Id, Apttus__MergeEvent__c> 
    //							oldObjectMap, Map<Id, Apttus__MergeEvent__c> newObjectMap){
  
   // }

	//AFTER UPDATE LOGIC
    //public void OnAfterUpdate(Apttus__MergeEvent__c[] oldObjects, Apttus__MergeEvent__c[] updatedObjects, 
    //							Map<Id, Apttus__MergeEvent__c> objectMap, Map<Id, Apttus__MergeEvent__c> objectNewMap){ 
        
    //}

	//BEFORE DELETE LOGIC
    //public void OnBeforeDelete(Apttus__MergeEvent__c[] objectsToDelete, 
    //							Map<Id, Apttus__MergeEvent__c> objectMap){  
    //}

    //AFTER DELETE LOGIC
    //public void OnAfterDelete(Apttus__MergeEvent__c[] deletedObjects, 
    //							Map<Id, Apttus__MergeEvent__c> objectMap){ 
    //}

    //AFTER UNDELETE LOGIC
    //public void OnUndelete(Apttus__MergeEvent__c[] restoredObjects){
    //}

}