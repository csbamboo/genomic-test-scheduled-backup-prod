public with sharing class GHI_MergeEvent_Helper {

	public GHI_MergeEvent_Helper() {
		
	}

	public void renameAttachmentTitle(Apttus__MergeEvent__c[] newObjects){

		//List of Attachment records to update name to 'Contract Summary.docx'
		List<Attachment> renamedAttachments = new List<Attachment>();
		//ID Sets
		Set<Id> attachmentIds = new Set<Id>();
		Set<Id> templateIds = new Set<Id>();

		for(Apttus__MergeEvent__c mergeEvent : newObjects){
			//DEBUG Apttus__MergeEvent__c records
			System.debug('************** mergeEvent : newObjects: '+mergeEvent);
			//Add Template IDs to Set<ID>
			templateIds.add(mergeEvent.Apttus__TemplateId__c);
		}

		
		System.debug('*********** templateIds: '+templateIds);

		//Check Template ID exists
		if(templateIds.size()>0){
			//Map of Template Records
			Map<Id,Apttus__APTS_Template__c> mapTemplates = new Map<Id,Apttus__APTS_Template__c>([
									SELECT Id, Name FROM Apttus__APTS_Template__c WHERE Id IN: templateIds]);

			//Check that Map has at least 1 record
			if(mapTemplates.size()>0){
				for(Apttus__MergeEvent__c mergeEvent : newObjects){
					if(mapTemplates.get(mergeEvent.Apttus__TemplateId__c).Name == 'Contract Summary'){
						//Add Attachment IDs to Set<ID>
						attachmentIds.add(mergeEvent.Apttus__ResultDocId__c);
					}
				}
				//Check Attachment ID contains at least 1 record
				if(attachmentIds.size()>0){

					System.debug('*********** attachmentIds: '+attachmentIds);

					//Select the Attachments and rename
					for(Attachment a : [SELECT Id, Name FROM Attachment WHERE Id IN: attachmentIds]){
						a.Name = 'Contract Summary.docx';
						renamedAttachments.add(a);
					}

					//List to be updated
					System.debug('***************** renamedAttachments: '+renamedAttachments);

					try{
						update renamedAttachments;
					} catch(DmlException e){
						System.debug('The following error has occurred: '+e.getMessage());
					}
				}
			}
		}
	}
}