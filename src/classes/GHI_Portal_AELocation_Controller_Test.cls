@isTest
public class GHI_Portal_AELocation_Controller_Test{
    
     @testsetup 
     static void setup() {
     
      User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
    User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
            
        Account testAcct = GHI_Portal_TestUtilities.createAccountHCO(); 
        insert testAcct; 
        
        Customer_Affiliation__c testCA = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(
            testAcct.Id, portalUser.ContactId, 'Primary'); 
        insert testCA;  
        
        Contact testContact = GHI_Portal_TestUtilities.createContactHCP(testAcct.Id); 
        insert testContact; 
        
        Customer_Affiliation__c testCA2 = GHI_Portal_TestUtilities.createCNTtoCNTAffiliation(
            portalUser.ContactId, testContact.Id); 
        insert testCA2; 
        
        Customer_Affiliation__c testCA3 = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(
            testAcct.Id, testContact.Id, 'Primary'); 
        insert testCA3; 
        
        GHI_Portal_Settings__c customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
        insert customSetting;   

     }    
     }
     
     @isTest
     static void controllerTest(){          
        test.startTest();
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
        
        System.runAs(portalUser) {
            
        Id rId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();

        Account addEditLocation = new Account(
            CurrencyIsoCode = 'USD',
            Name = 'aelocation',
            RecordTypeId = rId,
            BillingStreet = '123 Main St',
            BillingCity = 'Jersey City',
            BillingState = 'NJ',
            BillingPostalCode = '99999',
            BillingCountry = 'US',
            Phone = '555-555-5555',
            OSM_Specialty__c = 'Urology'
        );
        
        insert addEditLocation; 
        
        Customer_Affiliation__c deleteLoc = [SELECT Id FROM Customer_Affiliation__c WHERE 
                OSM_Contact_1__c = :portalUser.ContactId AND OSM_Account_1__c != null LIMIT 1];

            PageReference pageRef = Page.GHI_Portal_AddEditLocation;
            test.setCurrentPage(pageRef);
            System.debug('#addEdit ' + addEditLocation);
            System.debug('#deteleLocation ' + deleteLoc.Id);            
            System.currentPageReference().getParameters().put('locId', addEditLocation.Id);
     
            
            GHI_Portal_AddEditLocation_Controller controller = new GHI_Portal_AddEditLocation_Controller();
                
                        
            Customer_Affiliation__c contactAffiliation = [SELECT Id FROM Customer_Affiliation__c WHERE 
                                                         OSM_Contact_1__c = :portalUser.ContactId AND 
                                                         OSM_Contact_2__c != null LIMIT 1]; 
            System.debug('#contactAff ' + contactAffiliation);
    
            Customer_Affiliation__c locationAffiliation = [SELECT Id FROM Customer_Affiliation__c WHERE 
                                                          OSM_Contact_1__c = :portalUser.ContactId AND 
                                                          OSM_Account_1__c != null LIMIT 1];
           System.debug('#locAff ' + locationAffiliation);
           
        OSM_Country_and_State__c countryState = new OSM_Country_and_State__c(
            OSM_State_Name__c = 'Zere',
            OSM_Country_Name__c = 'US');
            
            insert countryState;
           
            controller.goToAddressBook();
            
            controller.getDeletedLocationsList();
            controller.getContactList();
            controller.getPhysicianPageURL();
            
            
            controller.dlocId = deleteLoc.Id;
            controller.unDeleteLocation();
            
            controller.checkState();
            controller.getAllCountries();
            controller.getAllState();
            controller.saveLocation();
            controller.addLocationFromSelectedDuplicate();
            
            controller.street = 'test street';
            controller.city = 'test city';
            controller.zip = '123';
            controller.addLocation();
            controller.getSpecimenPageURL();
            
            
             
        }
        
        test.stopTest(); 

     }
     
      @isTest
     static void dupTest(){
        test.startTest();
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
        
        System.runAs(portalUser) {
            
        GHI_Portal_AddEditLocation_Controller controller = new GHI_Portal_AddEditLocation_Controller();
            
           Account accToReplicate = new Account(
                Name = 'test name',
                Phone = '555-555-5555',
                BillingState = 'NJ',
                BillingStreet = 'test street',
                BillingCity = 'test city',
                BillingPostalCode = '123',
                BillingCountry = 'Dubai',
                Fax = '111-222-3333'
                );
            insert accToReplicate;
            
            /*CRMfusionDBR101__Scenario__c dupeScenario = new CRMfusionDBR101__Scenario__c(
                Name = 'dupe',
                CRMfusionDBR101__Scenario_Type__c = 'Account');
                
            insert dupeScenario;
            
            CRMfusionDBR101__Duplicate_Warning__c dupeWarning = new CRMfusionDBR101__Duplicate_Warning__c(
                CRMfusionDBR101__Scenario_Type__c = 'Account',
                CRMfusionDBR101__Scenario__c = dupeScenario.Id);
                
            insert dupeWarning;*/
            
            controller.myLocation.Name = 'test name';
            controller.myLocation.Phone = '555-555-5555';
            controller.myLocation.BillingState = 'NJ';
            controller.myLocation.BillingCity = 'NJ';
            controller.myLocation.BillingStreet = '21 John Honmer';
            controller.street = 'test street';
            controller.city = 'test city';
            controller.zip = '123';
            controller.fax = '111-222-3333';
            controller.state = 'NJ';
            controller.country = 'USA';
            controller.myLocation.BillingPostalCode = '2123';
            controller.myLocation.Fax = '31121';
            controller.orderWorkflow = 'Domestic';
            try{  
            controller.addLocation();
            controller.selectedDupLoc = 'create_new';
            controller.addLocationFromSelectedDuplicate();  
            } catch(Exception ex){
               System.debug('@dupblockertest');
            }
        }
        
        test.stopTest(); 
     }
     
      @isTest
     static void saveTest(){
        test.startTest();
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
        
        System.runAs(portalUser) {
            
        GHI_Portal_AddEditLocation_Controller controller = new GHI_Portal_AddEditLocation_Controller();

            controller.myLocation.Name = 'UniqueLocation';
            controller.myLocation.Phone = '111-222-3333';
            controller.myLocation.BillingState = 'OI';
            controller.myLocation.BillingStreet = 'universe';
            controller.myLocation.BillingCity = 'the city is';
            controller.myLocation.BillingPostalCode = '4102';
            controller.myLocation.BillingCountry = 'Dubai';
            try{  
            controller.saveLocation();
            } catch(Exception ex){
                Logger.debugException(ex);
            }
        }
        
        test.stopTest(); 
     }
     
      @isTest
     static void saveEmptyTest(){
        test.startTest();
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
        
        System.runAs(portalUser) {
            
        GHI_Portal_AddEditLocation_Controller controller = new GHI_Portal_AddEditLocation_Controller();
            
            controller.myLocation.Name = '';
            controller.myLocation.Phone = '';
            controller.myLocation.BillingState = '';
            controller.myLocation.BillingStreet = '';
            controller.myLocation.BillingCity = '';
            controller.myLocation.BillingPostalCode = '';
            controller.myLocation.BillingCountry = null;
            try{  
            controller.saveLocation();
            } catch(Exception ex){
                Logger.debugException(ex);
            }
        }
        
        test.stopTest(); 
     }     
     
      @isTest
     static void noValueTest(){
        test.startTest();
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
        
        System.runAs(portalUser) {
            
        GHI_Portal_AddEditLocation_Controller controller = new GHI_Portal_AddEditLocation_Controller();
            
            controller.myLocation.Name = '';
            controller.myLocation.Phone = '';
            controller.state = '';
            controller.myLocation.BillingStreet = '';
            controller.myLocation.BillingCity = '';
            controller.myLocation.BillingPostalCode = '';
            controller.country = null;
            try{  
            controller.addLocation();
            } catch(Exception ex){
                Logger.debugException(ex);
            }
        }
        
        test.stopTest(); 
     }
     
    @isTest
    static void pageReferenceTest(){
        GHI_Portal_Settings__c customSetting = new GHI_Portal_Settings__c();
        customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
        insert customSetting;
        
        Test.startTest();
        
        PageReference pr; 
        
        GHI_Portal_AddEditLocation_Controller controller = new GHI_Portal_AddEditLocation_Controller();
        
        pr = controller.goToAddressBook();
        System.assertEquals(pr.getURL(), Page.GHI_Portal_AddressBook.getURL()); 
        
        pr = controller.saveLocation();
        
        Test.stopTest();
    }
         @isTest
     static void controllerCatchTest(){          
        test.startTest();
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
        
        System.runAs(portalUser) {
            
        Id rId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();

        Account addEditLocation = new Account(
            CurrencyIsoCode = 'USD',
            Name = 'aelocation',
            RecordTypeId = rId,
            BillingStreet = '123 Main St',
            BillingCity = 'Jersey City',
            BillingState = 'NJ',
            BillingPostalCode = '99999',
            BillingCountry = 'US',
            Phone = '555-555-5555',
            OSM_Specialty__c = 'Urology'
        );
        
        insert addEditLocation; 
        
        Customer_Affiliation__c deleteLoc = [SELECT Id FROM Customer_Affiliation__c WHERE 
                OSM_Contact_1__c = :portalUser.ContactId AND OSM_Account_1__c != null LIMIT 1];

            PageReference pageRef = Page.GHI_Portal_AddEditLocation;
            test.setCurrentPage(pageRef);
            System.debug('#addEdit ' + addEditLocation);
            System.debug('#deteleLocation ' + deleteLoc.Id);            
            System.currentPageReference().getParameters().put('locId', addEditLocation.Id);
     
            
            GHI_Portal_AddEditLocation_Controller controller = new GHI_Portal_AddEditLocation_Controller();
            try{    
            controller.dlocId = null;
            controller.unDeleteLocation();
            } catch(Exception e){
                System.debug('##CatchTesting ' + e);
            }
            
             
        }
        
        test.stopTest(); 

     }
}