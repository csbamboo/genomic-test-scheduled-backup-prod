@isTest
public class GHI_Portal_AccountInfo_Cntrllr_Test{
    
     @testsetup 
     static void setup() {
     	User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
        
        GHI_Portal_Static_Content__c   staticContent = new GHI_Portal_Static_Content__c();
        
        staticContent.GHI_Portal_Static_Content__c = 'The quick brown fox jumps over the lazy dog';
        staticContent.GHI_Portal_Locale__c  = 'en_US';
        staticContent.GHI_Portal_Insurance_Company__c  = null;
        staticContent.GHI_Portal_Content_Type__c = 'Account Information';

        insert staticContent;
     }    
     
     @isTest
     static void controllerTest(){ 	        
 	    test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    
     	System.runAs(portalUser) {
     		
	     	GHI_Portal_AccountInfo_Controller controller = new GHI_Portal_AccountInfo_Controller();
     	}

     	test.stopTest(); 

     }
}