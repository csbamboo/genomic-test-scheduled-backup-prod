/*------------------------------------------------------------------------
    Author:         Stephen James Laylo
    Company:        Cloud Sherpas
    Description:    A controller for Account Info page
                  
    Test Class:     GHI_Portal_AccountInfo_Controller_Test
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    9/16/2015       Cliff Fajardo		     Created this controller
*/
public with sharing class GHI_Portal_AccountInfo_Controller {
	public list<GHI_Portal_Static_Content__c> accountInfoItems {get;set;}
	public String selectedLocale {get;set;}
	public String contentText {get;set;}
	public String localeLink {get;set;}
	
	
	public GHI_Portal_AccountInfo_Controller(){
		this.accountInfoItems = new List<GHI_Portal_Static_Content__c>();
		
		//Query Delegate Attestation content from Portal Static Content object 
        accountInfoItems = new List<GHI_Portal_Static_Content__c >([SELECT Id, Name, GHI_Portal_Static_Content__c,GHI_Portal_Locale__c,
                                                                        GHI_Portal_Content_Type__c FROM 
                                                                        GHI_Portal_Static_Content__c WHERE 
                                                                        GHI_Portal_Content_Type__c = 'Account Information']);
                                                                     
        this.selectedLocale = ApexPages.currentPage().getParameters().get('lang');
        
        for(GHI_Portal_Static_Content__c content : accountInfoItems){
        	
        	if(selectedLocale == '' || selectedLocale == null){
        		if(content.GHI_Portal_Locale__c == 'en_US'){
        			contentText = content.GHI_Portal_Static_Content__c;
        		}
        	}else if(selectedLocale == content.GHI_Portal_Locale__c){
        		contentText = content.GHI_Portal_Static_Content__c;
        	}
        }
        
        System.debug('!@#displayContent ' + contentText);
	}
	
	/*-----------------------------------------------------------------------
    * Description   Change to a different language 
    ----------------------------------------------------------------------- */
    public PageReference changeLang() {
        PageReference pr = Page.GHI_Portal_Account_Info;
        pr.getParameters().put('lang', localeLink); 
        pr.setRedirect(true);
        return pr;
    }
}