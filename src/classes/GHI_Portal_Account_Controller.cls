/*----------------------------------------------------------------------------------------------
    Author:        Gypt Minierva
    Company:       Cloud Sherpas
    Description:   A controller class created for GHI portal My account visualforce page
                  
    Test Class:
    History
    <Date>      <Authors Name>          <Brief Description of Change>
    01/29/2015  Gypt Minierva           Created
    02/04/2015  Stephen James Laylo     Created a method for fetching
                                        related Accounts for user to 
                                        display it in My Locations
    02/04/2015  Stephen James Laylo     Created a method for the Reset 
                                        Password Button located at
                                        My Account Page.
    02/05/2015  Stephen James Laylo     Updated the constructor. Added a 
                                        query for fetching the Security 
                                        Questions and Answers from the 
                                        current user.
    03/03/2015  Stephen James Laylo     Updated the query for fetching delegates
    06/03/2015  Stephen James Laylo     Code Clean Up & Optimization
---------------------------------------------------------------------------------------------*/

public class GHI_Portal_Account_Controller {
    //variables for user information.
    public User user { get; set; }
    public Id userId { get; set; }
    public String notification { get; set; }
    public Boolean passwordReset { get; set; }

    //variables for setting new password.
    //public String oldPassword { get; set; }
    //public String newPassword { get; set; }
    //public String verifyNewPassword { get; set; }
    //public String charAllowed { get; set; }
    //public String passwordMsg  { get; set; }
    //public Boolean passwordMatch {get;set;}
    
    //public String debug { get; set; }

    public GHI_Portal_Account_Controller() {
        /*------------------------------------------------------------
         Author:        Gypt Minierva
         Company:       Cloud Sherpas
         Description:   Constructor method. Initialises all necessary
         *              components etc.   
         History
         01/29/2015     Gypt Minierva Created
         02/05/2015     Stephen James Laylo - Added a query for fetching
                        the Security Questions and Answers from the
                        current user.
        ------------------------------------------------------------*/
        //this.charAllowed = Label.GHI_Portal_AllowedChar;
        this.notification = '';
        this.userId = UserInfo.getUserId();
        this.user = GHI_Portal_Utilities.getCurrentUser();
        this.passwordReset = false;

        if (this.user.ReceivesAdminInfoEmails = true) {
            notification = Label.GHI_Portal_Yes;
        }
        else {
            notification = Label.GHI_Portal_No;
        }
    }
    
    /*------------------------------------------------------------
       Author:        Gypt Minierva
       Company:       Cloud Sherpas
       Description:   Changes the password of logged in user.
          
       History
       01/29/2015   Gypt Minierva Created
    ------------------------------------------------------------*/    
    /**public PageReference changePassword() {

        if (newPassword.length() > 20 || verifyNewPassword.length() > 20 || oldpassword.length() > 20) {
            this.passwordMsg = Label.GHI_Portal_PasswordMsg;
            passwordMatch = true;

        } else if (newPassword.contains(' ')) {
            this.passwordMsg = Label.GHI_Portal_PasswordMsg;
            passwordMatch = true;

        } else {

            return Site.changePassword(newPassword, verifyNewPassword, oldpassword);

        }
        return null;    
    }**/
    
    /**public PageReference changeSecurityQuestion() {
        //clarification
        return null;
    }**/
    
    public List<Account> getMyLocations() {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       FEB-04-2015
        * Description   Used for querying the Accounts related to the current
        *               user.
        * Returns       List of Accounts
        ----------------------------------------------------------------------- */ 
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();

        Id contId = this.user.ContactId;
        contacts = new List<Contact>([SELECT Account.Id 
                                      FROM Contact 
                                      WHERE Id = :contId]);

        if (contacts.size() > 0) {
            Set<Id> accIds = new Set<Id>();

            for (Contact cont : contacts) {
                accIds.add(cont.Account.Id);
            }

            accounts = new List<Account>([SELECT Id, 
                                                 Name, 
                                                 Phone, 
                                                 BillingStreet, 
                                                 BillingCity, 
                                                 BillingState, 
                                                 BillingPostalCode, 
                                                 BillingCountry 
                                          FROM Account 
                                          WHERE Id IN :accIds
                                          AND RecordType.Name = 'HCO']);
        }

        return accounts;
    }

    public List<Contact> getMyPhysicians() {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * Description   Used for querying the Physicians related to the current
        *               user's accounts.
        * Returns       List of Contacts
        * History
        * - 02/06/2015  Stephen James Laylo     Created
        * - 03/03/2015  Stephen James Laylo     Updated the query for fetching delegates
        ----------------------------------------------------------------------- */ 

        List<OSM_Delegate__c> myDelegates = new List<OSM_Delegate__c>();
        List<Contact> myPhysicians = new List<Contact>();
        List<Account> myLocations = getMyLocations();
        List<Contact> contacts = new List<Contact>();
        Id contId = this.user.ContactId;
        
        contacts = new List<Contact>([SELECT Id, 
                                             Name, 
                                             RecordType.Name 
                                      FROM Contact 
                                      WHERE Id = :contId]);
        
        if (contacts.size() > 0) {
            contId = contacts.get(0).Id;
            myDelegates = new List<OSM_Delegate__c>([SELECT Id, 
                                                            Name, 
                                                            OSM_HCP__c, 
                                                            OSM_Contact__c 
                                                     FROM OSM_Delegate__c 
                                                     WHERE (OSM_HCP__c = :contacts.get(0).Id OR OSM_Contact__c = :contacts.get(0).Id)
                                                     AND OSM_HCO__c IN :myLocations]);
        }


        if (myDelegates.size() > 0) {
            Set<Id> conIds = new Set<Id>();

            for (OSM_Delegate__c delegate : myDelegates) {
                if (delegate.OSM_HCP__c != null && delegate.OSM_HCP__c != contId) {
                    conIds.add(delegate.OSM_HCP__c);
                }
            }

            myPhysicians = new List<Contact>([SELECT Id, Name 
                                              FROM Contact
                                              WHERE Id IN :conIds]);

        }

        return myPhysicians;
    }

    public List<OSM_Delegate__c> getMyDelegates() {
        /*---------------------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * Description   Used for querying the Delegates related to the current
        *               user's Physicians.
        * Returns       List of Delegates
        * History
        * - 02/06/2015  Stephen James Laylo     Created
        * - 03/03/2015  Stephen James Laylo     Updated the query for fetching delegates
        * - 03/24/2015  Stephen James Laylo     Added GHI_Portal_Authorization_Date__c
                                                to the query of fetching My Delegates
        ---------------------------------------------------------------------------------- */
        List<OSM_Delegate__c> myDelegates = new List<OSM_Delegate__c>();
        List<Contact> contacts = new List<Contact>();
        List<Account> myLocations = getMyLocations();

        Id contId = this.user.ContactId;
        contacts = new List<Contact>([SELECT Id, 
                                             Name, 
                                             RecordType.Name 
                                      FROM Contact 
                                      WHERE Id = :contId]);

        if (contacts.size() > 0) {
            myDelegates = new List<OSM_Delegate__c>([SELECT Id, 
                                                            Name, 
                                                            GHI_Portal_Authorization_Date__c, 
                                                            OSM_HCP__c, 
                                                            OSM_Contact__c, 
                                                            OSM_Contact__r.Email 
                                                     FROM OSM_Delegate__c 
                                                     WHERE OSM_HCP__c = :contacts.get(0).Id
                                                     AND OSM_HCO__c IN :myLocations]);
        }

        return myDelegates;
    }

    public void logoutAndResetPassword() {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       FEB-04-2015
        * Description   It will redirect you to logout and Step 1 of Reset Password
        ----------------------------------------------------------------------- */ 
        List<User> userlist = new List<User>([SELECT Id, GHI_Portal_Okta_User_ID__c 
        										FROM User 
        										WHERE id = :UserInfo.getUserId() Limit 1]);
		String oktaID = '';
		if(userlist.size() > 0){
			for(User u : userlist){
				oktaID = u.GHI_Portal_Okta_User_ID__c;
			}
		}
		
        
        System.debug('Okta ID: ' + oktaID); 
        //PageReference pr = null;
        if(oktaID != null && oktaID != ''){
        	GHI_Portal_OktaAPI.resetUserPassword(oktaID);
        	this.passwordReset = true;
        	//pr = new PageReference('{!$Site.Prefix}/secur/logout.jsp');
        	
        }
        //else{
        //	pr = new PageReference('javascript:;');
        //}
        
        //return pr;
    }


    //public PageReference logoutAndProceedToSecurityQuestionPage() {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       FEB-05-2015
        * Description   It will redirect you to logout and to the page for
                        updating Security Questions and Answers
        ----------------------------------------------------------------------- */ 
/**
        PageReference pr = null;
        //pr.getParameters().put('startUrl', Page.GHI_Portal_ChangeSecurityQuestion.getUrl());
        pr.getParameters().put('un', user.Username);
        pr.setRedirect(true);

        return pr;
    }**/
    
}