/*-----------------------------------------------------------------------------------------------------
    Author:        Sairah Hadjinoor
    Company:       Cloud Sherpas
    Description:   A controller class created for Add Edit Location Page
                  
    Test Class:
    History:
    <Date>          <Authors Name>          <Brief Description of Change>
    03/18/2015      Sairah Hadjinoor        Created
    03/18/2015      Stephen James Laylo     Added a functionality for adding a location
                                            through Web To Lead using Web Service    
    06/10/2015      Katrina Guarina         Added logic to retrieve Contacts in a given location  
    06/16/2015      Stephen James Laylo     Added a functionality of adding Location with Dupeblocker
    08/17/2015      Katrina Guarina         Added logic to redirect to either Physician or Specimen tab
    08/31/2015		Paul Wittmeyer			Modified to make international state not required
-----------------------------------------------------------------------------------------------*/
public class GHI_Portal_AddEditLocation_Controller {
    
    /* DECLARATIONS */
    public User user { get; set; }
    public String dlocId { get; set; }
    public GHI_Portal_Settings__c orgDefaults { get; set; }
    public String contactAffiliationRecTypeId { get; set; }
    public String accountAffiliationRecTypeId { get; set; }
    public String hcoRecTypeId { get; set; }
    public String debug { get; set; }
    
    public String company { get; set; }
    public String phone { get; set; }
    public String fax { get; set; }
    public String address { get; set; }
    public String street { get; set; }
    public String city { get; set; }
    public String state { get; set; }
    public String zip { get; set; }
    public String country { get; set; }
    public String commUser { get; set; }
    public String leadSrc { get; set; }
    public Boolean isLocation { get; set; }
    public String displayLocation { get; set; }
    public Customer_Affiliation__c locationList { get; set; }
    public Account myLocation { get; set; }
    public Boolean disableInput { get; set; }
    public Boolean isEdit { get; set; }
    public String orderId {get;set;}
    public String backToOrderFlag {get;set;} 
    
    public Boolean hasCompany { get; set; }
    public Boolean hasPhone { get; set; }
    public Boolean hasFax { get; set; }
    public Boolean hasAddress { get; set; }
    public Boolean hasStreet { get; set; }
    public Boolean hasCity { get; set; }   
    public Boolean hasState { get; set; }
    public Boolean hasZip { get; set; }
    public Boolean hasCountry { get; set; }
    public String selectedCountry { get; set; }
    public String selectedState { get; set; }
    public Boolean hasNoState { get; set; }
    public List<Account> duplicatedLocations { get; set; }
    public String selectedDupLoc { get; set; }
    
    private Account tempMyLocation;
    private String defaultPortalUserCountry;
    public String orderWorkflow { get; set; }
    
    public GHI_Portal_AddEditLocation_Controller() {
        //get org defaults where we store all hard coded ids for web to lead fields             
        this.orgDefaults = GHI_Portal_Settings__c.getOrgDefaults();
        this.orderWorkflow = GHI_Portal_Utilities.getUserOrderWorkflow();
        this.disableInput = false;
        this.isEdit = false;
        //Holds list of duplicated locations returned by Dupeblocker 
        this.duplicatedLocations = new List<Account>();
        this.hasNoState = true;
        List<Customer_Affiliation__c> locationList = new List<Customer_Affiliation__c>();
        List<Customer_Affiliation__c> contactList = new List<Customer_Affiliation__c>();
        this.displayLocation = ApexPages.currentPage().getParameters().get('locId');
        this.orderId = ApexPages.currentPage().getParameters().get('id');
        this.backToOrderFlag = ApexPages.currentPage().getParameters().get('backToOrder');
        this.myLocation = new Account();
        this.tempMyLocation = new Account();
        this.defaultPortalUserCountry = GHI_Portal_Utilities.getPortalUserCountry();
        
        //Get records based on the parameter
        if (this.displayLocation != '' && this.displayLocation != null) {
            this.disableInput = true;
            this.isEdit = true;
            this.myLocation = [SELECT Id,
                                      Name,
                                      Phone,
                                      Fax,
                                      BillingStreet,
                                      BillingCity,
                                      BillingState,
                                      BillingPostalCode,
                                      BillingCountry,
                                      RecordTypeId, 
                                      GHI_Portal_Location__c
                               FROM Account 
                               WHERE Id = :this.displayLocation
                               LIMIT 1];
            System.debug('#myLoc ' + myLocation);
        }
        
        //Set record type Ids       
        this.accountAffiliationRecTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get(orgDefaults.GHI_Portal_CNT_ACCT_Recordtype__c).getRecordTypeId();
        System.debug('#AccrecTypeId ' + accountAffiliationRecTypeId);
        this.contactAffiliationRecTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get(orgDefaults.GHI_Portal_CNT_CNT_Recordtype__c).getRecordTypeId();
        System.debug('#CntrecTypeId ' + contactAffiliationRecTypeId);
        this.hcoRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        
        this.user = GHI_Portal_Utilities.getCurrentUser();
        
        if (this.myLocation.BillingCountry != null && this.myLocation.BillingCountry != '') {
            this.selectedCountry = this.myLocation.BillingCountry;
        } else {
            this.selectedCountry = this.defaultPortalUserCountry;
        }
        if (myLocation.BillingState != null && myLocation.BillingState != '') {
            this.selectedState = this.myLocation.BillingState;
        }
        
        //specify the values for the location
        this.leadSrc = 'Portal';
        this.isLocation = true;
        this.commUser = UserInfo.getUserId();
        this.debug = '';
        
        //set booleans for the page validations
        this.hasCompany = false;
        this.hasPhone = false;
        this.hasFax = false;
        this.hasStreet = false;
        this.hasCity = false;
        this.hasState = false;
        this.hasZip = false;
        this.hasCountry = false;
        checkState();
    }
    
    public PageReference goToAddressBook() {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       JAN-20-2015
        * Description   Redirecting to Address Book Page
        ----------------------------------------------------------------------- */
        PageReference pr = Page.GHI_Portal_AddressBook;
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference getPhysicianPageURL() {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       JAN-20-2015
        * Description   Redirecting to Physician Page
        ----------------------------------------------------------------------- */
        PageReference pr = Page.GHI_Portal_OIP_Physician;
        pr.getParameters().put('id', orderId); 
        pr.setRedirect(true);

        return pr;
    }
    
    //ADDED: KGuarina 08/17/15
    public PageReference getSpecimenPageURL() {
        /*-----------------------------------------------------------------------
        * Author        Katrina Guarina 
        * Company       Cloud Sherpas
        * History       AUG-17-2015
        * Description   Redirecting to Specimen/Options Page
        ----------------------------------------------------------------------- */
        PageReference pr = Page.GHI_Portal_OIP_Specimen;
        pr.getParameters().put('id', orderId); 
        pr.setRedirect(true);

        return pr;
    }
    
    public List<Customer_Affiliation__c> getDeletedLocationsList() {    
    /*-----------------------------------------------------------------------
    * @author         Gypt Minierva
    * @date           03/18/2015
    * @description    Generates a list of locations that is deactivated in
                      address book.
    -----------------------------------------------------------------------*/ 
       List<Customer_Affiliation__c> locationsRec = new List<Customer_Affiliation__c>();
       
       //Get all affiliated locations where Display in Portal flag is set to false 
       locationsRec = new List<Customer_Affiliation__c>([SELECT Id,
                                                                GHI_Portal_Display_in_Portal__c,
                                                                GHI_Portal_Account_1_Phone__c, 
                                                                IntlSFA_Phone__c, 
                                                                OSM_Account_1__c, 
                                                                GHI_Portal_Account_1_Address__c, 
                                                                OSM_Account_1_Name__c,
                                                                OSM_Contact_1__c 
                                                         FROM Customer_Affiliation__c 
                                                         WHERE GHI_Portal_Display_in_Portal__c = false 
                                                         AND OSM_Contact_1__c =: user.ContactId 
                                                         AND RecordTypeId= :accountAffiliationRecTypeId]);
       System.debug('@@locationsRec '+locationsRec);
       
       return locationsRec;
    }
    
    public List<Customer_Affiliation__c> getContactList() { 
        /*--------------------------------------------------------------------------
        * @author         Katrina Guarina
        * @date           06/10/2015
        * @description    Retrieves list of Contacts affiliated to a given Location
        ---------------------------------------------------------------------------*/
        System.debug('displayLocation: ' + displayLocation); 
        System.debug('accountAffiliationRecTypeId: ' + accountAffiliationRecTypeId); 
        Set<Id> affContactSet = new Set<Id>(); 
        
        //ADDED: KGuarina 10/1/15
        //Logic to only display Contacts that are also affiliated to the Logged in User
        //---- START---- 
        List<Customer_Affiliation__c> cntToCntList = new List<Customer_Affiliation__c>();
        cntToCntList = new List<Customer_Affiliation__c>([SELECT OSM_Contact_2__c
                                                         FROM Customer_Affiliation__c 
                                                         WHERE GHI_Portal_Display_in_Portal__c = true 
                                                         AND RecordTypeId=: contactAffiliationRecTypeId 
                                                         AND OSM_Contact_1__c =: user.ContactId 
                                                         AND OSM_Contact_2__c != null]);
        for(Customer_Affiliation__c currRec : cntToCntList) { 
            affContactSet.add(currRec.OSM_Contact_2__c);
        }
        //Also add the logged in user 
        affContactSet.add(user.ContactId); 
        //----END---- 
        
        List<Customer_Affiliation__c> contactList = new List<Customer_Affiliation__c>();
        contactList = new List<Customer_Affiliation__c>([SELECT Id,
                                                                GHI_Portal_Display_in_Portal__c,
                                                                OSM_Contact_1__r.Name,
                                                                OSM_Contact_1__r.Email, 
                                                                OSM_Account_1__r.Name
                                                         FROM Customer_Affiliation__c 
                                                         WHERE GHI_Portal_Display_in_Portal__c = true 
                                                         AND OSM_Account_1__c =: displayLocation 
                                                         AND RecordTypeId=: accountAffiliationRecTypeId 
                                                         AND OSM_Contact_1__c != null 
                                                         AND OSM_Account_1__c != null
                                                         AND OSM_Contact_1__c IN :affContactSet]);  //Added: KGuarina 10/1/15 
        return contactList;
    }
    
    public void unDeleteLocation() {
        /*-----------------------------------------------------------------------
        * Author        Sairah Hadjinoor
        * Company       Cloud Sherpas
        * History       MAR-18-2015
        * Description   Undeleting all deleted locations on portal
                        FYI - Locations are not hard deleted in salesforce org
                        rather the code is just updating a Boolean value
                        that flags if the location must be displayed in Portal or not
        ----------------------------------------------------------------------- */
        //get the customer affiliation to undelete
        //this will only be called once so no issues on the query
        if (dlocId != null && dlocId != '') {
            Customer_Affiliation__c locationToUndelete = [SELECT Id,
                                                                 GHI_Portal_Display_in_Portal__c, 
                                                                 IntlSFA_Phone__c, 
                                                                 OSM_Account_1__c, 
                                                                 GHI_Portal_Account_1_Address__c, 
                                                                 OSM_Account_1_Name__c,
                                                                 OSM_Contact_1__c 
                                                          FROM Customer_Affiliation__c 
                                                          WHERE Id =: dlocId];
            if (locationToUndelete != null) { 
                locationToUndelete.GHI_Portal_Display_in_Portal__c = true; 
                try{
                    update locationToUndelete;
                }catch(Exception e){
                    logger.debugException(e);
                } 
            }
        }
    }
    
    public PageReference addLocation() {
        /*---------------------------------------------------------------------------
        * @author         Stephen James Laylo
        * @date           06/11/2015
        * @description    Used in adding Location with DupeBlocker Support
        ---------------------------------------------------------------------------*/
        
        //Default all error flags to False 
        this.hasCompany = false;
        this.hasPhone = false;
        this.hasFax = false;
        this.hasStreet = false;
        this.hasCity = false;
        this.hasState = false;
        this.hasZip = false;
        this.hasCountry = false;
        
        Boolean dupeFound = false; 
        
        //Check if State has value 
        if (this.myLocation.BillingState != null && this.myLocation.BillingState != '') {
            this.state = myLocation.BillingState;
        } else if (selectedState != null && selectedState != '') {
            this.state = selectedState;
        } else {
            this.state = null;
        }
        
        //Check if Country has value 
        if (this.myLocation.BillingCountry != null && this.myLocation.BillingCountry != '') {
            this.country = this.myLocation.BillingCountry;
        } else if (selectedCountry != null && selectedCountry != '') {
            this.country = selectedCountry;
        } else {
            this.country = null;
        }
            System.debug('#myLocationStreet ' + street);
            System.debug('#myLocationCity ' + city);
            System.debug('#myLocationPostalCode ' + zip);
            
        this.myLocation.BillingStreet = this.street;
        this.myLocation.BillingCity = this.city;
        this.myLocation.BillingPostalCode = this.zip;
        
        this.myLocation.GHI_Portal_Location__c = true; 
        
            System.debug('#myLocationName ' + myLocation.Name);
            System.debug('#myLocationPhone ' + myLocation.Phone);

            System.debug('#myLocationState ' + state);
            System.debug('#myLocationCountry ' + country);
            
            System.debug('#myLocationFax ' + myLocation.Fax); 
            System.debug('this.orderWorkflow ' + this.orderWorkflow); 
            
        //Check if all required fields are populated     
        if ((this.myLocation.Name != null && this.myLocation.Name != '') && (this.myLocation.Phone != null && this.myLocation.Phone != '') && 
            																(this.myLocation.BillingStreet != null && this.myLocation.BillingStreet != '') && 
            																(this.myLocation.BillingCity != null && this.myLocation.BillingCity != '') && 
        																	(((this.state != null && this.state != '') && this.orderWorkflow == 'Domestic') || this.orderWorkflow != 'Domestic') &&
        																	(this.myLocation.BillingPostalCode != null && this.myLocation.BillingPostalCode != '') && 
            																(this.country != null && this.country != '') && 
            																((this.myLocation.Fax != null && this.myLocation.Fax != '' && this.orderWorkflow == 'Domestic') || 
        																	(this.orderWorkflow != 'Domestic'))) {

            this.debug = '';
    
            Savepoint sp = Database.setSavepoint();
            Boolean willCreateAffiliations = true;
            Boolean willCreateCntToAccount = true;
            Id acctIdToBeAssoc = null;
            
            try {
                system.debug('#myLocation ' + myLocation.Id);
                if (this.myLocation.Id == null) {
                    //Set State and Country from picklist 
                    this.myLocation.BillingCountry = this.country;
                    this.myLocation.BillingState = this.state;
                    this.myLocation.RecordTypeId = this.hcoRecTypeId;
                    
                    //Try to insert new Location 
                    try{
                        insert this.myLocation;
                    }catch(Exception e){
                        logger.debugException(e);
                    }
                    
                    this.tempMyLocation = this.myLocation;
                    acctIdToBeAssoc = this.myLocation.Id;
                    
                    Set<Id> acctIds = new Set<Id>();
                    //Check if Dupeblocker created any new Warning records 
                    List<CRMfusionDBR101__Duplicate_Warning__c> dupWarnings = new List<CRMfusionDBR101__Duplicate_Warning__c>([SELECT Id, 
                                                                                                                                      Name, 
                                                                                                                                      (SELECT Id, 
                                                                                                                                              Name, 
                                                                                                                                              CRMfusionDBR101__Account__c 
                                                                                                                                       FROM CRMfusionDBR101__Potential_Duplicates__r 
                                                                                                                                       ORDER BY Name DESC) 
                                                                                                                               FROM CRMfusionDBR101__Duplicate_Warning__c 
                                                                                                                               WHERE CRMfusionDBR101__Scenario_Type__c = 'Account'
                                                                                                                               ORDER BY LastModifiedDate DESC LIMIT 1]);
                    
                    //If query returned results, it means duplicate records were found                                                                                                            
                    if (!dupWarnings.isEmpty()) {
                        //Get Account Ids of potential duplicates 
                        for (CRMfusionDBR101__Potential_Duplicate__c potentialDup : dupWarnings.get(0).CRMfusionDBR101__Potential_Duplicates__r) {
                            acctIds.add(potentialDup.CRMfusionDBR101__Account__c);
                        }
                        
                        //Delete inserted location 
                        delete this.myLocation;
                        
                        //Get instance of potential duplicate Locations for display 
                        this.duplicatedLocations = new List<Account>([SELECT Id,
                                                                             Name,
                                                                             Phone,
                                                                             Fax,
                                                                             BillingStreet,
                                                                             BillingCity,
                                                                             BillingState,
                                                                             BillingPostalCode,
                                                                             BillingCountry,
                                                                             //BillingAddress,
                                                                             RecordTypeId,
                                                                             CreatedDate,
                                                                             CreatedBy.Name, 
                                                                             GHI_Portal_Location__c
                                                                      FROM Account 
                                                                      WHERE Id IN :acctIds
                                                                      ORDER BY CreatedDate DESC]);
                
                        if (!this.duplicatedLocations.isEmpty()) {
                            willCreateAffiliations = false;
                        }
                        
                        //Cleanup Warning records 
                        delete dupWarnings; 
                    }
                }
            } catch (Exception e) {
                Logger.debugException(e);
                //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
                Database.rollback(sp);
                
                System.debug('@@Exception caught');
                willCreateAffiliations = false;
            }
            
            //If no exceptions were caught, proceed with Customer Affiliation creation 
            if (willCreateAffiliations) {
                List<Customer_Affiliation__c> pendingCA = new List<Customer_Affiliation__c>();
                
                if (acctIdToBeAssoc != null) {
                    Customer_Affiliation__c caCntToAcct = new Customer_Affiliation__c();
                    
                    //Retrieve existing Contact to Account affiliation 
                    List<Customer_Affiliation__c> oldCaCntToAcct = new List<Customer_Affiliation__c>([SELECT OSM_Contact_1__c, 
                                                                                                             OSM_Account_1__c, 
                                                                                                             RecordTypeId
                                                                                                      FROM Customer_Affiliation__c
                                                                                                      WHERE OSM_Contact_1__c = :this.user.ContactId
                                                                                                      AND OSM_Account_1__c = :acctIdToBeAssoc
                                                                                                      AND RecordTypeId = :this.accountAffiliationRecTypeId]);
                    //Create new Contact to Account affiliation 
                    caCntToAcct.OSM_Contact_1__c = this.user.ContactId;
                    caCntToAcct.OSM_Account_1__c = acctIdToBeAssoc;
                    caCntToAcct.RecordTypeId = this.accountAffiliationRecTypeId;
                    pendingCA.add(caCntToAcct);

                    System.debug('@@pendingCA.size() : ' + pendingCA.size()); 
                    if (!pendingCA.isEmpty()) {
                        if (oldCaCntToAcct.isEmpty()) {
                            try{
                                insert pendingCA;
                            }catch(Exception e){
                                logger.debugException(e);
                            }
                        }
                        
                        Pagereference pageRef; 
                        //If 'Add new Location' from Physician tab, redirect back to Physician tab 
                        if(backToOrderFlag == '1') { 
                            pageRef = getPhysicianPageURL();
                        }
                        //If 'Add new Location' from Specimen tab, redirect back to Specimen tab 
                        else if(backToOrderFlag == '2') {   //Added: KGuarina 08/17/15 
                            pageRef = getSpecimenPageURL();  
                        }
                        //Else, redirect to Address Book page 
                        else {
                            pageRef = Page.GHI_Portal_AddressBook;
                            if (dupeFound) {
                                pageRef.getParameters().put('dup', '1');
                            }
                        }
                        
                        //saveContactLocation();
                        
                        return pageRef;
                    }
                }
            }
        } else {
            //update boolean for validations
            if (this.myLocation.Name == null || this.myLocation.Name == '') {
                this.hasCompany = true;
            }
            if (this.myLocation.Phone == null || this.myLocation.Phone == '') {
                this.hasPhone = true;
            }
            if (this.orderWorkflow == 'Domestic' && (this.myLocation.Fax == null || this.myLocation.Fax == '')) {
                this.hasFax = true;
            }
            if (this.myLocation.BillingStreet == null || this.myLocation.BillingStreet == '') {
                this.hasStreet = true;
            }
            if (this.myLocation.BillingCity == null || this.myLocation.BillingCity == '') {
                this.hasCity = true;
            }
            if (this.state == null || this.state == '') {
                this.hasState = true;
            }
            if (this.myLocation.BillingPostalCode == null || this.myLocation.BillingPostalCode == '') {
                this.hasZip = true;
            }
            if (this.country == null || this.country == '') {
                this.hasCountry = true;
            }
        }
        
        return null;
    }
    
    public PageReference addLocationFromSelectedDuplicate() {
        /*---------------------------------------------------------------------------
        * @author         Stephen James Laylo
        * @date           06/11/2015
        * @description    Used in adding Location selected from list of Duplicated locations 
        ---------------------------------------------------------------------------*/
        //Check user selection from list of duplicates 
        if (this.selectedDupLoc != null && this.selectedDupLoc != '') {
            //If user selected to create a new location 
            if (this.selectedDupLoc == 'create_new') {
                //creates new Location even there's an existing Location
                System.debug('!@#tempMyLoc ' + tempMyLocation);
                this.tempMyLocation.Id = null;
                this.tempMyLocation.GHI_Portal_Location__c = true; 
                try{
                    insert this.tempMyLocation;
                }catch(Exception e){
                        logger.debugException(e);
                    }
                
                this.selectedDupLoc = this.tempMyLocation.Id;
            }
            
            Customer_Affiliation__c caCntToAcct = new Customer_Affiliation__c();
            
            //Retrieve existing Contact to Account affiliation 
            List<Customer_Affiliation__c> oldCaCntToAcct = new List<Customer_Affiliation__c>([SELECT OSM_Contact_1__c, 
                                                                                                     OSM_Account_1__c, 
                                                                                                     RecordTypeId
                                                                                              FROM Customer_Affiliation__c
                                                                                              WHERE OSM_Contact_1__c = :this.user.ContactId
                                                                                              AND OSM_Account_1__c = :this.selectedDupLoc
                                                                                              AND RecordTypeId = :this.accountAffiliationRecTypeId]);
                                                                                              
            caCntToAcct.OSM_Contact_1__c = this.user.ContactId;
            caCntToAcct.OSM_Account_1__c = this.selectedDupLoc;
            caCntToAcct.RecordTypeId = this.accountAffiliationRecTypeId;

            if (oldCaCntToAcct.isEmpty()) {
                try{
                    insert caCntToAcct;
                }catch(Exception e){
                        logger.debugException(e);
                    }
            }
            
            //Redirect user to Addressbook page 
            Pagereference pageRef = Page.GHI_Portal_AddressBook;
            pageRef.setRedirect(true);
            
            return pageRef;
        }
        
        return null;
    }
    
    public PageReference saveLocation() {
        /*-----------------------------------------------------------------------
        * @author         Gypt Minierva
        * @date           03/26/2015
        * @description    Updates the location.
        * <History>       <Author>                  <Update>
          31.MAR.2015     Stephen James Laylo       Fixed the display of the error messages
        -----------------------------------------------------------------------*/     
        //Default error flags to false 
        this.hasCompany = false;
        this.hasPhone = false;
        this.hasFax = false;
        this.hasStreet = false;
        this.hasCity = false;
        this.hasState = false;
        this.hasZip = false;
        this.hasCountry = false;
        
        //Set selected Country and State in picklist 
        myLocation.BillingCountry = selectedCountry;
        if (selectedState!=null && selectedState!='') {
            myLocation.BillingState = selectedState;
        }
        
        //If all required fields are filled out, update Location record 
        if (myLocation != null && myLocation.Name!=null && myLocation.Name != '' && myLocation.Phone != null && myLocation.Phone != '' && myLocation.BillingStreet != '' &&
            myLocation.BillingStreet != null && myLocation.BillingCity != null && myLocation.BillingCity != '' && myLocation.BillingState != null && myLocation.BillingState != '' &&
            myLocation.BillingPostalCode != null && myLocation.BillingPostalCode != '' && myLocation.BillingCountry != null && myLocation.BillingCountry != '') {
            try{
                update myLocation;
            }catch(Exception e){
                logger.debugException(e);
            }
        //Else, set error flag for missing information 
        } else {
            if (myLocation.Name == null || myLocation.Name == '') {
                this.hasCompany = true;
            }
            if (myLocation.Phone == null || myLocation.Phone== '') {
                this.hasPhone = true;
            }
            if (this.orderWorkflow == 'Domestic' && (myLocation.Fax == null || myLocation.Fax == '')) {
                this.hasFax = true;
            }
            if (myLocation.BillingStreet == null || myLocation.BillingStreet == '') {
                this.hasStreet = true;
            }
            if (myLocation.BillingCity == null || myLocation.BillingCity == '') {
                this.hasCity = true;
            }
            if (myLocation.BillingState == null || myLocation.BillingState == '') {
                this.hasState = true;
            }
            if (myLocation.BillingPostalCode == null || myLocation.BillingPostalCode == '') {
                this.hasZip = true;
            }
            if (myLocation.BillingCountry == null || myLocation.BillingCountry == '') {
                this.hasCountry = true;
            }
            
            return null;
        }
        
        //Redirect user to address book page 
        return goToAddressBook();
    }
    
    public List<SelectOption> getAllCountries() {
    /*-----------------------------------------------------------------------
    * @author         Gypt Minierva
    * @date           03/18/2015
    * @description    Populates Country picklist from Country and State object 
    -----------------------------------------------------------------------*/ 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Select a country'));
        //Get list of Countries defined in object 
        List<AggregateResult> groupedResults = [SELECT OSM_Country_Name__c
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Country_Name__c != ''
                                                AND OSM_Country_Name__c != null
                                                GROUP BY OSM_Country_Name__c];
        //Add retrieved Countries as SelectList Options 
        for (AggregateResult country: groupedResults) {
            options.add(new SelectOption((String) country.get('OSM_Country_Name__c'), (String) country.get('OSM_Country_Name__c')));                                                       
        }

        return options;
    }
    
    public List<SelectOption> getAllState() {
    /*-----------------------------------------------------------------------
    * @author         Gypt Minierva
    * @date           03/18/2015
    * @description    Populates State picklist from Country and State object 
    -----------------------------------------------------------------------*/         
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Select a state'));
        System.debug('@@selectedCountry ' + selectedCountry);
        
        //Check if Country has value 
        if (selectedCountry!='' && selectedCountry!=null) {
            //Get list of States under selected Country defined in object
            List<OSM_Country_and_State__c> stateOptions = new List<OSM_Country_and_State__c>([SELECT OSM_Country_Name__c, 
                                                                                                     OSM_State_Name__c
                                                                                              FROM OSM_Country_and_State__c 
                                                                                              WHERE OSM_Country_Name__c = :selectedCountry
                                                                                              AND OSM_State_Name__c != ''
                                                                                              AND OSM_State_Name__c != null
                                                                                              ORDER BY OSM_State_Name__c]);
            System.debug('@@stateOptions '+stateOptions);
            //Add retrieved States as SelectList options 
            for (OSM_Country_and_State__c country: stateOptions) {
                if (country.OSM_State_Name__c!='' && country.OSM_State_Name__c!=null) {                                           
                    options.add(new SelectOption(country.OSM_State_Name__c, country.OSM_State_Name__c)); 
                }                                                   
            } 
        }
        System.debug('@@options '+options);
        return options;
    }
    
    public void checkState() {
    /*-----------------------------------------------------------------------
    * @author         Gypt Minierva
    * @date           03/18/2015
    * @description    Sets flag to either display State field as picklist or text field  
    -----------------------------------------------------------------------*/ 
        //If no State is retrieved, size is 1 because of 'Select a State' option 
        if (getAllState().size() == 1) {
            this.hasNoState = false;
        } else {
            this.hasNoState = true;
        }
    }
    public pagereference Access(){ 
         /*-----------------------------------------------------------------------
        * Author        Paul Wittmeyer
        * Company       Cloud Sherpas
        * History       October 12, 2015
        * Description   Redirects to error page if user does not have access
        ----------------------------------------------------------------------- */
		boolean Access = false;
		if(this.displayLocation != null && this.displayLocation != ''){
			for(Customer_Affiliation__C ca : [SELECT OSM_Account_1__c, OSM_Account_2__c from Customer_Affiliation__c where OSM_Contact_1__c = :this.user.ContactId ]){
				if(ca.OSM_Account_1__c == this.displayLocation || ca.OSM_Account_2__c == this.displayLocation){
					Access = true;
				}
			}
			if(Access){
				return null;
			}else{
	        	PageReference errorPage = new pagereference('/apex/GHI_Portal_Error'); 
	        	return errorPage;
	        }
		}else{
			return null;
		}
    }  
}