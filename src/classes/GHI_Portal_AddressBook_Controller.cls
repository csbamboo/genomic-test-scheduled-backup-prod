/*-----------------------------------------------------------------------------------------------------------------
    Author:        Sairah Hadjinoor
    Company:       Cloud Sherpas
    Description:   A controller class created for Address Book Page
                  
    Test Class:
    History:
    <Date>          <Authors Name>          <Brief Description of Change>
    03/18/2015      Sairah Hadjinoor        Created
    03/24/2015      Sairah Hadjinoor        Updated to include wrapper class for displaying primary location
                                            of logged in user to the My contacts section
    05/25/2015      Stephen James Laylo     Added a criteria in searching Address Book
                                            Code Clean Up                 
-------------------------------------------------------------------------------------------------------------------*/
public class GHI_Portal_AddressBook_Controller {
    
    public String searchOrder { get; set; }
    public String contactAffiliationRecTypeId { get; set; }
    public String accountAffiliationRecTypeId { get; set; }
    public User user { get; set; }
    public Id locId { get; set; } 
    public Id conId { get; set; }
    private GHI_Portal_Settings__c orgDefaults; 
    public String debug { get; set; }
    public String isDuped {get;set;} 
    public String dupErrMessage {get;set;} 
    
    public GHI_Portal_AddressBook_Controller() {
        //get custom setting for portal
        this.orgDefaults = GHI_Portal_Settings__c.getOrgDefaults();
        //get record type for contact to contact customer affiliation
        this.contactAffiliationRecTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get(orgDefaults.GHI_Portal_CNT_CNT_RecordType__c).getRecordTypeId();
        //get record type for account to contact customer affiliation
        this.accountAffiliationRecTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get(orgDefaults.GHI_Portal_CNT_ACCT_Recordtype__c).getRecordTypeId(); 
        //get fields of user record
        this.user = GHI_Portal_Utilities.getCurrentUser();
        
        this.debug = 'Debug: ';
        this.searchOrder = ApexPages.currentPage().getParameters().get('q');
        this.isDuped = ApexPages.currentPage().getParameters().get('dup');
        this.dupErrMessage = 'Contact you submitted has the same last name and email address in our system and cannot be created. If you have any questions contact Customer Service.';
    }
    
    public PageReference searchAddressBook() { 
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-16-2015
        * Description   Search Address Book of the logged in user from orders object
        ----------------------------------------------------------------------- */

        PageReference pr = Page.GHI_Portal_AddressBook;
        pr.getParameters().put('q', this.searchOrder);
        pr.setRedirect(true);
        
        return pr;
    }
    
    public PageReference goToAddEditContact() {
        PageReference pr = Page.GHI_Portal_AddEditContact;
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToAddEditLocation() {
        PageReference pr = Page.GHI_Portal_AddEditLocation;
        pr.setRedirect(true);

        return pr;
    } 
    
    public List<Customer_Affiliation__c> getLocationsList() {
        /*-----------------------------------------------------------------------
        * Author        Sairah Hadjinoor
        * Company       Cloud Sherpas
        * History       MAR-11-2015
        * Description   Display Locations from customer affiliations related to the user
        * History       
        * <Date>        <Author>                <Update>
        13.MAR.2015     Stephen James Laylo     added functionality to search on portal
        ----------------------------------------------------------------------- */
        List<Customer_Affiliation__c> locationsRec = new List<Customer_Affiliation__c>();
        //get input from user
        String searchStr = ApexPages.currentPage().getParameters().get('q');
        String filter = '';
        
        if (searchStr == null) {
            searchStr = '';
        }
        
        //check if contact of user is not null
        //to make sure the user is a comm user
        if (user.ContactId != null) {
            if (searchStr != '') {
            	searchStr = '%' + searchStr + '%';
                //if user inputs a search criteria, display records that matches
                locationsRec = [SELECT Id,
                                       OSM_Account_1__r.Id, 
                                       GHI_Portal_Display_in_Portal__c, 
                                       GHI_Portal_Account_1_Phone__c, 
                                       IntlSFA_Phone__c, 
                                       OSM_Account_1__c, 
                                       GHI_Portal_Account_1_Address__c, 
                                       OSM_Account_1__r.Name, 
                                       OSM_Contact_1__c, 
                                       OSM_Account_1__r.BillingState 
                                FROM Customer_Affiliation__c 
                                WHERE GHI_Portal_Display_in_Portal__c = true 
                                AND OSM_Contact_1__c = :this.user.ContactId 
                                AND RecordTypeId = :accountAffiliationRecTypeId
                                AND OSM_Account_1__c != null
                                AND (OSM_Account_1__r.Name LIKE :searchStr 
                                     OR GHI_Portal_Account_1_Address__c LIKE :searchStr 
                                     OR IntlSFA_Phone__c LIKE :searchStr 
                                     OR GHI_Portal_Account_1_Phone__c LIKE :searchStr)]; 
            } else {
                //if no search criteria then display records that is relatd to the user
                locationsRec = [SELECT Id, 
                                       OSM_Account_1__r.Id, 
                                       GHI_Portal_Display_in_Portal__c, 
                                       GHI_Portal_Account_1_Phone__c,
                                       IntlSFA_Phone__c, 
                                       OSM_Account_1__c, 
                                       GHI_Portal_Account_1_Address__c, 
                                       OSM_Account_1__r.Name, 
                                       OSM_Contact_1__c, 
                                       OSM_Account_1__r.BillingState 
                                FROM Customer_Affiliation__c 
                                WHERE GHI_Portal_Display_in_Portal__c = true 
                                AND OSM_Contact_1__c = :this.user.ContactId 
                                AND RecordTypeId = :accountAffiliationRecTypeId
                                AND OSM_Account_1__c != null]; 
            }
        }
        
        return locationsRec;
    }
             
    public PageReference deleteLocation() {
        /*-----------------------------------------------------------------------------------
        * @author         Gypt Minierva
        * @date           03/18/2015
        * @description    List of location that is an affiliate of the user.
                          Updates the selected location to deleted in portal.
                          NOTE - Locations are not deleted from saleforce backend
                          it is only updating a Boolean value
                          that marks if the location must be displayed in the Portal or not
        ------------------------------------------------------------------------------------*/    
        
        try {
            //check if param locId is not null to avoid exception
            if (locId != null) {
                Customer_Affiliation__c locationToUpdate = [SELECT Id, 
                                                                   GHI_Portal_Display_in_Portal__c, 
                                                                   IntlSFA_Phone__c, 
                                                                   OSM_Account_1__c, 
                                                                   GHI_Portal_Account_1_Address__c, 
                                                                   OSM_Account_1__r.Name, 
                                                                   OSM_Contact_1__c 
                                                            FROM Customer_Affiliation__c 
                                                            WHERE Id = :locId];
                //check if query returns a record
                if (locationToUpdate != null) {
                    locationToUpdate.GHI_Portal_Display_in_Portal__c = false; 
                    update locationToUpdate;
                }
            }
        } catch (Exception e) {
        	Logger.debugException(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An Error occured: ' + e));
        }

        return null;
    }
    
    public List<ContactWrapper> getContactsList() {
        /*--------------------------------------------------------------------------------------------------
        * Author        Sairah Hadjinoor
        * Company       Cloud Sherpas
        * History       MAR-11-2015
        * Description   Display Contacts from customer affiliations related to the user
        * History       
        * <Date>        <Author>                <Update>
        * 13.MAR.2015   Stephen James Laylo     Added functionality to search on portal
        * 24.MAR.2015   Sairah Hadjinoor        Added functionality to use wrapper class to 
        *                                       display user's primary location to My Contacts Section
        * 25.MAY.2015   Stephen James Laylo     Bug fixing on Search Criteria
        --------------------------------------------------------------------------------------------------- */
        List<Customer_Affiliation__c> contactsRec = new List<Customer_Affiliation__c>();
        List<Customer_Affiliation__c> primaryCustomer = new List<Customer_Affiliation__c>();
        List<ContactWrapper> conWrapperList = new List<ContactWrapper>();
        Map<String, String> conCustomerMap = new Map<String, String>();
        Set<String> contactIdSet = new Set<String>();
       
        //get input from user
        String searchStr = ApexPages.currentPage().getParameters().get('q');
        String searchStr2 = ApexPages.currentPage().getParameters().get('q');
        String filter = '';
        
        Boolean isPlanB = false;
        
        if (searchStr == null) {
            searchStr = '';
        }
        
        this.debug += searchStr2;
        
        if (searchStr2 == null) {
            searchStr2 = '';
        }
        
        //check if contact of user is not null
        //to make sure the user is a comm user
        if (this.user.ContactId != null) {
            if (searchStr != '') {
            	searchStr = '%' + searchStr + '%';
                //if user inputs a search criteria, display records that matches
                contactsRec = [SELECT Id, 
                                      OSM_Contact_2__c, 
                                      GHI_Portal_Display_in_Portal__c, 
                                      OSM_Contact_2__r.Name, 
                                      OSM_Contact_2__r.Email, 
                                      IntlSFA_Phone__c, 
                                      OSM_Account_1__c, 
                                      GHI_Portal_Contact_2_Address__c, 
                                      OSM_Account_1__r.Name, 
                                      OSM_Contact_1__c, 
                                      RecordTypeId, 
                                      OSM_Primary__c 
                               FROM Customer_Affiliation__c 
                               WHERE RecordTypeId = :contactAffiliationRecTypeId
                               AND GHI_Portal_Display_in_Portal__c = true 
                               AND OSM_Contact_1__c = :this.user.ContactId
                               AND OSM_Contact_2__c != :null
                               AND (OSM_Contact_2__r.Name LIKE :searchStr 
                                    OR OSM_Contact_2__r.Email LIKE :searchStr 
                                    OR GHI_Portal_Contact_2_Address__c LIKE :searchStr)]; 
            
                if (contactsRec.isEmpty()) {
                    isPlanB = true;
                }
            } else {
                isPlanB = true;
            }
            
            if (isPlanB) {
                //if no search criteria then display records that is relatd to the user
                contactsRec = [SELECT Id, 
                                      OSM_Contact_2__c, 
                                      GHI_Portal_Display_in_Portal__c, 
                                      OSM_Contact_2__r.Name, 
                                      OSM_Contact_2__r.Email, 
                                      IntlSFA_Phone__c, 
                                      OSM_Account_1__c, 
                                      GHI_Portal_Contact_2_Address__c, 
                                      OSM_Account_1__r.Name, 
                                      OSM_Contact_1__c, 
                                      RecordTypeId, 
                                      OSM_Primary__c 
                               FROM Customer_Affiliation__c 
                               WHERE RecordTypeId = :contactAffiliationRecTypeId                                
                               AND GHI_Portal_Display_in_Portal__c = true 
                               AND OSM_Contact_2__c != :null
                               AND OSM_Contact_1__c = :this.user.ContactId];
            }
        }
        //check if user has contact and a primary location
        if (!contactsRec.isEmpty()) {
            //loop through the contacts to get add to the wrapper list
            for (Customer_Affiliation__c conRec: contactsRec) {
                //add to Set to get the contacts of the current user
                contactIdSet.add(conRec.OSM_Contact_2__c);
            }
            //check if set is not null
            if (!contactIdSet.isEmpty()) {
                //query customer affiliation of contact to display their primary location
                for (Customer_Affiliation__c cust : [SELECT OSM_Account_1__r.Name, 
                                                            OSM_Contact_1__c, 
                                                            OSM_Role__c 
                                                     FROM Customer_Affiliation__c 
                                                     WHERE OSM_Contact_1__c IN :contactIdSet 
                                                     AND OSM_Role__c = 'Primary']) {
                    //add to Map
                    conCustomerMap.put(cust.OSM_Contact_1__c, cust.OSM_Account_1__r.Name);
                    
                }
            }
            //check if map is not empty to avoid exception
            if (!conCustomerMap.isEmpty()) {
                //loop again throug the customer affiliation
                for (Customer_Affiliation__c conRec : contactsRec) {
                    if (searchStr2 == '') {
                        //add the customer affiliation to the wrapper to display
                        ContactWrapper conWrapper = new ContactWrapper(conRec, conCustomerMap.get(conRec.OSM_Contact_2__c));
                        conWrapperList.add(conWrapper);
                        this.debug += '@@Okay1' + searchStr2 + ' - ' + searchStr;
                        
                    } else {
                        this.debug += '@@Okay2' + searchStr2;
                        String email = '';
                        String cont = '';
                        String contName = '';
                        
                        if (conRec.OSM_Contact_2__r.Email != null) {
                            email = conRec.OSM_Contact_2__r.Email.toLowerCase();
                        }
                        if (conCustomerMap.get(conRec.OSM_Contact_2__c) != null) {
                            cont = conCustomerMap.get(conRec.OSM_Contact_2__c).toLowerCase();
                        }
                        if (conRec.OSM_Contact_2__r.Name != null) {
                            contName = conRec.OSM_Contact_2__r.Name.toLowerCase();
                        }
                        
                        if (cont.contains(searchStr2.toLowerCase()) || email.contains(searchStr2.toLowerCase()) || contName.contains(searchStr2.toLowerCase())) {
                            //add the customer affiliation to the wrapper to display
                            ContactWrapper conWrapper = new ContactWrapper(conRec, conCustomerMap.get(conRec.OSM_Contact_2__c));
                            conWrapperList.add(conWrapper);
                        }
                    }  
                }
            } 
        }
        
        return conWrapperList;
    }
    
    public void deleteContact() {
        /*----------------------------------------------------------------------
        * @author         Gypt Minierva
        * @date           03/18/2015
        * @description    List of contacts that is an affiliate of the user.
                          Updates the selected contact to deleted in portal.
                          NOTE - Contacts are not deleted from saleforce backend
                          it is only updating a Boolean value
                          that marks if the contact must be displayed in the Portal or not
        ----------------------------------------------------------------------*/
        try { 
            //check if conId param is not null  
            if (this.conId != null) { 
                //query customer affiliation to be deleted    
                Customer_Affiliation__c contactToUpdate = [SELECT Id,
                                                                  GHI_Portal_Display_in_Portal__c, 
                                                                  IntlSFA_Phone__c, 
                                                                  OSM_Account_1__c, 
                                                                  GHI_Portal_Account_1_Address__c, 
                                                                  OSM_Account_1__r.Name, 
                                                                  OSM_Contact_1__c 
                                                           FROM Customer_Affiliation__c 
                                                           WHERE Id = :this.conId];
                                                           
                //check if query returns a record to avoid exception
                if (contactToUpdate != null) {
                    contactToUpdate.GHI_Portal_Display_in_Portal__c = false; 
                    //update 
                    update contactToUpdate;
                }
            }
        } catch (Exception e) {
        	Logger.debugException(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An Error occured: ' + e));
        }
        
    } 
    
    public PageReference displayContactDetails() {
        /*-----------------------------------------------------------------------
        * Author        Gypt Minierva
        * Company       Cloud Sherpas
        * History       MAR-18-2015
        * Description   Redirects the user to AddEditContact page
        ----------------------------------------------------------------------- */          
        PageReference pr = Page.GHI_Portal_AddEditContact;
        pr.getParameters().put('conId', this.conId);
        pr.setRedirect(true);

        return pr;
    }
        
    public PageReference displayLocationDetails() {
        /*-----------------------------------------------------------------------
        * Author        Gypt Minierva
        * Company       Cloud Sherpas
        * History       MAR-18-2015
        * Description   Redirects the user to AddEditLocation page
        ----------------------------------------------------------------------- */        
        PageReference pr = Page.GHI_Portal_AddEditLocation;
        pr.getParameters().put('locId', this.locId);
        pr.setRedirect(true);

        return pr;
    }
        
    public class ContactWrapper {
        /*-----------------------------------------------------------------------
        * Author        Sairah Hadjinoor
        * Company       Cloud Sherpas
        * History       MAR-24-2015
        * Description   Wrapper class to display user's primary location to the My Contacts Section
        * History       
        * <Date>        <Author>                <Update>
        24.MAR.2015     Sairah Hadjinoor        Created 
        ----------------------------------------------------------------------- */
        
        public Customer_Affiliation__c customerPrimaryLocation { get; set; }
        public String customerContact { get; set; }
            
        public ContactWrapper(Customer_Affiliation__c customerLocation, String conPrimary) {
            //assign passed params
            this.customerPrimaryLocation = customerLocation;
            this.customerContact = conPrimary;
            
        }
    }
}