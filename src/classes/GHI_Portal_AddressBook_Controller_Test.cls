@isTest
public class GHI_Portal_AddressBook_Controller_Test{
    
     @testsetup 
     static void setup() {
     
          User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
            
        Account testAcct = GHI_Portal_TestUtilities.createAccountHCO(); 
        insert testAcct; 
        
        Customer_Affiliation__c testCA = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(
            testAcct.Id, portalUser.ContactId, 'Primary'); 
        insert testCA;  
        
        Contact testContact = GHI_Portal_TestUtilities.createContactHCP(testAcct.Id); 
        insert testContact; 
        
        Customer_Affiliation__c testCA2 = GHI_Portal_TestUtilities.createCNTtoCNTAffiliation(
            portalUser.ContactId, testContact.Id); 
        insert testCA2; 
        
        Customer_Affiliation__c testCA3 = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(
            testAcct.Id, testContact.Id, 'Primary'); 
        insert testCA3; 
        
        GHI_Portal_Settings__c customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
        insert customSetting; 
     }
     }
     
     @isTest
     static void addressBookTest(){             
        test.startTest(); 
        User portalUser = [SELECT Id, ContactId FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
        
        System.runAs(portalUser) {
            PageReference pageRef = Page.GHI_Portal_AddressBook;
            test.setCurrentPage(pageRef);
            
            GHI_Portal_AddressBook_Controller controller = new GHI_Portal_AddressBook_Controller();
            
            //Get locations associated to the user 
            controller.getLocationsList(); 
            
            //Get contacts associated to the user
            controller.getContactsList(); 
            
            //Get locations matching a search 
            pageRef.getParameters().put('q', 'test');
            test.setCurrentPage(pageRef);
            controller.getLocationsList();
                        
            //Get contacts matching a search
            pageRef.getParameters().put('q', 'Jack');
            test.setCurrentPage(pageRef);
            controller.getContactsList(); 
            
            Customer_Affiliation__c contactDel = [SELECT Id FROM Customer_Affiliation__c WHERE 
                OSM_Contact_1__c = :portalUser.ContactId AND OSM_Contact_2__c != null LIMIT 1]; 
            controller.conId = contactDel.Id; 
            controller.deleteContact();
            
            Customer_Affiliation__c locationDel = [SELECT Id FROM Customer_Affiliation__c WHERE 
                OSM_Contact_1__c = :portalUser.ContactId AND OSM_Account_1__c != null LIMIT 1]; 
            controller.locId = locationDel.Id;
            controller.deleteLocation(); 
             
        }
        
        test.stopTest(); 

     }
     
     @isTest
     static void referenceTest(){
         Test.startTest();
         
         PageReference pr; 
         GHI_Portal_AddressBook_Controller controller = new GHI_Portal_AddressBook_Controller();
         
         pr = controller.displayContactDetails();
         System.assertEquals(pr.getURL(), Page.GHI_Portal_AddEditContact.getURL());
         
         pr = controller.displayLocationDetails();
         System.assertEquals(pr.getURL(), Page.GHI_Portal_AddEditLocation.getURL());
         
         pr = controller.searchAddressBook();
         System.assertEquals(pr.getURL(), Page.GHI_Portal_AddressBook.getURL());
         
         pr = controller.goToAddEditContact();
         System.assertEquals(pr.getURL(), Page.GHI_Portal_AddEditContact.getURL());
         
         pr = controller.goToAddEditLocation();
         System.assertEquals(pr.getURL(), Page.GHI_Portal_AddEditLocation.getURL());
         
         Test.stopTest();
     }
}