/*------------------------------------------------------------------------
    Author:         Stephen James Laylo
    Company:        Cloud Sherpas
    Description:    A controller for GHI_Portal_ContactUs Page
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    03/23/2015      Stephen James Laylo     Created this controller
    03/25/2015      Stephen James Laylo     Converted everything here into
                                            Standard Controller & Extensions
                                            use.
    04/01/2015      Andrew Castillo         Added localization 
    5/12/2015       Paul Wittmeyer          Added Case Assignment rule firing
---------------------------------------------------------------------------*/

public class GHI_Portal_ContactUs_Controller {
    
    public User user { get; set; }
    //public String debug { get; set; }
    public String selectedCountry { get; set; }
    public String contactDetail { get; set; }
    public String successMessage { get; set; }
    public String noPhoneNumberErrMsg { get; set; }
    public String noTxtErrMsg { get; set; }
    public String noInRegardsToErrMsg { get; set; }
    public String noPrefConMethodErrMsg { get; set; }
    public Case newCase { get; set; }
    
    public GHI_Portal_ContactUs_Controller(ApexPages.StandardController controller) {
        this.newCase = (Case) controller.getRecord();
        this.user = GHI_Portal_Utilities.getCurrentUser();
        System.debug('@@User2: ' + this.user.Name + ' : ' + this.user.ContactId);
        this.successMessage = '';   
        this.noPhoneNumberErrMsg = '';
        this.noInRegardsToErrMsg = '';
        this.noPrefConMethodErrMsg = '';
        this.noTxtErrMsg = '';  
        this.selectedCountry = 'United States';
        reloadContactInformation();
    }
    
    public List<SelectOption> getCountries() {
        /*---------------------------------------------------------------------------------
           Author:        Stephen James Laylo
           Company:       Cloud Sherpas
           Description:   Used for retrieving Countries Picklist
           History
           03/23/2015     Stephen James Laylo       Created
           05/12/2015     Stephen James Laylo       Updated the values to be fetched
        ----------------------------------------------------------------------------------*/
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Select a country'));
        List<AggregateResult> groupedResults = [SELECT OSM_Country_Name__c
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Country_Name__c != ''
                                                AND OSM_Country_Name__c != null
                                                GROUP BY OSM_Country_Name__c];
        
        for (AggregateResult country: groupedResults) {
            options.add(new SelectOption((String) country.get('OSM_Country_Name__c'), (String) country.get('OSM_Country_Name__c')));                                                       
        }

        return options;
    }
    
    public void reloadContactInformation() {
        /*--------------------------------------------------------------------------------
           Author:        Stephen James Laylo
           Company:       Cloud Sherpas
           Description:   Used for reloading Contact Information
           History
           03/23/2015     Stephen James Laylo       Created
           03/24/2015     Stephen James Laylo       Updated - adjusted 
                                                    initialization from 
                                                    queries
           05/12/2015     Stephen James Laylo       Updated to add filter criteria by
                                                    Assigned Countries CSV
        ---------------------------------------------------------------------------------*/
        this.contactDetail = '';
        
        List<GHI_Portal_Contact_Details__c> contactDetails = new List<GHI_Portal_Contact_Details__c>([SELECT GHI_Portal_Assigned_Countries_CSV__c, 
                                                                                                             GHI_Portal_Contact_Information__c, 
                                                                                                             GHI_Portal_Support_Location__c
                                                                                                      FROM GHI_Portal_Contact_Details__c
                                                                                                      WHERE GHI_Portal_Locale__c = :GHI_Portal_Utilities.getUserLocale()]);
        Set<String> assignedCountries = new Set<String>();

        for (GHI_Portal_Contact_Details__c contactDetail : contactDetails) {
            if (contactDetail.GHI_Portal_Assigned_Countries_CSV__c != '' && contactDetail.GHI_Portal_Assigned_Countries_CSV__c != null && contactDetail.GHI_Portal_Assigned_Countries_CSV__c.contains(this.selectedCountry)) {
                this.contactDetail += contactDetail.GHI_Portal_Contact_Information__c + '<br /><br />';
            }
        }
    }
    
    public Boolean contains(String strSearch, List<String> strs) {
        for (String str : strs) {
            if (str == strSearch) {
                return true;
            }
        }
        
        return false;
    }
    
    
    public PageReference submitCase() {
        /*------------------------------------------------------------
           Author:        Stephen James Laylo
           Company:       Cloud Sherpas
           Description:   Used for inserting a new case.
           History
           03/24/2015     Stephen James Laylo       Created
           03/25/2015     Stephen James Laylo       Adjusted to make 
                                                    it used in
                                                    Standard Controller
        ------------------------------------------------------------*/
        
        Boolean hasMsg = true;
        Boolean hasPhone = true;
        Boolean hasInRegardsTo = true;
        Boolean hasPrefConMethod = true;
        
        this.successMessage = '';
        System.debug('!@#' + newCase.GHI_Portal_Preferred_Contact_Method__c);
        if (this.newCase.GHI_Portal_Preferred_Contact_Method__c == 'Phone') {
            System.debug('#@!' + newCase.GHI_Portal_Contact_Phone__c);
            if (this.newCase.GHI_Portal_Contact_Phone__c == '' || this.newCase.GHI_Portal_Contact_Phone__c == null) {
                this.noPhoneNumberErrMsg = Label.GHI_Portal_CU_TelephoneRequired;
                
                hasPhone = false;
            } else {
                this.noPhoneNumberErrMsg = '';
            }
        }
        
        if (this.newCase.Description == '' || this.newCase.Description == null) {
            this.noTxtErrMsg = Label.GHI_Portal_CU_MessageRequired;
            
            hasMsg = false;
            
        } else {
            if (this.newCase.Description.length() >= 1 && this.newCase.Description.length() <= 2000) {
                this.noTxtErrMsg = '';
            } else {
                this.noTxtErrMsg = Label.GHI_Portal_CU_CharacterLimit;
                hasMsg = false;
            }
        }
        
        if (this.newCase.GHI_Portal_In_Regards_To__c == '' || this.newCase.GHI_Portal_In_Regards_To__c == null) {
            this.noInRegardsToErrMsg = Label.GHI_Portal_CU_IRTRequired;
            
            hasInRegardsTo = false;
        } else {
            this.noInRegardsToErrMsg = '';
        }
        
        if (this.newCase.GHI_Portal_Preferred_Contact_Method__c == '' || this.newCase.GHI_Portal_Preferred_Contact_Method__c == null) {
            this.noPrefConMethodErrMsg = Label.GHI_Portal_CU_PMCRequired;
            
            hasPrefConMethod = false;
        } else {
            this.noPrefConMethodErrMsg = '';
        }
        
        if (!hasPhone || !hasMsg || !hasPrefConMethod || !hasInRegardsTo) {
            return null;
        }
        
        System.debug('@@Contact ID: ' + this.user.ContactId);
        if (this.user.ContactId != null) {
            Contact contact = [SELECT Id, 
                                      Name, 
                                      AccountId
                               FROM Contact
                               WHERE Id = :this.user.ContactId];

            this.newCase.Subject = this.newCase.GHI_Portal_In_Regards_To__c;
            
            this.newCase.ContactId = contact.Id;
            this.newCase.AccountId = contact.AccountId;
            
            this.newCase.Origin = 'Portal';
            this.newCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Generic').getRecordTypeId();
            this.newCase.Priority = 'Unset';
            this.newCase.GHI_Portal_User_Order_Workflow__c = GHI_Portal_Utilities.getUserOrderWorkflow();
            this.newCase.Type = 'Customer Inquiry';
            
            if (Test.isRunningTest() == false) {
                AssignmentRule ar = new AssignmentRule();
                ar = [SELECT Id 
                      FROM AssignmentRule 
                      WHERE SobjectType = 'Case' 
                      AND Active = true 
                      LIMIT 1];
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                
                dmlOpts.assignmentRuleHeader.assignmentRuleId = ar.Id;
                this.newCase.setOptions(dmlOpts);
            }
            
            try {
                insert newCase;
            } catch (Exception e) {
                Logger.debugException(e);
                return null;
            }

            // reset form
            this.noTxtErrMsg = '';
            this.noPhoneNumberErrMsg = '';
            this.noInRegardsToErrMsg = '';
            this.noPrefConMethodErrMsg = '';
            this.successMessage = Label.GHI_Portal_CU_CaseSubmitted;
            this.newCase = new Case();
            
            return Page.GHI_Portal_ContactUs;
        }
        
        return null;
    }

}