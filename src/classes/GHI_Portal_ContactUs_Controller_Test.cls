@isTest
public class GHI_Portal_ContactUs_Controller_Test{
    
     @testsetup 
     private static void setup() {
     	User portalUser = GHI_Portal_TestUtilities.createPortalUser();
        insert portalUser;
        
     }    
     
     @isTest
     private static void contactUsTestWithBlankFields() {	        
 	    Test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    
     	System.runAs(portalUser) {
     		PageReference pageRef = Page.GHI_Portal_ContactUs;
     		Test.setCurrentPage(pageRef);
     		
 	     	Case currentCase = GHI_Portal_TestUtilities.createCase();
	     	currentCase.GHI_Portal_Preferred_Contact_Method__c = 'Phone';
	     	currentCase.Description = null;
	     	currentCase.GHI_Portal_In_Regards_To__c = null;
	     	currentCase.GHI_Portal_Contact_Phone__c = null;
	     	

	     	GHI_Portal_ContactUs_Controller controller = new GHI_Portal_ContactUs_Controller(new ApexPages.StandardController(currentCase));
	     	
	     	OSM_Country_and_State__c cntry = GHI_Portal_TestUtilities.createCountries();
	     	insert cntry;
	     	controller.getCountries(); 
	     	
	     	GHI_Portal_Contact_Details__c contactDetail = GHI_Portal_TestUtilities.createContactDetails();
	     	insert contactDetail;
	     	
	     	List<String> strs = new List<String>{'a', 'b'};
	     	String strSearch = 'a';
	     	String strC = 'c';
	     	
	     	Boolean missing;
	     	missing = controller.contains(strC, strs);
	     	System.assert(missing == false);
	     	
	     	Boolean present;
	     	present = controller.contains(strSearch, strs);
	     	System.assert(present == true);
	     	controller.reloadContactInformation(); 
	     	

	     	System.debug('##Case' + currentCase.Id);
	     	
	     	//GHI_Portal_ContactUs_Controller caseControl = new GHI_Portal_ContactUs_Controller(new ApexPages.StandardController(currentCase));
	     	Account caseAcct = new Account(Name='Test Case Account 1');
	     	insert caseAcct;
	     	
	     	Contact caseCntct = new Contact(AccountId=caseAcct.Id, LastName='Red');
	     	insert caseCntct;
	     	System.debug('###Contact' + portalUser.ContactId);
	     	controller.submitCase();
	     	 
     	}
     	
     	Test.stopTest();
     }
      
     @isTest
     private static void contactUsTestWithCharacterLimit() {	        
 	    Test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    
     	System.runAs(portalUser) {
     		PageReference pageRef = Page.GHI_Portal_ContactUs;
     		Test.setCurrentPage(pageRef);
     		
 	     	Case currentCase = GHI_Portal_TestUtilities.createCase();
	     	currentCase.GHI_Portal_Preferred_Contact_Method__c = null;
	     	currentCase.GHI_Portal_In_Regards_To__c = null;
	     	currentCase.GHI_Portal_Contact_Phone__c = null;
	     	
	     	String tempStr = '';
	     	
	     	for (Integer i = 0; i < 2001; i ++) {
	     	    tempStr += 'a';
	     	}
	     	
	     	currentCase.Description = tempStr;
            
            insert currentCase;
            
	     	GHI_Portal_ContactUs_Controller controller = new GHI_Portal_ContactUs_Controller(new ApexPages.StandardController(currentCase));
	     	
	     	OSM_Country_and_State__c cntry = GHI_Portal_TestUtilities.createCountries();
	     	insert cntry;
	     	controller.getCountries(); 
	     	
	     	GHI_Portal_Contact_Details__c contactDetail = GHI_Portal_TestUtilities.createContactDetails();
	     	insert contactDetail;
	     	
	     	List<String> strs = new List<String>{'a', 'b'};
	     	String strSearch = 'a';
	     	String strC = 'c';
	     	
	     	Boolean missing;
	     	missing = controller.contains(strC, strs);
	     	System.assert(missing == false);
	     	
	     	Boolean present;
	     	present = controller.contains(strSearch, strs);
	     	System.assert(present == true);
	     	controller.reloadContactInformation(); 
	     	

	     	System.debug('##Case' + currentCase.Id);
	     	
	     	//GHI_Portal_ContactUs_Controller caseControl = new GHI_Portal_ContactUs_Controller(new ApexPages.StandardController(currentCase));
	     	Account caseAcct = new Account(Name='Test Case Account 3');
	     	insert caseAcct;
	     	
	     	Contact caseCntct = new Contact(AccountId=caseAcct.Id, LastName='Green');
	     	insert caseCntct;
	     	System.debug('###Contact' + portalUser.ContactId);
	     	controller.submitCase();
	     	 
     	}
     	
     	Test.stopTest();
     }
     
     @isTest
     private static void contactUsTestHappyPath() {	        
 	    Test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    
 	    System.debug('@@User1: ' + portalUser.Username + ' : ' + portalUser.ContactId);
 	    
     	System.runAs(portalUser) {
     		PageReference pageRef = Page.GHI_Portal_ContactUs;
     		Test.setCurrentPage(pageRef);
     		
 	     	Case currentCase = GHI_Portal_TestUtilities.createCase();
	     	currentCase.GHI_Portal_Preferred_Contact_Method__c = 'Phone';
	     	currentCase.GHI_Portal_Contact_Phone__c = '+639353782504';
	     	currentCase.Description = 'Test Description';
	     	currentCase.GHI_Portal_In_Regards_To__c = 'Ordering Process';

	     	GHI_Portal_ContactUs_Controller controller = new GHI_Portal_ContactUs_Controller(new ApexPages.StandardController(currentCase));
	     	
	     	OSM_Country_and_State__c cntry = GHI_Portal_TestUtilities.createCountries();
	     	insert cntry;
	     	controller.getCountries(); 
	     	
	     	GHI_Portal_Contact_Details__c contactDetail = GHI_Portal_TestUtilities.createContactDetails();
	     	insert contactDetail;
	     	
	     	List<String> strs = new List<String>{'a', 'b'};
	     	String strSearch = 'a';
	     	String strC = 'c';
	     	
	     	Boolean missing;
	     	missing = controller.contains(strC, strs);
	     	System.assert(missing == false);
	     	
	     	Boolean present;
	     	present = controller.contains(strSearch, strs);
	     	System.assert(present == true);
	     	controller.reloadContactInformation(); 
	     	

	     	System.debug('##Case' + currentCase.Id);
	     	
	     	//GHI_Portal_ContactUs_Controller caseControl = new GHI_Portal_ContactUs_Controller(new ApexPages.StandardController(currentCase));
	     	Account caseAcct = new Account(Name='Test Case Account 2');
	     	insert caseAcct;
	     	
	     	Contact caseCntct = new Contact(AccountId=caseAcct.Id, LastName='Blue');
	     	insert caseCntct;
	     	System.debug('###Contact' + portalUser.ContactId);
	     	controller.submitCase();
	     	 
     	}
     	
     	Test.stopTest();
     }
    
}