/*------------------------------------------------------------------------
    Author:         Stephen James Laylo
    Company:        Cloud Sherpas
    Description:    A controller for most portal VF pages
                  
    Test Class:     GHI_Portal_Controller_Test
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    12/17/2014      Stephen James Laylo     Created this controller
    01/13/2015      Stephen James Laylo     Created a module for querying
                                            Submitted Orders, Completed Orders
                                            and Archived Orders.
    01/20/2015      Stephen James Laylo     Updated the constructor
    01/20/2015      Stephen James Laylo     Created a module for fetching
                                            attachments for each Alerts &
                                            Announcements
    01/20/2015      Stephen James Laylo     Created a module for navigation
                                            of pages and the pagination of 
                                            Submitted Orders.
    01/20/2015      Stephen James Laylo     Created a module for sorting
                                            Submitted Orders based on what
                                            column you are clicking to sort.
    02/03/2015      Stephen James Laylo     Added attributes for making
                                            Order Steps dynamic in display 
                                            based on what assay is selected by
                                            the user.
    02/05/2015      Stephen James Laylo     Worked on pagination for Alerts &
                                            Announcements.
    02/24/2015      Stephen James Laylo     Updated the constructor for language translation
    03/02/2015      Stephen James Laylo     Created a Standard Set Controller for Recent Orders
                                            for pagination purposes.
    03/16/2015      Stephen James Laylo     Added a search functionality in querying Locations & Contacts
                                            in Address Book.
    06/11/2015      Katrina Guarina         Added logic to retrieve and display Terms & Conditions 
---------------------------------------------------------------------------*/

public class GHI_Portal_Controller {
    /* DECLARATIONS */
    public User user { get; set; }
    transient Id userId;
    public List<Order> orders { get; set; }
    public List<Order> ordersToday { get; set; }
    public List<Order> ordersYesterday { get; set; }
    public List<Order> ordersOlder { get; set; }
    public list<GHI_Portal_Static_Content__c> termsCheckList {get;set;}
    public String debug { get; set; }
    // public Boolean hasRecentOrder { get; set; }
    public Boolean isOrderInProgress { get; set; }
    public boolean displayPopup {get; set;} 
    // public String pageTitle { get; set; }
    public String selectedTab { get; set; }
    // public List<Contact> contactResult { get; set; }
    public List<GHI_Portal_Alerts_and_Announcement__c> alertsList { get; set; }
    public Date dateToday { get; set; }
    public Integer numberOfAlertsAndAnnouncements { get; set; }
    public Integer numberOfRecentOrdersToday { get; set; }
    public Integer numberOfRecentOrdersYesterday { get; set; }
    public Integer numberOfRecentOrdersOlder { get; set; }
    // public User currentUser { get; set; }
    public String lang { get; set; }
    public String logo { get; set; }
    // public String searchOrder { get; set; }
    // public List<Customer_Affiliation__c>  myOrdersList { get; set; }
    public String queryOrder { get; set; }
    // public Id locId { get; set; }
    // public Id dlocId { get; set; }
    // public Id conId { get; set; }
    public GHI_Portal_Okta__c oktaOrgDefaults; 
    public GHI_Portal_Settings__c orgDefaults{get;set;}
    // public String contactAffiliationRecTypeId{get;set;}
    // public String accountAffiliationRecTypeId{get;set;}
    public String ordId { get; set; }
    public String ownrId { get; set; } 
    public String speakerPortalStr {get;set;} 
    public GHI_Portal_Static_Content__c termsConds {get;set;} 
    public boolean isDelegate{get;set;} 
    public boolean termsCheck{get;set;} 
    public set<id> sponsorIdSet {get;set;}
    public list<user> delegateUserList {get;set;}
    public boolean notAllChecked {get;set;}
    public static boolean termsCheckFinal {get;set;}
    public GHI_Portal_Static_Content__c delAtt {get;set;} 
    public Set<OSM_Delegate__c> delegateSet {get;set;}
    public boolean delegateShow {get;set;} 
    
    public GHI_Portal_Controller(){
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       JAN-20-2015
        * Description   Consructor. All initialization goes here.
        ----------------------------------------------------------------------- */

        this.lang = UserInfo.getLanguage() + Label.GHI_Portal_Slash;
        
        if (this.lang == Label.GHI_Portal_EnglishLanguage + Label.GHI_Portal_Slash) {
            this.lang = '';
        }
        //Get Custom Settings for Portal Setting and Okta 
        orgDefaults = GHI_Portal_Settings__c.getOrgDefaults();
        oktaOrgDefaults = GHI_Portal_Okta__c.getOrgDefaults();
        
        this.speakerPortalStr = oktaOrgDefaults.GHI_Portal_Link_Speaker_Portal_Tab__c; 
        this.logo = Label.GHI_Portal_Logo;
        userId = UserInfo.getUserId();
        //isDelegate = false;
        delegateShow = false;
        this.notAllChecked = false;
        
        //Get permissions defined in logged in user's User record 
        user = [SELECT FirstName, LastName, ContactId, Contact.AccountId, Contact.Account.Name, 
            GHI_Portal_Must_Agree_to_Terms__c, GHI_Portal_Authorization_Date__c, 
            GHI_Portal_Ordering_Permissions__c, GHI_Portal_Box_Access__c, 
            GHI_Portal_Tools_Access__c, GHI_Portal_Speaker_Portal_Access__c, GHI_Portal_Delegate_Attestation_Date__c
            FROM User WHERE Id = :userId LIMIT 1];
            System.debug('!@#user '+ user); 
            System.debug('!@#userCon '+ user.ContactId);
        
        //Retrieve Delegate records to check if User is a delegate     
        Set<OSM_Delegate__c> delegateSet = new Set<OSM_Delegate__c>([SELECT id, OSM_Contact__c, OSM_HCP__r.name,  OSM_HCP__c
                                                                    from OSM_Delegate__c where OSM_Contact__c = :user.ContactId]);
                
        this.sponsorIdSet = new Set<Id>();
        for(OSM_Delegate__c d : delegateSet){
            sponsorIdSet.add(d.OSM_HCP__c);
        }                       
        this.delegateUserList = new List<user>([SELECT id, name, GHI_Portal_Authorization_Date__c from user where contactid IN : sponsorIdSet]);
        system.debug('!@#DELEGATELIST ' + delegateUserList);
        
        //Added: KGuarina 061115, modified Paul Wittmeyer 7/1/2015 
        //---START---
        //Set flag if User should agree to terms and show pop-up page 
        displayPopup = user.GHI_Portal_Must_Agree_to_Terms__c; 
        this.termsCheckList = new List<GHI_Portal_Static_Content__c>();
        termsCheck = false;
        termsCheckFinal = false;
        system.debug('!@#DELEGATESET ' + delegateSet);
        
        //If logged  in user is a delegate
        if(delegateSet.size() > 0 && !displayPopUp ){
            isDelegate = true;
            delegateshow = true;
            //If Attestation date is null, display Delegate Attestation page 
            if(!displayPopUp && (user.GHI_Portal_Delegate_Attestation_Date__c == null)){
            	delegateshow = true;
            }
            //If Attestation date is NOT null, DO NOT display Delegate Attestation page 
            if(user.GHI_Portal_Delegate_Attestation_Date__c != null){
                delegateShow = false;
            }
        }else{
    		isDelegate = false;
        }
        System.debug('!@#TermsCon ' + termsConds);
        //If user must agree to terms and conditions
        if(displayPopup == true){ 
            //Query terms and conditions content from Portal Static Content object 
            List<GHI_Portal_Static_Content__c> termsConds = new List<GHI_Portal_Static_Content__c>([SELECT Name, GHI_Portal_Static_Content__c FROM GHI_Portal_Static_Content__c WHERE 
                                                    GHI_Portal_Content_Type__c = 'Terms and Conditions' LIMIT 1]);
            if (termsConds.size() > 0) {
            this.termsConds = termsConds.get(0);
            }
        }
        
        //Query Delegate Attestation content from Portal Static Content object 
        List<GHI_Portal_Static_Content__c> termsList = new List<GHI_Portal_Static_Content__c >([SELECT Id, Name, GHI_Portal_Static_Content__c,
                                                                        GHI_Portal_Content_Type__c FROM 
                                                                        GHI_Portal_Static_Content__c WHERE 
                                                                        GHI_Portal_Content_Type__c = 'Delegate Attestation' or
                                                                        GHI_Portal_Content_Type__c = 'Delegate Attestation Checkboxes']);
                                                                        
        System.debug('!@#StaticContent ' + termsList);
        
        //Loop through Attestation content 
        for(GHI_Portal_Static_Content__c t : termsList){
            //Construct Attestation checkboxes 
            if(t.GHI_Portal_Content_Type__c == 'Delegate Attestation Checkboxes'){
                termsCheckList.add(t);
            }   
            if(delegateShow){
                if(t.GHI_Portal_Content_Type__c == 'Delegate Attestation'){
                    delAtt = t;
                }
            }
            
        }
        //---END---
        
        //Retrieve ALL recent orders created by logged in user 
        this.orders = new List<Order>([SELECT Id, CreatedById, CreatedDate, LastModifiedDate, OwnerId, OSM_Patient__r.Name, OSM_Status__c, Status, OSM_Patient__c, OSM_Product__c, OrderNumber, OSM_Patient_DOB__c, OSM_Patient_MRN__c,
                                      (SELECT Id, PriceBookEntry.Name, OSM_Order_Line_Item_Status__c FROM OrderItems ORDER BY Id)  
                                       FROM Order
                                       //WHERE OwnerId = :UserInfo.getUserId()
                                       WHERE CreatedById = :UserInfo.getUserId()
                                       AND (OSM_Status__c = 'Order Intake'
                                       OR OSM_Status__c = 'Canceled'
                                       OR OSM_Status__c = 'Closed')
                                       ORDER BY LastModifiedDate DESC]);
        /*this.ordersToday = new List<Order>([SELECT Id, OwnerId, OSM_Patient__r.Name, OSM_Status__c, Status, OSM_Patient__c, OSM_Product__c, OrderNumber, OSM_Patient_DOB__c, OSM_Patient_MRN__c  
                                            FROM Order
                                            WHERE CreatedDate >= TODAY
                                            
                                            ORDER BY CreatedDate DESC]);
        this.ordersYesterday = new List<Order>([SELECT Id, OwnerId, EffectiveDate, OSM_Patient__r.Name, OSM_Status__c, Status, OSM_Patient__c, OSM_Product__c, OrderNumber, OSM_Patient_DOB__c, OSM_Patient_MRN__c  
                                                FROM Order
                                                WHERE CreatedDate = YESTERDAY
                                                
                                                ORDER BY CreatedDate DESC]);
        this.ordersOlder = new List<Order>([SELECT Id, OwnerId, OSM_Patient__r.Name, OSM_Status__c, Status, OSM_Patient__c, OSM_Product__c, OrderNumber, OSM_Patient_DOB__c, OSM_Patient_MRN__c  
                                            FROM Order
                                            WHERE CreatedDate <= LAST_N_DAYS:2 AND CreatedDate >= LAST_N_DAYS:21
                                            
                                            ORDER BY CreatedDate DESC]);*/
        this.ordersToday = new List<Order>();
        this.ordersYesterday = new List<Order>();
        this.ordersOlder = new List<Order>();
        
        //Set current date
        DateTime dT = System.now();
		//Date currentDate = dT.dateGMT();
		Date currentDate = dT.date();
		
		//Loop through all recent orders retrieved
        for (Order order : orders) {
            //Get last modified date of each record 
            //String[] arrStrDate = (order.LastModifiedDate + '').split(' ')[0].split('-');
            //Date newDate = Date.newInstance(Integer.valueOf(arrStrDate[0]), Integer.valueOf(arrStrDate[1]), Integer.valueOf(arrStrDate[2]));
            Date newDAte = order.LastModifiedDate.date(); 
            
            System.debug('!@#newDate: ' + newDate); 
            System.debug('!@#System.now(): ' + System.now()); 
            System.debug('!@#currentDate: ' + currentDate); 
            System.debug('!@#dT.date(): ' + dT.date()); 
            System.debug('!@#dT.dateGMT()(): ' + dT.dateGMT()); 
            
            //If order last modified date is equal to current date 
            if (newDate == currentDate) {
                //Add to orders TODAY 
                ordersToday.add(order);
            //Else If order last modified date is one day less the current date 
            } else if (newDate == currentDate.addDays(-1)) {
                //Add to orders YESTERDAY 
                ordersYesterday.add(order);
            //Else If order last modified date is 2-21 days less the current date 
            } else if (newDate <= currentDate.addDays(-2) && newDate >= currentDate.addDays(-21)) {
                //Add to orders OLDER 
                ordersOlder.add(order);
            }
        }
        
        //Retrieve Alerts and Announcment records published for User's locale 
        this.alertsList = new List<GHI_Portal_Alerts_and_Announcement__c>([SELECT Id, Name, GHI_Portal_Description__c,GHI_Portal_Published_Date__c, 
                                                                                  GHI_Portal_End_Date__c, GHI_Portal_Published__c, GHI_Portal_Start_Date__c, 
                                                                                  GHI_Portal_Subject__c, 
                                                                                  (SELECT Id, Name, ContentType FROM Attachments)
                                                                                   FROM GHI_Portal_Alerts_and_Announcement__c 
                                                                                   WHERE GHI_Portal_Alert_Today__c = true
                                                                                   AND GHI_Portal_Published__c = true 
                                                                                   AND GHI_Portal_Locale__c = :GHI_Portal_Utilities.getUserLocale()
                                                                                   ORDER BY GHI_Portal_Published_Date__c DESC]);
        
        this.dateToday = date.today();
        
        //Set number of records retrieved for display 
        this.numberOfAlertsAndAnnouncements = getAlertsAndAnnouncements(false).size();
        this.numberOfRecentOrdersToday = getRecentOrdersToday(false).size();
        this.numberOfRecentOrdersYesterday = getRecentOrdersYesterday(false).size();
        this.numberOfRecentOrdersOlder = getRecentOrdersOlder(false).size();
        
        //Set first part of Order query string 
        queryOrder = 'SELECT Id,GHI_Portal_Display_in_Portal__c, IntlSFA_Phone__c, OSM_Account_1__c, GHI_Portal_Account_1_Address__c, OSM_Account_1_Name__c FROM Customer_Affiliation__c WHERE ';
        debug = '';
    }


    public ApexPages.StandardSetController alertsAndAnnouncementsSetCon {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       FEB-05-2015
        * Description   Initialization for the Standard Set Controller of
        *               Alerts.
        ----------------------------------------------------------------------- */ 
        get {
            if (this.alertsAndAnnouncementsSetCon == null) {
                //Set Alerts standard set controller 
                this.alertsAndAnnouncementsSetCon = new ApexPages.StandardSetController(this.alertsList);
            }
            //If defined, set page size from custom setting 
            if (orgDefaults.GHI_Portal_Records_Per_Page__c != null) {
                this.alertsAndAnnouncementsSetCon.setPageSize((Integer) orgDefaults.GHI_Portal_Records_Per_Page__c);
            //Else, set default page size to 10 
            } else {
                this.alertsAndAnnouncementsSetCon.setPageSize(10);
            }
            return alertsAndAnnouncementsSetCon;
        }
        set;
    }

    public ApexPages.StandardSetController roSetConToday {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-02-2015
        * Description   Initialization for the Standard Set Controller of
        *               Recent Orders from Today.
        ----------------------------------------------------------------------- */ 
        get {
            if (this.roSetConToday == null) {
                //Set Standard Set Controller for Recent Orders 
                this.roSetConToday = new ApexPages.StandardSetController(this.ordersToday);
            }
            //If defined, set page size from custom setting 
            if (orgDefaults.GHI_Portal_Records_Per_Page__c != null) {
                this.roSetConToday.setPageSize((Integer) orgDefaults.GHI_Portal_Records_Per_Page__c);
            //Else, set default page size to 10 
            } else {
                this.roSetConToday.setPageSize(10);
            }
            return roSetConToday ;
        }
        set;
    }

    public ApexPages.StandardSetController roSetConYesterday {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-02-2015
        * Description   Initialization for the Standard Set Controller of
        *               Recent Orders from Yesterday.
        ----------------------------------------------------------------------- */ 
        get {
            if (this.roSetConYesterday == null) {
                //Set Standard Set Controller for Yesterday's Orders 
                this.roSetConYesterday = new ApexPages.StandardSetController(this.ordersYesterday);
            }
            //If defined, set page size from custom setting 
            if (orgDefaults.GHI_Portal_Records_Per_Page__c != null) {
                this.roSetConYesterday.setPageSize((Integer) orgDefaults.GHI_Portal_Records_Per_Page__c);
            //Else, set default page size to 10 
            } else {
                this.roSetConYesterday.setPageSize(10);
            }
            return roSetConYesterday ;
        }
        set;
    }

    public ApexPages.StandardSetController roSetConOlder {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-02-2015
        * Description   Initialization for the Standard Set Controller of
        *               Older Recent Orders.
        ----------------------------------------------------------------------- */ 
        get {
            if (this.roSetConOlder == null) {
                //Set Standard Set Controller for Older Orders 
                this.roSetConOlder = new ApexPages.StandardSetController(this.ordersOlder);
            }
            //If defined, set page size from custom setting 
            if (orgDefaults.GHI_Portal_Records_Per_Page__c != null) {
                this.roSetConOlder.setPageSize((Integer) orgDefaults.GHI_Portal_Records_Per_Page__c);
            } else {
                //Else, set default page size to 10 
                this.roSetConOlder.setPageSize(10);
            }
            return roSetConOlder ;
        }
        set;
    }

    public DateTime dateTimeValue { get; set; }
    public Boolean isDateOnly { get; set; }
    
    public String getTimeZoneValue() {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-02-2015
        * Description   Format DateTime value to get Date part only 
        ----------------------------------------------------------------------- */ 
        if (dateTimeValue != null) {
            String localeFormatDT = dateTimeValue.format();
            if (isDateOnly) {
                return localeFormatDT.split(' ')[0];
            }
            return localeFormatDT;
        }
        return null;
    }

    
    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       FEB-05-2015
    * Description   Query for selecting all alerts and announcements
    * Inputs        "isLimited" - if true, it will return paginated list.
                    Otherwise, it will return all Submitted Orders in a list.
    * Returns       List of Alerts and Announcements
    ----------------------------------------------------------------------- */ 
    public List<GHI_Portal_Alerts_and_Announcement__c> getAlertsAndAnnouncements(Boolean isLimited) {
        if (isLimited) {
            return (List<GHI_Portal_Alerts_and_Announcement__c>) alertsAndAnnouncementsSetCon.getRecords();
        } else {
            return this.alertsList;
        }
    }
    //Calls the first function with a default value of true for the isLimited argument.
    public List<GHI_Portal_Alerts_and_Announcement__c> getAlertsAndAnnouncements() {
        return getAlertsAndAnnouncements(true);
    }

    public void setAlertsAndAnnouncements(List<GHI_Portal_Alerts_and_Announcement__c> alertsAndAnnouncements) {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       FEB-05-2015
        * Description   Method for setting a list of Alerts and Announcements.
        * Inputs        "alertsAndAnnouncements" - holds the list of Alerts and Announcements.
        ----------------------------------------------------------------------- */ 
        this.alertsList = alertsAndAnnouncements;
    }

    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       MAR-02-2015
    * Description   Query for selecting all recent orders
    * Inputs        "isLimited" - if true, it will return paginated list.
                    Otherwise, it will return all Today's Submitted Orders in a list.
    * Returns       List of Recent Orders (Today)
    ----------------------------------------------------------------------- */ 
    public List<Order> getRecentOrdersToday(Boolean isLimited) {
        if (isLimited) {
            return (List<Order>) roSetConToday.getRecords();
        } else {
            return this.ordersToday;
        }
    }
    //Calls the above function with a default value of true for the isLimited argument.
    public List<Order> getRecentOrdersToday() {
        return getRecentOrdersToday(true);
    }

    public void setRecentOrdersToday(List<Order> recentOrdersToday) {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-02-2015
        * Description   Method for setting a list of Today's Recent Orders.
        * Inputs        "recentOrders" - holds the list of Today's Recent Orders.
        ----------------------------------------------------------------------- */ 
        this.ordersToday = recentOrdersToday;
    }

    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       MAR-02-2015
    * Description   Query for selecting all recent orders
    * Inputs        "isLimited" - if true, it will return paginated list.
                    Otherwise, it will return all Yesterday's Submitted Orders in a list.
    * Returns       List of Recent Orders (Yesterday)
    ----------------------------------------------------------------------- */ 
    public List<Order> getRecentOrdersYesterday(Boolean isLimited) {
        if (isLimited) {
            return (List<Order>) roSetConYesterday.getRecords();
        } else {
            return this.ordersYesterday;
        }
    }
    //Calls the above function with a default value of true for the isLimited argument.
    public List<Order> getRecentOrdersYesterday() {
        return getRecentOrdersYesterday(true);
    }

    public void setRecentOrdersYesterday(List<Order> recentOrdersYesterday) {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-02-2015
        * Description   Method for setting a list of Yesterday's Recent Orders.
        * Inputs        "recentOrders" - holds the list of Yesterday's Recent Orders.
        ----------------------------------------------------------------------- */ 
        this.ordersYesterday = recentOrdersYesterday;
    }

    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       MAR-02-2015
    * Description   Query for selecting all recent orders
    * Inputs        "isLimited" - if true, it will return paginated list.
                    Otherwise, it will return all Older Submitted Orders in a list.
    * Returns       List of Recent Orders (Older)
    ----------------------------------------------------------------------- */ 
    public List<Order> getRecentOrdersOlder(Boolean isLimited) {
        if (isLimited) {
            return (List<Order>) roSetConOlder.getRecords();
        } else {
            return this.ordersOlder;
        }
    }
    //Calls the above function with a default value of true for the isLimited argument.
    public List<Order> getRecentOrdersOlder() {
        return getRecentOrdersOlder(true);
    }

    public void setRecentOrdersOlder(List<Order> recentOrdersOlder) {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-02-2015
        * Description   Method for setting a list of Older Recent Orders.
        * Inputs        "recentOrders" - holds the list of Older Recent Orders.
        ----------------------------------------------------------------------- */ 
        this.ordersOlder = recentOrdersOlder;
    }

    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       JAN-20-2015
    * Description   Page redirections.
    ----------------------------------------------------------------------- */
    
    //Redirects the user to the Home page 
    public PageReference goToHome() {
        PageReference pr = Page.GHI_Portal_Home; 
        pr.setRedirect(true);
        
        return pr;
    }
    
    //Redirects the user to the Login page 
    public PageReference goToLogIn() {
        PageReference pr = Page.CommunitiesLogin;
        pr.setRedirect(true);
        
        return pr;
    }
    
    //Redirects the user to the Start Continue page 
    public PageReference goToOrderInProgress() {
        PageReference pr = Page.GHI_Portal_OIP_StartContinue;
        pr.setRedirect(true);

        return pr;
    }
    
    //Redirects the user to the Orders page 
    public PageReference viewAllOrders() {
        PageReference pr = Page.GHI_Portal_Order;
        pr.setRedirect(true);
        
        return pr;
    }
    
    //Redirects the user to the Orders page 
    public PageReference viewOrderDetails() {
        PageReference pr = Page.GHI_Portal_Order;
        pr.setRedirect(true);

        return pr;
    }
    
    //Redirects the user to the Addressbook page 
    public PageReference goToAddressBook() {
        PageReference pr = Page.GHI_Portal_AddressBook;
        pr.setRedirect(true);

        return pr;
    }
    
    //Redirects the user to the Logout page 
    public PageReference logout() {
        // PageReference pr = Page.GHI_Portal_RedirectLogout;
        // pr.setRedirect(true);
        
        // return pr;
        return new PageReference('/secur/logout.jsp');
    }
    
    //Redirects the user to the Help page 
    public PageReference goToHelp() {
        PageReference pr = Page.GHI_Portal_Help;
        pr.getParameters().put('page', 'help');
        pr.setRedirect(true);
        
        return pr;
    }
    
    public PageReference goToHowToOrder() {
        ApexPages.currentPage().getParameters().put('page', 'order');
        System.debug('@@sai '+ApexPages.currentPage().getParameters().get('page'));
        
        return null;
    }        
    
    //Redirects the user to the My Account page 
    public PageReference goToMyAccount() {
        PageReference pr = Page.GHI_Portal_Account;
        pr.setRedirect(true);
        
        return pr;
    }
    
    //Redirects the user to the Help page 
    public PageReference goToInsurance() {
        PageReference pr = Page.GHI_Portal_Help;
        pr.setRedirect(true);
        
        return pr;
    }
    
    //Redirects the user to the Help page 
    public PageReference goToHelpSpecimen() {
        PageReference pr = Page.GHI_Portal_Help;
        pr.setRedirect(true);
        
        return pr;
    } 
    
    //Redirects the user to the Add Edit Contact page 
    public PageReference goToAddEditContact() {
        PageReference pr = Page.GHI_Portal_AddEditContact;
        pr.setRedirect(true);

        return pr;
    }
    
    //Redirects the user to the Add Edit Location page 
    public PageReference goToAddEditLocation() {
        PageReference pr = Page.GHI_Portal_AddEditLocation;
        pr.setRedirect(true);

        return pr;
    } 
    
    //Redirects the user to the Order Details page 
    public PageReference displayOrderDetail(){
        PageReference pr = Page.GHI_Portal_OrderDetails;
        pr.getParameters().put('ordId', ordId);
        pr.getParameters().put('ownrId', ownrId);
        pr.setRedirect(true);

        return pr;
        }       

    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       FEB-05-2015
    * Description   Navigation for Alerts and Announcements. Used in pagination
    ----------------------------------------------------------------------- */
    
    //Gets next set of Alert/Announcment records 
    public void showNextRecordsForAlertsAndAnnouncements() {
        this.alertsAndAnnouncementsSetCon.next();
    }
    //Gets previous set of Alert/Announcment records 
    public void showPreviousRecordsForAlertsAndAnnouncements() {
        this.alertsAndAnnouncementsSetCon.previous();
    }
    //Gets first set of Alert/Announcment records 
    public void showFirstRecordsForAlertsAndAnnouncements() {
        this.alertsAndAnnouncementsSetCon.first();
    }
    //Gets last set of Alert/Announcment records 
    public void showLastRecordsForAlertsAndAnnouncements() {
        this.alertsAndAnnouncementsSetCon.last();
    }
    //Gets hasNext flag of Alert/Announcment records 
    public Boolean getHasNextRecordsForAlertsAndAnnouncements() {
        return this.alertsAndAnnouncementsSetCon.getHasNext();
    }
    //Gets hasPrevious flag of Alert/Announcment records 
    public Boolean getHasPreviousRecordsForAlertsAndAnnouncements() {
        return this.alertsAndAnnouncementsSetCon.getHasPrevious();
    }

    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       MAR-02-2015
    * Description   Navigation for Today's Recent Orders. Used in pagination
    ----------------------------------------------------------------------- */
    
    //Gets next set of Recent Order records 
    public void showNextRecordsForRecentOrdersToday() {
        this.roSetConToday.next();
    }
    //Gets previous set of Recent Order records 
    public void showPreviousRecordsForRecentOrdersToday() {
        this.roSetConToday.previous();
    }
    //Gets first set of Recent Order records 
    public void showFirstRecordsForRecentOrdersToday() {
        this.roSetConToday.first();
    }
    //Gets last set of Recent Order records 
    public void showLastRecordsForRecentOrdersToday() {
        this.roSetConToday.last();
    }
    //Gets hasnext flag of Recent Order records 
    public Boolean getHasNextRecordsForRecentOrdersToday() {
        return this.roSetConToday.getHasNext();
    }
    //Gets hasprevious flag of Recent Order records 
    public Boolean getHasPreviousRecordsForRecentOrdersToday() {
        return this.roSetConToday.getHasPrevious();
    }

    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       MAR-02-2015
    * Description   Navigation for Yesterday's Recent Orders. Used in pagination
    ----------------------------------------------------------------------- */
    
    //Gets next set of Yesterday Order records 
    public void showNextRecordsForRecentOrdersYesterday() {
        this.roSetConYesterday.next();
    }
    //Gets previous set of Yesterday Order records 
    public void showPreviousRecordsForRecentOrdersYesterday() {
        this.roSetConYesterday.previous();
    }
    //Gets first set of Yesterday Order records 
    public void showFirstRecordsForRecentOrdersYesterday() {
        this.roSetConYesterday.first();
    }
    //Gets last set of Yesterday Order records 
    public void showLastRecordsForRecentOrdersYesterday() {
        this.roSetConYesterday.last();
    }
    //Gets hasnext flag of Yesterday Order records 
    public Boolean getHasNextRecordsForRecentOrdersYesterday() {
        return this.roSetConYesterday.getHasNext();
    }
    //Gets hasprevious flag of Yesterday Order records 
    public Boolean getHasPreviousRecordsForRecentOrdersYesterday() {
        return this.roSetConYesterday.getHasPrevious();
    }

    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       MAR-02-2015
    * Description   Navigation for Older Recent Orders. Used in pagination
    ----------------------------------------------------------------------- */
    
    //Gets next set of Older Order records 
    public void showNextRecordsForRecentOrdersOlder() {
        this.roSetConOlder.next();
    }
    //Gets previous set of Older Order records 
    public void showPreviousRecordsForRecentOrdersOlder() {
        this.roSetConOlder.previous();
    }
    //Gets first set of Older Order records 
    public void showFirstRecordsForRecentOrdersOlder() {
        this.roSetConOlder.first();
    }
    //Gets last set of Older Order records 
    public void showLastRecordsForRecentOrdersOlder() {
        this.roSetConOlder.last();
    }
    //Gets hasnext flag of Older Order records 
    public Boolean getHasNextRecordsForRecentOrdersOlder() {
        return this.roSetConOlder.getHasNext();
    }
    //Gets hasprevious flag of Older Order records 
    public Boolean getHasPreviousRecordsForRecentOrdersOlder() {
        return this.roSetConOlder.getHasPrevious();
    }  
    
    public void closePopup(){
        /*-----------------------------------------------------------------------
        * Author        Katrina Guarina
        * Company       Cloud Sherpas
        * History       Jun-11-2015
        * Description   Method invoked to close T&C Popup 
        ----------------------------------------------------------------------- */
        system.debug('FINAL CHECHBOX' + TermsCheckFinal);
        if(displayPopup){
            //Set authorization date to today's date 
            user.GHI_Portal_Authorization_Date__c = Date.today();
            //Set Must Agree to Terms field to false 
            user.GHI_Portal_Must_Agree_to_Terms__c = false; 
            //Check if user is a delegate 
            Set<OSM_Delegate__c> delegateSet = new Set<OSM_Delegate__c>([SELECT id, OSM_Contact__c, OSM_HCP__r.name,  OSM_HCP__c
                                                                    from OSM_Delegate__c where OSM_Contact__c = :user.ContactId]);
            System.debug('!@#Delegate ' + delegateSet);
    		if(delegateset.size() >0){
    		    //Show delegate attestation popup 
    			delegateshow = true;
    		}
            try{ 
                //Update user record 
                update user;
            }
            catch(Exception e){ 
                Logger.debugException(e);
                System.debug('@@Error: ' + e);
            }
            //Hide T&C popup 
            displayPopup = false;
        //If user is a delegate and has agreed to delegate attestation page 
        }else if(delegateShow && termsCheckFinal){
            //Set attestation date to today's date 
            user.GHI_Portal_Delegate_Attestation_Date__c = system.today();
            //Hide delegate attestation popup 
            delegateShow = false;
            try{ 
                //Update user record 
                update user;
            }catch(Exception e){ 
                Logger.debugException(e);
                System.debug('@@Error: ' + e);
            }
        //Set flag if not all delegate attestation checkboxes were ticked 
        }else if(delegateShow){
            notAllChecked = TRUE;
        }
    }       
}