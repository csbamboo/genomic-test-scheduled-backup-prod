@isTest
public class GHI_Portal_Controller_Test{
    
    @testsetup 
    private static void setup() {
    
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
        Account testAcct = GHI_Portal_TestUtilities.createAccountHCO(); 
        insert testAcct;
        
        Contact testContact = GHI_Portal_TestUtilities.createContactHCP(testAcct.Id); 
        insert testContact; 
        
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        portalUser.GHI_Portal_Order_Workflow__c = 'Intl-Non Partner';
        portalUser.Country = 'United Kingdom';
        portalUser.ContactId = testContact.Id;
        insert portalUser;
         
        OSM_Delegate__c delegate = GHI_Portal_TestUtilities.createDelegateAsDelegate(testAcct.Id, portalUser.ContactId);
        insert delegate;
        
        Account testAcct2 = GHI_Portal_TestUtilities.createDefaultAcc(); 
        insert testAcct2;
        
        Customer_Affiliation__c testCA = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(testAcct.Id, portalUser.ContactId, 'Primary'); 
        insert testCA;  
        
        Customer_Affiliation__c testCA2 = GHI_Portal_TestUtilities.createCNTtoCNTAffiliation(portalUser.ContactId, testContact.Id); 
        insert testCA2; 
        
        Customer_Affiliation__c testCA3 = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(testAcct.Id, testContact.Id, 'Primary'); 
        insert testCA3; 
        
        GHI_Portal_Settings__c customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
        insert customSetting;
        
        Record_Type__c patientRT = new Record_Type__c(Contact_Patient_Default_Account__c=testAcct.Id,
                                                      Contact_Patient_Record_Type__c='012K00000004vRW',
                                                      OSM_Order_Account_Default__c=testAcct2.Id);
        insert patientRT;
        
        GHI_Internal__c internal = new GHI_Internal__c ();
        
        internal.Name = 'GHI Internal';
        insert internal;
        
        GHI_Portal_Okta__c supportConfig = GHI_Portal_Okta__c.getOrgDefaults();
        insert supportConfig;
        
    } 
    }
    @isTest
    private static void controllerTest() {
    

        Test.startTest();
        
        List<Attachment> Attch = new List<Attachment>();
        List<GHI_Portal_Alerts_and_Announcement__c> alertsAndAnnouncements = new  List<GHI_Portal_Alerts_and_Announcement__c>();
        insert Attch;
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        
        GHI_Portal_Static_Content__c termscon = new GHI_Portal_Static_Content__c(
                GHI_Portal_Content_Type__c = 'Terms and Conditions',
                CurrencyIsoCode = 'USD'
                );
        insert termscon;
        System.debug('##portalCons ' + termscon);
        GHI_Portal_Static_Content__c termsList = new GHI_Portal_Static_Content__c(
                GHI_Portal_Content_Type__c = 'Delegate Attestation',
                CurrencyIsoCode = 'USD'
                );
        insert termsList;
        System.debug('##portalList ' + termsList);        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 

        System.runAs(portalUser) {
            
            // Contact delegcon = [SELECT Id, RecordTypeId, Name FROM Contact WHERE Id = :portalUser.ContactId];
            // System.debug('##contactDel ' + delegcon);
            // OSM_Delegate__c dlgt = [SELECT id, OSM_Contact__c, OSM_HCP__c
            //                       from OSM_Delegate__c where OSM_Contact__c = :portalUser.ContactId];
            // System.debug('##Del111 ' + delegcon);
            

            
            Account portalOrderAcc = new Account(
                RecordTypeId = accRecId,
                Name = 'Default Ordering Account'
                );
            insert portalOrderAcc;
            
            //Contact newContact = createContactHCP(portalOrderAcc.Id); 
            
            OSM_Delegate__c delegate = new OSM_Delegate__c(
            Name = 'Test Delegate', 
            //OSM_HCP__c = newContact.Id, 
            OSM_HCO__c = portalOrderAcc.Id, 
            OSM_Contact__c = portalUser.ContactId
            ); 
            insert delegate;
            
            Order portalOrder = new Order(
                 RecordTypeId = orderRecType,
                 AccountId = portalOrderAcc.Id,
                 //Account = portalOrderAcc,
                 Name = 'TestPortalOrder',
                 OSM_Study_ID__c = '123',
                 //OSM_Study_Name__c = 'test study name', 
                 //OSM_Patient__c = portalPatient.Id,
                 OSM_Product__c = 'Prostate',
                 OSM_Triage_Outcome__c = 'New', 
                 GHI_Portal_Associated_Requisition__c = 'Requisition',
                 GHI_Portal_CurrentStep__c = 1, 
                 Order_Location__c = 'Domestic', 
                 OSM_Bill_Type__c = 'Patient Pre-Pay',
                //  OSM_Related_Order__c = ,
                //  OSM_Related_Order__r.OrderNumber = ,
                 OSM_Patient_MRN__c = 'MRN',
                 Status = 'New',
                 OSM_Status__c = 'Order Intake',
                 EffectiveDate = Date.today()
             );
            Insert portalOrder;

            PageReference pageRef = Page.GHI_Portal_Home;
            //pageRef.getParameters().put('logo', 'logo');          
            Test.setCurrentPage(pageRef);
            
         GHI_Portal_Controller controller = new GHI_Portal_Controller();
         controller.displayPopup = false;
            
         List<Order> ROT = new List<Order>();
         ROT = controller.getRecentOrdersToday();
         controller.setRecentOrdersToday(ROT);

         List<Order> ROY = new List<Order>();
         ROY = controller.getRecentOrdersYesterday();
         controller.setRecentOrdersYesterday(ROY);
         
         List<Order> ROS = new List<Order>();
         ROS = controller.getRecentOrdersOlder();
         controller.setRecentOrdersOlder(ROS);
         
         controller.showNextRecordsForRecentOrdersToday();
         controller.showPreviousRecordsForRecentOrdersToday();
         controller.showFirstRecordsForRecentOrdersToday();
         controller.showLastRecordsForRecentOrdersToday();
         controller.getHasNextRecordsForRecentOrdersToday();
         controller.getHasPreviousRecordsForRecentOrdersToday();
         
         controller.showNextRecordsForRecentOrdersYesterday();
         controller.showPreviousRecordsForRecentOrdersYesterday();
         controller.showFirstRecordsForRecentOrdersYesterday();
         controller.showLastRecordsForRecentOrdersYesterday();
         controller.getHasNextRecordsForRecentOrdersYesterday();
         controller.getHasPreviousRecordsForRecentOrdersYesterday();
        
         controller.showNextRecordsForRecentOrdersOlder();
         controller.showPreviousRecordsForRecentOrdersOlder();
         controller.showFirstRecordsForRecentOrdersOlder();
         controller.showLastRecordsForRecentOrdersOlder();
         controller.getHasNextRecordsForRecentOrdersOlder();
         controller.getHasPreviousRecordsForRecentOrdersOlder();
         
         controller.setAlertsAndAnnouncements(alertsAndAnnouncements);
         controller.goToHome();
         controller.goToOrderInProgress();
         controller.viewAllOrders();
         controller.viewOrderDetails();
         controller.goToAddressBook();
         controller.goToLogIn();
         controller.goToHelp();
         controller.goToHowToOrder();
         controller.goToMyAccount();
         controller.goToInsurance();
         controller.goToHelpSpecimen();
         controller.goToAddEditContact();
         controller.goToAddEditLocation();
         controller.displayOrderDetail();
         
         controller.showNextRecordsForAlertsAndAnnouncements();
         controller.showPreviousRecordsForAlertsAndAnnouncements();
         controller.showFirstRecordsForAlertsAndAnnouncements();
         controller.showLastRecordsForAlertsAndAnnouncements();
         controller.getHasNextRecordsForAlertsAndAnnouncements();
         controller.getHasPreviousRecordsForAlertsAndAnnouncements();
         controller.displayPopup = true;
         controller.closePopup();
         
         controller.getTimeZoneValue();
         
         controller.logout();
         
         
         
        }
        
        Test.stopTest(); 
        
    }
    
    
    @isTest
    private static void controllerTest2() {
    
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
        Test.startTest();
        
        List<Attachment> Attch = new List<Attachment>();
        List<GHI_Portal_Alerts_and_Announcement__c> alertsAndAnnouncements = new  List<GHI_Portal_Alerts_and_Announcement__c>();
        insert Attch;
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        
        GHI_Portal_Static_Content__c termscon = new GHI_Portal_Static_Content__c(
                GHI_Portal_Content_Type__c = 'Terms and Conditions',
                CurrencyIsoCode = 'USD'
                );
        insert termscon;
        System.debug('##portalCons ' + termscon);
        GHI_Portal_Static_Content__c termsList = new GHI_Portal_Static_Content__c(
                GHI_Portal_Content_Type__c = 'Delegate Attestation',
                CurrencyIsoCode = 'USD'
                );
        insert termsList;
        System.debug('##portalList ' + termsList);        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 
        portalUser.GHI_Portal_Must_Agree_to_Terms__c = false;
        update portalUser;
        System.runAs(portalUser) {
            
            // Contact delegcon = [SELECT Id, RecordTypeId, Name FROM Contact WHERE Id = :portalUser.ContactId];
            // System.debug('##contactDel ' + delegcon);
            // OSM_Delegate__c dlgt = [SELECT id, OSM_Contact__c, OSM_HCP__c
            //                       from OSM_Delegate__c where OSM_Contact__c = :portalUser.ContactId];
            // System.debug('##Del111 ' + delegcon);
            

            
            Account portalOrderAcc = new Account(
                RecordTypeId = accRecId,
                Name = 'Default Ordering Account'
                );
            insert portalOrderAcc;
            
            //Contact newContact = createContactHCP(portalOrderAcc.Id); 
            
            OSM_Delegate__c delegate = new OSM_Delegate__c(
            Name = 'Test Delegate', 
            //OSM_HCP__c = newContact.Id, 
            OSM_HCO__c = portalOrderAcc.Id, 
            OSM_Contact__c = portalUser.ContactId
            ); 
            insert delegate;
            
            Order portalOrder = new Order(
                 RecordTypeId = orderRecType,
                 AccountId = portalOrderAcc.Id,
                 //Account = portalOrderAcc,
                 Name = 'TestPortalOrder',
                 OSM_Study_ID__c = '123',
                 //OSM_Study_Name__c = 'test study name', 
                 //OSM_Patient__c = portalPatient.Id,
                 OSM_Product__c = 'Prostate',
                 OSM_Triage_Outcome__c = 'New', 
                 GHI_Portal_Associated_Requisition__c = 'Requisition',
                 GHI_Portal_CurrentStep__c = 1, 
                 Order_Location__c = 'Domestic', 
                 OSM_Bill_Type__c = 'Patient Pre-Pay',
                //  OSM_Related_Order__c = ,
                //  OSM_Related_Order__r.OrderNumber = ,
                 OSM_Patient_MRN__c = 'MRN',
                 Status = 'New',
                 OSM_Status__c = 'Order Intake',
                 EffectiveDate = Date.today()
             );
            Insert portalOrder;

            PageReference pageRef = Page.GHI_Portal_Home;
            //pageRef.getParameters().put('logo', 'logo');          
            Test.setCurrentPage(pageRef);
            
         GHI_Portal_Controller controller = new GHI_Portal_Controller();
         controller.displayPopup = false;
            
         List<Order> ROT = new List<Order>();
         ROT = controller.getRecentOrdersToday();
         controller.setRecentOrdersToday(ROT);

         List<Order> ROY = new List<Order>();
         ROY = controller.getRecentOrdersYesterday();
         controller.setRecentOrdersYesterday(ROY);
         
         List<Order> ROS = new List<Order>();
         ROS = controller.getRecentOrdersOlder();
         controller.setRecentOrdersOlder(ROS);
         
         controller.showNextRecordsForRecentOrdersToday();
         controller.showPreviousRecordsForRecentOrdersToday();
         controller.showFirstRecordsForRecentOrdersToday();
         controller.showLastRecordsForRecentOrdersToday();
         controller.getHasNextRecordsForRecentOrdersToday();
         controller.getHasPreviousRecordsForRecentOrdersToday();
         
         controller.showNextRecordsForRecentOrdersYesterday();
         controller.showPreviousRecordsForRecentOrdersYesterday();
         controller.showFirstRecordsForRecentOrdersYesterday();
         controller.showLastRecordsForRecentOrdersYesterday();
         controller.getHasNextRecordsForRecentOrdersYesterday();
         controller.getHasPreviousRecordsForRecentOrdersYesterday();
        
         controller.showNextRecordsForRecentOrdersOlder();
         controller.showPreviousRecordsForRecentOrdersOlder();
         controller.showFirstRecordsForRecentOrdersOlder();
         controller.showLastRecordsForRecentOrdersOlder();
         controller.getHasNextRecordsForRecentOrdersOlder();
         controller.getHasPreviousRecordsForRecentOrdersOlder();
         
         controller.setAlertsAndAnnouncements(alertsAndAnnouncements);
         controller.goToHome();
         controller.goToOrderInProgress();
         controller.viewAllOrders();
         controller.viewOrderDetails();
         controller.goToAddressBook();
         controller.goToLogIn();
         controller.goToHelp();
         controller.goToHowToOrder();
         controller.goToMyAccount();
         controller.goToInsurance();
         controller.goToHelpSpecimen();
         controller.goToAddEditContact();
         controller.goToAddEditLocation();
         controller.displayOrderDetail();
         
         controller.showNextRecordsForAlertsAndAnnouncements();
         controller.showPreviousRecordsForAlertsAndAnnouncements();
         controller.showFirstRecordsForAlertsAndAnnouncements();
         controller.showLastRecordsForAlertsAndAnnouncements();
         controller.getHasNextRecordsForAlertsAndAnnouncements();
         controller.getHasPreviousRecordsForAlertsAndAnnouncements();
         controller.displayPopup = false;
         controller.closePopup();
         controller.dateTimeValue = system.today();
         controller.isDateOnly = true;
         controller.getTimeZoneValue();
         
         controller.logout();
         
         
         
        }
        
        Test.stopTest(); 
    }    
    }
    
}