/*------------------------------------------------------------------------
    Author:         Stephen James Laylo
    Company:        Cloud Sherpas
    Description:    A controller for DTLocaleFormatter component 

    History
    <Date>          <Authors Name>          <Brief Description of Change>
    03/05/2015      Stephen James Laylo     Created this controller
---------------------------------------------------------------------------*/
public class GHI_Portal_DTLocaleFormatter_Controller {
    public DateTime dateTimeValue { get; set; }
    public Boolean isDateOnly { get; set; }
    
    public String getTimeZoneValue() { 
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       MAR-05-2015
        * Description   Formats Date or DateTime according to User's locale
        ----------------------------------------------------------------------- */
        User currentUser = GHI_Portal_Utilities.getCurrentUser(); 
        
        if (dateTimeValue != null) {
            String localeFormatDT;
            //If only the Date part is needed 
            if (isDateOnly) {
        		/**
            	String[] arrStrDate = new List<String>(); 
            	//Get Date part from DateTime value 
            	if(dateTimeValue != null) {
            		arrStrDate = (dateTimeValue + '').split(' ')[0].split('-');
            	}
            	//Create new instance of Date 
				Date newDate = Date.newInstance(Integer.valueOf(arrStrDate[0]), Integer.valueOf(arrStrDate[1]), Integer.valueOf(arrStrDate[2]));
				 **/
				//Format the date according to User's locale
				Date newDate = dateTimeValue.date();
				localeFormatDT = newDate.format();

                return localeFormatDT;
            //Else if Date and Time are needed 
            } else {
                //Format the dateTime value according to User's locale 
            	localeFormatDT = dateTimeValue.format();
            	
            	return localeFormatDT;
            }
        }
        return null;
    }
}