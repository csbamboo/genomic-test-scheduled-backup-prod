@isTest
public class GHI_Portal_DTLocaleFormatter_CtrlrTest{
  
	static testmethod void myUnitTest() {                 
		Test.startTest(); 
		GHI_Portal_DTLocaleFormatter_Controller c = new GHI_Portal_DTLocaleFormatter_Controller();        
		c.dateTimeValue = System.Now();
		c.isDateOnly = TRUE;
		c.getTimeZoneValue();   
		c.isDateOnly = FALSE;
		c.getTimeZoneValue();  
		c.dateTimeValue = null;
		c.getTimeZoneValue();                 
		Test.stopTest();   
	}
}