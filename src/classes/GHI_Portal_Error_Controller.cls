/*------------------------------------------------------------------------
    Author:        Andrew Castillo
    Company:       Cloud Sherpas
    Description:   A controller class created for the default error page of the portal
                  
    Test Class:
    History:
    <Date>          <Authors Name>         <Brief Description of Change>
    03/24/2015      Andrew Castillo       Created   
--------------------------------------------------------------------------*/
public class GHI_Portal_Error_Controller
{
	public GHI_Portal_Error_Controller()
	{
		
	}
	
	public PageReference goToHome() 
	{
        PageReference pr = Page.GHI_Portal_Home; 
        pr.setRedirect(true);
        
        return pr;
    }
}