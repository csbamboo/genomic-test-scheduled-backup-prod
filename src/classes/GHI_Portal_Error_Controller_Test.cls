@isTest
public class GHI_Portal_Error_Controller_Test{
    
     @testsetup 
     static void setup() {
     	User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
     }    
     
     @isTest
     static void controllerTest(){ 	        
 	    test.startTest();
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    
     	System.runAs(portalUser) {
     		PageReference pageRef = Page.GHI_Portal_Error;
     		test.setCurrentPage(pageRef);
     		String logo;
     		System.currentPageReference().getParameters().put('logo', logo);

     		
	     	GHI_Portal_Error_Controller controller = new GHI_Portal_Error_Controller();
            controller.goToHome();
            
	     	
	     	 
     	}
     	
     	test.stopTest(); 

     }
    
}