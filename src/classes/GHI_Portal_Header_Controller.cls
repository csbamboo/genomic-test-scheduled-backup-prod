public class GHI_Portal_Header_Controller {
	public GHI_Portal_Okta__c orgDefaults; 
	public String calcToolStr {get;set;} 
	public String speakerPortalStr {get;set;}
	public String documentsStr {get;set;}
	
	public GHI_Portal_Header_Controller() {
		this.orgDefaults = GHI_Portal_Okta__c.getOrgDefaults();
		this.calcToolStr = orgDefaults.GHI_Portal_Link_Calculation_Tools_Tab__c; 
		this.speakerPortalStr = orgDefaults.GHI_Portal_Link_Speaker_Portal_Tab__c; 
		this.documentsStr = orgDefaults.GHI_Portal_Link_Box_Tab__c; 
	}
}