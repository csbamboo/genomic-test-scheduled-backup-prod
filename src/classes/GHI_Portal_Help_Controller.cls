/*------------------------------------------------------------------------
    Author:        Sairah Hadjinoor
    Company:       Cloud Sherpas
    Description:   A controller class created for Help Page
                  
    Test Class:
    History:
    <Date>          <Authors Name>         <Brief Description of Change>
    03/24/2015      Sairah Hadjinoor       Created
    03/25/2015      Andrew Castillo        Added Terms of Use 
    03/27/2015      Sairah Hadjinoor       Transferred Code from GHI_Portal_Help
    04/01/2015      Andrew Castillo        Added localization 
--------------------------------------------------------------------------*/
public with sharing class GHI_Portal_Help_Controller { 
    public User user {get;set;} 
    public String param{get;set;}
    public List<GHI_Portal_Static_Content__c> portalStaticContent{get;set;} 
    public List<Attachment> portalStaticDocs{get;set;} 
    public Boolean hasAdditionalContent { get; set; }
    public Boolean isGenInfo { get; set; }  
    public Boolean hasAttachs { get; set; }
    public GHI_Portal_Static_Content__c portalContentRec{get;set;}
    private String contentType;
    private String contentName;
    
    /*-----------------------------------------------------------------------
    * Author        Sairah Hadjinoor
    * Company       Cloud Sherpas
    * History       03/24/2015
    * Description   Page constructor
    ----------------------------------------------------------------------- */
    public GHI_Portal_Help_Controller()
    {
        isGenInfo = false;
        this.user = GHI_Portal_Utilities.getCurrentUser(); 
        
        // get page reference
        param = ApexPages.currentPage().getParameters().get('page');
        system.debug('@@param '+param);
        if(param == 'help'){
            isGenInfo = true;
            contentType = 'General Information';
        }
        else if(param == 'order'){
            contentType = 'How to Place an Online Order';
        }
        else if(param == 'specimen'){
            contentType = 'How to Submit a Specimen';
        }
        else if(param == 'insurance'){
            contentType = 'Insurance Coverage';
        }
        else if(param == 'term'){
            contentType = 'Terms of Use';
        }
        else{
            isGenInfo = true;
            contentType = 'General Information';
            param = 'help';
        }
        
        // query for static content for page referenced
        portalStaticContent = [SELECT Name, GHI_Portal_Content_Type__c, GHI_Portal_Locale__c, GHI_Portal_Static_Content__c 
                               FROM GHI_Portal_Static_Content__c 
                               WHERE GHI_Portal_Content_Type__c =:contentType
                               AND GHI_Portal_Insurance_Company__c = null
                               AND GHI_Portal_Locale__c = :GHI_Portal_Utilities.getUserLocale() LIMIT 1];
        
        
        // query for attachments assigned to the content record
        if(portalStaticContent.size()!=0){
            portalContentRec =portalStaticContent[0];
             
            portalStaticDocs = [SELECT Id, Description, Name 
                                FROM Attachment 
                                WHERE ParentId=:portalStaticContent[0].Id
                                ORDER BY Name];
            
            // check if there are attachments               
            if (portalStaticDocs.size() > 0)
            {
                hasAttachs = true;
            }
        }
    }
    
    public PageReference goToHelp() {
        //ApexPages.currentPage().getParameters().put('page', 'help');
        //param = ApexPages.currentPage().getParameters().get('page');
        
        PageReference pr = Page.GHI_Portal_Help;
        pr.getParameters().put('page', 'help');
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference goToHowToOrder() {
        //ApexPages.currentPage().getParameters().put('page', 'order');
        //param = ApexPages.currentPage().getParameters().get('page');
        PageReference pr = Page.GHI_Portal_Help;
        pr.getParameters().put('page', 'order');
        pr.setRedirect(true);
        return pr;
    }   
    public PageReference goToInsurance() {
        //ApexPages.currentPage().getParameters().put('page', 'insurance');
        //param = ApexPages.currentPage().getParameters().get('page');
        PageReference pr = Page.GHI_Portal_Help;
        pr.getParameters().put('page', 'insurance');
        pr.setRedirect(true);
        return pr;
    }
    public PageReference goToHelpSpecimen() {
        //ApexPages.currentPage().getParameters().put('page', 'specimen');
        //param = ApexPages.currentPage().getParameters().get('page');
        PageReference pr = Page.GHI_Portal_Help;
        pr.getParameters().put('page', 'specimen');
        pr.setRedirect(true);
        return pr;
    }
}