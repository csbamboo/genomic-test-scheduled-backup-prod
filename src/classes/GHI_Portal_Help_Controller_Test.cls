@isTest
public class GHI_Portal_Help_Controller_Test{
    
     @testsetup 
     static void setup() {
     	User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
     }    
     
     @isTest
     static void controllerTest(){ 	        
 	    test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    PageReference pageRef = Page.GHI_Portal_Help;
 	    test.setCurrentPage(pageRef);
     	System.runAs(portalUser) {
     		ApexPages.currentPage().getParameters().put('page', 'help');
     		
     		
     		
     		
     		
            //ApexPages.setCurrentPage.getParameters().put('param', 'help');
            System.debug('*parameterValue ' + pageRef);
     		//ApexPages.currentPage().getParameters().put('
     		
	     	GHI_Portal_Help_Controller controller = new GHI_Portal_Help_Controller();


	     	controller.goToHelpSpecimen();
	     	controller.goToInsurance();
	     	controller.goToHowToOrder();
	     	controller.goToHelp();
     	}

     	test.stopTest(); 

     }
     @isTest
     static void controllerTest2(){ 	        
 	    test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    PageReference pageRef = Page.GHI_Portal_Help;
 	    test.setCurrentPage(pageRef);
     	System.runAs(portalUser) {
     		ApexPages.currentPage().getParameters().put('page', 'order');
     		
     		
     		
     		
     		
            //ApexPages.setCurrentPage.getParameters().put('param', 'help');
            System.debug('*parameterValue ' + pageRef);
     		//ApexPages.currentPage().getParameters().put('
     		
	     	GHI_Portal_Help_Controller controller = new GHI_Portal_Help_Controller();


	     	controller.goToHelpSpecimen();
	     	controller.goToInsurance();
	     	controller.goToHowToOrder();
	     	controller.goToHelp();
     	}

     	test.stopTest(); 

     }
     @isTest
     static void controllerTest3(){ 	        
 	    test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    PageReference pageRef = Page.GHI_Portal_Help;
 	    test.setCurrentPage(pageRef);
     	System.runAs(portalUser) {
     		ApexPages.currentPage().getParameters().put('page', 'specimen');
     		
     		
     		
     		
     		
            //ApexPages.setCurrentPage.getParameters().put('param', 'help');
            System.debug('*parameterValue ' + pageRef);
     		//ApexPages.currentPage().getParameters().put('
     		
	     	GHI_Portal_Help_Controller controller = new GHI_Portal_Help_Controller();


	     	controller.goToHelpSpecimen();
	     	controller.goToInsurance();
	     	controller.goToHowToOrder();
	     	controller.goToHelp();
     	}

     	test.stopTest(); 

     }
     @isTest
     static void controllerTest4(){ 	        
 	    test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    PageReference pageRef = Page.GHI_Portal_Help;
 	    test.setCurrentPage(pageRef);
     	System.runAs(portalUser) {
     		ApexPages.currentPage().getParameters().put('page', 'insurance');
     		
     		
     		
     		
     		
            //ApexPages.setCurrentPage.getParameters().put('param', 'help');
            System.debug('*parameterValue ' + pageRef);
     		//ApexPages.currentPage().getParameters().put('
     		
	     	GHI_Portal_Help_Controller controller = new GHI_Portal_Help_Controller();


	     	controller.goToHelpSpecimen();
	     	controller.goToInsurance();
	     	controller.goToHowToOrder();
	     	controller.goToHelp();
     	}

     	test.stopTest(); 

     }
      @isTest
     static void controllerTest5(){ 	        
 	    test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    PageReference pageRef = Page.GHI_Portal_Help;
 	    test.setCurrentPage(pageRef);
     	System.runAs(portalUser) {
     		ApexPages.currentPage().getParameters().put('page', '');
     		
     		
     		
     		
     		
            //ApexPages.setCurrentPage.getParameters().put('param', 'help');
            System.debug('*parameterValue ' + pageRef);
     		//ApexPages.currentPage().getParameters().put('
     		
	     	GHI_Portal_Help_Controller controller = new GHI_Portal_Help_Controller();


	     	controller.goToHelpSpecimen();
	     	controller.goToInsurance();
	     	controller.goToHowToOrder();
	     	controller.goToHelp();
     	}

     	test.stopTest(); 

     }
     @isTest
     static void controllerTest6(){ 	        
 	    test.startTest(); 
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 
 	    PageReference pageRef = Page.GHI_Portal_Help;
 	    test.setCurrentPage(pageRef);
     	System.runAs(portalUser) {
     		ApexPages.currentPage().getParameters().put('page', 'term');
     		
     		
     		
     		
     		
            //ApexPages.setCurrentPage.getParameters().put('param', 'help');
            System.debug('*parameterValue ' + pageRef);
     		//ApexPages.currentPage().getParameters().put('
     		
	     	GHI_Portal_Help_Controller controller = new GHI_Portal_Help_Controller();


	     	controller.goToHelpSpecimen();
	     	controller.goToInsurance();
	     	controller.goToHowToOrder();
	     	controller.goToHelp();
     	}

     	test.stopTest(); 

     }

     /******************************************************************************
        Author         : Mark Cagurong
        Description    : Test Initialization for static content and attachment coverage
        History
            <date>                <author>                <description>
            31-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
     @isTest
     static void controllerTest_AttachmentTest(){             
        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1]; 

        GHI_Portal_Static_Content__c   staticContent = new GHI_Portal_Static_Content__c();
        staticContent.GHI_Portal_Static_Content__c = 'The quick brown fox jumps over the lazy dog';
        staticContent.GHI_Portal_Content_Type__c = 'General Information';
        staticContent.GHI_Portal_Locale__c  = 'en_US';
        staticContent.GHI_Portal_Insurance_Company__c  = null;
        staticContent.GHI_Portal_Content_Type__c = 'General Information';

        insert staticContent;

        Attachment staticAttachment = OSM_DataFactory.createAttachment(staticContent.ID);
        insert staticAttachment;

        PageReference pageRef = Page.GHI_Portal_Help;
        test.setCurrentPage(pageRef);
        System.runAs(portalUser) {
            ApexPages.currentPage().getParameters().put('page', 'help');            
            
            Test.startTest();
            GHI_Portal_Help_Controller controller = new GHI_Portal_Help_Controller();
            Test.stopTest();

            // stub for code coverage
            controller.hasAdditionalContent = true;

            System.assertEquals(staticContent.Id, controller.portalContentRec.Id);
            System.assertEquals(true, controller.hasAttachs);
            System.assertEquals(1, controller.portalStaticDocs.size());
        }        

     }
}


// Test.setCurrentPage(Page.ConvertLead);
//       //TestDataGenerator.setupPriceBooksEntryAndProducts('Deposit', 'Deposits',  leadSSCOrecType, opportunitySSCOAcquisitionrectype);

//       System.runAs(adminUser) {
//           Lead newLead = TestDataGenerator.createTestLeads('First', 'Last', 'firstlast@email.com', '', '', 'Internal BFS', 'Company', 'Trading', 'Accounting', RecordFinder.RECORDTYPEID_LEAD_SSCO_SALES, 1)[0];
//           insert newLead;
//           ApexPages.currentPage().getParameters().put('leadid', newLead.Id);