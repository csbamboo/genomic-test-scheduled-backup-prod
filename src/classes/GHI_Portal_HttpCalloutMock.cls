/*
 *  @author         Raus Kenneth Ablaza
 *  @description    Used for HTTP mock callout in test classes ; 
 *                  reference: https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_callouts_wsdl2apex_testing.htm
 *  @date           03 August 2015
 */

@isTest
global class GHI_Portal_HttpCalloutMock implements HttpCalloutMock {
    
    // private Integer STATUS_OKAY_CODE = 202;
    private Integer STATUS_OKAY_CODE = 200;
    private Integer STATUS_FAIL_CODE = 404;
    private Integer STATUS_CODE;
    private String STATUS_OKAY = 'OKAY';
    private String STATUS_FAIL = 'FAIL';
    private String STATUS;
    private Boolean STATUS_FLAG;
    
    global GHI_Portal_HttpCalloutMock(Boolean reqStatus){
        this.STATUS_FLAG = reqStatus;
        this.STATUS_CODE = (reqStatus ? this.STATUS_OKAY_CODE : this.STATUS_FAIL_CODE);
        this.STATUS = (reqStatus ? this.STATUS_OKAY : this.STATUS_FAIL);
    }
    
    global httpResponse respond(HTTPrequest req){
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(this.STATUS_CODE);
        res.setStatus(this.STATUS);
        res.setBody((this.STATUS_FLAG ? GHI_Portal_OktaProfile.testResult() : GHI_Portal_OktaErrorResponse.testParse()));
        return res;
    }

}