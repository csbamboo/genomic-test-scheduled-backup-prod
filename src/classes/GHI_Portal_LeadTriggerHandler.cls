/*------------------------------------------------------------------------
    Author:        Andrew Castillo
    Company:       Cloud Sherpas
    Description:   Trigger Handler for Lead object
                  
    Test Class:
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    13.MAR.2015     Andrew Castillo         Created
    16.MAR.2015     Sairah Hadjinoor        Updated - code optimization 
                                            Updated - added my location trigger
--------------------------------------------------------------------------*/

public with sharing class GHI_Portal_LeadTriggerHandler 
{
    public static void onAfterInsert(List<Lead> newLeadList){   
        Map<Id,Id> portalUserIdContactIdMap = new Map<Id,Id>();
        Map<Id,Id> portalUserIdAccountIdMap = new Map<Id,Id>();
        
        //loop through the new leads
        if(newLeadList.size()!=0){ 
            for(Lead leadRec: newLeadList){
                if(leadRec.LeadSource == Label.GHI_Portal_Portal && leadRec.GHI_Portal_Community_User__c!=null && leadRec.GHI_Portal_Community_User__c!=''){
                    //create a map that will hold the ghi portal user Id and the corresponding convertedcontactId of the new lead
                    if(leadRec.GHI_Portal_IsLocation__c){
                        system.debug('@@leadRec.GHI_Portal_IsLocation__c '+leadRec.GHI_Portal_IsLocation__c);
                        if(leadRec.ConvertedAccountId!=null){
                            portalUserIdAccountIdMap.put(leadRec.GHI_Portal_Community_User__c, leadRec.ConvertedAccountId);
                        }
                    }
                    else{
                        if(leadRec.ConvertedContactId!=null){
                            portalUserIdContactIdMap.put(leadRec.GHI_Portal_Community_User__c, leadRec.ConvertedContactId); 
                        }
                    }
                }
            }   
        }
        system.debug('@@portalUserIdAccountIdMap '+portalUserIdAccountIdMap);
        if(!portalUserIdContactIdMap.isEmpty()){
            createCustomerContactAffiliation(portalUserIdContactIdMap);
        }
        if(!portalUserIdAccountIdMap.isEmpty()){
            createCustomerLocationAffiliation(portalUserIdAccountIdMap);
        }
    }
    
    /*-----------------------------------------------------------------------
        * Author        Andrew Castillo
        * Company       Cloud Sherpas
        * History       MAR-18-2015
        * Description   Method creates the association of contact to contact used for My Contacts
        ----------------------------------------------------------------------- */
    public static void createCustomerContactAffiliation(Map<Id,Id> portalUserIdContactIdMap){
        List<Customer_Affiliation__c> customerAffiliationList = new List<Customer_Affiliation__c>();
        Id CC_RecordTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get(Label.GHI_Portal_CNT_RecordTypeId).getRecordTypeId();
        //check if the map is not empty
        if(!portalUserIdContactIdMap.isEmpty()){
            //lop through the user record to get the corresponding contact id of the user
            //then create a noew customer affiliation record based on the contactId of user 
            //and their corresponding converted  contact Id based on the portalUserIdContactIdMap
            for(User userRec: [Select ContactId from User where Id IN: portalUserIdContactIdMap.keySet()]){
                Customer_Affiliation__c custAffiliation = new Customer_Affiliation__c();
                custAffiliation.OSM_Contact_1__c = userRec.ContactId;
                custAffiliation.OSM_Contact_2__c = portalUserIdContactIdMap.get(userRec.Id);
                custAffiliation.RecordTypeId = CC_RecordTypeId;
                custAffiliation.GHI_Portal_Display_in_Portal__c = true;
                customerAffiliationList.add(custAffiliation);
            }
        }
        //check if customer affiliation is not empty
        //then insert if it has values
        if(!customerAffiliationList.isEmpty()){
            insert customerAffiliationList;
        }
    }
    
    /*-----------------------------------------------------------------------
        * Author        Andrew Castillo
        * Company       Cloud Sherpas
        * History       MAR-18-2015
        * Description   Method creates the association of contact to acount used for My Locations
        ----------------------------------------------------------------------- */
    public static void createCustomerLocationAffiliation(Map<Id,Id> portalUserIdAccountIdMap){
        List<Customer_Affiliation__c> customerAffiliationList = new List<Customer_Affiliation__c>();
        system.debug('@@portalUserIdAccountIdMap '+portalUserIdAccountIdMap);
        Id ACCT_RecordTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get(Label.GHI_Portal_ACCT_RecordTypeId).getRecordTypeId();
        //check if the map is not empty
        if(!portalUserIdAccountIdMap.isEmpty()){
            //lop through the user record to get the corresponding contact id of the user
            //then create a new customer affiliation record based on the contactId of user 
            //and their corresponding converted  contact Id based on the portalUserIdContactIdMap
            for(User userRec: [Select ContactId from User where Id IN: portalUserIdAccountIdMap.keySet()]){
                system.debug('@@userRec '+userRec);
                Customer_Affiliation__c custAffiliation = new Customer_Affiliation__c();
                custAffiliation.OSM_Contact_1__c = userRec.ContactId;
                custAffiliation.OSM_Account_1__c = portalUserIdAccountIdMap.get(userRec.Id);
                custAffiliation.RecordTypeId = ACCT_RecordTypeId;
                custAffiliation.GHI_Portal_Display_in_Portal__c = true;
                customerAffiliationList.add(custAffiliation);
            }
        }
        //check if customer affiliation is not empty
        //then insert if it has values
        system.debug('@@customerAffiliationList '+customerAffiliationList);
        if(!customerAffiliationList.isEmpty()){
            insert customerAffiliationList;
        }
    }
}