/*
  @author: Amanpreet Singh Sidhu 
  @date: 21 June 2015
  @description: Test Class for GHI_Portal_LeadTriggerHandler apex class
  @history: 21 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest(seeAllData = true)
//@isTest
public class GHI_Portal_LeadTriggerHandler_Test 
{
  
    static testMethod void testConvertedAccount() 
    {       
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    
        System.runAs(thisUser) 
        {
            
            TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
            triggerSwitch.Account_Trigger__c = false;
            triggerSwitch.Contact_Trigger__c = false;
            triggerSwitch.GHI_Portal_UserTrigger__c = false;
            update triggerSwitch;
            
            List<Lead> lstInsertLead = new List<Lead>();
            
            User portalUser = GHI_Portal_TestUtilities.createPortalUser();
            insert portalUser;
            
            String portalUserId = String.Valueof(portalUser.Id);
            
            Lead lead=new Lead(LastName='Doe',FirstName='John',Company='Test',Status='Inquiry',
                               LeadSource = 'Portal',GHI_Portal_Community_User__c = portalUserId,
                               GHI_Portal_IsLocation__c = true);
                               
            Lead lead2 =new Lead(LastName='Doe',FirstName='John',Company='Test',Status='Inquiry',
                               LeadSource = 'Portal',GHI_Portal_Community_User__c = portalUserId,
                               GHI_Portal_IsLocation__c = false);
            
            lstInsertLead.add(lead);
            lstInsertLead.add(lead2);                   
            insert lstInsertLead;                

            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(lead.id);
            lc.setDoNotCreateOpportunity(true);
            lc.setConvertedStatus('Qualified');

            Database.LeadConvertResult lcr = Database.convertLead(lc);
            
            Database.LeadConvert lc2 = new database.LeadConvert();
            lc2.setLeadId(lead2.id);
            lc2.setDoNotCreateOpportunity(true);
            lc2.setConvertedStatus('Qualified');

            Database.LeadConvertResult lcr2 = Database.convertLead(lc2);         
        }   
    }
    
}