@isTest
global class GHI_Portal_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://genomichealthonline.oktapreview.com/api/v1', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(getResponse());
        res.setStatusCode(200);
        return res;
    }
    
    private String getResponse() {
		String json = '{'+
		'	\"id\":\"12345dsd3w\",'+
		'	\"status\":\"STAGED\",'+
		'	\"created\":\"2015-04-13T17:44:53.000Z\",'+
		'	\"activated\":null,'+
		'	\"statusChanged\":null,'+
		'	\"lastLogin\":null,'+
		'	\"lastUpdated\":\"2015-04-13T17:44:53.000Z\",'+
		'	\"passwordChanged\":null,'+
		'	\"profile\":'+
		'	{'+
		'		\"email\":\"test@email.com\",'+
		'		\"firstName\":\"Test\",'+
		'		\"lastName\":\"Last\",'+
		'		\"login\":\"tlast1@ghi.odx\",'+
		'		\"mobilePhone\":null,'+
		'		\"secondEmail\":null'+
		'	},'+
		'	\"credentials\":'+
		'	{'+
		'		\"provider\":'+
		'		{'+
		'			\"type\":\"OKTA\",'+
		'			\"name\":\"OKTA\"'+
		'		}'+
		'	},'+
		'	\"_links\":'+
		'	{'+
		'		\"activate\":'+
		'		{'+
		'			\"href\":\"https://test.com/api/v1/users/00u3rmjtiyUGt7xkv0h7/lifecycle/activate\",'+
		'			\"method\":\"POST\"'+
		'		},'+
		'		\"deactivate\":'+
		'		{'+
		'			\"href\":\"https://test.com/api/v1/users/00u3rmjtiyUGt7xkv0h7/lifecycle/deactivate\",'+
		'			\"method\":\"POST\"'+
		'		}'+
		'	}'+
		'}';
		return json;
	}
}