@isTest
public class GHI_Portal_OIPExt_Test{
    
     @testsetup 
     static void setup() {
     
          User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
            insert portalUser;  
                
            Account testAcct = GHI_Portal_TestUtilities.createAccountHCO(); 
            insert testAcct; 
            
            Account portalOrderAcc = GHI_Portal_TestUtilities.createDefaultAcc();
            insert portalOrderAcc;
            
            Customer_Affiliation__c testCA = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(
                testAcct.Id, portalUser.ContactId, 'Primary'); 
            insert testCA;  
            
            Contact testContact = GHI_Portal_TestUtilities.createContactHCP(testAcct.Id); 
            insert testContact; 
            
            Customer_Affiliation__c testCA2 = GHI_Portal_TestUtilities.createCNTtoCNTAffiliation(
                portalUser.ContactId, testContact.Id); 
            insert testCA2; 
            
            Customer_Affiliation__c testCA3 = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(
                testAcct.Id, testContact.Id, 'Primary'); 
            insert testCA3; 
            
            GHI_Portal_Settings__c customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
            insert customSetting;
            
            List<OSM_Orderable_Creation__c> orderable = GHI_Portal_TestUtilities.createOrderable();
            insert orderable;
            

     } 
     }
     @isTest
     static void controllerTest(){
         
        test.startTest();

        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];

        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();

        System.runAs(portalUser) {

            Account portalOrderAcc = new Account(
                RecordTypeId = accRecId,
                Name = 'Default Ordering Account'
                );
            insert portalOrderAcc;
            
            Order portalOrder = new Order(
                 RecordTypeId = orderRecType,
                 AccountId = portalOrderAcc.Id,
                 //Account = portalOrderAcc,
                 Name = 'TestPortalOrder',
                 OSM_Study_ID__c = '123',
                 //OSM_Study_Name__c = 'test study name', 
                 //OSM_Patient__c = portalPatient.Id,
                 OSM_Product__c = 'IBC',
                 OSM_Triage_Outcome__c = 'New', 
                 GHI_Portal_Associated_Requisition__c = 'Requisition',
                 GHI_Portal_CurrentStep__c = 1, 
                 Order_Location__c = 'Domestic', 
                 OSM_Bill_Type__c = 'Patient Pre-Pay',
                //  OSM_Related_Order__c = ,
                 OSM_Patient_MRN__c = 'MRN',
                 Status = 'New',
                 OSM_Status__c = 'Draft',
                 EffectiveDate = Date.today()
             );
            Insert portalOrder;
            System.debug('##OrderIdParam ' + portalOrder.OrderNumber);
            
            String dateString = string.valueOfGmt(Date.today() - 1);
            
            PageReference pageRef = Page.GHI_Portal_Order;
            test.setCurrentPage(pageRef);
            //System.currentPageReference().getParameters().put('id', portalOrder.id);
            
            System.debug('##OrderIdParam ' + portalOrder.Id);
            portalOrder = [SELECT Id, OrderNumber FROM Order WHERE Id = : portalOrder.Id];
            System.debug('##portalNum.OrderNumber ' + portalOrder.OrderNumber);
            // System.currentPageReference().getParameters().put('order-id', portalOrder.OrderNumber);
            // System.currentPageReference().getParameters().put('patient', 'P');
            // System.currentPageReference().getParameters().put('patient-mrn', '1');
            // System.currentPageReference().getParameters().put('ordering-physician', 'A');
            // System.currentPageReference().getParameters().put('patient-bdate', dateString);
            // System.currentPageReference().getParameters().put('order-date', dateString);
            System.currentPageReference().getParameters().put('search', 'test');
            //System.currentPageReference().getParameters().put('isadvancedsearch' , '1');
            
            System.debug('##DebugPageRef ' + pageRef);
            
            GHI_Portal_OrderInProgressExt cntrllr = new GHI_Portal_OrderInProgressExt();
            ApexPages.StandardController sc = new ApexPages.standardController(portalOrder);
            
            GHI_Portal_OrderInProgressExt controller = new GHI_Portal_OrderInProgressExt(sc);
            

            controller.initialize();
            controller.getCompletedOrders();
            controller.getPhysicians();
            controller.getPhysicianLocation();
            controller.recipient2();
            controller.getRecipients();
            controller.getRecipientLocation();
            controller.recipientOnchange();
            controller.getPathologyDepartment();
            controller.displayOrderDetail();
            //conrtoller.deleteSavedOrder();
            controller.getIcdOptions();
            controller.archiveOrder();
            controller.unarchiveOrder();
            controller.saveOrderId = portalOrder.Id;
            // controller.resubmitOrder();
            controller.getNodalStatus();
            controller.searchStr = 'O';         
            controller.searchOrder();
            controller.searchOrderInAdvanced();
            //controller.clonedOrderRoleFuture();
            //controller.changeSorter();

        controller.assignSorterAsOrderNumber();
        controller.selectedSorter = Label.GHI_Portal_SortByRequisition;
        controller.sortOrderByParam = Label.GHI_Portal_SortByRequisition;
        controller.changeSorter();

        controller.assignSorterAsPatient();
        controller.selectedSorter = Label.GHI_Portal_SortByPatient;
        controller.sortOrderByParam = Label.GHI_Portal_SortByPatient;
        controller.changeSorter();

        controller.assignSorterAsOrderingPhysician();
        controller.selectedSorter = Label.GHI_Portal_SortByOrderingPhysician;
        controller.sortOrderByParam = Label.GHI_Portal_SortByOrderingPhysician;
        controller.changeSorter();

        controller.assignSorterAsTestType();
        controller.selectedSorter = Label.GHI_Portal_SortByTestType;
        controller.sortOrderByParam = Label.GHI_Portal_SortByTestType;
        
        
            controller.changeSorterCO();
            controller.assignSorterAsOrderNumberCO();
            controller.assignSorterAsPatientCO();
            controller.assignSorterAsOrderingPhysicianCO();
            controller.assignSorterAsTestTypeCO();
            controller.assignSorterAsStatusCO();
            controller.assignSorterAsOrderDateCO();
            controller.changeSorterAO();
            controller.assignSorterAsOrderNumberAO();
            controller.assignSorterAsPatientAO();
            controller.assignSorterAsOrderingPhysicianAO();
            controller.assignSorterAsTestTypeAO();
            controller.assignSorterAsStatusAO();
            controller.assignSorterAsOrderDateAO();
            
            controller.goToHome();
            controller.goToLogIn();
            controller.goToOrderInProgress();
            controller.viewAllOrders();
            controller.viewOrderDetails();
            controller.goToNewOrderPatient();
            controller.goToNewOrderBilling();
            controller.goToNewOrderPhysician();
            controller.goToNewOrderSpecimen();
            controller.goToNewOrderReview();
            controller.goToAddressBook();
            controller.logout();
            controller.goToHelp();
            controller.goToHowToOrder();
            controller.goToMyAccount();
            controller.goToInsurance();
            controller.goToHelpSpecimen();
            controller.showNextRecords();
            controller.showPreviousRecords();
            controller.showFirstRecords();
            controller.showLastRecords();
            controller.getHasNextRecords();
            controller.getHasPreviousRecords();
            controller.showNextRecordsCO();
            controller.showPreviousRecordsCO();
            controller.showFirstRecordsCO();
            controller.showLastRecordsCO();
            controller.getHasNextRecordsCO();
            controller.getHasPreviousRecordsCO();
            controller.showNextRecordsAO();
            controller.showPreviousRecordsAO();
            controller.showFirstRecordsAO();
            controller.showLastRecordsAO();
            controller.getHasNextRecordsAO();
            controller.getHasPreviousRecordsAO();
            controller.assignSorterAsOrderDate();
            controller.assignSorterAsStatus();
            controller.getSubmittedOrders();


        controller.getNodeStatus();
        controller.setNodeStatus('NodeStatus');
        
            controller.getReceptorStatus();
            
        controller.getErStatus();
        controller.setErStatus('ErStatus');

            controller.getSubmissionType();

        controller.getSubType();
        controller.setSubType('Subtype');

            controller.getContactGender();

        controller.getConGender();
        controller.setConGender('ConGender');
        
            //controller.clonedOrderRoleFuture();
            
        }
        
        test.stopTest(); 

     }
     
     @isTest
     static void controllerSearchTest(){
         
        test.startTest();

        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];

        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();

        System.runAs(portalUser) {

            Account portalOrderAcc = new Account(
                RecordTypeId = accRecId,
                Name = 'Default Ordering Account'
                );
            insert portalOrderAcc;
            
            Order portalOrder = new Order(
                 RecordTypeId = orderRecType,
                 AccountId = portalOrderAcc.Id,
                 //Account = portalOrderAcc,
                 Name = 'TestPortalOrder',
                 OSM_Study_ID__c = '123',
                 //OSM_Study_Name__c = 'test study name', 
                 //OSM_Patient__c = portalPatient.Id,
                 OSM_Product__c = 'IBC',
                 OSM_Triage_Outcome__c = 'New', 
                 GHI_Portal_Associated_Requisition__c = 'Requisition',
                 GHI_Portal_CurrentStep__c = 1, 
                 Order_Location__c = 'Domestic', 
                 OSM_Bill_Type__c = 'Patient Pre-Pay',
                //  OSM_Related_Order__c = ,
                //  OSM_Related_Order__r.OrderNumber = ,
                 OSM_Patient_MRN__c = 'MRN',
                 Status = 'New',
                 OSM_Status__c = 'Draft',
                 EffectiveDate = Date.today()
             );
            Insert portalOrder;
            
            String dateString = string.valueOfGmt(Date.today() - 5);

            PageReference pageRef = Page.GHI_Portal_Order;
            test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('isadvancedsearch' , '1');
            portalOrder = [SELECT Id, OrderNumber FROM Order WHERE Id = : portalOrder.Id];
            System.debug('##portalNum.OrderNumber ' + portalOrder.OrderNumber);
            System.currentPageReference().getParameters().put('order-id', portalOrder.OrderNumber);
            System.currentPageReference().getParameters().put('patient', 'P');
            System.currentPageReference().getParameters().put('patient-mrn', '1');
            System.currentPageReference().getParameters().put('ordering-physician', 'A');
            System.currentPageReference().getParameters().put('patient-bdate', '7/02/2015');
            System.currentPageReference().getParameters().put('order-date', '7/02/2015');            
            
            System.debug('##DebugPageRefTest1 ' + pageRef);
            
            ApexPages.StandardController sc = new ApexPages.standardController(portalOrder);

            
            GHI_Portal_OrderInProgressExt controller = new GHI_Portal_OrderInProgressExt(sc);
            
            controller.changeSorter();
            controller.initialize();
            controller.getCompletedOrders();
            controller.getPhysicians();
            controller.getPhysicianLocation();
            controller.recipient2();
            controller.getRecipients();
            controller.getRecipientLocation();
            controller.recipientOnchange();
            controller.getPathologyDepartment();
            controller.displayOrderDetail();
       //   conrtoller.deleteSavedOrder();
            controller.getIcdOptions();
            //String dateString = string.valueOfGmt(Date.today() - 1);

       //   controller.searchPatientName = 'Test';
       //   controller.searchBirthDate = dateString;
       //   controller.searchPhysician = 'TestPhysician';
       //   controller.searchOrderNumber = portalOrder.OrderNumber;
       //   controller.searchPatientMRN = 'MRN';
       //   controller.searchOrderDate = dateString;
       //     controller.searchOrderInAdvanced();
            System.debug('##DebugPageRefTest2 ' + pageRef);
            // controller.setSubType('test');
            // controller.setConGender('male');
            controller.setCompletedOrders(new list<Order> {portalOrder});
            controller.setArchivedOrders(new list<Order> {portalOrder});
            controller.setSubmittedOrders(new list<Order> {portalOrder});
            


            
        }
        
        test.stopTest(); 

     }
    
}