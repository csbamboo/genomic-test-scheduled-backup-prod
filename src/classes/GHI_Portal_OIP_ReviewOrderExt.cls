/*----------------------------------------------------------------------------------
    Author:         Gypt Minierva
    Company:        Cloud Sherpas
    Description:    A controller for Order In Progress - Review Order Page
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    04/09/2015      Gypt Minierva           Created this controller
    04/10/2015      Stephen James Laylo     Code clean up & added data binding for
                                            Signed on Behalf of: Picklist
    06/10/2015		Katrina Guarina			Replaced instances of GHI_Portal_X10_Year_Survival_Benefit__c with 
    										OSM_10_Yr_Survival_w_Chemo_PREDICT__c and OSM_10_Yr_Survival_w_Chemo_Adjuvant__c
	06/11/2015		Katrina Guarina			Modified query of Static content to retrieve attachments 
----------------------------------------------------------------------------------- */

public class GHI_Portal_OIP_ReviewOrderExt {
    
    private String patientContactRecType;
    private GHI_Portal_Settings__c orgDefaults;
    public Map<String, OSM_Orderable_Creation__c> orderableCreationOrgDefaults { get; set; }
    private String orderId;
    private String signature = '';
    public List<OSM_Order_Role__c> orderRoles { get; set; }
    public List<OSM_Order_Role__c> orderRoles2 { get; set; }
    public List<OrderItem> orderItems { get; set; }
    
    private final String ORDER_ROLE_ORDERING = 'Ordering';
    private final String ORDER_ROLE_REPORTING = 'Additional Report Receiver';
    private final String ORDER_ROLE_ADDITIONAL = 'Additional';
    
    public String debug { get; set; }
    public Order order { get; set; }
    public Order billType { get; set;}
    public Order selectedBillType { get; set; }
    public OrderItem orderItem { get; set; }
    public Contact patientContact { get; set; }    
    
    public String product { get; set; }
    public String stage { get; set; }
    public String nccn { get; set; }
    public String header { get; set; }
    public String logo { get; set; }
    public String orderWorkflow { get; set; }
    
    public User user { get; set; }
    public Date birthDate { get; set; }
    public GHI_Portal_Static_Content__c portalStaticContent { get; set; }
    public List<GHI_Portal_Static_Content__c> pscList {get;set;}
    public String cityState { get; set; }
    
    public OSM_Order_Role__c specimenPathology { get; set; }
    public OSM_Order_Role__c optionPathology { get; set; }
    public OSM_Order_Role__c selectedPhysician { get; set; }
    public OSM_Order_Role__c optionContact { get; set; }
    public Account physicianAccount { get; set; }
    public Account specimenAccount { get; set; }
    public Account optionAccount { get; set; }
    public Contact physicianContact { get; set; }
    public Contact optionContacts { get; set; }

    
    //link values for sections
    public String details { get; set; }
    public String physicianDetails { get; set; }
    public String physicianLocDetails { get; set; }
    public String billingDetails { get; set; }
    public String additionalDetails { get; set; }
    public String additionalLocation { get; set; }
    public String specimenDetails { get; set; }
    public String optionPrimaries { get; set; }
    public String optionLocation { get; set; }
    
    //booleans used for rendering sections
    public Boolean showPatientDetails { get; set; }
    public Boolean showPhysicianDetails { get; set; }
    public Boolean showPhysicianLoc { get; set; }
    public Boolean showBillingDetails { get; set; }
    public Boolean ShowAdditionalDetails { get; set; }
    public Boolean showAdditionalLocation { get; set; }
    public Boolean showSpecimenDetails { get; set; }
    public Boolean isMedicare { get; set; }
    public Boolean isPrivateInsurance { get; set; }
    public Boolean showOptionLocation { get; set; }
    public Boolean showOptionPrimaries { get; set; }
    public Boolean isHardSave {get;set;} 
    
    //boolean for error messages to display
    public Boolean patientHasError { get; set; }
    public Boolean billingHasError { get; set; }
    public Boolean physicianHasError { get; set; }
    public Boolean specimenHasError { get; set; }
    public Boolean optionHasError { get; set;}
    
    public Boolean isErrorPage { get; set; }
        
    public GHI_Portal_OIP_ReviewOrderExt(ApexPages.StandardController controller) {
        
        ///Domestic, Intl-Partner values
        this.orderWorkflow = GHI_Portal_Utilities.getUserOrderWorkflow();
       
        this.patientHasError = false;
        this.billingHasError = false;
        this.physicianHasError = false;
        this.specimenHasError = false;
        this.optionHasError = false;
        
        this.product = '';
        this.logo = '';
        this.nccn = '';
        this.stage = '';

        this.orgDefaults = GHI_Portal_Settings__c.getOrgDefaults();
        this.orderableCreationOrgDefaults = OSM_Orderable_Creation__c.getAll();
        
        this.patientContactRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(this.orgDefaults.GHI_Portal_PatientContactRecType__c).getRecordTypeId();
        this.orderId = ApexPages.currentPage().getParameters().get('id');
        this.user = GHI_Portal_Utilities.getCurrentUser();
        
        //Default value of isErrorPage. If true, the page will redirect to an error page.
        this.isErrorPage = true;
        
        if (this.orderId != null && this.orderId != '') {
            GHI_Portal_Utilities.ValidatorWrapperClass vwc = new GHI_Portal_Utilities.ValidatorWrapperClass(this.orderId);
            
            this.order = vwc.order;

            this.orderItem = vwc.orderItem;
                         
            this.specimenPathology = vwc.specimenPathology;
            this.optionPathology = vwc.optionPathology;
            this.specimenAccount = vwc.specimenAccount;
            this.optionAccount = vwc.optionAccount;
            this.cityState = vwc.cityState;
                                                
            List<Order> selectedBillType = new List<Order>([SELECT Id, toLabel(OSM_Bill_Type__c) FROM Order WHERE Id = :orderId]);
            
            if(selectedBillType.size() > 0){
                this.billType = selectedBillType.get(0);
            }
            
            List<OSM_Order_Role__c> selectedPhysicians = new List<OSM_Order_Role__c>([SELECT OSM_Account__c, 
                                                                                             OSM_Account__r.Id, 
                                                                                             OSM_Contact__c, 
                                                                                             OSM_Contact__r.Id, 
                                                                                             OSM_Role__c, 
                                                                                             OSM_Order__c 
                                                                                      FROM OSM_Order_Role__c 
                                                                                      WHERE OSM_Order__c = :this.orderId 
                                                                                      AND OSM_Role__c = 'Ordering'
                                                                                      LIMIT 1]);
                                                                                      
            List<OSM_Order_Role__c> selectedPathologist = new List<OSM_Order_Role__c>(); 
            selectedPathologist = [SELECT OSM_Account__c, OSM_Account__r.Id, OSM_Contact__c, OSM_Contact__r.Id, OSM_Role__c, OSM_Order__c
                FROM OSM_Order_Role__c WHERE OSM_Order__c = :this.orderId AND OSM_Contact__c != null AND OSM_Role__c = 'Specimen Submitting' LIMIT 1];
                
            /**if (selectedPhysicians.size() > 0) {
                this.optionContact = selectedPhysicians.get(0);
                List<Contact> optionContacts = new List<Contact>([SELECT Id,Name FROM Contact Where Id = :optionContact.OSM_Contact__c]);
                
                if (optionContacts.size() > 0) {
                    this.optionContacts = optionContacts.get(0);
                }
            } **/
            if(selectedPathologist.size() > 0) { 
                this.optionContact = selectedPathologist.get(0);
                List<Contact> optionContacts = new List<Contact>([SELECT Id,Name FROM Contact Where Id = :optionContact.OSM_Contact__c]);
                
                if (optionContacts.size() > 0) {
                    this.optionContacts = optionContacts.get(0);
                }
            }
            
            this.physicianAccount = new Account();
            this.physicianContact = new Contact();
            this.selectedPhysician = new OSM_Order_Role__c();
            
            if (this.order.Id != null) {
                //Order exists. So it will not redirect to an error page
                this.isErrorPage = false;
                
                //get product
                this.product = this.order.OSM_Product__c;
                
                this.patientContact = vwc.patientContact;
                if (this.patientContact.Name == '[Not Provided]') {
                    this.patientContact = new Contact();
                }
                
                //fetches the Order Line Item related to the existing Order
                this.orderItems = new List<OrderItem>([SELECT Id, 
                                                              OSM_Colon_Clinical_Stage__c, 
                                                              OSM_NCCN_Risk_Category__c, 
                                                              OSM_Nodal_Status__c, 
                                                              OSM_ER_Status__c, 
                                                              OSM_Exception_Criteria__c,
                                                              OSM_Submitting_Diagnosis__c, 
                                                              OSM_ICD_Code__c,
                                                              GHI_Portal_How_to_Confirm_Int_Risk__c, 
                                                              OSM_Tumor_Grade__c,
                                                              //GHI_Portal_X10_Year_Survival_Benefit__c, 
                                                              OSM_10_Yr_Survival_w_Chemo_Adjuvant__c, 
                                                              OSM_10_Yr_Survival_w_Chemo_PREDICT__c, 
                                                              OSM_Tumor_Size__c,
                                                              GHI_PortalTreatment_Rec_Prior_to_DX__c, 
                                                              OSM_Her_2_Status__c,
                                                              GHI_Portal_Intl_Risk_Range__c
                                                       FROM OrderItem 
                                                       WHERE OrderId = :this.order.Id]);
    
                if (orderItems.size() > 0) {
                    this.orderItem = orderItems.get(0);
                    
                    if (this.product == this.orderableCreationOrgDefaults.get('Colon').GHI_Portal_Name__c || this.product == this.orderableCreationOrgDefaults.get('MMR').GHI_Portal_Name__c || this.product == this.orderableCreationOrgDefaults.get('MMR and Colon').GHI_Portal_Name__c || this.product == this.orderableCreationOrgDefaults.get('MMR Reflex to Colon').GHI_Portal_Name__c) {
                        //get stage if the Product is a Colon Assay
                        this.stage = this.orderItem.OSM_Colon_Clinical_Stage__c;
                    } else if (this.product == this.orderableCreationOrgDefaults.get('Prostate').GHI_Portal_Name__c) {
                        //get NCCN if the Product is a Prostate Assay
                        this.nccn = this.orderItem.OSM_NCCN_Risk_Category__c;
                    }
                }
                
                // set field visibility and logo based on product
                
                if (this.product != null && this.product != '') {
                	if (this.product == this.orderableCreationOrgDefaults.get('IBC').GHI_Portal_Name__c) {
                        this.logo = 'logobreast';
                        this.header = 'Onco<i>type</i> DX - Breast (' + this.orderableCreationOrgDefaults.get('IBC').GHI_Portal_DisplayLabel__c + ')';
                        
                    } else if (this.product == this.orderableCreationOrgDefaults.get('Prostate').GHI_Portal_Name__c) {
                        this.logo = 'logoprostate';
                        this.header = 'Onco<i>type</i> DX - Prostate (' + this.nccn + ')';

                    } else if (this.product == this.orderableCreationOrgDefaults.get('Colon').GHI_Portal_Name__c) {
                        this.logo = 'logocolon';
                        this.header = 'Onco<i>type</i> DX - Colon ' + this.stage;
                        
                    } else if (this.product == this.orderableCreationOrgDefaults.get('MMR Reflex to Colon').GHI_Portal_Name__c) {
                        this.logo = 'logocolon';
                        this.header = 'Onco<i>type</i> DX - Colon (' + this.orderableCreationOrgDefaults.get('MMR Reflex to Colon').GHI_Portal_DisplayLabel__c + ') ' + this.stage;
                        
                    } else if (this.product == this.orderableCreationOrgDefaults.get('MMR and Colon').GHI_Portal_Name__c) {
                        this.logo = 'logocolon';
                        this.header = 'Onco<i>type</i> DX - Colon ' + this.orderableCreationOrgDefaults.get('MMR and Colon').GHI_Portal_DisplayLabel__c + ' ' + this.stage;
                        
                    } else if (this.product == this.orderableCreationOrgDefaults.get('MMR').GHI_Portal_Name__c) {
                        this.logo = 'logocolon';
                        this.header = 'Onco<i>type</i> DX - Colon (' + this.orderableCreationOrgDefaults.get('MMR').GHI_Portal_DisplayLabel__c + ') ' + this.stage;
                        
                    } else if (this.product == this.orderableCreationOrgDefaults.get('DCIS').GHI_Portal_Name__c) {
                        this.logo = 'logodcis';
                        this.header = 'Onco<i>type</i> DX - Breast (' + this.orderableCreationOrgDefaults.get('DCIS').GHI_Portal_DisplayLabel__c + ')';
                        
                    } else {
                        this.logo = 'logo';
                        this.header = 'Unknown';
                    }
                	
                    if (this.patientContact.Id != null) {
                        this.header += ': ' + this.patientContact.Name;
                    }
                }
            }
            // get list of specialities
            String[] specArr = GHI_Portal_Settings__c.getOrgDefaults().GHI_Portal_Physician_Specialities__c.split(',', 0);
            
            // loop through and trim array values
            List<String> specList = new List<String>();
            for (String spec : specArr) {
                specList.add(spec.trim());
            }
            
            this.orderRoles = vwc.orderRoles;
            this.orderRoles2 = new List<OSM_Order_Role__c>([SELECT Id, 
                                                                   Name, 
                                                                   OSM_Account__r.Name, 
                                                                   OSM_Contact__r.Name, 
                                                                   OSM_Account__r.Phone, 
                                                                   OSM_Account__r.Fax, 
                                                                   OSM_Account__r.BillingStreet, 
                                                                   OSM_Account__r.BillingCity, 
                                                                   OSM_Account__r.BillingState, 
                                                                   OSM_Account__r.BillingPostalCode, 
                                                                   OSM_Account__r.BillingCountry, 
                                                                   OSM_Contact__r.Email
                                                            FROM OSM_Order_Role__c
                                                            WHERE (OSM_Role__c = :ORDER_ROLE_REPORTING OR 
                                                            OSM_Role__c = :ORDER_ROLE_ADDITIONAL)  //Added: Katanacio 071415
                                                            AND OSM_Order__c = :this.orderId]);
            if (this.orderRoles.size() > 0) {
                this.order.GHI_Portal_Signed_on_Behalf_of__c = this.orderRoles.get(0).OSM_Contact__r.LastName + ', ' + this.orderRoles.get(0).OSM_Contact__r.FirstName;
            }
            
            if (selectedPhysicians.size() > 0) {
                this.selectedPhysician = selectedPhysicians.get(0);
                String accId = selectedPhysician.OSM_Account__c;
                
                if (accId != null && accId != '') {
                    List<Account> physicianAccounts = new List<Account>([SELECT Id, 
                                                                                Name, 
                                                                                Phone, 
                                                                                Fax, 
                                                                                BillingCity, 
                                                                                BillingCountry, 
                                                                                BillingState, 
                                                                                BillingStreet, 
                                                                                BillingPostalCode 
                                                                         FROM Account 
                                                                         WHERE Id = :accId]);
                    if (physicianAccounts.size() > 0) {
                        this.physicianAccount = physicianAccounts.get(0);
                    }
                }
                
                String contId = selectedPhysician.OSM_Contact__c;
                if (contId != null && contId != '') {
                    List<Contact> physicianContacts = new List<Contact>([SELECT Id, 
                                                                                Name, 
                                                                                Email 
                                                                         FROM Contact 
                                                                         WHERE Id = :contId]); 
                    if (physicianContacts.size() > 0) {
                        this.physicianContact = physicianContacts.get(0);
                    }                  
                }
            }
            
            
            this.patientHasError = vwc.patientHasError;
            this.billingHasError = vwc.billingHasError;
            this.specimenHasError = vwc.specimenHasError;
            this.physicianHasError = vwc.physicianHasError;
            this.debug = vwc.debug;
        }
        
        //query to get portal static content for the physician signature section.
        //Modified: KGuarina 061115 
        //---START---
        try{ 
	        this.pscList = [SELECT Name, GHI_Portal_Product__c, GHI_Portal_Content_Type__c,
	             GHI_Portal_Locale__c, GHI_Portal_Static_Content__c, 
	             (SELECT Id, Name, Description FROM Attachments) FROM GHI_Portal_Static_Content__c WHERE 
	             (GHI_Portal_Content_Type__c = 'Physician Signature' AND GHI_Portal_Product__c = :signature)
	             OR (GHI_Portal_Content_Type__c = 'Insurance Coverage' AND 
	             GHI_Portal_Insurance_Company__c = :order.GHI_Portal_Primary_Insurance_Name__c)
	             ];
             
	        Integer j = 0;
	        while (j < pscList.size()) {
	            if(pscList.get(j).GHI_Portal_Content_Type__c == 'Physician Signature') {
	            	portalStaticContent = pscList.get(j);
	            	pscList.remove(j);
		        }
		        else{ 
		        	j++;
		        }
	        }
        } catch (Exception ex) {
        	Logger.debugException(ex);
            this.portalStaticContent = null;
        }   
        //---END--- 
                          
        //values of render commandlinks.
        this.details = Label.GHI_Portal_OIP_ShowDetails;
        this.physicianDetails = Label.GHI_Portal_OIP_ShowDetails;
        this.physicianLocDetails = Label.GHI_Portal_OIP_ShowDetails;
        this.billingDetails = Label.GHI_Portal_OIP_ShowDetails;
        this.additionalDetails = Label.GHI_Portal_OIP_ShowDetails;
        this.additionalLocation = Label.GHI_Portal_OIP_ShowDetails;
        this.specimenDetails = Label.GHI_Portal_OIP_ShowDetails;
        this.optionPrimaries = Label.GHI_Portal_OIP_ShowDetails;
        this.optionLocation = Label.GHI_Portal_OIP_ShowDetails;
    }
    
     public List<SelectOption> getPhysicians() {
        /*-----------------------------------------------------------------------------
        * Author        Stephen James Laylo
        * History       APR-10-2015
        * Description   Generates a list of select options for the Signed on Behalf of
        *               field in the Review Order page.
        * Returns       List of Select Options for the Signed on Behalf of field
        ------------------------------------------------------------------------------ */ 
        List<SelectOption> options = new List<SelectOption>();
        
        String tempContName = '';

        // get portal user's contact info
        Contact Cont = GHI_Portal_Utilities.getPortalUserContact();
        tempContName = Cont.LastName + ', ' + Cont.Firstname;
        options.add(new SelectOption(tempContName, tempContName));

        // loop through physicians and assign to picklist
        for (OSM_Order_Role__c dc : this.orderRoles) {
            String contName = dc.OSM_Contact__r.LastName + ', ' + dc.OSM_Contact__r.FirstName;
            if (contName != tempContName) {
                options.add(new SelectOption(contName, contName));
            }
        }
        
        if (options.isEmpty()) {
            options.add(new SelectOption('', '--None--'));
        }
        
        return options;
    }
    
    //change the status of the order to complete
    public PageReference saveOrder() {
        if (this.orderId != null && this.orderId != '') {
            
            if(isHardSave == true) { 
				return null;
			}
        				
            Savepoint sp = Database.setSavepoint();
            
            List<Order> ordersToUpdate = new List<Order>([SELECT Id, 
                                                                 OSM_Status__c, 
                                                                 GHI_Portal_Signed_on_Behalf_of__c, 
                                                                 GHI_Portal_Order_Terms_Accepted__c,
                                                                 OSM_Patient__c
                                                         FROM Order 
                                                         WHERE Id = :this.orderId
                                                         LIMIT 1]);
            
            if (ordersToUpdate.size() > 0) {
                Order orderToUpdate = ordersToUpdate.get(0);

                if (this.orderItems.size() > 0) {

                    if (!this.patientHasError && !this.billingHasError && !this.physicianHasError && !this.specimenHasError) {
                        orderToUpdate.Status = 'New';
                        orderToUpdate.OSM_Status__c = 'Order Intake';
                        
                        for (OrderItem orderItem : this.orderItems) {
                        	orderItem.OSM_Exception_Criteria__c = this.orderItem.OSM_Exception_Criteria__c;
                        }

                        //orderToUpdate.GHI_Portal_Exception_Criteria__c = this.order.GHI_Portal_Exception_Criteria__c;
                        orderToUpdate.GHI_Portal_Signed_on_Behalf_of__c = this.order.GHI_Portal_Signed_on_Behalf_of__c;
                        orderToUpdate.GHI_Portal_Order_Terms_Accepted__c = this.order.GHI_Portal_Order_Terms_Accepted__c;
                        
                        //update order to reflect modified values - Paul Wittmeyer - 9/1/2015 
						try{
                        	update OSM_OrderTriggerHandler.populatePatientFields(new List<Order>{orderToUpdate}, false);
						}catch(Exception e){
							Logger.debugException(e);
						}
                        
                        try {
                            //update orderToUpdate;
                            
                            if (this.orderItems.size() > 0) {
                            	update this.orderItems;
                            }
                           
                        } catch (Exception err) {
                            //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
                            Logger.debugException(err);
                            Database.rollback(sp);
                            this.debug = 'Debug (Saving Order): ' + err;
                            return null;
                        }
                        
                        return goToOrderDetails();

                    } 
                }
            }
        }
        
        return null;
    }
    
    public PageReference goToOrderDetails() {       
        PageReference pr = Page.GHI_Portal_OrderDetails;
        pr.getParameters().put('ordId', this.order.Id);
        pr.getParameters().put('ownrId', this.order.CreatedById);
        pr.getParameters().put('success', '1');
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToPreviousPage() {
        if (this.orderWorkflow == 'Domestic' || this.orderWorkflow == 'Intl-Non Partner') {
            return goToNewOrderSpecimen();
        } else {
            return goToNewOrderPhysician();
        }
    }

    //methods used to render different sections of the review page.
    public void showOptionPrimary() {
        if (this.optionPrimaries == Label.GHI_Portal_OIP_ShowDetails) {
            this.optionPrimaries = Label.GHI_Portal_OIP_HideDetails;
            this.showOptionPrimaries = true;
        } else {
            this.optionPrimaries = Label.GHI_Portal_OIP_ShowDetails;
            this.showOptionPrimaries = false;
        }
    }
    
    public void showOptionLocation() {
        if (this.optionLocation == Label.GHI_Portal_OIP_ShowDetails) {
            this.optionLocation = Label.GHI_Portal_OIP_HideDetails;
            this.showoptionLocation = true;
        } else {
            this.optionLocation = Label.GHI_Portal_OIP_ShowDetails;
            this.showoptionLocation = false;
        }
    }
    
    public void showPatientInfo() {
        if (this.details == Label.GHI_Portal_OIP_ShowDetails) {
            this.details = Label.GHI_Portal_OIP_HideDetails;
            this.showPatientDetails = true;
        } else {
            this.details = Label.GHI_Portal_OIP_ShowDetails;
            this.showPatientDetails = false;
        }
    }
    
    public void showPhysicianInfo() {
        if (this.physicianDetails == Label.GHI_Portal_OIP_ShowDetails) {
            this.physicianDetails = Label.GHI_Portal_OIP_HideDetails;
            this.showPhysicianDetails = true;
        } else {
            this.physicianDetails= Label.GHI_Portal_OIP_ShowDetails;
            this.showPhysicianDetails = false;
        }
    }
   
    public void showPhysicianLocation() {
        if (physicianLocDetails == Label.GHI_Portal_OIP_ShowDetails) {
            physicianLocDetails = Label.GHI_Portal_OIP_HideDetails;
            showPhysicianLoc = true;
       } else {
            physicianLocDetails = Label.GHI_Portal_OIP_ShowDetails;
            showPhysicianLoc = false;
       }
    }
   
    public void showAdditionalLocation() {
        if (additionalLocation == Label.GHI_Portal_OIP_ShowDetails) {
            additionalLocation = Label.GHI_Portal_OIP_HideDetails;
            showAdditionalLocation = true;
        } else {
            additionalLocation = Label.GHI_Portal_OIP_ShowDetails;
            showAdditionalLocation = false;
        }
    }
   
    public void showAdditionalDetail() {
         if (additionalDetails == Label.GHI_Portal_OIP_ShowDetails) {
             additionalDetails = Label.GHI_Portal_OIP_HideDetails;
             ShowAdditionalDetails = true;
         } else {
             additionalDetails = Label.GHI_Portal_OIP_ShowDetails;
             ShowAdditionalDetails = false;
         }
    }
    
    public void showSpecimenDetail() {
         if (specimenDetails == Label.GHI_Portal_OIP_ShowDetails) {
              specimenDetails = Label.GHI_Portal_OIP_HideDetails;
              showSpecimenDetails = true;
         } else {
              specimenDetails = Label.GHI_Portal_OIP_ShowDetails;
              showSpecimenDetails = false;
         }
    }
    
    public void showBillingDetail() {
        if (billingDetails == Label.GHI_Portal_OIP_ShowDetails) {
            billingDetails = Label.GHI_Portal_OIP_HideDetails;
            showBillingDetails = true;
         } else {
            billingDetails = Label.GHI_Portal_OIP_ShowDetails;
            showBillingDetails = false;
         }
    }

    //page redirections
    public PageReference goToNewOrderPatient() {
        PageReference pr = Page.GHI_Portal_OIP_Patient;
        pr.getParameters().put('id', this.orderId);
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToNewOrderBilling() {
        PageReference pr = Page.GHI_Portal_OIP_Billing;
        pr.getParameters().put('id', this.orderId);
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToNewOrderPhysician() {
        PageReference pr = Page.GHI_Portal_OIP_Physician;
        pr.getParameters().put('id', this.orderId);      
        pr.setRedirect(true);

        return pr;
    }  
      
    public PageReference goToNewOrderSpecimen() {
        PageReference pr = Page.GHI_Portal_OIP_Specimen;
        pr.getParameters().put('id', this.orderId);      
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToOrder() {
        PageReference pr = Page.GHI_Portal_Order;
        pr.setRedirect(true);
        return pr;
    }      
    
    public PageReference deleteOrder() {
        PageReference pr = Page.GHI_Portal_OIP_StartContinue;
        pr.setRedirect(true);
        
        Order orderToDelete = new Order();
        orderToDelete = [SELECT Id FROM Order WHERE Id = :this.orderId LIMIT 1];
        Savepoint sp = Database.setSavepoint();
        
        try {
                List<OrderItem> savedOrderItemsToDelete = new List<OrderItem>([SELECT Id 
                                                                               FROM OrderItem 
                                                                               WHERE OrderId = :orderToDelete.Id]);
                if (savedOrderItemsToDelete.size() > 0) {
                    delete savedOrderItemsToDelete.get(0);
                }
                
                delete orderToDelete;
                return pr; 
        } catch (Exception err) {
        	Logger.debugException(err);
            //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
            Database.rollback(sp);
            
            this.debug = '****Exception for deleting Saved Order: ' + err;
            System.debug(this.debug);
        }
        
        return null;                                            
    }     
}