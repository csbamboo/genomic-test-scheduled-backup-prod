/*-------------------------------------------------------------------------------------------------------------------------
    Author:         Gypt Minierva
    Company:        Cloud Sherpas
    Description:    A controller for New Order Specimen Page
                  
    Test Class:     
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    04/06/2015      Gypt Minierva           Created this controller
    04/09/2015      Stephen James Laylo     Fixed issues in Specimen Retrieval Checkboxes
    04/15/2015      Stephen James Laylo     Updated to map the Stage, Clinical Stage, and Products
                                            into the Page Section Title.
    04/20/2015      Stephen James Laylo     Updated to include Internation WorkFlow
    05/19/2015		Stephen James Laylo		Updated to change the source of the Product retrieval and the conditions.
    08/17/2015      Katrina Guarina         Modified backToOrder parameter from 1 to 2 to differentiate betweeen Physician and  Specimen redirection.
--------------------------------------------------------------------------------------------------------------------------*/

public class GHI_Portal_OIP_SpecimenExt {

    /* DECLARATIONS */
    public String debug { get; set; }
    public String orderId;
    public String product { get; set; }
    public String stage { get; set; }
    public String nccn { get; set; }
    public String header { get; set; }
    public String logo { get; set; }

    public User user { get; set; }    
    public Order specimenOrder { get; set; }
    public List<OrderItem> orderItems { get; set; }
    public OrderItem orderItem { get; set; }
    public Contact patientContact { get; set; }
    public Boolean showYesPanel { get; set; }
    
    public GHI_Portal_Settings__c orgDefaults;
    public Map<String, OSM_Orderable_Creation__c> orderableCreationOrgDefaults { get; set; }
    
    public String pathologyDept { get; set; }
    public Boolean isRetrieved { get; set; } 
    public Boolean isSpecimenSent { get; set; }
    public String conName { get; set; }
    public Map<Id, Customer_Affiliation__c> selectedAffiliation { get; set; }
    public String address { get; set; }
    public String cityState { get; set; }
    public String phone { get; set; }
    public List<OSM_Order_Role__c> specimenPathologyList { get; set; }
    public OSM_Order_Role__c specimenPathology { get; set; }
    public String pathologyDprt { get; set; }
    public String orderWorkflow { get; set; }
    public String location { get; set; }
    public String pathology { get; set; }
    public GHI_Portal_Utilities.ValidatorWrapperClass vwc { get; set; }
    public String nodeStatus { get; set; }
    public Boolean isErrorPage { get; set; }
    public Boolean isHardSave {get;set;} 
    public final static String ADD_LOCATION = 'add_location';
    
    private String contactAffiliationRecTypeId;
    private String contactToAccountAffiliationRecTypeId;
    private static final String VAR_ID = 'id';
    private String tempRoleStr; 
    
    public GHI_Portal_OIP_SpecimenExt(ApexPages.StandardController controller) {
        this.orgDefaults = GHI_Portal_Settings__c.getOrgDefaults();
        this.orderableCreationOrgDefaults = OSM_Orderable_Creation__c.getAll();
        
        this.orderItem = new OrderItem();
        this.patientContact = new Contact();
        this.debug = '';
        this.user = GHI_Portal_Utilities.getCurrentUser();
        this.orderWorkflow = GHI_Portal_Utilities.getUserOrderWorkflow();
        
        //get record type for contact to contact customer affiliation
        this.contactAffiliationRecTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get(this.orgDefaults.GHI_Portal_CNT_CNT_RecordType__c).getRecordTypeId();
        //get record type for contact to account customer affiliation
        this.contactToAccountAffiliationRecTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get(this.orgDefaults.GHI_Portal_CNT_ACCT_Recordtype__c).getRecordTypeId();
        
        this.product = '';
        this.logo = '';
        this.nccn = '';
        this.stage = '';

        this.orderId = ApexPages.currentPage().getParameters().get('id');       
        this.specimenPathology = new OSM_Order_Role__c();
        
        //Default value of isErrorPage. If true, the page will redirect to an error page.
        this.isErrorPage = true;

        List<Order> specimenOrderList = new List<Order>([SELECT Id,
                                                                OSM_Specimen_Retrieval_Option__c,
                                                                OSM_Multiple_Primaries_Requested__c,
                                                                OSM_Number_of_Multiple_Primaries_Request__c,
                                                                GHI_Portal_Primary_Specimen_ID__c,
                                                                OSM_Date_of_Collection_Surgery__c,
                                                                OSM_How_to_Test_Multiple_Primaries__c,
                                                                GHI_Portal_Secondary_Specimen_ID__c,
                                                                GHI_Portal_Secondary_Date_of_Surgery__c,
                                                                GHI_Portal_Comments__c,
                                                                GHI_Portal_CurrentStep__c,
                                                                OSM_Product__c,
                                                                GHI_Portal_Accessioning_Num__c,
                                                                Order_Location__c,
                                                                GHI_Portal_Instructions__c,
                                                                OSM_Patient__c
                                                         FROM Order 
                                                         WHERE Id = :orderId 
                                                		 AND CreatedById = :UserInfo.getUserId()
                                                         AND OSM_Status__c = 'Draft'
                                                         LIMIT 1]);
        
        if(this.orderWorkflow == 'Intl-Partner') { 
            tempRoleStr = 'Material Return'; 
        }
        else { 
            tempRoleStr = 'Specimen Submitting'; 
        }
        
        this.specimenPathologyList = new List<OSM_Order_Role__c>([SELECT Id, 
                                                                         OSM_Account__c, 
                                                                         OSM_Account__r.Phone, 
                                                                         OSM_Account__r.BillingStreet, 
                                                                         OSM_Account__r.BillingCity, 
                                                                         OSM_Account__r.BillingState, 
                                                                         OSM_Account__r.BillingPostalCode, 
                                                                         OSM_Account__r.BillingCountry, 
                                                                         OSM_Contact__c, 
                                                                         OSM_Account_Address__c, 
                                                                         OSM_Role__c, 
                                                                         Office_Contact__c
                                                                  FROM OSM_Order_Role__c 
                                                                  WHERE OSM_Order__c = :this.orderId
                                                                  AND OSM_Contact__c = null	//ADDED: Katanacio 072015 
                                                                  AND OSM_Role__c = :tempRoleStr]);
     
        if (specimenPathologyList.size() > 0) {
            this.specimenPathology = specimenPathologyList.get(0);

            if (this.specimenPathology.OSM_Account__c != null ) {
                Customer_Affiliation__c ca = [SELECT Id, 
                                                     OSM_Account_1__c, 
                                                     GHI_Portal_Account_1_Phone__c, 
                                                     OSM_Account_1__r.BillingStreet, 
                                                     OSM_Account_1__r.BillingCity, 
                                                     OSM_Account_1__r.BillingState, 
                                                     OSM_Account_1__r.BillingPostalCode
                                              FROM Customer_Affiliation__c 
                                              WHERE OSM_Account_1__c = :this.specimenPathology.OSM_Account__c
                                              AND GHI_Portal_Display_in_Portal__c = true 
                                              AND OSM_Contact_1__c = :this.user.ContactId 
                                              AND RecordTypeId = :contactToAccountAffiliationRecTypeId]; 
                if (ca != null) {
                    this.pathologyDprt = ca.Id;
                    this.address = ca.OSM_Account_1__r.BillingStreet;
                    this.cityState = ca.OSM_Account_1__r.BillingCity + ', ' +
                                ca.OSM_Account_1__r.BillingState + ' ' +
                                ca.OSM_Account_1__r.BillingPostalCode;
                    this.phone = ca.GHI_Portal_Account_1_Phone__c;
                    this.conName = this.specimenPathology.Office_Contact__c;
                }
            }
            
            this.pathologyDprt = this.specimenPathology.OSM_Account__c;
            this.pathology = this.specimenPathology.OSM_Contact__c;
        }
        
        
        if (specimenOrderList.size() > 0) {
            //Order exists. So it will not redirect to an error page
            this.isErrorPage = false;
                
            this.specimenOrder = specimenOrderList.get(0);

            if (this.specimenOrder.OSM_Specimen_Retrieval_Option__c == 'GHI to Request') {
                this.isRetrieved = true;
                this.isSpecimenSent = false;
                
            } else if (this.specimenOrder.OSM_Specimen_Retrieval_Option__c == 'MD to Request') {
                this.isRetrieved = false;
                this.isSpecimenSent = true;
                
            } else {
                this.isRetrieved = false;
                this.isSpecimenSent = false;
            }
            
            List<Contact> patientContacts = new List<Contact>([SELECT Id, 
                                                                      Name 
                                                               FROM Contact 
                                                               WHERE Id = :this.specimenOrder.OSM_Patient__c]);
            if (patientContacts.size() > 0) {
                this.patientContact = patientContacts.get(0);
                
                if (this.patientContact.Name == '[Not Provided]') {
                    this.patientContact = new Contact();
                }
            }
            
            //get product
            this.product = this.specimenOrder.OSM_Product__c;

            //fetches the Order Line Item related to the existing Order
            this.orderItems = new List<OrderItem>([SELECT Id, 
                                                          OSM_Colon_Clinical_Stage__c, 
                                                          OSM_NCCN_Risk_Category__c, 
                                                          OSM_Nodal_Status__c, 
                                                          OSM_ER_Status__c
                                                   FROM OrderItem 
                                                   WHERE OrderId = :this.orderId]);
            
            if (orderItems.size() > 0) {
                this.orderItem = orderItems.get(0);
                
                if (this.product == this.orderableCreationOrgDefaults.get('Colon').GHI_Portal_Name__c || this.product == this.orderableCreationOrgDefaults.get('MMR').GHI_Portal_Name__c || this.product == this.orderableCreationOrgDefaults.get('MMR and Colon').GHI_Portal_Name__c || this.product == this.orderableCreationOrgDefaults.get('MMR Reflex to Colon').GHI_Portal_Name__c) {
                    //get stage if the Product is a Colon Assay
                    this.stage = this.orderItem.OSM_Colon_Clinical_Stage__c;
                } else if (this.product == this.orderableCreationOrgDefaults.get('Prostate').GHI_Portal_Name__c) {
                    //get NCCN if the Product is a Prostate Assay
                    this.nccn = this.orderItem.OSM_NCCN_Risk_Category__c;
                }
            }
            
            // set field visibility and logo based on product
            if (this.product != null && this.product != '') {
            	if (this.product == this.orderableCreationOrgDefaults.get('IBC').GHI_Portal_Name__c) {
                    this.logo = 'logobreast';
                    this.header = 'Onco<i>type</i> DX - Breast (' + this.orderableCreationOrgDefaults.get('IBC').GHI_Portal_DisplayLabel__c + ')';
                    
                } else if (this.product == this.orderableCreationOrgDefaults.get('Prostate').GHI_Portal_Name__c) {
                    this.logo = 'logoprostate';
                    this.header = 'Onco<i>type</i> DX - Prostate (' + this.nccn + ')';

                } else if (this.product == this.orderableCreationOrgDefaults.get('Colon').GHI_Portal_Name__c) {
                    this.logo = 'logocolon';
                    this.header = 'Onco<i>type</i> DX - Colon ' + this.stage;
                    
                } else if (this.product == this.orderableCreationOrgDefaults.get('MMR Reflex to Colon').GHI_Portal_Name__c) {
                    this.logo = 'logocolon';
                    this.header = 'Onco<i>type</i> DX - Colon (' + this.orderableCreationOrgDefaults.get('MMR Reflex to Colon').GHI_Portal_DisplayLabel__c + ') ' + this.stage;
                    
                } else if (this.product == this.orderableCreationOrgDefaults.get('MMR and Colon').GHI_Portal_Name__c) {
                    this.logo = 'logocolon';
                    this.header = 'Onco<i>type</i> DX - Colon ' + this.orderableCreationOrgDefaults.get('MMR and Colon').GHI_Portal_DisplayLabel__c + ' ' + this.stage;
                    
                } else if (this.product == this.orderableCreationOrgDefaults.get('MMR').GHI_Portal_Name__c) {
                    this.logo = 'logocolon';
                    this.header = 'Onco<i>type</i> DX - Colon (' + this.orderableCreationOrgDefaults.get('MMR').GHI_Portal_DisplayLabel__c + ') ' + this.stage;
                    
                } else if (this.product == this.orderableCreationOrgDefaults.get('DCIS').GHI_Portal_Name__c) {
                    this.logo = 'logodcis';
                    this.header = 'Onco<i>type</i> DX - Breast (' + this.orderableCreationOrgDefaults.get('DCIS').GHI_Portal_DisplayLabel__c + ')';
                    
                } else {
                    this.logo = 'logo';
                    this.header = 'Unknown';
                }
            	
                if (this.patientContact.Id != null) {
                    this.header += ': ' + this.patientContact.Name;
                }
            }
        }                                                 
    }
    
    public void updateCurrentStep() {
        //for updating the current step in Order Progress
        if (this.specimenOrder.GHI_Portal_CurrentStep__c == null || 
            (((this.orderWorkflow == 'Domestic' || this.orderWorkflow == 'Intl-Non Partner') && this.specimenOrder.GHI_Portal_CurrentStep__c <= 4) || 
             (this.orderWorkflow == 'Intl-Partner' && this.specimenOrder.GHI_Portal_CurrentStep__c <= 3))) {
            
            try {
                if (this.orderWorkflow == 'Domestic' || this.orderWorkflow == 'Intl-Non Partner') {
                    this.specimenOrder.GHI_Portal_CurrentStep__c = 5;
                } else {
                    this.specimenOrder.GHI_Portal_CurrentStep__c = 4;
                }
            } catch (Exception err) {
            	Logger.debugException(err);
                System.debug('Error in updating Current Step in Order. ' + err);
            }
        }
    }
    
    public List<SelectOption> getPathologies() {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * History       APR-21-2015
        * Description   Generates a list of select options for the Pathologist
        *               field in the New Order Specimen page.
        * Returns       List of Select Options for the Pathologist field
        ----------------------------------------------------------------------- */
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        
               
        if (this.pathologyDprt != null && this.pathologyDprt != '') {

            // query for contact associations with portal user
            List<Customer_Affiliation__c> CA1s = [SELECT OSM_Contact_2__c
                                                  FROM Customer_Affiliation__c
                                                  WHERE RecordTypeId = :contactAffiliationRecTypeId
                                                  AND GHI_Portal_Display_in_Portal__c = true
                                                  AND OSM_Contact_2__c != null
                                                  AND OSM_Contact_1__c = :this.user.ContactId
                                                  ];
            
            // loop through customer affiliations, construct list of contact IDs                                     
            List<Id> contIds = new List<Id>();
            for (Customer_Affiliation__c CA : CA1s) {
                contIds.add(CA.OSM_Contact_2__c);
            }
            
            // query for contact associations for list of contacts and selected location ID
            List<Customer_Affiliation__c> CA2s = [SELECT OSM_Contact_1__c,
                                                         OSM_Contact_1__r.FirstName, 
                                                         OSM_Contact_1__r.LastName
                                                  FROM Customer_Affiliation__c
                                                  WHERE OSM_Role__c = 'Primary'
                                                  AND RecordTypeId = :contactToAccountAffiliationRecTypeId
                                                  AND OSM_Contact_1__c IN :contIds
                                                  AND OSM_Account_1__c = :this.pathologyDprt];
            
            for (Customer_Affiliation__c CA : CA2s) {
                options.add(new SelectOption(CA.OSM_Contact_1__c, CA.OSM_Contact_1__r.LastName + ', ' + CA.OSM_Contact_1__r.Firstname));
            }
        }

        return options;
    }
    
    
    public List<SelectOption> getNodeStatusOption() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        Schema.DescribeFieldResult fieldResult = Schema.sObjectType.OrderItem.fields.OSM_Nodal_Status__c.getSObjectField().getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for (Schema.PicklistEntry f : ple) {
            String label = f.getLabel();
            
            if (label != 'Unspecified') {
                options.add(new SelectOption(f.getValue(), label));
            }
        }
        
        return options;
    } 
         
    /*-----------------------------------------------------------------------
    * Author        Gypt Minierva
    * Company       Cloud Sherpas
    * History       April 6, 2015
    * Description   Page redirections.
    ----------------------------------------------------------------------- */

    public PageReference goToNewOrderReview() {
        PageReference pr = Page.GHI_Portal_OIP_ReviewOrder;
        pr.getParameters().put('id', this.orderId);
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToHowToOrder() {
        PageReference pr = Page.GHI_Portal_Help;
        pr.getParameters().put('page', 'order');
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToNewOrderPatient() {
        PageReference pr = Page.GHI_Portal_OIP_Patient;
        pr.getParameters().put('id', this.orderId);
        
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToNewOrderPhysician() {
        PageReference pr = Page.GHI_Portal_OIP_Physician;
        pr.getParameters().put('id', this.orderId);
        pr.setRedirect(true);

        return pr;
    }  
    
    public PageReference goToNewOrderBilling() {
        PageReference pr = Page.GHI_Portal_OIP_Billing;
        pr.getParameters().put('id', this.orderId);
        pr.setRedirect(true);

        return pr;
    }  
      
    public PageReference saveSpecimen() {
        /*-----------------------------------------------------------------------------------------
        * Author        Gypt Minierva
        * Company       Cloud Sherpas
        * History       APR-06-2015
        * Description   Method for saving the Specimen  
        * History       
        * <Date>        <Author>                    <Description>
        * 04/06/2015    Gypt Minierva               Created
        * 04/09/2015    Sairah Hadjinoor            Updated to add functionality of mapping fields
        *                                           and saving orders and order role
        * 04/09/2015    Stephen James Laylo         Fixed saving issues and code clean-up
        * 04/21/2015    Stephen James Laylo         Updated for inserting Pathology in International 
                                                    Workflow
        * 07/10/2015	Paul Wittmeyer				Added [add location] functionality to specimen 
        											retrieval
        ------------------------------------------------------------------------------------------- */
        Savepoint sp = Database.setSavepoint();
        
        updateCurrentStep();
        
        try {
            if (this.orderId != null) {
                OSM_Order_Role__c newSpecimenPathology;
                
                if (this.pathologyDprt != null && this.pathologyDprt != '') {
                    if (this.orderWorkflow == 'Intl-Partner') {
                        this.isRetrieved = true;
                    }
                    if (this.isRetrieved) {
                        Boolean willCreate = true;
                        
                        newSpecimenPathology = new OSM_Order_Role__c();
                        
                        newSpecimenPathology.OSM_Account__c = this.pathologyDprt;
                        newSpecimenPathology.Office_Contact__c = this.conName;
                        //Do not map to Contact full name  - Paul Wittmeyer 9/9/2015
                        //newSpecimenPathology.OSM_Contact_Full_Name__c = this.conName;
                        
                        
                        if (this.orderWorkflow != 'Intl-Partner') {
                            this.specimenOrder.OSM_Specimen_Retrieval_Option__c = 'GHI to Request';
                        }
                        
                        
                        if (this.orderWorkflow != 'Domestic') {
                            if (this.pathology != null && this.pathology != '') {
                                
                                newSpecimenPathology.OSM_Contact__c = this.pathology;
                                
                                if (this.orderWorkflow == 'Intl-Non Partner') {
                                    willCreate = false;
                                }
                            }
                        }
                        
                        if (willCreate) {
                            if (this.specimenPathology.Id == null) {
                                newSpecimenPathology.OSM_Order__c = this.orderId;
                                newSpecimenPathology.OSM_Role__c = tempRoleStr;
                                try{
                                	insert newSpecimenPathology;
                                }catch(Exception e){
                                	logger.debugException(e);
                                }
                            } else {
                                newSpecimenPathology.Id = this.specimenPathology.Id;
                                try{
                                	update newSpecimenPathology;
                            	}catch(Exception e){
                                	logger.debugException(e);
                                }
                            }
                        }
                        
                    }
                }
                else { 
                	if (this.isRetrieved && this.orderWorkflow != 'Intl-Partner') {
                            this.specimenOrder.OSM_Specimen_Retrieval_Option__c = 'GHI to Request';
                    }
                }
                
                if (this.isSpecimenSent && this.orderWorkflow != 'Intl-Partner') {
                    this.specimenOrder.OSM_Specimen_Retrieval_Option__c = 'MD to Request';
        
                    List<OSM_Order_Role__c> oldSpecimenPathology = new List<OSM_Order_Role__c>([SELECT Id 
                                                                                                FROM OSM_Order_Role__c
                                                                                                WHERE OSM_Role__c = :tempRoleStr
                                                                                                AND OSM_Contact__c = null
                                                                                                AND OSM_Order__c = :this.orderId]);
                                                                                                
                    if (oldSpecimenPathology.size() > 0) {
                        delete oldSpecimenPathology;
                    }
                    
                }
                
                //ADDED: KGuarina 090915 
                if(specimenOrder.GHI_Portal_Accessioning_Num__c != null && specimenOrder.GHI_Portal_Accessioning_Num__c != '') { 
                    specimenOrder.GHI_Portal_Primary_Specimen_ID__c = specimenOrder.GHI_Portal_Accessioning_Num__c; 
                }
                
                try{   
                	update this.specimenOrder;
            	}catch(Exception e){
                	logger.debugException(e);
                }

                //requerying to correct oli stamping mismatch for portal - Paul Wittmeyer 7/14/2015
                this.orderItems = new List<OrderItem>([SELECT Id, 
                                                          OSM_Colon_Clinical_Stage__c, 
                                                          OSM_NCCN_Risk_Category__c, 
                                                          OSM_Nodal_Status__c, 
                                                          OSM_ER_Status__c, 
                                                          OSM_Specimen_Retrieval_Option__c
                                                   FROM OrderItem 
                                                   WHERE OrderId = :this.orderId]); 

                for (OrderItem orderItem : this.orderItems) {
                	orderItem.OSM_Nodal_Status__c = this.orderItem.OSM_Nodal_Status__c;
                	orderItem.OSM_ER_Status__c = this.orderItem.OSM_ER_Status__c;
                	orderItem.OSM_Specimen_Retrieval_Option__c = this.specimenOrder.OSM_Specimen_Retrieval_Option__c;
                }

                if (this.orderItems.size() > 0) {
                	try{
                    	update this.orderItems;
                	}catch(Exception e){
                    	logger.debugException(e);
                    }
                }
            }
        } catch (Exception e) {
            //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
            Logger.debugException(e);
            Database.rollback(sp);
            this.debug = 'Debug: ' + e;
            
            return null;
        }
        
        if(isHardSave == true) { 
        	return null;
        }
        else {
	        if (this.orderWorkflow == 'Domestic' || this.orderWorkflow == 'Intl-Non Partner') {
	            return goToNewOrderReview();
	        } else {
	            return goToNewOrderPhysician();
	        }
        }
    }
    
    public PageReference goToPreviousPage() {
        if (this.orderWorkflow == 'Domestic' || (this.orderWorkflow == 'Intl-Non Partner' && this.user.Country != 'United Kingdom')) {
            return goToNewOrderPhysician();
        } else {
            if (this.orderWorkflow == 'Intl-Non Partner' && this.user.Country == 'United Kingdom') {
                return goToNewOrderPatient();
            }
            
            return goToNewOrderBilling();
        }
    }
    
    public void rerenderMultiplePrimaries() {
        this.showYesPanel = true;
    }
    
    
    public List<SelectOption> getPathologyDepartment() {
        
        List<SelectOption> options = new List<SelectOption>();
        selectedAffiliation = new Map<Id, Customer_Affiliation__c>();
       
        options.add(new SelectOption('', '-- Select Pathology --'));
        //added for add location functionality 7/10/2015 - Paul Wittmeyer
        options.add(new SelectOption(ADD_LOCATION, '[Add Location]'));

        
        for (Customer_Affiliation__c sp : [SELECT Id, 
                                                  OSM_Account_1_Name__c, 
                                                  GHI_Portal_Display_in_Portal__c,
                                                  OSM_Account_1__r.BillingStreet, 
                                                  OSM_Account_1__r.BillingCity,
                                                  OSM_Account_1__r.BillingPostalCode, 
                                                  OSM_Account_1__r.BillingCountry,   
                                                  OSM_Account_1__r.BillingState, 
                                                  OSM_Account_1__c,
                                                  GHI_Portal_Account_1_Phone__c
                                           FROM Customer_Affiliation__c
                                           WHERE GHI_Portal_Display_in_Portal__c = true 
                                           AND OSM_Contact_1__c = :user.ContactId 
                                           AND RecordTypeId = :contactToAccountAffiliationRecTypeId
                                           AND OSM_Account_1_Name__c != null
                                           ORDER BY Name ASC]) {
            selectedAffiliation.put(sp.OSM_Account_1__c, sp);
            options.add(new SelectOption(sp.OSM_Account_1__c, sp.OSM_Account_1_Name__c));
        }
            
            
        return options;
    }
    
    public pagereference selectedPathology() {
        if (pathologyDprt != '' && pathologyDprt != null && pathologyDprt != ADD_LOCATION){
            
            if (!selectedAffiliation.isEmpty()) {
                if (selectedAffiliation.containsKey(pathologyDprt)) {
                    address = selectedAffiliation.get(pathologyDprt).OSM_Account_1__r.BillingStreet;
                    cityState = selectedAffiliation.get(pathologyDprt).OSM_Account_1__r.BillingCity + ', ' +
                                selectedAffiliation.get(pathologyDprt).OSM_Account_1__r.BillingState + ' ' +
                                selectedAffiliation.get(pathologyDprt).OSM_Account_1__r.BillingPostalCode;
                    phone = selectedAffiliation.get(pathologyDprt).GHI_Portal_Account_1_Phone__c;
                }
            }
        //added for add location functionality 7/10/2015 - Paul Wittmeyer  
        return null;  
        }else if(pathologyDprt == ADD_LOCATION){
        	PageReference pr = goToAddEditLocation();
            pr.getParameters().put('backToOrder', '2');     //Modified: KGuarina 08/17/15 to differentiate betweeen Physician and Specimen tab redirection
        	return pr;
        	
        }else{
            address = '';
            cityState = '';
            phone = '';
            return null;
        }
    }
    //added for add location functionality 7/10/2015 - Paul Wittmeyer
     public PageReference goToAddEditLocation(){
        PageReference pr = Page.GHI_Portal_AddEditLocation;
        pr.getParameters().put(VAR_ID, this.orderId);
        pr.setRedirect(true);

        return pr;
    }
    
    public void toggleRetrieval() {
        if (isSpecimenSent) {
            isRetrieved = false;
        }
    }
    
    public void toggleSpecimen() {
        if (isRetrieved) {
            isSpecimenSent = false;
        }
    }
    
    public void resetQuantity() {
        /**if (this.specimenOrder.OSM_Multiple_Primaries_Requested__c == false){
            this.specimenOrder.OSM_Number_of_Multiple_Primaries_Request__c = '';
        }**/
        this.specimenOrder.OSM_Number_of_Multiple_Primaries_Request__c = null; 
		this.specimenOrder.OSM_How_to_Test_Multiple_Primaries__c = null; 
		this.specimenOrder.GHI_Portal_Instructions__c = null; 
		this.specimenOrder.GHI_Portal_Secondary_Specimen_ID__c = null; 
		this.specimenOrder.GHI_Portal_Secondary_Date_of_Surgery__c = null; 
    }
    
    public void resetFromQuantity() { 
		this.specimenOrder.OSM_How_to_Test_Multiple_Primaries__c = null; 
		this.specimenOrder.GHI_Portal_Instructions__c = null; 
		this.specimenOrder.GHI_Portal_Primary_Specimen_ID__c  = null; 
		this.specimenOrder.OSM_Date_of_Collection_Surgery__c = null; 
		//System.debug('this.specimenOrder.OSM_Number_of_Multiple_Primaries_Request__c: ' + 
		//    this.specimenOrder.OSM_Number_of_Multiple_Primaries_Request__c);
		//if(this.specimenOrder.OSM_Number_of_Multiple_Primaries_Request__c == '1') { 
			this.specimenOrder.GHI_Portal_Secondary_Specimen_ID__c = null; 
			this.specimenOrder.GHI_Portal_Secondary_Date_of_Surgery__c = null;
		//} 
    }
    
    public void resetFromHowTo() { 
		this.specimenOrder.GHI_Portal_Instructions__c = null; 
    }
    
    public PageReference deleteOrder() {
        PageReference pr = Page.GHI_Portal_OIP_StartContinue;
        pr.setRedirect(true);
        
        Order orderToDelete = new Order();
        orderToDelete = [SELECT Id FROM Order WHERE Id = :this.orderId LIMIT 1];
        Savepoint sp = Database.setSavepoint();
        
        try {
                List<OrderItem> savedOrderItemsToDelete = new List<OrderItem>([SELECT Id 
                                                                               FROM OrderItem 
                                                                               WHERE OrderId = :orderToDelete.Id]);
                if (savedOrderItemsToDelete.size() > 0) {
                    delete savedOrderItemsToDelete.get(0);
                }
                
                delete orderToDelete;
                return pr; 
        } catch (Exception err) {
        	Logger.debugException(err);
            //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
            Database.rollback(sp);
            
            this.debug = '****Exception for deleting Saved Order: ' + err;
            System.debug(this.debug);
        }
        
        return null;                                            
    }
}