/*------------------------------------------------------------------------------
    Author:         Sairah Hadjinoor
    Company:        Cloud Sherpas
    Description:    A controller for Start/Continue Order Page
                  
    Test Class:     
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    02/11/2015      Sairah Hadjinoor        Created 
    04/02/2015      Stephen James Laylo     Updated - Code clean up & added
                                            pagination for the Saved Orders 
    04/16/2015      Stephen James Laylo     Added a functionality for inserting
                                            Orders also in Colon and Prostate 
                                            Assay
    04/24/2015      Stephen James Laylo     Updated to let the user Cancel an
                                            Order from Saved Orders
    08/07/2015      Katrina Guarina         Added workaround for UNABLE_TO_LOCK_ROW error
-------------------------------------------------------------------------------*/

public class GHI_Portal_OIP_StartContinue_Controller {
    
	private GHI_Portal_Settings__c orgDefaults; 
	public Map<String, OSM_Orderable_Creation__c> orderableCreationOrgDefaults { get; set; }
    public Order newOrder { get; set; }
    public OrderItem newOrderItem { get; set; }
    
    public String debug { get; set; }
    public String logo { get; set; }
    public String saveOrderId { get; set; }
    public String productSelect { get; set; }
    public String stage { get; set; }
    public String nccn { get; set; }
    public String orderWorkflow { get; set; }
    
    public List<Order> listOfSavedOrders { get; set; }
    public Integer numberOfSavedOrders { get; set; }
    public User currentUser { get; set; }
    
    private String patientContactRecType;
    
    public GHI_Portal_OIP_StartContinue_Controller() {
        this.orgDefaults = GHI_Portal_Settings__c.getOrgDefaults();
        this.orderableCreationOrgDefaults = OSM_Orderable_Creation__c.getAll();
        this.orderWorkflow = GHI_Portal_Utilities.getUserOrderWorkflow();
        
        this.patientContactRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(this.orgDefaults.GHI_Portal_PatientContactRecType__c).getRecordTypeId();
        
        String logoParam = ApexPages.currentPage().getParameters().get('logo');
        
        this.logo = (logoParam != null && logoParam != '') ? logoParam : 'logo';
        this.productSelect = '';
        this.stage = '';
        this.nccn = '';
        this.currentUser = GHI_Portal_Utilities.getCurrentUser();
        this.listOfSavedOrders = new List<Order>([SELECT Id, 
                                                         OrderNumber, 
                                                         LastModifiedDate, 
                                                         OSM_Product__c, 
                                                         OSM_Patient__c, 
                                                         OSM_Patient__r.Name, 
                                                         OSM_Patient_DOB__c, 
                                                         OSM_Ordering_HCP_Name__c, 
                                                         Type, 
                                                         EffectiveDate, 
                                                         OSM_Status__c, 
                                                         OSM_Estimated_Report_Date__c, 
                                                         OSM_Patient_MRN__c  
                                                  FROM Order
                                                  WHERE OSM_Status__c = 'Draft'
                                                  AND CreatedById = :UserInfo.getUserId()
                                                  ORDER BY LastModifiedDate DESC]);
                                                  
        this.numberOfSavedOrders = getSavedOrders(false).size();
        this.debug = '';
        this.newOrder = new Order();
        this.newOrderItem = new OrderItem();
    }
    
    /*-----------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       APR-02-2015
    * Description   Query for selecting all submitted orders
    * Inputs        "isLimited" - if true, it will return paginated list.
                    Otherwise, it will return all Saved Orders in a list.
    * Returns       List of Saved Orders
    ----------------------------------------------------------------------- */ 
    public List<Order> getSavedOrders(Boolean isLimited) {
        
        if (isLimited) {
            return (List<Order>) savedOrdersSetCon.getRecords();
        } else {
            return this.listOfSavedOrders;
        }
    }
    //Calls the first function with a default value of true for the isLimited argument.
    public List<Order> getSavedOrders() {
        return getSavedOrders(true);
    }
    
    public void setCompletedOrders(List<Order> savedOrders) {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       APR-02-2015
        * Description   Method for setting a list of Saved Orders.
        * Inputs        "savedOrders" - holds the list of Saved Orders.
        ----------------------------------------------------------------------- */ 
        this.listOfSavedOrders = savedOrders;
    }
    
    public ApexPages.StandardSetController savedOrdersSetCon {
        /*-----------------------------------------------------------------------
        * Author        Stephen James Laylo
        * Company       Cloud Sherpas
        * History       APR-02-2015
        * Description   Initialization for the Standard Set Controller of
        *               Saved Orders.
        ----------------------------------------------------------------------- */ 
        get {
            if (this.savedOrdersSetCon == null) {
                this.savedOrdersSetCon = new ApexPages.StandardSetController(this.listOfSavedOrders);
            }
            GHI_Portal_Settings__c ps = GHI_Portal_Settings__c.getOrgDefaults();
            if (ps.GHI_Portal_Records_Per_Page__c != null) {
                this.savedOrdersSetCon.setPageSize((Integer) ps.GHI_Portal_Records_Per_Page__c);
            } else {
                this.savedOrdersSetCon.setPageSize(10);
            }
            return savedOrdersSetCon ;
        }
        set;
    }
    
    public PageReference deleteSavedOrder() {
        /*-------------------------------------------------------------------------
           @author        Stephen James Laylo
           @company       Cloud Sherpas
           @description   Deletes an Order Record as well as the related 
                          records associated to it except the Account and Contact
           @date          04/24/2015
           @param         N/A
           @return        N/A
           
           <History>      <Author>                  <Description>
           04/24/2015     Stephen James Laylo       Created
        ---------------------------------------------------------------------------*/      
        
        PageReference pr = Page.GHI_Portal_OIP_StartContinue;
        pr.setRedirect(true);
        //validate user should have CRUD access for this order - Paul Wittmeyer 10/15/2015
        if(!GHI_Portal_Utilities.hasOrderAccess(this.saveOrderId)){
        	PageReference errorPage = new pagereference('/apex/GHI_Portal_Error'); 
        	return errorPage;
        }else{
	        List<Order> savedOrdersToDelete = new List<Order>([SELECT Id 
	                                                           FROM Order 
	                                                           WHERE Id = :this.saveOrderId 
	                                                           LIMIT 1]);
	        Order savedOrderToDelete = new Order();
	        Savepoint sp = Database.setSavepoint();
	        
	        try {
	            if (savedOrdersToDelete.size() > 0) {
	                savedOrderToDelete = savedOrdersToDelete.get(0);
	                
	                List<OrderItem> savedOrderItemsToDelete = new List<OrderItem>([SELECT Id 
	                                                                               FROM OrderItem 
	                                                                               WHERE OrderId = :savedOrderToDelete.Id 
	                                                                               LIMIT 1]);
	                System.debug('##savedOrderSize ' + savedOrderItemsToDelete.size());
	                if (savedOrderItemsToDelete.size() > 0) {
	                    delete savedOrderItemsToDelete.get(0);
	                }
	                
	                List<OSM_Order_Role__c> savedOrderRolesToDelete = new List<OSM_Order_Role__c>([SELECT Id 
	                                                                                               FROM OSM_Order_Role__c 
	                                                                                               WHERE OSM_Order__c = :savedOrderToDelete.Id 
	                                                                                               LIMIT 1]);
	                if (savedOrderRolesToDelete.size() > 0) {
	                    delete savedOrderRolesToDelete.get(0);
	                }
	                
	                delete savedOrderToDelete;
	                
	                return pr; 
	            }
	        } catch (Exception err) {
	        	Logger.debugException(err);
	            //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
	            Database.rollback(sp);
	            
	            this.debug = '****Exception for deleting Saved Order: ' + err;
	            System.debug(this.debug);
	        }
	        
	        return null;                                            
	    }
    }
    public PageReference goToNewOrderPatient() {
        PageReference pr = Page.GHI_Portal_OIP_Patient;
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference goToHelp() {
    PageReference pr = Page.GHI_Portal_Help;
    pr.getParameters().put('page', 'order'); 
    pr.setRedirect(true);
    
    return pr;
    }
    public PageReference goToNewOrderPhysician() {
        PageReference pr = Page.GHI_Portal_OIP_Physician;
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference saveOrder() {
        Savepoint sp = Database.setSavepoint();
        PageReference pr;
        
        if (this.orderWorkflow == 'Intl-Non Partner' && this.currentUser.Country == 'United Kingdom') {
            pr = goToNewOrderPhysician();
        } else {
            pr = goToNewOrderPatient();
        }
        
        
        try {
            this.newOrder.Status = 'New';
            this.newOrder.OSM_Status__c = 'Draft';
            this.newOrder.OSM_Triage_Outcome__c = 'New';
            this.newOrder.EffectiveDate = Date.today();
            this.newOrder.OSM_Channel__c = 'Portal';
            this.newOrder.AccountId = this.currentUser.Contact.AccountId; 
            System.debug('this.newOrder.AccountId: ' + this.newOrder.AccountId); 
            
            //@rao Default Account for Patient and Order is populated in Trigger
         /*   List<Account> accounts = new List<Account>([SELECT Id, 
                                                               Name 
                                                        FROM Account 
                                                        WHERE Name = 'DEFAULT ACCOUNT FOR PATIENT']);
            
            this.newOrder.AccountId = accounts.get(0).Id;
            System.debug('$##AccountTest ' + accounts); */
            /*
            List<PriceBook2> pbEntry = new List<PriceBook2>([SELECT Id from PriceBook2 WHERE Name =: this.orgDefaults.GHI_Portal_StandardPriceBook__c]);
            
            if (accounts.size() > 0) {
            	this.newOrder.AccountId = accounts.get(0).Id;
            }
            
            if (pbEntry.size() > 0) {
            	this.newOrder.Pricebook2Id = pbEntry.get(0).Id;
            }
            */
            
            if (this.orderWorkflow == 'Intl-Non Partner' && this.currentUser.Country == 'United Kingdom') {
          //      System.debug('$##AcountSize ' + accounts.size());
          //      if (accounts.size() > 0) {
                    Contact patientContact = new Contact();
                    
          //        patientContact.AccountId = accounts.get(0).Id;
                    patientContact.RecordTypeId = this.patientContactRecType;
                    patientContact.LastName = '[Not Provided]';
                    try{
                    	insert patientContact;
	                    System.debug('$##InsertedContact ' + patientContact);
                    }catch(Exception e){
                    	logger.debugException(e);
                    	System.debug('!@#PatientError ' + e);
                    }

                    if (patientContact.Id != null) {
                        this.newOrder.OSM_Patient__c = patientContact.Id;
             //       }
                }
            }
            
            // set product to what was selected in Assay Selection
            this.newOrder.OSM_Product__c = this.productSelect;                    
            
            // default order origin to Domestic for sprint 41: TODO: make dynamic for sprint 42
            String orderWorkflow = GHI_Portal_Utilities.getUserOrderWorkflow();
            if (orderWorkflow == 'Domestic') {
                this.newOrder.Order_Location__c = 'Domestic';
            } else {
                this.newOrder.Order_Location__c = 'International';
            }
            
        //    System.debug('@@@Account: ' + this.newOrder.AccountId);
            
            try{
            insert this.newOrder;
            System.debug('!@#insertedOrder ' + newOrder);
            System.debug('!@#orderAccountName ' + newOrder.Account.Name);
            System.debug('!@#orderAccountId ' + newOrder.AccountId);
            System.debug('!@#orderPatient ' + newOrder.OSM_Patient__c);
            System.debug('!@#orderOwner ' + newOrder.OwnerId);
            System.debug('!@#orderOwner ' + newOrder.Owner.Name);
            } catch (Exception err) {
                //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
                System.debug('##Exception: ' + err);
                Logger.debugException(err);
                Database.rollback(sp);
            }
            
            String product = this.productSelect;
            System.debug('@@@ProductSelect: ' + this.productSelect);
            
            if (this.productSelect == this.orderableCreationOrgDefaults.get('Colon').GHI_Portal_Name__c) {
                product = this.orgDefaults.GHI_Portal_OncotypeDXColon__c;
            } else if (this.productSelect == this.orderableCreationOrgDefaults.get('MMR Reflex to Colon').GHI_Portal_Name__c) {
                product = this.orgDefaults.GHI_Portal_OncotypeDXColon__c;
            } else if (this.productSelect == this.orderableCreationOrgDefaults.get('MMR').GHI_Portal_Name__c) {
                product = this.orgDefaults.GHI_Portal_OncotypeDXColon__c;
            } else if (this.productSelect == this.orderableCreationOrgDefaults.get('IBC').GHI_Portal_Name__c) {
                product = this.orgDefaults.GHI_Portal_OncotypeDXBreast__c;
            } else if (this.productSelect == this.orderableCreationOrgDefaults.get('DCIS').GHI_Portal_Name__c) {
                product = this.orgDefaults.GHI_Portal_OncotypeDXDCIS__c;
            } else if (this.productSelect == this.orderableCreationOrgDefaults.get('Prostate').GHI_Portal_Name__c) {
                product = this.orgDefaults.GHI_Portal_OncotypeDXProstate__c;
            }

            
            if (this.newOrder.Id != null) {
            	// query for OLI
            	List<OrderItem> orderItems = new List<OrderItem>([SELECT Id, 
            	                                                         OSM_Colon_Clinical_Stage__c, 
            	                                                         OSM_NCCN_Risk_Category__c 
            	                                                  FROM OrderItem 
            	                                                  WHERE OrderId = :this.newOrder.Id]);
            	
            	// set OLI fields
            	for (OrderItem orderItem : orderItems) {
            		if (this.productSelect == this.orderableCreationOrgDefaults.get('Colon').GHI_Portal_Name__c || this.productSelect == this.orderableCreationOrgDefaults.get('MMR').GHI_Portal_Name__c || this.productSelect == this.orderableCreationOrgDefaults.get('MMR and Colon').GHI_Portal_Name__c || this.productSelect == this.orderableCreationOrgDefaults.get('MMR Reflex to Colon').GHI_Portal_Name__c) {
            			orderItem.OSM_Colon_Clinical_Stage__c = this.stage;
                    }
                    
                    if (productSelect == this.orderableCreationOrgDefaults.get('Prostate').GHI_Portal_Name__c) {
                    	orderItem.OSM_NCCN_Risk_Category__c = this.nccn;
                    }
            	}
            	
            	// update OLI
            	if (orderItems.size() > 0) {
                    try{
                        update orderItems;
                    } catch(Exception e1){ if(e1.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update orderItems; } catch(Exception e2){ if(e2.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update orderItems; } catch(Exception e3){ if(e3.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update orderItems; } catch(Exception e4){ if(e4.getMessage().contains('UNABLE_TO_LOCK_ROW')){ try{ update orderItems; } catch(Exception e5){ if(e5.getMessage().contains('UNABLE_TO_LOCK_ROW')){ update orderItems;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
            	}
            }

            /*
            List<Pricebook2> pb2s = new List<Pricebook2>([SELECT Name, Id, 
                                                                 (SELECT Id, Name, UnitPrice 
                                                                  FROM PriceBookEntries 
                                                                  WHERE Name = :product) 
                                                          FROM Pricebook2 
                                                          WHERE Name = :this.orgDefaults.GHI_Portal_StandardPriceBook__c 
                                                          LIMIT 1]);
                
            if (pb2s.size() > 0) {
                List<PricebookEntry> pbes = pb2s.get(0).PriceBookEntries;
                List<OrderItem> pendingOLI = new List<OrderItem>();
                
                for (PricebookEntry pe : pbes) {
                    this.newOrderItem.OrderId = this.newOrder.Id;
                    this.newOrderItem.PricebookEntryId = pe.Id;
                    this.newOrderItem.UnitPrice = pe.UnitPrice;
                    this.newOrderItem.Quantity = 1;
                    this.newOrderItem.OSM_Nodal_Status__c = null;
                    this.newOrderItem.OSM_ER_Status__c = null;
                    this.newOrderItem.OSM_Lab_and_Report_Status__c = 'Pre-Processing';
                    
                    if (this.productSelect == 'Oncotype DX Colon' || this.productSelect == 'MMR' || this.productSelect == 'MMR + Oncotype DX Colon' || this.productSelect == 'MMR/Oncotype DX Colon Sequential') {
                        this.newOrderItem.OSM_Clinical_Stage__c = this.stage;
                    }
                    
                    if (productSelect == 'Oncotype DX Prostate') {
                        this.newOrderItem.OSM_NCCN_Risk_Category__c = this.nccn;
                    }
                    
                    pendingOLI.add(this.newOrderItem);
                }
                
                if (pbes.size() > 0) {
                	try{
                    	insert pendingOLI;
                	}catch(Exception e){
                    	logger.debugException(e);
                    }
                } else {
                    return null;
                }
            }*/

            pr.getParameters().put('id', this.newOrder.Id);
            pr.setRedirect(true);
        } catch (Exception err) {
            //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
            Logger.debugException(err);
            Database.rollback(sp);
            System.debug('****Exception: ' + err);
            this.debug += '' + err;
            return null;
        }
        
        return pr;
    }
    
    /*------------------------------------------------------------------------
    * Author        Stephen James Laylo
    * Company       Cloud Sherpas
    * History       APR-02-2015
    * Description   Navigation for Saved Orders. Used in pagination
    ------------------------------------------------------------------------ */
    public void showNextRecords() {
        this.savedOrdersSetCon.next();
    }
    public void showPreviousRecords() {
        this.savedOrdersSetCon.previous();
    }
    public void showFirstRecords() {
        this.savedOrdersSetCon.first();
    }
    public void showLastRecords() {
        this.savedOrdersSetCon.last();
    }
    public Boolean getHasNextRecords() {
        return this.savedOrdersSetCon.getHasNext();
    }
    public Boolean getHasPreviousRecords() {
        return this.savedOrdersSetCon.getHasPrevious();
    }
}