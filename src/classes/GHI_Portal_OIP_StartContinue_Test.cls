@isTest
public class GHI_Portal_OIP_StartContinue_Test{
    
    @testsetup
    private static void setup() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
            User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
            portalUser.GHI_Portal_Order_Workflow__c = 'Intl-Non Partner';
            portalUser.Country = 'United Kingdom';
            insert portalUser;
             
            Account testAcct = GHI_Portal_TestUtilities.createAccountHCO(); 
            insert testAcct;
            
            Account testAcct2 = GHI_Portal_TestUtilities.createDefaultAcc(); 
            insert testAcct2;
            
            Contact patAcct = GHI_Portal_TestUtilities.createContactPatient(testAcct.Id);
            insert patAcct;
            
            Customer_Affiliation__c testCA = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(testAcct.Id, portalUser.ContactId, 'Primary'); 
            insert testCA;  
            
            Contact testContact = GHI_Portal_TestUtilities.createContactHCP(testAcct.Id); 
            insert testContact; 
            
            Customer_Affiliation__c testCA2 = GHI_Portal_TestUtilities.createCNTtoCNTAffiliation(portalUser.ContactId, testContact.Id); 
            insert testCA2; 
            
            Customer_Affiliation__c testCA3 = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(testAcct.Id, testContact.Id, 'Primary'); 
            insert testCA3; 
            
            GHI_Portal_Settings__c customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
            insert customSetting;
            
            Id patRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
            
            Id cntAcnt = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Cnt-Cnt Affiliation').getRecordTypeId();
            
            
            User ownerUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
            Record_Type__c patientRT = new Record_Type__c(RT_Cnt_Acct_CustAffiliation__c = cntAcnt,
                                                          User_DPA_Owner__c = thisUser.Id,
                                                          Contact_Patient_Default_Account__c=testAcct.Id,
                                                          Contact_Patient_Record_Type__c=patRecType,
                                                          OSM_Order_Account_Default__c= testAcct2.Id+ ',' +testAcct2.Id+ ',' +testAcct2.Id+ ',' +testAcct2.Id+ ',' +testAcct2.Id+ ',' + testAcct2.Id+ ',' +testAcct2.Id+ ',' +testAcct2.Id+ ',' +testAcct2.Id+ ',' +testAcct2.Id);
            insert patientRT;
            
            GHI_Internal__c internal = new GHI_Internal__c ();
            
            internal.Name = 'GHI Internal';
            insert internal;
            
            Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();    
            Account dAccount =  new Account(
                Name = 'DEFAULT ACCOUNT FOR PATIENT',
                RecordTypeId = accRecId
                );
            Insert dAccount;
            
            TriggerSwitch__c trggr = new TriggerSwitch__c(
                Order_Trigger__c = true,
                Contact_Trigger__c = true,
                Customer_Affiliation_Trigger__c = true
                );
            insert trggr;
            
            Pricebook2 standardPBook = new Pricebook2(
                Name = 'Standard Price Book'
                );
            insert standardPBook;
            System.debug('!@#Pbook' + standardPBook);
            
            //Create Orderable Creation custom setting
            // List<OSM_Orderable_Creation__c> listCustomSetting = new List <OSM_Orderable_Creation__c>();
            // OSM_Orderable_Creation__c oc1 = new OSM_Orderable_Creation__c(Name='Colon',GHI_Portal_Name__c='TestName1',Product_Name__c='TestProduct');
            // listCustomSetting.add(oc1);
            // OSM_Orderable_Creation__c oc2 = new OSM_Orderable_Creation__c(Name='MMR Reflex to Colon',GHI_Portal_Name__c='TestName1',Product_Name__c='TestProduct');
            // listCustomSetting.add(oc2);
            // OSM_Orderable_Creation__c oc3 = new OSM_Orderable_Creation__c(Name='MMR',GHI_Portal_Name__c='TestName1',Product_Name__c='TestProduct');
            // listCustomSetting.add(oc3);
            // OSM_Orderable_Creation__c oc4 = new OSM_Orderable_Creation__c(Name='IBC',GHI_Portal_Name__c='TestName1',Product_Name__c='TestProduct');
            // listCustomSetting.add(oc4);
            // OSM_Orderable_Creation__c oc5 = new OSM_Orderable_Creation__c(Name='DCIS',GHI_Portal_Name__c='TestName1',Product_Name__c='TestProduct');
            // listCustomSetting.add(oc5);
            // OSM_Orderable_Creation__c oc6 = new OSM_Orderable_Creation__c(Name='Prostate',GHI_Portal_Name__c='TestName1',Product_Name__c='TestProduct');
            // listCustomSetting.add(oc6);
            // insert listCustomSetting;
            
            List<OSM_Orderable_Creation__c> orderable = GHI_Portal_TestUtilities.createOrderable();
            insert orderable;
            
            LoggerSettings__c logSet = new LoggerSettings__c(
                Is_Active__c = true,
                Log_Activities__c = true
                );
            insert logSet;
        } 
    }
     
    @isTest
    private static void controllerTest() {
    
        Test.startTest();
        System.debug('!@#TESTSTARTED');
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        
        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 

        System.runAs(portalUser) {
            
            Account portalOrderAcc = new Account(
            RecordTypeId = accRecId,
            Name = 'DEFAULT ACCOUNT FOR PATIENT'
            );
            insert portalOrderAcc;
            PageReference pageRef = Page.GHI_Portal_OIP_StartContinue;
            //pageRef.getParameters().put('logo', 'logo');          
            Test.setCurrentPage(pageRef);
            
            GHI_Portal_OIP_StartContinue_Controller controller = new GHI_Portal_OIP_StartContinue_Controller();
            //controller.newOrder.AccountId = portalOrderAcc.Id;
            controller.productSelect = 'IBC';
            controller.showNextRecords();
            controller.showPreviousRecords();
            controller.showFirstRecords();
            controller.showLastRecords();
            controller.getHasPreviousRecords();
            controller.getHasNextRecords();
            controller.goToNewOrderPhysician();
            controller.goToNewOrderPatient();
            
            controller.goToHelp();
            Schema.DescribeSObjectResult d = Order.SObjectType.getDescribe();
            System.debug('##OrderWirte? ' + d.isCreateable());
            Schema.DescribeSObjectResult c = Contact.SObjectType.getDescribe();
            System.debug('##ContactWrite? ' + c.isCreateable());
            controller.saveOrder();
            
            controller.orderWorkflow = 'Domestic';
            controller.saveOrder();
            
            controller.setCompletedOrders(controller.getSavedOrders());
            // controller.saveOrderId = controller.newOrder.Id;
            // controller.deleteSavedOrder();
        }
        
        Test.stopTest(); 
        
    }
    
    @isTest
    private static void controllerTest2() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs(thisUser){
            
            test.startTest();
            
            User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1];
            
            System.runAs(portalUser) {
                
            Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();

            
            Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();    
            Account defAccount =  new Account(
                Name = 'DEFAULT ACCOUNT FOR PATIENT',
                RecordTypeId = accRecId
                );
            Insert defAccount;
            
                PageReference pageRef = Page.GHI_Portal_OIP_StartContinue;
                //pageRef.getParameters().put('logo', 'logo');          
                Test.setCurrentPage(pageRef);
                
                GHI_Portal_OIP_StartContinue_Controller controller = new GHI_Portal_OIP_StartContinue_Controller();
                controller.newOrder.AccountId = defAccount.Id;
                controller.productSelect = 'Prostate';
                controller.showNextRecords();
                controller.showPreviousRecords();
                controller.showFirstRecords();
                controller.showLastRecords();
                controller.getHasPreviousRecords();
                controller.getHasNextRecords();
                controller.goToNewOrderPhysician();
                controller.goToNewOrderPatient();
                
                controller.goToHelp();
                
                controller.saveOrder();
                
                Product2 prod = new Product2(
                    Name = 'Prostate'
                    );
                insert prod;
                
        
                Id pricebookId = Test.getStandardPricebookId();
                
                PricebookEntry standardPrice = new PricebookEntry(
                    Pricebook2Id = pricebookId, Product2Id = prod.Id,
                    UnitPrice = 10000, IsActive = true);
                insert standardPrice;
                
                // PricebookEntry pBookEntry = new PriceBookEntry(
                //     UnitPrice = 15,
                //     Pricebook2Id = pBook2.Id,
                //     product2Id = product.Id,
                //     UseStandardPrice = false
                //     );
                // insert pBookEntry;
                System.debug('##pBook ' + standardPrice.Id);
                
                Order orderToDelete = new Order(
                    Status = 'New',
                    OSM_Status__c = 'Draft',
                    OSM_Triage_Outcome__c = 'New',
                    EffectiveDate = Date.today(),
                    OSM_Channel__c = 'Portal',
                    AccountId = defAccount.Id,
                    Pricebook2Id = pricebookId
                    );
                Insert orderToDelete;
                controller.newOrder = orderToDelete;
                
                System.debug('##OrderToDelete ' + orderToDelete.Id);
                System.debug('##OrderAccountDel ' + orderToDelete.Account.Name);
                
                OrderItem oItemToDelete = new OrderItem(
                    OrderId = OrderToDelete.Id,
                    PricebookEntryId = standardPrice.Id,
                    UnitPrice = 15,
                    Quantity = 1
                    );
                insert oItemToDelete;
                controller.saveOrder();
                System.debug('##OItemToDelete ' + oItemToDelete.Id);
                controller.saveOrderId = orderToDelete.Id;
                
                controller.deleteSavedOrder();
                
                // to cover the catch part of the method
                controller.saveOrderId = null;
                controller.deleteSavedOrder();
            }
            
            Test.stopTest(); 
            
        }
    }
    
    @isTest
    private static void controllerColonTest() {
    
        Test.startTest();
        System.debug('!@#TESTSTARTED');
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        
        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 

        System.runAs(portalUser) {
            
            Account portalOrderAcc = new Account(
            RecordTypeId = accRecId,
            Name = 'DEFAULT ACCOUNT FOR PATIENT'
            );
            insert portalOrderAcc;
            PageReference pageRef = Page.GHI_Portal_OIP_StartContinue;
            //pageRef.getParameters().put('logo', 'logo');          
            Test.setCurrentPage(pageRef);
            
            GHI_Portal_OIP_StartContinue_Controller controller = new GHI_Portal_OIP_StartContinue_Controller();
            //controller.newOrder.AccountId = portalOrderAcc.Id;
            controller.productSelect = 'Colon';
            controller.showNextRecords();
            controller.showPreviousRecords();
            controller.showFirstRecords();
            controller.showLastRecords();
            controller.getHasPreviousRecords();
            controller.getHasNextRecords();
            controller.goToNewOrderPhysician();
            controller.goToNewOrderPatient();
            
            controller.goToHelp();
            Schema.DescribeSObjectResult d = Order.SObjectType.getDescribe();
            System.debug('##OrderWirte? ' + d.isCreateable());
            Schema.DescribeSObjectResult c = Contact.SObjectType.getDescribe();
            System.debug('##ContactWrite? ' + c.isCreateable());
            controller.saveOrder();

        }
        
        Test.stopTest(); 
        
    }
    
    @isTest
    private static void controllerMMRReflexColTest() {
    
        Test.startTest();
        System.debug('!@#TESTSTARTED');
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        
        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 

        System.runAs(portalUser) {
            
            Account portalOrderAcc = new Account(
            RecordTypeId = accRecId,
            Name = 'DEFAULT ACCOUNT FOR PATIENT'
            );
            insert portalOrderAcc;
            PageReference pageRef = Page.GHI_Portal_OIP_StartContinue;
            //pageRef.getParameters().put('logo', 'logo');          
            Test.setCurrentPage(pageRef);
            
            GHI_Portal_OIP_StartContinue_Controller controller = new GHI_Portal_OIP_StartContinue_Controller();
            //controller.newOrder.AccountId = portalOrderAcc.Id;
            controller.productSelect = 'MMR Reflex to Colon';
            controller.showNextRecords();
            controller.showPreviousRecords();
            controller.showFirstRecords();
            controller.showLastRecords();
            controller.getHasPreviousRecords();
            controller.getHasNextRecords();
            controller.goToNewOrderPhysician();
            controller.goToNewOrderPatient();
            
            controller.goToHelp();
            Schema.DescribeSObjectResult d = Order.SObjectType.getDescribe();
            System.debug('##OrderWirte? ' + d.isCreateable());
            Schema.DescribeSObjectResult c = Contact.SObjectType.getDescribe();
            System.debug('##ContactWrite? ' + c.isCreateable());
            controller.saveOrder();

        }
        
        Test.stopTest(); 
        
    }
    
    @isTest
    private static void controllerMMRAndColTest() {
    
        Test.startTest();
        System.debug('!@#TESTSTARTED');
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        
        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 

        System.runAs(portalUser) {
            
            Account portalOrderAcc = new Account(
            RecordTypeId = accRecId,
            Name = 'DEFAULT ACCOUNT FOR PATIENT'
            );
            insert portalOrderAcc;
            PageReference pageRef = Page.GHI_Portal_OIP_StartContinue;
            //pageRef.getParameters().put('logo', 'logo');          
            Test.setCurrentPage(pageRef);
            
            GHI_Portal_OIP_StartContinue_Controller controller = new GHI_Portal_OIP_StartContinue_Controller();
            //controller.newOrder.AccountId = portalOrderAcc.Id;
            controller.productSelect = 'MMR and Colon';
            controller.showNextRecords();
            controller.showPreviousRecords();
            controller.showFirstRecords();
            controller.showLastRecords();
            controller.getHasPreviousRecords();
            controller.getHasNextRecords();
            controller.goToNewOrderPhysician();
            controller.goToNewOrderPatient();
            
            controller.goToHelp();
            Schema.DescribeSObjectResult d = Order.SObjectType.getDescribe();
            System.debug('##OrderWirte? ' + d.isCreateable());
            Schema.DescribeSObjectResult c = Contact.SObjectType.getDescribe();
            System.debug('##ContactWrite? ' + c.isCreateable());
            controller.saveOrder();

        }
        
        Test.stopTest(); 
        
    }  
    
    @isTest
    private static void controllerMMRTest() {
    
        Test.startTest();
        System.debug('!@#TESTSTARTED');
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        
        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 

        System.runAs(portalUser) {
            
            Account portalOrderAcc = new Account(
            RecordTypeId = accRecId,
            Name = 'DEFAULT ACCOUNT FOR PATIENT'
            );
            insert portalOrderAcc;
            PageReference pageRef = Page.GHI_Portal_OIP_StartContinue;
            //pageRef.getParameters().put('logo', 'logo');          
            Test.setCurrentPage(pageRef);
            
            GHI_Portal_OIP_StartContinue_Controller controller = new GHI_Portal_OIP_StartContinue_Controller();
            //controller.newOrder.AccountId = portalOrderAcc.Id;
            controller.productSelect = 'MMR';
            controller.showNextRecords();
            controller.showPreviousRecords();
            controller.showFirstRecords();
            controller.showLastRecords();
            controller.getHasPreviousRecords();
            controller.getHasNextRecords();
            controller.goToNewOrderPhysician();
            controller.goToNewOrderPatient();
            
            controller.goToHelp();
            Schema.DescribeSObjectResult d = Order.SObjectType.getDescribe();
            System.debug('##OrderWirte? ' + d.isCreateable());
            Schema.DescribeSObjectResult c = Contact.SObjectType.getDescribe();
            System.debug('##ContactWrite? ' + c.isCreateable());
            controller.saveOrder();

        }
        
        Test.stopTest(); 
        
    }   
    
    @isTest
    private static void controllerDCISTest() {
    
        Test.startTest();
        System.debug('!@#TESTSTARTED');
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        
        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 

        System.runAs(portalUser) {
            
            Account portalOrderAcc = new Account(
            RecordTypeId = accRecId,
            Name = 'DEFAULT ACCOUNT FOR PATIENT'
            );
            insert portalOrderAcc;
            PageReference pageRef = Page.GHI_Portal_OIP_StartContinue;
            //pageRef.getParameters().put('logo', 'logo');          
            Test.setCurrentPage(pageRef);
            
            GHI_Portal_OIP_StartContinue_Controller controller = new GHI_Portal_OIP_StartContinue_Controller();
            //controller.newOrder.AccountId = portalOrderAcc.Id;
            controller.productSelect = 'DCIS';
            controller.showNextRecords();
            controller.showPreviousRecords();
            controller.showFirstRecords();
            controller.showLastRecords();
            controller.getHasPreviousRecords();
            controller.getHasNextRecords();
            controller.goToNewOrderPhysician();
            controller.goToNewOrderPatient();
            
            controller.goToHelp();
            Schema.DescribeSObjectResult d = Order.SObjectType.getDescribe();
            System.debug('##OrderWirte? ' + d.isCreateable());
            Schema.DescribeSObjectResult c = Contact.SObjectType.getDescribe();
            System.debug('##ContactWrite? ' + c.isCreateable());
            controller.saveOrder();

        }
        
        Test.stopTest(); 
        
    }    
}