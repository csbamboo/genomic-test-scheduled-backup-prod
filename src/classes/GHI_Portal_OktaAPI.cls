/*
 * Class used to make API calls to Okta
 */
public with sharing class GHI_Portal_OktaAPI {
	
	public GHI_Portal_OktaAPI(){
	}
	
	public static Boolean isTest
	{
		get{
			return GHI_Portal_Okta__c.getOrgDefaults().GHI_Portal_Okta_Test_Prod_Box_Instance__c;
		}
	}
	
	public static GHI_Portal_Okta__c GetSettings(Boolean isTest1){
		GHI_Portal_Okta__c settings;
		if(isTest1){
			settings = GHI_Portal_Okta__c.getInstance(UserInfo.getProfileId());
		}
		else{
			settings = GHI_Portal_Okta__c.getOrgDefaults();
		}
		
		return settings;
	}
	
	
	//Method called by GHI_Portal_UserTriggerHandler.onAfterUpdate
	public static void updateUsers(List<User> portalUsers){
		//Check if API calls are active
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		if(!settings.GHI_Portal_Okta_API_Active__c){
			return;
		}
		
		//serialize request so we can pass to @future method
		Map<String, NewProfile> jsonRequest = new Map<String, NewProfile>();
		
		List<String> usersToUpdate = new List<String>();
		String serializedUser;
		for(User pu: portalUsers){
		    System.debug('@**************************** user debug update unserialized: ' + pu);
		    serializedUser = JSON.serialize(pu);
		    System.debug('@**************************** user debug update serialized: ' + serializedUser);
			usersToUpdate.add(serializedUser);
			
			//limit of 100 callouts
			if(usersToUpdate.size() == Limits.getLimitCallouts()){
				GHI_Portal_OktaAPI.updateUsersAsync(usersToUpdate);
				usersToUpdate.clear();
			}
		}
		
		//call @future method to do call-out
		//maximum @future call is 50
		if(!usersToUpdate.isEmpty()){
			GHI_Portal_OktaAPI.updateUsersAsync(usersToUpdate);	
		}
	}
	
	
	//Async method to update users
	@future (callout=true)
	public static void updateUsersAsync(List<String> portalUsers){
		
		if(portalUsers.isEmpty()){
			return;
		}
		
		User portalUser = null;
		
		//itirate through list of users and create an Okta account
		for(String pu: portalUsers){
			portalUser = (User)JSON.deserialize(pu, User.class);
			System.debug('@**************************** user id debug: ' + portalUser.GHI_Portal_Okta_User_ID__c);
			if(portalUser.GHI_Portal_Okta_User_ID__c != null && portalUser.GHI_Portal_Okta_User_ID__c != ''){
				//make call-out to Okta and receive Okta User ID if successful
				GHI_Portal_OktaAPI.updateUser(portalUser.GHI_Portal_Okta_User_ID__c, portalUser.FirstName, portalUser.LastName, portalUser.Email, portalUser.Username, portalUser.GHI_Portal_Tools_Access__c, portalUser.GHI_Portal_Speaker_Portal_Access__c, portalUser.GHI_Portal_Tools_Tab_Default__c, portalUser.GHI_Portal_Box_Access__c);
			}	
		}
	}
	
	//Called by GHI_Portal_UserTriggerHandler.onAfterInsert()
	public static void createUsers(List<User> portalUsers){
		
		//Check if API calls are active
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		if(!settings.GHI_Portal_Okta_API_Active__c){
			return;
		}
		
		//serialize request so we can pass to @future method
		Map<String, NewProfile> jsonRequest = new Map<String, NewProfile>();
		
		List<String> usersToUpdate = new List<String>();
		String serializedUser;
		
		for(User user: portalUsers){
		    System.debug('@****************************** user debug unserialized: ' + user);
		    serializedUser = JSON.serialize(user);
		    System.debug('@****************************** user debug serialized: ' + serializedUser);
			usersToUpdate.add(serializedUser);
			
			//limit of 100 callouts
			if(usersToUpdate.size() == Limits.getLimitCallouts()){
				GHI_Portal_OktaAPI.createUsersAsync(usersToUpdate);
				usersToUpdate.clear();
			}
		}
		
		//call @future method to do call-out
		//maximum @future call is 50
		if(!usersToUpdate.isEmpty()){
			GHI_Portal_OktaAPI.createUsersAsync(usersToUpdate);	
		}
	}
	
	@future (callout=true)
	public static void createUsersAsync(List<String> portalUsers){
		
		if(portalUsers.isEmpty()){
			return;
		}
		
		User portalUser = null;
		List<User> updatedUsers = new List<User>();
		
		//itirate through list of users and create an Okta account
		for(String pu: portalUsers){
			portalUser = (User)JSON.deserialize(pu, User.class);
			System.debug('@****************************** user debug deserialized: ' + portalUser);
			System.debug('@************************ portal user debug ||  firstname:  ' + portalUser.FirstName +
			             ' || lastname: ' + portalUser.LastName +
			             ' || email: ' + portalUser.Email +
			             ' || username: '+ portalUser.Username +
			             ' || GHI_Portal_Tools_Access__c: ' + portalUser.GHI_Portal_Tools_Access__c +
			             ' || GHI_Portal_Speaker_Portal_Access__c: ' + portalUser.GHI_Portal_Speaker_Portal_Access__c +
			             ' || GHI_Portal_Tools_Tab_Default__c: ' + portalUser.GHI_Portal_Tools_Tab_Default__c +
			             ' || GHI_Portal_Box_Access__c: ' + portalUser.GHI_Portal_Box_Access__c);
			//make call-out to Okta and receive Okta User ID if successful
			String oktaID = GHI_Portal_OktaAPI.createUser(portalUser.FirstName, portalUser.LastName, portalUser.Email, portalUser.Username, portalUser.GHI_Portal_Tools_Access__c, portalUser.GHI_Portal_Speaker_Portal_Access__c, portalUser.GHI_Portal_Tools_Tab_Default__c, portalUser.GHI_Portal_Box_Access__c);
			
			if(oktaID != null){
				portalUser.GHI_Portal_Okta_User_ID__c = oktaID;
				updatedUsers.add(portalUser);
			}
			
		}
		
		//update users with Okta ID
		if(updatedUsers.size() > 0){
			// turn off trigger
			TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
			triggerSwitch.GHI_Portal_UserTrigger__c = false;
			
			//adding logging functionality - Paul Wittmeyer 8/7/2015
			try{
				update updatedUsers;
			}catch(Exception ex){
				Logger.debugExceptionForAsync(ex);
			}
			
			
			
			triggerSwitch.GHI_Portal_UserTrigger__c = true;
		} 
	}
	
	
	
	
	/*
	 * Creates a user in Okta via API call
	 * @param	firstName	first name of the user
	 * @param	lastName	last name of the user
	 * @param	email		email address of the user
	 * @param	username	username of the user
	 * @param	active		flag to activate user in Okta
	 * @return				returns the Okta user id after successful creation. NULL if creation failed
	 * 
	*/
	public static String createUser(String firstName, String lastName, String email, String username, Boolean toolsAccess, Boolean speakerPortalAccess, String defaultToolsTab, Boolean boxAccess){
		
		String activityMsg = '';
		
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String apiurl = settings.GHI_Portal_Okta_API_Create_URL__c;
				
		NewProfile user = new NewProfile(firstName, lastName, email, username);
		user.toolsAccess = toolsAccess;
		user.speakerPortalAccess = speakerPortalAccess;
		user.defaultToolsTab = defaultToolsTab;
		//user.boxAccess = boxAccess;
		
		Map<String, NewProfile> jsonRequest = new Map<String, NewProfile>();
		jsonRequest.put('profile', user);
		System.debug('@******************* jsonRequest debug unserialized: ' + jsonRequest);
		String jsonReq = JSON.serializePretty(jsonRequest);
		System.debug('@******************* jsonReq debug serialized: ' + jsonReq);
		HttpResponse res = new HttpResponse();
		
		//We should save the response to a table for debugging
		res = GHI_Portal_OktaAPI.sendHttpRequest('POST', apiurl, jsonReq);
		
		System.debug('Response Status: ' + res.getStatusCode() + '\n');
		
		//Success
		if(res.getStatusCode() == 200 || res.getStatusCode() == 204){
			
			GHI_Portal_OktaProfile profile = GHI_Portal_OktaProfile.parse(res.getBody());
			activityMsg = 'Okta API Success: ' + res.getBody();
			
			String groupURL = settings.GHI_Portal_Okta_API_GroupPortal_URL__c.replace('{groupid}',settings.GHI_Portal_Okta_API_GroupPortal_ID__c) + profile.id;
			
			//Add newly created user into Portal group in Okta
			GHI_Portal_OktaAPI.sendHttpRequestNoPayload('PUT', groupURL);
			
			
			if(toolsAccess){
				addUserToRSPCGroup(profile.id);
			}
			
			
			if(speakerPortalAccess){
				addUserToSpeakerPortalGroup(profile.id);
			}
			
			if(boxAccess){
				addUserToBoxGroup(profile.id);
			}
			
			//Logger.logActivityForAsync(activityMsg);
			
			return profile.id;
		}
		
		//Error		
		if(res.getStatusCode() >= 400 && res.getStatusCode() <= 503){
			String errBody = res.getBody();
			System.debug('@************************* res getbody debug: ' + errBody);
			GHI_Portal_OktaErrorResponse errObj = GHI_Portal_OktaErrorResponse.parse(errBody);
			activityMsg = 'Okta API Error:' + '\nUsername: ' + username + '\nResponse: ' + res.getBody();
			System.debug('@************************* errObj debug: ' + errObj);
			if(errObj.errorCauses.size()>0){
				System.debug('\nError Summary:' + errObj.errorCauses[0].errorSummary);	
			}
			
			//Logger.logActivityForAsync(activityMsg);
			
			return null;
		}
			
		return null;
	}
	
	
	
	
	/*
	 * Creates a user in Okta via API call
	 * @param	firstName		first name of the user
	 * @param	lastName		last name of the user
	 * @param	email			email address of the user
	 * @param	username		username of the user
	 * @param	partialUpdate	flag to activate user in Okta
	 * @return					returns the Okta user id after successful creation. NULL if creation failed
	 * 
	*/
	public static string updateUser(String oktaUserId, String firstName, String lastName, String email, String username, Boolean toolsAccess, Boolean speakerPortalAccess, String defaultToolsTab, Boolean boxAccess){
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		String activityMsg = '';
		String apiurl = settings.GHI_Portal_Okta_API_Update_URL__c + oktaUserId;
		
		//fullURL = apiURL + updateUserURL + oktaUserId;
				
		NewProfile user = new NewProfile(firstName, lastName, email, username);
		user.toolsAccess = toolsAccess;
		user.speakerPortalAccess = speakerPortalAccess;
		user.defaultToolsTab = defaultToolsTab;
		
		Map<String, NewProfile> jsonRequest = new Map<String, NewProfile>();
		jsonRequest.put('profile', user);
		
		String jsonReq = JSON.serializePretty(jsonRequest);
		
		HttpResponse res = new HttpResponse();
		res = sendHttpRequest('POST', apiurl, jsonReq);
		
		System.debug('Response Status: ' + res.getStatusCode() + '\n');
		//Success
		if(res.getStatusCode() == 200 || res.getStatusCode() == 204){
			
			GHI_Portal_OktaProfile profile = GHI_Portal_OktaProfile.parse(res.getBody());
			activityMsg = 'Okta API Success: ' + res.getBody();
			//Logger.logActivityForAsync(activityMsg);
			
			if(toolsAccess){
				addUserToRSPCGroup(profile.id);
			}
			else{
				removeUserFromRSPCGroup(profile.id);
			}
			
			if(speakerPortalAccess){
				addUserToSpeakerPortalGroup(profile.id);
			}
			else{
				removeUserFromSpeakerPortalGroup(profile.id);
			}
			
			if(boxAccess){
				addUserToBoxGroup(profile.id);
			}
			else{
				removeUserFromBoxGroup(profile.id);
			}
			
			return profile.id;
		}
		
		//Error		
		if(res.getStatusCode() >= 400 || res.getStatusCode() <= 503){
			
			GHI_Portal_OktaErrorResponse errObj = GHI_Portal_OktaErrorResponse.parse(res.getBody());
			activityMsg = 'Okta API Error :' + '\nOkta User ID: ' + oktaUserId + '\nUsername: ' + username + '\nResponse: ' + res.getBody();
			System.debug('\nError Summary:' + errObj.errorCauses[0].errorSummary);
			//Logger.logActivityForAsync(activityMsg);
			return null;
		}
			
		return null;
	}
	
	
	public static void deactivateUsers(List<User> portalUsers){
		//Check if API calls are active
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		if(!settings.GHI_Portal_Okta_API_Active__c){
			return;
		}
		
		List<String> usersToUpdate = new List<String>();
		
		for(User user: portalUsers){
			usersToUpdate.add(JSON.serialize(user));
			
			//limit of 100 callouts
			if(usersToUpdate.size() == Limits.getLimitCallouts()){
				GHI_Portal_OktaAPI.deactivateUsersAsync(usersToUpdate);
				usersToUpdate.clear();
			}
		}
		
		//call @future method to do call-out
		//maximum @future call is 50
		if(!usersToUpdate.isEmpty()){
			GHI_Portal_OktaAPI.deactivateUsersAsync(usersToUpdate);	
		}
	}
		
	@future (callout=true)
	public static void deactivateUsersAsync(List<String> portalUsers){
		
		if(portalUsers.isEmpty()){
			return;
		}
		
		User portalUser = null;
		
		//itirate through list of users and create an Okta account
		for(String pu: portalUsers){
			portalUser = (User)JSON.deserialize(pu, User.class);
			
			if(portalUser.GHI_Portal_Okta_User_ID__c != null && portalUser.GHI_Portal_Okta_User_ID__c != ''){
				//make call-out to Okta and receive Okta User ID if successful
				GHI_Portal_OktaAPI.deactivateUser(portalUser.GHI_Portal_Okta_User_ID__c);
			}	
		}
	}
	
	public static void deactivateUser(String userID){
		String activityMsg = 'User ' + userID + ' DEACTIVATED';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String deactivateURL = settings.GHI_Portal_Deactivate_User_URL__c.replace('{userId}',userID);
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('POST', deactivateURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	
	public static void activateUsers(List<User> portalUsers){
		//Check if API calls are active
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		if(!settings.GHI_Portal_Okta_API_Active__c){
			return;
		}
		
		List<String> usersToUpdate = new List<String>();
		
		for(User user: portalUsers){
			usersToUpdate.add(JSON.serialize(user));
			
			//limit of 100 callouts
			if(usersToUpdate.size() == Limits.getLimitCallouts()){
				GHI_Portal_OktaAPI.activateUsersAsync(usersToUpdate);
				usersToUpdate.clear();
			}
		}
		
		//call @future method to do call-out
		//maximum @future call is 50
		if(!usersToUpdate.isEmpty()){
			GHI_Portal_OktaAPI.activateUsersAsync(usersToUpdate);	
		}
	}
		
	@future (callout=true)
	public static void activateUsersAsync(List<String> portalUsers){
		
		if(portalUsers.isEmpty()){
			return;
		}
		
		User portalUser = null;
		
		//itirate through list of users and create an Okta account
		for(String pu: portalUsers){
			portalUser = (User)JSON.deserialize(pu, User.class);
			
			if(portalUser.GHI_Portal_Okta_User_ID__c != null && portalUser.GHI_Portal_Okta_User_ID__c != ''){
				//make call-out to Okta and receive Okta User ID if successful
				GHI_Portal_OktaAPI.activateUser(portalUser.GHI_Portal_Okta_User_ID__c);
			}	
		}
	}
	
	public static void activateUser(String userID){
		String activityMsg = 'User ' + userID + ' ACTIVATED';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String activateURL = settings.GHI_Portal_Activate_User_URL__c.replace('{userId}',userID);
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('POST', activateURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	
	public static void addUserToPortalGroup(String userID){
		String activityMsg = 'User ' + userID + ' added to PORTAL group';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String groupURL = settings.GHI_Portal_Okta_API_GroupPortal_URL__c.replace('{groupid}',settings.GHI_Portal_Okta_API_GroupPortal_ID__c) + userID;
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('PUT', groupURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static void addUserToRSPCGroup(String userID){
		String activityMsg = 'User ' + userID + ' added to TOOLS group';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String groupURL = settings.GHI_Portal_Okta_API_GroupPortal_URL__c.replace('{groupid}',settings.GHI_Portal_Okta_API_GroupRSPC_ID__c) + userID;
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('PUT', groupURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static void removeUserFromRSPCGroup(String userID){
		String activityMsg = 'User ' + userID + ' removed from TOOLS group';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String groupURL = settings.GHI_Portal_Okta_API_GroupPortal_URL__c.replace('{groupid}',settings.GHI_Portal_Okta_API_GroupRSPC_ID__c) + userID;
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('DELETE', groupURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static void addUserToSpeakerPortalGroup(String userID){
		String activityMsg = 'User ' + userID + ' added to SPEAKER PORTAL group';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String groupURL = settings.GHI_Portal_Okta_API_GroupPortal_URL__c.replace('{groupid}',settings.GHI_Portal_API_GroupSpeakerPortal_ID__c) + userID;
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('PUT', groupURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static void removeUserFromSpeakerPortalGroup(String userID){
		String activityMsg = 'User ' + userID + ' removed from SPEAKER PORTAL group';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String groupURL = settings.GHI_Portal_Okta_API_GroupPortal_URL__c.replace('{groupid}',settings.GHI_Portal_API_GroupSpeakerPortal_ID__c) + userID;
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('DELETE', groupURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static void addUserToBoxGroup(String userID){
		String activityMsg = 'User ' + userID + ' added to BOX group';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String groupURL = settings.GHI_Portal_Okta_API_GroupPortal_URL__c.replace('{groupid}',settings.GHI_Portal_Okta_API_GroupBox_ID__c) + userID;
		
		HttpResponse res = GHI_Portal_OktaAPI.sendHttpRequestNoPayload('PUT', groupURL);
		
		//Error		
		if(res.getStatusCode() >= 400 || res.getStatusCode() <= 503){
			activityMsg = 'Error adding to Box'; 
			//Logger.logActivityForAsync(activityMsg);
		}
		//else{
		//	Logger.logActivityForAsync(activityMsg);
		//}
	}
	
	public static void removeUserFromBoxGroup(String userID){
		String activityMsg = 'User ' + userID + ' removed from BOX group';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String groupURL = settings.GHI_Portal_Okta_API_GroupPortal_URL__c.replace('{groupid}',settings.GHI_Portal_Okta_API_GroupBox_ID__c) + userID;
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('DELETE', groupURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static void resetUserPassword(String userID){
		String activityMsg = 'User ' + userID + ' password reset';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String apiURL = settings.GHI_Portal_Reset_Password_URL__c.replace('{userId}',userID);
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('POST', apiURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static void resetUserMFA(String userID){
		String activityMsg = 'User ' + userID + ' MFA reset';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String apiURL = settings.GHI_Portal_Reset_MFA__c.replace('{userId}',userID);
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('POST', apiURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static void unlockUser(String userID){
		String activityMsg = 'User ' + userID + ' unlocked';
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		
		String apiURL = settings.GHI_Portal_Unlock_User__c.replace('{userId}',userID);
		
		GHI_Portal_OktaAPI.sendHttpRequestNoPayload('POST', apiURL);
		
		//Logger.logActivityForAsync(activityMsg);
	}
	
	public static HttpResponse sendHttpRequest(String httpMethod, String url, String request){
		
		HttpRequest req = new HttpRequest();
		HttpResponse res = new HttpResponse();
		Http http = new Http();
		
		
		System.debug('\nURL >> ' + url + '\n');
		System.debug('Request In >> \n' + request + '\n');
		
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		String apiToken = settings.GHI_Portal_Okta_API_Token__c;
		
		System.debug('Token >> \n' + apiToken + '\n');
		
		String authorizationHeader = 'SSWS ' + apiToken;
		
		req.setHeader('Accept', 'application/json');
		req.setHeader('Content-Type', 'application/json');
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Cache-Control', 'no-cache');
		req.setEndpoint(url);
		req.setMethod(httpMethod);
		req.setBody(request);
		
		System.debug('Request: \n' + req.toString() + '\n');		
		
		try{
			res = http.send(req);
			
			System.debug('Response \n' + res.getBody() + '\n');
		}
		catch(System.CalloutException e){
			//Logger.debugExceptionForAsync(e);
			res = null;
			System.debug('HTTP Send Error ' + e);
			System.debug('Response Error: \n' + res.toString() + '\n');		
		}
		
		return res;
	}
	
	public static HttpResponse sendHttpRequestNoPayload(String httpMethod, String url){
		
		HttpRequest req = new HttpRequest();
		HttpResponse res = new HttpResponse();
		Http http = new Http();
		
		
		System.debug('\nURL >> ' + url + '\n');
		
		GHI_Portal_Okta__c settings = GetSettings(GHI_Portal_OktaAPI.isTest);
		String apiToken = settings.GHI_Portal_Okta_API_Token__c;
		
		System.debug('\nToken >> ' + apiToken + '\n');
		
		String authorizationHeader = 'SSWS ' + apiToken;
		
		req.setHeader('Accept', 'application/json');
		req.setHeader('Content-Type', 'application/json');
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Cache-Control', 'no-cache');
		req.setEndpoint(url);
		req.setMethod(httpMethod);
		//req.setBody(request);
		
		System.debug('Request: \n' + req.toString() + '\n');		
		
		try
		{
			res = http.send(req);
			System.debug('\nResponse Status >>' + res.getStatusCode() + '\n');
			System.debug('\nResponse Body >>' + res.getBody() + '\n');
		}
		catch(System.CalloutException e){
			//Logger.debugExceptionForAsync(e);
			res = null;
			System.debug('HTTP Send Error ' + e);		
		}
		
		return res;
	}
	
	
		
	/*
	 * Profile object used for Okta API calls
	*/
	public class NewProfile {
		
		public String firstName;
		public String lastName;
		public String email;
		public String login;
		public Boolean toolsAccess = false;
		public Boolean speakerPortalAccess = false;
		public String defaultToolsTab = 'Intro';
		//public Boolean boxAccess = false;
		
		public NewProfile(String fName, String lName, String emailIn, String userName){
			firstName = fName;
			lastName = lName;
			email = emailIn;
			login = userName;
		}
	}
	
	public class NewError{
    	public String errorCode;
    	public String errorSummary;
    	public String errorLink;
    	public String errorId;
    	
    	public NewError(String errCode, String errSum, String errLink, String errId){
    	    errorCode = errCode;
    	    errorLink = errLink;
    	    errorSummary = errSum;
    	    errorId = errId;
    	}
	}
	
}