@isTest//(seeAllData = true)
private class GHI_Portal_OktaAPI_Test {

    @testsetup 
    static void setup() {
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;
        
        portalUser.GHI_Portal_Okta_User_ID__c = portalUser.Id;
        update portalUser;
    }
    
    public static List<SObject> getAllFields(String sObj, Set<Id> idList, Integer recLimit){
        String selects = '';
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(sObj).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ 
                Schema.DescribeFieldResult fd = ft.getDescribe(); 
                // if (fd.isCreateable()){ 
                    selectFields.add(fd.getName());
                // }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
        
        String queryBuilder = 'select ' + selects + ' from ' + sObj + (idList != null ? ' where Id in :idList ' : '') + (recLimit != null ? ' limit :recLimit ' : '');
        
        return Database.query(queryBuilder);
    }

    @isTest
	static void testStatusOkay() {
// 		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
        
        GHI_Portal_Settings__c customSetting = new GHI_Portal_Settings__c();
        customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
 	    insert customSetting;
 	    
 	    GHI_Portal_Okta__c oktaSettings = new GHI_Portal_Okta__c();
 	    oktaSettings = GHI_Portal_TestUtilities.createPortalOktaSettings(); 
 	    insert oktaSettings;
        
// 		List<User> portalUsers = new List<User>{GHI_Portal_TestUtilities.createPortalUser()};
// 		insert portalUsers;
		
		List<User> portalUsers = [SELECT Id, ContactId, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
		Set<Id> idList = new Set<Id>();
		
		Test.startTest();
		
        for(User pu : portalUsers){
            idList.add(pu.Id);
        }
        portalUsers = (List<User>) getAllFields('User', idList, null);
        
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_HttpCalloutMock(true));

		GHI_Portal_OktaAPI.createUsers(portalUsers);
		GHI_Portal_OktaAPI.updateUsers(portalUsers);
		GHI_Portal_OktaAPI.deactivateUsers(portalUsers);
		GHI_Portal_OktaAPI.activateUsers(portalUsers);
		
		GHI_Portal_OktaAPI.addUserToPortalGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.addUserToRSPCGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.removeUserFromRSPCGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.addUserToSpeakerPortalGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.removeUserFromSpeakerPortalGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.addUserToBoxGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.removeUserFromBoxGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.resetUserPassword(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.resetUserMFA(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.unlockUser(portalUsers[0].CommunityNickname);
		
		Test.stopTest();
	}
	
	@isTest
	static void testStatusFail() {
// 		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
        
        GHI_Portal_Settings__c customSetting = new GHI_Portal_Settings__c();
        customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
 	    insert customSetting;
 	    
 	    GHI_Portal_Okta__c oktaSettings = new GHI_Portal_Okta__c();
 	    oktaSettings = GHI_Portal_TestUtilities.createPortalOktaSettings(); 
 	    insert oktaSettings;
        
// 		List<User> portalUsers = new List<User>{GHI_Portal_TestUtilities.createPortalUser()};
// 		insert portalUsers;
		
		List<User> portalUsers = [SELECT Id, ContactId, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
		Set<Id> idList = new Set<Id>();
		
		Test.startTest();
		
        for(User pu : portalUsers){
            idList.add(pu.Id);
        }
        portalUsers = (List<User>) getAllFields('User', idList, null);
        
        GHI_Portal_HttpCalloutMock httpMock = new GHI_Portal_HttpCalloutMock(false);
		Test.setMock(HttpCalloutMock.class, httpMock);
        
		GHI_Portal_OktaAPI.createUsers(portalUsers);
		GHI_Portal_OktaAPI.updateUsers(portalUsers);
		GHI_Portal_OktaAPI.deactivateUsers(portalUsers);
		GHI_Portal_OktaAPI.activateUsers(portalUsers);
		
		GHI_Portal_OktaAPI.addUserToPortalGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.addUserToRSPCGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.removeUserFromRSPCGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.addUserToSpeakerPortalGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.removeUserFromSpeakerPortalGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.addUserToBoxGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.removeUserFromBoxGroup(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.resetUserPassword(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.resetUserMFA(portalUsers[0].CommunityNickname);
		GHI_Portal_OktaAPI.unlockUser(portalUsers[0].CommunityNickname);
		
		Test.stopTest();
	}
	
    @isTest
	static void testCreateUpdateOkay() {
// 		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
        
        GHI_Portal_Settings__c customSetting = new GHI_Portal_Settings__c();
        customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
 	    insert customSetting;
 	    
 	    GHI_Portal_Okta__c oktaSettings = new GHI_Portal_Okta__c();
 	    oktaSettings = GHI_Portal_TestUtilities.createPortalOktaSettings(); 
 	    insert oktaSettings;
        
// 		List<User> portalUsers = new List<User>{GHI_Portal_TestUtilities.createPortalUser()};
// 		insert portalUsers;
		
		List<User> portalUsers = [SELECT Id, ContactId, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
		Set<Id> idList = new Set<Id>();
		
		Test.startTest();
		
        for(User pu : portalUsers){
            idList.add(pu.Id);
        }
        portalUsers = (List<User>) getAllFields('User', idList, null);
        
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_HttpCalloutMock(true));

		GHI_Portal_OktaAPI.createUsers(portalUsers);
		
		Test.stopTest();
	}
	
    @isTest
	static void testCreateUpdateFail() {
// 		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
        
        GHI_Portal_Settings__c customSetting = new GHI_Portal_Settings__c();
        customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
 	    insert customSetting;
 	    
 	    GHI_Portal_Okta__c oktaSettings = new GHI_Portal_Okta__c();
 	    oktaSettings = GHI_Portal_TestUtilities.createPortalOktaSettings(); 
 	    insert oktaSettings;
        
// 		List<User> portalUsers = new List<User>{GHI_Portal_TestUtilities.createPortalUser()};
// 		insert portalUsers;
		
		List<User> portalUsers = [SELECT Id, ContactId, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
		Set<Id> idList = new Set<Id>();
		
		Test.startTest();
		
        for(User pu : portalUsers){
            idList.add(pu.Id);
        }
        portalUsers = (List<User>) getAllFields('User', idList, null);
        
        GHI_Portal_HttpCalloutMock httpMock = new GHI_Portal_HttpCalloutMock(false);
		Test.setMock(HttpCalloutMock.class, httpMock);
        
		GHI_Portal_OktaAPI.createUsers(portalUsers);
		GHI_Portal_OktaAPI.updateUsers(portalUsers);
		
		Test.stopTest();
	}
	
	@isTest
	static void testLimitCallouts() {
        GHI_Portal_Settings__c customSetting = new GHI_Portal_Settings__c();
        customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
 	    insert customSetting;
 	    
 	    GHI_Portal_Okta__c oktaSettings = new GHI_Portal_Okta__c();
 	    oktaSettings = GHI_Portal_TestUtilities.createPortalOktaSettings(); 
 	    insert oktaSettings;
 	    
		User portalUser = [SELECT Id, ContactId, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
		Set<Id> idList = new Set<Id>();
		Account testAccount;
        List<Contact> testContact = new List<Contact>(); 
        Profile p = [SELECT Id FROM Profile WHERE Name = 'GHI Customer Community Plus' LIMIT 1];  
        List<User> portalUsers = new List<User>();
        Integer num = 101;
		
		Test.startTest();
		
        System.runAs(portalUser) {
            testAccount = GHI_Portal_TestUtilities.createAccountHCO(); 
            Database.insert(testAccount);
            
            for(Integer i = 0; i < num; i++){
                testContact.add(GHI_Portal_TestUtilities.createContactPatient(testAccount.Id));
            }
            Database.insert(testContact);
        } 
        
        for(Integer i = 0; i < num; i++){
            portalUsers.add(new User(
                ProfileId = p.id, 
                Username = 'jsp'+i+'@test.com',
                Email = 'jsparrow'+i+'@test.com',
                EmailEncodingKey = 'UTF-8', 
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                TimezoneSidKey = 'America/Los_Angeles',
                Alias = 'jspar', 
                LastName = 'Sparrow',
                FirstName = 'Jack',
                CommunityNickname = 'Jspar'+i,
                GHI_Portal_Order_Workflow__c = 'Domestic', 
                GHI_Portal_Tools_Access__c = true, 
                GHI_Portal_Speaker_Portal_Access__c = true, 
                GHI_Portal_Box_Access__c = true, 
                ContactId = testContact[i].Id
            ));
        }
        insert portalUsers;
        
        for(User pu : portalUsers){
            idList.add(pu.Id);
        }
        portalUsers = (List<User>) getAllFields('User', idList, null);
        
        Test.setMock(HttpCalloutMock.class, new GHI_Portal_HttpCalloutMock(true));

        GHI_Portal_OktaAPI okaAPI = new GHI_Portal_OktaAPI(); //just to cover the constructor code
        
		GHI_Portal_OktaAPI.createUsers(portalUsers);
		GHI_Portal_OktaAPI.updateUsers(portalUsers);
		GHI_Portal_OktaAPI.deactivateUsers(portalUsers);
		GHI_Portal_OktaAPI.activateUsers(portalUsers);
		
		Test.stopTest();
	}
    
}


/*
@isTest
private class GHI_Portal_OktaAPI_Test {
	
	@isTest
	static void testSettings() {
		
		GHI_Portal_Okta__c settings = new GHI_Portal_Okta__c();
		settings = GHI_Portal_OktaAPI.GetSettings(true);
		
		System.assertEquals(settings.GHI_Portal_Activate_User_URL__c.Contains('okta.com'), true);

	}

	@isTest
	static void createOneUserAsync() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		
		System.debug('createOneUserAsync portalUsers size:' + portalUsers.size());
		
		List<String> usersToUpdate = new List<String>();
		String serializedUser;
		for(User pu: portalUsers){
		    serializedUser = JSON.serialize(pu);
		    usersToUpdate.add(serializedUser);
		}
		GHI_Portal_OktaAPI.createUsersAsync(usersToUpdate);
		
	}
	
	@isTest
	static void createOneUser() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		
		GHI_Portal_OktaAPI.createUsers(portalUsers);
		
	}
	
	@isTest
	static void create101User() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(101);
		
		GHI_Portal_OktaAPI.createUsers(portalUsers);
		
	}
	
	@isTest
	static void updateOneUserAsync() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		
		List<String> usersToUpdate = new List<String>();
		String serializedUser;
		for(User pu: portalUsers){
		    serializedUser = JSON.serialize(pu);
		    usersToUpdate.add(serializedUser);
		}
		GHI_Portal_OktaAPI.updateUsersAsync(usersToUpdate);
		
	}
	
	@isTest
	static void updateOneUser() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		
		GHI_Portal_OktaAPI.updateUsers(portalUsers);
		
	}
	
	@isTest
	static void update101User() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(101);
		
		GHI_Portal_OktaAPI.updateUsers(portalUsers);
		
	}
	
	@isTest
	static void activateUser() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		for(User u: portalUsers){
			if(u.GHI_Portal_Okta_User_ID__c != null){
				GHI_Portal_OktaAPI.activateUser(u.GHI_Portal_Okta_User_ID__c);	
			}
		}
	}
	
	@isTest
	static void addUserToPortalGroup() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		for(User u: portalUsers){
			GHI_Portal_OktaAPI.addUserToPortalGroup(u.GHI_Portal_Okta_User_ID__c);
		}
	}
	
	@isTest
	static void addUserToRSPCGroup() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		for(User u: portalUsers){
			GHI_Portal_OktaAPI.addUserToRSPCGroup(u.GHI_Portal_Okta_User_ID__c);
		}
	}
	
	@isTest
	static void removeUserFromRSPCGroup() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		for(User u: portalUsers){
			GHI_Portal_OktaAPI.removeUserFromRSPCGroup(u.GHI_Portal_Okta_User_ID__c);
		}
	}
	
	@isTest
	static void addUserToSpeakerPortalGroup() {
		// Set mock callout class 
		Test.setMock(HttpCalloutMock.class, new GHI_Portal_MockHttpResponseGenerator());
		
		List<User> portalUsers = GHI_Portal_Okta_Test_Utility.createUsers(1);
		for(User u: portalUsers){
			GHI_Portal_OktaAPI.addUserToSpeakerPortalGroup(u.GHI_Portal_Okta_User_ID__c);
		}
	}
    
}
*/