public with sharing class GHI_Portal_OktaErrorResponse {
    public String errorCode;
    public String errorSummary;
    public String errorLink;
    public String errorId;
    public List<ErrorCauses> errorCauses;

    public class ErrorCauses {
        public String errorSummary;
    }

    
    public static GHI_Portal_OktaErrorResponse parse(String json) {
        return (GHI_Portal_OktaErrorResponse) System.JSON.deserialize(json, GHI_Portal_OktaErrorResponse.class);
    }
    
    public static String testParse() {
        String json = '{\"errorCode\":\"E0000001\",\"errorSummary\":\"Api validation failed: login\",\"errorLink\":\"E0000001\",\"errorId\":\"oaeaYbNZlRSR5-fWygpY-r-Gg\",\"errorCauses\":[{\"errorSummary\":\"login: An object with this field already exists in the current organization\"}]}';
        return json;
    }
}