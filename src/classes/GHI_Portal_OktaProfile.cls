public with sharing class GHI_Portal_OktaProfile {
    public String id;
    public String status;
    public String created;
    public String activated;
    public String statusChanged;
    public String lastLogin;
    public String lastUpdated;
    public String passwordChanged;
    public Profile profile;
    public Credentials credentials;

    public class Profile {
        public String email;
        public String firstName;
        public String lastName;
        public String login;
        public String mobilePhone;
        public String secondEmail;
    }

    public class Activate_Z {
        public String href;
        public String method;
    }

    public class Provider {
        public String type;
        public String name;
    }

    public class Credentials {
        public Provider provider;
    }

    
    public static GHI_Portal_OktaProfile parse(String json) {
        return (GHI_Portal_OktaProfile) System.JSON.deserialize(json, GHI_Portal_OktaProfile.class);
    }
    
    public static String testResult() {
        String json = '{'+
        '   \"id\":\"12345dsd3w\",'+
        '   \"status\":\"STAGED\",'+
        '   \"created\":\"2015-04-13T17:44:53.000Z\",'+
        '   \"activated\":null,'+
        '   \"statusChanged\":null,'+
        '   \"lastLogin\":null,'+
        '   \"lastUpdated\":\"2015-04-13T17:44:53.000Z\",'+
        '   \"passwordChanged\":null,'+
        '   \"profile\":'+
        '   {'+
        '       \"email\":\"test@email.com\",'+
        '       \"firstName\":\"Test\",'+
        '       \"lastName\":\"Last\",'+
        '       \"login\":\"tlast1@ghi.odx\",'+
        '       \"mobilePhone\":null,'+
        '       \"secondEmail\":null'+
        '   },'+
        '   \"credentials\":'+
        '   {'+
        '       \"provider\":'+
        '       {'+
        '           \"type\":\"OKTA\",'+
        '           \"name\":\"OKTA\"'+
        '       }'+
        '   },'+
        '   \"_links\":'+
        '   {'+
        '       \"activate\":'+
        '       {'+
        '           \"href\":\"https://test.com/api/v1/users/00u3rmjtiyUGt7xkv0h7/lifecycle/activate\",'+
        '           \"method\":\"POST\"'+
        '       },'+
        '       \"deactivate\":'+
        '       {'+
        '           \"href\":\"https://test.com/api/v1/users/00u3rmjtiyUGt7xkv0h7/lifecycle/deactivate\",'+
        '           \"method\":\"POST\"'+
        '       }'+
        '   }'+
        '}';
        return json;
    }
}