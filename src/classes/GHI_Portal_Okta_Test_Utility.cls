/*----------------------------------------------------------------------------------------------
    Author:        Cliff Fajardo 
    Company:       Genomic Health, Inc.
    Description:   Utility class containing methods to create test data/ dummy records 
                  
    Test Class:
    History
    <Date>      <Authors Name>          <Brief Description of Change>
    07/30/2015  Cliff Fajardo           Created
---------------------------------------------------------------------------------------------*/

@isTest(SeeAllData=true)
public class GHI_Portal_Okta_Test_Utility {
	
	public static List<User> CreateUsers(Integer numUsers){
		List<User> portalUsers = new List<User>();
		
		User userWithRole; 
		
		//Make sure that user that will create Account and Contact has Role 
		if (UserInfo.getUserRoleId() == null) {
	 	    UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            userWithRole = new User(
                Alias = 'hasrole',
                Username = 'hasrole@test.com',
                CommunityNickname = 'hrole',
                Email = 'userwithrole@roletest1.com', 
                UserRoleId = r.Id,
                EmailEncodingKey = 'UTF-8', 
                Lastname = 'Testing', 
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', 
                ProfileId = UserInfo.getProfileId(),
                TimezoneSidKey = 'America/Los_Angeles'
            );
        } else {
        	userWithRole = new User(
        	    Id = UserInfo.getUserId(), 
        	    UserRoleId = UserInfo.getUserRoleId()
        	);
        }
        
        Account testAccount;
        // Contact testContact;
        List<Contact> conList = new List<Contact>(); 
        
        System.runAs(userWithRole) {
            testAccount = createAccountHCO(); 
            Database.insert(testAccount);
            
            for(Integer i = 0; i < numUsers; i++){
                conList.add(createContactHCP(testAccount.Id));
            }
            // testContact = createContactHCP(testAccount.Id);
            // Database.insert(testContact);
            insert conList;
        }
        
        //Get Portal Plus profile 
        Profile p = [SELECT Id FROM Profile WHERE Name = 'GHI Customer Community Plus' LIMIT 1];  
        
        String username;
        for(Integer i=0; i<numUsers; i++){
        	//Create Portal User 
        	
        	username = 'UOkta' + i;
	        User portalUser = new User(
	            ProfileId = p.id, 
	            Username = username + '@test.com',
	            Email = username + 'jsparrow@test.com',
	            EmailEncodingKey = 'UTF-8', 
	            LocaleSidKey = 'en_US',
	            LanguageLocaleKey = 'en_US',
	            TimezoneSidKey = 'America/Los_Angeles',
	            Alias = username, 
	            LastName = 'Okta' + i,
	            FirstName = 'User',
	            CommunityNickname = username,
	            GHI_Portal_Order_Workflow__c = 'Domestic', 
	            ContactId = conList[i].Id,
                GHI_Portal_Okta_User_ID__c = '12345dsd3w' + i,
                GHI_Portal_Tools_Access__c = false,
                GHI_Portal_Speaker_Portal_Access__c = false,
                GHI_Portal_View_Reports__c = false,
                GHI_Portal_Box_Access__c = false,
                GHI_Portal_Tools_Tab_Default__c = '');
            
            portalUsers.Add(portalUser);
        }
        insert portalUsers;
            
        return portalUsers;
	}
	
	public static Contact createContactHCP(Id acctId) {
    	Id rId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
    	    
		Contact hcpContact = new Contact(
		    RecordTypeId = rId,
            LastName = 'Sparrow',
            FirstName = 'Jack',
            AccountId = acctId, 
            OSM_Specialty__c = 'Urologist', 
            GHI_Portal_Contact_Source__c = 'Portal'
        );
    	return hcpContact; 
    }
    
    public static Account createAccountHCO() {
    	Id rId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();

		Account hcoAcct = new Account(
		    CurrencyIsoCode = 'USD',
		    Name = 'test account',
		    RecordTypeId = rId,
            BillingStreet = '123 Main St',
            BillingCity = 'Jersey City',
            BillingState = 'NJ',
            BillingPostalCode = '99999',
            BillingCountry = 'US',
            Phone = '555-555-5555',
            OSM_Specialty__c = 'Urology'
        );
    	return hcoAcct; 
    }

}