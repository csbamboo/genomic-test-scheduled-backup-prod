/*---------------------------------------------------------------------------------
    Author:         Stephen James Laylo
    Company:        Cloud Sherpas
    Description:    A controller for Order Details
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    03/30/2015      Stephen James Laylo     Created this controller
    06/16/2015      Katrina Guarina         Added method to retrieve PDF files from Static Content
    08/26/2015      Andrew Castillo         Changed fax and req form methods to be more dynamic
    
----------------------------------------------------------------------------------- */

public class GHI_Portal_OrderDetailsExt{

    public User user { get; set; }
    public Order newOrder { get; set; }
    public String orderPlacedBy { get; set; }
    public String displayOrders { get; set; }
    public String displayOwner { get; set; }
    public String ordCom { get; set; }
    public String disp { get; set; }
    private Id orderId;
    public Order myOrder { get; set; }
    public OrderItem addInfo { get; set; }
    public User orderOwner { get; set; }
    public Attachment newAttachment { get; set; }
    public Attachment existingAttachment { get; set; }
    public Boolean hasDescription { get; set; }
    public String attachId { get; set; }
    public string congaFaxString {get;set;}
    public string congaReqString {get;set;}
    public String paymentFormString {get;set;}
    public List<GHI_Portal_Static_Content__c> pscList;
    public String orderWorkflow { get; set; }
    public string orderNumber {get;set;}
    public string oliSelection {get;set;}
    public string reqTemplateId {get;set;}
    public string faxFax {get;set;}
    public string faxPhone {get;set;}
    public string sessionId {get;set;}
    public string attachmentId {get;set;} 
    public string faxAttachmentId {get;set;}
    public Boolean noReq { get; set; }
    public Boolean showCCpdf {get;set;}
    public Boolean refreshTimerOn {get;set;}
    public string target {get;set;}
    public string faxTarget {get;set;}
    public String pfTarget {get;set;}
    public String congaPDFPopupURL {get;set;}
    
    public String orderingContact {get;set;} 
    public String submittingContact {get;set;} 
    public String additionalContact {get;set;} 
    
    @TestVisible private OSM_Order_Role__c orderingOR; 
    
    public GHI_Portal_OrderDetailsExt(ApexPages.StandardController controller) {
    
        this.orderWorkflow = GHI_Portal_Utilities.getUserOrderWorkflow();
        List<Order> orderList = new List<Order>();
        this.displayOrders = ApexPages.currentPage().getParameters().get('ordId');
        this.ordCom = ApexPages.currentPage().getParameters().get('ordCom');
        this.disp = ApexPages.currentPage().getParameters().get('disp');
        this.attachId = ApexPages.currentPage().getParameters().get('attachId');
        this.newAttachment = new Attachment();
        this.myOrder = new Order();
        this.orderOwner = new User();
        this.showCCpdf = false;  
        this.refreshTimerOn = true;
        this.noReq = false;


        if (attachId != null && attachId != ''){
            List<Attachment> attachmentList = [SELECT Id, Name, Description, BodyLength 
                                               FROM Attachment 
                                               WHERE Id = :this.attachId];
            if (!attachmentList.isEmpty()) {
                this.existingAttachment = new Attachment();
                this.existingAttachment = attachmentList.get(0);
                this.newAttachment.Description = '';
            }
        }

        //get records based on the parameter
        system.debug('this.displayOrders:::' + this.displayOrders);
        system.debug('Utilities Class Boolean based on this.displayOrders' + GHI_Portal_Utilities.hasOrderAccess(this.displayOrders));
        /*if(!GHI_Portal_Utilities.hasOrderAccess(this.displayOrders)){
        	noAccess();
        }*/
        if (this.displayOrders != '' && this.displayOrders != null){
            Set<String> contactIdSet = new Set<String>(); 
            contactIdSet.add(GHI_Portal_Utilities.getPortalUserContact().Id);
            
            System.debug('contactIdSet: ' + contactIdSet); 
            for(OSM_Delegate__c tempContact : [SELECT OSM_HCP__c, OSM_Contact__c FROM OSM_Delegate__c WHERE OSM_Contact__c = :GHI_Portal_Utilities.getPortalUserContact().Id]) { 
                contactIdSet.add(tempContact.OSM_HCP__c);
            }
            
            List<Order> myOrders = new List<Order>([SELECT Id, CreatedById, OrderNumber, OSM_Patient__c, OSM_Product__c, OSM_OA_SR_Form_Orderable_OLI_Selection__c,
                                                           GHI_Portal_Archived__c, OSM_Patient__r.Name, 
                                                           OSM_Patient__r.BirthDate, OSM_Patient__r.OSM_Gender__c, 
                                                           (SELECT Id, Name, OSM_Contact__r.Name, OSM_Role__c FROM Order_Roles__r WHERE (OSM_Role__c = 'Ordering' OR 
                                                           OSM_Role__c = 'Specimen Submitting' OR OSM_Role__c = 'Additional') /*AND OSM_Contact__c IN :contactIdSet*/),
                                                           Type, EffectiveDate, OSM_Status__c, OSM_Estimated_Report_Date__c, 
                                                           GHI_Portal_Primary_Insurance_Name__c,
                                                           //OSM_Patient__r.GHI_Portal_Medical_Record_Number__c,
                                                           OSM_Patient_MRN__c, OSM_Bill_Type__c, 
                                                           GHI_Portal_Primary_Specimen_ID__c, 
                                                           OSM_Date_of_Collection_Surgery__c, createddate
                                                    FROM Order 
                                                    WHERE Id = :this.displayOrders
                                                    //AND CreatedById = :UserInfo.getUserId()
                                                    LIMIT 1]);
                                                    
            
            
            GHI_Portal_Conga_Template__c cs = GHI_Portal_Conga_Template__c.getOrgDefaults();
            sessionId = UserInfo.getSessionId();
            for(Order o :MyOrders){
                orderNumber = o.orderNumber;
                oliSelection = o.OSM_OA_SR_Form_Orderable_OLI_Selection__c;
                if(o.OSM_Bill_Type__c == 'Patient Pre-Pay'){
                    system.debug('OSM Bill TYPE ' + o.OSM_Bill_Type__c);
                    system.debug('SHOWCCPDF BEFORE ' + showCCpdf);
                    showCCpdf = true;
                    system.debug('SHOWCCPDF AFTER ' + showCCpdf);
                }
                if(o.OSM_Product__c.contains('Prostate')){
                    faxFax = cs.GH_Portal_Prostate_Fax__c;
                    faxPhone = cs.GHI_Portal_Prostate_Phone__c;
                    reqTemplateId = cs.GHI_Portal_ReqProstate_TemplateId__c;
                }else{
                    faxFax = cs.GHI_Portal_nonProstate_Fax__c;
                    faxPhone = cs.GHI_Portal_nonProstate_Phone__c;
                    reqTemplateId = cs.GHI_Portal_ReqNonProstate_TemplateId__c;
                }
                /*if(o.createddate < System.now().addMinutes(-5)){
                    refreshTimerOn = false;
                }*/
            }
            
            //ADDED: KGuarina 081715 
            if(showCCpdf == true) { 
                List<GHI_Portal_Static_Content__c> paymentFormObj = new List<GHI_Portal_Static_Content__c>(); 
                paymentFormObj = [SELECT Id, GHI_Portal_Content_Type__c, (SELECT Id FROM Attachments) FROM GHI_Portal_Static_Content__c WHERE 
                     GHI_Portal_Content_Type__c = 'Payment Form' LIMIT 1]; 
                //added null check - Paul Wittmeyer 9/18/2015
                if(paymentFormObj[0].Attachments[0].id != null){
                    Attachment tempAtt = paymentFormObj[0].Attachments[0];
                    paymentFormString = 'servlet/servlet.FileDownload?file=' + tempAtt.Id; 
                    pfTarget = '_blank'; 
                }
            }
                       
            /*List<Attachment> AttachmentList = new List<Attachment>([SELECT id, name from attachment where parentId = :this.displayOrders
                                                                                                            order by createddate asc]);
            system.debug('ATTACHMENTLIST' + attachmentlist);
            for(Attachment a : AttachmentList){
                if(a.name.contains('Prostate') || a.name.contains('BreastColon') || a.name.contains('Req') || a.name.contains('Acknowledgement')){
                    AttachmentId = a.id;
                }
                if(a.name.contains('Fax')){
                    faxAttachmentId = a.id;
                }
            }         
            if(AttachmentId != null){
                congaReqString = 'servlet/servlet.FileDownload?file=' + AttachmentId;
                target = '_blank';
            }else{
                congaReqstring = 'javascript:;';
                target = '_self';
            }
            if(faxAttachmentId != null){
                congaFaxString = 'servlet/servlet.FileDownload?file=' + faxAttachmentId;
                faxTarget = '_blank';
            }else{
                congaFaxstring = 'javascript:;';
                faxTarget = '_self';
            }*/
            if (myOrders.size() > 0) {
                this.myOrder = myOrders.get(0);
                System.debug('LOOP ORDER ROLES'); 
                if(myOrder.Order_Roles__r.size() > 0) {
                    System.debug('HAS ROLES'); 
                    List<OSM_Order_Role__c> orderRoleList = myOrder.Order_Roles__r; 
                    for(OSM_Order_Role__c currOR : orderRoleList) { 
                        if(currOR.OSM_Role__c == 'Ordering') { 
                            orderingContact = currOR.OSM_Contact__r.Name; 
                            orderingOR = currOR; 
                        }
                        if(currOR.OSM_Role__c == 'Specimen Submitting' && currOR.OSM_Contact__r.Name != null && currOR.OSM_Contact__r.Name != '') { 
                            submittingContact = currOR.OSM_Contact__r.Name; 
                        }
                        if(currOR.OSM_Role__c == 'Additional') { 
                            additionalContact = currOR.OSM_Contact__r.Name; 
                        }
                    }
                }
                
                list<OrderItem> addInfos = new list<OrderItem>([SELECT OrderId, Id, OSM_Chemotherapy__c,OSM_FEC_Prescribed__c,OSM_Date_of_Treatment_Decision__c, 
                                                                OSM_Order_Line_Item_Status__c, PricebookEntry.Name  
                                                                FROM OrderItem
                                                                WHERE OrderId = :myOrder.Id]);
                
                if(addInfos.size() > 0){
                    this.addInfo = addInfos.get(0);
                    System.debug('@@Value ' + addInfo.OSM_Chemotherapy__c);
                }
                    
            }
            
            this.displayOwner = ApexPages.currentPage().getParameters().get('ownrId');
            if (this.displayOwner != '' && this.displayOwner != null) {
                List<User> orderOwners = new List<User>([SELECT Id, Name 
                                                               FROM User 
                                                               WHERE Id = :this.displayOwner 
                                                               LIMIT 1]);
                if (orderOwners.size() > 0) {
                    this.orderOwner = orderOwners.get(0);
                }
            }        
        }
        
        //gets the current user's name and contact Id
        this.user = GHI_Portal_Utilities.getCurrentUser();
        
        //holder of the order Id from the parameter
        this.orderId = ApexPages.currentPage().getParameters().get('id');
        
        //default value of newOrder if there is no orderId parameter
        this.newOrder = new Order();
        
        this.orderPlacedBy = '';
        
        //checks if the parameter orderId has a value
        if (!(this.orderId == null && this.orderId == '')) {
        
            List<Order> newOrders = new List<Order>([SELECT Id, CreatedById, OrderNumber, OSM_Patient__c, OSM_Product__c, 
                                                            GHI_Portal_Archived__c, OSM_Patient__r.Name, 
                                                            OSM_Patient__r.BirthDate, OSM_Patient__r.OSM_Gender__c, 
                                                            (SELECT Id, Name, OSM_Contact__r.Name, OSM_Role__c FROM Order_Roles__r WHERE OSM_Role__c = 'Ordering' OR 
                                                            OSM_Role__c = 'Specimen Submitting' OR OSM_Role__c = 'Additional'),  
                                                            Type, EffectiveDate, OSM_Status__c, OSM_Estimated_Report_Date__c, 
                                                            //OSM_Patient__r.GHI_Portal_Medical_Record_Number__c 
                                                            OSM_Patient_MRN__c,
                                                            GHI_Portal_Primary_Specimen_ID__c, 
                                                            OSM_Date_of_Collection_Surgery__c 
                                                     FROM Order 
                                                     WHERE Id = :this.orderId 
                                                     AND CreatedById = :UserInfo.getUserId()
                                                     LIMIT 1]);
            
            if (newOrders.size() > 0) {
                //assigns the existing order;
                this.newOrder = newOrders.get(0);
                
                List<Contact> conts = new List<Contact>([SELECT Name 
                                                         FROM Contact 
                                                         WHERE Id = :this.newOrder.CreatedById 
                                                         LIMIT 1]);
                if (conts.size() > 0) {
                    this.orderPlacedBy = conts.get(0).Name;
                }
            }
        }
    }
    public PageReference saveAddInfo() {
        
        if (this.displayOrders != null && this.displayOrders != '') {
            
            Savepoint sp = Database.setSavepoint();
            
            List<OrderItem> ordersToUpdate = new List<OrderItem>([SELECT OrderId, Id, OSM_Chemotherapy__c,OSM_FEC_Prescribed__c,OSM_Date_of_Treatment_Decision__c 
                                                         FROM OrderItem
                                                         WHERE OrderId = :this.displayOrders
                                                         LIMIT 1]);
            
            if (ordersToUpdate.size() > 0) {
                OrderItem orderToUpdate = ordersToUpdate.get(0);

                        orderToUpdate.OSM_Chemotherapy__c = this.addInfo.OSM_Chemotherapy__c;
                        orderToUpdate.OSM_FEC_Prescribed__c = this.addInfo.OSM_FEC_Prescribed__c;
                        orderToUpdate.OSM_Date_of_Treatment_Decision__c = this.addInfo.OSM_Date_of_Treatment_Decision__c;
                        
                        try {
                            System.debug('## OrderUpdated ' + this.displayOrders);
                            System.debug('## Chemo ' + this.addInfo.OSM_Chemotherapy__c);
                            System.debug('## FEC ' + this.addInfo.OSM_FEC_Prescribed__c);
                            System.debug('## DOTD ' + this.addInfo.OSM_Date_of_Treatment_Decision__c);
                            update orderToUpdate;
                           
                        } catch (Exception err) {
                            //An exception was caught. We need to revert back to our Savepoint. This will rollback all successful changes.
                            System.debug('$$ Encounter' + err);
                            Database.rollback(sp);
                            return null;
                        }
        
                        return null;
            }
        }
        
        return null;
    }
    
    public List<Attachment> getAllAttachments() {
        List<Attachment> attachList = new List<Attachment>();
        
        if (this.displayOrders != '' && this.displayOrders != null) {
            attachList = new List<Attachment>([SELECT Name, Description, Bodylength, ParentId 
                                               FROM Attachment 
                                               WHERE ParentId = :this.displayOrders]);
        }
        
        if (attachList.size() > 0) {
            return attachList;
        }
        
        return attachList;
    }
    
    public PageReference uploadAttachment() {
        
        this.newAttachment.ParentId = this.displayOrders;
        
        /**if (this.newAttachment.Description == '' || this.newAttachment.Description == null) {
            this.hasDescription = true;
        }**/
        
        try {
            insert newAttachment; 
        } catch (DMLException e) {
            Logger.debugException(e);
            System.debug('@***************************exception debug: ' + e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));
            
            return null;
        }
        
        PageReference pr = ApexPages.currentPage();
        
        if (this.newAttachment.Id != null) {
            pr.getParameters().put('attachId', this.newAttachment.Id);
            
            //Create Case 
            Case newCase = new Case(); 
            newCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Generic').getRecordTypeId();
            newCase.Type = 'New Portal Documents'; 
            newCase.OSM_Primary_Order__c = this.displayOrders; 
            newCase.Priority = '4 - Urgent'; 
            newCase.OSM_Primary_Customer__c = orderingOR.Id; 
            newCase.OSM_Follow_Up_Date_Time__c = Datetime.now();
            newCase.Origin = 'Portal';
            
            if (Test.isRunningTest() == false) {
                AssignmentRule ar = new AssignmentRule();
                ar = [SELECT Id 
                      FROM AssignmentRule 
                      WHERE SobjectType = 'Case' 
                      AND Active = true 
                      LIMIT 1];
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                
                dmlOpts.assignmentRuleHeader.assignmentRuleId = ar.Id;
                newCase.setOptions(dmlOpts);
            }
            
            try{ 
                insert newCase; 
            }
            catch(Exception e) {
                Logger.debugException(e);
                System.debug('@***************************exception debug: ' + e);
            }
        }
        
        pr.setRedirect(true);
        
        return pr;
    }
    
    public List<GHI_Portal_Static_Content__c> getPscList(){ 
        System.debug('myOrder.GHI_Portal_Primary_Insurance_Name__c: ' + 
            myOrder.GHI_Portal_Primary_Insurance_Name__c);
        string InsNameString = '';
        if(myorder.GHI_Portal_Primary_Insurance_Name__c != null){
        	InsNameString = string.escapesinglequotes(myorder.GHI_Portal_Primary_Insurance_Name__c);
        }
      
        pscList = new List<GHI_Portal_Static_Content__c>(); 
        if(InsNameString.length() > 2){
	        pscList = database.query('SELECT Name, GHI_Portal_Product__c, GHI_Portal_Content_Type__c, ' +
	            'GHI_Portal_Locale__c, GHI_Portal_Static_Content__c, ' +
	            '(SELECT Id, Name, Description FROM Attachments) FROM GHI_Portal_Static_Content__c WHERE ' + 
	            'GHI_Portal_Content_Type__c = \'Insurance Coverage\' AND ' +
	            'GHI_Portal_Insurance_Company__c LIKE \'%' + InsNameString + '%\' '); 
        }
        /*pscList = [SELECT Name, GHI_Portal_Product__c, GHI_Portal_Content_Type__c,
             GHI_Portal_Locale__c, GHI_Portal_Static_Content__c, 
             (SELECT Id, Name, Description FROM Attachments) FROM GHI_Portal_Static_Content__c WHERE 
             GHI_Portal_Content_Type__c = 'Insurance Coverage' AND 
             GHI_Portal_Insurance_Company__c = :myOrder.GHI_Portal_Primary_Insurance_Name__c AND 
             GHI_Portal_Insurance_Company__c != null]; */
         System.debug('pscList: ' + pscList);
        return pscList; 
    }
    
    
    public pagereference reqNull(){
         /*-----------------------------------------------------------------------
        * Author        Paul Wittmeyer
        * Company       Cloud Sherpas
        * History       June-23-2015
        * Description   Ensures Conga has created an attachment for Order
        ----------------------------------------------------------------------- */
        GHI_Portal_Conga_Template__c cs = GHI_Portal_Conga_Template__c.getOrgDefaults();
        for(Attachment a : [SELECT id, name FROM attachment WHERE parentId = :this.displayOrders ORDER BY createddate ASC]){
            if(a.name.contains('PortalOrderForm')){
                AttachmentId = a.id;
            }
        }  
        if(attachmentId != null){
            String contentURL = ApexPages.currentPage().getHeaders().get('X-Salesforce-Forwarded-To');
            //PageReference pr = new pagereference('https://' + contentURL + '/servlet/servlet.FileDownload?file=' + AttachmentId);  
            //return pr;
            //congaPDFPopupURL = 'https://' + contentURL + '/servlet/servlet.FileDownload?file=' + AttachmentId;
            congaPDFPopupURL = cs.Communities_sites_url__c +'/servlet/servlet.FileDownload?file=' + AttachmentId;
            return null;     
        }else{
            noReq = TRUE;
            return null;
        }
    } 
    public pagereference reqNullFax(){ 
         /*-----------------------------------------------------------------------
        * Author        Paul Wittmeyer
        * Company       Cloud Sherpas
        * History       June-23-2015
        * Description   Ensures Conga has created a fax cover sheet for Order
        ----------------------------------------------------------------------- */
        GHI_Portal_Conga_Template__c cs = GHI_Portal_Conga_Template__c.getOrgDefaults();
        for(Attachment a : [SELECT id, name FROM attachment WHERE parentId = :this.displayOrders ORDER BY createddate ASC]){
            if(a.name.contains('PortalFaxCoverSheet')){
                faxAttachmentId = a.id;
            }
        }  
        if(faxAttachmentId != null){
            String contentURL =  ApexPages.currentPage().getHeaders().get('X-Salesforce-Forwarded-To'); 
            //PageReference pr = new pagereference('https://' + contentURL + '/servlet/servlet.FileDownload?file=' + faxAttachmentId);  
            //return pr;  
            //congaPDFPopupURL = 'https://' + contentURL + '/servlet/servlet.FileDownload?file=' + faxAttachmentId;
            congaPDFPopupURL = cs.Communities_sites_url__c +'/servlet/servlet.FileDownload?file=' + faxAttachmentId;
            return null; 
        }else{
            noReq = TRUE;
            return null;
        }
    }
    public pagereference Access(){ 
         /*-----------------------------------------------------------------------
        * Author        Paul Wittmeyer
        * Company       Cloud Sherpas
        * History       October 2, 2015
        * Description   Redirects to error page if user does not have access
        ----------------------------------------------------------------------- */
        if(!GHI_Portal_Utilities.hasOrderAccess(this.displayOrders)){
        	//pass orderId to ordershare to delete sharing rule if user shouldn't have access
        	GHI_Portal_Utilities.orderShare(this.displayOrders, false);
        	PageReference errorPage = new pagereference('/apex/GHI_Portal_Error'); 
        	return errorPage;
        }else{
        	//pass orderId to ordershare to create sharing rule if user has access
        	GHI_Portal_Utilities.orderShare(this.displayOrders, true);
        	return null;
        }
    }
        
}