@isTest
public class GHI_Portal_OrderDetailsExt_Test{
    
    @testsetup 
    private static void setup() {
        User portalUser = GHI_Portal_TestUtilities.createPortalUser();
        insert portalUser;  
    } 
     
    private static void setupUserData(Id conId){
        Account testAcct = GHI_Portal_TestUtilities.createAccountHCO(); 
        insert testAcct; 
        
        Account portalOrderAcc = GHI_Portal_TestUtilities.createDefaultAcc();
        insert portalOrderAcc;
        
        Customer_Affiliation__c testCA = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(
        testAcct.Id, conId, 'Primary'); 
        insert testCA;  
        
        Contact testContact = GHI_Portal_TestUtilities.createContactHCP(testAcct.Id); 
        testContact.OSM_Status__c = 'Draft';
        insert testContact; 
        
        Customer_Affiliation__c testCA2 = GHI_Portal_TestUtilities.createCNTtoCNTAffiliation(
        conId, testContact.Id); 
        insert testCA2; 
        
        Customer_Affiliation__c testCA3 = GHI_Portal_TestUtilities.createCNTtoACCTAffiliation(
        testAcct.Id, testContact.Id, 'Primary'); 
        insert testCA3;
        
        GHI_Portal_Settings__c customSetting = GHI_Portal_TestUtilities.createPortalSettings(); 
        insert customSetting;
        
        List<OSM_Orderable_Creation__c> orderable = GHI_Portal_TestUtilities.createOrderable();
        insert orderable;
        
        Record_Type__c patientRT = GHI_Portal_TestUtilities.createPatientRT();
        insert patientRT;
    }
    
    private static testMethod void testOrderDetailsExt1() {
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false; 
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Order_Territory_Assignment_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Order_Role_Trigger__c = false;
        insert triggerSwitch;
       
        
        
        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
        Contact userCon = [SELECT Id, GHI_Portal_Select_to_Enter__c FROM Contact WHERE Id = :portalUser.ContactId LIMIT 1];
        update userCon;
        
        setupUserData(portalUser.ContactId);
        
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
        Id ordRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id ordRoleRecType=Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Delegate').getRecordTypeId(); 
        
        System.runAs(portalUser) {
            Account portalOrderAcc = new Account(
                RecordTypeId = accRecId,
                Name = 'Default Ordering Account'
            );
            insert portalOrderAcc;
            
            Account portalPatAcc = GHI_Portal_TestUtilities.createAccountHCO();
            insert portalPatAcc;
            
            Contact portalPatCon = GHI_Portal_TestUtilities.createContactPatient(portalPatAcc.Id);
            portalPatCon.GHI_Portal_Select_to_Enter__c = 'Patient Identifier';
            portalPatCon.MailingCountry = 'US';
            portalPatCon.MailingState = 'NY';
            insert portalPatCon;
            
            Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
            insert prod2;
            
            Id pricebookStandard = Test.getStandardPricebookId();
            
            PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
            insert pbe;
            
            Order portalOrder = new Order(
                RecordTypeId = orderRecType,
                AccountId = portalPatAcc.Id,
                Name = 'TestPortalOrder',
                OSM_Study_ID__c = '123',
                OSM_Product__c = 'Unknown',
                OSM_Triage_Outcome__c = 'New', 
                GHI_Portal_Associated_Requisition__c = 'Requisition',
                GHI_Portal_CurrentStep__c = 1, 
                Order_Location__c = 'Domestic', 
                OSM_Bill_Type__c = 'Medicare',
                GHI_Portal_Secondary_Relation_to_Subs__c = 'Self', 
                GHI_Portal_Primary_Portal_Relation_Subs__c = 'Self', 
                OSM_Patient_MRN__c = 'MRN',
                Status = 'New',
                OSM_Status__c = 'Draft',
                EffectiveDate = Date.today(),
                Pricebook2Id = pricebookStandard
            );
            Insert portalOrder;

            Order portalOrder2 = new Order(
                RecordTypeId = ordRecType,
                AccountId = portalPatAcc.Id,
                Name = 'TestPortalOrder',
                OSM_Study_ID__c = '123',
                OSM_Product__c = 'Unknown',
                OSM_Triage_Outcome__c = 'New', 
                GHI_Portal_Associated_Requisition__c = 'Requisition',
                GHI_Portal_CurrentStep__c = 1, 
                Order_Location__c = 'Domestic', 
                OSM_Bill_Type__c = 'Medicare',
                GHI_Portal_Secondary_Relation_to_Subs__c = 'Self', 
                GHI_Portal_Primary_Portal_Relation_Subs__c = 'Self', 
                OSM_Patient_MRN__c = 'MRN',
                Status = 'New',
                OSM_Status__c = 'Draft',
                EffectiveDate = Date.today(),
                Pricebook2Id = pricebookStandard
            );
            Insert portalOrder2;
            
            List<OrderItem> oliList = new List<OrderItem>();
            for(Integer i = 0; i < 2; i++){
                oliList.add(new OrderItem(
                    OrderId = portalOrder.Id , 
                    PricebookEntryId = pbe.Id , 
                    OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                    UnitPrice = 2 , 
                    Quantity = 1
                ));
            }
            insert oliList;
            
            Attachment testAtt = new Attachment(
                Name = 'Prostate.jpg', 
                Body = Blob.valueOf('This is a sample attachment.'), 
                Description = 'This is a sample attachment.', 
                ParentId = portalOrder.Id 
            );
            insert testAtt;
        
            OSM_Order_Role__c testOrdRole = new OSM_Order_Role__c(
                OSM_Role__c = 'Ordering', 
                OSM_Order__c = portalOrder.Id, 
                OSM_Account__c = portalOrderAcc.Id
            );
            insert testOrdRole;
            
            OSM_Order_Role__c testOrdRole2 = new OSM_Order_Role__c(
                OSM_Role__c = 'Ordering', 
                OSM_Order__c = portalOrder2.Id, 
                OSM_Account__c = portalOrderAcc.Id
            );
            insert testOrdRole2;
            
            GHI_Portal_Static_Content__c portalStatic = new GHI_Portal_Static_Content__c(
                GHI_Portal_Content_Type__c = 'Payment Form'
            );
            insert portalStatic;
            
            Attachment testAtt2 = new Attachment(
                Name = 'Prostate.jpg', 
                Body = Blob.valueOf('This is a sample attachment.'), 
                Description = 'This is a sample attachment.', 
                ParentId = portalStatic.Id 
            );
            insert testAtt2;

            Attachment testAtt3 = new Attachment(
                Name = 'Prostate.jpg', 
                Body = Blob.valueOf('This is a sample attachment.'), 
                Description = 'This is a sample attachment.', 
                ParentId = portalOrder.Id 
            );
            insert testAtt3;
            
            PageReference pageRef = Page.GHI_Portal_OrderDetails;
            test.setCurrentPage(pageRef);
            
            PageReference pr; 
            List<Attachment> testAttList;
            List<GHI_Portal_Static_Content__c> testStatCont;
            
            ApexPages.StandardController sc; 
            GHI_Portal_OrderDetailsExt controller;
            
            /***********************************************************************************/
            
            System.currentPageReference().getParameters().put('ordId', portalOrder.id);
            System.currentPageReference().getParameters().put('id', portalOrder.id);
            System.currentPageReference().getParameters().put('disp', portalOrder.id);
            System.currentPageReference().getParameters().put('attachId', testAtt.id);
            System.currentPageReference().getParameters().put('ownrId', portalUser.id);
            
            sc = new ApexPages.standardController(portalOrder);
            controller = new GHI_Portal_OrderDetailsExt(sc);
            pr = controller.saveAddInfo();
            testAttList = controller.getAllAttachments();
            pr = controller.uploadAttachment();
            testStatCont = controller.getPscList();
            pr = controller.reqNull();
            
            /***********************************************************************************/
            
            System.currentPageReference().getParameters().put('attachId', '');
            System.currentPageReference().getParameters().put('ordId', portalOrder.id);
            System.currentPageReference().getParameters().put('disp', portalOrder.id);
            System.currentPageReference().getParameters().put('ownrId', portalUser.id);
            System.currentPageReference().getParameters().put('id', portalOrder.id);
            
            portalOrder.OSM_Product__c = 'Prostate';
            update portalOrder;
            testOrdRole.OSM_Role__c = 'Specimen Submitting';
            update testOrdRole;
            
            sc = new ApexPages.standardController(portalOrder);
            controller = new GHI_Portal_OrderDetailsExt(sc);
            pr = controller.saveAddInfo();
            testAttList = controller.getAllAttachments();
            //pr = controller.uploadAttachment();
            testStatCont = controller.getPscList();
            pr = controller.reqNull();
            
            /***********************************************************************************/
            
            System.currentPageReference().getParameters().put('ordId', portalOrder.id);
            System.currentPageReference().getParameters().put('disp', portalOrder.id);
            System.currentPageReference().getParameters().put('attachId', testAtt.id);
            System.currentPageReference().getParameters().put('ownrId', portalUser.id);
            System.currentPageReference().getParameters().put('id', portalOrder.id);
            
            testAtt.Name = 'PortalOrderForm';
            update testAtt;
            testOrdRole.OSM_Role__c = 'Additional';
            update testOrdRole;
            
            sc = new ApexPages.standardController(portalOrder);
            controller = new GHI_Portal_OrderDetailsExt(sc);
            pr = controller.saveAddInfo();
            testAttList = controller.getAllAttachments();
            // pr = controller.uploadAttachment();
            testStatCont = controller.getPscList();
            pr = controller.reqNull();
            pr = controller.reqNullFax();
            
            /***********************************************************************************/
            
            System.currentPageReference().getParameters().put('attachId', '');
            System.currentPageReference().getParameters().put('ordId', portalOrder.id);
            System.currentPageReference().getParameters().put('disp', portalOrder.id);
            System.currentPageReference().getParameters().put('ownrId', portalUser.id);
            System.currentPageReference().getParameters().put('id', portalOrder.id);
            
            portalOrder.OSM_Bill_Type__c = 'Patient Pre-Pay';
            update portalOrder;
            testOrdRole.OSM_Role__c = 'Specimen Submitting';
            update testOrdRole;
            
            
            sc = new ApexPages.standardController(portalOrder);
            controller = new GHI_Portal_OrderDetailsExt(sc);
            pr = controller.saveAddInfo();
            testAttList = controller.getAllAttachments();
            // pr = controller.uploadAttachment();
            testStatCont = controller.getPscList();
            pr = controller.reqNull();
            

            /***********************************************************************************/
            
            System.currentPageReference().getParameters().put('ordId', portalOrder.id);
            System.currentPageReference().getParameters().put('attachId', testAtt3.id);
            
            testAtt3.Name = 'PortalFaxCoverSheet';
            update testAtt3;
            testOrdRole.OSM_Role__c = 'Additional';
            update testOrdRole;
            
            sc = new ApexPages.standardController(portalOrder2);
            controller = new GHI_Portal_OrderDetailsExt(sc);
            pr = controller.saveAddInfo();
            testAttList = controller.getAllAttachments();
            // pr = controller.uploadAttachment();
            testStatCont = controller.getPscList();
            pr = controller.reqNullFax();

            /***********************************************************************************/
        test.startTest();    
            System.currentPageReference().getParameters().put('ordId', portalOrder2.id);
            
            sc = new ApexPages.standardController(portalOrder2);
            controller = new GHI_Portal_OrderDetailsExt(sc);
            pr = controller.saveAddInfo();
            testAttList = controller.getAllAttachments();
            // pr = controller.uploadAttachment();

            /***********************************************************************************/
            
            System.currentPageReference().getParameters().put('ordId', portalOrder2.id);

            sc = new ApexPages.standardController(portalOrder2);
            controller = new GHI_Portal_OrderDetailsExt(sc);
            // pr = controller.uploadAttachment();

        }
        
        test.stopTest();
    }
    
     @isTest
     static void controllerTest2(){
         
         GHI_Portal_Static_Content__c portalStatic = new GHI_Portal_Static_Content__c(
            GHI_Portal_Content_Type__c = 'Payment Form'
        );
        insert portalStatic;
        
        Attachment testAtt2 = new Attachment(
            Name = 'Prostate.jpg', 
            Body = Blob.valueOf('This is a sample attachment.'), 
            Description = 'This is a sample attachment.', 
            ParentId = portalStatic.Id 
        );
        insert testAtt2; 
        
        test.startTest();

        User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
        setupUserData(portalUser.ContactId);
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();

        //System.runAs(portalUser) {

            Account portalOrderAcc = new Account(
                RecordTypeId = accRecId,
                Name = 'Default Ordering Account'
                );
            insert portalOrderAcc;
            
            Order portalOrder = new Order(
                 RecordTypeId = orderRecType,
                 AccountId = portalOrderAcc.Id,
                 //Account = portalOrderAcc,
                 Name = 'TestPortalOrder',
                 OSM_Study_ID__c = '123',
                 //OSM_Study_Name__c = 'test study name', 
                 //OSM_Patient__c = portalPatient.Id,
                 OSM_Product__c = 'Prostate',
                 OSM_Triage_Outcome__c = 'New', 
                 GHI_Portal_Associated_Requisition__c = 'Requisition',
                 GHI_Portal_CurrentStep__c = 1, 
                 Order_Location__c = 'Domestic', 
                 OSM_Bill_Type__c = 'Patient Pre-Pay',
                //  OSM_Related_Order__c = ,
                //  OSM_Related_Order__r.OrderNumber = ,
                 OSM_Patient_MRN__c = 'MRN',
                 Status = 'New',
                 OSM_Status__c = 'Draft',
                 EffectiveDate = Date.today()
             );
            Insert portalOrder;
            
            Id recType1 = Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Account & Contact').getRecordTypeId(); 
            
            
        Contact testContact = GHI_Portal_TestUtilities.createContactHCP(portalOrderAcc.Id); 
        testContact.OSM_Status__c = 'Draft';
        insert testContact; 
            
            //Order Role
            OSM_Order_Role__c orderRole = OSM_DataFactory.createOrderRole(portalOrder.Id, portalOrderAcc.Id, testContact.Id);
                        orderRole.OSM_Account_Attention_To__c = 'attention';
                        orderRole.OSM_Account_Phone__c  = '1123';
                        orderRole.OSM_Account_Address_Line_2__c  = 'address22';
                        orderRole.Office_Contact__c = 'karla';
                        orderRole.OSM_Account_Name__c = 'reytas';
                        orderRole.OSM_Order_Role_Fax_Email__c  = 'email@cloudsherpas.com';
                        orderRole.OSM_Account_Fax__c = '1234567896';
                        orderRole.OSM_Account_Phone__c = '2311231224';
                        orderRole.OSM_Account_State__c = 'CA';
                        orderRole.OSM_Delegate_Contact__c  = portalUser.ContactId;
                        orderRole.OSM_State_Code__c = 'CA';
                        orderRole.RecordTypeId = recType1;
                        orderRole.OSM_Role__c = 'Ordering';
                        orderRole.OSM_Account_Country__c = 'US';
            insert orderRole;
            
            orderRole.OSM_Role__c = 'Ordering';
            update orderRole;
            
            PageReference pageRef = Page.GHI_Portal_OIP_Billing;
            test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('ordId', portalOrder.id);
            
            System.debug('#orderId ' + portalOrder.id);
            System.debug('#pageLink ' + pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(portalOrder);

            GHI_Portal_OrderDetailsExt controller = new GHI_Portal_OrderDetailsExt(sc);

            controller.hasDescription = false;
            controller.congaFaxString = 'congaFaxString';
            controller.congaReqString = 'congaReqString';
            controller.target = 'target';
            controller.faxTarget = 'faxTarget';

            
            
            controller.newAttachment.Description = 'Test Description';
            controller.newAttachment.Name = 'Prostate.jpg';
            controller.newAttachment.Body = Blob.valueOf('This is a sample attachment.');
            controller.saveAddInfo();
            controller.getAllAttachments();
            controller.OrderingOR = orderRole;
            controller.uploadAttachment();

            
        //}
        
        test.stopTest(); 

     }
    
}