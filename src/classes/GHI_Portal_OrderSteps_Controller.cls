/*------------------------------------------------------------------------------
    Author:         Stephen James Laylo
    Company:        Cloud Sherpas
    Description:    A controller for Order Step Component
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    04/17/2015      Stephen James Laylo     Created this controller for
                                            Order Steps Component
    04/20/2015      Stephen James Laylo     Updated to include Internation WorkFlow
--------------------------------------------------------------------------------*/

public class GHI_Portal_OrderSteps_Controller {
    
    //Constant Declarations & Initializations
    private final String STR_ERROR = 'error';
    private final String STR_COMPLETED = 'completed';
    private final String STR_DOMESTIC = 'Domestic';
    private final String STR_INTL_PARTNER = 'Intl-Partner';
    private final String STR_INTL_NON_PARTNER = 'Intl-Non Partner';
    private final String STR_ID = 'id';
    private final String STR_COUNTRY_UK = 'United Kingdom';
    
    public String orderId { get; set; }
    public GHI_Portal_Utilities.ValidatorWrapperClass vwc { get; set; }
    
    public String step1Status { get; set; }
    public String step2Status { get; set; }
    public String step3Status { get; set; }
    public String step4Status { get; set; }
    public String orderWorkflow { get; set; }
    public String error { get; set; }
    public String debs { get; set; }
    
    public User currentUser { get; set; }
    
    public Order newOrder { get; set; }

    public GHI_Portal_OrderSteps_Controller() {
    
        this.orderId = ApexPages.currentPage().getParameters().get(STR_ID);
        List<Order> orders = new List<Order>([SELECT GHI_Portal_CurrentStep__c, Order_Location__c FROM Order WHERE Id = :this.orderId]);
        this.newOrder = new Order();
        this.orderWorkflow = GHI_Portal_Utilities.getUserOrderWorkflow();

        this.currentUser = GHI_Portal_Utilities.getCurrentUser();
        
        if (this.orderId != null && this.orderId != '') {
            this.vwc = new GHI_Portal_Utilities.ValidatorWrapperClass(this.orderId);
            this.error = vwc.debug;
            if (this.orderWorkflow == STR_INTL_NON_PARTNER && this.currentUser.Country == STR_COUNTRY_UK) {
                this.step1Status = (vwc.physicianHasError) ? STR_ERROR : STR_COMPLETED;
            } else {
                this.step1Status = (vwc.patientHasError) ? STR_ERROR : STR_COMPLETED;
            }
            
            this.step2Status = (vwc.billingHasError) ? STR_ERROR : STR_COMPLETED;
            
            if (this.orderWorkflow == STR_DOMESTIC || this.orderWorkflow == STR_INTL_NON_PARTNER) {
                if (this.orderWorkflow == STR_INTL_NON_PARTNER && this.currentUser.Country == STR_COUNTRY_UK) {
                    this.step3Status = (vwc.patientHasError) ? STR_ERROR : STR_COMPLETED;
                } else {
                    this.step3Status = (vwc.physicianHasError) ? STR_ERROR : STR_COMPLETED;
                }
                
                this.step4Status = (vwc.specimenHasError) ? STR_ERROR : STR_COMPLETED;
            } else {
                this.step3Status = (vwc.specimenHasError) ? STR_ERROR : STR_COMPLETED;
                this.step4Status = (vwc.physicianHasError) ? STR_ERROR : STR_COMPLETED;
            }
            
            if (orders.size() > 0) {
                this.newOrder = orders.get(0);
            }
        }
    
    }
    
    public PageReference goToNewOrderPatient() {
        PageReference pr = Page.GHI_Portal_OIP_Patient;
        pr.getParameters().put(STR_ID, this.orderId);
        
        pr.setRedirect(true);

        return pr;
    }

    public PageReference goToNewOrderBilling() {
        
        PageReference pr = Page.GHI_Portal_OIP_Billing;
        pr.getParameters().put(STR_ID, this.orderId);
        pr.setRedirect(true);

        return pr;
    }

    public PageReference goToNewOrderPhysician() {
        
        PageReference pr = Page.GHI_Portal_OIP_Physician;
        pr.getParameters().put(STR_ID, this.orderId);
        pr.setRedirect(true);

        return pr;
    }

    public PageReference goToNewOrderSpecimen() {
        PageReference pr = Page.GHI_Portal_OIP_Specimen;
        pr.getParameters().put(STR_ID, this.orderId);
        pr.setRedirect(true);

        return pr;
    }

    public PageReference goToNewOrderReview() {
        PageReference pr = Page.GHI_Portal_OIP_ReviewOrder;
        pr.getParameters().put(STR_ID, this.orderId);      
        pr.setRedirect(true);

        return pr;
    }

}