@isTest
public class GHI_Portal_OrderSteps_Controller_Test{
    
     @testsetup 
     static void setup() {
     	User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;
        
     }
     
     @isTest
     static void controllerTest(){
 	    test.startTest();
 	    User portalUser = [SELECT Id, ContactId, Username, CommunityNickname FROM User WHERE Username = 'jsparrow@test.com' LIMIT 1];
 	    
 	    Order orderForParam = new Order(
            Status = 'New',
            OSM_Status__c = 'Draft',
            OSM_Triage_Outcome__c = 'New',
            EffectiveDate = Date.today(),
            OSM_Channel__c = 'Portal' 
            );
 	    
     	System.runAs(portalUser) {
     		PageReference pageRef = Page.GHI_Portal_OIP_Patient;
     		test.setCurrentPage(pageRef);
            String orderId = orderForParam.Id;
     		System.currentPageReference().getParameters().put('id', orderId);

     		
	     	GHI_Portal_OrderSteps_Controller controller = new GHI_Portal_OrderSteps_Controller();
            controller.goToNewOrderPatient();
            controller.goToNewOrderBilling();
            controller.goToNewOrderPhysician();
            controller.goToNewOrderSpecimen();
            controller.goToNewOrderReview();
            
	     	
	     	 
     	}
     	
     	test.stopTest(); 

     }
    
}