/*--------------------------------------------------------------------------
    @author         Alex Koleszar
    @company        Cloud Sherpas
    @description    Class create a secured restful web service URL to pass 
    				to user's browser
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    06/08/2015      Alex Koleszar         	Created this controller
    06/08/2015      Andrew Castillo     	Added comments and customer settings
---------------------------------------------------------------------------*/
public class GHI_Portal_Report_ServiceAPI {
    
    public static Boolean isTest {
		get {
			return false;
		}
	}
	
	private String DE_ID { get; set; }
	
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Constructor methods. Extracts the report ID from URL parameter
       @date          06/08/2015
       @param         N/A
       @return        N/A
       
       History
       06/08/2015     Andrew Castillo       Created
    ------------------------------------------------------------*/
	public GHI_Portal_Report_ServiceAPI() {
		
		DE_ID = ApexPages.currentPage().getParameters().get('id');
		System.debug('**** DE_ID: ' + DE_ID);
	}
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns the custom settings object for this class.
       @date          06/08/2015
       @param         GHI_Portal_Report__c - custom setting object
       @return        Boolean - is a test
       
       History
       06/08/2015     Andrew Castillo       Created
    ------------------------------------------------------------*/
    private static GHI_Portal_Report__c GetSettings(Boolean test) {
    	
		GHI_Portal_Report__c settings;
		if(test){
			settings = GHI_Portal_Report__c.getInstance(UserInfo.getProfileId());
		}
		else{
			settings = GHI_Portal_Report__c.getOrgDefaults();
		}
		
		return settings;
	}
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns the URL to the web service.
       @date          06/08/2015
       @param         String - unencrypted report URL
       @return        String - encrypted report URL
       
       History
       06/08/2015     Andrew Castillo		Created
    ------------------------------------------------------------*/
    public String getReportServiceURL(String repURL) {
    	
    	// get the endpoint URL
     	String serviceEndpoint = repURL.left(repURL.lastIndexOf('/'));
     	
     	System.debug('**** serviceEndpoint: ' + serviceEndpoint);
     	
     	// get report ID
     	String reportId = repURL.substring(repURL.lastIndexOf('/')+1);
     	
     	System.debug('**** reportId: ' + reportId);
        
        // get timestamp in seconds since  January 1, 1970, 00:00:00 GMT
        string timeStampInSeconds = String.ValueOf((DateTime.now().getTime()/1000));
        
        // get ecrypted report ID and timestamp
        string encryptedReportID = aes256Encrypt(timeStampInSeconds + ' ' + reportId);
        
        System.debug('**** encryptedReportID: ' + encryptedReportID);
                
        return serviceEndpoint + '/' + encryptedReportID;
    }
    
    /*------------------------------------------------------------
       @author        Alex Koleszar
       @company       Cloud Sherpas
       @description   Returns the redirect URL to the web service.
       @date          06/08/2015
       @param         PageReference
       @return        N/A
       
       History
       06/08/2015     Alex Koleszar		    Created
       06/08/2015     Andrew Castillo       Modified to use custom settings
    ------------------------------------------------------------*/
    public String getSecurePDFURL() {
    	
    	if (DE_ID == null || DE_ID == '') {
    		
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'No ID passed.'));
			
    		return null;
    	}
    	
    	// query for distribution event
    	OSM_Distribution_Event__c ODE = [SELECT Id, OSM_Report_URL__c, OSM_Order_ID__c
    									 FROM OSM_Distribution_Event__c
    									 WHERE Id = :DE_ID LIMIT 1];
    	
		// determine if invalid Distribution Event was passed
    	if (ODE == null) {
    		
    		// return null with no error message - prevents spoofing
    		return null;
    	}
    	
    	// determine if the user is a portal user and has access to the associated order
    	if (!GHI_Portal_Utilities.hasOrderAccess(ODE.OSM_Order_ID__c)) {
    		
    		// return null with no error message - prevents spoofing
    		return null;
    	}

		if (ODE.OSM_Report_URL__c == null || ODE.OSM_Report_URL__c == '') {
    		
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Report URL can not be empty.'));
			
    		return null;
    	}
    	
    	return getReportServiceURL(ODE.OSM_Report_URL__c);
    }
    
    /*------------------------------------------------------------
       @author        Alex Koleszar
       @company       Cloud Sherpas
       @description   Returns the redirect URL to the web service.
       @date          06/08/2015
       @param         PageReference
       @return        N/A
       
       History
       06/08/2015     Alex Koleszar		    Created
       06/08/2015     Andrew Castillo       Modified to use custom settings
    ------------------------------------------------------------*/
    public pageReference GetSecurePDF() {
     	
     	String secURL = getSecurePDFURL();
     	
		if (secURL == null) {
			
			return null;
		}
		
        //construct page reference URL
        PageReference pr = new PageReference(secURL);
        
        return pr;
    }
    
    /*------------------------------------------------------------
       @author        Alex Koleszar
       @company       Cloud Sherpas
       @description   Encrypts report ID and timestamp.
       @date          06/08/2015
       @param         String - report ID
       @return        String - encrypted and encoded report ID and timestamp
       
       History
       06/08/2015     Alex Koleszar		    Created
       06/08/2015     Andrew Castillo       Modified to user custom settings
    ------------------------------------------------------------*/
    private String aes256Encrypt(string repordId) {
    	
    	// get private key from custom settings
    	String keyStr = GetSettings(GHI_Portal_Report_ServiceAPI.isTest).GHI_Portal_Private_Key__c;
    	
        //32 byte string. since characters used are ascii, each char is 1 byte.
        Blob key = Blob.valueOf(keyStr);

        Blob cipherText = Crypto.encryptWithManagedIV('AES256', key, Blob.valueOf(repordId));
        
        // convert encrypted string to hex
	    String hexData = EncodingUtil.convertToHex(cipherText);
	    Blob macData = crypto.generateMac('HmacSHA256', Blob.valueOf(hexData),key);
	    String hexMac = EncodingUtil.convertToHex(macData);
	    
	    return (hexMac+hexData);
    }
}