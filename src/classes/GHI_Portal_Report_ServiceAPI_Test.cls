/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GHI_Portal_Report_ServiceAPI_Test {
		
	@testSetup 
	static void setup() {
        
        // create test account
		Account A = OSM_DataFactory.createAccount('New Account');
		insert A;

		// create test contact
		Id patientRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
		Contact C = OSM_DataFactory.createContact('David', 'Patient', patientRTId);
		C.AccountId = A.Id;
		insert C;

		// create test order
		Order O = OSM_DataFactory.createOrder('New Order', C.Id, A.Id, System.Today(), 'Active');
		insert O;
		
		// create test result
		OSM_Result__c R = new OSM_Result__c();
		R.Order_ID__c = O.Id;
		R.OSM_Results_Approval_Date__c = System.Today();
		insert R;
        
        // create test distribution event
        OSM_Distribution_Event__c DE = new OSM_Distribution_Event__c();
        DE.OSM_Order_ID__c = O.Id;
        DE.OSM_Report_URL__c = 'https://online-staging.genomichealth.com/TestDisplayReportService/api/v10/DisplayReport/test1.pdf';
        DE.OSM_Distribution_Status__c = 'Success';
        DE.OSM_Created_date__c = System.Today();
        DE.OSM_Lab_Task_ID__c = 'test111111';
        DE.OSM_Results_ID__c = R.Id;
        insert DE;

        DE = new OSM_Distribution_Event__c();
        DE.OSM_Order_ID__c = O.Id;
        DE.OSM_Distribution_Status__c = 'Success';
        DE.OSM_Created_date__c = System.Today();
        DE.OSM_Lab_Task_ID__c = 'test111111';
        DE.OSM_Results_ID__c = R.Id;
        insert DE;
        
        // create custom setting
        GHI_Portal_Report__c settings = GHI_Portal_Report__c.getOrgDefaults();
		settings.GHI_Portal_Private_Key__c = '12345678901234567890123456789012';
        insert settings;
        
        settings = GHI_Portal_Report__c.getInstance(UserInfo.getProfileId());
        settings.GHI_Portal_Private_Key__c = '12345678901234567890123456789012';
        insert settings;
    }
	
	@isTest
    static void myUnitTest() {
    	
    	for (OSM_Distribution_Event__c DE : [SELECT Id FROM OSM_Distribution_Event__c WHERE OSM_Lab_Task_ID__c = 'test111111']) {

	    	PageReference pageRef = Page.GHI_Portal_ReportService;
	        pageRef.getParameters().put('id', DE.Id);
	        Test.setCurrentPage(pageRef);
			GHI_Portal_Report_ServiceAPI controller = new GHI_Portal_Report_ServiceAPI();
			PageReference PR = controller.getSecurePDF();
    	}
    	
    	// negative test
		PageReference pageRef = Page.GHI_Portal_ReportService;
	    pageRef.getParameters().put('id', null);
	    Test.setCurrentPage(pageRef);
		GHI_Portal_Report_ServiceAPI controller = new GHI_Portal_Report_ServiceAPI();
		GHI_Portal_Report_ServiceAPI.isTest = true;
		String URL = controller.getSecurePDFURL();
    }
}