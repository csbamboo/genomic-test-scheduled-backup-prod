/*------------------------------------------------------------------------
    Author:        Paul Wittmeyer
    Company:       Cloud Sherpas
    Description:   A controller class created for for Portal signup functionality
                  
    Test Class:
    History:
    <Date>          <Authors Name>         <Brief Description of Change>
    05/18/2015      Paul Wittmeyer         Created
    05/28/2015      Sairah Hadjinoor       Updated Country and State picklist functionality
                    
--------------------------------------------------------------------------*/

public with sharing class GHI_Portal_SignUp_Controller {

    public String PatientContact { get; set; }

    //declaration of class level variables
    public string streetString {get;set;} 
    public string pmocString {get;set;}
    public Lead signupLead {get;set;}
    public List<OSM_Country_and_State__c> stateOptions {get;set;}
    public String selectedCountry {get;set;}
    public String selectedState {get;set;}
    public Boolean hasNoState {get;set;}
    public String selectedSpecialty {get;set;}
    public Boolean notDoctor {get;set;}
    public string emailString {get;set;}
    public string companyString {get;set;} 
    public string lastNameString {get;set;}  
    public boolean addError {get;set;}
    public boolean addErrorLN {get;set;}
    public boolean addErrorCompany {get;set;}
    public boolean addErrorEmail {get;set;}
    public boolean addErrorSpecialty {get;set;} 
    public List<lead> signupleadlist {get;set;}
    
    //constructor for page Lead
    public GHI_Portal_SignUp_Controller(){
        signupLead = new Lead();
        this.notDoctor = false;
        this.hasNoState = true;
        this.selectedCountry = 'United States';
        checkState();
        this.addError = false;
        this.addErrorLN = false;
        this.addErrorCompany = false;
        this.addErrorEmail = false;
        this.addErrorSpecialty = false;
    }
    public List<SelectOption> getAllCountries() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        List<AggregateResult> groupedResults = [SELECT OSM_Country_Name__c
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Country_Name__c != ''
                                                AND OSM_Country_Name__c != null
                                                GROUP BY OSM_Country_Name__c];
        
        for (AggregateResult country: groupedResults) {
            options.add(new SelectOption((String)country.get('OSM_Country_Name__c'),(String)country.get('OSM_Country_Name__c')));                                                       
        }
        return options;
    }
    
    public List<SelectOption> getAllState() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        if (selectedCountry!='' && selectedCountry!=null) {
            List<OSM_Country_and_State__c> stateOptions = new List<OSM_Country_and_State__c>([SELECT OSM_Country_Name__c,
                                                           OSM_State_Name__c
                                                           FROM OSM_Country_and_State__c 
                                                           WHERE OSM_Country_Name__c=:selectedCountry
                                                           AND OSM_State_Name__c!=:''
                                                           AND OSM_State_Name__c!=:null
                                                           ORDER BY OSM_State_Name__c]);
            
            for(OSM_Country_and_State__c country: stateOptions) {
                if (country.OSM_State_Name__c!='' && country.OSM_State_Name__c!=null) {                                           
                    options.add(new SelectOption(country.OSM_State_Name__c,country.OSM_State_Name__c)); 
                }                                                   
            } 
        }

        return options;
    }
    
    public void checkState(){
        if (getAllState().size() == 1) {
            hasNoState = false;
        } else {
            hasNoState = true;
        }
    }
    
    //method to set render flag for 'On Behalf of' field.
    public void checkSpecialty(){

        if(signupLead.GHI_Portal_Specialty__c == null){
            notDoctor = false;
        }else{
            notDoctor = true;
        }
        system.debug('NOTDOCTOR - after: ' + notdoctor);
    }
            
    //method to assign VF page values to a new Lead and insert
    public PageReference createSignUpLead(){
        signupLead.street = streetString;
        signupLead.status = 'Open';
        signupLead.GHI_Portal_Pref_Method_of_Communication__c = pmocString;
        signupLead.email = emailString;
        signupLead.company = companyString;
        signupLead.LastName = lastNameString;
        signupLead.State = selectedState;
        signupLead.Country = selectedCountry;
        System.debug('!@#signupLead ' + signupLead);
        if(signupLead.LastName == null || signupLead.LastName == ''){
            addErrorLN = true;
        }else{
        	addErrorLN = false;
        }
        if(signupLead.company == null || signupLead.company == ''){
            addErrorCompany = true;
        }else{
        	addErrorCompany = false;
        }
        if(signupLead.email == null || signupLead.email == '' || (!signupLead.email.contains('@')) ){
            addErrorEmail = true; 
        }else{
        	addErrorEmail = false;
        }
        if(signupLead.GHI_Portal_Specialty__c == null || signupLead.GHI_Portal_Specialty__c == ''){
        	addErrorSpecialty = true;
        }else{
    		addErrorSpecialty = false;
        }
        if(!addErrorLN && !addErrorCompany && !addErrorEmail && !addErrorSpecialty){
    		try{
    			insert signuplead;
    			//upon save, redirects to a thank you page
        		GHI_Portal_Customer_Provisioning_Wizard__c cpw = GHI_Portal_Customer_Provisioning_Wizard__c.getOrgDefaults();
            	Pagereference page = new PageReference (cpw.SignUp_Complete_sites_url__c);
            	return page;
			}catch(Exception ex){
				Logger.debugException(ex);
				return null;
			}
        }else{
        	return null;
        }
    }
}