@isTest
public class GHI_Portal_SignUp_Controller_Test{
  
  static testmethod void myUnitTest() {
  
       User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
    // List<Account> locList = new List<Account>();
    // List<Contact> physicianList = new List<Contact>(); 
    // List<OSM_Delegate__c> delegateList = new List<OSM_Delegate__c>(); 
     
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
        
        OSM_Country_and_State__c CounrtyAndState=new OSM_Country_and_State__c(); 
        CounrtyAndState.OSM_Country_Name__c='United States';
        CounrtyAndState.CurrencyIsoCode='USD';
        CounrtyAndState.OSM_National_Currency__c='USD - U.S. Dollar';
        CounrtyAndState.OSM_GHI_Currency__c='USD';
        CounrtyAndState.OSM_State_Name__c='North Carolina';
        insert CounrtyAndState;
        
        Test.startTest(); 
        
        GHI_Portal_SignUp_Controller c=new GHI_Portal_SignUp_Controller();        
        List<SelectOption> countryList=c.getAllCountries();
        List<SelectOption> stateList=c.getAllState();
        c.checkState();
        c.checkSpecialty();
        Pagereference page=c.createSignUpLead();  
        
        Test.stopTest();   
    }
    }
    
  static testmethod void myUnitTest2() {
  
         User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
     
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
        
        OSM_Country_and_State__c CounrtyAndState=new OSM_Country_and_State__c(); 
        CounrtyAndState.OSM_Country_Name__c='United States';
        CounrtyAndState.CurrencyIsoCode='USD';
        CounrtyAndState.OSM_National_Currency__c='USD - U.S. Dollar';
        CounrtyAndState.OSM_GHI_Currency__c='USD';
        CounrtyAndState.OSM_State_Name__c='North Carolina';
        insert CounrtyAndState;
        
        Test.startTest(); 
        
        GHI_Portal_SignUp_Controller c = new GHI_Portal_SignUp_Controller();        
        List<SelectOption> countryList = c.getAllCountries();
        List<SelectOption> stateList = c.getAllState();
        c.checkState();
        c.checkSpecialty();
        c.lastNameString = 'test';
        c.companyString = '';
        Pagereference page=c.createSignUpLead();  
        
        Test.stopTest();   
    }
    }
    
  static testmethod void myUnitTest3() {
        
         User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
     
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
        
        OSM_Country_and_State__c CounrtyAndState=new OSM_Country_and_State__c(); 
        CounrtyAndState.OSM_Country_Name__c='United States';
        CounrtyAndState.CurrencyIsoCode='USD';
        CounrtyAndState.OSM_National_Currency__c='USD - U.S. Dollar';
        CounrtyAndState.OSM_GHI_Currency__c='USD';
        CounrtyAndState.OSM_State_Name__c='North Carolina';
        insert CounrtyAndState;
        
        Test.startTest(); 
        
        GHI_Portal_SignUp_Controller c = new GHI_Portal_SignUp_Controller();        
        List<SelectOption> countryList = c.getAllCountries();
        List<SelectOption> stateList = c.getAllState();
        c.checkState();
        c.checkSpecialty();
        c.lastNameString = 'test';
        c.companyString = 'test';
        c.emailString = '';
        Pagereference page=c.createSignUpLead();  
        
        Test.stopTest();   
    }
    }
    
  static testmethod void myUnitTest4() {
        
         User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser){
     
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        insert portalUser;  
        
        OSM_Country_and_State__c CounrtyAndState=new OSM_Country_and_State__c(); 
        CounrtyAndState.OSM_Country_Name__c='United States';
        CounrtyAndState.CurrencyIsoCode='USD';
        CounrtyAndState.OSM_National_Currency__c='USD - U.S. Dollar';
        CounrtyAndState.OSM_GHI_Currency__c='USD';
        CounrtyAndState.OSM_State_Name__c='North Carolina';
        insert CounrtyAndState;
        
        Test.startTest(); 
        
        GHI_Portal_SignUp_Controller c = new GHI_Portal_SignUp_Controller();        
        List<SelectOption> countryList = c.getAllCountries();
        List<SelectOption> stateList = c.getAllState();
        c.checkState();
        c.checkSpecialty();
        c.PatientContact = 'test';
        c.selectedSpecialty = 'test';
        c.lastNameString = 'test';
        c.companyString = 'test';
        c.emailString = 'test@yahoo.com';
        GHI_Portal_Customer_Provisioning_Wizard__c cw = new GHI_Portal_Customer_Provisioning_Wizard__c(
            SignUp_Complete_sites_url__c = 'http://dev2-online-genomichealth.cs9.force.com/PortalSignUpComplete'
            );
        insert cw;
        try{
        Pagereference page=c.createSignUpLead();
        } catch(Exception ex){
                Logger.debugException(ex);
            }
        
        Test.stopTest();   
    }    
    }
}