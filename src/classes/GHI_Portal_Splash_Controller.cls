/*------------------------------------------------------------------------
    Author:        Paul Wittmeyer
    Company:       Cloud Sherpas
    Description:   A controller class created for Help Page
                  
    Test Class:
    History:
    <Date>          <Authors Name>         <Brief Description of Change>
    05/26/2015    Paul Wittmeyer            Created
--------------------------------------------------------------------------*/
public with sharing class GHI_Portal_Splash_Controller { 
    
    public List<GHI_Portal_Static_Content__c> portalStaticContent{get;set;} 
    public GHI_Portal_Static_Content__c portalContentRec{get;set;}
    private String contentType;
    public string Content {get;set;}

    public GHI_Portal_Splash_Controller(){
        contentType = 'Customer Provisioning Wizard Splash Page';      
        portalStaticContent = [SELECT Name, GHI_Portal_Content_Type__c, GHI_Portal_Locale__c, GHI_Portal_Static_Content__c 
                               FROM GHI_Portal_Static_Content__c 
                               WHERE GHI_Portal_Content_Type__c =:contentType LIMIT 1];

        if(portalStaticContent.size()!=0){
          //portalContentRec = portalStaticContent[0];
			for(GHI_Portal_Static_Content__c c : portalStaticContent){
				Content = c.GHI_Portal_Static_Content__c;
        	}
        }
    }  
}