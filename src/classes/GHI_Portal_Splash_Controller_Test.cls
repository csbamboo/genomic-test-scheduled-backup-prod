@isTest
public class GHI_Portal_Splash_Controller_Test{
  
  static testmethod void myUnitTest() {                 
        Test.startTest(); 
        GHI_Portal_Static_Content__c sc=new GHI_Portal_Static_Content__c();
        sc.GHI_Portal_Content_Type__c='Customer Provisioning Wizard Splash Page';
        sc.CurrencyIsoCode='USD';
        insert sc;
        
        GHI_Portal_Splash_Controller c=new GHI_Portal_Splash_Controller();         
        
        Test.stopTest();   
    }
}