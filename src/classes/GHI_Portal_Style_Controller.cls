/*------------------------------------------------------------------------
    Author:         Stephen James Laylo
    Company:        Cloud Sherpas
    Description:    A controller for GHI_Portal_Style Component
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    04/20/2015      Stephen James Laylo     Created this controller
---------------------------------------------------------------------------*/

public class GHI_Portal_Style_Controller {

    public String lang { get; set; }

    public GHI_Portal_Style_Controller() {
    
        this.lang = UserInfo.getLanguage() + '/';
        
        if (this.lang == 'en_US/') {
            this.lang = '';
        }
    
    }

}