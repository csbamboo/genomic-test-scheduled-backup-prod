/*----------------------------------------------------------------------------------------------
    Author:        Katrina Guarina 
    Company:       Cloud Sherpas
    Description:   Utility class containing methods to create test data/ dummy records 
                  
    Test Class:
    History
    <Date>      <Authors Name>          <Brief Description of Change>
    06/22/2015  Katrina Guarina           Created
    07/20/1015  Gypt Minierva             Create additional methods for test records.
    08/04/2015  Raus Kenneth Ablaza       Created additional method for Okta settings
---------------------------------------------------------------------------------------------*/
@isTest(SeeAllData=true)
public class GHI_Portal_TestUtilities {
    
    public static User createPortalUser() { 
        User userWithRole; 
        
        //Make sure that user that will create Account and Contact has Role 
        if (UserInfo.getUserRoleId() == null) {
            UserRole r = new UserRole(name = 'DEFAULT ACCOUNT FOR PATIENT Customer User');
            Database.insert(r);
            
            userWithRole = new User(
                Alias = 'hasrole',
                Username = 'hasrole@test.com',
                CommunityNickname = 'hrole',
                Email = 'userwithrole@roletest1.com', 
                UserRoleId = r.Id,
                EmailEncodingKey = 'UTF-8', 
                Lastname = 'Testing', 
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', 
                ProfileId = UserInfo.getProfileId(),
                TimezoneSidKey = 'America/Los_Angeles'
            );
        } else {
            userWithRole = new User(
                Id = UserInfo.getUserId(), 
                UserRoleId = UserInfo.getUserRoleId()
            );
        }
        
        Account testAccount;
        Contact testContact; 
        
        System.runAs(userWithRole) {
            testAccount = createAccountHCO(); 
            Database.insert(testAccount);
            
            testContact = createContactPatient(testAccount.Id);
            Database.insert(testContact);
        }
        
        //Get Portal Plus profile 
        Profile p = [SELECT Id FROM Profile WHERE Name = 'GHI Customer Community Plus' LIMIT 1];  
        
        //Create Portal User 
        User portalUser = new User(
            ProfileId = p.id, 
            Username = 'jsparrow@test.com',
            Email = 'jsparrow@test.com',
            EmailEncodingKey = 'UTF-8', 
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            TimezoneSidKey = 'America/Los_Angeles',
            Alias = 'jspar', 
            LastName = 'Sparrow',
            FirstName = 'Jack',
            CommunityNickname = 'Jspar',
            GHI_Portal_Order_Workflow__c = 'Domestic', 
            ContactId = testContact.Id);
            
        return portalUser;
    }
    
    public static User createPortalUserIntlUK() { 
        User userWithRole; 
        
        //Make sure that user that will create Account and Contact has Role 
        if (UserInfo.getUserRoleId() == null) {
            UserRole r = new UserRole(name = 'DEFAULT ACCOUNT FOR PATIENT Customer User');
            Database.insert(r);
            
            userWithRole = new User(
                Alias = 'hasrole',
                Username = 'hasroleIntlUK@test.com',
                CommunityNickname = 'hroleIntlUK',
                Email = 'userwithroleIntlUK@roletest1.com', 
                UserRoleId = r.Id,
                EmailEncodingKey = 'UTF-8', 
                Lastname = 'Testing', 
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', 
                ProfileId = UserInfo.getProfileId(),
                TimezoneSidKey = 'America/Los_Angeles'
            );
        } else {
            userWithRole = new User(
                Id = UserInfo.getUserId(), 
                UserRoleId = UserInfo.getUserRoleId()
            );
        }
        
        Account testAccount;
        Contact testContact; 
        
        System.runAs(userWithRole) {
            testAccount = createAccountHCO(); 
            Database.insert(testAccount);
            
            testContact = createContactPatient(testAccount.Id);
            Database.insert(testContact);
        }
        
        //Get Portal Plus profile 
        Profile p = [SELECT Id FROM Profile WHERE Name = 'GHI Customer Community Plus' LIMIT 1];  
        
        //Create Portal User 
        User portalUser = new User(
            ProfileId = p.id, 
            Username = 'jsparrowIntlUK@test.com',
            Email = 'jsparrow@test.com',
            EmailEncodingKey = 'UTF-8', 
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            TimezoneSidKey = 'America/Los_Angeles',
            Alias = 'jspar', 
            LastName = 'Sparrow',
            FirstName = 'Jack',
            CommunityNickname = 'Jsparintluk',
            GHI_Portal_Order_Workflow__c = 'Intl-Non Partner',
            country = 'United Kingdom',
            ContactId = testContact.Id);
            
        return portalUser;
    }
    
    public static User createPortalUserPartner() { 
        User userWithRole; 
        
        //Make sure that user that will create Account and Contact has Role 
        if (UserInfo.getUserRoleId() == null) {
            UserRole r = new UserRole(name = 'DEFAULT ACCOUNT FOR PATIENT Customer User');
            Database.insert(r);
            
            userWithRole = new User(
                Alias = 'hasrole',
                Username = 'hasrolepartner@test.com',
                CommunityNickname = 'hrolepartner',
                Email = 'userwithrolepartner@roletest1.com', 
                UserRoleId = r.Id,
                EmailEncodingKey = 'UTF-8', 
                Lastname = 'Testing', 
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', 
                ProfileId = UserInfo.getProfileId(),
                TimezoneSidKey = 'America/Los_Angeles'
            );
        } else {
            userWithRole = new User(
                Id = UserInfo.getUserId(), 
                UserRoleId = UserInfo.getUserRoleId()
            );
        }
        
        Account testAccount;
        Contact testContact; 
        
        System.runAs(userWithRole) {
            testAccount = createAccountHCO(); 
            Database.insert(testAccount);
            
            testContact = createContactPatient(testAccount.Id);
            Database.insert(testContact);
        }
        
        //Get Portal Plus profile 
        Profile p = [SELECT Id FROM Profile WHERE Name = 'GHI Customer Community Plus' LIMIT 1];  
        
        //Create Portal User 
        User portalUser = new User(
            ProfileId = p.id, 
            Username = 'jsparrowIntlpartner@test.com',
            Email = 'jsparrow@test.com',
            EmailEncodingKey = 'UTF-8', 
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            TimezoneSidKey = 'America/Los_Angeles',
            Alias = 'jspar', 
            LastName = 'Sparrow',
            FirstName = 'Jack',
            CommunityNickname = 'Jsparintlpartner',
            GHI_Portal_Order_Workflow__c = 'Intl-Partner', 
            ContactId = testContact.Id);
            
        return portalUser;
    }
    
        public static User createPortalUserIntl() { 
        User userWithRole; 
        
        //Make sure that user that will create Account and Contact has Role 
        if (UserInfo.getUserRoleId() == null) {
            UserRole r = new UserRole(name = 'DEFAULT ACCOUNT FOR PATIENT Customer User');
            Database.insert(r);
            
            userWithRole = new User(
                Alias = 'hasrole',
                Username = 'hasroleIntl@test.com',
                CommunityNickname = 'hroleIntl',
                Email = 'userwithroleIntl@roletest1.com', 
                UserRoleId = r.Id,
                EmailEncodingKey = 'UTF-8', 
                Lastname = 'Testing', 
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', 
                ProfileId = UserInfo.getProfileId(),
                TimezoneSidKey = 'America/Los_Angeles'
            );
        } else {
            userWithRole = new User(
                Id = UserInfo.getUserId(), 
                UserRoleId = UserInfo.getUserRoleId()
            );
        }
        
        Account testAccount;
        Contact testContact; 
        
        System.runAs(userWithRole) {
            testAccount = createAccountHCO(); 
            Database.insert(testAccount);
            
            testContact = createContactPatient(testAccount.Id);
            Database.insert(testContact);
        }
        
        //Get Portal Plus profile 
        Profile p = [SELECT Id FROM Profile WHERE Name = 'GHI Customer Community Plus' LIMIT 1];  
        
        //Create Portal User 
        User portalUser = new User(
            ProfileId = p.id, 
            Username = 'jsparrowIntl@test.com',
            Email = 'jsparrow@test.com',
            EmailEncodingKey = 'UTF-8', 
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            TimezoneSidKey = 'America/Los_Angeles',
            Alias = 'jspar', 
            LastName = 'Sparrow',
            FirstName = 'Jack',
            CommunityNickname = 'Jsparintl',
            GHI_Portal_Order_Workflow__c = 'Intl-Non Partner', 
            ContactId = testContact.Id);
            
        return portalUser;
    }
    
    public static List<User> createPortalUsers(Integer num) { 
        User userWithRole; 
        
        //Make sure that user that will create Account and Contact has Role 
        if (UserInfo.getUserRoleId() == null) {
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            userWithRole = new User(
                Alias = 'hasrole',
                Username = 'hasrole@test.com',
                CommunityNickname = 'hrole',
                Email = 'userwithrole@roletest1.com', 
                UserRoleId = r.Id,
                EmailEncodingKey = 'UTF-8', 
                Lastname = 'Testing', 
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', 
                ProfileId = UserInfo.getProfileId(),
                TimezoneSidKey = 'America/Los_Angeles'
            );
        } else {
            userWithRole = new User(
                Id = UserInfo.getUserId(), 
                UserRoleId = UserInfo.getUserRoleId()
            );
        }
        
        Account testAccount;
        List<Contact> testContact = new List<Contact>(); 
        
        System.runAs(userWithRole) {
            testAccount = createAccountHCO(); 
            Database.insert(testAccount);
            
            for(Integer i = 0; i < num; i++){
                testContact.add(createContactPatient(testAccount.Id));
            }
            Database.insert(testContact);
        }
        
        //Get Portal Plus profile 
        Profile p = [SELECT Id FROM Profile WHERE Name = 'GHI Customer Community Plus' LIMIT 1];  
        
        //Create Portal User 
        List<User> portalUsers = new List<User>();
        
        for(Integer i = 0; i < num; i++){
            portalUsers.add(new User(
                ProfileId = p.id, 
                Username = 'jsp'+i+'@test.com',
                Email = 'jsparrow'+i+'@test.com',
                EmailEncodingKey = 'UTF-8', 
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                TimezoneSidKey = 'America/Los_Angeles',
                Alias = 'jspar', 
                LastName = 'Sparrow',
                FirstName = 'Jack',
                CommunityNickname = 'Jspar'+i,
                GHI_Portal_Order_Workflow__c = 'Domestic', 
                ContactId = testContact[i].Id
            ));
        }
        return portalUsers;
    }
    
    public static Account createAccountHCO() {
        Id rId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();

        Account hcoAcct = new Account(
            CurrencyIsoCode = 'USD',
            Name = 'test account',
            RecordTypeId = rId,
            BillingStreet = '123 Main St',
            BillingCity = 'Jersey City',
            BillingState = 'NJ',
            BillingPostalCode = '99999',
            BillingCountry = 'US',
            Phone = '555-555-5555',
            OSM_Specialty__c = 'Urology'
        );
        return hcoAcct; 
    }
    
    public static Account createDefaultAcc() {
        Id rId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        
            Account portalOrderAcc = new Account(
            RecordTypeId = rId,
            Name = 'DEFAULT ACCOUNT FOR PATIENT',
            Phone = '213-123-1231',
            OSM_Specialty__c = 'Hospital'
             );
            return portalOrderAcc;
    }
    
    public static Contact createPortalPatient(Id portalOrder) {
           Id rId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
            
            Contact portalPatient = new Contact(
                      FirstName = 'Perseus', 
                      LastName = 'Jackson',
                      //GHI_Portal_Medical_Record_Number__c, 
                      Phone = '111-111-1111', 
                      Email = 'percyjackson@demigod.com', 
                      MailingCountry = 'United States', 
                      MailingStreet = 'Farm Road 3141', 
                      MailingCity = 'Long Island', 
                      MailingPostalCode = '11854',
                      MailingState = 'New York',
                      GHI_Portal_Patient_Identifier__c = 'Jackson', 
                      OSM_Gender__c = 'Male', 
                      GHI_Portal_Select_to_Enter__c = 'Patient Name', 
                      Birthdate = (Date.Today() - 1),
                      AccountId = portalOrder
                );
            return portalPatient;
    }
    
    public static Contact createContactHCP(Id acctId) {
        Id rId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
            
        Contact hcpContact = new Contact(
            RecordTypeId = rId,
            Salutation = 'Mr.',
            LastName = 'Sparrow',
            FirstName = 'Jack',
            Email = 'jsparrow@test.com', 
            AccountId = acctId, 
            OSM_Specialty__c = 'Urologist', 
            GHI_Portal_Contact_Source__c = 'Portal',
            OSM_Status__c = 'Draft'
        );
        return hcpContact; 
    }
    // public static Contact createContactHCO(Id acctId) {
    //     Id rId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
            
    //     Contact hcoContact = new Contact(
    //         RecordTypeId = rId,
    //         Salutation = 'Mr.',
    //         LastName = 'Thomas',
    //         FirstName = 'Jack',
    //         Email = 'jThomas@test.com', 
    //         AccountId = acctId, 
    //         OSM_Specialty__c = 'Urologist', 
    //         GHI_Portal_Contact_Source__c = 'Portal'
    //     );
    //     return hcoContact; 
    // }
    
    public static Contact createContactPatient(Id acctId) {
        Id rId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
            
        Contact patientContact = new Contact(
            RecordTypeId = rId,
            Salutation = 'Mr.',
            LastName = 'Sparrow',
            FirstName = 'Jack',
            Email = 'jsparrow@test.com', 
            AccountId = acctId, 
            OSM_Specialty__c = 'Urologist', 
            GHI_Portal_Contact_Source__c = 'Portal'
        );
        return patientContact;
    }
    
    public static OSM_Delegate__c createDelegateAsDelegate(Id acctId, Id contactId) {
        Id rId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Contact newContact = createContactHCP(acctId);
        newContact.RecordTypeId = rId;
        insert newContact; 
        System.Debug('!@#FromUtilNewContact ' + newContact.RecordTypeId + newContact.OSM_Status__c);
        OSM_Delegate__c delegate = new OSM_Delegate__c(
            Name = 'Test Delegate', 
            OSM_HCP__c = newContact.Id, 
            OSM_HCO__c = acctId, 
            OSM_Contact__c = contactId
        ); 
        return delegate; 
    }
    
    public static OSM_Delegate__c createDelegateAsPhysician(Id acctId, Id contactId) {
        Contact newContact = createContactHCP(acctId); 
        insert newContact; 
        
        OSM_Delegate__c delegate = new OSM_Delegate__c(
            Name = 'Test Delegate', 
            OSM_HCP__c = contactId, 
            OSM_HCO__c = acctId, 
            OSM_Contact__c = newContact.Id
        ); 
        return delegate; 
    }
    
    public static Customer_Affiliation__c createCNTtoACCTAffiliation(Id acctId, Id contactId, String role) {
        Id rId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Cnt-Acct Affiliation').getRecordTypeId();
             
        Customer_Affiliation__c cToA = new Customer_Affiliation__c(
            RecordTypeId = rId, 
            OSM_Contact_1__c = contactId, 
            OSM_Account_1__c = acctId,
            OSM_Role__c = role,
            GHI_Portal_Display_in_Portal__c = true
        ); 
        return cToA; 
    }
    
    public static Customer_Affiliation__c createCNTtoCNTAffiliation(Id contactId1, Id contactId2) {
        Id rId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Cnt-Cnt Affiliation').getRecordTypeId();
             
        Customer_Affiliation__c cToC = new Customer_Affiliation__c(
            RecordTypeId = rId, 
            OSM_Contact_1__c = contactId1, 
            OSM_Contact_2__c = contactId2,
            GHI_Portal_Display_in_Portal__c = true
        ); 
        return cToC; 
    }
    
    public static GHI_Portal_Contact_Details__c createContactDetails() {
        GHI_Portal_Contact_Details__c contactDetail = new GHI_Portal_Contact_Details__c(
             GHI_Portal_Assigned_Countries_CSV__c = 'United States',
             GHI_Portal_Contact_Information__c = 'test',
             GHI_Portal_Support_Location__c = 'test'
            );
        return contactDetail;    
    }
    
    public static OSM_Country_and_State__c createCountries() {
        OSM_Country_and_State__c cntry = new OSM_Country_and_State__c(
             OSM_Country_Name__c = 'test'
            );
        return cntry;
    }
    
    public static Case createCase() {
        Case currentCase = new Case(
            GHI_Portal_Preferred_Contact_Method__c = 'Phone',
            GHI_Portal_Contact_Phone__c = '',
            Description = 'OLI Cancellation Recommended Due to Billing Policy',
            GHI_Portal_In_Regards_To__c = 'Test Regards'
            );
        return currentCase;
    }
    public static Order createSavedOrder() {
        Id rtId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Portal Order').getRecordTypeId();
            
        Order savedOrder = new Order(
             RecordTypeId = rtId,
             OSM_Status__c = 'Draft'
            );
        return savedOrder;
    }
    
    public static GHI_Portal_Settings__c createPortalSettings() { 
        GHI_Portal_Settings__c setting = new GHI_Portal_Settings__c( 
            GHI_Portal_CNT_ACCT_Recordtype__c = 'Cnt-Acct Affiliation',
            GHI_Portal_CNT_CNT_RecordType__c = 'Cnt-Cnt Affiliation',
            GHI_Portal_ACCT_CNT_Recordtype__c = 'Acct-Cnt Affiliation', 
            GHI_Portal_Breast_Assay__c = 'Breast',
            GHI_Portal_Colon_Assay__c = 'Colon', 
            GHI_Portal_IsLocation__c = '00NK0000001bYwf',
            GHI_Portal_OncotypeDXBreast__c = 'IBC', 
            GHI_Portal_OncotypeDXColon__c = 'Colon',
            GHI_Portal_OncotypeDXDCIS__c = 'DCIS',
            GHI_Portal_OncotypeDXProstate__c = 'Prostate',
            GHI_Portal_Prostate_Assay__c = 'Prostate',
            GHI_Portal_Records_Per_Page__c = 10,
            GHI_Portal_StandardPriceBook__c = 'Standard Price Book',
            GHI_Portal_Community_User__c = '00NK0000001bYM3',
            GHI_Portal_Contact_Credential__c = '00NK0000001bYLj',
            GHI_Portal_Contact_MiddleName__c = '00NK0000001bYNu',
            GHI_Portal_Contact_NPI__c = '00NK0000001bYLo',
            GHI_Portal_Organization_Id__c = '00DK000000W4U6d',
            GHI_Portal_PatientContactRecType__c = 'Patient',
            GHI_Portal_Physician_Specialities__c = 'Medical Oncologist, Pathologist, Surgeon, Urologist, NP, PA, Other Physician, Prostate Urologist, Gynecologist',
            GHI_Portal_WebToLeadLink__c = '/servlet/servlet.WebToLead?encoding=UTF-8',
            GHI_Portal_WebToLeadRetURL__c = '/GHIPortal/GHI_Portal_AddressBook'
        ); 
        return setting;
    }
    
    public static GHI_Portal_Settings__c portalSettings(){
        GHI_Portal_Settings__c psettings = GHI_Portal_Settings__c.getOrgDefaults();
        return psettings;
    }
    
    public static GHI_Portal_Okta__c createPortalOktaSettings() { 
        GHI_Portal_Okta__c setting = new GHI_Portal_Okta__c( 
            GHI_Portal_Activate_User_URL__c = 'https://genomichealthonline.oktapreview.com/api/v1/users/{userId}/lifecycle/activate?sendEmail=true',
            GHI_Portal_Okta_API_GroupBox_ID__c = '00gmrwb4sFhYvRdDB0x6', 
            GHI_Portal_Okta_API_Create_URL__c = 'https://genomichealthonline.oktapreview.com/api/v1/users?activate=true', 
            GHI_Portal_Deactivate_User_URL__c = 'https://genomichealthonline.oktapreview.com/api/v1/users/{userId}/lifecycle/deactivate', 
            GHI_Portal_Okta_API_GroupRSPC_ID__c = '00g45daaz9BqdbKnC0h7', 
            GHI_Portal_Link_Box_Tab__c = 'https://genomichealthonline.okta.com/home/boxnet/0oaiw9gtl0n62VDKo0x6/72', 
            GHI_Portal_Link_Calculation_Tools_Tab__c = 'https://genomichealthonline.oktapreview.com/app/genomichealthexternal_rspc_1/exk44ng8bliDJsZw60h7/sso/saml', 
            GHI_Portal_Link_Speaker_Portal_Tab__c = 'https://genomichealthonline.oktapreview.com/app/genomichealthinc_speakerportal_1/exk44yqgwvWnK8grS0h7/sso/saml', 
            GHI_Portal_Okta_API_Active__c = true, 
            GHI_Portal_Okta_API_Token__c = '004Yjv25mYtuz8GMEPLkGnpeuQER9NVZQfiq5LtE0g', 
            GHI_Portal_Okta_API_GroupPortal_URL__c = 'https://genomichealthonline.oktapreview.com/api/v1/groups/{groupid}/users/',
            GHI_Portal_Okta_API_GroupPortal_ID__c = '00g44njk58vTGUsSC0h7', 
            GHI_Portal_Reset_MFA__c = 'https://genomichealthonline.oktapreview.com/api/v1/users/{userId}/lifecycle/reset_factors', 
            GHI_Portal_Reset_Password_URL__c = 'https://genomichealthonline.oktapreview.com/api/v1/users/{userId}/credentials/forgot_password?sendEmail=true', 
            GHI_Portal_API_GroupSpeakerPortal_ID__c = '00g45d9mcyNcGqkQP0h7', 
            GHI_Portal_Okta_Test_Prod_Box_Instance__c = true, 
            GHI_Portal_Unlock_User__c = 'https://genomichealthonline.oktapreview.com/api/v1/users/{userId}/lifecycle/unlock', 
            GHI_Portal_Okta_API_Update_URL__c = 'https://genomichealthonline.oktapreview.com/api/v1/users/'
        ); 
        return setting;
    }
    
    public static Record_Type__c createPatientRT() {
        Record_Type__c patientRT = new Record_Type__c(
            Contact_Patient_Default_Account__c='001K0000015K6vE',
            Contact_Patient_Record_Type__c='012K00000004vRW',
            OSM_Order_Account_Default__c='001K0000012w6n5');
            
        return patientRT;
    }
    
    public static List<OSM_Orderable_Creation__c> createOrderable() {
        List<OSM_Orderable_Creation__c> orderable = new List<OSM_Orderable_Creation__c>();

            orderable.add(            
                new OSM_Orderable_Creation__c (
                Name = 'Colon',
                GHI_Portal_DisplayLabel__c = '',
                Label__c = '',
                GHI_Portal_Name__c = 'Colon',
                Product_Name__c = 'Colon'
                ));
                
            orderable.add(            
                new OSM_Orderable_Creation__c (
                Name = 'DCIS',
                GHI_Portal_DisplayLabel__c = 'Breast DCIS',
                Label__c = '',
                GHI_Portal_Name__c = 'DCIS',
                Product_Name__c = 'DCIS'
                ));
                
            orderable.add(            
                new OSM_Orderable_Creation__c (
                Name = 'IBC',
                GHI_Portal_DisplayLabel__c = 'Patient with Invasive Breast',
                Label__c = '',
                GHI_Portal_Name__c = 'IBC',
                Product_Name__c = 'IBC'
                ));
                
            orderable.add(            
                new OSM_Orderable_Creation__c (
                Name = 'MMR',
                GHI_Portal_DisplayLabel__c = 'MMR for Recurrence Risk Assessment',
                Label__c = '',
                GHI_Portal_Name__c = 'MMR',
                Product_Name__c = 'MMR'
                ));
                
            orderable.add(            
                new OSM_Orderable_Creation__c (
                Name = 'MMR and Colon',
                GHI_Portal_DisplayLabel__c = '& MMR Assays',
                Label__c = '',
                GHI_Portal_Name__c = 'MMR and Colon',
                Product_Name__c = 'MMR;Colon'
                ));
                
            orderable.add(            
                new OSM_Orderable_Creation__c (
                Name = 'MMR Reflex to Colon',
                GHI_Portal_DisplayLabel__c = 'Sequential Assays',
                Label__c = '',
                GHI_Portal_Name__c = 'MMR Reflex to Colon',
                Product_Name__c = 'MMR;Colon'
                ));
                
            orderable.add(            
                new OSM_Orderable_Creation__c (
                Name = 'Prostate',
                GHI_Portal_DisplayLabel__c = '',
                Label__c = '',
                GHI_Portal_Name__c = 'Prostate',
                Product_Name__c = 'Prostate'
                ));
                
            orderable.add(            
                new OSM_Orderable_Creation__c (
                Name = 'Unknown',
                GHI_Portal_DisplayLabel__c = '',
                Label__c = '',
                GHI_Portal_Name__c = '',
                Product_Name__c ='Unknown'
                ));
                
        return orderable;

    }
    
    public static GHI_Internal__c createInternal() {
        GHI_Internal__c internal = new GHI_Internal__c(
            Name = 'GHI Internal',
            GHI_Internal_Account__c='001K000001BJ7kU',
            GHI_Internal_Account_Name__c='GHI Internal'
            );
            
        return internal;
    }
    
}