/*------------------------------------------------------------------------
    Author:        Sairah Hadjinoor
    Company:       Cloud Sherpas
    Description:   Trigger Handler for User object
                  
    Test Class:
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    10.FEB.2015     Sairah Hadjinoor        Created
--------------------------------------------------------------------------*/

public with sharing class GHI_Portal_UserTriggerHandler {
    
    public static void onBeforeInsert(List<User> newUserList){
        
    }
    
    public static void onBeforeUpdate(List<User> newUserList, Map<Id, User> oldUserMap){
        
    }
    
    public static void onAfterInsert(List<User> newUserList, Map<ID, User> newUserMap){
    	
    	//Create list of portal user object
    	List<User> portalUsers = new List<User>();
    	
        Map<Id, String> conIdOrderMap = new Map<Id, String>();
        
        for(User user: newUserList){
            System.debug('#ThisNewUserList' + newUserList);
            //change operator to "OR" - Gypt Minierva 7/8/2015
            if(user.GHI_Portal_Order_Workflow__c != null || user.GHI_Portal_Order_Workflow__c != ''){
                conIdOrderMap.put(user.ContactId, user.GHI_Portal_Order_Workflow__c);
                System.debug('#putContactMap' + conIdOrderMap);
            }
            
            //Populate collection of Portal users
            if(user.IsPortalEnabled){
            	portalUsers.add(user);
            }
        }
        if(!conIdOrderMap.isEmpty()){
            populateOrderWorkflow(conIdOrderMap);
            System.debug('*ContactIDMap' + conIdOrderMap);
        }
        
        //Add Okta API Call
        if(!portalUsers.isEmpty()){
        	//Call Okta API user
        	//Modify Okta API to accept list of users --> create a batch API call 
        	GHI_Portal_OktaAPI.createUsers(portalUsers);
        }
        //Look at newUserList
    }
    
    public static void onAfterUpdate(List<User> updatedUserList, Map<ID, User> updatedUserMap, Map<ID, User> oldUserMap){
        
        //Create list of portal user object
    	List<User> portalUsersUpdate = new List<User>();
    	List<User> portalUsersCreate = new List<User>();
    	List<User> portalUsersDeactivate = new List<User>();
    	List<User> portalUsersActivate = new List<User>();
    	
        Map<Id, String> conIdOrderMap = new Map<Id, String>();
        
        
        
        for(User user: updatedUserList){
            if(oldUserMap.get(user.Id).GHI_Portal_Order_Workflow__c != user.GHI_Portal_Order_Workflow__c){
                conIdOrderMap.put(user.ContactId, user.GHI_Portal_Order_Workflow__c);
                System.debug('#ThisContactID' + conIdOrderMap);
            }
            
            System.debug('\n>>>User:' + user + '\n');
            
            //Populate collection of Portal users
            if(user.IsPortalEnabled){
            	//if no Okta ID - create user in Okta
            	//check if user has Okta ID - check for changes to: email, permissions (RSPC,Speaker Portal,Box?, View Reports), deactivation?
            	if(user.GHI_Portal_Okta_User_ID__c == null || user.GHI_Portal_Okta_User_ID__c == ''){
            		portalUsersCreate.add(user);
            		System.debug('#ThisUser:' + user + '\n');
            	}
            	else if (oldUserMap.get(user.Id).email != user.email 
            		|| oldUserMap.get(user.Id).FirstName != user.FirstName 
            		|| oldUserMap.get(user.Id).LastName != user.LastName
            		|| oldUserMap.get(user.Id).GHI_Portal_Tools_Access__c != user.GHI_Portal_Tools_Access__c 
            		|| oldUserMap.get(user.Id).GHI_Portal_Speaker_Portal_Access__c != user.GHI_Portal_Speaker_Portal_Access__c
            		|| oldUserMap.get(user.Id).GHI_Portal_View_Reports__c != user.GHI_Portal_View_Reports__c
            		|| oldUserMap.get(user.Id).GHI_Portal_Box_Access__c != user.GHI_Portal_Box_Access__c
            		|| oldUserMap.get(user.Id).GHI_Portal_Tools_Tab_Default__c != user.GHI_Portal_Tools_Tab_Default__c){
            			
            		portalUsersUpdate.add(user);
            	}
            	
            }else if(oldUserMap.get(user.Id).IsPortalEnabled != user.IsPortalEnabled){
            	
            	if(user.IsPortalEnabled && user.GHI_Portal_Okta_User_ID__c.trim().length() > 0){
            		portalUsersActivate.add(user);
        		}
        		else{
        			if(user.GHI_Portal_Okta_User_ID__c.trim().length() > 0){
        				portalUsersDeactivate.add(user);	
        			}
        		}
            }
        }
        if(!conIdOrderMap.isEmpty()){
            System.debug('#ValueConID ' + conIdOrderMap);
            populateOrderWorkflow(conIdOrderMap);
        }
        
        //Create new Portal Users in Okta
        if(!portalUsersCreate.isEmpty()){
            System.debug('#ValuePortalUser ' + portalUsersCreate);
        	GHI_Portal_OktaAPI.createUsers(portalUsersCreate);
        }
        //Update existing users in Okta
        if(!portalUsersUpdate.isEmpty()){
            System.debug('#ValuePortalUserUpdate ' + portalUsersUpdate);
        	GHI_Portal_OktaAPI.updateUsers(portalUsersUpdate);
        }
        
        //Deactivate users in Okta
        if(!portalUsersDeactivate.isEmpty()){
            System.debug('#ValuePortalDeac ' + portalUsersDeactivate);
        	GHI_Portal_OktaAPI.deactivateUsers(portalUsersDeactivate);
        }
        
        //Activate users in Okta
        if(!portalUsersActivate.isEmpty()){
            System.debug('#portalUserAct ' + portalUsersActivate);
        	GHI_Portal_OktaAPI.activateUsers(portalUsersActivate);
        }
    }
    //serialized list and moved @future - Paul Wittmeyer 7/29/2015
    //@future
    public static void populateOrderWorkflow(Map<Id, String> userOrderMap){
        System.debug('*ThisUserOrderMap ' + userOrderMap);
        List<Contact> updateContactList = new List<Contact>();
        System.debug('*ThisUpdateContact ' + updateContactList);
        //get all users with corresponding contact from the Map
         //added functionality to reduce query on what is now line 136 - Paul Wittmeyer 7/2/2015
		List<Contact> mapContactList = new List<Contact>();
        for(id cId : userOrderMap.keySet()){
        	contact c = new Contact(id=cID);
        	System.debug('*ThisContact#C ' + c);
        	mapContactList.add(c);
        	System.debug('*ThisCotnactList ' + mapContactList);
        }
        //for(Contact conRec:[Select Id from Contact where Id IN: userOrderMap.keySet()]){
        for(Contact conRec: mapContactList){
            System.debug('*ThisContactRecord ' + conRec);
            //set value of portal order workflow
            conRec.GHI_Portal_Order_Workflow__c = userOrderMap.get(conRec.Id);
            updateContactList.add(conRec);
        }
        //update user
        if(!updateContactList.isEmpty()){
        	//serialize for @future  - pw
        	String updateContactListString = JSON.serialize(updateContactList);
            //update updateContactList;
            System.debug('*ThisUpdateList ' + updateContactList);
        }
    } 
	//added method for @future - pw
    @future
	public static void updateContactList(string updateContactListString){
		List<Contact> ListParsed = (List<Contact>)JSON.deserialize(updateContactListString,  List<Contact>.class);
		//added logging code - Paul Wittmeyer 8/7/2015
		try{
			update ListParsed;
		}catch(exception e){
			Logger.debugExceptionForAsync(e);
		}
	}
}