/******************************************************************************
    Author         : 
    Description    : Unit tests for GHI_Portal_UserTriggerHandler
    History
        <date>                <author>                <description>
        31-JUL-2015           Mark Cagurong             Updated Test class to @isTest(seeAllData = false) and set @isTest(seeAllData = true) to original test methods
********************************************************************************/
@isTest
private class GHI_Portal_UserTriggerHandler_Test {

    /*
    @isTest(seeAllData = true)
    private static void Valid() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
    
        GHI_Portal_UserTriggerHandler_Test.updateTriggerSwitch();

        System.runAs(thisUser) {
            Account acc = new Account(Name='Test Account Name', Phone='09262741575', OSM_Specialty__c='Urology', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
            insert acc;

            Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
            Contact cnt = OSM_DataFactory.createContact('Udi', acc.Id, HCPRecType);
            cnt.OSM_Specialty__c = 'Urology';

            insert cnt;

            User u = OSM_DataFactory.createGHIPortalUser('Todas');
            u.GHI_Portal_Order_Workflow__c = 'Domestic';
            u.ContactId = cnt.Id;
            u.CommunityNickname = 'testUser123';
            u.IsActive = true;
            //u.UserRoleId = role[0].Id;

            insert u;

            u.lastName = 'Sayson'; 
            u.GHI_Portal_Order_Workflow__c = 'Intl-Partner';
            u.GHI_Portal_Okta_User_ID__c=null;

            update u;
        }
    }

    @future
    public static void updateTriggerSwitch()
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.GHI_Portal_UserTrigger__c = true;
        GHI_Portal_UserTriggerHandler controller = new GHI_Portal_UserTriggerHandler();
        update triggerSwitch; 
    }
    */
    
    private static final String TEST_OKTA_ID = '00u45d9n0eBRBr32d0h7';
    private static final String ADMIN_USERNAME = 'Admin123';

     /******************************************************************************
        Author         : Mark Cagurong
        Description    : Helper method to get the admin test running user 
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    private static User getAdminUser(){
        User u = [SELECT Id, Name FROM User WHERE Alias = :ADMIN_USERNAME];
        return u;
    }
    
     /******************************************************************************
        Author         : Mark Cagurong
        Description    : Helper method to get the test portal user
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    static User getPortalUser(){

        User portalUser = [SELECT Id, Name, FirstName, LastName, Email, GHI_Portal_Okta_User_ID__c, GHI_Portal_Order_Workflow__c, GHI_Portal_Tools_Access__c, GHI_Portal_Speaker_Portal_Access__c, GHI_Portal_View_Reports__c, GHI_Portal_Box_Access__c, GHI_Portal_Tools_Tab_Default__c FROM User WHERE Username = 'jsparrow@Test.com' LIMIT 1]; 
        return portalUser;
    }

     /******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test initialization
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    @testsetup 
    static void setup() {

        TriggerSwitch__c tSwitch = new TriggerSwitch__c();
        tSwitch.GHI_Portal_UserTrigger__c = true;
        tSwitch.Contact_Trigger__c= false;

        insert tSwitch;

        User adminUser = OSM_DataFactory.createUser(ADMIN_USERNAME);
        insert adminUser;

    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test for insert trigger covering the OKTA user activation
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    @isTest
    static void updateUsers_TriggerInvocation(){

        Test.startTest();

        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        portalUser.GHI_Portal_Okta_User_ID__c =  TEST_OKTA_ID;
        insert portalUser;

        Test.stopTest();

        System.assertEquals(TEST_OKTA_ID, portalUser.GHI_Portal_Okta_User_ID__c);
        //system.debug('portalUser.v2:' + JSON.serialize(portalUser));
        
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test for insert trigger covering the OKTA user deactivation
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    @isTest
    static void updateUsers_TriggerInvocationUpdate_portalUsersCreate(){


        User portalUser = GHI_Portal_TestUtilities.createPortalUser();         
        //GHI_Portal_Order_Workflow__c, 
        portalUser.GHI_Portal_Okta_User_ID__c =  TEST_OKTA_ID;
        portalUser.GHI_Portal_Tools_Access__c = false;
        portalUser.GHI_Portal_Speaker_Portal_Access__c = false;
        portalUser.GHI_Portal_View_Reports__c = false;
        portalUser.GHI_Portal_Box_Access__c = false;
        portalUser.GHI_Portal_Tools_Tab_Default__c = 'Intro';

        insert portalUser;       

        Test.startTest();

        portalUser = getPortalUser();
        portalUser.GHI_Portal_Okta_User_ID__c =  null;

        update portalUser;
       
        Test.stopTest();

        //List<User> users = [SELECT Id, Name, FirstName, LastName, Email, IsPortalEnabled, Profile.Id, Profile.Name FROM User ];
        system.debug('updateUsers_TriggerInvocationUpdate:' + JSON.serialize(portalUser));        
    }


    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test for insert trigger covering the OKTA user deactivation
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    @isTest
    static void updateUsers_TriggerInvocationUpdate(){


        User portalUser = GHI_Portal_TestUtilities.createPortalUser();         
        portalUser.GHI_Portal_Okta_User_ID__c =  TEST_OKTA_ID;
        portalUser.GHI_Portal_Tools_Access__c = false;
        portalUser.GHI_Portal_Speaker_Portal_Access__c = false;
        portalUser.GHI_Portal_View_Reports__c = false;
        portalUser.GHI_Portal_Box_Access__c = false;
        portalUser.GHI_Portal_Tools_Tab_Default__c = 'Intro';        

        insert portalUser;       

        Test.startTest();

        portalUser = getPortalUser();        
        portalUser.LastName =  portalUser.LastName + 'x';
        portalUser.GHI_Portal_Tools_Access__c = !portalUser.GHI_Portal_Tools_Access__c;
        portalUser.GHI_Portal_Speaker_Portal_Access__c = !portalUser.GHI_Portal_Speaker_Portal_Access__c;
        portalUser.GHI_Portal_View_Reports__c = !portalUser.GHI_Portal_View_Reports__c;
        portalUser.GHI_Portal_Box_Access__c = !portalUser.GHI_Portal_Box_Access__c;
        //portalUser.GHI_Portal_Tools_Tab_Default__c = !portalUser.GHI_Portal_Tools_Tab_Default__c;

        update portalUser;
       
        Test.stopTest();

        //List<User> users = [SELECT Id, Name, FirstName, LastName, Email, IsPortalEnabled, Profile.Id, Profile.Name FROM User ];
        system.debug('updateUsers_TriggerInvocationUpdate:' + JSON.serialize(portalUser));        
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test for insert trigger covering the OKTA user workflow
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    @isTest
    static void updateUsers_TriggerInvocationUpdateWorkflow(){
  

        User portalUser = GHI_Portal_TestUtilities.createPortalUser();         
        portalUser.GHI_Portal_Okta_User_ID__c =  TEST_OKTA_ID;
        portalUser.GHI_Portal_Tools_Access__c = false;
        portalUser.GHI_Portal_Speaker_Portal_Access__c = false;
        portalUser.GHI_Portal_View_Reports__c = false;
        portalUser.GHI_Portal_Box_Access__c = false;
        portalUser.GHI_Portal_Tools_Tab_Default__c = 'Intro';        
        portalUser.GHI_Portal_Order_Workflow__c = 'Domestic';

        insert portalUser;       

        Test.startTest();

        portalUser = getPortalUser();        
        portalUser.GHI_Portal_Order_Workflow__c = 'Intl-Partner';

        update portalUser;
       
        Test.stopTest();

        //List<User> users = [SELECT Id, Name, FirstName, LastName, Email, IsPortalEnabled, Profile.Id, Profile.Name FROM User ];
        system.debug('updateUsers_TriggerInvocationUpdate:' + JSON.serialize(portalUser));        
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test for after insert trigger covering the OKTA user deactivation
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    @isTest
    static void updateUsers_TriggerInvocation_DeactivateOKTA(){

        User adminUser = getAdminUser();
        
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 

        //portalUser.IsPortalEnabled = true;
        portalUser.GHI_Portal_Okta_User_ID__c =  TEST_OKTA_ID;                

        insert portalUser;
        
        System.runAs(adminUser){
            Test.startTest();

            portalUser = getPortalUser();
            portalUser.IsPortalEnabled = false;
            
            update portalUser;
            
            Test.stopTest();
            

            //List<User> users = [SELECT Id, Name, FirstName, LastName, Email, IsPortalEnabled, Profile.Id, Profile.Name FROM User ];
            system.debug('updateUsers_TriggerInvocationUpdate:' + JSON.serialize(portalUser));        
        }
    }
    
    /*
        @author             Raus Kenneth Ablaza
        @description        Test method to cover updateContactList method of GHI_Portal_UserTriggerHandler class.
        @date               18 September 2015
    */
    @isTest
    static void testUpdateContactList(){
        /* Inititialization of variables */
        List<Contact> testContactList = new List<Contact>();
        Contact testCon;
        Account testAcc;
        
        /* Account */
        testAcc = GHI_Portal_TestUtilities.createDefaultAcc();
        insert testAcc;
        
        /* Contacts */
        testCon = GHI_Portal_TestUtilities.createContactHCP(testAcc.Id);
        testCon.OSM_Specialty__c = 'Urologist';
        testContactList.add(testCon);
        insert testContactList;
        
        /* Test */
        Test.startTest();
        
        testContactList[0].OSM_Specialty__c = 'Oncologist';
        GHI_Portal_UserTriggerHandler.updateContactList(JSON.serialize(testContactList));
        
        Test.stopTest();
        
        System.assertEquals('Oncologist', testContactList[0].OSM_Specialty__c);
    }
    
     /******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test for after insert trigger covering the OKTA user activation
        History
            <date>                <author>                <description>
            03-AUG-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    /*
    @isTest
    static void updateUsers_TriggerInvocation_ActivateOKTA(){
        
        GHI_Portal_Okta__c settings = new GHI_Portal_Okta__c();
		settings.GHI_Portal_Okta_API_Active__c = false;
		insert settings;

        User adminUser = getAdminUser();
        
        User portalUser = GHI_Portal_TestUtilities.createPortalUser(); 
        portalUser.GHI_Portal_Okta_User_ID__c =  TEST_OKTA_ID;
        insert portalUser;
        
        System.runAs(adminUser){
            portalUser = getPortalUser();
            portalUser.IsPortalEnabled = false;
            
            update portalUser;
            
            system.debug('updateUsers_TriggerInvocation_ActivateOKTA.v0:' + JSON.serialize(portalUser));        
        }
        
        
        System.runAs(adminUser){
            Test.startTest();
            
            system.debug('updateUsers_TriggerInvocation_ActivateOKTA.v1:' + JSON.serialize(portalUser));        
            
            portalUser = getPortalUser();
            //portalUser.GHI_Portal_Okta_User_ID__c =  TEST_OKTA_ID;
            portalUser.IsPortalEnabled = true;
            
            system.debug('updateUsers_TriggerInvocation_ActivateOKTA.v2:' + JSON.serialize(portalUser));        
            
            update portalUser;
            
            Test.stopTest();
        }
        
    }
    */


}