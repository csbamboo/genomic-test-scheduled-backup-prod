public with sharing class Guid {

public static string NewGuid()
{
  Blob b;  
  String h; 
  String guid;
    
  try
  {
    b = Crypto.GenerateAESKey(128); 
    h = EncodingUtil.ConvertTohex(b); 
    guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
    
    return guid;
  }
  catch(Exception ex) 
  {
    //Log the exception in a later version
    return '';
  }
  finally
  {
    b = null;  
    h = null; 
  }
}

}