public with sharing class ImportFullySignedDocumentController {

  private Apttus__APTS_Agreement__c agreement;

  public ImportFullySignedDocumentController(ApexPages.StandardController stdController) { 
    
    this.agreement = (Apttus__APTS_Agreement__c)stdController.getRecord();
    
  }

  public PageReference finalize(){
    
    agreement.Apttus__Status_Category__c = 'In Signatures';
    agreement.Apttus__Status__c = 'Fully Signed';
    
    update agreement;
    
    PageReference result = new ApexPages.StandardController(agreement).view();
    
    return result;
    
  }


}