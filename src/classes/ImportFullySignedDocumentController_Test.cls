/*
  @author: Jerome Liwanag
  @date: 05 August 2015
  @description: Test Class for ImportFullySignedDocumentController apex class
  @history: 05 August 2015 - Created (Jerome Liwanag)

*/
@isTest
private class ImportFullySignedDocumentController_Test {

	private static testMethod void testMethod1() {
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        insert agreement;
        
        PageReference pageRef = Page.ImportFullySignedDocument;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ordItem);
        ApexPages.currentPage().getParameters().put('Id',agreement.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(agreement);
        ImportFullySignedDocumentController cont = new ImportFullySignedDocumentController(sc);
        
        Test.startTest();
        cont.finalize();
        
        Test.stopTest();
	}

}