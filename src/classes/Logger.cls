/*------------------------------------------------------------------------
    Author:         Paul Wittmeyer
    Company:        Cloud Sherpas
    Description:    Logger for all exceptions
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    05/13/2015      Paul Wittmeyer          Created this Class
    07/22/2015      Paul Wittmeyer          Added *Async methods
    8/6/2015        Paul Wittmeyer          Added functinality to throw exception to debug logs
---------------------------------------------------------------------------*/

Public Class Logger{
    
    public static Integer loggingLevelInt {
        //get logging level enumeration and return as integer
        get{ 
            String logLevel = LoggerSettings__c.getOrgDefaults().Debug_Log_Level__c;
            // loop through loggingLevel enumeration
            for(Integer i = 0; i < system.logginglevel.values().size(); i++) {
                if (system.logginglevel.values()[i].name() == logLevel) {
                    loggingLevelInt = i;
                }
            }
            return loggingLevelInt; 
        }
        set;
    }

    static public void logActivity(string logActivityString){
        //Method called within Apex code to capture an activity message and create Log record
        List<Log__c> activityList = new List<Log__c>();
        Log__c log = new Log__c();
        log.Log_Type__c = 'Activity';
        log.Logging_Level__c = system.logginglevel.INFO.name();
        log.Message__c = logActivityString;
        if(LoggerSettings__c.getOrgDefaults().Is_Active__c && LoggerSettings__c.getOrgDefaults().Log_Activities__c){
            activityList.add(log);
            //serialize list to pass primitive to @future 
            String logListString = JSON.serialize(activityList);
            writeLogsToDatabase(logListString);
        }
    }
    
    //LogActivity for @future methods
    static public void logActivityForAsync(string logActivityString){
        //Method called within Apex code to capture an activity message and create Log record
        List<Log__c> activityList = new List<Log__c>();
        Log__c log = new Log__c();
        log.Log_Type__c = 'Activity';
        log.Logging_Level__c = system.logginglevel.INFO.name();
        log.Message__c = logActivityString;
        if(LoggerSettings__c.getOrgDefaults().Is_Active__c && LoggerSettings__c.getOrgDefaults().Log_Activities__c){
            activityList.add(log);
        }
        if(activityList.size() > 0 ){
            insert activityList;
        }
    }
    
    static public void debug(LoggingLevel logLevel, object msg){
        //Method called within Apex code to capture a debug statement and create Log record
        List<Log__c> debugList = new List<Log__c>();
        Log__c log = new Log__c();
        log.Message__c = string.valueof(msg);
        log.Log_Type__c = 'Debug';
        log.Logging_Level__c = logLevel.name();
        if(LoggerSettings__c.getOrgDefaults().Is_Active__c){
            if(loggingLevelInt >= loglevel.ordinal()){
                debugList.add(log);
                //serialize list to pass primitive to @future
                String logListString = JSON.serialize(debugList);
                writeLogsToDatabase(logListString);
            }
        }
    }
    
    //debug for @future methods
    static public void debugForAsync(LoggingLevel logLevel, object msg){
        //Method called within Apex code to capture a debug statement and create Log record
        List<Log__c> debugList = new List<Log__c>();
        Log__c log = new Log__c();
        log.Message__c = string.valueof(msg);
        log.Log_Type__c = 'Debug';
        log.Logging_Level__c = logLevel.name();
        if(LoggerSettings__c.getOrgDefaults().Is_Active__c){
            if(loggingLevelInt >= loglevel.ordinal()){
                debugList.add(log);
            }
        }
        if(debugList.size() > 0 ){
            insert debugList;
        }
    }
   
    static public void debugException(Exception ex){
        //Method called within Apex code to capture an exception and create Log record
        List<Log__c> exceptionList = new List<Log__c>();
        string exceptionInfo = ex.getMessage() + ' stack: ' + ex.getStackTraceString();
        system.debug(exceptionInfo);
        Log__c log = new Log__c();
        log.Message__c = exceptionInfo;
        log.Exception_Line__c = ex.getLineNumber();
        log.Exception_Type__c = ex.getTypeName();
        log.Log_Type__c = 'Exception';
        log.Logging_Level__c = system.logginglevel.ERROR.name();
        if(LoggerSettings__c.getOrgDefaults().Is_Active__c){
            exceptionList.add(log);
            //serialize list to pass primitive to @future
            String logListString = JSON.serialize(exceptionList);
            writeLogsToDatabase(logListString);
        }
        if (!Test.isRunningTest()){
        exceptionthrow(exceptionInfo);
        } 
    }
    
    //debugException for @future methods
    static public void debugExceptionForAsync(Exception ex){
        //Method called within Apex code to capture an exception and create Log record
        List<Log__c> exceptionList = new List<Log__c>();
        string exceptionInfo = ex.getMessage() + ' stack: ' + ex.getStackTraceString();
        system.debug(exceptionInfo);
        Log__c log = new Log__c();
        log.Message__c = exceptionInfo;
        log.Exception_Line__c = ex.getLineNumber();
        log.Exception_Type__c = ex.getTypeName();
        log.Log_Type__c = 'Exception';
        log.Logging_Level__c = system.logginglevel.ERROR.name();
        if(LoggerSettings__c.getOrgDefaults().Is_Active__c){
            exceptionList.add(log);   
        } 
        if(exceptionList.size() > 0){
            insert exceptionList;
        }
        if (!Test.isRunningTest()){
        Throw new ThrowException(exceptionInfo);
        }
    }
    
    @future
    static private void writeLogsToDatabase(string logListString){
        //@future to insert log record in new context
        //deserialize primitive to log__c object and insert record
        List<Log__c> ListParsed = (List<Log__c>)JSON.deserialize(logListString,  List<Log__c>.class);
        insert ListParsed;
    }
    @future
    static public void exceptionthrow(string exceptionInfo){
        system.debug(exceptionInfo);
        Throw new ThrowException(exceptionInfo);
    }
}