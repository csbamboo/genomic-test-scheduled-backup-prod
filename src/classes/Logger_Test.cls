@isTest
private class Logger_Test {
    public class TestException extends Exception {}

    private static testMethod void test() {
        LoggerSettings__c loggerSettings = LoggerSettings__c.getOrgDefaults();
        loggerSettings.Is_Active__c = true;
        loggerSettings.Log_Activities__c = true;
        insert loggerSettings;
        
        String testString = 'test';
        Account acc1 = OSM_DataFactory.createAccount('Test');
        //insert acc1;
        
        Exception a = new TestException('error');

        
        Logger.logActivity(testString);
        Logger.logActivityForAsync(testString);
        Logger.debug(Logginglevel.FINEST, acc1 );        
        Logger.debugForAsync(Logginglevel.FINEST, acc1 );
        Logger.debugException(a);
        Logger.debugExceptionForAsync(a);
    }

}