/*
      @author: David Catindoy
      @date: 28 JAN 2015 - Created
      @description: Account Territory Assignment Trigger Handler Test Class
      @history: 28 JAN 2015 - Updated (PL)

*/
@isTest
private class OSM_AccountTATriggerHandler_Test {
    
    static testmethod void TerritoryTestMethod(){

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Account_Territory_Assignment_Trigger__c = true;
        insert triggerSwitch;

        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id PartnerRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();

        Account account = OSM_DataFactory.createAccount('New Account 1');
        account.RecordTypeId = HCORecType;
        insert account;

        //Account account1 = OSM_DataFactory.createAccount('New Account 2');
  //      account1.RecordTypeId = PartnerRecType;
        //insert account1;

        User testUser = OSM_DataFactory.createUser('Catindoy');

        OSM_Territory__c parentTerritory = OSM_DataFactory.createTerritory('Parent Territory');
        insert parentTerritory;

        OSM_Territory__c territory = OSM_DataFactory.createTerritory('New Territory');
        territory.OSM_Parent_Territory__c = parentTerritory.Id;
        insert territory;

        List<OSM_Territory_Sales_Rep_Assignment__c> territorySRA = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer i=0; i<4; i++){
            territorySRA.add(new OSM_Territory_Sales_Rep_Assignment__c(OSM_Territory_ID__c = territory.Id, OSM_Sales_Rep__c = testUser.Id));
            if(i<2){
                territorySRA[i].OSM_Account_Access_Level__c = 'Read';
            }
            else {
                territorySRA[i].OSM_Account_Access_Level__c = 'Edit';
            }
        }
        insert territorySRA;
        
        List<OSM_Account_Territory_Assignment__c> accountTAList = new List<OSM_Account_Territory_Assignment__c>();
        for(Integer i = 0; i<2; i++){
            accountTAList.add(OSM_DataFactory.createATA(account.Id, territory.Id));
            if(i<1){
                accountTAList[i].OSM_Override__c = false;
                accountTAList[i].OSM_Manual_Assignment__c = false;
            } else {
                accountTAList[i].OSM_Override__c = true;
                accountTAList[i].OSM_Manual_Assignment__c = true;
            }
        }

        Test.startTest();
        insert accountTAList;

        for(Integer i = 0; i<2; i++){
            if(i<1){
                accountTAList[i].OSM_Override__c = true;
                accountTAList[i].OSM_Manual_Assignment__c = true;
            } else {
                accountTAList[i].OSM_Override__c = false;
                accountTAList[i].OSM_Manual_Assignment__c = false;
            }
        }
        update accountTAList;

        delete accountTAList;
        Test.stopTest();
    }
    
}