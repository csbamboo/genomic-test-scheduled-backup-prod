/*
    @author: patrick lorilla
    @date: 2 October 2014
    @description: Account Territory Assignment Trigger Handler

*/
public class OSM_AccountTerritoryAssignTriggerHandler{
    
    public static boolean updateNumberofTerritoriesRunOnce = false;
    public static boolean createAccountShareRunOnce = false;
    public static boolean deleteAccountShareRunOnce = false;
    public static boolean createTerritoryHistoryRunOnce = false;
    
     /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newAAT- new Acct Assign List, newHTAMap - new Acct Assign Map oldHTAMAP- old Acct Assign Map
    @description: Before Update Method
    */
    public static void OnBeforeUpdate(List<OSM_Account_Territory_Assignment__c> newAAT, Map<ID, OSM_Account_Territory_Assignment__c> newAATMap, Map<ID, OSM_Account_Territory_Assignment__c> oldAATMap){   
         //Set override user and override date
         for(OSM_Account_Territory_Assignment__c aat :newAAT){
             if(aat.OSM_Override__c && !oldAATMap.get(aat.ID).OSM_Override__c){
                 aat.OSM_Overrided_By__c = userinfo.getUserId();
                 Datetime today = Datetime.now();
                 aat.OSM_Date_Override__c= Date.parse(today.format('MM/dd/yyyy'));
             }    
         }
    }
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newATT - new AAT List, newAATMap - new AAT Map
    @description: After Insert Method
    */
    public static void OnAfterInsert(List<OSM_Account_Territory_Assignment__c> newAAT, Map<ID, OSM_Account_Territory_Assignment__c> newAATMap){
        Set<ID> territoryIDS = new Set<ID>();
        for(OSM_Account_Territory_Assignment__c aat :newAAT){
            territoryIDS.add(aat.OSM_Territory_Name__c);         
        }      
        if(!newAAT.isEmpty() && !territoryIDS.isEmpty() && !createAccountShareRunOnce){
            createAccountShareRunOnce = true;
            createAccountShare(newAAT,territoryIDS);
        }
        if(!updateNumberofTerritoriesRunOnce){
            updateNumberofTerritoriesRunOnce = true;
            updateNumberofTerritories(newAAT);
        }
    }
    
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newATT - new AAT List, newAATMap - new AAT Map, oldAATMap - old AAT Map
    @description: After Update Method
    */
    public static void OnAfterUpdate(List<OSM_Account_Territory_Assignment__c> newAAT, Map<ID, OSM_Account_Territory_Assignment__c> newAATMap, Map<ID, OSM_Account_Territory_Assignment__c> oldAATMap){
        
        Set<ID> territoryIDS = new Set<ID>();
        Set<ID> territoryIDSDelete = new Set<ID>();
        List<OSM_Account_Territory_Assignment__c> overAAT = new List<OSM_Account_Territory_Assignment__c>();
        List<OSM_Account_Territory_Assignment__c> delAAT = new List<OSM_Account_Territory_Assignment__c>();
        for(OSM_Account_Territory_Assignment__c aat :newAAT){
            if(!aat.OSM_Override__c && oldAATMap.get(aat.ID).OSM_Override__c ){
                overAAT.add(aat);
                territoryIDS.add(aat.OSM_Territory_Name__c);         
            }
            else if(aat.OSM_Override__c && !oldAATMap.get(aat.ID).OSM_Override__c){
                delAAT.add(aat);
                territoryIDSDelete.add(aat.OSM_Territory_Name__c);     
            }
            
            if(aat.OSM_Manual_Assignment__c && !oldAATMap.get(aat.ID).OSM_Manual_Assignment__c ){
                overAAT.add(aat);
                territoryIDS.add(aat.OSM_Territory_Name__c);         
            }
        }    
        if(!newAAT.isEmpty() && !territoryIDS.isEmpty()&& !createAccountShareRunOnce){
            createAccountShare(overAAT,territoryIDS);
        }
        if(!delAAT.isEmpty() && !territoryIDSDelete .isEmpty()){
            deleteAccountShare(delAAT,territoryIDSDelete);
        }
        if(!updateNumberofTerritoriesRunOnce){
            updateNumberofTerritoriesRunOnce = true;
            updateNumberofTerritories(newAAT);
        }
    }
    
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: delAAT- AAT List, delAATMap- AAT Map
    @description: Before Delete Method
    */
    
    public static void OnBeforeDelete(List<OSM_Account_Territory_Assignment__c> delAAT, Map<ID, OSM_Account_Territory_Assignment__c> delAATMap){
        Set<ID> territoryIDS = new Set<ID>();
        for(OSM_Account_Territory_Assignment__c aat :delAAT){
            territoryIDS.add(aat.OSM_Territory_Name__c);         
        }    
        if(!delAAT.isEmpty() && !territoryIDS.isEmpty() && !createTerritoryHistoryRunOnce && !deleteAccountShareRunOnce){
            createTerritoryHistoryRunOnce = true;
            deleteAccountShareRunOnce = true;
            deleteAccountShare(delAAT,territoryIDS);
            createTerritoryHistory(delAAT, territoryIDS);
        }
    }
    
     /*
    @author: patrick lorilla
    @date: 16 June 2015
    @param: delAAT- AAT List, delAATMap- AAT Map
    @description: After Delete Method
    */
    
    public static void OnAfterDelete(List<OSM_Account_Territory_Assignment__c> delAAT, Map<ID, OSM_Account_Territory_Assignment__c> delAATMap){
        if(!updateNumberofTerritoriesRunOnce){
            updateNumberofTerritoriesRunOnce = true;
            updateNumberofTerritories(delAATMap.values());
        }
    }
    
    /*
    @author: patrick lorilla
    @date: 23 January 2015
    @param: delAAT- AAT List, delAATMap- AAT Map
    @description: Create Territory History
    */
    
    public static void createTerritoryHistory(List<OSM_Account_Territory_Assignment__c> delAAT, Set<ID> territoryIDS){
 
        Map<Id, OSM_Territory__c> mapTerritoryInfo = new Map<Id, OSM_Territory__c>();
        for(OSM_Territory__c terrLoop : [Select Id, Name, OSM_Area__c, OSM_Business_Unit__c, OSM_Description__c, OSM_Division__c, OSM_GHI_Region_ID__c, OSM_Global__c,  
                                        OSM_International_Area__c, OSM_Number_of_Parents__c, Partner_Account__c, Partner_Account_Id__c, OSM_Parent_Territory__c, 
                                        OSM_Region__c, OSM_Territory_Franchise__c, OSM_Territory_ID__c, OSM_Effective_Start_Date__c, OSM_Effective_Ending_Date__c, OSM_Territory_Market__c, OSM_Territory_Type__c
                                        From OSM_Territory__c Where Id IN: territoryIDS]){
        mapTerritoryInfo.put(terrLoop.Id, terrLoop);
        }
        List<OSM_Territory_History__c> insertTerritoryHistory = new List<OSM_Territory_History__c>();
        for(OSM_Account_Territory_Assignment__c ataLoop : delAAT){
            OSM_Territory_History__c newTH = new OSM_Territory_History__c();
            //newTH.OSM_Account_Name__c = ataLoop.
            system.debug('***@@@' + mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c));
            if(mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c) != null){
                newTH.OSM_Area__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Area__c;
                newTH.OSM_Business_Unit__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Business_Unit__c;
                newTH.OSM_Deleted_Date__c = system.today();
                newTH.OSM_Description__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Description__c;
                newTH.OSM_Division__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Division__c;
                newTH.OSM_Effective_Ending_Date__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Effective_Ending_Date__c;
                newTH.OSM_Effective_Start_Date__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Effective_Start_Date__c;
                newTH.OSM_GHI_Region_ID__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_GHI_Region_ID__c;
                newTH.OSM_Global__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Global__c;
                //newTH.OSM_HCP_Name__c = ataLoop.
                newTH.OSM_International_Area__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_International_Area__c;
                newTH.OSM_Number_of_Parents__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Number_of_Parents__c;
                newTH.OSM_Account_Name__c = ataLoop.OSM_Account_Name__c ;
                newTH.OSM_Partner_Account__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).Partner_Account__c;
                //newTH.OSM_Partner_Account_ID__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).Partner_Account_Id__c;
                newTH.OSM_Parent_Territory__c= mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Parent_Territory__c;
                newTH.OSM_Region__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Region__c;
                newTH.OSM_Territory_name__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).Id;
                newTH.OSM_Territory_Franchise__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Territory_Franchise__c;
                newTH.OSM_Territory_ID__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Territory_ID__c;
                newTH.OSM_Territory_Market__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Territory_Market__c;
                newTH.OSM_Territory_Type__c = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).OSM_Territory_Type__c;
                newTH.OSM_Territory_Nametext__c  = mapTerritoryInfo.get(ataLoop.OSM_Territory_Name__c).Name;
                insertTerritoryHistory.add(newTH);
            }
        }
        
        if(insertTerritoryHistory.size()>0){
            insert insertTerritoryHistory;
        }
    
    }
    
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newATT - new AAT List, terrID - territory ID Set
    @description: delete account share record
    */
    public static void deleteAccountShare(List<OSM_Account_Territory_Assignment__c> delAAT, Set<ID> terrID){
        //Retrieve child and parents/ancestors territories
        Map<Id, Set<Id>> childGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();

        for(OSM_Territory__c loopTerritory : [SELECT Id, OSM_Parent_Territory__c FROM OSM_Territory__c WHERE OSM_Parent_Territory__c != null LIMIT 25000]){
            childParentMap.put(loopTerritory.Id,loopTerritory.OSM_Parent_Territory__c);
            parentChildMap.put(loopTerritory.OSM_Parent_Territory__c,loopTerritory.Id);
            childGroupMap.put(loopTerritory.Id, new Set<Id>{loopTerritory.OSM_Parent_Territory__c});
        }

        for(Integer counter = 0; counter < 50; counter++){
           for(Id loopChildId : childGroupMap.keySet()){
               for(Id loopParentId : childGroupMap.get(loopChildId)){
                   if(childParentMap.get(loopParentId) != null){
                        childGroupMap.get(loopChildId).add(childParentMap.get(loopParentId));
                   }
                }
             }
        }

        for(OSM_Account_Territory_Assignment__c loopAssignment : delAAT){
            if(childGroupMap.containsKey(loopAssignment.OSM_Territory_Name__c)){
                terrID.addAll(childGroupMap.get(loopAssignment.OSM_Territory_Name__c));
            }
        }

        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = [SELECT OSM_Sales_Rep__c, OSM_Account_Access_Level__c, OSM_Territory_ID__c  from OSM_Territory_Sales_Rep_Assignment__c where OSM_Territory_ID__c IN: terrID AND OSM_Inactive__c = false AND OSM_Sales_Rep__r.IsActive = true];
        
        //Prepare AA List
        List<AccountAccessLevel> aaList = prepareAAList(tsraList, delAAT, childGroupMap);
        Set<ID> userIDSet = new Set<ID>();
        Set<ID> acctIDSet = new Set<ID>();
        
        //prepare wrapper object
        System.debug('****** aaListData'+ aaList);
        for(AccountAccessLevel aal : aaList){
            userIDSet.add(aal.userID);
            acctIDSet.add(aal.acctID);
        }
        
        //Prepare acct share list to be inserted
        if(!userIDSet.isEmpty() && !acctIDSet.isEmpty()){
            List<AccountShare> asList = [SELECT ID, OpportunityAccessLevel, UserOrGroupId, AccountAccessLevel, AccountId from AccountShare where UserorGroupId IN: userIDSet AND AccountID IN: acctIDSet AND RowCause = 'Manual'];
            system.debug('test asList 1 ' + asList);
            if(asList != null && !asList.isEmpty() && asList.size()>0){
                delete asList;
            }
        }
    }

    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newATT - new AAT List, terrID - territory ID Set
    @description: prepare and create account share record
    @history: Remove the system.debug inside the loop, causing CPU Timelimit - DQ
              Removed try-catch on insert, let it bubble up - Rescian Rey
    */    
    public static void createAccountShare(List<OSM_Account_Territory_Assignment__c> newAAT, Set<ID> terrID){
        //Retrieve child and parents/ancestors territories
        Map<Id, Set<Id>> childGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();

        for(OSM_Territory__c loopTerritory : [SELECT Id, OSM_Parent_Territory__c FROM OSM_Territory__c WHERE OSM_Parent_Territory__c != null LIMIT 25000]){
            childParentMap.put(loopTerritory.Id,loopTerritory.OSM_Parent_Territory__c);
            parentChildMap.put(loopTerritory.OSM_Parent_Territory__c,loopTerritory.Id);
            childGroupMap.put(loopTerritory.Id, new Set<Id>{loopTerritory.OSM_Parent_Territory__c});
        }

        for(Integer counter = 0; counter < 50; counter++){
           for(Id loopChildId : childGroupMap.keySet()){
               for(Id loopParentId : childGroupMap.get(loopChildId)){
                   if(childParentMap.get(loopParentId) != null){
                        childGroupMap.get(loopChildId).add(childParentMap.get(loopParentId));
                   }
                }
             }
        }

        for(OSM_Account_Territory_Assignment__c loopAssignment : newAAT){
            if(childGroupMap.containsKey(loopAssignment.OSM_Territory_Name__c)){
                terrID.addAll(childGroupMap.get(loopAssignment.OSM_Territory_Name__c));
            }
        }

        //Prepare Sales Rep
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        
        if(!terrID.isEmpty()){
            tsraList = [SELECT OSM_Sales_Rep__c, OSM_Account_Access_Level__c, OSM_Territory_ID__c from OSM_Territory_Sales_Rep_Assignment__c where OSM_Territory_ID__c IN: terrID AND OSM_Inactive__c = false AND OSM_Sales_Rep__r.IsActive = true]; 
        }
        
        List<AccountShare> asList = new List<AccountShare>();

        //prepare wrapper object
        List<AccountAccessLevel> aaList = prepareAAList(tsraList, newAAT, childGroupMap);

        //Prepare acct share list to be inserted
        if(aaList != null && !aaList.isEmpty()){
            for(AccountAccessLevel aal: aaList){
                 asList.add(new AccountShare(AccountId = aal.acctID, AccountAccessLevel = aal.acctAccess, OpportunityAccessLevel = 'None', UserorGroupId = aal.userId));     
            }
        } 
        
        Set<Id> accountShareAccountIds = new Set<Id>();
        Set<Id> accountShareGroupIds = new Set<Id>();
        String queryAccountShare = 'SELECT Id, AccountId, UserOrGroupId FROM AccountShare WHERE ';
        
        for(AccountShare loopShare : asList){
            if(loopShare.accountId != null && loopShare.UserOrGroupId != null){
                accountShareAccountIds.add(loopShare.accountId);
                accountShareGroupIds.add(loopShare.UserOrGroupId);
            }
        }

        if(accountShareAccountIds.size() > 0 && accountShareGroupIds.size() > 0){
            queryAccountShare += '(AccountId IN :accountShareAccountIds AND UserOrGroupId IN :accountShareGroupIds AND RowCause != \'Manual\')';
        }
        
        Set<AccountShare> toInsert = new Set<AccountShare>();
        for(AccountShare loopShare : asList){
            if(loopShare.AccountId != null && loopShare.UserOrGroupId != null){
                toInsert.add(loopShare);
            }
        }
        
        //Collect Query AccountShare
        System.debug('\n\n\n ACCTSHAREQUERY: ' +queryAccountShare );
        Map<String,AccountShare> accountShareMap = new Map<String,AccountShare>();
        if(accountShareAccountIds.size() > 0 && accountShareGroupIds.size() > 0){
            for(AccountShare loopAccountShare : database.query(queryAccountShare)){
                accountShareMap.put(loopAccountShare.AccountId + ':::' + loopAccountShare.UserOrGroupId, loopAccountShare);
            }
        }
        
        Set<AccountShare> toRemove = new Set<AccountShare>();
        for(AccountShare loopShare : toInsert){
            //Bug 39247:Exceed query length: Needs to be addressed before full copy org Fix
            if(accountShareMap.containsKey(loopShare.AccountId + ':::' + loopShare.UserOrGroupId)){
                toRemove.add(loopShare);
            }
        }
        
        for(AccountShare loopShare : toRemove){
            toInsert.remove(loopShare);
        }
        
        asList = new List<AccountShare>();   
        for(AccountShare loopShare : toInsert){
            if(loopShare.AccountId != null && loopShare.UserOrGroupId != null){
                asList.add(loopShare);
            }
        }
        
        if(asList != null && !asList.isEmpty() && asList.size()>0){
            insert asList;
        }    
    }
    
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: tsraList - Territory Sales Rep List newATT - new AAT List
    @description: prepare wrapper object
    */
    public static List<AccountAccessLevel> prepareAAList(List<OSM_Territory_Sales_Rep_Assignment__c> tsraList, List<OSM_Account_Territory_Assignment__c> newAAT, Map<ID, Set<ID>> childMap){
        List<AccountAccessLevel> aaList = new List<AccountAccessLevel>();   
        //Prepare list for user id, territoryID, account access and account id
        for(OSM_Territory_Sales_Rep_Assignment__c trsa: tsraList){
            for(OSM_Account_Territory_Assignment__c aat: newAAT){
                if(trsa.OSM_Territory_ID__c == aat.OSM_Territory_Name__c){
                    aaList.add(new AccountAccessLevel(trsa.OSM_Sales_Rep__c, aat.OSM_Account_Name__c, trsa.OSM_Account_Access_Level__c));
                }
            }
        }    
        if(!childMap.isEmpty()){
            for(OSM_Territory_Sales_Rep_Assignment__c tsra: tsraList){
                for(ID loopId: childMap.keySet()){
                    if(childMap.get(loopId).contains(tsra.OSM_Territory_ID__c)){
                        for(OSM_Account_Territory_Assignment__c aat: newAAT){
                            if(loopId == aat.OSM_Territory_Name__c){
                                aaList.add(new AccountAccessLevel(tsra.OSM_Sales_Rep__c, aat.OSM_Account_Name__c, tsra.OSM_Account_Access_Level__c));
                            }
                        }  
                    } 
                }        
            }
        }
        return aaList;
    }  
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @description: Account Share data list
   */    
    public class AccountAccessLevel{
        public ID userID {get; set;}
        public ID acctID {get; set;}
        public String acctAccess {get; set;} 
         /*
        @author: patrick lorilla
        @date: 2 October 2014
        @param: user- user id, acct- acct id, access- access value
        @description: constructor
         */   
        public AccountAccessLevel(ID user, ID acct, String access){
            userID = user;
            acctID = acct;
            acctAccess = access;           
        } 
    }
    
    
    public static void updateNumberofTerritories(List<OSM_Account_Territory_Assignment__c> newAAT){
        Set<ID> acctIds = new Set<ID>();
        Set<ID> aatIDS= new Set<ID>();
        Map<Id, Integer> acctTerrCtrVal = new Map<Id, Integer>();

        for(OSM_Account_Territory_Assignment__c aat: newAAT){ 
            acctIds.add(aat.OSM_Account_Name__c);   
        }

        List<OSM_Account_Territory_Assignment__c> aatList = [SELECT ID, OSM_Account_Name__c from OSM_Account_Territory_Assignment__c where OSM_Account_Name__c IN: acctIds ];
        for(OSM_Account_Territory_Assignment__c aat: aatList){ 
            Integer ctr = 1;
            if(acctTerrCtrVal.containsKey(aat.OSM_Account_Name__c)){
                ctr = acctTerrCtrVal.get(aat.OSM_Account_Name__c);
                ++ctr;
            }
            acctTerrCtrVal.put(aat.OSM_Account_Name__c, ctr);
        }
        
        List<Account> acctUpdate = new List<Account>();
        for(ID aid: acctIds){
            Account acct = new Account(Id = aid, OSM_Number_of_Territories__c = acctTerrCtrVal.get(aid));
            acctUpdate.add(acct);
        }

        update acctUpdate;
    }
}