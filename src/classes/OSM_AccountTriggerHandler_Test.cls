/*
@author: Patrick Lorilla
@date: 24 September 2014
@description: Test Account Trigger handler

*/

@isTest
public class OSM_AccountTriggerHandler_Test{
    /*
    @author: Patrick Lorilla
    @date: 24 September 2014
    @description: Test Update Related Contacts
    */
    static testmethod void testUpdateRelatedContacts(){
        //Prepare triggers and record types
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Address_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = true;
        insert triggerSwitch;
        
        
        // TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        // triggerSwitch.Address_Trigger__c = true;
        // triggerSwitch.Account_Trigger__c = true;
        // triggerSwitch.Agreement_Trigger__c = false;
        // insert TriggerSwitch;
        
        
        
        // Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        // Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        // Record_Type__c recTypeList = Record_Type__c.getOrgDefaults();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        
        //Prepare account
       
      
        List<Account> accList = new List<Account>();
        for(Integer a=0; a<20; a++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(0,'United States', 'Street 2', 'City 2', 'Alabama', '456', HCORecType));
            //accList.add(OSM_DataFactory.createAccountWithBillingAddress(0,'United States', 'Street 2', 'City 2', 'Alabama', '456', recordTypeIds.Account_HCO_Record_Type__c));
        }
        
        insert accList;
        
        List<Contact> conList = new List<Contact>();
        for(Integer a=0; a<20; a++){
            // conList.add(OSM_DataFactory.createContact(a,accList[a].ID,HCPRecType));
             conList.add(OSM_DataFactory.createContactWithMailingAddress(a, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', HCPRecType));
            //conList.add(OSM_DataFactory.createContactWithMailingAddress(a, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', recordTypeIds.Contact_HCP_Record_Type__c));
        }
        insert conList;
        
        Test.startTest();
        for(Account acc: accList){
            acc.BillingCountry = 'United States';
            acc.BillingStreet = 'Street 2';
            acc.BillingCity = 'City 2';
            acc.BillingState = 'Alabama';     
            acc.BillingPostalCode = '456';   
        }
        
        update accList;    
        Test.stopTest();
        
        /* method for these assertions is commented out
        conList = [SELECT MailingCountry ,MailingStreet, MailingCity, MailingState , MailingPostalCode from Contact]; 
        for(Contact con: conList){
            System.assertEquals(con.MailingCountry,  accList[19].BillingCountry);
            System.assertEquals(con.MailingStreet,  accList[19].BillingStreet);
            System.assertEquals(con.MailingCity,  accList[19].BillingCity);
            System.assertEquals(con.MailingState,  accList[19].BillingState);
            System.assertEquals(con.MailingPostalCode,  accList[19].BillingPostalCode);
        }
        */
    }
    
    /*
     * @author         Kristian Vegerano
     * @description    Tests association of Account Territory Assignment on account creation. 
     * @history        27.OCT.2014 - Kristian Vegerano - Created  
     */
    public static testmethod void testInsertAccountTerritoryAssignment(){
        //Prepare triggers and record types
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Address_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = true;
        insert triggerSwitch;
        
        //Id PayRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        
        // TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        // triggerSwitch.Address_Trigger__c = true;
        // triggerSwitch.Account_Trigger__c = true;
        // triggerSwitch.Agreement_Trigger__c = false;
        // insert TriggerSwitch;
        
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        OSM_Territory__c testTerritory = OSM_DataFactory.createTerritory('Test Territory');
        testTerritory.OSM_Territory_Market__c = 'Oncology';
        testTerritory.OSM_Territory_Franchise__c = 'Breast';
        insert testTerritory;
        
        List<OSM_Alignment_Rule__c> alignList = new List<OSM_Alignment_Rule__c>();
        
        OSM_Alignment_Rule__c testAlignmentRule1 = OSM_DataFactory.createAlignmentRule(1, 'US', '12345', 'Oncology', testTerritory.Id);
        testAlignmentRule1.Rule_Type__c = 'Country';
        testAlignmentRule1.OSM_Territory_Franchise__c = 'Breast';
        alignList.add(testAlignmentRule1);
        
        OSM_Alignment_Rule__c testAlignmentRule2 = OSM_DataFactory.createAlignmentRule(2, 'US', '12345', 'Oncology', testTerritory.Id);
        testAlignmentRule2.Rule_Type__c = 'Partial Postal';
        testAlignmentRule2.OSM_Territory_Franchise__c = 'Breast';
        alignList.add(testAlignmentRule2);
        
        OSM_Alignment_Rule__c testAlignmentRule3 = OSM_DataFactory.createAlignmentRule(1, 'Philippines', '67890', 'Oncology', testTerritory.Id);
        testAlignmentRule3.Rule_Type__c = 'Postal';
        testAlignmentRule3.OSM_Territory_Franchise__c = 'Breast';
        alignList.add(testAlignmentRule3);
        
        OSM_Alignment_Rule__c testAlignmentRule4 = OSM_DataFactory.createAlignmentRule(2, 'Philippines', '67890', 'Oncology', testTerritory.Id);
        testAlignmentRule4.Rule_Type__c = 'Country';
        testAlignmentRule4.OSM_Territory_Franchise__c = 'Breast';
        alignList.add(testAlignmentRule4);
        
        OSM_Alignment_Rule__c testAlignmentRule5 = OSM_DataFactory.createAlignmentRule(3, 'Philippines', '67891', 'Oncology', testTerritory.Id);
        testAlignmentRule5.Rule_Type__c = 'Country';
        testAlignmentRule5.OSM_Territory_Franchise__c = 'Breast';
        testAlignmentRule5.OSM_Country_Code__c = 'PH';
        alignList.add(testAlignmentRule5);
        
        insert alignList;
        
        List<Account> insertAccountList = new List<Account>();
        Account testAccount1 = OSM_DataFactory.createAccountWithBillingAddress(1, 'US', 'Test Street', 'Test City', 'Test Province', '12345', null);
        testAccount1.OSM_Country_Code__c = 'US';
        testAccount1.OSM_Status__c = 'Approved';
        testAccount1.OSM_Specialty__c = 'Hospital';
        testAccount1.OSM_Type__c = 'Hospital';
        testAccount1.OSM_Oncology__c = true;
        testAccount1.OSM_Urology__c = true;
        testAccount1.OSM_Managed_Care__c = true;
        testAccount1.OSM_Strategic_Account__c = true;
        insertAccountList.add(testAccount1);
        
        Account testAccount2 = OSM_DataFactory.createAccountWithBillingAddress(2, 'Philippines', 'Test Street', 'Test City', 'Test Province', '67892', null);
        testAccount2.OSM_Country_Code__c = 'PH';
        testAccount2.OSM_Status__c = 'Approved';
        testAccount2.OSM_Specialty__c = 'Hospital';
        testAccount2.OSM_Type__c = 'Hospital';
        testAccount2.OSM_Oncology__c = true;
        testAccount2.OSM_Urology__c = true;
        testAccount2.OSM_Managed_Care__c = true;
        testAccount2.OSM_Strategic_Account__c = true;
        insertAccountList.add(testAccount2);
        
        Account testAccount3 = OSM_DataFactory.createAccountWithBillingAddress(3, 'Philippines', 'Test Street', 'Test City', 'Test Province', '67890', null);
        testAccount3.OSM_Country_Code__c = 'PH';
        testAccount3.OSM_Status__c = 'Approved';
        testAccount3.OSM_Specialty__c = 'Hospital';
        testAccount3.OSM_Type__c = 'Hospital';
        testAccount3.OSM_Oncology__c = true;
        testAccount3.OSM_Urology__c = true;
        testAccount3.OSM_Managed_Care__c = true;
        testAccount3.OSM_Strategic_Account__c = true;
        insertAccountList.add(testAccount3);
        
        Account testAccount4 = OSM_DataFactory.createAccountWithBillingAddress(4, 'Philippines', 'Test Street', 'Test City', 'Test Province', '09231', null);
        testAccount4.OSM_Country_Code__c = 'PH';
        testAccount4.OSM_Status__c = 'Approved';
        testAccount4.OSM_Specialty__c = 'Hospital';
        testAccount4.OSM_Type__c = 'Hospital';
        testAccount4.OSM_Oncology__c = true;
        testAccount4.OSM_Urology__c = true;
        testAccount4.OSM_Managed_Care__c = true;
        testAccount4.OSM_Strategic_Account__c = true;
        insertAccountList.add(testAccount4);
        
        Account testAccount5 = OSM_DataFactory.createAccountWithBillingAddress(5, 'Philippines', 'Test Street', 'Test City', 'Test Province', '09231', null);
         testAccount5.OSM_Country_Code__c = 'PH';
         testAccount5.OSM_Status__c = 'Approved';
         testAccount5.OSM_Specialty__c = 'Hospital';
         testAccount5.OSM_Type__c = 'Hospital';
         testAccount5.OSM_Oncology__c = true;
         testAccount5.OSM_Urology__c = true;
         testAccount5.OSM_Managed_Care__c = true;
         testAccount5.OSM_Strategic_Account__c = true;
         testAccount5.BillingPostalCode = null;
         testAccount5.OSM_Payor_Hierarchy__c = '2';
         testAccount5.OSM_Payor_Category__c = 'Private';
         insertAccountList.add(testAccount5);
        
        // System.assertEquals(0, [SELECT Id FROM OSM_Account_Territory_Assignment__c].size());
        Test.startTest();
        
        insert insertAccountList;
        
        Set<Id> accIdList = new Set<Id>();
    
        for(Account acc : insertAccountList){
            accIdList.add(acc.Id);
        }
        Test.stopTest();
        // System.assertEquals(3, [SELECT Id FROM OSM_Account_Territory_Assignment__c where OSM_Account_Name__c in :accIdList].size());
    }
    
    /*
     * @author         Kristian Vegerano
     * @description    Tests association of Account Territory Assignment on account creation. 
     * @history        27.OCT.2014 - Kristian Vegerano - Created  
     */
    public static testmethod void testUpdateAccountTerritoryAssignment(){
        //Prepare triggers and record types
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Address_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = true;
        triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        // TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        // triggerSwitch.Address_Trigger__c = true;
        // triggerSwitch.Account_Trigger__c = true;
        // triggerSwitch.Agreement_Trigger__c = false;
        // insert TriggerSwitch;
        
        Id PayRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id agreementRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('International Distributor').getRecordTypeId();
        
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        OSM_Territory__c testTerritory = OSM_DataFactory.createTerritory('Test Territory');
        testTerritory.OSM_Territory_Market__c = 'Oncology';
        testTerritory.OSM_Territory_Franchise__c = 'Breast';
        insert testTerritory;
        
        List<OSM_Alignment_Rule__c> alignmentRulesList = new List<OSM_Alignment_Rule__c>();
        OSM_Alignment_Rule__c testAlignmentRule1 = OSM_DataFactory.createAlignmentRule(1, 'US', '12345', 'Oncology', testTerritory.Id);
        testAlignmentRule1.Rule_Type__c = 'Country';
        testAlignmentRule1.OSM_Territory_Franchise__c = 'Breast';
        alignmentRulesList.add(testAlignmentRule1);
        
        OSM_Alignment_Rule__c testAlignmentRule2 = OSM_DataFactory.createAlignmentRule(2, 'US', '12345', 'Oncology', testTerritory.Id);
        testAlignmentRule2.Rule_Type__c = 'Partial Postal';
        testAlignmentRule2.OSM_Territory_Franchise__c = 'Breast';
        alignmentRulesList.add(testAlignmentRule2);
        
        OSM_Alignment_Rule__c testAlignmentRule3 = OSM_DataFactory.createAlignmentRule(3, 'PH', '67890', 'Oncology', testTerritory.Id);
        testAlignmentRule3.Rule_Type__c = 'Postal';
        testAlignmentRule3.OSM_Territory_Franchise__c = 'Breast';
        alignmentRulesList.add(testAlignmentRule3);
        
        OSM_Alignment_Rule__c testAlignmentRule4 = OSM_DataFactory.createAlignmentRule(4, 'PH', '67890', 'Oncology', testTerritory.Id);
        testAlignmentRule4.Rule_Type__c = 'Postal';
        testAlignmentRule4.OSM_Territory_Franchise__c = 'Breast';
        alignmentRulesList.add(testAlignmentRule4);
        
        OSM_Alignment_Rule__c testAlignmentRule5 = OSM_DataFactory.createAlignmentRule(5, 'PH', '67891', 'Oncology', testTerritory.Id);
        testAlignmentRule5.Rule_Type__c = 'Partial Postal';
        testAlignmentRule5.OSM_Territory_Franchise__c = 'Breast';
        alignmentRulesList.add(testAlignmentRule5);
        
        OSM_Alignment_Rule__c testAlignmentRule6 = OSM_DataFactory.createAlignmentRule(6, 'PH', null, 'Oncology', testTerritory.Id);
        testAlignmentRule6.Rule_Type__c = 'Country';
        testAlignmentRule6.OSM_Territory_Franchise__c = 'Breast';
        alignmentRulesList.add(testAlignmentRule6);
        
        OSM_Alignment_Rule__c testAlignmentRule7 = OSM_DataFactory.createAlignmentRule(7, 'PH', null, 'Oncology', testTerritory.Id);
        alignmentRulesList.add(testAlignmentRule7);
        
        insert alignmentRulesList;
        
        List<Account> insertAccountList = new List<Account>();
        Account testAccount1 = OSM_DataFactory.createAccountWithBillingAddress(1, 'US', 'Test Street', 'Test City', 'Test Province', '12345', PayRecType);
        //Account testAccount1 = OSM_DataFactory.createAccountWithBillingAddress(1, 'US', 'Test Street', 'Test City', 'Test Province', '12345', recordTypeIds.Account_Payor_Record_Type__c);
        testAccount1.OSM_Country_Code__c = 'US';
        testAccount1.OSM_Payor_Hierarchy__c = '2';
        testAccount1.OSM_Payor_Category__c = 'Private';
        testAccount1.OSM_Specialty__c = 'Hospital';
        testAccount1.OSM_Type__c = 'Hospital';
        testAccount1.OSM_Oncology__c = true;
        testAccount1.OSM_Urology__c = true;
        testAccount1.OSM_Managed_Care__c = true;
        testAccount1.OSM_Strategic_Account__c = true;
        
        testAccount1.OSM_Status__c = 'Draft';
        insertAccountList.add(testAccount1);
        
        Account testAccount2 = OSM_DataFactory.createAccountWithBillingAddress(2, 'US', 'Test Street', 'Test City', 'Test Province', '67892', PayRecType);
        //Account testAccount2 = OSM_DataFactory.createAccountWithBillingAddress(2, 'US', 'Test Street', 'Test City', 'Test Province', '67892', recordTypeIds.Account_Payor_Record_Type__c);
        testAccount2.OSM_Country_Code__c = 'US';
        testAccount2.OSM_Payor_Hierarchy__c = '2';
        testAccount2.OSM_Payor_Category__c = 'Private';
        testAccount2.OSM_Specialty__c = 'Hospital';
        testAccount2.OSM_Type__c = 'Hospital';
        testAccount2.OSM_Oncology__c = true;
        testAccount2.OSM_Urology__c = true;
        testAccount2.OSM_Managed_Care__c = true;
        testAccount2.OSM_Strategic_Account__c = true;
        testAccount2.OSM_Status__c = 'Draft';
        insertAccountList.add(testAccount2);
        
        Account testAccount3 = OSM_DataFactory.createAccountWithBillingAddress(3, 'Philippines', 'Test Street', 'Test City', 'Test Province', '67890', PayRecType);
        //Account testAccount3 = OSM_DataFactory.createAccountWithBillingAddress(3, 'Philippines', 'Test Street', 'Test City', 'Test Province', '67890', recordTypeIds.Account_Payor_Record_Type__c);
        testAccount3.OSM_Country_Code__c = 'PH';
        testAccount3.OSM_Payor_Hierarchy__c = '2';
        testAccount3.OSM_Payor_Category__c = 'Private';
        testAccount3.OSM_Specialty__c = 'Hospital';
        testAccount3.OSM_Type__c = 'Hospital';
        testAccount3.OSM_Oncology__c = true;
        testAccount3.OSM_Urology__c = true;
        testAccount3.OSM_Managed_Care__c = true;
        testAccount3.OSM_Strategic_Account__c = true;
        testAccount3.OSM_Status__c = 'Draft';
        insertAccountList.add(testAccount3);
        
        Account testAccount4 = OSM_DataFactory.createAccountWithBillingAddress(4, 'Philippines', 'Test Street', 'Test City', 'Test Province', '09231', null);
        testAccount4.OSM_Country_Code__c = 'PH';
        testAccount4.OSM_Specialty__c = 'Hospital';
        testAccount4.OSM_Type__c = 'Hospital';
        testAccount4.OSM_Oncology__c = true;
        testAccount4.OSM_Urology__c = true;
        testAccount4.OSM_Managed_Care__c = true;
        testAccount4.OSM_Strategic_Account__c = true;
        testAccount4.OSM_Status__c = 'Draft';
        insertAccountList.add(testAccount4);
        
        Account testAccount5 = OSM_DataFactory.createAccountWithBillingAddress(4, 'Philippines', 'Test Street', 'Test City', 'Test Province', null, PayRecType);
        testAccount5.OSM_Country_Code__c = 'PH';
        testAccount5.OSM_Specialty__c = 'Hospital';
        testAccount5.OSM_Type__c = 'Hospital';
        testAccount5.OSM_Oncology__c = true;
        testAccount5.OSM_Urology__c = true;
        testAccount5.OSM_Managed_Care__c = true;
        testAccount1.BillingPostalCode = null;
        testAccount5.OSM_Strategic_Account__c = true;
        testAccount5.OSM_Status__c = 'Draft';
        insertAccountList.add(testAccount5);
        
        insert insertAccountList;
        
        List<OSM_Address_Affiliation__c> addAffList = new List<OSM_Address_Affiliation__c>();
        
        for(Account acc : insertAccountList){
            addAffList.add(new OSM_Address_Affiliation__c(
                OSM_Account__c = acc.Id
            ));
        }
        
        insert addAffList;
        
        Set<Id> accIdList = new Set<Id>();
        
        for(Account acc : insertAccountList){
            accIdList.add(acc.Id);
        }
        
        // Products
        List<Product2> testProdList = new List<Product2>();
        Product2 testProd;
        for(String str: Label.OSM_Product_List.split('::')){
            if(str.length() > 0){
                testProdList.add(OSM_DataFactory.createProduct(str, true));  
            }
        }
        insert testProdList;
        
        // Agreement
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        agreement.RecordTypeId = agreementRecType;
        agreement.Name = Label.OSM_Dummy_Agreement_Name;
        agreement.Apttus__Perpetual__c = true;
        insert agreement;
        
        // Agreement Line Item
        Double lprice = Math.random();
        Datetime today = Datetime.now();
        Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(50 + Math.random());        
        Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')); + Integer.valueOf(50 + Math.random());
        Apttus__AgreementLineItem__c agreementli;
        for(Product2 prod : testProdList){
            agreementli = new Apttus__AgreementLineItem__c(
                Apttus__AgreementId__c = agreement.Id, 
                Apttus__ProductId__c = prod.Id, 
                Apttus__NetPrice__c = Math.random(),  
                Apttus__ListPrice__c =lprice, 
                Apttus__ExtendedPrice__c =Math.random(), 
                Apttus__Quantity__c = 0, 
                GHI_CLM_Expected_Rate__c = 'List Price', 
                Apttus_CMConfig__EndDate__c =defDateEnd, 
                Apttus_CMConfig__StartDate__c =defDateStart, 
                Apttus_CMConfig__PriceMethod__c ='Per Unit', 
                Apttus_CMConfig__PriceType__c ='One Time',
                Apttus__Description__c = 'Agreement Line Item for '+prod.Name,
                GHI_CPQ_Currency__c = 'USD', 
                GHI_CPQ_Price_Method__c = 'Dummy Method',
                GHI_CPQ_Pricing_Schema__c = 'Dummy Schema', 
                GHI_CPQ_Billing_Cycle__c = 'Monthly', 
                GHI_CPQ_CPT_Code__c = 'samplecode123', 
                GHI_CPQ_Billing_Category__c = 'Monthly'
            );
        }
        insert agreementli;
        
        System.assertEquals(0, [SELECT Id FROM OSM_Account_Territory_Assignment__c where OSM_Account_Name__c in :accIdList].size());
        Test.startTest();
        
        for(Account loopAccount : insertAccountList){
            loopAccount.OSM_Status__c = 'Approved';
           // loopAccount.OSM_Country_Code__c = 'WA';
        }            
        update insertAccountList;
        
        Test.stopTest();
        //System.assertEquals(5, [SELECT Id FROM OSM_Account_Territory_Assignment__c].size());
    }
}