/*
    @author: patrick lorilla
    @date: 27 Oct 2014
    @description: Account Territory Assignment Trigger Handler
    @history: 27 Oct 2014 - Patrick Lorilla - Created

*/
@isTest
public class OSM_AcctTerrAssignTriggerHandler_Test{
   /*
        @author: patrick lorilla
        @date: 27 Oct 2014
        @description: Test create account share
        @history: 27 Oct 2014 - Patrick Lorilla - Created
    */
    static testmethod void createAccountShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = false;
        insert triggerSwitch;
        
        //Prepare users accounts and contacts
        User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        insert testUsr;
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
        
        //Prepare parent territories
        List<OSM_Territory__c> pterrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            pterrList.add(OSM_DataFactory.createTerritory('My ParentTerr'+a));  
        }
        
        insert pterrList;
        //Prepare parent territories
        List<OSM_Territory__c> terrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            terrList.add(OSM_DataFactory.createTerritory('My Terr'+a));  
            terrList[a].OSM_Parent_Territory__c = pterrList[a].ID;
        }
        insert terrList ;
        
        //Prepare Sales Reps for TRSA
        List<User> srList = new List<User>();
        for(Integer a=0; a<20; a++){
            srList.add(OSM_DataFactory.createSalesRep('srep'+a));
        }
        insert srList;
        //Prepare TSRA
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<20; a++){
              if(a<5){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, 'Read', null));
                }
                else if (a<10){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, 'Edit', null));
                }
                else if(a<15){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, 'Read', null));
                }
                else{
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, 'Edit', null));
                }
        }
        insert tsraList ;
        test.startTest();
        System.runAs(testUsr){
            //Insert ATA
            List<OSM_Account_Territory_Assignment__c> ataList = new List<OSM_Account_Territory_Assignment__c>();
            for(Integer a=0; a<20; a++){
                if(a<10){
                    ataList.add(OSM_DataFactory.createATA(acct.ID, pterrList[a].ID));
                }
                else{
                    ataList.add(OSM_DataFactory.createATA(acct.ID, terrList[a-10].ID));
                }
            }
            
            insert ataList;
        }
        test.stopTest();
                
    }
     /*
        @author: patrick lorilla
        @date: 27 Oct 2014
        @description: Test Update Account Share
        @history: 27 Oct 2014 - Patrick Lorilla - Created
    */
    static testmethod void updateAccountShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = false;
        insert triggerSwitch;
        
        //Prepare users accounts and contacts
        User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        insert testUsr;
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
        
        //Prepare parent territories
        List<OSM_Territory__c> pterrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            pterrList.add(OSM_DataFactory.createTerritory('My ParentTerr'+a));  
        }
        
        insert pterrList;
        //Prepare parent territories
        List<OSM_Territory__c> terrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            terrList.add(OSM_DataFactory.createTerritory('My Terr'+a));  
            terrList[a].OSM_Parent_Territory__c = pterrList[a].ID;
        }
        insert terrList ;
        
        //Prepare Sales Reps for TRSA
        List<User> srList = new List<User>();
        for(Integer a=0; a<20; a++){
            srList.add(OSM_DataFactory.createSalesRep('srep'+a));
        }
        insert srList;
         //Prepare TSRA
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<20; a++){
              if(a<5){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, 'Read', null));
                }
                else if (a<10){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, 'Edit', null));
                }
                else if(a<15){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, 'Read', null));
                }
                else{
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, 'Edit', null));
                }
        }
        insert tsraList ;
        //Prepare ATA
        List<OSM_Account_Territory_Assignment__c> ataList = new List<OSM_Account_Territory_Assignment__c>();
        for(Integer a=0; a<20; a++){
            if(a<10){
                ataList.add(OSM_DataFactory.createATA(acct.ID, pterrList[a].ID));
                ataList[a].OSM_Override__c = true;
            }
            else{
                ataList.add(OSM_DataFactory.createATA(acct.ID, terrList[a-10].ID));
                ataList[a].OSM_Override__c = false;
            }
        }
            
        insert ataList;
        test.startTest();
        System.runAs(testUsr){
             //Disable override
             for(OSM_Account_Territory_Assignment__c ata: ataList){
                if(!ata.OSM_Override__c){
                    ata.OSM_Override__c = true;
                }
                else{
                    ata.OSM_Override__c = false;
                }
            }    
            
            update ataList;
        }
        test.stopTest();
                
    }
     /*
        @author: patrick lorilla
        @date: 27 Oct 2014
        @description: Test Delete Account Share
        @history: 27 Oct 2014 - Patrick Lorilla - Created
    */
    static testmethod void deleteAccountShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = false;
        insert triggerSwitch;
        
        User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        insert testUsr;
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
        
        //Prepare parent territories
        List<OSM_Territory__c> pterrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            pterrList.add(OSM_DataFactory.createTerritory('My ParentTerr'+a));  
        }
        
        insert pterrList;
        //Prepare parent territories
        List<OSM_Territory__c> terrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            terrList.add(OSM_DataFactory.createTerritory('My Terr'+a));  
            terrList[a].OSM_Parent_Territory__c = pterrList[a].ID;
        }
        insert terrList ;
        
        //Prepare Sales Reps for TRSA
        List<User> srList = new List<User>();
        for(Integer a=0; a<20; a++){
            srList.add(OSM_DataFactory.createSalesRep('srep'+a));
        }
        insert srList;
        //Insert TRSA List
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<20; a++){
              if(a<5){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, 'Read', null));
                }
                else if (a<10){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, 'Edit', null));
                }
                else if(a<15){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, 'Read', null));
                }
                else{
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, 'Edit', null));
                }
        }
        insert tsraList ;
        //Insert ATA List.
        List<OSM_Account_Territory_Assignment__c> ataList = new List<OSM_Account_Territory_Assignment__c>();
        for(Integer a=0; a<20; a++){
            if(a<10){
                ataList.add(OSM_DataFactory.createATA(acct.ID, pterrList[a].ID));
            }
            else{
                ataList.add(OSM_DataFactory.createATA(acct.ID, terrList[a-10].ID));
            }
        }
            
        insert ataList;
        test.startTest();
        
        //System.runAs(testUsr){
            //Delete ATA Kust
            delete ataList;
        //}
        test.stopTest();
                
    }


}