/**
 * File Info
 * ----------------------------------
 * @filename       OSM_AddressAffiliationHandler_Test.cls
 * @created        25.SEPT.2014
 * @author         Kristian Vegerano
 * @description    Test class for OSM_AddressAffiliationTriggerHandler 
 * @history        25.SEPT.2014 - Kristian Vegerano - Created  
 * @history        27.OCT.2014 - Michiel Patricia Robrigado - Modified
 */

@isTest (seeAllData = true)
public class OSM_AddressAffiliationHandler_Test{
    /**
     * @author         Kristian Vegerano
     * @description    Tests address transfer from old to new account 
     * @history        25.SEPT.2014 - Kristian Vegerano - Created  
     * @history        27.OCT.2014 - Michiel Patricia Robrigado - Modified
     */
    static testmethod void testTransferAddress(){
        List<OSM_Address_Affiliation__c> addAffList = new List<OSM_Address_Affiliation__c>();
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Address_Affiliation_Trigger__c = true;
        //insert triggerSwitch;
        Record_Type__c recordLists = OSM_DataFactory.recordLists();
        //insert recordLists;
        id conRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        id custAffRecTypeId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Cnt-Cnt Affiliation').getRecordTypeId();

        Account testOldAccount = OSM_DataFactory.createAccountWithBillingAddress(1, 
                                                                               'United States', 
                                                                               'Test Street', 
                                                                               'Test City', 
                                                                               'Alaska', 
                                                                               '123', 
                                                                               recordLists.Account_HCO_Record_Type__c);
                                                                               
        testOldAccount.OSM_CUstomer_service_Team__c = 'fefasfs';
        insert testOldAccount;
        
        Account payorAccount = OSM_DataFactory.createPayorAccountWithBillingAddress(2,
                                                                               '2',
                                                                               'Private',
                                                                               'United States', 
                                                                               'Test Street', 
                                                                               'Test City', 
                                                                               'Alaska', 
                                                                               '123', 
                                                                               recordLists.Account_Payor_Record_Type__c);
        insert payorAccount;
        
        Account testNewAccount = OSM_DataFactory.createAccountWithBillingAddress(3, 
                                                                                'England', 
                                                                                'Test Street', 
                                                                                'Test City', 
                                                                                'Test State', 
                                                                                '123', 
                                                                                recordLists.Account_HCO_Record_Type__c);
        testNewAccount.OSM_CUstomer_service_Team__c = 'fefasfs';
        insert testNewAccount;
        
        OSM_Address__c testAddress = OSM_DataFactory.createAddress('Test Address 1',
                                                                   'Test Address 2',
                                                                   'Test City',
                                                                   'England',
                                                                   'Test State',
                                                                   '12345');
        insert testAddress;
        
        OSM_Address__c testAddress2 = OSM_DataFactory.createAddress('Test Address 12',
                                                                   null,
                                                                   'Test City',
                                                                   'England',
                                                                   'Test State',
                                                                   '12345');
        insert testAddress2;
        
        OSM_AddressAffiliationTriggerHandler.hasRun = false;
        OSM_AddressAffiliationTriggerHandler.removeOtherMainRun = false;
        OSM_Address_Affiliation__c testAffiliation = OSM_DataFactory.createAddressAffiliation(testOldAccount.Id, 
                                                                                              testAddress.Id, 
                                                                                              'Test Attention', 
                                                                                              false, 
                                                                                              'testEmail@test.com', 
                                                                                              'Test Fax', 
                                                                                              null,
                                                                                              null, 
                                                                                              true, 
                                                                                              '123123', 
                                                                                              'Main', 
                                                                                              recordLists.HCO_Address_Affiliation__c);
        testAffiliation.OSM_Phone_Extension__c ='2313';
        insert testAffiliation;
        
        OSM_Plan__c testPlan = OSM_DataFactory.createPlan(payorAccount.Id, 'New Plan', 'COMM', 'Active');
        
        insert testPlan;
        OSM_AddressAffiliationTriggerHandler.hasRun = false;
        OSM_AddressAffiliationTriggerHandler.removeOtherMainRun = false;
        OSM_Address_Affiliation__c testAffiliation2 = OSM_DataFactory.createAddressAffiliation2(testOldAccount.Id, 
                                                                                              testAddress.Id,
                                                                                              testPlan.Id,
                                                                                              'Test Attention', 
                                                                                              false, 
                                                                                              'testEmail@test.com', 
                                                                                              'Test Fax', 
                                                                                              null,
                                                                                              null, 
                                                                                              true, 
                                                                                              '123123', 
                                                                                              'Main', 
                                                                                              recordLists.HCO_Address_Affiliation__c);
                                                                                              testAffiliation2.OSM_Phone_Extension__c = '2313';
        insert testAffiliation2;
        OSM_AddressAffiliationTriggerHandler.hasRun = false;
        OSM_AddressAffiliationTriggerHandler.removeOtherMainRun = false;
        OSM_Address_Affiliation__c testAffiliation3 = OSM_DataFactory.createAddressAffiliation2(testOldAccount.Id, 
                                                                                              testAddress2.Id,
                                                                                              null,
                                                                                              'Test Attention', 
                                                                                              false, 
                                                                                              'testEmail@test.com', 
                                                                                              'Test Fax', 
                                                                                              null,
                                                                                              null, 
                                                                                              true, 
                                                                                              '123123', 
                                                                                              'Main', 
                                                                                              recordLists.HCO_Address_Affiliation__c);
        testAffiliation3.OSM_Phone_Extension__c = '2313';
        insert testAffiliation3;
        
        OSM_Address_Affiliation__c testAffiliation5 = OSM_DataFactory.createAddressAffiliation2(testOldAccount.Id, 
                                                                                              testAddress.Id,
                                                                                              testPlan.Id,
                                                                                              'Test Attention', 
                                                                                              false, 
                                                                                              'testEmail@test.com', 
                                                                                              'Test Fax', 
                                                                                              null,
                                                                                              null, 
                                                                                              true, 
                                                                                              '123123', 
                                                                                              'Main', 
                                                                                              recordLists.Plan_Address_Affiliation__c);
        OSM_AddressAffiliationTriggerHandler.hasRun = false;
        testAffiliation5.OSM_Phone_Extension__c = '2313';
        insert testAffiliation5;
        
        OSM_Address_Affiliation__c testAffiliation4 = OSM_DataFactory.createAddressAffiliation2(null, 
                                                                                              testAddress.Id,
                                                                                              testPlan.Id,
                                                                                              'Test Attention', 
                                                                                              false, 
                                                                                              'testEmail@test.com', 
                                                                                              'Test Fax', 
                                                                                              null,
                                                                                              null, 
                                                                                              false, 
                                                                                              '123123', 
                                                                                              'Main', 
                                                                                              recordLists.Plan_Address_Affiliation__c);
        OSM_AddressAffiliationTriggerHandler.hasRun = false;
        testAffiliation4.OSM_Phone_Extension__c = '2313';
        insert testAffiliation4;
        
        Contact testContact = OSM_DataFactory.createContact(1, testOldAccount.Id, conRecTypeId);
        
        insert testContact;
        
        Customer_Affiliation__c testCustomerAffiliation = OSM_DataFactory.createAccountContactCustomerAffiliation(testOldAccount.Id,
                                                                   testContact.Id,
                                                                   recordLists.RT_Acct_Cnt_CustAffiliation__c);
        testCustomerAffiliation.OSM_Role__c = 'Primary';
        insert testCustomerAffiliation;
        
        Test.startTest();
            OSM_AddressAffiliationTriggerHandler.hasRun = false;
            testAffiliation.OSM_Account__c = testNewAccount.Id;
            testAffiliation.OSM_Main_Address__c = false;
            OSM_AddressAffiliationTriggerHandler.AddAffRollUpRunOnce = false;
            OSM_AddressAffiliationTriggerHandler.removeOtherMainRun = false;
            update testAffiliation;
            testAffiliation3.OSM_Main_Address__c = false;
            OSM_AddressAffiliationTriggerHandler.removeOtherMainRun = false;
            update testAffiliation3;
            OSM_AddressAffiliationTriggerHandler.removeOtherMainRun = false;
            testAffiliation4.OSM_Main_Address__c = false;
            update testAffiliation4;
            OSM_AddressAffiliationTriggerHandler.removeOtherMainRun = false;
            testAffiliation4.OSM_Account__c = testNewAccount.Id;
            update testAffiliation4;
            OSM_AddressAffiliationTriggerHandler.setNotRun();
            testCustomerAffiliation.OSM_Role__c = 'Primary';
            update testCustomerAffiliation;
            delete testAffiliation2;
        Test.stopTest();
    }
}