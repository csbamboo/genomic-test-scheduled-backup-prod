/**
 * File Info
 * ----------------------------------
 * @filename       OSM_AddressAffiliationTriggerHandler.cls
 * @created        24.SEPT.2014
 * @author         Kristian Vegerano
 * @description    Class contains functionality of OSM_AddressAffiliationTrigger. 
 * @history        24.SEPT.2014 - Kristian Vegerano - Created  
 */
public class OSM_AddressAffiliationTriggerHandler{
    public static Boolean hasRun = false;
    public static Boolean AddAffRollUpRunOnce = false;
    public static Boolean removeOtherMainRun = false;
    public static void setRun(){
        hasRun = true;
    }
    public static void setNotRun(){
        hasRun = false;
    }    
     
    /**
     * @author         Kristian Vegerano
     * @description    Method for that fires before insertion of a record. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
     */
    public static void onBeforeInsert(List<OSM_Address_Affiliation__c> addressAffilationList){
                system.debug('test before insert');
        Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
        List<OSM_Address_Affiliation__c> filteredAddress = new List<OSM_Address_Affiliation__c>();
        Set<Id> planIds = new Set<Id>();
        for(OSM_Address_Affiliation__c aaLoop : addressAffilationList){
            if(aaLoop.RecordTypeId != recordTypes.Study_Address_Affiliation_RT_ID__c){
                filteredAddress.add(aaLoop);
            }
            if(aaLoop.OSM_Plan__c != null){
                planIds.add(aaLoop.OSM_Plan__c);    
            }
            
        }
        removeOtherMain(filteredAddress, true);
      
    } 
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for that fires before updating a record. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
     */
    public static void onBeforeUpdate(List<OSM_Address_Affiliation__c> addressAffilationList, Map<ID, OSM_Address_Affiliation__c> oldAddAffiliationMap){
        system.debug('test before update');
        Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
        List<OSM_Address_Affiliation__c> filteredAddress = new List<OSM_Address_Affiliation__c>();
        for(OSM_Address_Affiliation__c aaLoop : addressAffilationList){
            if(aaLoop.RecordTypeId != recordTypes.Study_Address_Affiliation_RT_ID__c){
                filteredAddress.add(aaLoop);
            }
           
        }
        
        removeOtherMain(filteredAddress, false);
    }
    
   
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for transferring address between Accounts. 
     * @history        24.SEPT.2014 - Kristian Vegerano - Created  
     */
    public static void onAfterUpdate(List<OSM_Address_Affiliation__c> addAffiliationList, Map<ID, OSM_Address_Affiliation__c> oldAddAffiliationMap){
        system.debug('test after update');
        //Updates main address from old to new account
        List<OSM_Address_Affiliation__c> processAddAffList = new List<OSM_Address_Affiliation__c>();
        List<OSM_Address_Affiliation__c> processAddAffListRem = new List<OSM_Address_Affiliation__c>();
        Map<Id, OSM_Address_Affiliation__c> processAddAffMap = new Map<Id, OSM_Address_Affiliation__c>();
        
        //Get Main Address Affiliation Records
        for(OSM_Address_Affiliation__c loopAddAff : addAffiliationList){
            if(loopAddAff.OSM_Main_Address__c){
                processAddAffList.add(loopAddAff);
                processAddAffMap.put(loopAddAff.Id, oldAddAffiliationMap.get(loopAddAff.Id));
            }
            if(!loopAddAff.OSM_Main_Address__c && oldAddAffiliationMap.get(loopAddAff.Id).OSM_Main_Address__c){
                processAddAffListRem.add(loopAddAff);
            }
        }
         
        //Process Main Address Affiliation Records
        transferAddress(processAddAffList, processAddAffMap);
        if(!AddAffRollUpRunOnce){
          
           AddAffRollUp(addAffiliationList);
        }
        
        if(!removeOtherMainRun){
            nullifyAddress(processAddAffListRem);
        }
    }
    /**
     * @author         Michiel Patricia M. Robrigado
     * @description    Method for after insert
     * @history        5.Feb.2015 - Michiel Patricia M. Robrigado - Created  
     */
    public static void onAfterInsert(List<OSM_Address_Affiliation__c> addAffiliationList){
       
        if(!AddAffRollUpRunOnce){
           AddAffRollUp(addAffiliationList);
        }
    }
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 5.Feb.2015
    @param: delAddAffil- ddAffil List
    @description: After Delete Method
    */
    
    public static void onBeforeDelete(List<OSM_Address_Affiliation__c> delAddAff){
        //AddAffRollUp(delAddAff);
        preventDeleteNoMainAdd(delAddAff);
    }
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 5.Feb.2015
    @param: delAddAffil- ddAffil List
    @description: After Delete Method
    */
    
    public static void onAfterDelete(List<OSM_Address_Affiliation__c> delAddAff){
        List<OSM_Address_Affiliation__c> processAddAffListRem = new List<OSM_Address_Affiliation__c>();
        for(OSM_Address_Affiliation__c loopAddAff : delAddAff){
            processAddAffListRem.add(loopAddAff);
        }
        
        nullifyAddress(processAddAffListRem);
        AddAffRollUp(delAddAff);
    }
     /**
     * @author         Patrick Lorilla
     * @description    make address null
     * @param:         prevent main address deletion 
     * @history        4.April.2015 - Patrick Lorilla- Created  
     */
    public static void preventDeleteNoMainAdd(List<OSM_Address_Affiliation__c> delAddAff){
        Set<ID> acctsID = new Set<ID>();
        for(OSM_Address_Affiliation__c addAff: delAddAff){
            
            acctsID.add(addAff.OSM_Account__c);    
        }
        
        Map<ID, Account> acctMap = new Map<Id,Account>([SELECT Id, OSM_Status__c from Account where ID IN: acctsID]);
        
        for(OSM_Address_Affiliation__c addAff: delAddAff ){
            if(acctMap.containsKey(addAff.OSM_Account__c) &&  acctMap.get(addAff.OSM_Account__c).OSM_Status__c == 'Approved'){
                addAff.addError(Label.No_Main_Address_Delete);    
            } 
        }
        
    }
    /**
     * @author         Patrick Lorilla
     * @description    make address null
     * @param:         address affiliations
     * @history        2.Feb.2015 - Patrick Lorilla- Created  
     */
    public static void nullifyAddress(List<OSM_Address_Affiliation__c> addAffiliationList){
        Set<Id> deleteAddressPlanIds = new Set<Id>();
        Set<Id> deleteAddressAccountIds = new Set<Id>();
        List<Account> updateAccountsList = new List<Account>();
        List<OSM_Plan__c> updatePlanList = new List<OSM_Plan__c>();
        for(OSM_Address_Affiliation__c loopAddAff : addAffiliationList){
            if(loopAddAff.OSM_Type__c != null && loopAddAff.OSM_Type__c.contains('Main') && loopAddAff.OSM_Account__c != null){
                deleteAddressAccountIds.add(loopAddAff.OSM_Account__c); 
            }
            else if (loopAddAff.OSM_Type__c != null && loopAddAff.OSM_Type__c.contains('Main') && loopAddAff.OSM_Plan__c != null){
                 deleteAddressPlanIds.add(loopAddAff.OSM_Plan__c);     
            }
        }
        //nullify
        for(Id loopAccountId : deleteAddressAccountIds){
                Account addressAccount = new Account(Id = loopAccountId);
                addressAccount.BillingStreet = null;
                addressAccount.BillingCity = null;
                addressAccount.BillingState =  null;
                addressAccount.BillingPostalCode = null;
                addressAccount.BillingCountry = null;
                addressAccount.OSM_Email__c = null;
                addressAccount.Fax = null;
                addressAccount.Phone = null;
              
                updateAccountsList.add(addressAccount);
         }
         
         for(Id loopPlanId : deleteAddressPlanIds){
                OSM_Plan__c addressPlan = new OSM_Plan__c(Id = loopPlanId);
                addressPlan.OSM_Address_1__c =  null;
                addressPlan.OSM_Address_2__c =  null;
                addressPlan.OSM_City__c =  null;
                addressPlan.OSM_State__c =  null;
                addressPlan.OSM_Zip__c =  null;
                addressPlan.OSM_Country__c = null;
              
                updatePlanList.add(addressPlan);
         }

         //Update Accounts
         if(updateAccountsList.size() > 0){
                update updateAccountsList;
          }
          if(updatePlanList.size() > 0){
                update updatePlanList;
          }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for that transfers address from old Accounts to new Accounts. 
     * @history        24.SEPT.2014 - Kristian Vegerano - Created  
     */
    public static void transferAddress(List<OSM_Address_Affiliation__c> addAffiliationList, Map<ID, OSM_Address_Affiliation__c> oldAddAffiliationMap){
        system.debug('test enter transferAddress');
        //if(!hasRun){
            //Stop recursive loops
            setRun();
            Set<Id> deleteAddressAccountIds = new Set<Id>();
            List<Account> updateAccountsList = new List<Account>();
            Map<Id, Id> accountToAddress = new Map<Id, Id>();
            Map<Id, Id> accountToAddAff = new Map<Id, Id>();
            
            //Filter address affiliation records 
            for(OSM_Address_Affiliation__c loopAddAff : addAffiliationList){
                if(oldAddAffiliationMap.containsKey(loopAddAff.Id)){
                    if(loopAddAff.OSM_Account__c != oldAddAffiliationMap.get(loopAddAff.Id).OSM_Account__c){
                        //Get old Account
                        deleteAddressAccountIds.add(oldAddAffiliationMap.get(loopAddAff.Id).OSM_Account__c);
                        //Get new Account
                        accountToAddress.put(loopAddAff.OSM_Account__c, loopAddAff.OSM_Address__c);
                        accountToAddAff.put(loopAddAff.OSM_Account__c, loopAddAff.Id);
                    }
                }
            }
            
            //Delete Addresses on old Accounts
            for(Id loopAccountId : deleteAddressAccountIds){
                Account addressAccount = new Account(Id = loopAccountId);
                addressAccount.BillingStreet = null;
                addressAccount.BillingCity = null;
                addressAccount.BillingState =  null;
                addressAccount.BillingPostalCode = null;
                addressAccount.BillingCountry = null;
                addressAccount.OSM_Email__c = null;
                addressAccount.Fax = null;
                addressAccount.Phone = null;
                updateAccountsList.add(addressAccount);
            }
            
            //Set Addresses on new Accounts
            if(!accountToAddAff.values().isEmpty() && !accountToAddress.values().isEmpty()){
                Map<Id, OSM_Address_Affiliation__c> newAddressAffMap = new Map<Id, OSM_Address_Affiliation__c>([SELECT Id, OSM_Email__c, OSM_Fax__c, 
                                                                                                                       OSM_Phone_Number__c
                                                                                                                FROM OSM_Address_Affiliation__c 
                                                                                                                WHERE Id IN :accountToAddAff.values()]);
                Map<Id, OSM_Address__c> newAddressMap = new Map<Id, OSM_Address__c>([SELECT Id, OSM_Address_Line_1__c, OSM_Address_Line_2__c, OSM_City__c, 
                                                                                            OSM_Country__c, OSM_State__c, OSM_Zip__c
                                                                                     FROM OSM_Address__c 
                                                                                     WHERE Id IN :accountToAddress.values()]);
                for(Id loopAccountId : accountToAddress.keySet()){
                    Account addressAccount = new Account(Id = loopAccountId);
                    //Get details from Address
                    system.debug('test newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_Address_Line_2__c ' + newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_Address_Line_2__c);
                    if(newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_Address_Line_2__c != null){
                        system.debug('test enter add2');
                        addressAccount.BillingStreet = newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_Address_Line_1__c + ' , ' + newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_Address_Line_2__c;
                    }
                    else{
                        addressAccount.BillingStreet = newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_Address_Line_1__c;
                    }
                    system.debug('test addressAccount.BillingStreet ' + addressAccount.BillingStreet);
                    addressAccount.BillingCity = newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_City__c;
                    addressAccount.BillingState =  newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_State__c;
                    addressAccount.BillingPostalCode = newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_Zip__c;
                    addressAccount.BillingCountry = newAddressMap.get(accountToAddress.get(loopAccountId)).OSM_Country__c;
                    //Get details from Address Affiliate
                    addressAccount.OSM_Email__c = newAddressAffMap.get(accountToAddAff.get(loopAccountId)).OSM_Email__c;
                    addressAccount.Fax = newAddressAffMap.get(accountToAddAff.get(loopAccountId)).OSM_Fax__c;
                    addressAccount.Phone = newAddressAffMap.get(accountToAddAff.get(loopAccountId)).OSM_Phone_Number__c;
                    
                    updateAccountsList.add(addressAccount);
                }
            }
            
            //Update Accounts
            if(updateAccountsList.size() > 0){
                update updateAccountsList;
            }
        //}
    }
    
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 5 Feb 2015
    @description: Trigger Roll up for Address Affil Count
   */  
    public static void AddAffRollUp(List<OSM_Address_Affiliation__c> addAffiliationList){
       Set<Id> addAffilId = new Set<Id>();
        for(OSM_Address_Affiliation__c addAffLoop: addAffiliationList){
            addAffilId.add(addAffLoop.OSM_Account__c);
        }
        addAffilId.remove(null);
        map<Id, Integer> addAffCount = new map<Id, Integer>();
        list<Account> accListUpdate = new list<Account>();
        
        if(!addAffilId.isEmpty()){
            for(AggregateResult ar: [Select OSM_Account__c, Count(Id) From OSM_Address_Affiliation__c Where OSM_Account__c IN: addAffilId Group BY OSM_Account__c]){
                addAffCount.put((Id)ar.get('OSM_Account__c'),(Integer)ar.get('expr0'));
            }
            //modified to reduce queries - Paul Wittmeyer 7/3/2015
            //list<account> accountList = new List<Account>([Select Id, OSM_Address_Affiliation_Count__c From Account Where Id IN: addAffilId]);
			list<Account> accountlist = new List<Account>();
			for(Id aId : addAffilId){
				Account a = new Account(Id = aId);
				accountList.add(a);
			}
            for(Account acc: accountList){
                if(acc.Id != null){ //needed to check if null to avoid the null ID exception (patrick l 7.10.2015)
                    Integer addAffCounts = addAffCount.get(acc.Id);
                    acc.OSM_Address_Affiliation_Count__c = addAffCounts == null ? 0 : addAffCounts;
                    accListUpdate.add(acc);
                }
            }
        }
        
        if(accListUpdate.size() > 0) {
            update accListUpdate;
            AddAffRollUpRunOnce = true;
        }
        
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Reset primary value on role field for other customer affiliation records. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
                       7 MAY 2015 - Patrick Added Plan Addres Affiliation
     */
    public static void removeOtherMain(List<OSM_Address_Affiliation__c> addressAffilationList, boolean isInsert){
        if(!hasRun){
            //Stop recursive loops
            setRun();
            Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
            
            //Process correct record type
            List<OSM_Address_Affiliation__c> accountAddressList = new List<OSM_Address_Affiliation__c>();
             List<OSM_Address_Affiliation__c> planAddressList = new List<OSM_Address_Affiliation__c>();
            for(OSM_Address_Affiliation__c loopAddressAffiliation : addressAffilationList){
                if(loopAddressAffiliation.RecordTypeId == recordTypes.Plan_Address_Affiliation__c){
                     planAddressList.add(loopAddressAffiliation);   
                }
                else{
                     accountAddressList.add(loopAddressAffiliation);
                }
            }

            if(accountAddressList.size() > 0){
                List<OSM_Address_Affiliation__c> excludeRecords = new List<OSM_Address_Affiliation__c>();                
                Set<Id> excludeSet = new Set<Id>();
                Set<Id> removeMainSet = new Set<Id>();
                for(OSM_Address_Affiliation__c loopAddressAffiliation : addressAffilationList){
                    if(loopAddressAffiliation.OSM_Type__c != null){
                        if(loopAddressAffiliation.OSM_Type__c.contains('Main') && loopAddressAffiliation.OSM_Account__c != null){
                            loopAddressAffiliation.OSM_Main_Address__c = true;
                            excludeSet.add(loopAddressAffiliation.Id);
                            excludeRecords.add(loopAddressAffiliation);
                            removeMainSet.add(loopAddressAffiliation.OSM_Account__c);
                        }
                    }
                }

                List<OSM_Address_Affiliation__c> removeMainList = new List<OSM_Address_Affiliation__c>();
                if(removeMainSet.size() > 0){
                    
                    for(OSM_Address_Affiliation__c loopAddressAffilation : [SELECT Id, OSM_Type__c FROM OSM_Address_Affiliation__c WHERE OSM_Account__c IN :removeMainSet AND Id NOT IN :excludeSet]){
                        if(loopAddressAffilation.OSM_Type__c != null){
                            loopAddressAffilation.OSM_Type__c = loopAddressAffilation.OSM_Type__c.remove('Main');
                            loopAddressAffilation.OSM_Main_Address__c = false;
                            removeMainList.add(loopAddressAffilation);
                        }
                    } 
                }
                if(removeMainList.size() > 0){
                    update removeMainList;
                    removeOtherMainRun = true;
                }  
                copyAddressesOnAccounts(removeMainSet, excludeRecords, isInsert);
                copyAddressesOnContacts(removeMainSet);
 
            }
            system.debug('test planAddressList ' + planAddressList);
            if(planAddressList.size() > 0){
                List<OSM_Address_Affiliation__c> excludeRecordsPlan = new List<OSM_Address_Affiliation__c>();                
                Set<Id> excludeSetPlan = new Set<Id>();
                Set<Id> removeMainSetPlan = new Set<Id>();
                for(OSM_Address_Affiliation__c loopAddressAffiliation : addressAffilationList){
                        if(loopAddressAffiliation.OSM_Type__c != null){
                            if(loopAddressAffiliation.OSM_Type__c.contains('Main')){
                                loopAddressAffiliation.OSM_Main_Address__c = true;
                                excludeSetPlan .add(loopAddressAffiliation.Id);
                                excludeRecordsPlan.add(loopAddressAffiliation);
                                removeMainSetPlan.add(loopAddressAffiliation.OSM_Plan__c);
                                System.debug('\n\n\n NOWMAIN'+loopAddressAffiliation);
                                System.debug('\n\n\n NOWMAINID'+loopAddressAffiliation.Id); 
                            }
                        }
                  }
                  
                List<OSM_Address_Affiliation__c> removeMainListPlan = new List<OSM_Address_Affiliation__c>();
                system.debug('test removeMainListPlan ' + removeMainListPlan);
                if(removeMainSetPlan.size() > 0){
                    
                    for(OSM_Address_Affiliation__c loopAddressAffilation : [SELECT Id, OSM_Type__c FROM OSM_Address_Affiliation__c WHERE OSM_Plan__c IN :removeMainSetPlan AND Id NOT IN :excludeSetPlan]){
                        system.debug('test loopAddressAffilation.OSM_Type__c ' + loopAddressAffilation.OSM_Type__c);
                        if(loopAddressAffilation.OSM_Type__c != null){
                            loopAddressAffilation.OSM_Type__c = loopAddressAffilation.OSM_Type__c.remove('Main');
                            loopAddressAffilation.OSM_Main_Address__c = false;
                            removeMainListPlan.add(loopAddressAffilation);
                            System.debug('\n\n\n NOTMAIN'+loopAddressAffilation);
                            System.debug('\n\n\n NOTMAINID'+loopAddressAffilation.Id); 
                        }
                    } 
                }
                system.debug('test removeMainListPlan ' + removeMainListPlan);
                if(removeMainListPlan .size() > 0){
                    update removeMainListPlan;
                    removeOtherMainRun = true;
                } 
                 System.debug('\n\n\n NOWMAINLIST'+removeMainSetPlan);
                copyAddressOnPlans(removeMainSetPlan,excludeRecordsPlan);
            }
        }
    } 
   
    /**
     * @author         Kristian Vegerano
     * @description    Copies Address account's address fields based on address affiliations. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
     */
    public static void copyAddressesOnAccounts(Set<Id> accountIds, List<OSM_Address_Affiliation__c> excludeRecords, boolean isInsert){
        Map<Id,Id> accountAddressMap = new Map<Id,Id>();
        Map<Id,OSM_Address_Affiliation__c> accountAffiliateMap = new Map<Id,OSM_Address_Affiliation__c>();
        for(OSM_Address_Affiliation__c loopAddressAffilation : excludeRecords){
            accountAddressMap.put(loopAddressAffilation.OSM_Account__c, loopAddressAffilation.OSM_Address__c);
            accountAffiliateMap.put(loopAddressAffilation.OSM_Account__c, loopAddressAffilation);
        }
        
        Map<Id, OSM_Address__c> addressMap = new Map<Id, OSM_Address__c>();
        
        if(!accountAddressMap.values().isEmpty()){
            addressMap = new Map<Id, OSM_Address__c>([SELECT Id, OSM_Address_Line_1__c, OSM_Address_Line_2__c, OSM_City__c, OSM_Country__c,
                                                                                     OSM_Full_Address__c, OSM_State__c, OSM_Zip__c, OSM_Country_Code__c, OSM_Customer_Service_Team__c
                                                                              FROM OSM_Address__c
                                                                              WHERE Id IN :accountAddressMap.values()]);
        }
        
        Account tempAccount;
        List<Account> updateAccounts = new List<Account>();
        for(Id loopAccountId : accountIds){
            tempAccount = new Account(Id = loopAccountId);
            if(accountAddressMap.containsKey(loopAccountId)){
                if(addressMap.containsKey(accountAddressMap.get(loopAccountId))){
                    if(addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Address_Line_1__c != null){
                        tempAccount.BillingStreet = addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Address_Line_1__c;
                    }
                    if(addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Address_Line_2__c != null){
                        tempAccount.BillingStreet += ', ' + addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Address_Line_2__c;
                    }
                    if(addressMap.get(accountAddressMap.get(loopAccountId)).OSM_City__c != null){
                        tempAccount.BillingCity = addressMap.get(accountAddressMap.get(loopAccountId)).OSM_City__c;
                    }
                    if(addressMap.get(accountAddressMap.get(loopAccountId)).OSM_State__c != null){
                        tempAccount.BillingState =  addressMap.get(accountAddressMap.get(loopAccountId)).OSM_State__c;
                    }
                    if(addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Zip__c != null){
                        tempAccount.BillingPostalCode = addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Zip__c;
                    }
                    if(addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Country__c != null){
                        tempAccount.BillingCountry = addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Country__c;
                    }
                    if(addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Country__c != null){
                        tempAccount.OSM_Country_Code__c = addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Country_Code__c;
                    }
                    /*
                        @author: Rulleth Decena
                        @createdDate: 29 APR 2015
                        @description: Assign Customer Service Team to Parent Account - User Story 29375:Stamp Customer Service Team on Account
                    */
                    if(addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Customer_Service_Team__c != null){
                        tempAccount.OSM_Customer_Service_Team__c = addressMap.get(accountAddressMap.get(loopAccountId)).OSM_Customer_Service_Team__c;
                    }
                }
                if(accountAffiliateMap.containsKey(loopAccountId)){
                    if(accountAffiliateMap.get(loopAccountId).OSM_Email__c != null){
                        tempAccount.OSM_Email__c = accountAffiliateMap.get(loopAccountId).OSM_Email__c;
                    }
                    if(accountAffiliateMap.get(loopAccountId).OSM_Fax__c != null){
                        tempAccount.Fax = accountAffiliateMap.get(loopAccountId).OSM_Fax__c;
                    }
                    if(accountAffiliateMap.get(loopAccountId).OSM_Phone_Number__c != null){
                        tempAccount.Phone = accountAffiliateMap.get(loopAccountId).OSM_Phone_Number__c;
                    }
                    if(accountAffiliateMap.get(loopAccountId).OSM_Phone_Extension__c != null){
                        tempAccount.OSM_Main_Phone_Extension__c= accountAffiliateMap.get(loopAccountId).OSM_Phone_Extension__c;
                    }
                    if(accountAffiliateMap.get(loopAccountId).OSM_Attention_To__c != null){
                        tempAccount.OSM_Address_Attn_To__c = accountAffiliateMap.get(loopAccountId).OSM_Attention_To__c;
                    }
                }
            }
            updateAccounts.add(tempAccount);
        }
        
        if(updateAccounts.size() > 0){
            update updateAccounts;
            /*accountIds = new Set<Id>();
            for(Account loopAccount : updateAccounts){
                accountIds.add(loopAccount.Id);
            }
            List<OSM_Order_Role__c> updateOrderRoleList = [SELECT Id, OSM_Account__c, OSM_Contact__c, OSM_Delegate__c, OSM_Phone_and_Fax_Locked__c, OSM_Address_Affiliation__c, OSM_Order__c, OSM_Product__c FROM OSM_Order_Role__c WHERE OSM_Account__c IN :accountIds];
            //Call on create logic - copies account/contact details to order role fields
            List<OSM_Order_Role__c> copiedDetailsOR = OSM_OrderRoleTriggerHandler.copyAccountContactDetails(updateOrderRoleList, false, isInsert, null);
            if(copiedDetailsOR.size() > 0){
                OSM_OrderRoleTriggerHandler.runAccountContactCopyOnce = true;
                update copiedDetailsOR;
            }*/
        }
    } 
    
    public static void copyAddressOnPlans(Set<Id> planIds, List<OSM_Address_Affiliation__c> newAddAff){
        system.debug('test enter copyAddressOnPlans');
        
        //Map<Id, OSM_Address_Affiliation__c> addAffMap = new Map<Id, OSM_Address_Affiliation__c>([SELECT OSM_Address_Line_1__c, OSM_Address_Line_2__c, OSM_Type__c, OSM_Plan__c , OSM_Main_Address__c , OSM_City__c, OSM_State__c, OSM_Zip__c, OSM_Country__c from OSM_Address_Affiliation__c where OSM_Plan__c IN: planIds AND OSM_Main_Address__c= true ]);
        system.debug('test planIds ' + planIds);
        List<OSM_Plan__c> updatePlans = [SELECT ID, OSM_Address_1__c, OSM_Address_2__c, OSM_City__c, OSM_State__c, OSM_Zip__c, OSM_Country__c from OSM_Plan__c WHERE ID IN: planIds];
        System.debug('\n\n\n PLANIDSSET'+updatePlans);
        //System.debug('\n\n\n ADDAFFMAPVALS'+addAffMap);
        for(OSM_Plan__c loopPlan : updatePlans){
            for(OSM_Address_Affiliation__c addAff: newAddAff){
                if(loopPlan.Id == addAff.OSM_Plan__c && addAff.OSM_Main_Address__c){
                    loopPlan.OSM_Address_1__c = addAff.OSM_Address_Line_1__c;   
                    loopPlan.OSM_Address_2__c = addAff.OSM_Address_Line_2__c;    
                    loopPlan.OSM_City__c = addAff.OSM_City__c;    
                    loopPlan.OSM_State__c = addAff.OSM_State__c;    
                    loopPlan.OSM_Zip__c = addAff.OSM_Zip__c;    
                    loopPlan.OSM_Country__c = addAff.OSM_Country__c;    
                }
            System.debug('\n\n\n TEMPPLANVALS'+loopPlan);
            }
            
        }
        
        if(!updatePlans.isEmpty()){
            update updatePlans;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Copies Address account's address fields based on address affiliations. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
     */
    public static void copyAddressesOnContacts(Set<Id> accountIds){
        Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
        Map<Id,Customer_Affiliation__c> contactAccountMap = new Map<Id,Customer_Affiliation__c>();  
        for(Customer_Affiliation__c loopCustomerAffilation : [SELECT Id, OSM_Phone_Extension__c, OSM_Role__c, OSM_Contact_1__c, OSM_Account_1__r.BillingStreet, OSM_Account_1__r.BillingCity,
                                                                     OSM_Account_1__r.BillingState, OSM_Account_1__r.BillingPostalCode, OSM_Account_1__r.BillingCountry,
                                                                     OSM_Account_1__r.OSM_Email__c, OSM_Account_1__r.OSM_Main_Phone_Extension__c, OSM_Account_1__r.Fax, OSM_Account_1__r.Phone, OSM_Account_1__r.OSM_Country_Code__c
                                                                FROM Customer_Affiliation__c
                                                                WHERE OSM_Account_1__c IN :accountIds
                                                                    AND (RecordTypeId = :recordTypes.RT_Acct_Cnt_CustAffiliation__c OR RecordTypeId = :recordTypes.RT_Cnt_Acct_CustAffiliation__c)]){
            if(loopCustomerAffilation.OSM_Role__c != null){
                if(loopCustomerAffilation.OSM_Role__c.contains('Primary')){
                    if(loopCustomerAffilation.OSM_Contact_1__c != null){
                        contactAccountMap.put(loopCustomerAffilation.OSM_Contact_1__c, loopCustomerAffilation);
                    }
                }
            }
        }
        
        Contact tempContact;
        List<Contact> updateContacts = new List<Contact>();
        for(Id loopContactId : contactAccountMap.keySet()){
            if(contactAccountMap.containsKey(loopContactId)){
                tempContact = new Contact(Id = loopContactId);
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingStreet != null){
                    tempContact.MailingStreet = contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingStreet;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingCity != null){
                    tempContact.MailingCity = contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingCity;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingState != null){
                    tempContact.MailingState =  contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingState;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingPostalCode != null){
                    tempContact.MailingPostalCode = contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingPostalCode;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingCountry != null){
                    tempContact.MailingCountry = contactAccountMap.get(loopContactId).OSM_Account_1__r.BillingCountry;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.OSM_Email__c != null){
                    tempContact.Email = contactAccountMap.get(loopContactId).OSM_Account_1__r.OSM_Email__c;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.Fax != null){
                    tempContact.Fax = contactAccountMap.get(loopContactId).OSM_Account_1__r.Fax;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.Phone != null){
                    tempContact.Phone = contactAccountMap.get(loopContactId).OSM_Account_1__r.Phone;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.OSM_Main_Phone_Extension__c != null){
                    tempContact.OSM_Phone_Extension__c = contactAccountMap.get(loopContactId).OSM_Account_1__r.OSM_Main_Phone_Extension__c;
                }
                if(contactAccountMap.get(loopContactId).OSM_Account_1__c != null && contactAccountMap.get(loopContactId).OSM_Account_1__r.OSM_Main_Phone_Extension__c != null){
                    tempContact.OSM_Country_Code__c = contactAccountMap.get(loopContactId).OSM_Account_1__r.OSM_Country_Code__c;
                }
            }
            updateContacts.add(tempContact);
        }
        
        if(updateContacts.size() > 0){
            update updateContacts;
        }
    } 
}