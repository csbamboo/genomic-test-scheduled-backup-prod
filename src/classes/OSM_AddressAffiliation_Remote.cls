/*
@author: Patrick Lorilla
@date: 17 September 2014
@description: Remote class for the javascript buttons of the address affilitation object.

*/

global class OSM_AddressAffiliation_Remote{ 

    /*
    @author: Patrick Lorilla
    @date: 17 September 2014
    @param: addAff - Address Affiliation object
    @description: Sets record to main address and updates the address fields of related object records.
    */
    webService static void saveAsMain(OSM_Address_Affiliation__c addAff) {
        Record_Type__c recordType = Record_Type__c.getOrgDefaults();
        //Select all other associated address affiliation of the same account and set the main address to false
        List<OSM_Address_Affiliation__c> addAffList = [SELECT ID, OSM_Main_Address__c from OSM_Address_Affiliation__c  where ID != :addAff.ID AND OSM_Account__C =: addAff.OSM_Account__c ];
        for(OSM_Address_Affiliation__c add: addAffList){
            add.OSM_Main_Address__c = false;    
        }
        //The main address of the principal address affiliation object is set to true.
        addAff.OSM_Main_Address__c  = true;
        
        
        //populate the Billing Address (BillingAddress) of the associated Account with the values of fields on the Address linked in the Address Affiliation record that is now the Main Address.
        OSM_Address__c address = [SELECT OSM_Address_Line_1__c, OSM_City__c, OSM_Country__c, OSM_State__c, OSM_Zip__c from OSM_Address__c  where ID =: addAff.OSM_Address__c];
        //Account acct = [SELECT BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Phone, OSM_Email__c, Fax from Account where ID =:  addAff.OSM_Account__c];
        //line 31 to reduce the number of queries - Paul Wittmeyer 
        Account acct = new Account(Id=addAff.OSM_Account__c);
        acct.BillingStreet = address.OSM_Address_Line_1__c;
        acct.BillingCity = address.OSM_City__c;
        acct.BillingState =  address.OSM_State__c;
        acct.BillingPostalCode = address.OSM_Zip__c;
        acct.BillingCountry = address.OSM_Country__c;
        acct.OSM_Email__c = addAff.OSM_Email__c;
        acct.Fax = addAff.OSM_Fax__c;
        acct.Phone = addAff.OSM_Phone_Number__c;
        
        
        //any associated Contact records to that associated Account will have the Mailing Address (MailingAddress) field populated by the values of fields on the Address linked in  the Address Affiliation record that is now the Main Address.
        //this query cannot be eliminated - Paul Wittmeyer 7/2/2015
        List<Contact> conList1 = [SELECT MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry from Contact where AccountId =: addAff.OSM_Account__c];
        
        for(Contact con: conList1){
            con.MailingStreet = address.OSM_Address_Line_1__c;
            con.MailingCity = address.OSM_City__c;
            con.MailingState = address.OSM_State__c;
            con.MailingPostalCode = address.OSM_Zip__c;
            con.MailingCountry  =  address.OSM_Country__c;
        }
        
        //copy email, fax and phone from address affiliation to related Contact
        List<Contact> conList2  = new List<Contact>();
        if(addAff.RecordTypeId == recordType.HCO_Address_Affiliation__c){
            //conList2 = [SELECT Email, Fax, Phone  from Contact where ID =: addAff.OSM_HCP__c];
            //to reduce queries - Paul Wittmeyer 7/2/2015
            Contact con2 = new Contact(Id = addAff.OSM_HCP__c);
            conList2.add(con2);
        }
        else{
            //conList2 = [SELECT Email, Fax, Phone  from Contact where ID =: addAff.OSM_Contact__c];
            //to reduce queries - Paul Wittmeyer 7/2/2015
            Contact con3 = new Contact(Id = addAff.OSM_Contact__c);
            conList2.add(con3);
        }

        for(Contact con: conList2){
            con.Email = addAff.OSM_Email__c;
            con.Fax = addAff.OSM_Fax__c;
            con.Phone = addAff.OSM_Phone_Number__c;
        
        }
        //perform update with the objects
        performUpdate(conList1,conList2,acct,addAffList,addAff);   
    }
   
    /*
    @author: Patrick Lorilla
    @date: 24 September 2014
    @param: cList - contact list acclist - accountlist, addAffList- address affiliation list addAff - address affiliation record
    @description: update to database
    */
    public static void performUpdate(List<Contact> cList1, List<Contact> cList2, Account accList, List<OSM_Address_Affiliation__c> addAffList, OSM_Address_Affiliation__c addAff){
       try{
            
            update accList;
            update addAffList;
            update addAff;
            update cList1;
            update cList2;
        }
        catch(Exception ex){
            System.debug('ERROR: ' + ex.getMessage());
        }
   }
   
   
}