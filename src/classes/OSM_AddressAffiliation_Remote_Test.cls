/*
@author: Patrick Lorilla
@date: 24 September 2014
@description: Test Address Affiliation Remote Class

*/
@isTest
public class OSM_AddressAffiliation_Remote_Test{
    /*
    @author: Patrick Lorilla
    @date: 24 September 2014
    @description: Test Save As Main

    */
    static testmethod void testSaveAsMainHCO(){
    
        //Prepare recordTypes and trigger switches
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = true;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = false;
        insert triggerSwitch;
        //Prepare account
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        //Prepare contact
        List<Contact> conList = new List<Contact>();
        for(Integer a=0; a<20; a++){
            conList.add(OSM_DataFactory.createContact(a,acct.ID,null));
        
        }
        insert conList;
        //Prepare address
        OSM_Address__c add = OSM_DataFactory.createAddress('Street 1', null, 'City 1', 'United States', 'California', '100');
        insert add;
        
        //prepare hcp
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
      
        //Prepare address affiliation
        OSM_Address_Affiliation__c addAff = OSM_DataFactory.createAddressAffiliation(acct.ID, add.ID, '', false, 'email@email.com', '1234', hcp.ID, null, false, '1234', '', recTypes.HCO_Address_Affiliation__c);
        insert addAff;
        
        OSM_Address_Affiliation__c addAff1 = OSM_DataFactory.createAddressAffiliation(acct.ID, add.ID, '', true, 'email1@email.com', '1234', hcp.ID, null,  false, '1234', '', null);
        insert addAff1;
        
        Test.startTest();
        //Save As Main
        OSM_AddressAffiliation_Remote.saveAsMain(addAff);  
        Test.stopTest();
        System.debug('\n\n\n ASSERT');
        
        acct = [SELECT BillingStreet, OSM_Email__c, Fax , Phone, BillingCity, BillingState, BillingPostalCode, BillingCountry from Account where ID =: acct.ID];
        //Assert correct populated fields
        
        // System.assertEquals(acct.BillingCity,add.OSM_City__c);
        // System.assertEquals(acct.BillingStreet,add.OSM_Address_Line_1__c);
        // System.assertEquals(acct.BillingState,add.OSM_State__c);
        // System.assertEquals(acct.BillingPostalCode,add.OSM_Zip__c);
        // System.assertEquals(acct.OSM_Email__c,addAff.OSM_Email__c);
        // System.assertEquals(acct.Fax,addAff.OSM_Fax__c);
        // System.assertEquals(acct.Phone,addAff.OSM_Phone_Number__c);
        // hcp = [SELECT Email, Fax, Phone from Contact where ID =: hcp.ID];
        // System.assertEquals(hcp.Email,addAff.OSM_Email__c);
        // System.assertEquals(hcp.Fax,addAff.OSM_Fax__c);
        // System.assertEquals(hcp.Phone,addAff.OSM_Phone_Number__c);
        
        conList = [SELECT MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry from Contact where ID !=:hcp.ID];
       
        for(Contact con: conList){
            // System.assertEquals(con.MailingCountry,add.OSM_Country__c);
            // System.assertEquals(con.MailingCity,add.OSM_City__c);
            // System.assertEquals(con.MailingStreet,add.OSM_Address_Line_1__c);
            // System.assertEquals(con.MailingState,add.OSM_State__c);
            // System.assertEquals(con.MailingPostalCode,add.OSM_Zip__c);
        
        }
        
    
    
    }
    
    /*
    @author: Patrick Lorilla
    @date: 24 September 2014
    @description: Test Save As Main

    */
    static testmethod void testSaveAsMainHCP(){
    
        //Prepare recordTypes and trigger switches
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = true;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = false;
        insert triggerSwitch;
        //Prepare account
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        //Prepare contact
        List<Contact> conList = new List<Contact>();
        for(Integer a=0; a<20; a++){
            conList.add(OSM_DataFactory.createContact(a,acct.ID,null));
        
        }
        insert conList;
        //Prepare address
        OSM_Address__c add = OSM_DataFactory.createAddress('Street 1', null, 'City 1', 'United States', 'California', '100');
        insert add;
        
        //prepare hcp
        Contact con1=  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert con1;
      
        //Prepare address affiliation
        OSM_Address_Affiliation__c addAff = OSM_DataFactory.createAddressAffiliation(acct.ID, add.ID, '', false, 'email@email.com', '1234', null, con1.ID, false, '1234', '', recTypes.HCP_Address_Affiliation__c);
        insert addAff;
        
        OSM_Address_Affiliation__c addAff1 = OSM_DataFactory.createAddressAffiliation(acct.ID, add.ID, '', true, 'email1@email.com', '1234', null, con1.ID, false, '1234', '', null);
        insert addAff1;
        
        Test.startTest();
        //Save As Main
        OSM_AddressAffiliation_Remote.saveAsMain(addAff);  
        Test.stopTest();
        System.debug('\n\n\n ASSERT');
        
        acct = [SELECT BillingStreet, OSM_Email__c, Fax , Phone, BillingCity, BillingState, BillingPostalCode, BillingCountry from Account where ID =: acct.ID];
        //Assert correct populated fields
       
        // System.assertEquals(acct.BillingCity,add.OSM_City__c);
        // System.assertEquals(acct.BillingStreet,add.OSM_Address_Line_1__c);
        // System.assertEquals(acct.BillingState,add.OSM_State__c);
        // System.assertEquals(acct.BillingPostalCode,add.OSM_Zip__c);
        // System.assertEquals(acct.OSM_Email__c,addAff.OSM_Email__c);
        // System.assertEquals(acct.Fax,addAff.OSM_Fax__c);
        // System.assertEquals(acct.Phone,addAff.OSM_Phone_Number__c);
        // con1 = [SELECT Email, Fax, Phone from Contact where ID =: con1.ID];
        // System.assertEquals(con1 .Email,addAff.OSM_Email__c);
        // System.assertEquals(con1 .Fax,addAff.OSM_Fax__c);
        // System.assertEquals(con1 .Phone,addAff.OSM_Phone_Number__c);
        
        conList = [SELECT MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry from Contact where ID !=:con1.ID];
       
        for(Contact con: conList){
            // System.assertEquals(con.MailingCountry,add.OSM_Country__c);
            // System.assertEquals(con.MailingCity,add.OSM_City__c);
            // System.assertEquals(con.MailingStreet,add.OSM_Address_Line_1__c);
            // System.assertEquals(con.MailingState,add.OSM_State__c);
            // System.assertEquals(con.MailingPostalCode,add.OSM_Zip__c);
        
        }
        
    
    
    }
    /*
    @author: Patrick Lorilla
    @date: 24 September 2014
    @description: Test Exception

    */
    static testmethod void testException(){
        
        Test.startTest();
        try{
        OSM_AddressAffiliation_Remote.performUpdate(null,null,null,null,null); 
        } 
        catch(Exception ex){
            System.assert(ex.getMessage() != null);
        }
        Test.stopTest();
        //assert that update not succesful.
        
       
    }

}