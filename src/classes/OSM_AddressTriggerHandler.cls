/*

@author: Patrick Lorilla
@date: 23 September 2014
@description: Address Trigger Handler

*/

public class OSM_AddressTriggerHandler{
    /*

        @author: Patrick Lorilla
        @date: 23 September 2014
        @param: newAddress - new Address list, oldAddressMap - Map of the existing address, newAddressMap - new Address Map
        @description: After Update Method

    */
    public static void onAfterUpdate(List<OSM_Address__c> newAddress, Map<ID, OSM_Address__c> oldAddressMap, Map<ID, OSM_Address__c> newAddressMap){
        
        updateAddressAffiliation(newAddressMap, newAddressMap.keySet());  
    }
    /*

        @author: Patrick Lorilla
        @date: 23 September 2014
        @param: newAddressMap- new Address Map, newAddressKeySet- IDs of the new address
        @description: Update address fields of account

    */
    public static void updateAddressAffiliation(Map<ID, OSM_Address__c> newAddressMap, Set<ID> newAddressKeySet){
        System.debug('\n\n\n  update add aff method');
        System.debug('\n\n\n  new address keyset:'+newAddressKeySet);
        Map<ID, OSM_Address_Affiliation__c> addAffMap=  new Map<ID, OSM_Address_Affiliation__c>([SELECT ID, OSM_Account__c, OSM_Address__c from OSM_Address_Affiliation__c where OSM_Address__c IN: newAddressKeySet AND OSM_Main_Address__c= true]);
        Set<ID> accountIDs = new Set<ID>();
        for(OSM_Address_Affiliation__c addAff: addAffMap.values()){
            accountIDs.add(addAff.OSM_Account__c);
        }
        System.debug('\n\n\n  addaffmap:'+addAffMap.values());
        
        //refactored to reduce queries - Paul Wittmeyer 7/3/2015
        List<Account> accountList = new List<Account>();
        for(Id aId : accountIDs){
            Account a = new Account(id = aId);
            accountList.add(a);
        }
        //Map<ID, Account> accountsUpdate = new Map<ID, Account>([SELECT BillingCountry, BillingCity, BillingState, BillingPostalCode, BillingStreet from Account where ID IN:accountIDs]);
        Map<ID, Account> accountsUpdate = new Map<ID, Account>(accountList); 
        System.debug('\n\n\n accounts:'+accountsUpdate);
        for(OSM_Address_Affiliation__c addaffil: addAffMap.values()){
            System.debug('\n\n\n ADD AFFIL ID:'+ addaffil.ID);
            System.debug('\n\n\n ADD AFFIL ACCT ID:'+ addaffil.OSM_Account__c);
            System.debug('\n\n\n ADD AFFIL ADDRESS ID:'+ addaffil.OSM_Address__c);
          if(accountsUpdate.get(addaffil.OSM_Account__c) != null && newAddressMap.get(addaffil.OSM_Address__c) != null){
                accountsUpdate.get(addaffil.OSM_Account__c).BillingCountry =  newAddressMap.get(addaffil.OSM_Address__c ).OSM_Country__c;
                accountsUpdate.get(addaffil.OSM_Account__c).BillingCity=  newAddressMap.get(addaffil.OSM_Address__c ).OSM_City__c;      
                accountsUpdate.get(addaffil.OSM_Account__c).BillingState=  newAddressMap.get(addaffil.OSM_Address__c ).OSM_State__c; 
                accountsUpdate.get(addaffil.OSM_Account__c).BillingPostalCode=  newAddressMap.get(addaffil.OSM_Address__c ).OSM_Zip__c;  
                accountsUpdate.get(addaffil.OSM_Account__c).OSM_Country_Code__c=  newAddressMap.get(addaffil.OSM_Address__c ).OSM_Country_Code__c;
                accountsUpdate.get(addaffil.OSM_Account__c).OSM_Customer_Service_Team__c=  newAddressMap.get(addaffil.OSM_Address__c ).OSM_Customer_Service_Team__c;  
                //accountsUpdate.get(addaffil.OSM_Account__c).BillingStreet=  newAddressMap.get(addaffil.OSM_Address__c ).OSM_Address_Line_1__c; 
                if(newAddressMap.get(addaffil.OSM_Address__c ).OSM_Address_Line_2__c != null){
                    system.debug('test enter add2');
                    accountsUpdate.get(addaffil.OSM_Account__c).BillingStreet=  newAddressMap.get(addaffil.OSM_Address__c ).OSM_Address_Line_1__c + ' , ' + newAddressMap.get(addaffil.OSM_Address__c ).OSM_Address_Line_2__c; 
                }
                else{
                    accountsUpdate.get(addaffil.OSM_Account__c).BillingStreet=  newAddressMap.get(addaffil.OSM_Address__c ).OSM_Address_Line_1__c;
                }
            }                 
        }
        
       
        
        updateAccounts(accountsUpdate.values(), newAddressMap.values()[0]);
        
    }
    /*

        @author: Patrick Lorilla
        @date: 23 September 2014
        @param: accountsUpdate - list of accounts to be updated, address- address record being involved
        @description: Update account in database

    */
    public static void updateAccounts(List<Account> accountsUpdate, OSM_Address__c address){
        try{
            if(accountsUpdate.size() > 0){
            update accountsUpdate;
            }
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
            address.addError(ex.getMessage());
        }
    }


}