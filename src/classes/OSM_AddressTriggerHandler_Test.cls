/*
@author: Patrick Lorilla and David E. Catindoy
@date: 24 September 2014
@description: Test Address Trigger handler
@history: 27 January 2015 - David E. Catindoy (Updated)
*/

@isTest
public class OSM_AddressTriggerHandler_Test{
    /*
    @author: Patrick Lorilla and David E. Catindoy
    @date: 24 September 2014
    @description: Test Update Account Address
    @history: 27 January 2015 - David E. Catindoy (Updated)
    */
    static testmethod void testUpdateAccountAddress(){
        //Prepare triggers and record types
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Address_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        
        //Prepare account
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        
        List<Contact> conList = new List<Contact>();
        for(Integer a=0; a<20; a++){
            conList.add(OSM_DataFactory.createContact(a,acct.ID,null));
        
        }
        insert conList;
        //Prepare addressList;
        List<OSM_Address__c> addList = new List<OSM_Address__c>();
        for(Integer a=0; a<20; a++){
           
            addList.add(OSM_DataFactory.createAddress('Street ', null, 'City ', 'Canada', 'Northwest Territories', '100'));
            
        }
        
        insert addList;
        //Prepare address affiliation list
        List<OSM_Address_Affiliation__c> addAffilList = new List<OSM_Address_Affiliation__c>();
        for(Integer a=0; a<20; a++){
            if(a==10){
                //Main Address
                addAffilList.add(OSM_DataFactory.createAddressAffiliation(acct.ID, addList[a].Id, '', true, 'email1@email.com', '1234', null, conList[a].Id, true, '1234', 'Main', recTypes.HCO_Address_Affiliation__c ));     
            }
            else{
                addAffilList.add(OSM_DataFactory.createAddressAffiliation(acct.ID, addList[a].Id, '', true, 'email1@email.com', '1234', null, conList[a].Id, false, '1234', 'Failure Case Outreach', recTypes.HCO_Address_Affiliation__c ));        
            }
            
        }
        
        insert addAffilList;
        
        Test.startTest();
            //Update addresses
            for(Integer a=0; a<20; a++){
                addList[a].OSM_Address_Line_1__c = 'New Street '+a;
                addList[a].OSM_City__c = 'New City '+a;
                addList[a].OSM_State__c = 'California';
                addList[a].OSM_Country__c = 'United States';
            }
            update addList;
        Test.stopTest();

            for(Integer a=0; a<20; a++){
                System.assertEquals('New Street '+a, addList[a].OSM_Address_Line_1__c);
                System.assertEquals('New City '+a, addList[a].OSM_City__c);
                System.assertEquals('California', addList[a].OSM_State__c);
                System.assertEquals('United States', addList[a].OSM_Country__c);
            }
      }
    
    static testmethod void testException(){
        //Prepare address;
        OSM_Address__c add = OSM_DataFactory.createAddress('Street ', null, 'City ', 'Canada', 'Northwest Territories', '100');
        Test.startTest();
        try{
            OSM_AddressTriggerHandler.updateAccounts(null,add); 
        } 
        catch(Exception ex){
            System.assert(ex.getMessage() != null);
        }
        Test.stopTest();
    }
    
}