/*
 *Author: Michiel Patricia M. Robrigado
 *Date created: November 25, 2014
 *Controller for OSM_Affiliations
 */

public with sharing class OSM_AffiliationsController {

    public List<Customer_Affiliation__c> customerAffiliationAccountList{get;set;}
    public List<Customer_Affiliation__c> customerAffilDispAccountList{get;set;}

    public List<Customer_Affiliation__c> customerAffiliationContactList{get;set;}
    public List<Customer_Affiliation__c> customerAffilDispContactList{get;set;}
    
    public List<AffiliationWrapper> wrapperAccount {get;set;}

    Id accId;
    Account acc, scAccount;
    public Record_Type__c recordTypes;
    public String relatedId {get;set;}
    Id recAccToAccId = Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Acct-Acct Affiliation').getRecordTypeId();
    public String accName {get;set;}
    
    public String sortOrder { get; set; } 
    public String previousSortField {get;set;}
    public String sortField {get;set;}
    
    //Variable Pagination for Account to Account
    private Integer iCurRow = 0;
    private Integer iTableMaxSize = 0;
    private Map<Integer, List<AffiliationWrapper>> accAccPage = new Map<Integer, List<AffiliationWrapper>>();
    public boolean bDisableNext {get; set;}
    public boolean bDisablePrev {get; set;}
    public boolean notPayor{get; set;}
    public String sHeightDiv {get; set;}
    
    //Variable Pagination for Account to Contact
    private Integer iCurRow2 = 0;
    private Integer iTableMaxSize2 = 0;
    private Map<Integer, List<Customer_Affiliation__c>> accConPage = new Map<Integer, List<Customer_Affiliation__c>>();
    public boolean bDisableNext2 {get; set;}
    public boolean bDisablePrev2 {get; set;}
    public String sHeightDiv2 {get; set;}
    
    public String theUrlAccAcc {get;set;}
    public String theUrlAccCon {get;set;}
    public String theUrlCustPay {get;set;}
    public String theUrlStud {get;set;}
    public String newStudBtnVisiblity {get;set;}

    public OSM_AffiliationsController(ApexPages.StandardController controller) {
        //Get Payor account record
        scAccount = (Account)controller.getRecord();

        recordTypes = Record_Type__c.getOrgDefaults();

        customerAffilDispAccountList = new List<Customer_Affiliation__c>();
        customerAffilDispContactList = new List<Customer_Affiliation__c>();

        accId = apexpages.currentpage().getParameters().get('id');
                
        acc = [SELECT Name, RecordTypeId FROM Account WHERE Id =: accId];
        accName = acc.Name;
        
        ID hcoId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        ID hcoUkId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO UK').getRecordTypeId();
        ID hcoDeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO DE').getRecordTypeId();
        
        if(acc.RecordTypeId == recordTypes.Account_Payor_Record_Type__c ){
            notPayor = false;
        }
        else{
            notPayor = true;
        }
        if(acc.RecordTypeId == recordTypes.Account_Partner_Record_Type__c || acc.RecordTypeId == hcoId || acc.RecordTypeId == hcoUkId || acc.RecordTypeId == hcoDeId){
            newStudBtnVisiblity = 'hidden';    
        }
        else{
             newStudBtnVisiblity = 'visible'; 
        }
        wrapperAccount = new List<AffiliationWrapper>();
        
        customerAffiliationAccountList = [SELECT Id, OSM_Account_1__c, RecordTypeId, RecordType.Name, OSM_Study__r.Name, CreatedDate, OSM_View__c, OSM_Account_1_Name__c, OSM_Account_2_Name__c, OSM_Role__c, OSM_Account_1_Record_Type__c, OSM_Account_2_Record_Type__c FROM Customer_Affiliation__c WHERE (OSM_Account_1__c =: accId OR OSM_Account_2__c =: accId) AND ((OSM_Account_1__c != null AND OSM_Account_2__c != null) OR (OSM_Account_1__c != null AND OSM_Study__c != null) ) /*AND (RecordTypeId =: recordTypes.RT_Acct_Acct_CustAffiliation__c OR RecordTypeId =: recordTypes.Payor_Customer_Affiliation_RT_ID__c)*/];

        System.debug('\n\n\n *****custAffilAcct'+customerAffiliationAccountList);
        for(Customer_Affiliation__c cusAff : customerAffiliationAccountList){
            customerAffilDispAccountList.add(cusAff);
            
            String accountName = cusAff.OSM_Account_1__c != accId ? String.valueof(cusAff.OSM_Account_1_Name__c) : cusAff.OSM_Account_2_Name__c;
            String studyName = cusAff.OSM_Account_1__c == accId ? cusAff.OSM_Study__r.Name : '';
            String recName = cusAff.OSM_Account_1__c != accId ? cusAff.OSM_Account_1_Record_Type__c : cusAff.OSM_Account_2_Record_Type__c;
            wrapperAccount.add(new AffiliationWrapper(cusAff.Id, cusAff.OSM_Role__c, accountName, recName, studyName, cusAff.CreatedDate));
            
        }
        
        //customerAffiliationContactList = [SELECT Id, CreatedDate, OSM_View__c, OSM_Contact_1_Name__c, OSM_Contact_2_Name__c, OSM_Role__c, OSM_Contact_1_Record_Type__c, OSM_Contact_2_Record_Type__c FROM Customer_Affiliation__c WHERE (OSM_Account_1__c =: accId OR OSM_Account_2__c =: accId) AND (RecordTypeId =: recordTypes.RT_Acct_Cnt_CustAffiliation__c )];
        //updated the record type to RT_Cnt_Acct_CustAffiliation__c
        customerAffiliationContactList = [SELECT Id, CreatedDate, OSM_View__c, OSM_Contact_1_Name__c, OSM_Contact_2_Name__c, OSM_Role__c, OSM_Contact_1_Record_Type__c, OSM_Contact_2_Record_Type__c FROM Customer_Affiliation__c WHERE (OSM_Account_1__c =: accId OR OSM_Account_2__c =: accId) AND (RecordTypeId =: recordTypes.RT_Cnt_Acct_CustAffiliation__c )];
        system.debug('testlist customerAffiliationContactList ' + customerAffiliationContactList);
        for(Customer_Affiliation__c cusAff : customerAffiliationContactList){
            customerAffilDispContactList.add(cusAff);
        }
        
        
        Schema.DescribeSObjectResult r = Customer_Affiliation__c.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix();
        theUrlAccAcc = '/' +keyPrefix+ '/e?RecordType=' + recordTypes.RT_Acct_Acct_CustAffiliation__c + '&retURL=/' + accId + '&CF' + recordTypes.CF_Account1_CustAffiliation__c + '=' + acc.Name + '&CF' + recordTypes.CF_Account1_CustAffiliation__c + '_lkid=' + accId;
        theUrlAccCon = '/' +keyPrefix+ '/e?RecordType=' + recordTypes.RT_Cnt_Acct_CustAffiliation__c+ '&retURL=/' + accId + '&CF' + recordTypes.CF_Account1_CustAffiliation__c + '=' + acc.Name + '&CF' + recordTypes.CF_Account1_CustAffiliation__c + '_lkid=' + accId;
        theUrlCustPay = '/' +keyPrefix+ '/e?RecordType='+recordTypes.Payor_Customer_Affiliation_RT_ID__c+'&CF'+recordTypes.CF_Account1_CustAffiliation__c+'_lkid='+accId +'&CF'+recordTypes.CF_Account1_CustAffiliation__c+'='+accName;
        theUrlStud= '/' +keyPrefix+ '/e?RecordType='+recordTypes.Study_Customer_Affiliation_RT_ID__c+'&CF'+recordTypes.CF_Account1_CustAffiliation__c+'_lkid='+accId +'&CF'+recordTypes.CF_Account1_CustAffiliation__c+'='+accName;
        formPages();
        formPages2();
        
         
    }

    /*public pageReference newAccAff(){
        PageReference pr = new PageReference('/a00/e?ent=01IJ00000005w7a&RecordType=' + recordTypes.RT_Acct_Acct_CustAffiliation__c + '&retURL=/' + accId + '&CF' + recordTypes.CF_Account1_CustAffiliation__c + '=' + acc.Name + '&CF' + recordTypes.CF_Account1_CustAffiliation__c + '_lkid=' + accId);
        return pr;
    }*/

    /*public pageReference newAccConAff(){
        PageReference pr = new PageReference('/a00/e?ent=01IJ00000005w7a&RecordType=' + recordTypes.RT_Acct_Cnt_CustAffiliation__c + '&retURL=/' + accId + '&CF' + recordTypes.CF_Account1_CustAffiliation__c + '=' + acc.Name + '&CF' + recordTypes.CF_Account1_CustAffiliation__c + '_lkid=' + accId);
        return pr;
    }*/

    public pageReference editAffil(){
        Id relatedId = ApexPages.currentPage().getParameters().get('relatedId');
        PageReference pageRef = new PageReference('/' + relatedId + '/e?retURL=' + scAccount.Id);
        
        return pageRef;
    }
    public pageReference deleteAffil(){
        Id relatedId = ApexPages.currentPage().getParameters().get('relatedId');
        List<Customer_Affiliation__c > deleteList = [SELECT Id FROM Customer_Affiliation__c  WHERE Id =: relatedId];
        
        delete deleteList;
        System.debug('\n\n\n ACCIDVAL:'+accId);
        PageReference pageRef = new PageReference('/' + accId+'?inline=0');
        
        return pageRef;
    }
    
    /*
    *This is the Pagination Section
    *
    *
    *
    */
    public list<AffiliationWrapper> getAccToAccList(){
        if(accAccPage.size()>0){
            if(accAccPage.containsKey(iCurRow)){
                return accAccPage.get(iCurRow);
            }
        }
        
        return new List<AffiliationWrapper>();
    }
    public list<Customer_Affiliation__c> getAccToConList(){
        if(accConPage.size()>0){
            if(accConPage.containsKey(iCurRow2)){
                return accConPage.get(iCurRow2);
            }
        }
        
        return new List<Customer_Affiliation__c>();
    }
  
    private void setDisableButtons(){
        //Disabling Buttons by size    
        Integer totalPage = (iTableMaxSize / 5);
        System.debug('**************'+getMinRecords()+' '+getMaxRecords()+' '+iCurRow+' '+totalPage);
        if(iCurRow > 0 && iTableMaxSize > 0){
            bDisablePrev=true;
        }else{
            bDisablePrev=false;
        }
        if(iCurRow < totalPage && (getMaxRecords()<iTableMaxSize)){
            bDisableNext=true;
        }else{
            bDisableNext=false;
        } 
        if(getMaxRecords() == 0){
            sHeightDiv = '150px';
        }else if((getMaxRecords()-getMinRecords()) > 5){
            sHeightDiv = '0px';
        }else{
            sHeightDiv = '150px';
        }
    }
    
    private void setDisableButtons2(){
        //Disabling Buttons by size    
        Integer totalPage = (iTableMaxSize2 / 5);

        if(iCurRow2 > 0 && iTableMaxSize2 > 0){
            bDisablePrev2=true;
        }else{
            bDisablePrev2=false;
        }
        if(iCurRow2 < totalPage && (getMaxRecords2()<iTableMaxSize2)){
            bDisableNext2=true;
        }else{
            bDisableNext2=false;
        } 
        if(getMaxRecords2() == 0){
            sHeightDiv2 = '150px';
        }else if((getMaxRecords2()-getMinRecords2()) > 5){
            sHeightDiv2 = '0px';
        }else{
            sHeightDiv2 = '150px';
        }
        system.debug('getMaxRecords2()' + getMaxRecords2());
        system.debug('getMinRecords2()' + getMinRecords2());
    }
    
    public Integer getMinRecords(){
        Integer nCount = 1;
        if(iCurRow > 0){
            nCount = (iCurRow * 5 + 1);
        }

            if(wrapperAccount.size() == 0){
                nCount = 0;
            }

        return nCount;
    }//end getStartRow() 
    
    public Integer getMinRecords2(){
        Integer nCount = 1;
        if(iCurRow2 > 0){
            nCount = (iCurRow2 * 5 + 1);
        }

            if(customerAffilDispContactList.size() == 0){
                nCount = 0;
            }

        return nCount;
    }//end getStartRow() 
    
    public Integer getMaxRecords(){
        Integer nCount = 5;
        Integer nMRow = iTableMaxSize / 5;
        if(nMRow == 0){
            nCount = iTableMaxSize;
        }else{
            nCount = ((iCurRow+1) * 5);
            if(nMRow == iCurRow){
                nCount = (iTableMaxSize - (nMRow * 5) + (iCurRow * 5));
            }
        }
        return nCount;
    }//end getTotalRows()
    
    public Integer getMaxRecords2(){
        Integer nCount = 5;
        Integer nMRow = iTableMaxSize2 / 5;
        if(nMRow == 0){
            nCount = iTableMaxSize2;
        }else{
            nCount = ((iCurRow2+1) * 5);
            if(nMRow == iCurRow2){
                nCount = (iTableMaxSize2 - (nMRow * 5) + (iCurRow2 * 5));
            }
        }
        return nCount;
    }//end getTotalRows()
    
    public Integer getTotalRecords(){
        return iTableMaxSize;
    }//end getTotalRows()
    
    public Integer getTotalRecords2(){
        return iTableMaxSize2;
    }//end getTotalRows()
    
    public void gotoPrevious(){
        iCurRow--;
        setDisableButtons();
         
    }//end gotoPrevious()
    public void gotoPrevious2(){
        iCurRow2--;
        setDisableButtons2();
         
    }//end gotoPrevious()
    
    public void gotoNext(){
        iCurRow++;
        setDisableButtons();
         
    }//end gotoNext()
    
    public void gotoNext2(){
        iCurRow2++;
        setDisableButtons2();
         
    }//end gotoNext()
    
    public void formPages(){
        accAccPage = new Map<Integer, List<AffiliationWrapper>>();
        iTableMaxSize = wrapperAccount.size();
        if(accAccPage  != null && wrapperAccount.size()>0){
            Integer oUnitCount = 0;
            List<AffiliationWrapper> lTemp = new List<AffiliationWrapper>();
            for(AffiliationWrapper oLM : wrapperAccount){
                if(oUnitCount < 5){
                    lTemp.add(oLM);
                }else{
                    accAccPage.put(iCurRow,lTemp);
                    iCurRow++;
                    oUnitCount = 0;
                    lTemp = new List<AffiliationWrapper>();
                    lTemp.add(oLM);
                }
                oUnitCount++;
            }
            accAccPage.put(iCurRow,lTemp);
            iCurRow = 0;
        }
        setDisableButtons();
    }
    
    public void formPages2(){
        accConPage = new Map<Integer, List<Customer_Affiliation__c>>();
        iTableMaxSize2 = customerAffilDispContactList.size();
        if(accConPage  != null && customerAffilDispContactList.size()>0){
            Integer oUnitCount = 0;
            List<Customer_Affiliation__c> lTemp = new List<Customer_Affiliation__c>();
            for(Customer_Affiliation__c oLM : customerAffilDispContactList){
                if(oUnitCount < 5){
                    lTemp.add(oLM);
                }else{
                    accConPage.put(iCurRow2,lTemp);
                    iCurRow2++;
                    oUnitCount = 0;
                    lTemp = new List<Customer_Affiliation__c>();
                    lTemp.add(oLM);
                }
                oUnitCount++;
            }
            accConPage.put(iCurRow2,lTemp);
            iCurRow2 = 0;
        }
        setDisableButtons2();
    }
    
    /*
    *SORTING
    */
    
    public void doSort() {
        String order = 'asc';
        if(previousSortField == this.sortField) {
            order = 'desc';
            this.previousSortField = null;
        }
        else {
            this.previousSortField = this.sortField;
        }
        this.sortOrder = order;
        if (this.customerAffiliationAccountList.size() > 0){
            sortCustomerList(wrapperAccount, order, this.sortField);
        }
    }
    
    public void sortCustomerList(List<AffiliationWrapper> list2sort, String orderSort, String sortField) {
                
                //String addedField = orderSort == 'OSM_Account_1_Name__c' ? 'OSM_Account_2_Name__c' : 'OSM_Account_2_Record_Type__c';
                //Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
                //String rcId = recordTypes.RT_Acct_Acct_CustAffiliation__c;
               // String SOQL = 'SELECT Id, CreatedDate, OSM_View__c, OSM_Account_1_Name__c, OSM_Account_2_Name__c, OSM_Role__c, OSM_Account_1_Record_Type__c, OSM_Account_2_Record_Type__c '; 
                //SOQL += 'FROM Customer_Affiliation__c ';
                //SOQL += 'WHERE (OSM_Account_1__c =: accId OR OSM_Account_2__c =: accId) AND RecordTypeId =: rcId ';
                //SOQL += 'Order By ' + sortField + ' ' + orderSort + ', ' + addedField + ' ' + orderSort;
        
                //customerAffilDispAccountList = Database.Query(SOQL);
                Map<String, AffiliationWrapper> fieldMap = new Map<String, AffiliationWrapper>();
                for(AffiliationWrapper tempWrap : list2sort){
                    fieldMap.put(tempWrap.recAccount, tempWrap);
                }
                
                List<String> aList = new List<String>();
                aList.addAll(fieldMap.keySet());
                if(orderSort == 'asc'){
                    aList.sort();
                } else {
                    aList.sort();
                    List<String> finalList = new List<String>();
                    for(Integer i = aList.size()-1; i>=0;i--){
                        finalList.add(aList.get(i));
                    }
                    aList = new List<String>();
                    aList = finalList;
                }
                wrapperAccount = new List<AffiliationWrapper>();
                for(String a : aList){
                    wrapperAccount.add(fieldMap.get(a));
                }
                
                
                formPages();
    }
    
    public class AffiliationWrapper{
        public String recId {get;set;}
        public String recRole {get;set;}
        public String recAccount {get;set;}
        public String recRecordType {get;set;}
        public String recStudy {get;set;}
        public DateTime createDate {get;set;}
        
        public AffiliationWrapper(String rId, String rRole, String rAccount, String rRecordType, String rStudy, DateTime cDate){
            recId = rId;
            recRole = rRole;
            recAccount = rAccount;
            recRecordType = rRecordType;
            recStudy = rStudy;
            createDate = cDate;
        }
    }
    
}