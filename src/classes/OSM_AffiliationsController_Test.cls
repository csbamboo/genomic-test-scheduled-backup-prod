/*
 *Author: Michiel Patricia M. Robrigado
 *Date created: December 2, 2014
 *Test for OSM_AffiliationsController 
 History
        <date>                <author>                <description>
        24-JUL-2015           Mark Cagurong            Added additional unit tests
 */
@isTest//(seeAllData=true)
private class OSM_AffiliationsController_Test{

    private static final String TEST_ACCOUNT_NAME = 'ACT1234';
    private static final String TEST_CONTACT_FIRSTNAME = 'FName123';
    private static final String TEST_CONTACT_LASTNAME = 'LASTNAME123';

    public static testMethod void OSM_AffiliationsControllerTestAccountToAccount() {   
        
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        id conId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();

        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount('testName');
        insert acc;
        
        //insert the details from the data builder to account
        Account acc1 = new Account();
        acc1 = OSM_DataFactory.createAccount('testName');
        insert acc1;
        
        //insert the details from the data builder to contact
        Contact con = new Contact();
        con = OSM_DataFactory.createContact('testfName', 'testlName', conId);
        insert con;
        
        //insert the details from the data builder to rebate detail
        Customer_Affiliation__c newCustomerAffiliation = OSM_DataFactory.createAccountContactCustomerAffiliation(acc.Id, con.Id, recordTypeIds.RT_Acct_Acct_CustAffiliation__c);
        newCustomerAffiliation.OSM_Account_2__c = acc1.id;
        insert newCustomerAffiliation;
        
        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController extension = new OSM_AffiliationsController(sc);
        extension.editAffil();
        extension.deleteAffil();
        extension.getAccToAccList();
        extension.getAccToConList();
        extension.getMinRecords();
        extension.getMaxRecords();
        extension.getTotalRecords();
        extension.getTotalRecords2();
        extension.gotoPrevious();
        extension.gotoPrevious2();
        extension.gotoNext();
        extension.gotoNext2();
        extension.doSort();
        //extension.sortCustomerList();
        //extension.AffiliationWrapper();
    }
    public static testMethod void OSM_AffiliationsControllerTestAccountToContact() {   
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        id conId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount('testName');
        insert acc;
        
        //insert the details from the data builder to account
        Account acc1 = new Account();
        acc1 = OSM_DataFactory.createAccount('testName');
        insert acc1;
        
        //insert the details from the data builder to contact
        Contact con = new Contact();
        con = OSM_DataFactory.createContact('testfName', 'testlName', conId);
        insert con;
        
        //insert the details from the data builder to rebate detail
        Customer_Affiliation__c newCustomerAffiliation = OSM_DataFactory.createAccountContactCustomerAffiliation(acc.Id, con.Id, recordTypeIds.RT_Acct_Cnt_CustAffiliation__c);
        newCustomerAffiliation.OSM_Account_2__c = acc1.id;
        insert newCustomerAffiliation;
        
        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController extension = new OSM_AffiliationsController(sc);
        extension.editAffil();
        extension.deleteAffil();
        extension.getAccToAccList();
        extension.getAccToConList();
        extension.getMinRecords();
        extension.getMaxRecords();
        extension.getTotalRecords();
        extension.getTotalRecords2();
        extension.gotoPrevious();
        extension.gotoPrevious2();
        extension.gotoNext();
        extension.gotoNext2();
        extension.doSort();
        //extension.sortCustomerList();
        //extension.AffiliationWrapper();
    }


    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to retrieve and verify the 2nd contact list affiliates
        History
            <date>                <author>                <description>
            24-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    public static testMethod void getAccToConList_AffiliationsControllerPagedFirstSet() {   
        Record_Type__c recordTypes = OSM_DataFactory.recordLists();
        insert recordTypes;

        recordTypes = Record_Type__c.getOrgDefaults();
        
        id conId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();        
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME);
        insert acc;        
        
        List<Contact> contacts = new List<Contact>();
        for(Integer ctr=0; ctr<10; ctr++){
            //insert the details from the data builder to contact
            Contact con = new Contact();
            con = OSM_DataFactory.createContact(TEST_CONTACT_FIRSTNAME, TEST_CONTACT_LASTNAME, conId);
            con.AccountId = acc.Id;
            //inser con;

            contacts.add(con);
        }

        insert contacts;
        
        List<Customer_Affiliation__c> contactAffiliations = new List<Customer_Affiliation__c>();
        for(Contact ct: contacts){
            Customer_Affiliation__c ctAffliation = OSM_DataFactory.createAccountContactCustomerAffiliation(acc.Id, ct.Id, recordTypes.RT_Cnt_Acct_CustAffiliation__c);
            ctAffliation.OSM_Account_1__c = acc.id;

            contactAffiliations.add(ctAffliation);
        }

        insert contactAffiliations;
        
        Test.startTest();

        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController controller = new OSM_AffiliationsController(sc);

        List<Customer_Affiliation__c> contactList = controller.getAccToConList();

        Test.stopTest();

        // We have 10 result sets, but on the paged list we only display 5
        System.assertEquals(5, contactList.size());        
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering test for this public property
        History
            <date>                <author>                <description>
            24-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    public static testMethod void relatedId_EnsureCoverage() {   
        Record_Type__c recordTypes = OSM_DataFactory.recordLists();
        insert recordTypes;

        recordTypes = Record_Type__c.getOrgDefaults();
        
        id conId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();        
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME);
        insert acc;        

        Test.startTest();

        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController controller = new OSM_AffiliationsController(sc);

        System.assertEquals(null, controller.relatedId);
        

        Test.stopTest();

    }


    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering test for disable flag checks on the account affiliates
        History
            <date>                <author>                <description>
            24-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
     public static testMethod void getAccToAccList_NavigateForwardAndBack() {   
        Record_Type__c recordTypes = OSM_DataFactory.recordLists();
        insert recordTypes;

        recordTypes = Record_Type__c.getOrgDefaults();
        
        id conId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();        
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME);
        insert acc;        
        
        List<Contact> contacts = new List<Contact>();
        for(Integer ctr=0; ctr<10; ctr++){
            //insert the details from the data builder to contact
            Contact con = new Contact();
            con = OSM_DataFactory.createContact(TEST_CONTACT_FIRSTNAME, TEST_CONTACT_LASTNAME, conId);
            con.AccountId = acc.Id;
            //inser con;

            contacts.add(con);
        }

        insert contacts;
        
        List<Customer_Affiliation__c> contactAffiliations = new List<Customer_Affiliation__c>();
        for(Contact ct: contacts){
            Customer_Affiliation__c ctAffliation = OSM_DataFactory.createAccountContactCustomerAffiliation(acc.Id, ct.Id, recordTypes.RT_Acct_Acct_CustAffiliation__c);
            ctAffliation.OSM_Account_1__c = acc.id;
            ctAffliation.OSM_Account_2__c = acc.id;

            contactAffiliations.add(ctAffliation);
        }

        insert contactAffiliations;       

    
        Test.startTest();

        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController controller = new OSM_AffiliationsController(sc);

        controller.gotoNext();
        controller.gotoPrevious();

        List<OSM_AffiliationsController.AffiliationWrapper> acctList = controller.getAccToAccList(); 

        Test.stopTest();

        // We have 10 result sets, navigate forward and back - now at the first page, only display 5,
        System.assertEquals(5, acctList.size());        
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering test for the contact affilites disable mode
        History
            <date>                <author>                <description>
            24-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    public static testMethod void getAccToConList_NavigateForwardAndBack() {   
        Record_Type__c recordTypes = OSM_DataFactory.recordLists();
        insert recordTypes;

        recordTypes = Record_Type__c.getOrgDefaults();
        
        id conId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();        
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME);
        insert acc;        
        
        List<Contact> contacts = new List<Contact>();
        for(Integer ctr=0; ctr<10; ctr++){
            //insert the details from the data builder to contact
            Contact con = new Contact();
            con = OSM_DataFactory.createContact(TEST_CONTACT_FIRSTNAME, TEST_CONTACT_LASTNAME, conId);
            con.AccountId = acc.Id;
            //inser con;

            contacts.add(con);
        }

        insert contacts;
        
        List<Customer_Affiliation__c> contactAffiliations = new List<Customer_Affiliation__c>();
        for(Contact ct: contacts){
            Customer_Affiliation__c ctAffliation = OSM_DataFactory.createAccountContactCustomerAffiliation(acc.Id, ct.Id, recordTypes.RT_Cnt_Acct_CustAffiliation__c);
            ctAffliation.OSM_Account_1__c = acc.id;            

            contactAffiliations.add(ctAffliation);
        }

        insert contactAffiliations;

    
        Test.startTest();

        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController controller = new OSM_AffiliationsController(sc);

        controller.gotoNext2();
        controller.gotoPrevious2();

        List<Customer_Affiliation__c> contactList = controller.getAccToConList(); 

        Test.stopTest();

        // We have 10 result sets, navigate forward and back - now at the first page, only display 5,
        System.assertEquals(5, contactList.size());        
    }


    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering test for: else if((getMaxRecords()-getMinRecords()) > 5){
        History
            <date>                <author>                <description>
            24-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/    
     public static testMethod void getAffiliateList_MInMax() {   
        Record_Type__c recordTypes = OSM_DataFactory.recordLists();
        insert recordTypes;

        recordTypes = Record_Type__c.getOrgDefaults();
        
        id conId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();        
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME);
        insert acc;        
        
        List<Contact> contacts = new List<Contact>();
        // account affiliates
        for(Integer ctr=0; ctr<24; ctr++){
            //insert the details from the data builder to contact
            Contact con = new Contact();
            con = OSM_DataFactory.createContact(TEST_CONTACT_FIRSTNAME, TEST_CONTACT_LASTNAME, conId);
            con.AccountId = acc.Id;
            //inser con;

            contacts.add(con);
        }
        insert contacts;        
        
        List<Customer_Affiliation__c> affiliations = new List<Customer_Affiliation__c>();
        for(Contact ct: contacts){
            Customer_Affiliation__c ctAffliation = OSM_DataFactory.createAccountContactCustomerAffiliation(acc.Id, ct.Id, recordTypes.RT_Acct_Acct_CustAffiliation__c);
            ctAffliation.OSM_Account_1__c = acc.id;
            ctAffliation.OSM_Account_2__c = acc.id;

            affiliations.add(ctAffliation);
        }

        insert affiliations;       

        contacts.clear();
        affiliations.clear();

        // contact  affiliates
        for(Integer ctr=0; ctr<24; ctr++){
            //insert the details from the data builder to contact
            Contact con = new Contact();
            con = OSM_DataFactory.createContact(TEST_CONTACT_FIRSTNAME, TEST_CONTACT_LASTNAME, conId);
            con.AccountId = acc.Id;
            //inser con;

            contacts.add(con);
        }

        insert contacts;        

        for(Contact ct: contacts){
            Customer_Affiliation__c ctAffliation = OSM_DataFactory.createAccountContactCustomerAffiliation(acc.Id, ct.Id, recordTypes.RT_Cnt_Acct_CustAffiliation__c);
            ctAffliation.OSM_Account_1__c = acc.id;
            ctAffliation.OSM_Account_2__c = acc.id;

            affiliations.add(ctAffliation);
        }

        insert affiliations;
    
        Test.startTest();

        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController controller = new OSM_AffiliationsController(sc);

        controller.gotoNext();
        controller.gotoPrevious();
        controller.gotoNext();

        controller.gotoNext2();
        controller.gotoPrevious2();
        controller.gotoNext2();        

        Test.stopTest();

        // We have 10 result sets, navigate forward and back - now at the second page, only display 5,
        System.assertEquals(6, controller.getMinRecords());
        System.assertEquals(10, controller.getMaxRecords());
        System.assertEquals(6, controller.getMinRecords2());
        System.assertEquals(10, controller.getMaxRecords2());
    }
    

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering test for empty affiliate return value
        History
            <date>                <author>                <description>
            24-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    public static testMethod void getAccToAccList_EmptyAffiliate() {   
        Record_Type__c recordTypes = OSM_DataFactory.recordLists();
        insert recordTypes;

        recordTypes = Record_Type__c.getOrgDefaults();
        
        id conId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();        
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME);
        insert acc;
        
        Test.startTest();

        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController controller = new OSM_AffiliationsController(sc);

        List<OSM_AffiliationsController.AffiliationWrapper> accToConList = controller.getAccToAccList();

        Test.stopTest();

        // We have 10 result sets, but on the paged list we only display 5
        System.assertEquals(0, accToConList.size());        
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering test for account type Payor
        History
            <date>                <author>                <description>
            24-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    public static testMethod void notPayor_AccountRecordTypePayor() {   
        Record_Type__c recordTypes = OSM_DataFactory.recordLists();
        insert recordTypes;

        recordTypes = Record_Type__c.getOrgDefaults();
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME);
        acc.RecordTypeId = recordTypes.Account_Payor_Record_Type__c;
        insert acc;
        
        Test.startTest();

        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController controller = new OSM_AffiliationsController(sc);

        Test.stopTest();

        System.assertEquals(false, controller.notPayor);
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering test for Account type partner
        History
            <date>                <author>                <description>
            24-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    public static testMethod void newStudBtnVisiblity_AccountRecordTypePartner() {   
        Record_Type__c recordTypes = OSM_DataFactory.recordLists();
        insert recordTypes;

        recordTypes = Record_Type__c.getOrgDefaults();
        
        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME);
        acc.RecordTypeId = recordTypes.Account_Partner_Record_Type__c;
        insert acc;
        
        Test.startTest();

        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_AffiliationsController controller = new OSM_AffiliationsController(sc);

        Test.stopTest();

        System.assertEquals('hidden', controller.newStudBtnVisiblity);
    }

}