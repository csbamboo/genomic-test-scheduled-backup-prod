/*
    @author: patrick lorilla
    @description: Apptus Agreement Trigger Handler
    @date: 27-APR-2015

*/
public class OSM_ApptusAgreementTriggerHandler{
    /*
    @author: patrick lorilla
    @description: Apptus Agreement Trigger Handler
    @param: List and map of agreement records both old and new 
    @date: 27-APR-2015
    */
    
    public static boolean populateAgreementIDPayorRunOnce = false;
    public static boolean checkExistingAgreementsRunOnce = false;
    public static void onAfterInsert(List<Apttus__APTS_Agreement__c> agreementList, Map<Id, Apttus__APTS_Agreement__c> agreementMapOld){
        List<Apttus__APTS_Agreement__c> filteredAgList = new List<Apttus__APTS_Agreement__c>();
        for(Apttus__APTS_Agreement__c apts: agreementList){
            if(apts.Apttus__Account__c != null){
                filteredAgList.add(apts);    
            }
        }
        
        if(!filteredAgList.isEmpty()){
            if(!populateAgreementIDPayorRunOnce){
                populateAgreementIDPayorRunOnce = true;
                //populateAgreementIDPayor(agreementList); 
                setAgreementIDPayor(filteredAgList);
            } 
        }   
    }
       /*
    @author: patrick lorilla
    @description: Apptus Agreement Trigger Handler
    @param: List and map of agreement records both old and new
    @date: 27-APR-2015
    */
    public static void onAfterUpdate(List<Apttus__APTS_Agreement__c> agreementList, Map<Id, Apttus__APTS_Agreement__c> agreementMapOld){
        List<Apttus__APTS_Agreement__c> filteredAgList = new List<Apttus__APTS_Agreement__c>();
        for(Apttus__APTS_Agreement__c apts: agreementList){
            if(apts.Apttus__Account__c != null){
                filteredAgList.add(apts);    
            }
        }
       
        if(!filteredAgList.isEmpty()){
            if(!populateAgreementIDPayorRunOnce){
                populateAgreementIDPayorRunOnce = true;
                //populateAgreementIDPayor(agreementList);
                //checkExistingAgreements(agreementList);
                setAgreementIDPayor(filteredAgList);
            }
        }    
     
        
    }
        /*
    @author: patrick lorilla
    @description: Apptus Agreement Trigger Handler
    @param: List and map of agreement records both old and new
    @date: 27-APR-2015
    */
    public static void onAfterDelete(Map<Id, Apttus__APTS_Agreement__c> agreementMapOld){
        
         List<Apttus__APTS_Agreement__c> filteredAgList = new List<Apttus__APTS_Agreement__c>();
        for(Apttus__APTS_Agreement__c apts: agreementMapOld.values()){
            if(apts.Apttus__Account__c != null){
                filteredAgList.add(apts);    
            }
        }
       
        if(filteredAgList != null && !filteredAgList.isEmpty()){
            if(!populateAgreementIDPayorRunOnce){
                populateAgreementIDPayorRunOnce = true;
                //populateAgreementIDPayor(agreementList);
                //checkExistingAgreements(agreementList);
                setAgreementIDPayor(filteredAgList);
            }
        }    
     
        
    }
     /*
    @author: patrick lorilla
    @description: Apptus Agreement Trigger Handler
    @param: List and map of agreement records both old and new
    @date: 26-MAY-2015
    */
    public static void onBeforeUpdate(List<Apttus__APTS_Agreement__c> agreementList, Map<Id, Apttus__APTS_Agreement__c> agreementMapOld){
       
        
    }
    /*
    public static void checkExistingAgreements( List<Apttus__APTS_Agreement__c> agreementList){
        Set<ID> acctIds = new Set<ID>();     
        Set<ID> agIds = new Set<Id>();
        for(Apttus__APTS_Agreement__c agl: agreementList){
            acctIds.add(agl.Apttus__Account__c);    
            Datetime today = Datetime.now();
            Date todayDate = Date.parse(today.format('MM/dd/yyyy'));
            boolean goodDate = (todayDate >= agl.GHI_CLM_Effective_Date__c && todayDate <= agl.Apttus__Contract_End_Date__c);
            if(!goodDate){
                agIds.add(agl.Id);    
            }
            
        } 
        if(!agIds.isEmpty()){
            System.debug('\n\n\n agIDSvals' + agIds);
            Map<ID, Account> acctMap = new Map<Id,Account>([SELECT GHI_CLM_Agreement_ID__c from Account where ID IN: acctIds]);
            List<Apttus__APTS_Agreement__c> agreementListExst = [SELECT GHI_CLM_Effective_Date__c, Apttus__Contract_End_Date__c, Apttus__Account__c from Apttus__APTS_Agreement__c where Apttus__Account__c IN: acctIds AND ID NOT IN: agIds];
            System.debug('\n\n\n agreementListExstVals' + agreementListExst);
            for(Integer a=0; a<agreementListExst.size(); a++){
                Integer ctr =0;
                
                for(Integer b=(agreementListExst.size()-1); b >= 0; b--){
                    Datetime today = Datetime.now();
                    Date todayDate = Date.parse(today.format('MM/dd/yyyy'));
                    if(agreementListExst[a].Apttus__Account__c == agreementListExst[b].Apttus__Account__c){
                         boolean goodDate1 = (todayDate >= agreementListExst[a].GHI_CLM_Effective_Date__c && todayDate <= agreementListExst[a].Apttus__Contract_End_Date__c);
                         boolean goodDate2 = (todayDate >= agreementListExst[b].GHI_CLM_Effective_Date__c && todayDate <= agreementListExst[b].Apttus__Contract_End_Date__c);
                         if(goodDate1 && goodDate2){
                            ctr++;
                         }
                    }
                }
                System.debug('\n\n\n agIDval' + agreementListExst[a]);
                System.debug('\n\n\n agctr' + ctr);
                if(ctr==0){
                    acctMap.get(agreementListExst[a].Apttus__Account__c).GHI_CLM_Agreement_ID__c =  agreementListExst[a].Id;    
                }
                else if (ctr >0){
                    acctMap.get(agreementListExst[a].Apttus__Account__c).GHI_CLM_Agreement_ID__c =  null;             
                }
                
                
            }
            
            
            try{
                update acctMap.values();
            }
            catch(Exception ex){
                agreementList[0].addError(ex.getMessage());    
            }
        }
        
    } */
    /*
    @author: patrick lorilla
    @description: Apptus Agreement Trigger Handler
    @param: List and map of agreement records
    @date: 27-APR-2015
    
    public static void populateAgreementIDPayor( List<Apttus__APTS_Agreement__c> agreementList){
        Set<ID> acctIds = new Set<ID>();
        Set<ID> agIds = new Set<Id>();
       
        for(Apttus__APTS_Agreement__c agl: agreementList){
            acctIds.add(agl.Apttus__Account__c);
            if(agl.Id != null){
                agIds.add(agl.Id);
            }
        }
        //Gather payor accounts
        Map<ID, Account> acctMap = new Map<Id,Account>([SELECT GHI_CLM_Agreement_ID__c from Account where ID IN: acctIds]);
        List<Apttus__APTS_Agreement__c> agreementListExst = [SELECT GHI_CLM_Effective_Date__c, Apttus__Contract_End_Date__c, Apttus__Account__c from Apttus__APTS_Agreement__c where Apttus__Account__c IN: acctIds AND ID NOT IN: agIds];
        //Loop through the list and map
        for(Apttus__APTS_Agreement__c agl: agreementList){
            if(acctMap.containsKey(agl.Apttus__Account__c)){
                Datetime today = Datetime.now();
                Date todayDate = Date.parse(today.format('MM/dd/yyyy'));
                //If today's date fits into the date range (start date & end date of related agreement record) then stamp the ID to the payor's agreement ID field. Otherwise nullify it.
                if(agl.GHI_CLM_Effective_Date__c != null && agl.Apttus__Contract_End_Date__c != null){
                     System.debug('\n\n\n scan1'+acctMap.get(agl.Apttus__Account__c).  GHI_CLM_Agreement_ID__c);
                     System.debug('\n\n\n scan2'+ agl.Id);
                     System.debug('\n\n\n comp1'+ (todayDate >= agl.GHI_CLM_Effective_Date__c) );
                     System.debug('\n\n\n comp2'+ (todayDate <= agl.Apttus__Contract_End_Date__c));
                    if(todayDate >= agl.GHI_CLM_Effective_Date__c && todayDate <= agl.Apttus__Contract_End_Date__c ){
                        if(acctMap.get(agl.Apttus__Account__c).GHI_CLM_Agreement_ID__c != agl.Id){
                            acctMap.get(agl.Apttus__Account__c).GHI_CLM_Agreement_ID__c = agl.Id;
                        }
                        System.debug('\n\n\n edited');
                                  
                        
                    }
                    else{
                        if(acctMap.get(agl.Apttus__Account__c).GHI_CLM_Agreement_ID__c == agl.Id){
                            acctMap.get(agl.Apttus__Account__c).GHI_CLM_Agreement_ID__c = null;
                        }
                    }
                    
                    for(Apttus__APTS_Agreement__c appAgg: agreementListExst){
                        if(agl.Apttus__Account__c == appAgg.Apttus__Account__c){
                            if(todayDate >= agl.GHI_CLM_Effective_Date__c && todayDate <= agl.Apttus__Contract_End_Date__c && todayDate >= appAgg.GHI_CLM_Effective_Date__c && todayDate <= appAgg.Apttus__Contract_End_Date__c  ){
                                if(acctMap.get(agl.Apttus__Account__c).GHI_CLM_Agreement_ID__c != null){
                                    acctMap.get(agl.Apttus__Account__c).GHI_CLM_Agreement_ID__c = null; 
                                    System.debug('\n\n\n edited2');
                                }
                            }
        
                         }

                     }         
                }
   
            } 
        }
        
        try{
            update acctMap.values();
        }
        catch(Exception ex){
            agreementList[0].addError(ex.getMessage());    
        }
        
        
    
    }*/
    public static void setAgreementIDPayor(List<Apttus__APTS_Agreement__c> agreementList){
        Set<ID> acctIds = new Set<ID>();
        Set<ID> agIds = new Set<Id>();
        
        for(Apttus__APTS_Agreement__c agl: agreementList){
            acctIds.add(agl.Apttus__Account__c);
            if(agl.Id != null){
                agIds.add(agl.Id);
            }
        } 
        
        //Gather payor accounts
        Map<ID, Account> acctMap = new Map<Id,Account>([SELECT GHI_CLM_Agreement_ID__c from Account where ID IN: acctIds]);
        List<Apttus__APTS_Agreement__c> agreementListExst = [SELECT GHI_CLM_Effective_Date__c, Apttus__Contract_End_Date__c, Apttus__Account__c, Apttus__Perpetual__c, Apttus__Status_Category__c from Apttus__APTS_Agreement__c where Apttus__Account__c IN: acctIds];
        for(Integer a=0; a<agreementListExst.size(); a++){
                Integer ctr =0;
                Integer pctr = 0;
                for(Integer b=(agreementListExst.size()-1); b >= 0; b--){
                    Datetime today = Datetime.now();
                    Date todayDate = Date.parse(today.format('MM/dd/yyyy'));
                    if(agreementListExst[a].Apttus__Account__c == agreementListExst[b].Apttus__Account__c && agreementListExst[a].Id != agreementListExst[b].Id ){
                         pctr++;
                         
                         boolean goodDate1 =  agreementListExst[a].Apttus__Status_Category__c == 'In Effect' && ((agreementListExst[a].Apttus__Contract_End_Date__c != null && todayDate >= agreementListExst[a].GHI_CLM_Effective_Date__c && todayDate <= agreementListExst[a].Apttus__Contract_End_Date__c ) || (todayDate >= agreementListExst[a].GHI_CLM_Effective_Date__c && agreementListExst[a].Apttus__Contract_End_Date__c == null && agreementListExst[a].Apttus__Perpetual__c ));
                         boolean goodDate2 =  agreementListExst[b].Apttus__Status_Category__c == 'In Effect' && ((agreementListExst[b].Apttus__Contract_End_Date__c != null && todayDate >= agreementListExst[b].GHI_CLM_Effective_Date__c && todayDate <= agreementListExst[b].Apttus__Contract_End_Date__c ) || (todayDate >= agreementListExst[b].GHI_CLM_Effective_Date__c && agreementListExst[b].Apttus__Contract_End_Date__c == null && agreementListExst[b].Apttus__Perpetual__c ));
                         System.debug('\n\n\n gd1Val' +goodDate1);
                         System.debug('\n\n\n gd2Val' +goodDate2);
                         if(goodDate1 && goodDate2){
                            ctr++;
                         }
                    }
                }
                System.debug('\n\n\n agIDval' + agreementListExst[a]);
                 System.debug('\n\n\n agpctr' + pctr);
               
                Datetime today = Datetime.now();
                Date todayDate = Date.parse(today.format('MM/dd/yyyy'));
                boolean goodDate = agreementListExst[a].Apttus__Status_Category__c == 'In Effect' &&  ((agreementListExst[a].Apttus__Contract_End_Date__c != null && todayDate >= agreementListExst[a].GHI_CLM_Effective_Date__c && todayDate <= agreementListExst[a].Apttus__Contract_End_Date__c ) || (todayDate >= agreementListExst[a].GHI_CLM_Effective_Date__c && agreementListExst[a].Apttus__Contract_End_Date__c == null && agreementListExst[a].Apttus__Perpetual__c ));
                if(pctr == 0){
                    if(goodDate){
                        acctMap.get(agreementListExst[a].Apttus__Account__c).GHI_CLM_Agreement_ID__c =  agreementListExst[a].Id;    
                    }
                    else{
                        acctMap.get(agreementListExst[a].Apttus__Account__c).GHI_CLM_Agreement_ID__c =  null;                 
                    }
                    
                }
                else{
                     System.debug('\n\n\n agctr' + ctr);
                     System.debug('\n\n\n goodDateVal' + goodDate);
                     Integer sctr=0;
                     
                    if(ctr==0 ){
                        if(goodDate){
                            acctMap.get(agreementListExst[a].Apttus__Account__c).GHI_CLM_Agreement_ID__c =  agreementListExst[a].Id;
                            break;
                        }
                        else{
                            acctMap.get(agreementListExst[a].Apttus__Account__c).GHI_CLM_Agreement_ID__c =  null;
                        }
                           
                    }
                    else if (ctr >0){
                        acctMap.get(agreementListExst[a].Apttus__Account__c).GHI_CLM_Agreement_ID__c =  null;
                    }
                    
                }
                
        }
        
        try{
            update acctMap.values();
        }
        catch(Exception ex){
            agreementList[0].addError(ex.getMessage());    
        }
                
                
        
   }
         
    

}