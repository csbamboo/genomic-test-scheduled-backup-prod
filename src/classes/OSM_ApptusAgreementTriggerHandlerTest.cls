@isTest
public class OSM_ApptusAgreementTriggerHandlerTest {
    
    @testSetup static void setup() {
        // Create common test payor
        List<Account> payorList = new List<Account>();
        for(Integer i=0;i<20;i++) {
            if(i<10){
                payorList.add(OSM_DataFactory.createAccount('TestpayorgoodDate'));
            }
            else{
                payorList.add(OSM_DataFactory.createAccount('TestpayorbadDate'));
            }
        }
        insert payorList;        
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Agreement_Trigger__c = true;
        insert triggerSwitch;

    }
    testMethod static void test1(){
        List<Account> accList = new List<Account>();
        accList.add(OSM_DataFactory.createAccount('test'));
        insert accList;
        
        List<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();
        
        Datetime today = Datetime.now();
        Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
        Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) + Integer.valueOf(1000);  
        agreementList.add(new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Account__c = accList[0].Id, GHI_CLM_Agreement_SubType__c = 'Commercial'));
        agreementList[0].GHI_CLM_Effective_Date__c = defDateStart;
        agreementList[0].Apttus__Contract_End_Date__c = defDateEnd;
        agreementList.add(new Apttus__APTS_Agreement__c(Name = 'Test Agreement2', Apttus__Account__c = accList[0].Id, GHI_CLM_Agreement_SubType__c = 'Commercial'));
        agreementList[1].GHI_CLM_Effective_Date__c = defDateStart;
        agreementList[1].Apttus__Contract_End_Date__c = defDateEnd;
        insert agreementList;
        
        OSM_ApptusAgreementTriggerHandler.setAgreementIDPayor(agreementList);
        
        
        
    }
      
    testmethod static void testInsertAgreementGoodDate(){
        List<Account> payorList = [SELECT ID From Account where Name = 'TestpayorgoodDate' ];
        List<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();
        for(Integer a=0;a<10;a++) {
            Datetime today = Datetime.now();
            Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
            Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) + Integer.valueOf(1000 );   
            agreementList.add(new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Account__c = payorList[a].Id, GHI_CLM_Agreement_SubType__c = 'Commercial'));
            agreementList[a].GHI_CLM_Effective_Date__c = defDateStart;
            agreementList[a].Apttus__Contract_End_Date__c = defDateEnd;
            
        }
        insert agreementList;   
        
    }
   
    testmethod static void testInsertAgreementMultiple(){
        List<Account> payorList = [SELECT ID From Account where Name = 'TestpayorbadDate' ];
        List<Apttus__APTS_Agreement__c> agreementList1 = new List<Apttus__APTS_Agreement__c>();
         
        
        for(Integer a=0;a<10;a++) {
            
            Datetime today = Datetime.now();
            Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
            Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(250 );   
            agreementList1.add(new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Account__c = payorList[a].Id, GHI_CLM_Agreement_SubType__c = 'Commercial'));
            agreementList1[a].GHI_CLM_Effective_Date__c = defDateStart;
            agreementList1[a].Apttus__Contract_End_Date__c = defDateEnd;
            
        }
        insert agreementList1;   
        
        
    }
    
     testmethod static void testUpdateAgreement(){
        List<Account> payorList = [SELECT ID From Account where Name = 'TestpayorbadDate' ];
        List<Apttus__APTS_Agreement__c> agreementList1 = new List<Apttus__APTS_Agreement__c>();
         
        
        for(Integer a=0;a<10;a++) {
            
            Datetime today = Datetime.now();
            Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
            Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(250 );   
            agreementList1.add(new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Account__c = payorList[a].Id, GHI_CLM_Agreement_SubType__c = 'Commercial'));
            agreementList1[a].GHI_CLM_Effective_Date__c = defDateStart;
            agreementList1[a].Apttus__Contract_End_Date__c = defDateEnd;
            
        }
      
      
        insert agreementList1;   
        OSM_ApptusAgreementTriggerHandler.populateAgreementIDPayorRunOnce = false;
        
        for(Integer a=0;a<10;a++) {
            
            Datetime today = Datetime.now();
            Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
            Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) + Integer.valueOf(1000 );  
            agreementList1[a].GHI_CLM_Effective_Date__c = defDateStart;
            agreementList1[a].Apttus__Contract_End_Date__c = defDateEnd;
            
        }
        update agreementList1;
        
        
        
    }
    
     testmethod static void testDeleteAgreement(){
        List<Account> payorList = [SELECT ID From Account where Name = 'TestpayorbadDate' ];
        List<Apttus__APTS_Agreement__c> agreementList1 = new List<Apttus__APTS_Agreement__c>();
         
        
        for(Integer a=0;a<10;a++) {
            
            Datetime today = Datetime.now();
            Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
            Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(250 );   
            agreementList1.add(new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Account__c = payorList[a].Id, GHI_CLM_Agreement_SubType__c = 'Commercial'));
            agreementList1[a].GHI_CLM_Effective_Date__c = defDateStart;
            agreementList1[a].Apttus__Contract_End_Date__c = defDateEnd;
            
        }
        insert agreementList1;   
        OSM_ApptusAgreementTriggerHandler.populateAgreementIDPayorRunOnce = false;
        delete agreementList1;
        
        
    }
    
    

}