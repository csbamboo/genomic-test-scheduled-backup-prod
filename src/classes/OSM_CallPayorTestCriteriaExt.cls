public class OSM_CallPayorTestCriteriaExt {
    
    private final OSM_Criteria__c criteria;
    
    public OSM_CallPayorTestCriteriaExt(ApexPages.StandardController stdController){
        List<String> fieldList = new List<String>();
        fieldList.add('OSM_Payor__c');
        fieldList.add('RecordTypeId');
        stdController.addFields(fieldList);
        this.criteria = (OSM_Criteria__c)stdController.getRecord();
    }
    
    public PageReference pageRedirect(){
        String fullFileURL = URL.getSalesforceBaseUrl().toExternalForm() +  '/apex/OSM_PayorTestCriteriaPage?Id='+criteria.Id+'&payorId='+criteria.OSM_Payor__c+'&RecordType='+criteria.RecordTypeId;
        PageReference pgRef = new PageReference(fullFileURL);
        pgRef.setRedirect(true);
        return pgRef;
    }
}