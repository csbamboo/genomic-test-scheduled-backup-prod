/*------------------------------------------------------------------------
    @author         Andrew Castillo
    @company        Cloud Sherpas
    @description    Data domain class for the Case object
                    https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    05/28/2015      Andrew Castillo         Created this class
---------------------------------------------------------------------------*/
public with sharing class OSM_CaseDomain {
    
    private static Set<Case> insertCaseSet;
    private static Set<Case> updateCaseSet; 
    private static List<Case> queriedCases;
    private static Set<Id> oliIdSet;
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns a list of Case object queried by Order Line Item ID.
       @date          05/28/2015
       @param         Set<Id> - list of OLI IDs
       @return        List<Case> - list of Case objects
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static List<Case> queryCaseByOLI(Set<Id> oliIDs) {
        
        // check id OLI IDs were passed
        if (oliIDs == null || oliIDs.size() == 0) {
            oliIdSet = null;
            return null;
        }
        
        // check if these case have already beed queried with the same OLI IDs
        if (queriedCases != null && !queriedCases.isEmpty() && oliIdSet.size() == oliIDs.size() && oliIdSet.containsAll(oliIDs)) {
            return queriedCases;
        }
        else {
            oliIdSet = oliIDs;
        }
        
        // query for cases by OLI ID
        queriedCases = [SELECT Id, OSM_Data_Transmission_Status__c, Status, OSM_Plan__c, OSM_Primary_Order_Line_Item__c,
                               OSM_Plan__r.OSM_Payor__c, OSM_Payor__c, RecordTypeId, RecordType.DeveloperName,
                               OSM_Transmission_Sent_Date__c, Type, OSM_Primary_Order__c, OSM_Tracking_Number_lookup__c, 
                               OSM_Tracking_Number_lookup__r.Name, OSM_Potentially_Responsible_Hospital_Cus__c, 
                               OSM_Material_Return_HCO__c, OSM_Material_Return_Address_Option__c
                               
                        FROM Case 
                        WHERE OSM_Primary_Order_Line_Item__c IN: oliIdSet];
                            
        return queriedCases;
    }
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns a list of Case object queried by Order Line Item ID, Data Transmission Status, and Status.
       @date          05/28/2015
       @param         Set<Id> - list of OLI IDs
       @param         String - Data Transmission Status
       @param         String - Status
       @return        List<Case> - list of Case objects
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static List<Case> queryCaseByOLIDTSStatus(Set<Id> oliIDs, String dataTransStatus, String status) {
        
        // get case list queried by OLI ID
        List<Case> caseLst = queryCaseByOLI(oliIDs);
        
        if (caseLst == null) {
            return null;
        }
        
        // loop through list of cases and filter by statuses
        List<Case> filteredCases = new List<Case>();
        for (Case C : caseLst) {
            
            // filter on status fields
            if (C.OSM_Data_Transmission_Status__c == dataTransStatus && C.Status == status) {
                filteredCases.add(C);
            }
        }
        
        return filteredCases;
    }
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns a list of Case object queried by Order Line Item ID, and Type.
       @date          05/28/2015
       @param         Set<Id> - list of OLI IDs
       @param         String - Type
       @return        List<Case> - list of Case objects
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static List<Case> queryCaseByOLIType(Set<Id> oliIDs, String cType) {
        
        // get case list queried by OLI ID
        List<Case> caseLst = queryCaseByOLI(oliIDs);
        
        if (caseLst == null) {
            return null;
        }
        
        // loop through list of cases and filter by type
        List<Case> filteredCases = new List<Case>();
        for (Case C : caseLst) {
            
            // filter on status fields
            if (C.Type == cType) {
                filteredCases.add(C);
            }
        }
        
        return filteredCases;
    }
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns a list of Case object queried by Order Line Item ID, and Record Type ID.
       @date          05/28/2015
       @param         Set<Id> - list of OLI IDs
       @param         Id - Record Type Id
       @return        List<Case> - list of Case objects
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static List<Case> queryCaseByOLIRecordTypeId(Set<Id> oliIDs, Id recordTypeId) {
        
        // get case list queried by OLI ID
        List<Case> caseLst = queryCaseByOLI(oliIDs);
        
        if (caseLst == null) {
            return null;
        }
        
        // loop through list of cases and filter by type
        List<Case> filteredCases = new List<Case>();
        for (Case C : caseLst) {
            
            // filter on status fields
            if (C.RecordTypeId == recordTypeId) {
                filteredCases.add(C);
            }
        }
        
        return filteredCases;
    }
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns a list of Case object queried by Order Line Item ID, and Record Type.
       @date          05/28/2015
       @param         Set<Id> - list of OLI IDs
       @param         String - Record Type Name
       @return        List<Case> - list of Case objects
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static List<Case> queryCaseByOLIRecordTypeName(Set<Id> oliIDs, String recordTypeName) {
        
        // get case record type ID
        Id caseRTId = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        
        // get and return case list queried by OLI ID and record type ID
        return queryCaseByOLIRecordTypeId(oliIDs, caseRTId);
    }
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns a list of Case object queried by Order Line Item ID, and Record Type ID.
       @date          05/28/2015
       @param         Set<Id> - list of OLI IDs
       @param         Id - Record Type Name
       @param         String - status
       @return        List<Case> - list of Case objects
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static List<Case> queryCaseByOLIRecordTypeNameStatus(Set<Id> oliIDs, String recordTypeName, String status) {
        
        // get case list queried by OLI ID and record type name
        List<Case> caseLst = queryCaseByOLIRecordTypeName(oliIDs, recordTypeName);
        
        if (caseLst == null) {
            return null;
        }
        
        // loop through list of cases and filter by type
        List<Case> filteredCases = new List<Case>();
        for (Case C : caseLst) {
            
            // filter on status fields
            if (C.Status == status) {
                filteredCases.add(C);
            }
        }
        
        return filteredCases;
    }
    
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Add a case to the list of cases to insert.
       @date          05/28/2015
       @param         Case - Case sObject
       @return        N/A
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static void addInsertCase(Case C) {
        
        // check if set is initiallized
        if (insertCaseSet == null) {
            insertCaseSet = new Set<Case>();
        }
        
        if (!insertCaseSet.contains(C)) {
            insertCaseSet.add(C);
        }
    }
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Add a case to the list of cases to update.
       @date          05/28/2015
       @param         Case - Case sObject
       @return        N/A
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static void addUpdateCase(Case C) {
        
        // check if set is initiallized
        if (updateCaseSet == null) {
            updateCaseSet = new Set<Case>();
        }
        
        if (!updateCaseSet.contains(C)) {
            updateCaseSet.add(C);
        }
    }   
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Performs DML operations on lists of cases.
       @date          05/28/2015
       @param         N/A
       @return        N/A
       
       History
       05/28/2015      Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    public static void commitCase() {
        
        // perform insert
        if (insertCaseSet.size() > 0) {
            insert new List<Case>(insertCaseSet);
        }
        
        // perform update
        if (updateCaseSet.size() > 0) {
            update new List<Case>(updateCaseSet);
        }
    }

}