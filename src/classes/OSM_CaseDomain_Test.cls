/*
  @author: Amanpreet Singh Sidhu 
  @date: 21 June 2015
  @description: Test Class for OSM_CaseDomain apex class
  @history: 21 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest(seeAllData = false)
//@isTest
public class OSM_CaseDomain_Test 
{
  
    static testMethod void orderItemWithCases() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = true;
        triggerSwitch.Work_Order_Trigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        
        Set<Id> setOrderItemcases = new Set<Id>();
        List<Case> lstCase = new List<Case>();
        List<OrderItem> lstOrderItem = new List<OrderItem>();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;
        
        Id pricebookStandard = Test.getStandardPricebookId();
        
        Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        order1.Pricebook2Id = pricebookStandard;
        insert order1;
                       
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(order1.id);
        insert workOrder;
        
        Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
        insert prod2;
        
        
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
        insert pbe;
        
        OrderItem ordItem = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, OSM_Lab_and_Report_Status__c = 'Pre-Processing');
              
        OrderItem ordItem2 = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , UnitPrice = 5 , Quantity = 2, OSM_Lab_and_Report_Status__c = 'Pre-Processing');
             
        lstOrderItem.add(ordItem);
        lstOrderItem.add(ordItem2);
                
        insert lstOrderItem;
             
        setOrderItemcases.add(ordItem.Id);
        setOrderItemcases.add(ordItem2.Id);
                
        Case cs1 = new Case(RecordTypeId = caseRecordType,OSM_Primary_Order__c = order1.Id, OSM_Primary_Order_Line_Item__c = ordItem.Id,
                            OSM_Data_Transmission_Status__c = 'Complete', Status = 'Open', Type = 'TestType');
                            
        Case cs2 = new Case(RecordTypeId = caseRecordType,OSM_Primary_Order__c = order1.Id, OSM_Primary_Order_Line_Item__c = ordItem2.Id,
                            OSM_Data_Transmission_Status__c = 'Complete', Status = 'Open', Type = 'TestType');
        
        lstCase.add(cs1);
        lstCase.add(cs2);
        
        insert lstCase;
        
        Test.startTest();
        
        OSM_CaseDomain.queryCaseByOLIDTSStatus(setOrderItemcases,'Complete','Open');
        OSM_CaseDomain.queryCaseByOLIType(setOrderItemcases,'TestType');
        OSM_CaseDomain.queryCaseByOLIRecordTypeId(setOrderItemcases,caseRecordType);
        OSM_CaseDomain.queryCaseByOLIRecordTypeName(setOrderItemcases,'Billing');
        OSM_CaseDomain.queryCaseByOLIRecordTypeNameStatus(setOrderItemcases,'Billing','Open');
                   
        Test.stopTest();
    
    }
    
    static testMethod void insertUpdateCommitCases() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Case_Trigger__c = false;
        insert triggerSwitch;
        
        Case cs1 = OSM_DataFactory.createCase('Open', 'TestTypeInsert');
        Case cs2 = OSM_DataFactory.createCase('Open', 'TestTypeUpdate');
        insert cs2;
        
        Test.startTest();
        
        OSM_CaseDomain.addInsertCase(cs1);
        OSM_CaseDomain.addUpdateCase(cs2);
        OSM_CaseDomain.commitCase();
                   
        Test.stopTest();
    
    }
    
    static testMethod void emptySet() 
    {
        Set<Id> setOrderItemcases = new Set<Id>();
        
        Test.startTest();
        
        OSM_CaseDomain.queryCaseByOLIDTSStatus(setOrderItemcases,'Complete','Open');
                   
        Test.stopTest();
    
    }
    
    static testMethod void orderItemWithoutCases() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = true;
        triggerSwitch.Work_Order_Trigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        
        Set<Id> setOrderItem = new Set<Id>();
        Set<Id> setOrderItem2 = new Set<Id>();
        //List<Case> lstCase = new List<Case>();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;
        
        Id pricebookStandard = Test.getStandardPricebookId();
        
        Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        order1.Pricebook2Id = pricebookStandard;
        insert order1;
                       
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(order1.id);
        insert workOrder;
        
        Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
        insert prod2;
        
        
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
        insert pbe;
        
        OrderItem ordItem = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, OSM_Lab_and_Report_Status__c = 'Pre-Processing');
        insert ordItem;
                
        setOrderItem.add(ordItem.Id);
        setOrderItem2.add(ordItem.Id);
        
        Test.startTest();
        
        OSM_CaseDomain.queryCaseByOLIDTSStatus(setOrderItem,'Complete','Open');
        OSM_CaseDomain.queryCaseByOLIType(setOrderItem,'TestType');
        OSM_CaseDomain.queryCaseByOLIRecordTypeId(setOrderItem,caseRecordType);
        OSM_CaseDomain.queryCaseByOLIRecordTypeName(setOrderItem,'Billing');
        OSM_CaseDomain.queryCaseByOLIRecordTypeNameStatus(setOrderItem2,'Billing','Open');
           
        Test.stopTest();
    
    }
}