/**
 * File Info
 * ----------------------------------
 * @filename       OSM_CasePrimaryOrderRolesExt.cls
 * @created        13.NOV.2014
 * @author         Kristian Vegerano
 * @description    Extension class for OSM_CasePrimaryOrderRoles page. 
 * @history        13.NOV.2014 - Kristian Vegerano - Created  
 */
public class OSM_CasePrimaryOrderRolesExt{
    public Case pageCase {get;set;}

    //Pagination
    public Integer pageSize {get;set;}
    public Integer currentPage {get;set;}
    public Integer totalPage {get;set;}
    public Integer currentRecord {get;set;}
    public List<OSM_Order_Role__c> pagedIIList {get;set;}
    public Boolean hasPageError {get;set;}
    public Boolean hasNext {get;set;}
    public Boolean hasPrevious {get;set;}
    public Boolean addOnly {get;set;}
    public Integer addRowCount {get;set;}
    public String accountName {get;set;}
    public String contactName {get;set;}
    public String opportunityName {get;set;}
    public List<OSM_Order_Role__c> managedIIList {get;set;}
    
    /**
     * @author         Kristian Vegerano
     * @description    Constructor.
     * @history        13.NOV.2014 - Kristian Vegerano - Created  
     */
    public OSM_CasePrimaryOrderRolesExt(ApexPages.StandardController stdController) {
        Id pageCaseId = stdController.getRecord().Id;
        pageCase = [SELECT Id, OSM_Primary_Order__c FROM Case WHERE Id = :pageCaseId];
        
        //Initialize Variables
        pageSize = 3;
        currentPage = 1;
        currentRecord = 1;
        hasNext = true;
        hasPrevious = false;
        pagedIIList = new List<OSM_Order_Role__c>();
        managedIIList = new List<OSM_Order_Role__c>();
            
        for(OSM_Order_Role__c loopOrderRole : [SELECT Id, Name, OSM_Account__c, OSM_Contact__c, OSM_Role__c FROM OSM_Order_Role__c WHERE OSM_Order__c = :pageCase.OSM_Primary_Order__c]){
             managedIIList.add(loopOrderRole);
        }
        
        for(Integer counter = 0; counter < (managedIIList.size() < pageSize ? managedIIList.size() : pageSize); counter++){
            pagedIIList.add(managedIIList[counter]);
        }
        totalPage = (Integer)Math.ceil((Double)managedIIList.size()/pageSize);
        if(managedIIList.size() <= pageSize){
            hasNext = false;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the next 10 records.
     * @history        09 June,2014 - Kristian Vegerano - Created  
     */
    public PageReference nextPage(){
        try{
            currentPage += 1;
            currentRecord += pageSize;
            pagedIIList.clear();
            for(Integer counter = currentRecord; counter < (managedIIList.size()+1 < (currentRecord + pageSize) ? managedIIList.size()+1 : (currentRecord + pageSize)); counter++){
                pagedIIList.add(managedIIList[counter-1]);
            }
            hasPrevious = true;
            if(currentPage == (Integer)Math.ceil((Double)managedIIList.size()/pageSize)){
                hasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**********************************************************************************************
    @author       Kristian Vegerano
    @date         09 June,2014
    @description: Method that shows the previous 10 records.
    **********************************************************************************************/
    public PageReference previousPage(){
        try{
            currentPage -= 1;
            currentRecord -= pageSize;
            pagedIIList.clear();
            for(Integer counter = currentRecord; counter < (managedIIList.size() < (currentRecord + pageSize) ? managedIIList.size() : (currentRecord + pageSize)); counter++){
                pagedIIList.add(managedIIList[counter-1]);
            }
            hasNext = true;
            if(currentPage == 1){
                hasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**********************************************************************************************
    @author       Kristian Vegerano
    @date         09 June,2014
    @description: Method that shows the last page.
    **********************************************************************************************/
    public PageReference lastPage(){
        try{
            currentPage = (Integer)Math.ceil((Double)managedIIList.size()/pageSize) - 1;
            currentRecord = currentPage * pageSize + 1;
            pagedIIList.clear();
            for(Integer counter = currentRecord; counter < (managedIIList.size()+1 < (currentRecord + pageSize) ? managedIIList.size()+1 : (currentRecord + pageSize)); counter++){
                pagedIIList.add(managedIIList[counter-1]);
            }
            hasPrevious = true;
            currentPage = (Integer)Math.ceil((Double)managedIIList.size()/pageSize);
            if(currentPage == (Integer)Math.ceil((Double)managedIIList.size()/pageSize)){
                hasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**********************************************************************************************
    @author       Kristian Vegerano
    @date         09 June,2014
    @description: Method that shows the first page.
    **********************************************************************************************/
    public PageReference firstPage(){
        try{
            currentPage = 1;
            currentRecord = 1;
            pagedIIList.clear();
            for(Integer counter = 0; counter < pageSize; counter++){
                pagedIIList.add(managedIIList[counter]);
            }
            hasNext = true;
            if(currentPage == 1){
                hasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
}