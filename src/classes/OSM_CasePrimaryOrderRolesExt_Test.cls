@isTest
public class OSM_CasePrimaryOrderRolesExt_Test{

    static testmethod void testPages(){
        //prepare record types, account and delegate
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        Contact con = OSM_DataFactory.createContact(1,acct.ID,null);
        insert con;
        Contact pat = OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_Patient_Record_Type__c);
        insert pat;
        Order ord = OSM_DataFactory.createOrder(pat.ID, acct.ID);
        ord.Status = 'Active';
        insert ord;
        OSM_Order_Role__c ordRole = OSM_DataFactory.createOrderRole(ord.ID,acct.ID, con.ID);
        insert ordRole;
        Case mycase = OSM_DataFactory.createCase('Open','Potential Duplicate');
        mycase.OSM_Primary_Order__c = ord.Id;
        insert mycase;
        Test.startTest();
        PageReference pageRef = Page.OSM_CasePrimaryOrderRoles;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mycase);
        ApexPages.currentPage().getParameters().put('Id',mycase.id);
        OSM_CasePrimaryOrderRolesExt cons = new OSM_CasePrimaryOrderRolesExt(sc);
        cons.nextPage();
        cons.previousPage();
        cons.firstPage();
        cons.lastPage();
        
        cons.accountName = 'sample account';
        cons.contactName = 'sample contact';
        cons.opportunityname = 'sample opportunity';
        cons.hasPageError = false;
        
        // to cover exception codes
        cons.pageSize = 0;
        cons.totalPage = 0;
        cons.nextPage();
        cons.previousPage();
        cons.firstPage();
        cons.lastPage();
        
        Test.stopTest();
    }
    
    static testmethod void testException1(){
        //prepare record types, account and delegate
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        Contact con = OSM_DataFactory.createContact(1,acct.ID,null);
        insert con;
        Contact pat = OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_Patient_Record_Type__c);
        insert pat;
        Order ord = OSM_DataFactory.createOrder(pat.ID, acct.ID);
        ord.Status = 'Active';
        insert ord;
        OSM_Order_Role__c ordRole = OSM_DataFactory.createOrderRole(ord.ID,acct.ID, con.ID);
        insert ordRole;
        Case mycase = OSM_DataFactory.createCase('Open','Potential Duplicate');
        insert mycase;
        Test.startTest();
        PageReference pageRef = Page.OSM_CasePrimaryOrderRoles;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mycase);
        ApexPages.currentPage().getParameters().put('Id',mycase.id);
        OSM_CasePrimaryOrderRolesExt cons = new OSM_CasePrimaryOrderRolesExt(sc);
        cons.nextPage();
        cons.previousPage();
        cons.firstPage();
        cons.lastPage();
        Test.stopTest();
    }
    
    static testmethod void testException2(){
        //prepare record types, account and delegate
        Case mycase = OSM_DataFactory.createCase('Open','Potential Duplicate');
        insert mycase;
        Test.startTest();
        PageReference pageRef = Page.OSM_CasePrimaryOrderRoles;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mycase);
        OSM_CasePrimaryOrderRolesExt cons = new OSM_CasePrimaryOrderRolesExt(sc);
        cons.nextPage();
        cons.previousPage();
        cons.firstPage();

        Test.stopTest();
    }
    
   
}