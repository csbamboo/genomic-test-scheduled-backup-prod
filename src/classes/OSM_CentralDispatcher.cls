/*
    @author: Rescian Rey
    @description: Trigger Dispatcher. One class to rule them all!
    @history:
        MAY 11 2015     Rescian Rey    Created
        MAY 12 2015     Rescian Rey    Renamed class. Prepended 'OSM_'
*/
public without sharing class OSM_CentralDispatcher {

    public static OSM_TriggerHandlerBase activefunction = null;
    public static void MainEntry(String TriggerObject, Boolean IsBefore, Boolean IsDelete, Boolean IsAfter,
                                    Boolean IsInsert, Boolean IsUpdate, Boolean IsExecuting, List<SObject> newlist, Map<ID, SObject> newmap, List<SObject> oldlist, Map<ID,SObject> oldmap){   
        
        Boolean triggerEnabled = Config_Bypass__c.getInstance(UserInfo.getUserId()).EnableTriggers__c;
        // Perform some logic based on the custom setting to determine whether the triggers are switched on 
        //or off and therefore whether this logic should proceed or exit and set 
        
        // Always enable trigger for testing
        if(Test.isRunningTest()){
            triggerEnabled = true;
        }

        try{
            if(activefunction != null && triggerEnabled){ 
                activefunction.InProgressEntry(TriggerObject, IsBefore, IsDelete, IsAfter, 
                    IsInsert, IsUpdate, IsExecuting, newlist, newmap, oldlist, oldmap);
            }else if(triggerEnabled){
                activefunction = getTriggerHandler(TriggerObject);
                activefunction.MainEntry(TriggerObject, IsBefore, IsDelete, IsAfter, IsInsert, 
                    IsUpdate, IsExecuting,  newlist, newmap, oldlist, oldmap);
            }
        }catch(Exception ex){
            /* Commenting out because we don't have this */
            // Diagnostics.DebugException(ex);
            // Diagnostics.PopAll();  
            System.debug(ex.getMessage()); 
        }
    }

    public static OSM_TriggerHandlerBase getTriggerHandler(String TriggerObject){
        String TypeName = TriggerObject + 'Handler';
        Type t = Type.forName(TypeName);
        OSM_TriggerHandlerBase handler = (OSM_TriggerHandlerBase)t.newInstance();
        return handler;
    }
}