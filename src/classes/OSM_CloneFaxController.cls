/********************************************************************
    @author : Rescian Rey
    @description : Controller for cloning a Fax
    @history:
        <date>                <author>                <description>
        MAY 21 2015           Rescian Rey             Created class
********************************************************************/
public with sharing class OSM_CloneFaxController {
    private ApexPages.StandardController controller;
    private Fax__c fax; // fax to clone
    public String faxID {get; set;}

	public OSM_CloneFaxController(ApexPages.StandardController c) {
        controller = c;
		
        fax = [SELECT Id, Fax_Number__c, Case__c, Order__c, Account__c,
                    (SELECT OSM_Attachment_ID__c, OSM_File_Name__c FROM Fax_Attachments__r)
                FROM Fax__c WHERE Id = :controller.getId()];
    }


    public PageReference cloneFax(){
        // Clone the fax
        Fax__c faxClone = new Fax__c(
                Fax_Number__c = fax.Fax_Number__c,
                Case__c = fax.Case__c,
                Order__c = fax.Order__c,
                Account__c = fax.Account__c,
                Resent__c = true
            );
        insert faxClone;
        faxID = faxClone.Id;

        // Clone fax attachments
        List<OSM_Fax_Attachment__c> faxAttachments = new List<OSM_Fax_Attachment__c>();
        for(OSM_Fax_Attachment__c att: fax.Fax_Attachments__r){
            OSM_Fax_Attachment__c fa = new OSM_Fax_Attachment__c(
                OSM_Attachment_ID__c = att.OSM_Attachment_ID__c,
                OSM_Fax__c = faxClone.Id,
                OSM_File_Name__c = att.OSM_File_Name__c
            );
            faxAttachments.add(fa);
        }
        insert faxAttachments;
        return null;
	}
}