/*
  @author: Amanpreet Singh Sidhu 
  @date: 28 June 2015
  @description: Test Class for OSM_CloneFaxController apex class
  @history: 28 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest(seeAllData = true)
//@isTest
public class OSM_CloneFaxController_Test 
{
  
    static testMethod void testMain() 
    {
        Fax__c fax = OSM_DataFactory.createFax();
        insert fax;
        
        OSM_Fax_Attachment__c faxAttch = new OSM_Fax_Attachment__c(OSM_Fax__c = fax.Id, OSM_File_Name__c = 'Test Attachment');
        insert faxAttch;
        
        Test.startTest();
        OSM_CloneFaxController obj = new OSM_CloneFaxController(new ApexPages.StandardController(fax));
        obj.cloneFax();
        Test.stopTest();
    }
}