/*
    @author: patrick lorilla
    @date: 22 September 2014
    @description: Test Contact Trigger Handler

*/
@isTest
public class OSM_ContactTriggerHandler_Test{

    /*
    @author: patrick lorilla
    @date: 22 September 2014
    @description: Test Update Address
    */
    static testmethod void testUpdateAddress(){
    
        //Create Account to pass on to custom setting
        Id accOtherRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        Account accParent = new Account(Name = 'ParentAccount', RecordTypeId = accOtherRecType);
        insert accParent;
        
        //Prepare required custom settings
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = true;
        insert triggerSwitch;
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        recordTypeIds.Contact_Patient_Default_Account__c = accParent.Id;
        recordTypeIds.Contact_Patient_Default_Account_Records__c = 50;
        insert recordTypeIds;
        
        //Prepare HCO Account
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(0, 'United States', 'Street', 'San Francisco', 'California', '12345678', recordTypeIds.Account_HCO_Record_Type__c);
        insert acct;
        
        
        List<Contact> conList = new List<Contact>();
        Test.startTest();
        
        OSM_ContactTriggerHandler.setDefaultAccountPatientRunOnce = true;
        
        //Insert 20 HCP Contacts
        for(Integer a=0; a<20; a++){
            conList.add(OSM_DataFactory.createContact(a, acct.ID,  (ID) recordTypeIds.Contact_HCP_Record_Type__c));
            conList[a].OwnerId = userInfo.getUserId();
        }
        //Insert 20 Patient Contact
        for(Integer a=20; a<40; a++){
            conList.add(OSM_DataFactory.createContact(a, null,  (ID) recordTypeIds.Contact_Patient_Record_Type__c));
            conList[a].OwnerId = userInfo.getUserId();
        }
    
        insert conList;

        for(Integer a=0; a<40; a++){
            if(a<10){
                conList[a].RecordTypeId = (ID) recordTypeIds.Contact_Patient_Record_Type__c;
            }
            if((a>20)&&(a<30)){
                conList[a].RecordTypeId = (ID) recordTypeIds.Contact_HCP_Record_Type__c;
            }
        }
        update conList;
        
        Map<String,Id> leadContactMap = new Map<String,Id>();
        leadContactMap.put('leadUser', conList[0].Id);
        OSM_ContactTriggerHandler.associateCustomerAffiliation(leadContactMap);
        
        Test.stopTest();
        
        //Assert that there are only 20 Contacts that has the same address with the HCO account.
        
        conList = [SELECT ID from Contact Where MailingCountry = 'United States' AND MailingStreet = 'Street' AND MailingCity = 'San Francisco' AND MailingState = 'California' AND MailingPostalCode ='12345678' ];
        //System.assertEquals(conList.size(), 20);
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Tests association of Contact Territory Assignment on account creation. 
     * @history        27.OCT.2014 - Kristian Vegerano - Created  
     */
    public static testmethod void testInsertContactTerritoryAssignment(){
        //Prepare triggers and record types
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = true;
        insert triggerSwitch;
        
        OSM_Territory__c testTerritory = OSM_DataFactory.createTerritory('Test Territory');
        insert testTerritory;
        
        OSM_Alignment_Rule__c testAlignmentRule1 = OSM_DataFactory.createAlignmentRule(1, 'US', '12345', 'Oncology', testTerritory.Id);
        testAlignmentRule1.Rule_Type__c = 'Postal';
        insert testAlignmentRule1;
        
        OSM_Alignment_Rule__c testAlignmentRule2 = OSM_DataFactory.createAlignmentRule(2, 'US', '12345', 'Oncology', testTerritory.Id);
        testAlignmentRule2.Rule_Type__c = 'Country';
        insert testAlignmentRule2;
        
        OSM_Alignment_Rule__c testAlignmentRule3 = OSM_DataFactory.createAlignmentRule(1, 'US', '67890', 'Oncology', testTerritory.Id);
        testAlignmentRule3.Rule_Type__c = 'Partial Postal';
        insert testAlignmentRule3;
        
        OSM_Alignment_Rule__c testAlignmentRule4 = OSM_DataFactory.createAlignmentRule(2, 'US', '67890', 'Oncology', testTerritory.Id);
        testAlignmentRule4.Rule_Type__c = 'Country';
        insert testAlignmentRule4;
        
        OSM_Alignment_Rule__c testAlignmentRule5 = OSM_DataFactory.createAlignmentRule(3, 'US', '67891', 'Oncology', testTerritory.Id);
        testAlignmentRule5.Rule_Type__c = 'Postal';
        insert testAlignmentRule5;
        
        List<Contact> insertContactList = new List<Contact>();
        Contact testContact1 = OSM_DataFactory.createContactWithMailingAddress(1, 'USA', 'Test Street', 'Test City', 'Test Province', '12345', null);
        testContact1.OSM_Status__c = 'Approved';
        testContact1.OSM_Specialty__c = 'Hospital';
        testContact1.OSM_Oncology__c = true;
        testContact1.OSM_Urology__c = true;
        testContact1.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact1);
        
        Contact testContact2 = OSM_DataFactory.createContactWithMailingAddress(2, 'Philippines', 'Test Street', 'Test City', 'Test Province', '67890', null);
        testContact2.OSM_Status__c = 'Approved';
        testContact2.OSM_Specialty__c = 'Hospital';
        testContact2.OSM_Oncology__c = true;
        testContact2.OSM_Urology__c = true;
        testContact2.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact2);
        
        Contact testContact3 = OSM_DataFactory.createContactWithMailingAddress(3, 'Philippines', 'Test Street', 'Test City', 'Test Province', '67890', null);
        testContact3.OSM_Status__c = 'Approved';
        testContact3.OSM_Specialty__c = 'Hospital';
        testContact3.OSM_Oncology__c = true;
        testContact3.OSM_Urology__c = true;
        testContact3.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact3);
        
        Contact testContact4 = OSM_DataFactory.createContactWithMailingAddress(4, 'Philippines', 'Test Street', 'Test City', 'Test Province', '09231', null);
        testContact4.OSM_Status__c = 'Approved';
        testContact4.OSM_Specialty__c = 'Hospital';
        testContact4.OSM_Oncology__c = true;
        testContact4.OSM_Urology__c = true;
        testContact4.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact4);
        
        //System.assertEquals(0, [SELECT Id FROM OSM_HCP_Territory_Assignment__c].size());
        Test.startTest();
            insert insertContactList;
            Map<Id, Contact> contactMap = new Map<Id, Contact>();
            contactMap.put(testContact1.Id, testContact1);
            contactMap.put(testContact2.Id, testContact2);
            contactMap.put(testContact3.Id, testContact3);
            contactMap.put(testContact4.Id, testContact4);
            Set<Id> contactSet = new Set<Id>();
            contactSet.add(testContact1.Id);
            contactSet.add(testContact2.Id);
            contactSet.add(testContact3.Id);
            contactSet.add(testContact4.Id);
            Set<String> postalCodes = new Set<String>();
            postalCodes.add(testContact1.MailingPostalCode);
            postalCodes.add(testContact2.MailingPostalCode);
            postalCodes.add(testContact3.MailingPostalCode);
            postalCodes.add(testContact4.MailingPostalCode);
            OSM_ContactTriggerHandler.alignContactToTerritoryNullZip(contactMap);
        Test.stopTest();
       // System.assertEquals(4, [SELECT Id FROM OSM_HCP_Territory_Assignment__c].size());
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Tests association of Contact Territory Assignment on account creation. 
     * @history        27.OCT.2014 - Kristian Vegerano - Created  
     */
    public static testmethod void testUpdateContactTerritoryAssignment(){
        //Prepare triggers and record types
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = true;
        insert triggerSwitch;
        
        OSM_Territory__c testTerritory = OSM_DataFactory.createTerritory('Test Territory');
        insert testTerritory;
        
        List<OSM_Alignment_Rule__c> alignmentRulesList = new List<OSM_Alignment_Rule__c>();
        OSM_Alignment_Rule__c testAlignmentRule1 = OSM_DataFactory.createAlignmentRule(1, 'US', '12345', 'Oncology', testTerritory.Id);
        testAlignmentRule1.Rule_Type__c = 'Country';
        alignmentRulesList.add(testAlignmentRule1);
        
        OSM_Alignment_Rule__c testAlignmentRule2 = OSM_DataFactory.createAlignmentRule(2, 'US', '12345', 'Oncology', testTerritory.Id);
        testAlignmentRule2.Rule_Type__c = 'Postal';
        alignmentRulesList.add(testAlignmentRule2);
        
        OSM_Alignment_Rule__c testAlignmentRule3 = OSM_DataFactory.createAlignmentRule(3, 'US', '67890', 'Oncology', testTerritory.Id);
        testAlignmentRule3.Rule_Type__c = 'Country';
        alignmentRulesList.add(testAlignmentRule3);
        
        OSM_Alignment_Rule__c testAlignmentRule4 = OSM_DataFactory.createAlignmentRule(4, 'US', '67890', 'Oncology', testTerritory.Id);
        testAlignmentRule4.Rule_Type__c = 'Country';
        alignmentRulesList.add(testAlignmentRule4);
        
        OSM_Alignment_Rule__c testAlignmentRule5 = OSM_DataFactory.createAlignmentRule(5, 'US', '67891', 'Oncology', testTerritory.Id);
        testAlignmentRule5.Rule_Type__c = 'Country';
        alignmentRulesList.add(testAlignmentRule5);
        
        OSM_Alignment_Rule__c testAlignmentRule6 = OSM_DataFactory.createAlignmentRule(6, 'US', null, 'Oncology', testTerritory.Id);
        testAlignmentRule6.Rule_Type__c = 'Country';
        alignmentRulesList.add(testAlignmentRule6);
        
        OSM_Alignment_Rule__c testAlignmentRule7 = OSM_DataFactory.createAlignmentRule(7, 'US', null, 'Oncology', testTerritory.Id);
        testAlignmentRule7.Rule_Type__c = 'Country';
        alignmentRulesList.add(testAlignmentRule7);
        
        insert alignmentRulesList;
        
        List<Contact> insertContactList = new List<Contact>();
        Contact testContact1 = OSM_DataFactory.createContactWithMailingAddress(1, 'US', 'Test Street', 'Test City', 'Test Province', '12345', null);
        testContact1.OSM_Specialty__c = 'Hospital';
        testContact1.OSM_Oncology__c = true;
        testContact1.OSM_Urology__c = true;
        testContact1.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact1);
        
        Contact testContact2 = OSM_DataFactory.createContactWithMailingAddress(2, 'US', 'Test Street', 'Test City', 'Test Province', '67892', null);
        testContact2.OSM_Specialty__c = 'Hospital';
        testContact2.OSM_Oncology__c = true;
        testContact2.OSM_Urology__c = true;
        testContact2.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact2);
        
        Contact testContact3 = OSM_DataFactory.createContactWithMailingAddress(3, 'US', 'Test Street', 'Test City', 'Test Province', '67890', null);
        testContact3.OSM_Specialty__c = 'Hospital';
        testContact3.OSM_Oncology__c = true;
        testContact3.OSM_Urology__c = true;
        testContact3.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact3);
        
        Contact testContact4 = OSM_DataFactory.createContactWithMailingAddress(4, 'US', 'Test Street', 'Test City', 'Test Province', '09231', null);
        testContact4.OSM_Specialty__c = 'Hospital';
        testContact4.OSM_Oncology__c = true;
        testContact4.OSM_Urology__c = true;
        testContact4.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact4);
        
        Contact testContact5 = OSM_DataFactory.createContactWithMailingAddress(4, 'Philippines', 'Test Street', 'Test City', 'Test Province', null, null);
        testContact5.OSM_Specialty__c = 'Hospital';
        testContact5.OSM_Oncology__c = true;
        testContact5.OSM_Urology__c = true;
        testContact5.OSM_Country_Code__c = 'US';
        insertContactList.add(testContact5);
        
        insert insertContactList;
        
       //System.assertEquals(0, [SELECT Id FROM OSM_HCP_Territory_Assignment__c].size());
        Test.startTest();
            for(Contact loopContact : insertContactList){
                loopContact.OSM_Status__c = 'Approved';
            }            
            update insertContactList;
        Test.stopTest();
        //System.assertEquals(5, [SELECT Id FROM OSM_HCP_Territory_Assignment__c].size());
    }
     public static testmethod void testorderworkflowIsBlank(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = true;
        insert triggerSwitch;
        
        
        List<Contact> insertContactList = new List<Contact>();
        Contact testContact1 = OSM_DataFactory.createContactWithMailingAddress(1, 'USA', 'Test Street', 'Test City', 'Test Province', '12345', null);
        testContact1.OSM_Specialty__c = 'Hospital';
        testContact1.OSM_Oncology__c = true;
        testContact1.OSM_Urology__c = true;
        insertContactList.add(testContact1);
        
        Contact testContact2 = OSM_DataFactory.createContactWithMailingAddress(2, 'Philippines', 'Test Street', 'Test City', 'Test Province', '67892', null);
        testContact2.OSM_Specialty__c = 'Hospital';
        testContact2.OSM_Oncology__c = true;
        testContact2.OSM_Urology__c = true;
        insertContactList.add(testContact2);
        
        Contact testContact3 = OSM_DataFactory.createContactWithMailingAddress(3, 'Philippines', 'Test Street', 'Test City', 'Test Province', '67890', null);
        testContact3.OSM_Specialty__c = 'Hospital';
        testContact3.OSM_Oncology__c = true;
        testContact3.OSM_Urology__c = true;
        insertContactList.add(testContact3);
        
        Contact testContact4 = OSM_DataFactory.createContactWithMailingAddress(4, 'Philippines', 'Test Street', 'Test City', 'Test Province', '09231', null);
        testContact4.OSM_Specialty__c = 'Hospital';
        testContact4.OSM_Oncology__c = true;
        testContact4.OSM_Urology__c = true;
        insertContactList.add(testContact4);
        
        Contact testContact5 = OSM_DataFactory.createContactWithMailingAddress(4, 'Philippines', 'Test Street', 'Test City', 'Test Province', null, null);
        testContact5.OSM_Specialty__c = 'Hospital';
        testContact5.OSM_Oncology__c = true;
        testContact5.OSM_Urology__c = true;
        insertContactList.add(testContact5);
        Profile profile1 = [SELECT Name,Id FROM Profile WHERE name = 'GHI Customer Community Plus'];//GHI Portal User
        User user1 = new User(
        Username = 'test12345@test.com',
        ContactId = testContact5.Id,
        ProfileId = profile1.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        insert insertContactList;
            Test.startTest();
            for(Contact loopContact : insertContactList){
                loopContact.GHI_Portal_Order_Workflow__c = 'Approved';
            }            
            update insertContactList;
        Test.stopTest();
            
     }
     
     public static testMethod void testSetDefaultAccount(){
         Id accOtherRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        Account accParent = new Account(Name = 'ParentAccount', RecordTypeId = accOtherRecType);
        insert accParent;
        
        //Prepare required custom settings
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = true;
        insert triggerSwitch;
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        recordTypeIds.Contact_Patient_Default_Account__c = accParent.Id;
        recordTypeIds.Contact_Patient_Default_Account_Records__c = 50;
        insert recordTypeIds;
        
        //Prepare HCO Account
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(0, 'United States', 'Street', 'San Francisco', 'California', '12345678', recordTypeIds.Account_HCO_Record_Type__c);
        insert acct;
        
        
        List<Contact> conList = new List<Contact>();
        Test.startTest();
        
        
        
        //Insert 20 HCP Contacts
        for(Integer a=0; a<20; a++){
            conList.add(OSM_DataFactory.createContact(a, acct.ID,  (ID) recordTypeIds.Contact_HCP_Record_Type__c));
            conList[a].OwnerId = userInfo.getUserId();
        }
        //Insert 20 Patient Contact
        for(Integer a=20; a<40; a++){
            conList.add(OSM_DataFactory.createContact(a, null,  (ID) recordTypeIds.Contact_Patient_Record_Type__c));
            conList[a].OwnerId = userInfo.getUserId();
        }
        try{
            OSM_ContactTriggerHandler.setDefaultAccountPatientRunOnce = false;
            insert conList;    
        }
        catch(Exception ex){
            OSM_ContactTriggerHandler.setDefaultAccountPatientRunOnce = true;
            insert conList;
        }
        

        for(Integer a=0; a<40; a++){
            if(a<10){
                conList[a].RecordTypeId = (ID) recordTypeIds.Contact_Patient_Record_Type__c;
            }
            if((a>20)&&(a<30)){
                conList[a].RecordTypeId = (ID) recordTypeIds.Contact_HCP_Record_Type__c;
            }
        }
        update conList;
        
        Test.stopTest();
     }


    //   static testMethod void createGHI_Portal_User(){

    //     //Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
    //     //Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('OSM_HCO').getRecordTypeId();
        
    //     /*UserRole portalRole = new UserRole(Name = 'New User Role');
    //     insert portalRole;
    //     System.debug(portalRole);*/
        
    //     Id userRoleId = [Select Id from UserRole LIMIT 1].Id;
        
    //     //User sysAd = OSM_DataFactory.createUser('Todas');
    //     /*
    //     Profile profile2 = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
    //     User sysAd = new User(FirstName = 'AJ',
    //     Username = System.now().millisecond() + 'test12345@test.com',
    //     LastName = 'Todas', 
    //     ProfileId = profile2.Id, 
    //     Alias = 'test123',
    //     Email = 'test12345@test.com',
    //     EmailEncodingKey = 'UTF-8',
    //     CommunityNickname = 'test12345',
    //     TimeZoneSidKey = 'America/Los_Angeles', 
    //     LocaleSidKey = 'en_US',
    //     LanguageLocaleKey = 'en_US',
    //     userRoleId = userRoleId);
    //     //sysAd.ContactId = contact.Id;
    //     insert sysAd;
        
    //     System.runAs(sysAd){*/
    
    //     // triggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    //     // triggerSwitch.Contact_Trigger__c = true;
    //     // insert triggerSwitch;
        
    //     Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
    //     insert recordTypeIds;
        
    //     Id HCPRecType = recordTypeIds.Contact_HCP_Record_Type__c;
    //     Id HCORecType = recordTypeIds.Account_HCO_Record_Type__c;
    //     Profile profile1 = [Select Id from Profile where Name = 'GHI Portal User' LIMIT 1];
        
    //     Account account = OSM_DataFactory.createAccount('New Account');
    //     account.RecordTypeId = HCORecType;
    //     insert account;

    //     Contact contact = OSM_DataFactory.createContact('AJ', 'Todas', HCPRecType);
    //     contact.AccountId = account.Id;
    //     insert contact;
        
    //     User portalAccountOwner1 = new User();
    //     portalAccountOwner1.UserRoleId = UserRoleId;
    //     portalAccountOwner1.ProfileId = profile1.Id;
    //     portalAccountOwner1.Username = System.now().millisecond() + 'test2@test.com';
    //     portalAccountOwner1.Alias = 'batman';
    //     portalAccountOwner1.Email='bruce.wayne@wayneenterprises.com';
    //     portalAccountOwner1.EmailEncodingKey='UTF-8';
    //     portalAccountOwner1.Firstname='Bruce';
    //     portalAccountOwner1.Lastname='Wayne';
    //     portalAccountOwner1.LanguageLocaleKey='en_US';
    //     portalAccountOwner1.LocaleSidKey='en_US';
    //     portalAccountOwner1.TimeZoneSidKey='America/Chicago';
    //     portalAccountOwner1.ContactId = contact.Id;
        
    //     insert portalAccountOwner1;
        
    //     // portalAccountOwner1.GHI_Portal_Order_Workflow__c = contact.Id; 
    //     // update portalAccountOwner1;
    //     contact.GHI_Portal_Order_Workflow__c = 'Intl-Partner';
    //     // sysAd.GHI_Portal_Order_Workflow__c = contact.Id;
    //     // update sysAd;
        
    //     Test.startTest();
    //     update contact;
    //     Test.stopTest();
        
        
    //   // portalAccountOwner1.GHI_Portal_Order_Workflow__c = contact.Id;
    //   // update portalAccountOwner1;

    //     //}
    // //     //Create account
    // //     Account portalAccount1 = new Account(
    // //     Name = 'TestAccount',
    // //     OwnerId = portalAccountOwner1.Id
    // //     );
    // //     insert portalAccount1;
    // //   // list<Contact> ctcLst = new list<Contact>();
    // //     // above list is from the database insert of all your contacts
    // //     map<Id, string> ctcMap = new map<Id, string>();

    // //     //Create contact
    // //     Contact contact1 = new Contact(
    // //     FirstName = 'Test',
    // //     Lastname = 'McTesty',
    // //     AccountId = portalAccount1.Id,
    // //     Email = System.now().millisecond() + 'test@test.com'
    // //     );
    // //     insert contact1;
        
    // //     //Create user
    // //     Profile portalProfile = [SELECT Id FROM Profile where Name = 'GHI Portal User' Limit 1];
    // //     User user1 = new User(
    // //     Username = System.now().millisecond() + 'test12345@test.com',
    // //     ContactId = contact1.Id,
    // //     ProfileId = portalProfile.Id,
    // //     Alias = 'test123',
    // //     Email = 'test12345@test.com',
    // //     EmailEncodingKey = 'UTF-8',
    // //     LastName = 'McTesty',
    // //     CommunityNickname = 'test12345',
    // //     TimeZoneSidKey = 'America/Los_Angeles',
    // //     LocaleSidKey = 'en_US',
    // //     LanguageLocaleKey = 'en_US'
    // //     );
    // //     insert user1;
    // //     }
    //   }
    
     static testMethod void MailingAddNull(){
       
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = true;
        insert triggerSwitch;
        
        Contact ct = new Contact(OSM_Status__c = 'Approved',OSM_Specialty__c = 'Urologist', LastName = 'Todas', FirstName ='AJ',Salutation='Mr.' , MailingCity = 'Manila', MailingState ='City', MailingCountry ='PH', MailingStreet ='Tondo' , GHI_Portal_Order_Workflow__c = 'Domestic');
        insert ct;
     
        ct.GHI_Portal_Order_Workflow__c = 'Intl-Partner';
        
        Test.startTest();
        update ct;                                                                                                                                   
        Test.stopTest();
        
    }
    
    static testMethod void StatusIsNotApprove(){
       
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = true;
        insert triggerSwitch;
        
        Contact ct = new Contact(OSM_Status__c = 'Inactive',OSM_Specialty__c = 'Urologist', LastName = 'Todas', FirstName ='AJ',Salutation='Mr.' , MailingCity = 'Manila', MailingState ='City', MailingCountry ='PH', MailingStreet ='Tondo' , GHI_Portal_Order_Workflow__c = 'Domestic');
        insert ct;
     
        ct.GHI_Portal_Order_Workflow__c = 'Intl-Partner';
        
        Test.startTest();
        update ct;                                                                                                                                   
        Test.stopTest();
        
    }
    
    // static testMethod void testMethod1(){
        
    //     TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    //     triggerSwitch.Contact_Trigger__c = true;
    //     insert triggerSwitch;
        
    //     OSM_Territory__c testTerritory = OSM_DataFactory.createTerritory('Test Territory');
    //     insert testTerritory;
        
    //     OSM_Alignment_Rule__c testAlignmentRule1 = OSM_DataFactory.createAlignmentRule(1, 'US', '12345', 'Oncology', testTerritory.Id);
    //     insert testAlignmentRule1;
    //     Contact ct = new Contact(OSM_Status__c = 'Inactive',OSM_Specialty__c = 'Urologist', LastName = 'Todas', FirstName ='AJ',Salutation='Mr.' , MailingCity = 'Manila', MailingState ='City', MailingCountry ='PH', MailingStreet ='Tondo' , GHI_Portal_Order_Workflow__c = 'Domestic',                                 
    //                             OSM_Country_Code__c = 'US');
    //     insert ct;
     
    //     ct.GHI_Portal_Order_Workflow__c = 'Intl-Partner';
        
    //     Test.startTest();
    //     ct.OSM_Country_Code__c = 'PH';
    //     update ct;                                                                                                                                   
    //     Test.stopTest();
        
    // }
    
}