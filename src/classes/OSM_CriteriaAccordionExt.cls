/*
  @author: Daniel Quismorio
  @date: 20 NOV 2014
  @description: Extension for Criteria Accordion
  @history: 20 NOV 2014 - Created (DQ)

*/
public class  OSM_CriteriaAccordionExt{
    //Variables to be reference in VF Page
    Account payorAccount;
    public OSM_Criteria__c criteriaMMR {get;set;}
    public OSM_Criteria__c criteriaODXBreast {get;set;}
    public OSM_Criteria__c criteriaODXBreast2 {get;set;}
    public OSM_Criteria__c criteriaODXColon {get;set;}
    public OSM_Criteria__c criteriaODXDCIS {get;set;}
    public OSM_Criteria__c criteriaODXProstate {get;set;}
    public OSM_Criteria__c criteriaODXProstate2 {get;set;}
    public String criteriaId {get;set;}
    
    //Standard controller
    public OSM_CriteriaAccordionExt(ApexPages.StandardController stdController){
        //Get Payor account record
        payorAccount = (Account)stdController.getRecord();
        
        //Query neccessary fields to be displayed in the accordion vf page
        /*List<OSM_Criteria__c> criteriaList = [Select Id, OSM_Policy_Status__c, OSM_PA_Pre_DOS__c, OSM_PA_Post_DOS__c, OSM_SOMN_w_Claim__c, OSM_Billing_Modifier__c,
            OSM_GZ_modifier__c, OSM_SOMN__c, OSM_Eff_Start_Criteria_Validation__c, OSM_Eff_End_Criteria_Validation__c,
            OSM_CPT_Code__c, OSM_List_Price__c,
            OSM_Contracted_Price__c, OSM_Rate_Methodology__c, OSM_Eff_Start_Pricing__c, OSM_Eff_End_Pricing__c, OSM_Plan_Exceptions_Pricing__c,
            OSM_Contract_ID__c, OSM_Plan_Type_Group__c, OSM_Eff_Start_Clinical_Criteria__c, OSM_Eff_End_Clinical_Criteria__c, 
            RecordType.Name, OSM_Policy_ID_Clinical_Criteria__c, 
            OSM_All_Physician_Types__c, 
            OSM_Active_Surveillance__c
            From OSM_Criteria__c Where OSM_Payor__c =: payorAccount.Id Order By RecordType.Name];*/
            
            //raissa :)
        List<OSM_Criteria__c> criteriaList = [Select Id, OSM_Policy_Status__c, 
            OSM_Eff_Start_Criteria_Validation__c, OSM_Eff_End_Criteria_Validation__c,  
            OSM_Eff_Start_Clinical_Criteria__c, OSM_Eff_End_Clinical_Criteria__c, 
            RecordType.Name, OSM_Policy_ID_Clinical_Criteria__c, OSM_IBC_NodalStatus_Negative__c, 
            OSM_IBC_NodalStatus_Micromets__c, OSM_IBC_NodalStatus_Node_1_3__c, OSM_IBC_NodalStatus_Node_4__c, OSM_IBC_NodalStatus_Unknown_Uncertain__c, 
            OSM_IBC_NodalStatus_Unspecified__c, OSM_IBC_GHI_HER2Status_Positive__c, OSM_IBC_ERStatus_Negative__c, OSM_IBC_Multi_Tumor__c, OSM_IBC_Gender_Male__c, 
            OSM_IBC_Out_of_Criteria__c, OSM_IBC_Non_Med_Onc_Orders__c, OSM_DCIS_DCIS__c, OSM_DCIS_Out_of_Criteria__c, OSM_Colon_Clinical_Stage_II__c, 
            OSM_Colon_Clinical_Stage_III_A_B__c, OSM_Colon_Clinical_Stage_III_C__c, OSM_Colon_Out_of_Criteria__c, 
            OSM_Prostate_Age_of_Biopsy_6_mos__c, OSM_Prostate_Age_of_Biopsy_6_36_mos__c, OSM_Prostate_Submitted_GleasonScore_3_4__c, 
            OSM_Prostate_Submitted_GleasonScore_4_3__c, OSM_Prostate_Submitted_GleasonScoreG_4_3__c, OSM_Prostate_Out_of_Criteria__c
            From OSM_Criteria__c Where OSM_Payor__c =: payorAccount.Id Order By RecordType.Name];
         //List<OSM_Criteria__c> criteriaList = new List<OSM_Criteria__c>();
        //Instantiate object variable  
        criteriaMMR = new OSM_Criteria__c();
        criteriaODXBreast = new OSM_Criteria__c();
        criteriaODXBreast2 = new OSM_Criteria__c();
        criteriaODXColon = new OSM_Criteria__c();
        criteriaODXDCIS = new OSM_Criteria__c();
        criteriaODXProstate = new OSM_Criteria__c();
        criteriaODXProstate2 = new OSM_Criteria__c();
        
        //Assign record to a specific object variable
        for(OSM_Criteria__c criteriaLoop : criteriaList){
            if(criteriaLoop.RecordType.Name == 'MMR'){
                criteriaMMR = criteriaLoop;
            }
            if(criteriaLoop.RecordType.Name == 'ODX Breast'){
                criteriaODXBreast = criteriaLoop;
                criteriaODXBreast2 = criteriaLoop;
            }
            if(criteriaLoop.RecordType.Name == 'ODX Colon'){
                criteriaODXColon = criteriaLoop;
            }
            if(criteriaLoop.RecordType.Name == 'ODX DCIS'){
                criteriaODXDCIS = criteriaLoop;
            }
            if(criteriaLoop.RecordType.Name == 'ODX Prostate'){
                criteriaODXProstate = criteriaLoop;
                criteriaODXProstate2 = criteriaLoop;
            }
        }
    }
    
    public PageReference editPage(){
        Id criteriaId = ApexPages.currentPage().getParameters().get('criteriaId');
        PageReference pageRef = new PageReference('/' + criteriaId + '/e?retURL=' + payorAccount.Id);
        
        return pageRef;
    }
}