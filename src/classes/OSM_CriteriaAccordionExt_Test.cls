/*
 *Author: Michiel Patricia M. Robrigado
 *Date created: December 3, 2014
 *Test for OSM_AffiliationsController 
 */
@isTest//(seeAllData=true)
private class OSM_CriteriaAccordionExt_Test{
    public static testMethod void OSM_CriteriaAccordionExtTestMMRRecType() {   
    	Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        id critRecId = Schema.SObjectType.OSM_Criteria__c.getRecordTypeInfosByName().get('MMR').getRecordTypeId();
        id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();

        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount('testName');
        acc.RecordTypeId = accRecId;
        insert acc;
        system.debug('test acc ' + acc.Id);
        
        //insert the details from the data builder to criteria
        OSM_Criteria__c crit = new OSM_Criteria__c();
        crit = OSM_DataFactory.createCriteria('testName', acc.Id, critRecId);
        system.debug('test crit ' + crit);
        insert crit;
        
        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_CriteriaAccordionExt extension = new OSM_CriteriaAccordionExt(sc);
        extension.editPage();
    }
    public static testMethod void OSM_CriteriaAccordionExtTestODXBreastRecType() {   
    	Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        id critRecId = Schema.SObjectType.OSM_Criteria__c.getRecordTypeInfosByName().get('IBC').getRecordTypeId();
        id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();

        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount('testName');
        acc.RecordTypeId = accRecId;
        insert acc;
        system.debug('test acc ' + acc.Id);
        
        //insert the details from the data builder to criteria
        OSM_Criteria__c crit = new OSM_Criteria__c();
        crit = OSM_DataFactory.createCriteria('testName', acc.Id, critRecId);
        system.debug('test crit ' + crit);
        insert crit;
        
        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_CriteriaAccordionExt extension = new OSM_CriteriaAccordionExt(sc);
        extension.editPage();
    }
    public static testMethod void OSM_CriteriaAccordionExtTestODXColonRecType() {   
    	Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        id critRecId = Schema.SObjectType.OSM_Criteria__c.getRecordTypeInfosByName().get('Colon').getRecordTypeId();
        id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();

        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount('testName');
        acc.RecordTypeId = accRecId;
        insert acc;
        system.debug('test acc ' + acc.Id);
        
        //insert the details from the data builder to criteria
        OSM_Criteria__c crit = new OSM_Criteria__c();
        crit = OSM_DataFactory.createCriteria('testName', acc.Id, critRecId);
        system.debug('test crit ' + crit);
        insert crit;
        
        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_CriteriaAccordionExt extension = new OSM_CriteriaAccordionExt(sc);
        extension.editPage();
    }
    public static testMethod void OSM_CriteriaAccordionExtTestODXDCISRecType() {   
    	Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        id critRecId = Schema.SObjectType.OSM_Criteria__c.getRecordTypeInfosByName().get('DCIS').getRecordTypeId();
        id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();

        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount('testName');
        acc.RecordTypeId = accRecId;
        insert acc;
        system.debug('test acc ' + acc.Id);
        
        //insert the details from the data builder to criteria
        OSM_Criteria__c crit = new OSM_Criteria__c();
        crit = OSM_DataFactory.createCriteria('testName', acc.Id, critRecId);
        system.debug('test crit ' + crit);
        insert crit;
        
        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_CriteriaAccordionExt extension = new OSM_CriteriaAccordionExt(sc);
        extension.editPage();
    }
    public static testMethod void OSM_CriteriaAccordionExtTestODXProstateRecType() {   
    	Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        
        id critRecId = Schema.SObjectType.OSM_Criteria__c.getRecordTypeInfosByName().get('Prostate').getRecordTypeId();
        id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();

        //insert the details from the data builder to account
        Account acc = new Account();
        acc = OSM_DataFactory.createAccount('testName');
        acc.RecordTypeId = accRecId;
        insert acc;
        system.debug('test acc ' + acc.Id);
        
        //insert the details from the data builder to criteria
        OSM_Criteria__c crit = new OSM_Criteria__c();
        crit = OSM_DataFactory.createCriteria('testName', acc.Id, critRecId);
        system.debug('test crit ' + crit);
        insert crit;
        
        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OSM_CriteriaAccordionExt extension = new OSM_CriteriaAccordionExt(sc);
        extension.criteriaId = crit.Id;
        extension.editPage();
    }
}