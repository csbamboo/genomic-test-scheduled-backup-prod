/*
 *Author: Michiel Patricia M. Robrigado
 *Date created: Feb 11, 2011
 *Controller for OSM_CriteriaRelatedPage
 */
public class OSM_CriteriaRelatedPageExt{
    //variables
    public List<OSM_Criteria__c> critList{get;set;}
    Id oliId;
    Set<Id> prodId = new Set<Id>();
    Set<Id> priceBkEntryId = new Set<Id>();
    
    public OSM_CriteriaRelatedPageExt(ApexPages.StandardController controller) {
        critList = new List<OSM_Criteria__c>();
        oliId = apexpages.currentpage().getParameters().get('id');
        system.debug('test oliId ' + oliId);
        for(OrderItem oli : [SELECT Id, PricebookEntryId FROM OrderItem WHERE Id =: oliId]){
            priceBkEntryId.add(oli.PricebookEntryId);
        }
        system.debug('test priceBkEntryId ' + priceBkEntryId);
        for(PricebookEntry pbE : [SELECT Id, Product2Id FROM PricebookEntry WHERE Id IN : priceBkEntryId]){
            prodId.add(pbE.Product2Id);
        }
        system.debug('test prodId ' + prodId);
        //critList = [SELECT Name, NCCN_Risk_Category__c, Gleason_Score__c, Clinical_Stage__c, Positive_Cores__c, Nodal_Status__c, ER_Status__c, Her_2__c FROM OSM_Criteria__c WHERE Product__c IN : prodId];
        critList = [SELECT Name  FROM OSM_Criteria__c WHERE Product__c IN : prodId];
        system.debug('test critList ' + critList);
        system.debug('test critList size ' + critList.size());
    }
}