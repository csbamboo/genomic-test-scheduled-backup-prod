@isTest (seeAllData = false)
private class OSM_CriteriaRelatedPageExt_Test {
    
    
    public static testMethod void OSM_CRPE_Test1() {   
        //Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        //insert recordTypeIds;
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Trigger__c = false;
        insert triggerSwitch;
        
        PageReference pageRef = Page.OSM_CriteriaRelatedPage;
        Test.setCurrentPage(pageRef);
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id critRecId = Schema.SObjectType.OSM_Criteria__c.getRecordTypeInfosByName().get('MMR').getRecordTypeId();
        Id accRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();

        Account acc = new Account();
        acc = OSM_DataFactory.createAccount('testName');
        acc.RecordTypeId = accRecId;
        insert acc;
        system.debug('test acc ' + acc.Id);

        List<Contact> contactList = new List<Contact>();
        Contact con = OSM_DataFactory.createContact('First','Last', PatientRecType);
        contactList.add(con);
        insert contactList;
        
        //insert the details from the data builder to criteria
        OSM_Criteria__c crit = new OSM_Criteria__c();
        crit = OSM_DataFactory.createCriteria('testName', acc.Id, critRecId);
        system.debug('test crit ' + crit);
        insert crit;
        
        Id pb2Standard = Test.getStandardPricebookId();
        
        List<Product2> prod2 = new List<Product2>();
        for(Integer a=0; a<10; a++){
            prod2.add(OSM_DataFactory.createProduct('test', true));
        }
        insert prod2;

        List<PricebookEntry> pbe = new List<PricebookEntry>();
        for(Integer a=0; a<10; a++){
            pbe.add(OSM_DataFactory.createPricebookEntry(prod2[a].Id, pb2Standard, a+1));
        }
        insert pbe;
        
         Product2 prod23 = OSM_DataFactory.createProduct('test', true);
        insert prod23;
        
        PricebookEntry pbe2 = OSM_DataFactory.createPricebookEntry(prod23.Id, pb2Standard, 7);
        insert pbe2;

        Order workOrder1 = OSM_DataFactory.createOrder('New Order 1', con.Id, acc.Id, System.Today(), 'Active');
        workOrder1.Pricebook2Id = pb2Standard;
        insert workOrder1;
        
        OrderItem orderItem1 = new OrderItem(OrderId = workOrder1.Id, PricebookEntryId = pbe2.Id, UnitPrice = 2, Quantity = 1);
        insert orderItem1;

        List<Order> orderList = new List<Order>();
        for(Integer a=0; a<20; a++){
            orderList.add(OSM_DataFactory.createOrder('ord'+a, con.Id, acc.Id, System.Today(), 'Active'));
            orderList[a].OSM_Bill_Account__c = acc.Id;
            orderList[a].Pricebook2Id = pb2Standard;
            orderList[a].OSM_Order__c = workOrder1.Id;
            orderList[a].OSM_Bill_Type__c = 'Bill Account';
        }
        System.debug('This order: '+ orderList);
       
         OrderItem orderItem8 = new OrderItem(OrderId = orderList[0].Id , PricebookEntryId = pbe[0].Id , UnitPrice = 2 , Quantity = 1, OSM_State__c = 'Active', OSM_Lab_and_Report_Status__c = 'Pre-Processing');
        //insert orderItem8;
       
        
        // Instantiate a new extension and the standard controller with all parameters in the page
        ApexPages.currentPage().getParameters().put('id',orderItem1.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(orderItem1); 
        OSM_CriteriaRelatedPageExt ab = new OSM_CriteriaRelatedPageExt(sc);
        
        
        
        
        //         // Instantiate a new extension and the standard controller with all parameters in the page
        // ApexPages.currentPage().getParameters().put('id',orderItem8.Id);
        // ApexPages.StandardController sc2 = new ApexPages.StandardController(orderItem8);
       
        
        Test.startTest();
            //ordItems[19].OSM_State__c = 'Failure'; 
            pbe[1].UnitPrice = 8;
            //pbe.UnitPrice = crit.OSM_List_Price__c;
            update pbe[1];
            //crit.OSM_Test_Name__c = 'Test';
            update crit;
        Test.stopTest();
        System.assert(pbe[1].UnitPrice == 8);
        //System.assert(crit.OSM_Test_Name__c == 'Test');

        
    } 

        
}