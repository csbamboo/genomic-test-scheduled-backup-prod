/**
 * File Info
 * ----------------------------------
 * @filename       OSM_CustomOrdersRelatedListCc_Test.cls
 * @author         Francis Nasalita
 * @description    Test class for OSM_CustomOrdersRelatedList_Controller. 
 * @history        12.JAN.2015 - Francis Nasalita - Created  
 */
@isTest
private class OSM_CustomOrdersRelatedListCc_Test {

    private static testMethod void testContactPatientOrderRList_notEmptyList() {
    
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
    
        /* START SETUP */
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;
        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;
        
        Id pricebookStandard = Test.getStandardPricebookId();
        
        //Order ords =  OSM_DataFactory.createOrders(OSM_Patient__c) ;
        List<Order> orderLists = new List<Order>();
        Order order1 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order1.Pricebook2Id = pricebookStandard;
        orderLists.add(order1);
        Order order2 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order2.Pricebook2Id = pricebookStandard;
        orderLists.add(order2);
        Order order3 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order3.Pricebook2Id = pricebookStandard;
        orderLists.add(order3);
        Order order4 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order4.Pricebook2Id = pricebookStandard;
        orderLists.add(order4);
        Order order5 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order5.Pricebook2Id = pricebookStandard;
        orderLists.add(order5);
        Order order6 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order6.Pricebook2Id = pricebookStandard;
        orderLists.add(order6);
        Order order7 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order7.Pricebook2Id = pricebookStandard;
        orderLists.add(order7);
        Order order8 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order8.Pricebook2Id = pricebookStandard;
        orderLists.add(order8);
        Order order9 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order9.Pricebook2Id = pricebookStandard;
        orderLists.add(order9);
        Order order10 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order10.Pricebook2Id = pricebookStandard;
        orderLists.add(order10);
        insert orderLists;
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + ctct.Id);
        /* END SETUP */
        
        /* START TESTING */
        Test.startTest();
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ctct);
        OSM_CustomOrdersRelatedList_Controller controller = new OSM_CustomOrdersRelatedList_Controller(sCon);
        controller.nextPage();
        controller.previousPage();
        controller.lastPage();
        controller.firstPage();
        Test.stopTest();
        /* END TESTING */
        
        /* START VERIFICATION */
        Id conID = ApexPages.CurrentPage().getparameters().get('id');
        orderLists = [SELECT RecordTypeId,OrderNumber, OSM_Work_Order_ID__c, OSM_Product__c, OSM_Ordering_HCP_Name__c, CreatedDate, Status, OSM_Multiple_Primaries__c, OSM_Triage_Outcome__c
                    FROM Order
                    Where OSM_Patient__c = :conID AND RecordTypeId = :rt];
        for(Order ord :orderLists) {
            System.assertEquals(rt,ord.RecordTypeId,'The record type of order must be Order.');
        }
        /* END VERIFICATION */
        
    }
    
    private static testMethod void testContactPatientOrderRList_EmptyList() {
        /* START SETUP */
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;
        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + ctct.Id);
        /* END SETUP */
        
        /* START TESTING */
        Test.startTest();
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ctct);
        OSM_CustomOrdersRelatedList_Controller controller = new OSM_CustomOrdersRelatedList_Controller(sCon);
        Test.stopTest();
        /* END TESTING */
        
        /* START VERIFICATION */
        Id conID = ApexPages.CurrentPage().getparameters().get('id');
        List<Order> orderLists = [SELECT RecordTypeId,OrderNumber, OSM_Work_Order_ID__c, OSM_Product__c, OSM_Ordering_HCP_Name__c, CreatedDate, Status, OSM_Multiple_Primaries__c, OSM_Triage_Outcome__c
                    FROM Order
                    Where OSM_Patient__c = :conID AND RecordTypeId = :rt];
        System.assert(orderLists.isEmpty());
        /* END VERIFICATION */
    }
    
    private static testMethod void testDeleteOrder() {
        /* START SETUP */
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;
        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;
        
        Id pricebookStandard = Test.getStandardPricebookId();
        
        Order order1 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order1.Pricebook2Id = pricebookStandard;
        insert order1;
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + ctct.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ctct);
        OSM_CustomOrdersRelatedList_Controller controller = new OSM_CustomOrdersRelatedList_Controller(sCon);
        controller.orderId = order1.Id;
        /* END SETUP */
        
        /* START TESTING */
        Test.startTest();
        controller.deleteRecord();
        Test.stopTest();
        /* END TESTING */
        
        /* START VERIFICATION */
        List<Order> orderCheck = [SELECT Id
                            FROM Order
                            WHERE Id = :order1.id];
        System.assert(orderCheck.isEmpty());
        /* END VERIFICATION */
    }
    
    private static testMethod void testDeleteOrder_PageOrderList() {
        /* START SETUP */
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;
        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;
        
        Id pricebookStandard = Test.getStandardPricebookId();
        
        List<Order> orderLists = new List<Order>();
        Order order1 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order1.Pricebook2Id = pricebookStandard;
        orderLists.add(order1);
        Order order2 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order2.Pricebook2Id = pricebookStandard;
        orderLists.add(order2);
        Order order3 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order3.Pricebook2Id = pricebookStandard;
        orderLists.add(order3);
        Order order4 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order4.Pricebook2Id = pricebookStandard;
        orderLists.add(order4);
        Order order5 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order5.Pricebook2Id = pricebookStandard;
        orderLists.add(order5);
        Order order6 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order6.Pricebook2Id = pricebookStandard;
        orderLists.add(order6);
        Order order7 = OSM_DataFactory.createOrderWithRecordType(ctct.Id,acct.Id,rt);
        order7.Pricebook2Id = pricebookStandard;
        orderLists.add(order7);
        insert orderLists;
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + ctct.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ctct);
        OSM_CustomOrdersRelatedList_Controller controller = new OSM_CustomOrdersRelatedList_Controller(sCon);
        controller.orderId = order1.Id;
        /* END SETUP */
        
        /* START TESTING */
        Test.startTest();
        controller.deleteRecord();
        Test.stopTest();
        /* END TESTING */
        
        /* START VERIFICATION */
        List<Order> orderCheck = [SELECT Id
                            FROM Order
                            WHERE Id = :order1.id];
        System.assert(orderCheck.isEmpty());
        /* END VERIFICATION */
    }
    
    private static testMethod void testNewButton() {
        /* START SETUP */
        Record_Type__c mc = Record_Type__c.getOrgDefaults();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        insert ctct;
                
        String recordTypeId = mc.OSM_RT_Order_Order__c != null ? mc.OSM_RT_Order_Order__c : '';
        String firstName = ctct.FirstName != null ? ctct.FirstName : '';
        String initials = ctct.OSM_Patient_Initials__c != null ? ctct.OSM_Patient_Initials__c : '';
        String middleName = ctct.OSM_Middle_Name__c != null ? ctct.OSM_Middle_Name__c : '';
        String dob = String.valueOf(ctct.Birthdate) != null ? String.valueOf(ctct.Birthdate.format()) : '';
        String lastName = ctct.LastName != null ? ctct.LastName : '';
        String gender = ctct.OSM_Gender__c != null ? ctct.OSM_Gender__c : '';
        String street = ctct.MailingStreet != null ? ctct.MailingStreet : '';
        String city = ctct.MailingCity != null ? ctct.MailingCity : '';
        String state = ctct.MailingState != null ? ctct.MailingState : '';
        String country = ctct.MailingCountry != null ? ctct.MailingCountry : '';
        String name = ctct.Name != null ? ctct.Name : '';
        String id = ctct.Id != null ? ctct.Id : '';
        
        String url = '/801/e?RecordType=' + recordTypeId + '&'
                     + mc.OSM_CF_Order_Patient_First_Name__c + '=' + firstName + '&'
                     + mc.OSM_CF_Order_Patient_Initials__c + '=' + initials + '&'
                     + mc.OSM_CF_Order_Patient_Middle_Name__c + '=' + middleName + '&'
                     + mc.OSM_CF_Order_Patient_DOB__c + '=' + dob + '&'
                     + mc.OSM_CF_Order_Patient_Last_Name__c + '=' + lastName + '&'
                     + mc.OSM_CF_Order_Patient_Gender__c + '=' + gender + '&'
                     + mc.OSM_CF_Order_Patient_Street__c + '=' + street + '&'
                     + mc.OSM_CF_Order_Patient_City__c + '=' + city + '&'
                     + mc.OSM_CF_Order_Patient_State__c + '=' + state + '&'
                     + mc.OSM_CF_Order_Patient_Country__c + '=' + country + '&ent=Order&'
                     + 'CF' + mc.CF_Order_Patient__c + '=' + name + '&'
                     + 'CF' + mc.CF_Order_Patient__c + '_lkid=' + id + '&'
                     + 'retURL=/' + id + '&'
                     + 'save_new_url=%2F801%2Fe%3FretURL%3D%2F' + id; 
                     
        ctct = [Select Id, FirstName,OSM_Patient_Initials__c, OSM_Middle_Name__c, Birthdate, LastName, OSM_Gender__c, MailingStreet, MailingCity, MailingState, MailingCountry, Name
                FROM Contact
                WHERE (Id = :ctct.Id)];
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + ctct.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ctct);
        OSM_CustomOrdersRelatedList_Controller controller = new OSM_CustomOrdersRelatedList_Controller(sCon);
        /* END SETUP */
        
        /* START TESTING */
        Test.startTest();
        PageReference pageReference = controller.pageref();
        Test.stopTest();
        /* END TESTING */
    }

}