/**
 * File Info
 * ----------------------------------
 * @filename       OSM_CustomOrdersRelatedList_Controller.cls
 * @author         Francis Nasalita
 * @description    Controller class for OSM_CustomOrdersRelatedList Visualforce page. 
 * @history        12.JAN.2015 - Francis Nasalita - Created  
 */

public class OSM_CustomOrdersRelatedList_Controller {
    ID conID;
    Map<ID,RecordType> rt_Map;
    public List<Order> orderList {get; set;}
    private static Record_Type__c currentSettings = null;
    public Record_Type__c mc;
    public Contact ctct;
    public Boolean hasRecords {get; set;}
    
    //Pagination Variables
    public List<Order> pagedorderList {get;set;}
    public Integer totalPage {get;set;}
    public Integer currentPage {get;set;}
    public Integer currentRecord {get;set;}
    public Integer pageSize {get;set;}
    public Boolean hasNext {get;set;}
    public Boolean hasPrevious {get;set;}
    
    /**
     * @author         Francis Nasalita
     * @description    Constructor. 
     * @history        12.JAN.2015 - Francis Nasalita - Created
     * @parameters     Contact Object
     */
    public OSM_CustomOrdersRelatedList_Controller(ApexPages.StandardController controller) {
        pageSize = 5;
        currentPage = 1;
        currentRecord = 1;
        hasNext = true;
        hasPrevious = false;
        pagedorderList = new List<Order>();

        mc = Record_Type__c.getOrgDefaults();
        RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType='Order' AND Name='Order' LIMIT 1];
        conID = ApexPages.CurrentPage().getparameters().get('id');
        ctct = [Select Id, FirstName,OSM_Patient_Initials__c, OSM_Middle_Name__c, Birthdate, LastName, OSM_Gender__c, MailingStreet, MailingCity, MailingState, MailingCountry, Name
                FROM Contact
                WHERE (Id = :conID)];
        orderList = [SELECT OrderNumber, OSM_Work_Order_ID__c, OSM_Product__c, OSM_Ordering_HCP_Name__c, CreatedDate, Status, OSM_Multiple_Primaries__c, OSM_Triage_Outcome__c
                    FROM Order
                    Where OSM_Patient__c = :this.conID AND RecordTypeId = :rt.Id];
        
        if(!orderList.isEmpty()) {
            hasRecords = true;
        } else {
            hasRecords = false;
        }
        
        for(Integer counter = 0; counter < (orderList.size() < pageSize ? orderList.size() : pageSize); counter++){
            pagedorderList.add(orderList[counter]);
        }
                    
        if(pagedorderList.size() < pageSize){
            hasNext = false;
        }
        totalPage = (Integer)Math.ceil((Double)orderList.size()/pageSize);
    }
    
    /**
     * @author         Francis Nasalita
     * @description    Button for custom order related list - New Order button. 
     * @history        12.JAN.2015 - Francis Nasalita - Created
     */
    public PageReference pageref() {
        String recordTypeId = mc.OSM_RT_Order_Order__c != null ? mc.OSM_RT_Order_Order__c : '';
        String firstName = ctct.FirstName != null ? ctct.FirstName : '';
        String initials = ctct.OSM_Patient_Initials__c != null ? ctct.OSM_Patient_Initials__c : '';
        String middleName = ctct.OSM_Middle_Name__c != null ? ctct.OSM_Middle_Name__c : '';
        String dob = String.valueOf(ctct.Birthdate) != null ? String.valueOf(ctct.Birthdate.format()) : '';
        String lastName = ctct.LastName != null ? ctct.LastName : '';
        String gender = ctct.OSM_Gender__c != null ? ctct.OSM_Gender__c : '';
        String street = ctct.MailingStreet != null ? ctct.MailingStreet : '';
        String city = ctct.MailingCity != null ? ctct.MailingCity : '';
        String state = ctct.MailingState != null ? ctct.MailingState : '';
        String country = ctct.MailingCountry != null ? ctct.MailingCountry : '';
        String name = ctct.Name != null ? ctct.Name : '';
        String id = ctct.Id != null ? ctct.Id : '';
        
        String url = '/801/e?RecordType=' + recordTypeId + '&'
                     + mc.OSM_CF_Order_Patient_First_Name__c + '=' + firstName + '&'
                     + mc.OSM_CF_Order_Patient_Initials__c + '=' + initials + '&'
                     + mc.OSM_CF_Order_Patient_Middle_Name__c + '=' + middleName + '&'
                     + mc.OSM_CF_Order_Patient_DOB__c + '=' + dob + '&'
                     + mc.OSM_CF_Order_Patient_Last_Name__c + '=' + lastName + '&'
                     + mc.OSM_CF_Order_Patient_Gender__c + '=' + gender + '&'
                     + mc.OSM_CF_Order_Patient_Street__c + '=' + street + '&'
                     + mc.OSM_CF_Order_Patient_City__c + '=' + city + '&'
                     + mc.OSM_CF_Order_Patient_State__c + '=' + state + '&'
                     + mc.OSM_CF_Order_Patient_Country__c + '=' + country + '&ent=Order&'
                     + 'CF' + mc.CF_Order_Patient__c + '=' + name + '&'
                     + 'CF' + mc.CF_Order_Patient__c + '_lkid=' + id + '&'
                     + 'retURL=/' + id + '&'
                     + 'save_new_url=%2F801%2Fe%3FretURL%3D%2F' + id;
                    
        PageReference pgRef = new PageReference(url);
        pgRef.setRedirect(true);
        return pgRef;
    }
    
    /**
     * @author         Francis Nasalita
     * @description    Button for deleting orders. 
     * @history        12.JAN.2015 - Francis Nasalita - Created
     */
    public Id orderId {get;set;}
    public PageReference deleteRecord() {
        delete [SELECT Id FROM Order WHERE Id = :orderId];
        /*
        pageSize = 5;
        currentPage = 1;
        currentRecord = 1;
        hasNext = true;
        hasPrevious = false;
        pagedorderList = new List<Order>();
        RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType='Order' AND Name='Order' LIMIT 1];
        orderList = [SELECT OrderNumber, OSM_Work_Order_ID__c, OSM_Product__c, OSM_Ordering_HCP_Name__c, CreatedDate, Status, OSM_Multiple_Primaries__c, OSM_Triage_Outcome__c
                    FROM Order
                    Where OSM_Patient__c = :this.conID AND RecordTypeId = :rt.Id];
                    
        for(Integer counter = 0; counter < (orderList.size() < pageSize ? orderList.size() : pageSize); counter++){
            pagedorderList.add(orderList[counter]);
        }
                    
        if(pagedorderList.size() < pageSize){
            hasNext = false;
        }
        totalPage = (Integer)Math.ceil((Double)orderList.size()/pageSize);
        */
        PageReference acctPage = Page.OSM_Patient_Redirect;
        acctPage.getParameters().put('id', conID);
        acctPage.getParameters().put('sfdc.override', '1');
        acctPage.setRedirect(true);
        return acctPage;
    }
    
    /**
     * @author         Francis Nasalita
     * @description    pagination. 
     * @history        12.JAN.2015 - Francis Nasalita - Created
     */
    public PageReference nextPage(){
        currentPage += 1;
        currentRecord += pageSize;
        pagedorderList.clear();
        for(Integer counter = currentRecord; counter < (orderList.size()+1 < (currentRecord + pageSize) ? orderList.size()+1 : (currentRecord + pageSize)); counter++){
            pagedorderList.add(orderList[counter-1]);
        }
        hasPrevious = true;
        if(currentPage == (Integer)Math.ceil((Double)orderList.size()/pageSize)){
            hasNext = false;
        }
        return null;
    }
    
    /**
     * @author         Francis Nasalita
     * @description    pagination. 
     * @history        12.JAN.2015 - Francis Nasalita - Created
     */
    public PageReference previousPage(){
        currentPage -= 1;
        currentRecord -= pageSize;
        pagedorderList.clear();
        for(Integer counter = currentRecord; counter < (orderList.size() < (currentRecord + pageSize) ? orderList.size() : (currentRecord + pageSize)); counter++){
            pagedorderList.add(orderList[counter-1]);
        }
        hasNext = true;
        if(currentPage == 1){
            hasPrevious = false;
        }
        return null;
    }
    
    /**
     * @author         Francis Nasalita
     * @description    pagination. 
     * @history        12.JAN.2015 - Francis Nasalita - Created
     */
    public PageReference lastPage(){
        currentPage = (Integer)Math.ceil((Double)orderList.size()/pageSize) - 1;
        currentRecord = currentPage * pageSize + 1;
        pagedorderList.clear();
        for(Integer counter = currentRecord; counter < (orderList.size()+1 < (currentRecord + pageSize) ? orderList.size()+1 : (currentRecord + pageSize)); counter++){
            pagedorderList.add(orderList[counter-1]);
        }
        hasPrevious = true;
        currentPage = (Integer)Math.ceil((Double)orderList.size()/pageSize);
        if(currentPage == (Integer)Math.ceil((Double)orderList.size()/pageSize)){
            hasNext = false;
        }
        return null;
    }
    
    /**
     * @author         Francis Nasalita
     * @description    pagination. 
     * @history        12.JAN.2015 - Francis Nasalita - Created
     */
    public PageReference firstPage(){
        currentPage = 1;
        currentRecord = 1;
        pagedorderList.clear();
        for(Integer counter = 0; counter < pageSize; counter++){
            pagedorderList.add(orderList[counter]);
        }
        hasNext = true;
        if(currentPage == 1){
            hasPrevious = false;
        }
        return null;
    }
    
     /**
     * @author         Patrick Lorilla
     * @description    Update order count value. 
     * @history        12.DEC.2014 - Patrick Lorilla - Created  
     */
    public PageReference updateCounter(){
        System.debug('\n\n\n updCtrChk1');
        if(conID  != null && orderList != null){
            Contact con = [SELECT OSM_Order_Count__c from Contact where ID =: conID ];
            System.debug('\n\n\n updCtrChk2');
            if(con != null){
                System.debug('\n\n\n updCtrChk2');
                if(con.OSM_Order_Count__c != orderList.size()){
                    System.debug('\n\n\n updCtrChk3');
                    con.OSM_Order_Count__c  = orderList.size();
                    update con;
                    
                    PageReference pgRef = new PageReference('/'+conID);
                    pgRef.setRedirect(true);
                    return pgRef;
                }
            }
        }
        return null;
    }
}