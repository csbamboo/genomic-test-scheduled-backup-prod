/**
     * @author         Patrick Lorilla
     * @description    OSM Custom OT Related List Extension
     * @history        26.JAN.2015 - Patrick Lorilla - Created
     */
public class OSM_Custom_OR_Related_ListExt {
    ID ordID;
    Map<ID,RecordType> rt_Map;
    public List<OSM_Order_Role__c> ordRoleList {get; set;}
    private static Record_Type__c currentSettings = null;
    public Record_Type__c mc;
    public Order ord;
    public Boolean hasRecords {get; set;}
    public Boolean hasTSRARecords {get; set;}
    
    //Pagination Variables
    public List<OSM_Order_Role__c> pagedorderRoleList {get;set;}
    public Integer totalPage {get;set;}
    public Integer currentPage {get;set;}
    public Integer currentRecord {get;set;}
    public Integer pageSize {get;set;}
    public Boolean hasNext {get;set;}
    public Boolean hasPrevious {get;set;}
    public List<OSM_Order_Role__c> oTAList;
    
     /**
     * @author         Patrick Lorilla
     * @description    Constructor
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public OSM_Custom_OR_Related_ListExt (ApexPages.StandardController controller) {
        ordRoleList = new List<OSM_Order_Role__c>();
        Set<ID> terrIDS = new Set<ID>();
        pageSize = 5;
        currentPage = 1;
        currentRecord = 1;
        hasNext = true;
        hasPrevious = false;
        pagedorderRoleList = new List<OSM_Order_Role__c>();

        mc = Record_Type__c.getOrgDefaults();
       
        ordID = ApexPages.CurrentPage().getparameters().get('id');
        ord = [SELECT Id, Name,OrderNumber from order where ID =: this.ordID];
        ordRoleList = [SELECT OSM_Role__c, OSM_Account_Name__c, OSM_Account_Postal_Address__c, OSM_Account_Phone__c, OSM_Contact_Full_Name__c, OSM_Contact__r.MailingPostalCode, OSM_Contact__r.Phone, CreatedDate, Id
                    FROM OSM_Order_Role__c Where OSM_Order__c = :this.ordID AND OSM_Account__c != null AND OSM_Role__c = 'Specimen Submitting' ];
 
        if(!ordRoleList.isEmpty()) {
            hasRecords = true;
     
        } 
        System.debug('>>>>> ordRoleList: ' + ordRoleList.isEmpty());
        if(!ordRoleList.isEmpty()){
            hasRecords = true;
        }
        else {
            hasRecords = false;
        }
        
        for(Integer counter = 0; counter < (ordRoleList.size() < pageSize ? ordRoleList.size() : pageSize); counter++){
            pagedorderRoleList.add(ordRoleList [counter]);
        }
        
             
        if(pagedorderRoleList .size() <= pageSize){
            hasNext = false;
        }
        
        if(ordRoleList.size() > pagedorderRoleList.size()){
            hasNext = true;
        }
        
        totalPage = (Integer)Math.ceil((Double)ordRoleList .size()/pageSize);
        System.debug('/n/n/n ORDEROBJECT:'+ord);
    }
    
     /**
     * @author         Patrick Lorilla
     * @description    create OTA
     * @history        26.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference pageref() {
        
        //String orderNumber= ord.OrderNumber!= null ? ord .OrderNumber: '';
        //String orderRoleId= ord .ID!= null ? ord .ID: '';
        Record_Type__c cs = Record_Type__c.getOrgDefaults();
        //String url = '/'+cs.ObjectId_Order_Territory_Assignment__c+'/e?CF'+cs.CF_OTerritoryAssignment_Order_Name__c+'='+orderNumber+'&CF'+cs.CF_OTerritoryAssignment_Order_Name__c+'_lkid='+orderId+'&'+cs.CF_OTerritoryAssignment_Manual__c+'=0&retURL='+orderId +'&'
                 //  + 'save_new_url=%2F801%2Fe%3FretURL%3D%2F' + orderId;
     
        /*           
        PageReference pgRef = new PageReference(url);
        pgRef.setRedirect(true);
        return pgRef;*/
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new OSM_Order_Role__c());
        OSM_ManageOrderRoleExt mordExt = new OSM_ManageOrderRoleExt (sc);
        mordExt.orderId =  ord.Id;
        mordExt.orderName = ord.OrderNumber;
        return mordExt.createOrderRole();
    }
    public Id ordRolesId{get;set;}
      /**
     * @author         Patrick Lorilla
     * @description    delete Record
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
     
    public PageReference deleteRecord() {
      
        delete [SELECT Id FROM OSM_Order_Role__c WHERE Id = :ordRolesId];
        pageSize = 5;
        currentPage = 1;
        currentRecord = 1;
        hasNext = true;
        hasPrevious = false;
        pagedorderRoleList = new List<OSM_Order_Role__c>();
        //RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType='Order' AND Name='Order' LIMIT 1];
        ordRoleList =  [SELECT OSM_Role__c, OSM_Account_Name__c, OSM_Account_Postal_Address__c, OSM_Account_Phone__c, OSM_Contact_Full_Name__c, OSM_Contact__r.MailingPostalCode, OSM_Contact__r.Phone, CreatedDate, Id
                    FROM OSM_Order_Role__c Where OSM_Order__c = :this.ordID];
          
        for(Integer counter = 0; counter < (ordRoleList.size() < pageSize ? ordRoleList .size() : pageSize); counter++){
            pagedorderRoleList.add(ordRoleList [counter]);
        }
                    
        if(pagedorderRoleList.size() < pageSize){
            hasNext = false;
        }
        totalPage = (Integer)Math.ceil((Double)ordRoleList.size()/pageSize);
        
        PageReference pgRef = new PageReference('/'+ordID);
        pgRef.setRedirect(true);
        return pgRef;

    }
  
    /**
     * @author         Patrick Lorilla
     * @description    pagination. 
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference nextPage(){
        currentPage += 1;
        currentRecord += pageSize;
        pagedorderRoleList.clear();
        for(Integer counter = currentRecord; counter < (ordRoleList.size()+1 < (currentRecord + pageSize) ? ordRoleList.size()+1 : (currentRecord + pageSize)); counter++){
            pagedorderRoleList.add(ordRoleList[counter-1]);
        }
        hasPrevious = true;
        if(currentPage == (Integer)Math.ceil((Double)ordRoleList.size()/pageSize)){
            hasNext = false;
        }
        return null;
    }
    
    /**
     * @author         Patrick Lorilla
     * @description    pagination. 
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference previousPage(){
        currentPage -= 1;
        currentRecord -= pageSize;
        pagedorderRoleList.clear();
        for(Integer counter = currentRecord; counter < (ordRoleList.size() < (currentRecord + pageSize) ? ordRoleList.size() : (currentRecord + pageSize)); counter++){
            pagedorderRoleList.add(ordRoleList[counter-1]);
        }
        hasNext = true;
        if(currentPage == 1){
            hasPrevious = false;
        }
        return null;
    }
    
    /**
     * @author         Patrick Lorilla
     * @description    pagination. 
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference lastPage(){
        currentPage = (Integer)Math.ceil((Double)ordRoleList.size()/pageSize) - 1;
        currentRecord = currentPage * pageSize + 1;
        pagedorderRoleList.clear();
        for(Integer counter = currentRecord; counter < (ordRoleList.size()+1 < (currentRecord + pageSize) ? ordRoleList.size()+1 : (currentRecord + pageSize)); counter++){
            pagedorderRoleList.add(ordRoleList[counter-1]);
        }
        hasPrevious = true;
        currentPage = (Integer)Math.ceil((Double)ordRoleList.size()/pageSize);
        if(currentPage == (Integer)Math.ceil((Double)ordRoleList.size()/pageSize)){
            hasNext = false;
        }
        return null;
    }
    
    /**
     * @author         Patrick Lorilla
     * @description    pagination. 
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference firstPage(){
        currentPage = 1;
        currentRecord = 1;
        pagedorderRoleList.clear();
        for(Integer counter = 0; counter < pageSize; counter++){
            pagedorderRoleList.add(ordRoleList[counter]);
        }
        hasNext = true;
        if(currentPage == 1){
            hasPrevious = false;
        }
        return null;
    }
}