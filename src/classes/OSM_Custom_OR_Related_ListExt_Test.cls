/**
 * File Info
 * ----------------------------------
 * @filename       OSM_Custom_OR_Related_ListExt_Test.cls
 * @author         Francis Nasalita
 * @description    Test class for OSM_Custom_OR_Related_ListExt. 
 * @history        27.JAN.2015 - Francis Nasalita - Created  
 */
@isTest
private class OSM_Custom_OR_Related_ListExt_Test {

    private static testMethod void TestNewOrderRole() {
        /* START SETUP */        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id rtContact_HCP = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id orderRoleAccCon = Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Account & Contact').getRecordTypeId();

        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContactId);
        ctct.AccountId = acct.Id;
        insert ctct;
        
        Order order1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        //order1.RecordTypeId = recTypeOrdrId;
        order1.OSM_Credit_Card__c = true;
        order1.OSM_Bill_Type__c = 'Patient Pre-Pay';
        insert order1;

        Contact ctct1 = OSM_DataFactory.createContact('Francis','Nasalita',rtContact_HCP);
        ctct1.AccountId = acct.Id;
        ctct1.OSM_Status__c = 'Approved';
        ctct1.Salutation = 'Mr.';
        ctct1.MailingCountry = 'PH';
        ctct1.OSM_Specialty__c = 'Surgeon';
        insert ctct1;
        
        PageReference pageRef = new PageReference('/apex/OSM_OrderPaymentMode?id=' + order1.Id + '&paymentMode=credit');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(order1);
        OSM_Custom_OR_Related_ListExt controller = new OSM_Custom_OR_Related_ListExt(sCon);
        /* END SETUP */
        
        /* START TEST */
        Test.startTest();
            OSM_Order_Role__c orderRole = OSM_Datafactory.createOrderRole(order1.Id, acct.Id, ctct1.Id, 'Specimen Submitting', orderRoleAccCon);
            insert orderRole;
            controller.ordRolesId = orderRole.Id;
            controller.deleteRecord();

            PageReference pagRef = controller.pageref();
        Test.stopTest();
        /* END TEST */
        
        /* START VERIFICATION*/
        System.assert(pagRef != null);
        /* END VERIFICATION*/
    }

    private static testMethod void TestDisplayOrderRolesAndDeleteOrderRole() {
         /* START SETUP */
        Id rtContact_HCP = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id rtContact_Patient = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id orderRoleAccCon = Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Account & Contact').getRecordTypeId();
        
        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct1 = OSM_DataFactory.createContact('Francis','Nasalita',rtContact_HCP);
        ctct1.AccountId = acct.Id;
        ctct1.OSM_Status__c = 'Approved';
        ctct1.Salutation = 'Mr.';
        ctct1.MailingCountry = 'PH';
        ctct1.OSM_Specialty__c = 'Surgeon';
        insert ctct1;

        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact_Patient);
        ctct.AccountId = acct.Id;
        insert ctct;
        
        Order order1 = new Order();
        order1.OSM_Patient__c = ctct.Id;
        order1.AccountId = acct.Id;
        order1.EffectiveDate = system.today();
        order1.Status = 'New';
        insert order1;
        
        List<OSM_Order_Role__c> orderRoleList = new List<OSM_Order_Role__c>();
        for(Integer i = 0; i < 10; i++) {
            OSM_Order_Role__c orderRole = OSM_Datafactory.createOrderRole(order1.Id, acct.Id, ctct1.Id, 'Specimen Submitting', orderRoleAccCon);
            orderRoleList.add(orderRole);
        }
        insert orderRoleList;
        
        /* END SETUP */
        
        /* START TEST */
        Test.startTest();
            PageReference pageRef = new PageReference('https://c.cs9.visual.force.com/apex/OSM_OrderPaymentMode?id=' + order1.Id + '&paymentMode=credit');
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sCon = new ApexPages.StandardController(order1);
            OSM_Custom_OR_Related_ListExt controller = new OSM_Custom_OR_Related_ListExt(sCon); 

            controller.nextPage();
            controller.previousPage();
            controller.lastPage();
            controller.firstPage();

            controller.ordRolesId = orderRoleList[0].Id;
            controller.deleteRecord();
        Test.stopTest();
        /* END TEST */

        /* START VERIFICATION*/
        PageReference pageRef1 = new PageReference('https://c.cs9.visual.force.com/apex/OSM_OrderPaymentMode?id=' + order1.Id + '&paymentMode=credit');
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController sCon1 = new ApexPages.StandardController(order1);
        OSM_Custom_OR_Related_ListExt controller1 = new OSM_Custom_OR_Related_ListExt(sCon1); 
        System.assert(controller.hasRecords);
        /* END VERIFICATION*/        
    }

}