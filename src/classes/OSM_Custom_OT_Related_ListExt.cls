/**
     * @author         Patrick Lorilla
     * @description    OSM Custom OT Related List Extension
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
public class OSM_Custom_OT_Related_ListExt {
    ID ordID;
    Map<ID,RecordType> rt_Map;
    public List<oTAWrapper> oTAWrapperList {get; set;}
    private static Record_Type__c currentSettings = null;
    public Record_Type__c mc;
    public Order ord;
    public Boolean hasRecords {get; set;}
    public Boolean hasTSRARecords {get; set;}
    
    //Pagination Variables
    public List<oTAWrapper> pagedorderList {get;set;}
    public Integer totalPage {get;set;}
    public Integer currentPage {get;set;}
    public Integer currentRecord {get;set;}
    public Integer pageSize {get;set;}
    public Boolean hasNext {get;set;}
    public Boolean hasPrevious {get;set;}
    public List<OSM_Order_Territory_Assignment__c> oTAList;
    /**
     * @author         Patrick Lorilla
     * @description    Constructor
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public OSM_Custom_OT_Related_ListExt (ApexPages.StandardController controller) {
        oTAList = new List<OSM_Order_Territory_Assignment__c>();
        Set<ID> terrIDS = new Set<ID>();
       
        pageSize = 5;
        currentPage = 1;
        currentRecord = 1;
        hasNext = true;
        hasPrevious = false;
        pagedorderList = new List<oTAWrapper>();

        mc = Record_Type__c.getOrgDefaults();
       
        ordID = ApexPages.CurrentPage().getparameters().get('id');
        ord = [Select Id, OrderNumber
                FROM Order
                WHERE (Id = :ordID)];
        oTAList = [SELECT Name, OSM_Manual_Assignment__c, OSM_Partner_Account__c,OSM_Partner_Account_Id__c,OSM_Territory_Name__c, OSM_Territory_Name__r.Name, OSM_Territory_ID__c, OSM_Override__c, OSM_Overrided_by__c, CreatedDate, Id
                    FROM OSM_Order_Territory_Assignment__c Where OSM_OrderName__c= :this.ordID];
        oTAWrapperList  = new List<oTAWrapper>();
        System.debug('\n\n\n oTAList ' +oTAList );
        List<OSM_Territory_Sales_Rep_Assignment__c > salesRepAll = new List<OSM_Territory_Sales_Rep_Assignment__c >();

        if(!oTAList .isEmpty()) {
            hasRecords = true;
             for(OSM_Order_Territory_Assignment__c ota: oTAList){
                if(!ota.OSM_Override__c){
                    terrIDS.add(ota.OSM_Territory_Name__c);
                }
                
             }
             System.debug('\n\n\n TERRIDSALL' +terrIDS);
             //patrick l 08.12.2015 added filter to exclude inactive users
             for(OSM_Territory_Sales_Rep_Assignment__c tsrp: [Select OSM_Sales_Rep__c, OSM_Territory_ID__c, OSM_Sales_Rep__r.Name  from OSM_Territory_Sales_Rep_Assignment__c where OSM_Inactive__c = false AND OSM_Territory_ID__c IN: terrIDS  AND OSM_Sales_Rep__r.IsActive = true ]){
                 
                 salesRepAll.add(tsrp);
             }
             System.debug('\n\n\n SALESREPALL' +salesRepAll);
             for(OSM_Order_Territory_Assignment__c ota: oTAList){
                 if(!salesRepAll.isEmpty()){
                    hasTSRARecords = true;
                     List<OSM_Territory_Sales_Rep_Assignment__c > srUsersList = new List<OSM_Territory_Sales_Rep_Assignment__c >();
                     for(OSM_Territory_Sales_Rep_Assignment__c srep: salesRepAll){ 

                         if(ota.OSM_Territory_Name__c == srep.OSM_Territory_ID__c){
                             System.debug('\n\n\n abc');
                             srUsersList.add(srep);
                          }
                     }
                     oTAWrapperList.add(new oTAWrapper(ota,srUsersList));
                  }
                  else{
                      oTAWrapperList.add(new oTAWrapper(ota,null));
                      hasTSRARecords = false;
                  }
             }
            
        } 
        if(!oTAWrapperList.isEmpty()){
            hasRecords = true;
        }
        else {
            hasRecords = false;
        }
        
        for(Integer counter = 0; counter < (oTAWrapperList.size() < pageSize ? oTAWrapperList.size() : pageSize); counter++){
            pagedorderList.add(oTAWrapperList[counter]);
        }
        
        
        System.debug('\n\n\n PAGESIZE:'+pageSize);
        System.debug('\n\n\n PAGEORDERLISTSIZE:'+pagedorderList.size());            
        if(pagedorderList.size() <= pageSize){
            hasNext = false;
        }
        
        if(oTAWrapperList.size() > pagedorderList.size()){
            hasNext = true;
        }
        
        totalPage = (Integer)Math.ceil((Double)oTAWrapperList.size()/pageSize);
        
        System.debug('\n\n\n hasNext??:'+hasNext  );           
    }
    /**
     * @author         Patrick Lorilla
     * @description    create OTA
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference pageref() {
        
        String orderNumber= ord.OrderNumber!= null ? ord .OrderNumber: '';
        String orderId= ord .ID!= null ? ord .ID: '';
        Record_Type__c cs = Record_Type__c.getOrgDefaults();
        String url = '/'+cs.ObjectId_Order_Territory_Assignment__c+'/e?CF'+cs.CF_OTerritoryAssignment_Order_Name__c+'='+orderNumber+'&CF'+cs.CF_OTerritoryAssignment_Order_Name__c+'_lkid='+orderId+'&'+cs.CF_OTerritoryAssignment_Manual__c+'=0&retURL='+orderId +'&'
                   + 'save_new_url=%2F801%2Fe%3FretURL%3D%2F' + orderId;
     
                    
        PageReference pgRef = new PageReference(url);
        pgRef.setRedirect(true);
        return pgRef;

        //return null;
    }
    /**
     * @author         Patrick Lorilla
     * @description    create manual OTA
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
     public PageReference pagerefManual() {
        
        String orderNumber= ord.OrderNumber!= null ? ord .OrderNumber: '';
        String orderId= ord .ID!= null ? ord .ID: '';
        Record_Type__c cs = Record_Type__c.getOrgDefaults();
        String url = '/'+cs.ObjectId_Order_Territory_Assignment__c+'/e?CF'+cs.CF_OTerritoryAssignment_Order_Name__c+'='+orderNumber+'&CF'+cs.CF_OTerritoryAssignment_Order_Name__c+'_lkid='+orderId+'&'+cs.CF_OTerritoryAssignment_Manual__c+'=1&retURL='+orderId +'&'
                   + 'save_new_url=%2F801%2Fe%3FretURL%3D%2F' + orderId;
     
                    
        PageReference pgRef = new PageReference(url);
        pgRef.setRedirect(true);
        return pgRef;

        //return null;
    }
    public Id srId {get; set;}
    public PageReference gotoSRUser(){
        PageReference pgRef = new PageReference('/'+srId );
        pgRef.setRedirect(true);
        return pgRef;

        //return null;
    }
  
    public Id oTAId {get;set;}
    /**
     * @author         Patrick Lorilla
     * @description    delete Record
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference deleteRecord() {
      
        delete [SELECT Id FROM OSM_Order_Territory_Assignment__c WHERE Id = :oTAId];
        pageSize = 5;
        currentPage = 1;
        currentRecord = 1;
        hasNext = true;
        hasPrevious = false;
        pagedorderList = new List<OTAWrapper>();
        oTAList = [SELECT OSM_Territory_Name__c, OSM_Partner_Account__c,OSM_Partner_Account_Id__c,OSM_Territory_ID__c, OSM_Override__c, OSM_Overrided_by__c, CreatedDate, Id
                    FROM OSM_Order_Territory_Assignment__c Where OSM_OrderName__c= :this.ordID];
                    
        for(Integer counter = 0; counter < (oTAWrapperList.size() < pageSize ? oTAWrapperList.size() : pageSize); counter++){
            pagedorderList.add(oTAWrapperList[counter]);
        }
                    
        if(pagedorderList.size() < pageSize){
            hasNext = false;
        }
        totalPage = (Integer)Math.ceil((Double)oTAList.size()/pageSize);
        
        PageReference pgRef = new PageReference('/'+ordID);
        pgRef.setRedirect(true);
        return pgRef;
        

         
    }
    /**
     * @author         Patrick Lorilla
     * @description    pagination. 
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference nextPage(){
        currentPage += 1;
        currentRecord += pageSize;
        pagedorderList  .clear();
        for(Integer counter = currentRecord; counter < (oTAWrapperList .size()+1 < (currentRecord + pageSize) ? oTAWrapperList .size()+1 : (currentRecord + pageSize)); counter++){
            pagedorderList  .add(oTAWrapperList [counter-1]);
        }
        hasPrevious = true;
        if(currentPage == (Integer)Math.ceil((Double)oTAWrapperList .size()/pageSize)){
            hasNext = false;
        }
        return null;
    }
    
    /**
     * @author         Patrick Lorilla
     * @description    pagination. 
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference previousPage(){
        currentPage -= 1;
        currentRecord -= pageSize;
        pagedorderList.clear();
        for(Integer counter = currentRecord; counter < (oTAWrapperList .size() < (currentRecord + pageSize) ? oTAWrapperList .size() : (currentRecord + pageSize)); counter++){
            pagedorderList  .add(oTAWrapperList [counter-1]);
        }
        hasNext = true;
        if(currentPage == 1){
            hasPrevious = false;
        }
        return null;
    }
    
    /**
     * @author         Patrick Lorilla
     * @description    pagination. 
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference lastPage(){
        currentPage = (Integer)Math.ceil((Double)oTAWrapperList .size()/pageSize) - 1;
        currentRecord = currentPage * pageSize + 1;
        pagedorderList  .clear();
        for(Integer counter = currentRecord; counter < (oTAWrapperList .size()+1 < (currentRecord + pageSize) ? oTAWrapperList .size()+1 : (currentRecord + pageSize)); counter++){
            pagedorderList  .add(oTAWrapperList [counter-1]);
        }
        hasPrevious = true;
        currentPage = (Integer)Math.ceil((Double)oTAWrapperList .size()/pageSize);
        if(currentPage == (Integer)Math.ceil((Double)oTAWrapperList .size()/pageSize)){
            hasNext = false;
        }
        return null;
    }
    
    /**
     * @author         Patrick Lorilla
     * @description    pagination. 
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public PageReference firstPage(){
        currentPage = 1;
        currentRecord = 1;
        pagedorderList  .clear();
        for(Integer counter = 0; counter < pageSize; counter++){
            pagedorderList  .add(oTAWrapperList [counter]);
        }
        hasNext = true;
        if(currentPage == 1){
            hasPrevious = false;
        }
        return null;
    }
    /**
     * @author         Patrick Lorilla
     * @description    OTA Wrapper
     * @history        21.JAN.2015 - Patrick Lorilla - Created
     */
    public class oTAWrapper{
    
         public OSM_Order_Territory_Assignment__c oTAList {get;set;}
         public List<OSM_Territory_Sales_Rep_Assignment__c> srUsersList {get;set;}
         /**
         * @author         Patrick Lorilla
         * @description   Constructor
         * @history        21.JAN.2015 - Patrick Lorilla - Created
         */
         public oTAWrapper(OSM_Order_Territory_Assignment__c oTA, List<OSM_Territory_Sales_Rep_Assignment__c> srUsers){
             oTAList = oTA;
             srUsersList = srUsers;    
             
         }
    
    }
}