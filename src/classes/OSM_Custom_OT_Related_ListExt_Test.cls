/*
      @author: David Catindoy
      @date: 22 JAN 2015 - Created
      @description: Custom OT Related List Extension (Test Class)
      @history: 22 JAN 2015 - Updated

*/
@isTest
private class OSM_Custom_OT_Related_ListExt_Test {
	
	static testmethod void lessRecord(){
		Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
		Account account = OSM_DataFactory.createAccount('New Account');
		insert account;

		Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
		contact.AccountId = account.Id;
		insert contact;

		Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
		insert order;

		OSM_Territory__c territory = OSM_DataFactory.createTerritory('New Territory');
		insert territory;

		List<User> sr = new List<User>();
        for(Integer a=0; a<20; a++){
            sr.add(OSM_DataFactory.createUser('Cat'+a));
        }
        insert sr;

		List<OSM_Order_Territory_Assignment__c> orderTA = new List<OSM_Order_Territory_Assignment__c>();
		for(Integer i=0; i<4; i++){
			orderTA.add(new OSM_Order_Territory_Assignment__c(OSM_OrderName__c = order.Id, OSM_Territory_Name__c = territory.Id));
		}
		insert orderTA;

		List<OSM_Territory_Sales_Rep_Assignment__c> territorySRA = new List<OSM_Territory_Sales_Rep_Assignment__c>();
		for(Integer i=0; i<4; i++){
			territorySRA.add(new OSM_Territory_Sales_Rep_Assignment__c(OSM_Territory_ID__c = territory.Id, OSM_Sales_Rep__c = sr[i].Id));
			if(i<2){
				territorySRA[i].OSM_Account_Access_Level__c = 'Read';
            }
           	else {
                territorySRA[i].OSM_Account_Access_Level__c = 'Edit';
            }
		}
		insert territorySRA;

		Boolean hasNext;
        Boolean hasPrevious;
        Boolean error = false;
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + order.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController standardController = new ApexPages.StandardController(order);
        OSM_Custom_OT_Related_ListExt controller = new OSM_Custom_OT_Related_ListExt(standardController);
        Test.startTest();
        try{
        controller.deleteRecord();
        controller.nextPage();
        hasPrevious = controller.hasPrevious;
        controller.previousPage();
        hasNext = controller.hasNext;
    	} catch(Exception e){
    		error = true;
    	}
        Test.stopTest();
        System.assert(hasPrevious);
        System.assert(hasNext);
        System.assert(!error);
	}
	
	static testmethod void moreRecord(){
		Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
		Account account = OSM_DataFactory.createAccount('New Account');
		insert account;

		Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
		contact.AccountId = account.Id;
		insert contact;

		Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
		insert order;

		OSM_Territory__c territory = OSM_DataFactory.createTerritory('New Territory');
		insert territory;

		List<User> sr = new List<User>();
        for(Integer a=0; a<20; a++){
            sr.add(OSM_DataFactory.createUser('Cat'+a));
        }
        insert sr;

		List<OSM_Order_Territory_Assignment__c> orderTA = new List<OSM_Order_Territory_Assignment__c>();
		for(Integer i=0; i<10; i++){
			orderTA.add(new OSM_Order_Territory_Assignment__c(OSM_OrderName__c = order.Id, OSM_Territory_Name__c = territory.Id));
		}
		insert orderTA;

		List<OSM_Territory_Sales_Rep_Assignment__c> territorySRA = new List<OSM_Territory_Sales_Rep_Assignment__c>();
		for(Integer i=0; i<10; i++){
			territorySRA.add(new OSM_Territory_Sales_Rep_Assignment__c(OSM_Territory_ID__c = territory.Id, OSM_Sales_Rep__c = sr[i].Id));
			if(i<5){
				territorySRA[i].OSM_Account_Access_Level__c = 'Read';
            }
           	else {
                territorySRA[i].OSM_Account_Access_Level__c = 'Edit';
            }
		}
		insert territorySRA;

		Boolean hasNext;
        Boolean hasPrevious;
        Boolean error = false;
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + order.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController standardController = new ApexPages.StandardController(order);
        OSM_Custom_OT_Related_ListExt controller = new OSM_Custom_OT_Related_ListExt(standardController);
        Test.startTest();
        try{
        controller.nextPage();
        hasPrevious = controller.hasPrevious;
        controller.previousPage();
        hasNext = controller.hasNext;
        controller.lastPage();
        controller.firstPage();
        controller.pageref();
        controller.pagerefManual();
        controller.gotoSRUser();
        controller.deleteRecord();
    	} catch(Exception e){
    		error = true;
    	}
        Test.stopTest();
        System.assert(hasPrevious);
        System.assert(hasNext);
        System.assert(!error);
	}

	static testmethod void noTSRARecord(){
		Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
		Account account = OSM_DataFactory.createAccount('New Account');
		insert account;

		Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
		contact.AccountId = account.Id;
		insert contact;

		Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
		insert order;

		OSM_Territory__c territory = OSM_DataFactory.createTerritory('New Territory');
		insert territory;

		List<OSM_Order_Territory_Assignment__c> orderTA = new List<OSM_Order_Territory_Assignment__c>();
		for(Integer i=0; i<4; i++){
			orderTA.add(new OSM_Order_Territory_Assignment__c(OSM_OrderName__c = order.Id, OSM_Territory_Name__c = territory.Id));
		}
		insert orderTA;
		Boolean hasTSRARecords = false;
        Test.startTest();
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + order.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController standardController = new ApexPages.StandardController(order);
        OSM_Custom_OT_Related_ListExt controller = new OSM_Custom_OT_Related_ListExt(standardController);
        hasTSRARecords = controller.hasTSRARecords;
        Test.stopTest();
        System.assert(!hasTSRARecords);
	}

	static testmethod void noOTARecord(){
	    Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
		Account account = OSM_DataFactory.createAccount('New Account');
		insert account;

		Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
		contact.AccountId = account.Id;
		insert contact;

		Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
		insert order;
		Boolean hasRecords = false;
        Test.startTest();
        PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + order.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController standardController = new ApexPages.StandardController(order);
        OSM_Custom_OT_Related_ListExt controller = new OSM_Custom_OT_Related_ListExt(standardController);
        hasRecords = controller.hasRecords;
        Test.stopTest();
        System.assert(!hasRecords);
	}
	
}