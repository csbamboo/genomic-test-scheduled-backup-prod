/**
 * @author         Kristian Vegerano
 * @description    Test class for OSM_CustomerAffiliationTriggerHandler class. 
 * @history        27.OCT.2014 - Kristian Vegerano - Created  
 */
@isTest
public class OSM_CustomerAffiliationHandler_Test{

    /**
     * @author         Kristian Vegerano
     * @description    Tests removal of Primary flag to other Customer Affiliation records associated to one contact. 
     * @history        27.OCT.2014 - Kristian Vegerano - Created  
     */
    public static testmethod void testRemoveOtherPrimaryAcctCnt(){
        //Prepare required custom settings
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Customer_Affiliation_Trigger__c = true;
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;

        Id RT_Acct_Acct_CustAffiliation = String.valueOf(Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Acct-Acct Affiliation').getRecordTypeId()).substring(0,15);
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Id Payor_ID__c = String.valueOf(Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Payor Customer Affiliation').getRecordTypeId()).substring(0,15);

        Account testAccount1 = OSM_DataFactory.createAccountWithBillingAddress(1, 'USA', 'Test Street', 'Test City', 'Test Province', '12345', HCORecType);
        testAccount1.OSM_Status__c = 'Approved';
        testAccount1.OSM_Specialty__c = 'Hospital';
        testAccount1.OSM_Type__c = 'Hospital';
        insert testAccount1;
        
        Account testAccount2 = OSM_DataFactory.createAccountWithBillingAddress(1, 'USA', 'Test Street', 'Test City', 'Test Province', '12345', HCORecType);
        testAccount2.OSM_Status__c = 'Approved';
        testAccount2.OSM_Specialty__c = 'Hospital';
        testAccount2.OSM_Type__c = 'Hospital';
        insert testAccount2;
        
        Account testAccount3 = OSM_DataFactory.createAccountWithBillingAddress(1, 'USA', 'Test Street', 'Test City', 'Test Province', '12345', HCORecType);
        testAccount3.OSM_Status__c = 'Approved';
        testAccount3.OSM_Specialty__c = 'Hospital';
        testAccount3.OSM_Type__c = 'Hospital';
        insert testAccount3;
        
        Contact testContact1 = OSM_DataFactory.createContact(1, null, PatientRecType);
        //Contact testContact1 = new Contact(LastName= '1', RecordTypeId = PatientRecType);
        insert testContact1;
        
        Contact testContact2 = OSM_DataFactory.createContact(1, null, PatientRecType);
        //Contact testContact1 = new Contact(LastName= '1', RecordTypeId = PatientRecType);
        insert testContact2;
        
        Customer_Affiliation__c newCustomerAffiliation = OSM_DataFactory.createAccountContactCustomerAffiliation(testAccount1.Id, testContact1.Id, RT_Acct_Acct_CustAffiliation, true);
        newCustomerAffiliation.OSM_Role__c = 'Primary';
        newCustomerAffiliation.OSM_Primary__c = true;
        
        Customer_Affiliation__c newCustomerAffiliation2 = OSM_DataFactory.createAccountContactCustomerAffiliation(testAccount1.Id, testContact1.Id, recordTypeIds.Payor_Customer_Affiliation_RT_ID__c, true);
        newCustomerAffiliation2.OSM_Role__c = 'Payor Association';
        newCustomerAffiliation2.OSM_Primary__c = false;
        newCustomerAffiliation2.OSM_Account_2__c = testAccount3.Id;
        newCustomerAffiliation2.OSM_Contact_2__c = testContact2.Id;
        
        
        //Customer_Affiliation__c createdCustomerAffiliation = [SELECT Id, OSM_Role__c, OSM_Primary__c FROM Customer_Affiliation__c];
        //System.assertEquals(true, createdCustomerAffiliation.OSM_Role__c.contains('Primary'));
        
        Test.startTest();
        try{
            OSM_CustomerAffiliationTriggerHandler.setNotRun();
            newCustomerAffiliation.OSM_Primary__c = true;
            insert newCustomerAffiliation;
            insert newCustomerAffiliation2;
            newCustomerAffiliation.OSM_Primary__c = true;
            newCustomerAffiliation2.OSM_Account_1__c = testAccount2.Id;
            update newCustomerAffiliation;
            update newCustomerAffiliation2;
        System.assertEquals(true, newCustomerAffiliation.OSM_Primary__c); 
         List<Customer_Affiliation__c> a = new List<Customer_Affiliation__c>();
         a.add(newCustomerAffiliation2);
        //OSM_CustomerAffiliationTriggerHandler.setNotRun();
        OSM_CustomerAffiliationTriggerHandler.removePrimaryFlag(a);
        List<Customer_Affiliation__c> createdCustomerAffiliation2 = [SELECT Id, OSM_Role__c, OSM_Primary__c FROM Customer_Affiliation__c];
         OSM_CustomerAffiliationTriggerHandler.setNotRun();
         OSM_CustomerAffiliationTriggerHandler.removePrimaryFlag(createdCustomerAffiliation2);
        Test.stopTest();
        }
        catch(Exception ex){
            
        }
        System.assertEquals(2, [SELECT Id, OSM_Role__c FROM Customer_Affiliation__c].size());
        System.assertEquals(true, [SELECT Id, OSM_Role__c FROM Customer_Affiliation__c WHERE Id = :newCustomerAffiliation.Id LIMIT 1].OSM_Role__c.contains('Primary'));
    

    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Tests removal of Primary flag to other Customer Affiliation records associated to one contact. 
     * @history        27.OCT.2014 - Kristian Vegerano - Created  
     */
    public static testmethod void testRemoveOtherPrimaryCntAcct(){
        //Prepare required custom settings
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Customer_Affiliation_Trigger__c = true;
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;

        Id RT_Cnt_Acct_CustAffiliation = String.valueOf(Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Cnt-Acct Affiliation').getRecordTypeId()).substring(0,15); 
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();

        Account testAccount1 = OSM_DataFactory.createAccountWithBillingAddress(1, 'USA', 'Test Street', 'Test City', 'Test Province', '12345', HCORecType);
        testAccount1.OSM_Status__c = 'Approved';
        testAccount1.OSM_Specialty__c = 'Hospital';
        testAccount1.OSM_Type__c = 'Hospital';
        insert testAccount1;
        
        //Contact testContact1 = OSM_DataFactory.createContact(1, null , PatientRecType);
        Contact testContact1 = new Contact(LastName= '1', RecordTypeId = PatientRecType);
        insert testContact1;
        Contact testContact2 = OSM_DataFactory.createContact(2, testAccount1.Id, PatientRecType);
        insert testContact2;

        List<Customer_Affiliation__c> listCustAff = new List<Customer_Affiliation__c>();
        for(Integer a=0;a<10;a++){
            listCustAff.add(OSM_DataFactory.createAccountContactCustomerAffiliation(testAccount1.Id, testContact1.Id, RT_Cnt_Acct_CustAffiliation, true));
            listCustAff[a].OSM_Contact_2__c = testContact2.Id;
            listCustAff[a].OSM_Role__c = 'Primary';
        }
        insert listCustAff;
        
        Customer_Affiliation__c newCustomerAffiliation = OSM_DataFactory.createAccountContactCustomerAffiliation(testAccount1.Id, testContact1.Id, RT_Cnt_Acct_CustAffiliation, true);
        newCustomerAffiliation.OSM_Role__c = 'Primary';
        newCustomerAffiliation.OSM_Primary__c = true;
        
        //Customer_Affiliation__c createdCustomerAffiliation = [SELECT Id, OSM_Role__c, OSM_Primary__c FROM Customer_Affiliation__c];
        //System.assertEquals(true, createdCustomerAffiliation.OSM_Role__c.contains('Primary'));
        Test.startTest();
            OSM_CustomerAffiliationTriggerHandler.setNotRun();
            insert newCustomerAffiliation;
        Test.stopTest();
        System.assertEquals(11, [SELECT Id, OSM_Role__c FROM Customer_Affiliation__c].size());
        System.assertEquals(true, [SELECT Id, OSM_Role__c FROM Customer_Affiliation__c WHERE Id = :newCustomerAffiliation.Id LIMIT 1].OSM_Role__c.contains('Primary'));
        //System.assertEquals(true, [SELECT Id, OSM_Role__c FROM Customer_Affiliation__c WHERE Id = :createdCustomerAffiliation.Id LIMIT 1].OSM_Role__c == null);

    }
    

    public static testmethod void testRemovePrimaryFlag(){
        //Prepare required custom settings
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Customer_Affiliation_Trigger__c = true;
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;

        Id Payor_Customer_Affiliation_RT_ID = String.valueOf(Schema.SObjectType.Customer_Affiliation__c.getRecordTypeInfosByName().get('Payor Customer Affiliation').getRecordTypeId()).substring(0,15);
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        
        Account testAccount1 = OSM_DataFactory.createAccountWithBillingAddress(1, 'USA', 'Test Street', 'Test City', 'Test Province', '12345', HCORecType);
        testAccount1.OSM_Status__c = 'Approved';
        testAccount1.OSM_Specialty__c = 'Hospital';
        testAccount1.OSM_Type__c = 'Hospital';
        insert testAccount1;
        
        Contact testContact1 = OSM_DataFactory.createContact(1, testAccount1.Id, HCPRecType);
        insert testContact1;
        Contact testContact2 = OSM_DataFactory.createContact(2, testAccount1.Id, HCPRecType);
        insert testContact2;
        
        Customer_Affiliation__c newCustomerAffiliation = OSM_DataFactory.createAccountContactCustomerAffiliation(testAccount1.Id, testContact1.Id, Payor_Customer_Affiliation_RT_ID, true);
        newCustomerAffiliation.OSM_Role__c = 'Primary';
        newCustomerAffiliation.OSM_Primary__c = true;
        insert newCustomerAffiliation;

        List<Customer_Affiliation__c> listCustAff = new List<Customer_Affiliation__c>();
        for(Integer a=0;a<10;a++){
            listCustAff.add(OSM_DataFactory.createAccountContactCustomerAffiliation(testAccount1.Id, testContact1.Id, Payor_Customer_Affiliation_RT_ID, true));
            listCustAff[a].OSM_Contact_2__c = testContact2.Id;
        }
        //insert listCustAff;
        
        //Customer_Affiliation__c createdCustomerAffiliation = [SELECT Id, OSM_Role__c, OSM_Primary__c FROM Customer_Affiliation__c];
        //System.assertEquals(true, createdCustomerAffiliation.OSM_Role__c.contains('Primary'));
        Test.startTest();
            OSM_CustomerAffiliationTriggerHandler.runOnce = true;
            insert listCustAff;
            List<Customer_Affiliation__c> createdCustomerAffiliation2 = [SELECT Id, OSM_Role__c, OSM_Primary__c, OSM_Contact_2__c, OSM_Contact_1__c, RecordTypeId FROM Customer_Affiliation__c];
            //OSM_CustomerAffiliationTriggerHandler.setNotRun();
            OSM_CustomerAffiliationTriggerHandler.runOnce = true;
            OSM_CustomerAffiliationTriggerHandler.removePrimaryFlag(createdCustomerAffiliation2);
        Test.stopTest();
        //System.assertEquals(2, [SELECT Id, OSM_Role__c FROM Customer_Affiliation__c].size());
        //System.assertEquals(true, [SELECT Id, OSM_Role__c FROM Customer_Affiliation__c WHERE Id = :newCustomerAffiliation.Id LIMIT 1].OSM_Role__c.contains('Primary'));
        //System.assertEquals(true, [SELECT Id, OSM_Role__c FROM Customer_Affiliation__c WHERE Id = :createdCustomerAffiliation.Id LIMIT 1].OSM_Role__c == null);

    }
}