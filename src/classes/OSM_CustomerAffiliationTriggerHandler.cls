/**
 * File Info
 * ----------------------------------
 * @filename       OSM_CustomerAffiliationTriggerHandler.cls
 * @created        02.FEB.2014
 * @author         Kristian Vegerano
 * @description    Class that contains all actions for Customer Affiliation Trigger. 
  History
    <Date>          <Authors Name>          <Brief Description of Change>
    10/23/2014        Kristian Vegerano    Created
    06/2/2015       Paul Wittmeyer         modified to use OSM_Customer_AffiliationDomain (commented out)
  */
public class OSM_CustomerAffiliationTriggerHandler{
    public static Boolean hasRun = false;
    public static Boolean runOnce = true;
    public static void setRun(){
        hasRun = true;
    }
    public static void setNotRun(){
        hasRun = false;
    }   
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for that fires before insertion of a record. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
     */
    public static void onBeforeInsert(List<Customer_Affiliation__c> customerAffilationList){
        Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
        List<Customer_Affiliation__c> filteredAddress = new List<Customer_Affiliation__c>();
        List<Customer_Affiliation__c> checkonepayorAddress = new List<Customer_Affiliation__c>();
        Set<ID> checkonepayorAccts1 = new Set<ID>();
        Set<ID> checkonepayorAccts2 = new Set<ID>();
        for(Customer_Affiliation__c caLoop : customerAffilationList){
            system.debug('test bins caLoop ' + caLoop.RecordTypeId );
            if(caLoop.RecordTypeId != recordTypes.Payor_Customer_Affiliation_RT_ID__c){
                filteredAddress.add(caLoop);
               
            }
            else{
                 //the new record type features and changes of the Create a New Payor Association button in the vf page should be applied with the validation rule functionality.  (patrick l 7.13.2015)
                checkonepayorAccts1.add(caLoop.OSM_Account_1__c);
                checkonepayorAccts2.add(caLoop.OSM_Account_2__c);
                checkonepayorAddress.add(caLoop);    
                
            }
        }
         system.debug('test bins checkonepayorAccts1 ' + checkonepayorAccts1);
        system.debug('test bins checkonepayorAccts2 ' + checkonepayorAccts2);
        checkifOnePayor(checkonepayorAddress,checkonepayorAccts1,checkonepayorAccts2 );
        copyAccountAddress(filteredAddress);
        removeOtherPrimary(filteredAddress);
        removePrimaryFlag(customerAffilationList);
    } 
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for that fires before updating a record. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
     */
    public static void onBeforeUpdate(List<Customer_Affiliation__c> customerAffilationList, Map<Id, Customer_Affiliation__c> oldCustomAffilMap){
        Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
        List<Customer_Affiliation__c> filteredAddress = new List<Customer_Affiliation__c>();
        List<Customer_Affiliation__c> filteredAddress2 = new List<Customer_Affiliation__c>();
        List<Customer_Affiliation__c> checkonepayorAddress = new List<Customer_Affiliation__c>();
        Set<ID> checkonepayorAccts1 = new Set<ID>();
        Set<ID> checkonepayorAccts2 = new Set<ID>();
        for(Customer_Affiliation__c caLoop : customerAffilationList){
            if(caLoop.RecordTypeId != recordTypes.Payor_Customer_Affiliation_RT_ID__c){
                system.debug('test enter if');
                filteredAddress.add(caLoop);
               
            }
            else{
                system.debug('test enter else');
                if((caLoop.OSM_Account_1__c != oldCustomAffilMap.get(caLoop.Id).OSM_Account_1__c && caLoop.OSM_Account_2__c == oldCustomAffilMap.get(caLoop.Id).OSM_Account_2__c) ||
                  (caLoop.OSM_Account_2__c == oldCustomAffilMap.get(caLoop.Id).OSM_Account_2__c && caLoop.OSM_Account_2__c != oldCustomAffilMap.get(caLoop.Id).OSM_Account_2__c) ||
                   (caLoop.OSM_Account_2__c != oldCustomAffilMap.get(caLoop.Id).OSM_Account_2__c && caLoop.OSM_Account_2__c != oldCustomAffilMap.get(caLoop.Id).OSM_Account_2__c) 
                ){  //the new record type features and changes of the Create a New Payor Association button in the vf page should be applied with the validation rule functionality.  (patrick l 7.10.2015)
                    checkonepayorAddress.add(caLoop);
                    checkonepayorAccts1.add(caLoop.OSM_Account_1__c);
                    checkonepayorAccts2.add(caLoop.OSM_Account_2__c);
                    }
              
               }
        }
        checkifOnePayor(checkonepayorAddress,checkonepayorAccts1,checkonepayorAccts2 );
        copyAccountAddress(filteredAddress);
        removeOtherPrimary(filteredAddress);
        
        for(Customer_Affiliation__c cusAffil: customerAffilationList){
            if(cusAffil.OSM_Primary__c != oldCustomAffilMap.get(cusAffil.ID).OSM_Primary__c){
                filteredAddress2.add(cusAffil);
            }
        }
        removePrimaryFlag(filteredAddress2);
        system.debug('test checkonepayorAccts1 ' + checkonepayorAccts1);
        system.debug('test checkonepayorAccts2 ' + checkonepayorAccts2);
    }

    /**
     * @author         Patrick Lorilla
     * @description    Check if the account has only one payor.
     * @history        31.MAR.2015 - Patrick Lorilla - Created  
     */
    public static void checkifOnePayor(List<Customer_Affiliation__c> customerAffilationList, Set<ID> checkonepayorAccts1, Set<ID> checkonepayorAccts2){
        system.debug('test enter checkifOnePayor');
        Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
        Map<ID,ID> acctsIds1 = new Map<ID,ID>(); 
        Map<ID,ID> acctsIds2 = new Map<ID,ID>(); 
        Map<ID,ID> acctsIds3 = new Map<ID,ID>(); 
        //OSM_Customer_AffiliationDomain.queryCustomer_AffiliationMap(checkonepayorAccts1, checkonepayorAccts2);
        system.debug('checkonepayorAccts1 ' + checkonepayorAccts1);
        system.debug('checkonepayorAccts2 ' + checkonepayorAccts2);
        system.debug('MAP-----> ' + OSM_Customer_AffiliationDomain.queryCustomer_AffiliationMap(checkonepayorAccts1, checkonepayorAccts2));

        //Select records showing accounts in Account1 field that have existing associations
        for(Customer_Affiliation__c ca :OSM_Customer_AffiliationDomain.queryCustomer_AffiliationMap(checkonepayorAccts1, checkonepayorAccts2).values()){
            if(ca.OSM_Account_1__c != null && ca.OSM_Account_2__c != null){
                system.debug('CA. ACCOUNT 1 Id: '  + ca.OSM_Account_1__c);
                system.debug('CA. ACCOUNT 1 RT: '  +  String.valueOf(ca.OSM_Account_1__r.RecordTypeId).substring(0,15));
                acctsIds1.put(ca.OSM_Account_1__c, String.valueOf(ca.OSM_Account_1__r.RecordTypeId).substring(0,15));
            }
        }

        //Select records showing accounts in Account2 field that have existing associations
        for(Customer_Affiliation__c ca :OSM_Customer_AffiliationDomain.queryCustomer_AffiliationMap(checkonepayorAccts1, checkonepayorAccts2).values()){
            if(ca.OSM_Account_1__c != null && ca.OSM_Account_2__c != null){
                system.debug('CA. ACCOUNT 2 Id: '  + ca.OSM_Account_2__c);
                system.debug('CA. ACCOUNT 2 RT: '  +  String.valueOf(ca.OSM_Account_2__r.RecordTypeId).substring(0,15));
                acctsIds2.put(ca.OSM_Account_2__c, String.valueOf(ca.OSM_Account_2__r.RecordTypeId).substring(0,15));
            }
        }
       
         //Compare and check if the data being processed are available on those retrieved records
        for(Customer_Affiliation__c cs: customerAffilationList){
                System.debug('\n\n\n contains??'+acctsIds1.containsKey(cs.OSM_Account_2__c) );
                boolean isHCO = (acctsIds1.get(cs.OSM_Account_1__c) == recordTypes.Account_HCO_Record_Type__c || acctsIds1.get(cs.OSM_Account_1__c) == recordTypes.Account_HCO_DE_Record_Type__c || acctsIds1.get(cs.OSM_Account_1__c) == recordTypes.Account_HCO_UK_Record_Type__c);
                boolean isHCO2 = (acctsIds1.get(cs.OSM_Account_2__c) == recordTypes.Account_HCO_Record_Type__c || acctsIds1.get(cs.OSM_Account_2__c) == recordTypes.Account_HCO_DE_Record_Type__c || acctsIds1.get(cs.OSM_Account_2__c) == recordTypes.Account_HCO_UK_Record_Type__c);
                boolean isHCO3 = (acctsIds2.get(cs.OSM_Account_2__c) == recordTypes.Account_HCO_Record_Type__c || acctsIds2.get(cs.OSM_Account_2__c) == recordTypes.Account_HCO_DE_Record_Type__c || acctsIds2.get(cs.OSM_Account_2__c) == recordTypes.Account_HCO_UK_Record_Type__c);
                boolean isHCO4 = (acctsIds2.get(cs.OSM_Account_1__c) == recordTypes.Account_HCO_Record_Type__c || acctsIds2.get(cs.OSM_Account_1__c) == recordTypes.Account_HCO_DE_Record_Type__c || acctsIds2.get(cs.OSM_Account_1__c) == recordTypes.Account_HCO_UK_Record_Type__c);
                if(acctsIds1.containsKey(cs.OSM_Account_1__c) && isHCO){
                     cs.OSM_Account_1__c.addError(Label.HCO_Has_Payor_Association);        
                }
                else if(acctsIds1.containsKey(cs.OSM_Account_2__c) && isHCO2){
                    cs.OSM_Account_2__c.addError(Label.HCO_Has_Payor_Association);        
                }
                else if(acctsIds2.containsKey(cs.OSM_Account_2__c) && isHCO3){
                    cs.OSM_Account_2__c.addError(Label.HCO_Has_Payor_Association);     
                }
                else if(acctsIds2.containsKey(cs.OSM_Account_1__c) && isHCO4){
                    cs.OSM_Account_1__c.addError(Label.HCO_Has_Payor_Association);     
                }
               
            
        }
      
    }
    /**
     * @author        Michiel Patricia M. Robrigado
     * @description    Method for that fires after insertion of a record. 
     * @history        25.FEB.2015 - Michiel Patricia M. Robrigado - Created  
     */
    public static void onAfterInsert(List<Customer_Affiliation__c> customerAffilationList){
        //removePrimaryFlag(customerAffilationList);
    }
    /**
     * @author        Michiel Patricia M. Robrigado
     * @description    Method for that fires after updte of a record. 
     * @history        25.FEB.2015 - Michiel Patricia M. Robrigado - Created  
     */
    public static void onAfterUpdate(List<Customer_Affiliation__c> customerAffilationList){
        //removePrimaryFlag(customerAffilationList);
    }
    /**
     * @author         Kristian Vegerano
     * @description    Reset primary value on role field for other customer affiliation records. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
     */
    public static void removeOtherPrimary(List<Customer_Affiliation__c> customerAffilationList){
        if(!hasRun){
            //Stop recursive loops
            setRun();
            Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
            
            //Process correct record type
            Set<Id> excludeIds = new Set<Id>();
            List<Customer_Affiliation__c> accountContactList = new List<Customer_Affiliation__c>();
            for(Customer_Affiliation__c loopCustomerAffilation : customerAffilationList){
                if(loopCustomerAffilation.RecordTypeId == recordTypes.RT_Acct_Cnt_CustAffiliation__c || loopCustomerAffilation.RecordTypeId == recordTypes.RT_Cnt_Acct_CustAffiliation__c){
                    accountContactList.add(loopCustomerAffilation);
                    excludeIds.add(loopCustomerAffilation.Id);
                }
            }
            
            if(accountContactList.size() > 0){
                //Collect Customer Affiliation
                Set<Id> removePrimaryFromContact1Set = new Set<Id>();
                Set<Id> removePrimaryFromContact2Set = new Set<Id>();
                for(Customer_Affiliation__c loopCustomerAffilation : accountContactList){
                    if(loopCustomerAffilation.OSM_Role__c != null){
                        if(loopCustomerAffilation.OSM_Role__c.contains('Primary')){
                            if(loopCustomerAffilation.RecordTypeId == recordTypes.RT_Cnt_Acct_CustAffiliation__c && loopCustomerAffilation.OSM_Contact_1__c != null){
                                removePrimaryFromContact1Set.add(loopCustomerAffilation.OSM_Contact_1__c);
                            }/*else if(loopCustomerAffilation.RecordTypeId == recordTypes.RT_Acct_Cnt_CustAffiliation__c && loopCustomerAffilation.OSM_Contact_1__c != null){
                                removePrimaryFromContact2Set.add(loopCustomerAffilation.OSM_Contact_1__c);
                            }*/
                        }
                    }
                }
    
                List<Customer_Affiliation__c> removePrimaryList = new List<Customer_Affiliation__c>();
                List<Customer_Affiliation__c > CAcontact1List = new List<Customer_Affiliation__c>();
                List<Customer_Affiliation__c> CAcontact2List = new List<Customer_Affiliation__c>();
                if(removePrimaryFromContact1Set.size() > 0){
                	for(Customer_Affiliation__c ca : OSM_Customer_AffiliationDomain.queryCustomerAffiliationExcludeMap(removePrimaryFromContact1Set, removePrimaryFromContact2Set, excludeIds).values()){
                		for(id id1 :removePrimaryFromContact1Set){
                			if(id1 == ca.OSM_Contact_1__c )
                				CAcontact1List.add(ca);
                		}
                	}

                	if(CAcontact1List != null && CAcontact1List.size() > 0){
                		for(Customer_Affiliation__c loopCustomerAffilation : CAcontact1List){
                			if(loopCustomerAffilation.OSM_Role__c != null){
                            	loopCustomerAffilation.OSM_Role__c = loopCustomerAffilation.OSM_Role__c.remove('Primary');
                            	removePrimaryList.add(loopCustomerAffilation);
                			}
                        }
                	}
                }

                if(removePrimaryList.size() > 0){
                    update removePrimaryList;
                }           
            }
        }
    } 
    
    /**
     * @author         Kristian Vegerano
     * @description    Copy Address fields of account to contact for primary customer affiliation. 
     * @history        23.OCT.2014 - Kristian Vegerano - Created  
     */
    public static void copyAccountAddress(List<Customer_Affiliation__c> customerAffiliationList){
        Map<Id,Id> contactAccountMap = new Map<Id,Id>();
        for(Customer_Affiliation__c loopCustomerAffiliation : customerAffiliationList){
            if(loopCustomerAffiliation.OSM_Role__c != null){
                if(loopCustomerAffiliation.OSM_Role__c.contains('Primary')){
                   contactAccountMap.put(loopCustomerAffiliation.OSM_Contact_1__c,loopCustomerAffiliation.OSM_Account_1__c); 
                }
            }
        }
        
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, OSM_Email__c, Fax, Phone, OSM_Country_Code__c
                                                          FROM Account 
                                                          WHERE Id IN :contactAccountMap.values()]);
        
        Contact tempContact;
        List<Contact> updateContacts = new List<Contact>();
        for(Id loopContactId : contactAccountMap.keySet()){
            if(contactAccountMap.containsKey(loopContactId)){
                tempContact = new Contact(Id = loopContactId);
                if(accountMap.containsKey(contactAccountMap.get(loopContactId))){
                    /*if(accountMap.get(contactAccountMap.get(loopContactId)).BillingStreet != null){
                        tempContact.MailingStreet = accountMap.get(contactAccountMap.get(loopContactId)).BillingStreet;
                    }
                    if(accountMap.get(contactAccountMap.get(loopContactId)).BillingCity != null){
                        tempContact.MailingCity = accountMap.get(contactAccountMap.get(loopContactId)).BillingCity;
                    }
                    if(accountMap.get(contactAccountMap.get(loopContactId)).BillingState != null){
                        tempContact.MailingState = accountMap.get(contactAccountMap.get(loopContactId)).BillingState;
                    }
                    if(accountMap.get(contactAccountMap.get(loopContactId)).BillingPostalCode != null){
                        tempContact.MailingPostalCode = accountMap.get(contactAccountMap.get(loopContactId)).BillingPostalCode;
                    }
                    if(accountMap.get(contactAccountMap.get(loopContactId)).BillingCountry != null){
                        tempContact.MailingCountry = accountMap.get(contactAccountMap.get(loopContactId)).BillingCountry;
                    }
                    // Andrew Castillo 5.14.15 - removed - email should not come from the account, it is specific to contact
                    if(accountMap.get(contactAccountMap.get(loopContactId)).OSM_Email__c != null){
                        tempContact.Email = accountMap.get(contactAccountMap.get(loopContactId)).OSM_Email__c;
                    }
                    if(accountMap.get(contactAccountMap.get(loopContactId)).Fax != null){
                        tempContact.Fax = accountMap.get(contactAccountMap.get(loopContactId)).Fax;
                    }
                    // Kristian Vegerano 6.30.15 - removed - phone should not come from the account, it is specific to contact
                    //if(accountMap.get(contactAccountMap.get(loopContactId)).Phone != null){
                    //    tempContact.Phone = accountMap.get(contactAccountMap.get(loopContactId)).Phone;
                    //}
                    if(accountMap.get(contactAccountMap.get(loopContactId)).Phone != null){
                        tempContact.OSM_Country_Code__c = accountMap.get(contactAccountMap.get(loopContactId)).OSM_Country_Code__c;
                    }*/
                }
            }
            updateContacts.add(tempContact);
        }
        
        if(updateContacts.size() > 0){
            update updateContacts;
        }
    }
    /**
     * @author         Michiel Patricia M. Robrigado
     * @description    Remove the Primary Flag
     * @history        18.FEB.2015 - Michiel Patricia M. Robrigado - Created  
     */
    public static void removePrimaryFlag(List<Customer_Affiliation__c> customerAffiliationList){
        system.debug('test enter removePrimaryFlag');
        if(runOnce){
            system.debug('test enter removePrimaryFlag2');
            //Stop recursive loops
            setRun();
            Record_Type__c recordTypes = Record_Type__c.getOrgDefaults();
            system.debug('\n\n\n recordTypes.Payor_Customer_Affiliation_RT_ID__c ' + recordTypes.Payor_Customer_Affiliation_RT_ID__c + '\n\n\n');
            //system.debug('\n\n\n customerAffiliationList[0].RecordTypeId ' + customerAffiliationList[0].RecordTypeId + '\n\n\n');
            //Process correct record type
            Set<Id> excludeIds = new Set<Id>();
            for(Customer_Affiliation__c loopCustomerAffilation : customerAffiliationList){
                system.debug('\n\n\n loopCustomerAffilation.RecordTypeId ' + loopCustomerAffilation.RecordTypeId + '\n\n\n');
                if(loopCustomerAffilation.RecordTypeId == recordTypes.Payor_Customer_Affiliation_RT_ID__c){
                    excludeIds.add(loopCustomerAffilation.Id);
                }
            }
            
            //Collect Customer Affiliation
            Set<Id> removePrimaryFromContact1Set = new Set<Id>();
            Set<Id> removePrimaryFromContact2Set = new Set<id>();
            for(Customer_Affiliation__c loopCustomerAffilation : customerAffiliationList){
                system.debug('test loopCustomerAffilation.OSM_Primary__c ' + loopCustomerAffilation.OSM_Primary__c);
                if(loopCustomerAffilation.OSM_Primary__c){
                    if(loopCustomerAffilation.OSM_Contact_1__c != null){
                        removePrimaryFromContact1Set.add(loopCustomerAffilation.OSM_Contact_1__c);
                    }
                }
            }
            system.debug('CUSTOMERAFFILIATIONLIST ' + customerAffiliationList);
            system.debug('test removePrimaryFromContact1Set ' + removePrimaryFromContact1Set);
            List<Customer_Affiliation__c> removePrimaryList = new List<Customer_Affiliation__c>();
            //List<Customer_Affiliation__c> removePrimaryListUpdate = new List<Customer_Affiliation__c>();
            List<Customer_Affiliation__c> customAffilList = new List<Customer_Affiliation__c>();
            if(removePrimaryFromContact1Set.size() > 0){
            	for(id id :removePrimaryFromContact1Set){
	            	for(Customer_Affiliation__c CA :OSM_Customer_AffiliationDomain.queryCustomerAffiliationExcludeMap(removePrimaryFromContact1Set, removePrimaryFromContact2Set, excludeIds).values()){
	            		if(CA.OSM_Contact_1__c == id && CA.OSM_Primary__c == true){
	            			customAffilList.add(CA);
	            		}
	            	}
            	}
                //List<Customer_Affiliation__c> customAffilList = [SELECT Id, OSM_Primary__c FROM Customer_Affiliation__c WHERE OSM_Contact_1__c IN :removePrimaryFromContact1Set AND Id NOT IN :excludeIds AND OSM_Primary__c = true Order By LastModifiedDate DESC];
                system.debug('CUSTOMAFFILLIST ' + customAffilList);
                for(Customer_Affiliation__c loopCustomerAffilation : customAffilList){
                    if(loopCustomerAffilation.OSM_Primary__c){
                        loopCustomerAffilation.OSM_Primary__c = false;
                        removePrimaryList.add(loopCustomerAffilation);
                    }
                }
            }
            system.debug('test removePrimaryList ' + removePrimaryList);
            if(removePrimaryList.size() > 0){
                update removePrimaryList;
            } 
            runOnce = false;
        }
    }
}