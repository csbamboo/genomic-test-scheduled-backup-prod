/*------------------------------------------------------------------------
    @author         Paul Wittmeyer
    @company        Cloud Sherpas
    @description    Data domain class for the Customer Affiliation object
            https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    06/2/2015       Paul Wittmeyer         Created this class
---------------------------------------------------------------------------*/

public with sharing class OSM_Customer_AffiliationDomain {

    private static Map<Id, Customer_Affiliation__c> Customer_AffiliationMap;
    private static Map<Id, Customer_Affiliation__c> Customer_AffiliationExcludeMap;


    public static Map<Id, Customer_affiliation__c> queryCustomer_AffiliationMap( Set<ID> checkonepayorAccts1, Set<ID> checkonepayorAccts2){

        //check to see if the map exists and initialize it if necessary
        if(Customer_AffiliationMap == null){
            Customer_AffiliationMap = new Map<Id, Customer_Affiliation__c>();
        }
        Set<Id> mapKeyset = Customer_AffiliationMap.keySet();

        //combine the two incoming sets, remove all null values, and remove all values already queried
        Set<Id> incomingIds = new Set<Id>();
        incomingIds.addAll(checkonepayorAccts1);
        incomingIds.addAll(checkonepayorAccts2);
        incomingIds.remove(null);
        incomingIds.removeAll(mapKeyset);

        //query only if we have imported new Ids
        if(incomingIds.size() > 0){
            Customer_AffiliationMap.putAll([SELECT Id, OSM_Account_1__c, OSM_Account_2__c, OSM_Account_1__r.RecordTypeId, 
                                                                                        OSM_Account_2__r.RecordTypeId, 
                                                                                        OSM_Role__c FROM Customer_affiliation__c 
                                                                                        WHERE OSM_Role__c = 'Payor Association' 
                                                                                        AND (OSM_Account_1__c != null 
                                                                                        OR OSM_Account_2__c != null)
                                                                                        AND (OSM_Account_1__c in :incomingIds 
                                                                                        OR OSM_Account_2__c in :incomingIds)]);
        }
        return Customer_AffiliationMap;
    }
    
    public static Map<Id, Customer_Affiliation__c> queryCustomerAffiliationExcludeMap(Set<Id> removePrimaryFromContact1Set, Set<Id> removePrimaryFromContact2Set, Set<id>excludeIds){
    
        //check to see if the map exists and initialize it if necessary
        if(Customer_AffiliationExcludeMap == null){
            Customer_AffiliationExcludeMap = new Map<Id, Customer_Affiliation__c>();
        }
        Set<Id> mapKeyset = Customer_AffiliationExcludeMap.keySet();
        
        //combine the two incoming 'removePrimary'sets, remove all null values, and remove all values already queried
        Set<Id> incomingIds = new Set<Id>();
        incomingIds.addAll(removePrimaryFromContact1Set);
        incomingIds.addAll(removePrimaryFromContact2Set);
        incomingIds.remove(null);
        incomingIds.removeAll(mapKeyset);
        
        
        //query only if we have imported new Ids
        if(incomingIds.size() > 0){
            Customer_AffiliationExcludeMap.putAll([SELECT Id, OSM_Primary__c, OSM_Role__c, OSM_Contact_1__c, OSM_Contact_2__c FROM Customer_affiliation__c  
                                                                                        WHERE (OSM_Contact_1__c in :incomingIds
                                                                                        OR OSM_Contact_2__c in :incomingIds) Order By LastModifiedDate DESC]);
                                                                                        
                                                                                        
            for(id e : excludeIds){
                if(Customer_AffiliationExcludeMap.get(e) != null){
                    Customer_AffiliationExcludeMap.remove(e);
                }
            }
        }
        return Customer_AffiliationExcludeMap;
    }
}