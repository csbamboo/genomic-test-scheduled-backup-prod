/*************************************************************
@Name: OSM_DefaultPatientAccountforContact
@Author: Amanpreet Sidhu, OSM Track
@CreateDate: 09/03/2015
@Description: This Utility class is responsible for:
              1. Assigning DefaultPatient Account to Contact records for which the RecordType = Patient
              2. Creating DefaultPatient Account records.
@UsedBy: OSM_ContactTriggerHandler
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/

public without sharing class OSM_DefaultPatientAccountforContact 
{
    private static Record_Type__c recordTypeIds = Record_Type__c.getOrgDefaults();
    public static Id accountOtherRecordType = Schema.SObjectType.Account.RecordTypeInfosByName.get('Other').RecordTypeId; // Account RecordType for Default Patient Accounts
    public static Id defParentAccforPatient = recordTypeIds.Contact_Patient_Default_Account__c; // Record Id of DEFAULT PARENT ACCOUNT FOR PATIENT
    public static Integer maxConRecordCount = (Integer)recordTypeIds.Contact_Patient_Default_Account_Count__c; // Maximum allowed Contacts against 1 Patient Account (5000)
    public static Integer defaultPatientAccounts = (Integer)recordTypeIds.Contact_Patient_Default_Account_Records__c; // Default number of Patienr Accounts that should exist in system for which max contact should be less then ////maxConRecordCount///
    public static boolean bypassAccountTrigger = false; // flag to bypass Account Trigger upon creation of Default Patient Accounts from this class
    
    public static void assignPatientAccount(List<Contact> newPatients)
    {
        List<String> lstDefAccNames = new List<String>();
        List<Id> lstDefAccIds = new List<Id>();
        Integer resultsSize = 0;
        Integer startIndex = 0;
        Integer range = 0;
        // Aggregate Query to find Default Patient Accounts against which contact records should be less as defined in  field "Contact_Patient_Default_Account_Count__c" of Custom Setting "System Setting"
        List<AggregateResult> aggRes = [SELECT Account.Name accountname, count(Id), AccountId accId
                                         FROM Contact 
                                         WHERE Account.parentId = :defParentAccforPatient
                                         GROUP BY Account.Name, AccountId
                                         HAVING count(Id) < :maxConRecordCount
                                         ORDER BY Account.Name ASC
                                         ];
        for (AggregateResult Result: aggRes)
        {
            Id accId = (Id) Result.get('accId');
            String accName = (String) Result.get('accountname');
            lstDefAccNames.add(accName);
            lstDefAccIds.add(accId);
        }// end of For
        
        // SOQl to find Default Patient Account records which are childless(Contacts)
        List<Account> lstChildlessAcc = new List<Account>([SELECT Name, Id, (SELECT ID FROM Contacts LIMIT 1) from Account where ParentId =: defParentAccforPatient]);
        for(Account acc : lstChildlessAcc)
        {
            if(acc.Contacts.isEmpty()){
                lstDefAccNames.add(acc.Name);
                lstDefAccIds.add(acc.Id);
            }
             
        }
        
        lstDefAccNames.sort();
               
        resultsSize = lstDefAccNames.size();
        
        // IF number of Default Patient Account records is less then defined number of records as in field "Contact_Patient_Default_Account_Records__c" of Custom Setting "System Setting"
        // then create additional Default Patient accounts first and then perform assignment of Default Patient Accounts to Contact records
        // ELSE perform assignment of Default Patient Accounts to Contact records
        if (resultsSize < defaultPatientAccounts)
        {
            startIndex = (resultsSize == 0) ?  0 : Integer.ValueOf(lstDefAccNames[resultsSize-1].substring(3));
            range = (defaultPatientAccounts - resultsSize);
            
            bypassAccountTrigger = true;
            lstDefAccIds.addall(createAccount(startIndex, range)); // Calling method "createAccount" to create Default Patient Accounts
            bypassAccountTrigger = false;
            
            assignAccountId(newPatients, lstDefAccIds); // Calling method "assignAccountId" to assign Default Patient Accounts to Contact records
        }
        else
        {
            assignAccountId(newPatients, lstDefAccIds); // Calling method "assignAccountId" to assign Default Patient Accounts to Contact records
        }
        
    }
    
/*************************************************************
@MethodName: assignAccountId
@Return Type: n/a
@Parameters:  List<Contact>, List<Id> default patient Account Id's
@Author: Amanpreet Sidhu, OSM Track
@CreateDate: 09/04/2015
@Description: This method is responsible for 
              1. Assigning DefaultPatient Account to Contact records by performing round robin operation for which a custom object records (DefaultPatientAccount RoundRobin) are created to obtain
              auto number which is used as Dividend in Modulo function against Divisor (defaultPatientAccounts)
              
@UsedBy: OSM_DefaultPatientAccountforContact.assignPatientAccount
*****************************************************************/
    
    public static void assignAccountId(List<Contact> newPatients, List<Id> defaultPatientAcctIds)
    {
        List<Integer> roundRobinsequence = new List<Integer>();
        Integer Dividend;
        Integer mathModulus = 0;
        List<OSM_DefaultPatientAccount_RoundRobin__c> listRRObj = new List<OSM_DefaultPatientAccount_RoundRobin__c>();
        for(Integer i=0; i<newPatients.size();i++)
        {
            OSM_DefaultPatientAccount_RoundRobin__c  roundRobinObj = new OSM_DefaultPatientAccount_RoundRobin__c();
            listRRObj.add(roundRobinObj);   
                     
        }
        
        database.insert(listRRObj);
        
        for(OSM_DefaultPatientAccount_RoundRobin__c rrObj : [Select Name from OSM_DefaultPatientAccount_RoundRobin__c where Id in :listRRObj])
        {
            roundRobinsequence.add(Integer.valueOf(rrObj.Name));
        }
        
        System.debug('PrintroundRobinsequence'+roundRobinsequence);
        
        for(Integer i=0; i<newPatients.size();i++)
        {
            dividend = roundRobinsequence[i];
            mathModulus = Math.Mod(dividend, defaultPatientAccounts);
            newPatients[i].AccountId = defaultPatientAcctIds[mathModulus];
        }
        
        
    }
    
/*************************************************************
@MethodName: createAccount
@Return Type: List<Id> Accounts
@Parameters:  Integer, Integer
@Author: Amanpreet Sidhu, OSM Track
@CreateDate: 09/03/2015
@Description: This method is responsible for 
              1. Creating DefaultPatient Account records.
              
@UsedBy: OSM_DefaultPatientAccountforContact.assignPatientAccount
*****************************************************************/
    
    public static List<Id> createAccount(Integer startIndex, Integer range)
    {
        List<Account> newAccList = new List<Account>();
        List<Id> newAccIdList = new List<Id>();
        
        for(Integer i = startIndex; i < (startIndex+range); i++)
        {
            String newAccountName = String.valueOf(i+1);
            Account acc = new Account(//Name = 'DPA'+(i+1), 
                                      Name = 'DPA' + newAccountName.leftPad(5).replace(' ', '0'),
                                      RecordTypeId = accountOtherRecordType,
                                      ParentId = defParentAccforPatient,
                                      OSM_Status__c = 'Draft',
                                      currencyISOCode = 'USD',
                                      OwnerId = recordTypeIds.User_DPA_Owner__c);
            newAccList.add(acc);
        }
        if(newAccList.size() > 0 && newAccList != null)
        {
            try
            {
                database.insert(newAccList);
            }
            catch (DMLException ex)
            {
                System.debug(ex);
            }
            for(Account accts : newAccList)
            {
                newAccIdList.add(accts.Id);      
            }
        }
        
        return newAccIdList;
    }
}