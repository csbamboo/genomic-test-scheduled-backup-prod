/*
  @author: Amanpreet Singh Sidhu 
  @date: 9 September 2015
  @description: Test Class for OSM_DefaultPatientAccountforContact apex class
  @history: 9 September 2015 - Created (Amanpreet S Sidhu)

*/

@isTest
public class OSM_DefaultPatientAccountforContact_Test 
{
    static testmethod void childlessDefaultPatientAccountTest()
    {    
        //Trigger Switch
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = false;
        insert triggerSwitch;
        
        //Create Default Patient ParentAccount to pass on to custom setting
        Id accOtherRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other').getRecordTypeId();
        Account accParent = new Account(Name = 'ParentAccount', RecordTypeId = accOtherRecType);
        insert accParent;
        
        
        //System Settings
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        recordTypeIds.Contact_Patient_Default_Account__c = accParent.Id;
        recordTypeIds.Contact_Patient_Default_Account_Records__c = 3;
        recordTypeIds.Contact_Patient_Default_Account_Count__c = 2;
        insert recordTypeIds;
        
        // Default Patient Account
        List<Account> lstAcc = new List<Account>();
        Account acc1 = new Account(Name = 'DPA00001', ParentId = accParent.Id);
        Account acc2 = new Account(Name = 'DPA00002', ParentId = accParent.Id);
        lstAcc.add(acc1);
        lstAcc.add(acc2);
        insert lstAcc;
        
        //Contact
        List<Contact> lstCon = new List<Contact>();
        Contact con = new Contact(LastName = 'TestCon', AccountId = acc1.Id);
        lstCon.add(con);
        insert lstCon;
        
        Test.startTest();
        OSM_DefaultPatientAccountforContact.defParentAccforPatient = accParent.Id;
        OSM_DefaultPatientAccountforContact.maxConRecordCount = 2;
        OSM_DefaultPatientAccountforContact.defaultPatientAccounts = 3;
        OSM_DefaultPatientAccountforContact.assignPatientAccount(lstCon);
        Test.stopTest();
        
        
        
        

    }
}