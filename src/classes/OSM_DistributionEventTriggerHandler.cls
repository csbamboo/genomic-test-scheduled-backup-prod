/*
  @author: Karla Reytas/ Roden Songco
  @date: 29 MAY 2015
  @description:Distribution Event Trigger Handler - This is for populating Test Delivered Date
  @history:29 MAY 2015 - Created (KR/RS)

*/
public class OSM_DistributionEventTriggerHandler {
    public static Boolean checkDistributionRuneOnce = false;
    public static Boolean runCreateDistEventFailedCase = false;
    
    
    public static void onAfterInsert(List<OSM_Distribution_Event__c> newDistribution) { 
          if(!runCreateDistEventFailedCase){
            runCreateDistEventFailedCase = true;
            List<OSM_Distribution_Event__c> toProcessDistributionEvent = new List<OSM_Distribution_Event__c>();
            for(OSM_Distribution_Event__c distributionEventLoop : newDistribution){
                if(distributionEventLoop.OSM_Distribution_Failure_Reason__c != null && 
                   distributionEventLoop.OSM_OLI_ID__c != null && 
                   distributionEventLoop.OSM_Order_ID__c != null){
                    
                    toProcessDistributionEvent.add(distributionEventLoop);
                }
            }
            createDistEventFailedCase(toProcessDistributionEvent);
        }
        if(!checkDistributionRuneOnce){
            checkDistributionRuneOnce = true;
            checkDistribution(newDistribution);
        }
   
    }
    
    public static void createDistEventFailedCase(List<OSM_Distribution_Event__c> distributionEventList){
        List<Case> insertCaseList = new List<Case>();
        Id coRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Outreach').getRecordTypeId();

        for(OSM_Distribution_Event__c distributionEventLoop : distributionEventList){
            insertCaseList.add(new Case(OSM_Primary_Order__c = distributionEventLoop.OSM_Order_ID__c,
                                        OSM_Primary_Order_Line_Item__c = distributionEventLoop.OSM_OLI_ID__c,
                                        Description = distributionEventLoop.OSM_Distribution_Failure_Reason__c,
                                        Status = 'Open',
                                        Type = 'Distribution Failure',
                                        RecordTypeId = coRecordTypeId));
        }
        if(insertCaseList.size() > 0){
            insert insertCaseList;
        }
    }
    
    public static void checkDistribution(List<OSM_Distribution_Event__c> newDistribution){
        Boolean isSuccess = false;
        Boolean isResults = false;
        Boolean isFailure = false;
        
        Set<String> disId = new Set<String>();
        Set<String> resId = new Set<String>();
        
        for(OSM_Distribution_Event__c dis : newDistribution) {
            if(dis.OSM_Distribution_Status__c == 'Success'){
                isSuccess = true;
                disId.add(dis.OSM_Results_ID__c);
            }
        }
        //system.debug('@@@@disId' + disId);
        //Result Query
        List<OSM_Result__c> resultList = new List<OSM_Result__c>();
        resultList = [SELECT Id, OSM_Result_Disposition__c, Order_Line_Item_ID__c FROM OSM_Result__c WHERE (Id IN :disId)];
        
        for (OSM_Result__c r : resultList) {
            if (r.OSM_Result_Disposition__c == 'Results') {
                        isResults = true;
                        resId.add(r.Order_Line_Item_ID__c);
            } 
            if (r.OSM_Result_Disposition__c == 'Failure') {
                        isFailure = true;
                        resId.add(r.Order_Line_Item_ID__c);
            }
        }      
        //system.debug('@@@@resId' + resId);
        //Order Query
        List<OrderItem> oliListToUpdate = new List<OrderItem>();
        List<OrderItem> oliList = new List<OrderItem>();
        oliList = [SELECT Id, OSM_Test_Delivered_Date__c, OSM_First_Successful_Date_and_Time__c FROM OrderItem WHERE (Id IN :resId)];
        
        for (OrderItem ol : oliList) {
            if (isSuccess){
                    if (isResults) {
                            if (ol.OSM_Test_Delivered_Date__c == null) {
                                ol.OSM_Test_Delivered_Date__c = system.now();
                            }
                            
                            if (ol.OSM_First_Successful_Date_and_Time__c == null) {
                                ol.OSM_First_Successful_Date_and_Time__c = system.now();
                            }
                            oliListToUpdate.add(ol);
                    } 
                    if (isFailure) {
                        ol.OSM_Test_Delivered_Date__c = null;
                        oliListToUpdate.add(ol);
                    }
            }
        }
        
        system.debug('@@@@oliListToUpdate' + oliListToUpdate);
        
        if(!oliListToUpdate.isEmpty()){
            update oliListToUpdate;
        }
    }
}