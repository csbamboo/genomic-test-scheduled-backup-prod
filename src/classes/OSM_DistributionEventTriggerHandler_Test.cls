@isTest(seeAllData = true)
private class OSM_DistributionEventTriggerHandler_Test {

	private static testMethod void test() {
       TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        update triggerSwitch;
        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        
        Account accList = OSM_DataFactory.createAccount('Dasdwe');
        insert accList;
        Contact conList = OSM_DataFactory.createContact(123, accList.Id, Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        insert conList;
        Order ordList =  new Order(OSM_Patient__c = conList.Id, AccountId = accList.Id ,Pricebook2Id = pb2Standard.Id, EffectiveDate = System.Today(), Status = 'New');
        insert ordList;
        //Pricebook2 pb2 = [SELECT Id FROM Pricebook2 WHERE isStandard = true];
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
         insert prod2;
         PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard.Id, 7);
         insert pbe;
        OrderItem ordItem = new OrderItem(OrderId = ordList.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1);
        insert ordItem;
        //ordItemList.OrderId = ordList.Id;
        //ordItemList.PricebookEntryId = [SELECT Id FROM Pricebook2 WHERE isStandard = true][0].Id;
        //PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        // PriceBook2 pb2Standard = OSM_DataFactory.createpb2('test');

        
         //OrderItem ordItemList = OSM_DataFactory.createOrderItem(ordList.Id, pbe.Id);
        
        // insert ordItemList;
        //UnitPrice = productMap.get(productName).UnitPrice
        OSM_Order_Specimen__c oSpecimenList = new OSM_Order_Specimen__c();
        oSpecimenList.Name = 'Name';
        oSpecimenList.CurrencyIsoCode = 'USD';
        insert oSpecimenList;
        List<OSM_Result__c> resultList = new List<OSM_Result__c>();
        OSM_Result__c result1 = new OSM_Result__c();
        result1.OSM_Results_Approval_Date__c = System.Today();
        result1.OSM_Resulted_Order_Specimen_ID__c = oSpecimenList.Id;
        result1.OSM_Result_Disposition__c = 'Results';
        result1.CurrencyIsoCode = 'USD';
        result1.Order_Line_Item_ID__c = ordItem.Id;
        resultList.add(result1);
        OSM_Result__c result2 = new OSM_Result__c();
        result2.OSM_Results_Approval_Date__c = System.Today();
        result2.OSM_Resulted_Order_Specimen_ID__c = oSpecimenList.Id;
        result2.OSM_Result_Disposition__c = 'Failure';
        result2.CurrencyIsoCode = 'USD';
        result2.OSM_Failure_Code__c = 'MISSING_OR_DAMAGED_SPECIMEN';
        //result1.Order_Line_Item_ID__c = ordItem.Id;
        resultList.add(result2);
        insert resultList;
        List<OSM_Distribution_Event__c> distEventList = new List<OSM_Distribution_Event__c>();
        OSM_Distribution_Event__c distEvent = new OSM_Distribution_Event__c();
        distEvent.OSM_Lab_Task_ID__c = 'Lab Task';
        distEvent.OSM_Created_date__c = System.Today();
        distEvent.OSM_Results_ID__c = result1.Id;
        distEvent.OSM_Distribution_Status__c = 'Success';
        distEvent.OSM_Distribution_Failure_Reason__c = 'Fail';
        distEvent.OSM_Order_ID__c = ordList.Id; 
        distEvent.OSM_OLI_ID__c = ordItem.Id;
        distEventList.add(distEvent); 
        // OSM_Distribution_Event__c distEvent2 = new OSM_Distribution_Event__c();
        // distEvent2.OSM_Lab_Task_ID__c = 'Lab Task23';
        // distEvent2.OSM_Created_date__c = System.Today();
        // distEvent2.OSM_Results_ID__c = result2.Id;
        // distEvent2.OSM_Distribution_Status__c = 'Success';
        // distEvent2.OSM_Distribution_Failure_Reason__c = 'Fail';
        //distEvent2.OSM_Order_ID__c = ordList.Id;
        //distEvent.OSM_OLI_ID__c = ordItem.Id;
        //distEventList.add(distEvent2); 
        
        
 
        insert distEventList;
        
         test.startTest();
          OSM_DistributionEventTriggerHandler.createDistEventFailedCase(distEventList);
          //OSM_DistributionEventTriggerHandler.checkDistribution(distEventList);
        
        test.stopTest();
	}

}