/*
    @author: Rescian Rey
    @description: Test class for OSM_EasyLink_API_Services class
    @createdDate: APR 30 2015
    @history:
        <date>     <author>    <change_description>
*/
@isTest
private class OSM_EasyLink_API_Services_Test {

    /*
        @author: Rescian Rey
        @description: Initialization test. SendFaxManagerService should run in test only mode when
                        no custom setting is set for EasyLink Web Service.
        @createdDate: APR 30 2015
        @history:
        <date>     <author>    <change_description>
    */
    static testmethod void sendFaxManagerService_NoCustomSetting_RunInTestOnlyMode(){
        Test.startTest();

        // Initialize SendFaxManagerService
        OSM_EasyLink_API_Services.SendFaxManagerService service = new OSM_EasyLink_API_Services.SendFaxManagerService();

        // Test only mode should be true
        System.assert(service.getTestOnly());
        Test.stopTest();
    }

    /*
        @author: Rescian Rey
        @description: Initialization test. SendFaxManagerService should follow the latest active custom setting.
        @createdDate:
        @history:
        <date>     <author>    <change_description>
    */
    static testmethod void sendFaxManagerService_ActiveCustomSetting_ConformsToSetting(){
        // Create 2 custom settings, one active and Test only is true, one inactive Test only is false.
        OSM_Easy_Link_API_Settings__c setting_1 = new OSM_Easy_Link_API_Settings__c(
            Name='setting_1', Active__c=true, Test_Only__c=true,
            Email__c='sample@email.com', Password__c='sample');
        OSM_Easy_Link_API_Settings__c setting_2 = new OSM_Easy_Link_API_Settings__c(
            Name='setting_2', Active__c=false, Test_Only__c=false,
            Email__c='sample@email.com', Password__c='sample');
        insert new List<OSM_Easy_Link_API_Settings__c>{setting_1, setting_2};

        Test.startTest();

        // Initialize SendFaxManagerService
        OSM_EasyLink_API_Services.SendFaxManagerService service = new OSM_EasyLink_API_Services.SendFaxManagerService();

        // Should follow the active setting, which is setting 1
        System.assertEquals(setting_1.Test_Only__c, service.getTestOnly()); 
        Test.stopTest();
    }

    /*
        @author: Rescian Rey
        @description: sendFax() should return a transaction ID on successful call.
        @createdDate:
        @history:
        <date>     <author>    <change_description>
    */
    static testmethod void sendFax_TransactionIDReturned(){
        // Create setting
        OSM_Easy_Link_API_Settings__c setting = new OSM_Easy_Link_API_Settings__c(
            Name='setting_1', Active__c=true, Test_Only__c=true,
            Email__c='sample@email.com', Password__c='sample');
        insert setting;

        Test.startTest();

        // Initialize SendFaxManagerService
        OSM_EasyLink_API_Services.SendFaxManagerService service = new OSM_EasyLink_API_Services.SendFaxManagerService();

        String transactionID = service.SendFax('111111', 'AccCode', 'UserCode', null, 'TSI', '', '', '', 'TO', 'FROM', 'CustBuff');
        System.assert((transactionID != null));
        
        Test.stopTest();
    }


    /*****************************************************************************************************************
        @author         : Rescian Rey
        @description    : queryFax() should return a status. This is just a dummy and simple test.
        @history:
            <date>                <author>                <description>
            24-JUN-2015           Rescian Rey             Created method.
    *****************************************************************************************************************/
    static testmethod void queryFax_StatusReturned(){
        // Create setting
        OSM_Easy_Link_API_Settings__c setting = new OSM_Easy_Link_API_Settings__c(
            Name='setting_1', Active__c=true, Test_Only__c=true,
            Email__c='sample@email.com', Password__c='sample');
        insert setting;

        Test.startTest();

        // Initialize SendFaxManagerService
        OSM_EasyLink_API_Services.SendFaxManagerService service = new OSM_EasyLink_API_Services.SendFaxManagerService();

        OSM_EasyLink_API_Wrappers.QueryFaxResponse_element response = service.QueryFax('someUsercode');
        System.assert((response.Status != null));
        
        Test.stopTest();
    }


    /*****************************************************************************************************************
        @author         : Rescian Rey
        @description    : resendFax() should return a status and failure reason. This is just a dummy and simple test.
        @history:
            <date>                <author>                <description>
            24-JUN-2015           Rescian Rey             Created method.
    *****************************************************************************************************************/
    static testmethod void resendFax_StatusAndFailureReasoneReturned(){
        // Create setting
        OSM_Easy_Link_API_Settings__c setting = new OSM_Easy_Link_API_Settings__c(
            Name='setting_1', Active__c=true, Test_Only__c=true,
            Email__c='sample@email.com', Password__c='sample');
        insert setting;

        Test.startTest();

        // Initialize SendFaxManagerService
        OSM_EasyLink_API_Services.SendFaxManagerService service = new OSM_EasyLink_API_Services.SendFaxManagerService();

        OSM_EasyLink_API_Wrappers.ResendFaxResponse_element response = service.ResendFax('someUsercode', 'someDestination');
        System.assert((response.Status != null));
        
        Test.stopTest();
    }
}