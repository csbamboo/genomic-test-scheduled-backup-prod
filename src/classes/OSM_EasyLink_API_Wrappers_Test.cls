/*
    @author: Rescian Rey
    @description: Test class for OSM_EasyLink_API_Wrappers. 
                    No logic testing as per the time of creation.
                    This is just a wrapper class.
    @createdDate: APR 30 2015
    @history:
    <date>     <author>    <change_description>
*/
@isTest
private class OSM_EasyLink_API_Wrappers_Test {

    /*
        @author: Rescian Rey
        @description: Initialization test only.
        @createdDate: No logic to test
        @history:
        <date>     <author>    <change_description>
    */
    static testmethod void initilizationTest(){
        new OSM_EasyLink_API_Wrappers.SendFaxResponse_element();
        new OSM_EasyLink_API_Wrappers.InvalidArgumentFault_element();
        new OSM_EasyLink_API_Wrappers.UserNotRegisteredFault_element();
        new OSM_EasyLink_API_Wrappers.ServiceNotAvailableFault_element();
        new OSM_EasyLink_API_Wrappers.SendFaxRequest_element();
        new OSM_EasyLink_API_Wrappers.FaxDataFiles_element();
        new OSM_EasyLink_API_Wrappers.GeneralFault_element();
        new OSM_EasyLink_API_Wrappers.FaxNotFoundFault_element();
        new OSM_EasyLink_API_Wrappers.Data_element();
        new OSM_EasyLink_API_Wrappers.QueryFaxRequest_element();
        new OSM_EasyLink_API_Wrappers.QueryFaxResponse_element();
        new OSM_EasyLink_API_Wrappers.ResendFaxResponse_element();
        new OSM_EasyLink_API_Wrappers.ResendFaxRequest_element();
    }
}