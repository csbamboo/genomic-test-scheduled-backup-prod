/******************************************************************************
     Author           : Rulleth Decena
     Description      : Controller for EditFax page
     Test Class       : OSM_EditFaxController_Test
     History
         <date>                <author>                <description>
         12-MAY-2015           Rulleth Decena          Created class.
         16-MAY-2015           Rescian Rey             Rename class, code clean up
         19-MAY-2015           Rescian Rey             Tracking of selected attachments
         07-JUL-2015           Rescian Rey             Added comments, code clean up
         13-JUL-2015           Rescian Rey             Added comments, reorganize methods
         24-JUL-2015           Mark Cagurong             removed unused properties: attachToOrderURL and attachToCaseURL, made AttachmentWrapper constructor public
 ********************************************************************************/
public with sharing class OSM_EditFaxController {
    private static final String ATTACHMENT_URL = '/p/attach/NoteAttach';
    private static final String NO_MATCH_FOUND_ERR = 'No fax record found.';
    private static final String SAVE_ERR = 'Error occured while saving. Please try again later.';
    private static final String DEFAULT_TAB_TITLE = 'New Fax';

    // GET parameter names
    public static final String ORDER_ID = 'order-id';
    public static final String CASE_ID = 'case-id';
    public static final String RETURN_URL = 'retURL';
    public static final String ATTACHMENT_PID = 'pid';
    public static final String ATTACHMENT_ID = 'attID';

    // On Base Document Settings Instance
    public static final String ON_BASE_DOC_INSTANCE_NAME = 'Main';

    private ApexPages.StandardController controller;
    private Boolean isFaxExisting = true;

    private Set<Id> selectedAttachments {get; set;}
    private Id previousCase;
    private Id previousOrder;
    private Set<Id> existingFaxAttachments;
    private Boolean forceQuery;
    private Set<Id> existingOrderAttachments;
    private Set<Id> existingCaseAttachments;

    public Fax__c fax {get; set;}
    public String tabTitle {get; set;}

    public String attachToOrderURL {set;}
    public String attachToCaseURL {set;}
    public String returnURL {get; private set;}

    public List<AttachmentWrapper> orderAttachments;
    public List<AttachmentWrapper> caseAttachments;

    public String domesticFax {get; set;}
    public String intFax {get; set;}
    public String rwcFax {get; set;}

    /******************************************************
     * Description      : Apex Controller for OSM_SendNewFaxController page
     * @author          : Rulleth Decena
     * @since           : May 12, 2015 
     * @History - 12 May 2015 - Rulleth Decena - created
     *            17 MAY 2015 - Rescian Rey    - code clean up
     *            09 JUN 2015 - Rescian Rey    - Added defaults from Order
     ******************************************************/
    public OSM_EditFaxController(Apexpages.StandardController cx){
        // Set the standard controller
        controller = cx;

        // Get fax record
        fax = (Fax__c)controller.getRecord();
        isFaxExisting = (fax.Id != null);

        // If existing fax, fetch details from database. Otherwise, populate the default fields.
        if(isFaxExisting){
            queryExistingFax();

            // if no fax found, return
            if(fax == null){
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR, NO_MATCH_FOUND_ERR);
                ApexPages.addMessage(errMsg);
                return;
            }
        }else{
            populateFieldDefaults();
        }

        // Tab title
        setTabTitle();

        // Populate On Base fax numbers
        getOnBaseDocumentSettings();

        // Set return URL
        setReturnURL();

        // track existing fax attachments on load
        trackExistingFaxAttachments();

        // initialize 'selected' attachments set
        initSelectedAttachments();
        
        // order and case attachment tracking initializations
        existingOrderAttachments = new Set<Id>();
        existingCaseAttachments = new Set<Id>();
        forceQuery = false;

        getOrderAttachments();
        getCaseAttachments();
    }

    /******************************************************************************
        Author           : Rescian Rey
        Description      : Save
        History
            <date>                <author>                <description>
            19-MAY-2015           Rescian Rey             Created method.
    ********************************************************************************/
    public Pagereference save(){
        cleanFaxNumber();

        try{
            upsert(fax);
        }catch(DmlException ex){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR, SAVE_ERR);
            ApexPages.addMessage(errMsg);
            return null;
        }
        
        try{
            saveFaxAttachments();
        }catch(DmlException ex){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR, SAVE_ERR);
            ApexPages.addMessage(errMsg);
            return null;
        }

        // Query fax details to be used for post save redirection
        queryExistingFax();
        return null;
    }

    /********************************************************************
        @author :       Rescian Rey
        @description :  Queries available order attachments.
        @history:
            <date>                <author>                <description>
            MAY 18 2015           Rescian Rey             Created method.
    ********************************************************************/
    public List<AttachmentWrapper> getOrderAttachments(){
        if(fax.Order__c != null && (orderAttachments == null || fax.Order__c != previousOrder || forceQuery)){
            orderAttachments = new List<AttachmentWrapper>();
            // Query Order attachments
            for(Attachment att: [SELECT Id,
                        ParentId,
                        Name,
                        CreatedDate
                    FROM Attachment
                    WHERE ParentId = :fax.Order__c ORDER BY CreatedDate DESC]){
                Boolean selected = selectedAttachments.contains(att.Id) || (forceQuery && !existingOrderAttachments.contains(att.Id));
                orderAttachments.add(new AttachmentWrapper(att, selected));
            }
            previousOrder = fax.Order__c;
        }
        return orderAttachments;
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Check if related Order has Attachments
        @history:
            <date>                <author>                <description>
            19-MAY-2015           Rescian Rey              Created method.
    ********************************************************************/
    public Boolean getHasOrderAttachments(){
        getOrderAttachments();
        return fax.Order__c != null && !orderAttachments.isEmpty();
    }


    /********************************************************************
        @author :       Rescian Rey
        @description :  Queries available case attachments.
        @history:
            <date>                <author>                <description>
            MAY 18 2015           Rescian Rey             Created method.
    ********************************************************************/
    public List<AttachmentWrapper> getCaseAttachments(){
        if(fax.Case__c != null && (caseAttachments == null || fax.Case__c != previousCase || forceQuery)){
            // Query Case attachments
            caseAttachments = new List<AttachmentWrapper>();
            for(Attachment att: [SELECT Id,
                        ParentId,
                        Name
                    FROM Attachment
                    WHERE ParentId = :fax.Case__c ORDER BY CreatedDate DESC]){
                Boolean selected = selectedAttachments.contains(att.Id) || (forceQuery && !existingCaseAttachments.contains(att.Id));
                caseAttachments.add(new AttachmentWrapper(att, selected));
            }

            // Email attachments
            for(EmailMessage email: [SELECT (SELECT Id, ParentId, Name FROM attachments) 
                    FROM EmailMessage WHERE ParentId = :fax.Case__c]){
                for(Attachment att: email.attachments){
                    Boolean selected = selectedAttachments.contains(att.Id) || (forceQuery && !existingCaseAttachments.contains(att.Id));
                    caseAttachments.add(new AttachmentWrapper(att, selected));
                }
            }
            previousCase = fax.Case__c;
        }
        return caseAttachments;
    }

    /********************************************************************
        @author : Rescian Rey
        @description : Checked if related Case has Attachments
        @history:
            <date>                <author>                <description>
            19-MAY-2015           Rescian Rey             Created method.
    ********************************************************************/
    public Boolean getHasCaseAttachments(){
        getCaseAttachments();
        return fax.Case__c != null && !caseAttachments.isEmpty();
    }


    /******************************************************************************
        Author         : Rescian Rey
        Description    : Get the Attachment page URL for a record.
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
    ********************************************************************************/
    private String getAttachmentPageURL(Id recordId){
        PageReference pageRef = new PageReference(ATTACHMENT_URL);

        if(recordId != null){
            pageRef.getParameters().put(ATTACHMENT_PID, recordId);
        }

        String url = ApexPages.currentPage().getParameters().get(RETURN_URL);
        if(url != null){
            pageRef.getParameters().put(RETURN_URL, ApexPages.currentPage().getUrl() + '&'+ RETURN_URL +'=' + EncodingUtil.urlEncode(url, 'UTF-8'));
        }
        
        return pageRef.getUrl();
    }


    /********************************************************************
        @author         : Rulleth Decena
        @description    : Returns the URL to Order attachment page
        @history:
            <date>                <author>                <description>
            MAY 12 2015           Rulleth Decena          Created.
            MAY 18 2015           Rescian Rey             Code cleanup
    ********************************************************************/
    public String getAttachToOrderURL(){
        return getAttachmentPageURL(fax.Order__c);
    }

    /********************************************************************
        @author         : Rulleth Decena
        @description    : Returns the URL to Case attachment page
        @history:
            <date>                <author>                <description>
            MAY 12 2015           Rulleth Decena          Created.
            MAY 18 2015           Rescian Rey             Code cleanup
    ********************************************************************/
    public String getAttachToCaseURL(){
        return getAttachmentPageURL(fax.Case__c);
    }


    /********************************************************************
        @author :       Rescian Rey
        @description :  Adds the Id to the selected attachment list. The list
                        will be used for retrieving 'checked' attachments when
                        the page reloads.
        @history:
            <date>                <author>                <description>
            19-MAY-2015           Rescian Rey             Created method
    ********************************************************************/
    public PageReference addToSelectedAttachment(){
        String attID = ApexPages.currentPage().getParameters().get(ATTACHMENT_ID);
        selectedAttachments.add((Id)attID);

        return null;
    }

    /********************************************************************
        @author :       Rescian Rey
        @description :  Removes the Id to the selected attachment list. The list
                        will be used for retrieving 'checked' attachments when
                        the page reloads.
        @history:
            <date>                <author>                <description>
            19-MAY-2015           Rescian Rey             Created method
    ********************************************************************/
    public PageReference removeToSelectedAttachment(){
        String attID = ApexPages.currentPage().getParameters().get(ATTACHMENT_ID);
        selectedAttachments.remove((Id)attID);
        return null;
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Requery attachments
        @history:
            <date>                <author>                <description>
            19-MAY-2015            Rescian Rey            Created method.
    ********************************************************************/
    public PageReference requeryAttachments(){
        forceQuery = true;

        if(orderAttachments != null){
            for(AttachmentWrapper att: orderAttachments){
                existingOrderAttachments.add(att.file.id);
            }
        }
        
        if(caseAttachments != null){
            for(AttachmentWrapper att: caseAttachments){
                existingCaseAttachments.add(att.file.id);
            }
        }

        getOrderAttachments();
        getCaseAttachments();
        forceQuery = false;
        return null;
    }


    /********************************************************************
        @author         : Rescian Rey
        @description    : Stores the selected attachments for future references/comparison
        @history:
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey              Created method.
    ********************************************************************/
    public PageReference trackSelected(){
        // Order Attachments
        selectedAttachments = new Set<Id>();
        if(orderAttachments != null){
            for(AttachmentWrapper att: orderAttachments){
                if(att.isSelected){
                    selectedAttachments.add(att.file.id);
                }
            }
        }

        // Case Attachments
        if(caseAttachments != null){
            for(AttachmentWrapper att: caseAttachments){
                if(att.isSelected){
                    selectedAttachments.add(att.file.id);
                }
            }
        }

        return null;
    }


    // Attachment-checkbox container
    public class AttachmentWrapper {
        public Attachment file {get; private set;}
        public Boolean isSelected {get; set;}
        
        public AttachmentWrapper (Attachment att, Boolean selected){
            file = att;
            isSelected = selected;
        }
    }


    /******************************************************************************
        Author         : Rescian Rey
        Description    : Queries the existing fax details.
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method
    ********************************************************************************/
    private void queryExistingFax(){
        try{
            fax =[SELECT 
                    Name,
                    Fax_Number__c,
                    Case__c,
                    Order__c,
                    Account__c, 
                    Status__c,
                    OSM_Send_Electronic_Storage__c,
                    OSM_Storage_Folder__c,
                    (SELECT OSM_Attachment_ID__c FROM Fax_Attachments__r)
                FROM Fax__c 
                WHERE Id = :fax.Id];
        }catch(QueryException ex){
            fax = null;
        }
    }


    /******************************************************************************
        Author         : Rescian Rey
        Description    : Handles the population of the field defaults
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
    ********************************************************************************/
    private void populateFieldDefaults(){
        // Order field default
        if(fax.Order__c == null){
            String orderID = Apexpages.currentPage().getParameters().get(ORDER_ID);
            fax.Order__c = (orderID=='')?null:orderID;
        }
        
        // Case field default
        if(fax.Case__c == null){
            String caseID = Apexpages.currentPage().getParameters().get(CASE_ID);
            fax.Case__c = caseID==''?null:caseID;
        }

        
        if(fax.Case__c != null){
            Case relatedCase = [SELECT Id,
                                        OSM_Primary_Order__c,
                                        AccountId,
                                        OSM_Primary_Customer_Fax__c,
                                        Account.Fax,
                                        RecordTypeID
                                    FROM Case
                                    WHERE Id = :fax.Case__c];
            Schema.RecordTypeInfo rtype = Schema.SObjectType.Case.getRecordTypeInfosById().get(relatedCase.RecordTypeID);

            // Order and Account defaults from related Case
            fax.Order__c = relatedCase.OSM_Primary_Order__c;
            fax.Account__c = relatedCase.AccountId;

            // Fax number defaults from related Case
            if(rtype.getName() == 'Specimen Retrieval' || 
                    rtype.getName() == 'Specimen Arrival' ||
                    rtype.getName() == 'Customer Outreach' ||
                    rtype.getName() == 'Order - Triage'){
                if(relatedCase.OSM_Primary_Customer_Fax__c != null){
                    fax.Fax_Number__c = relatedCase.OSM_Primary_Customer_Fax__c;
                }else{
                    fax.Fax_Number__c = relatedCase.Account.Fax;
                }
            }else if(rtype.getName() == 'Generic'){
                fax.Fax_Number__c = relatedCase.Account.Fax;
            }

        }else if(fax.Order__c != null){
            Order relatedOrder;
            // Get the fax number from the Order Role with role 'Ordering'
            try{
                relatedOrder = [SELECT Id, 
                                       (SELECT OSM_Account_Fax__c FROM Order_Roles__r
                        WHERE OSM_Role__c = 'Ordering'
                        ORDER BY CreatedDate DESC) 
                    FROM Order WHERE Id = :fax.Order__c];
            }catch(QueryException exp){
                // Do nothing for now...
            }

            if(relatedOrder != null && !relatedOrder.Order_Roles__r.isEmpty()){
                fax.Fax_Number__c = relatedOrder.Order_Roles__r[0].OSM_Account_Fax__c;
            }
        }
    }

    /******************************************************************************
        Author         : Rescian Rey
        Description    : Sets OnBase Document variables
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
    ********************************************************************************/
    private void getOnBaseDocumentSettings(){
        OSM_OnBaseDocument_Settings__c settings = OSM_OnBaseDocument_Settings__c.getInstance(ON_BASE_DOC_INSTANCE_NAME);
        if(settings != null){
            domesticFax = settings.OSM_Domestic_Fax__c;
            intFax = settings.OSM_INT_Fax__c;
            rwcFax = settings.OSM_RWC__c;
        }
    }


    /******************************************************************************
        Author         : Rescian Rey
        Description    : Cleans fax number.
                         - Removes spaces, dashes and parentheses.
                         - Prepend '1' if only consisting of 10 digits
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
            17-JUL-2015           Rescian Rey             Prepending of '1'
    ********************************************************************************/
    private void cleanFaxNumber(){
        if(fax.Fax_Number__c != null){
            fax.Fax_Number__c = fax.Fax_Number__c.replaceAll('[\\(\\)\\s-]', '');

            // if fax number consists only of 10 digits, prepend "1"
            if(fax.Fax_Number__c.length() == 10){
                fax.Fax_Number__c = '1' + fax.Fax_Number__c;
            }
        }
    }

    /******************************************************************************
        Author         : Rescian Rey
        Description    : Sets tab title. Sets the fax name as the tab title if
                         fax is existing. Otherwise, sets to a default tab title.
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
    ********************************************************************************/
    private void setTabTitle(){
        tabTitle = DEFAULT_TAB_TITLE;

        if(isFaxExisting){
            tabTitle = fax.Name;
        }
    }


    /******************************************************************************
        Author         : Rescian Rey
        Description    : Initializes the 'selected' attachment set. Existing fax attachments should be
                         marked as selected on page load.
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
    ********************************************************************************/
    private void initSelectedAttachments(){
        selectedAttachments = new Set<Id>();

        if(fax.Fax_Attachments__r != null){
            for(OSM_Fax_Attachment__c att: fax.Fax_Attachments__r){
                selectedAttachments.add(att.OSM_Attachment_ID__c);
            }
        }
    }


    /******************************************************************************
        Author         : Rescian Rey
        Description    : Tracks existing fax attachments on initialization.
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
    ********************************************************************************/
    private void trackExistingFaxAttachments(){
        existingFaxAttachments = new Set<Id>();
        if(fax.Fax_Attachments__r != null){
            for(OSM_Fax_Attachment__c att: fax.Fax_Attachments__r){
                existingFaxAttachments.add(att.OSM_Attachment_ID__c);
            }
        }
    }


    /******************************************************************************
        Author         : Rescian Rey
        Description    : Sets the return URL.
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
    ********************************************************************************/
    private void setReturnURL(){
        returnURL = ApexPages.currentPage().getParameters().get(RETURN_URL);
        returnURL = (returnURL==null)?'':returnURL;
    }


    /******************************************************************************
        Author         : Rescian Rey
        Description    : Saving fax attachments.
        History
            <date>                <author>                <description>
            13-JUL-2015           Rescian Rey             Created method.
    ********************************************************************************/
    private void saveFaxAttachments(){
        Set<Id> selectedIDs = new Set<Id>();

        // Order Attachments
        List<OSM_Fax_Attachment__c> faxAttachments = new List<OSM_Fax_Attachment__c>();
        if(orderAttachments != null){
            for(AttachmentWrapper att: orderAttachments){
                if(att.isSelected){
                    selectedIDs.add(att.file.Id);
                    if(!existingFaxAttachments.contains(att.file.Id)){
                        OSM_Fax_Attachment__c fa = new OSM_Fax_Attachment__c(
                            OSM_Attachment_ID__c = att.file.Id,
                            OSM_Fax__c = fax.Id,
                            OSM_File_Name__c = att.file.Name
                        );
                        faxAttachments.add(fa);
                    }
                    
                }
            }
        }

        // Case Attachments
        if(caseAttachments != null){
            for(AttachmentWrapper att: caseAttachments){
                if(att.isSelected){
                    selectedIDs.add(att.file.Id);
                    if(!existingFaxAttachments.contains(att.file.Id)){
                        OSM_Fax_Attachment__c fa = new OSM_Fax_Attachment__c(
                            OSM_Attachment_ID__c = att.file.Id,
                            OSM_Fax__c = fax.Id,
                            OSM_File_Name__c = att.file.Name
                        );
                        faxAttachments.add(fa);
                    }
                }
            }
        }

        if(orderAttachments != null) {
            system.debug('orderAttachments.size():' + orderAttachments.size());        
            system.debug('orderAttachments::' + JSON.serialize(orderAttachments));        
        }
        if(caseAttachments != null)  {
            system.debug('caseAttachments.size():' + caseAttachments.size());
            system.debug('caseAttachments::' + JSON.serialize(caseAttachments));        
        }
        if(selectedIDs != null)  system.debug('selectedIDs:' + JSON.serialize(selectedIDs));
        if(existingFaxAttachments != null)  system.debug('existingFaxAttachments:' + JSON.serialize(existingFaxAttachments));

        // delete unchecked fax attachments
        if(!existingFaxAttachments.isEmpty()){
            existingFaxAttachments.removeAll(selectedIDs);
            if(!existingFaxAttachments.isEmpty()){
                delete [SELECT Id FROM OSM_Fax_Attachment__c WHERE OSM_Fax__c = :fax.Id AND
                        OSM_Attachment_ID__c IN :existingFaxAttachments];
            }
        }

        insert faxAttachments;
    }
}