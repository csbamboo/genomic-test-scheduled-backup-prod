/**
 * File Info
 * ----------------------------------
 * @filename       OSM_EmailMessageTriggerHandler.cls
 * @created        7.April.2014
 * @author         Paul Angelo
 * @description    Class for EmailMessage dml actions. 
 * @history        7.April.2015 - Paul Angelo - Created  
 */
public class OSM_EmailMessageTriggerHandler {
    
     /**
     * @author         Paul Angelo
     * @description    Method for all actions that will take place after a EmailMessage has been inserted. 
     * @history        7.April.2015 - Paul Angelo - Created  
     */
    public static void onAfterInsert(Map<Id, EmailMessage> emailMessageMap){
        
        Set<Id> emIds = new Set<Id>();
        Set<String> em2cCsName = new Set<String>();
        List<Case> caseList = new List<Case>();
        List<Case> caseList2 = new List<Case>();
        Map<String,Email2CaseOnClosedCase__c> mapEmail2CaseCS = new Map<String,Email2CaseOnClosedCase__c>();
        Map<Id,String> mapCaseIdQueueId = new Map<Id,String>();
        Map<String,Id> mapQueueNameId = new Map<String,Id>();
        Email2CaseOnClosedCase__c emailToCaseSettings =  new Email2CaseOnClosedCase__c();
        List<Group> queueList = new List<Group>(); 
        
        //get all Email2CaseOnClosedCase__c records and put it on a map.
        mapEmail2CaseCS = Email2CaseOnClosedCase__c.getAll();
        
        //get all Email2CaseOnClosedCase__c Queue's Name
        for(Email2CaseOnClosedCase__c em2cs: mapEmail2CaseCS.values()){
            em2cCsName.add(em2cs.Queue__c);
        }
        
        //query all queue listed on the Email2CaseOnClosedCase__c
        queueList = [SELECT Id, Name FROM Group WHERE Name IN :em2cCsName AND Type = 'Queue'];
        
        //loop through queueList and put the Name and Id on a Map.
        for(Group q: queueList){
            mapQueueNameId.put(q.Name, q.Id);
        }
        
        

        //loop through emailMessages
        for(EmailMessage em : emailMessageMap.Values()){
            if(em.ParentId != null){
                if(em.ToAddress != null)
                
                    //loop through all Email2CaseOnClosedCase Records.
                    for(Email2CaseOnClosedCase__c em2c: mapEmail2CaseCS.values()){
                        if(em2c.Email__c != null){
                            if(em.ToAddress == em2c.Email__c){
                                emIds.add(em.ParentId);
                                
                                //put Case Id and QueueId on a map
                                mapCaseIdQueueId.put(em.ParentId, mapQueueNameId.get(em2c.Queue__c));
                            }
                        }
                    }
                    
            }
        }
        
        //query all cases related to the email messages
        caseList = [SELECT Id, Status, OwnerId FROM Case WHERE Id IN :emIds];
        
        
        //loop through queried caselist
        for(Case c : caseList){
            if(c.Status == 'Closed'){
                c.OwnerId = mapCaseIdQueueId.get(c.Id);
                c.Status = 'Closed';
                caseList2.add(c);
            }
        }
        
        update caseList2;
    }
    
    

}