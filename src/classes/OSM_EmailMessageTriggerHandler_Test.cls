/********************************************************************
    @author         : Mark Cagurong
    @description    : Fax utilities Test class
    @history:
        <date>                <author>                <description>
        July 21 2015           Mark Cagurong             Modified class to add additional unit tests
********************************************************************/
@isTest
public class OSM_EmailMessageTriggerHandler_Test
{
	private static final String EMAIL_SUBJECT= 'Test subject';

    public OSM_EmailMessageTriggerHandler_Test()
    {        
    }

    public static testmethod void onAfterInsert(){
    	Case cse = OSM_DataFactory.createCase('Open', 'General');
    	insert cse;

    	EmailMessage msg = new EmailMessage();
    	msg.ParentId = cse.Id;
    	msg.Subject = EMAIL_SUBJECT;
    	msg.TextBody = 'The quick brown fox jumps over the lazy dog';
    	msg.FromAddress = 'mark.cagurong@cloudsherpas.com';
    	msg.ToAddress = 'mark.cagurong@cloudsherpas.com';

    	insert msg;

    	msg = [SELECT Id, FromAddress, ToAddress from EmailMessage WHERE Subject = :EMAIL_SUBJECT LIMIT 1];

    	system.assert(msg != null);

    	Map<String,Email2CaseOnClosedCase__c> mapEmail2CaseCS = Email2CaseOnClosedCase__c.getAll();

    	system.debug('mapEmail2CaseCS: ' + JSON.serialize(mapEmail2CaseCS));
    }

    public static testmethod void emailmessage_testmethod()
    {
        Test.startTest();
        OSM_EmailMessageTriggerHandler e=new OSM_EmailMessageTriggerHandler();        
        Test.stopTest();
    }
}