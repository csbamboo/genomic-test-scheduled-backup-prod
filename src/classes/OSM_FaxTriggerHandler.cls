/********************************************************************
    @author         : Rescian Rey
    @description    : Fax trigger

        On Before Triggers:
            - Update
                - updateCasePriorityOnFaxFailure
                - preventOperationIfNotNew
    @history:
        <date>                <author>                <description>
        MAY 29 2015           Rescian Rey             Created class.


********************************************************************/
public without sharing class OSM_FaxTriggerHandler extends OSM_TriggerHandlerBase {

    private static final String OP_DELETE = 'delete';
    private static final String OP_EDIT = 'edit';
    public static Boolean bypassEditValidation = false;

    public override void MainEntry(String TriggerObject, Boolean IsBefore, Boolean IsDelete, Boolean 
                                    IsAfter, Boolean IsInsert, Boolean IsUpdate, Boolean IsExecuting, List<SObject> newlist, Map<ID, 
                                    SObject> newmap, List<SObject> oldlist, Map<ID, SObject> oldmap){
        
        try{
            if( isInsert && isBefore ){
                // Insert code for Before Insert here
            }

            if( isInsert && isAfter ){
                // Insert code for After Insert here
            }

            if( isUpdate && isBefore ){
                // Insert code for Before Update here
                preventOperationIfNotNew((List<Fax__c>)newlist, (Map<Id, Fax__c>)oldmap, OP_EDIT);
            //      updateCasePriorityOnFaxFailure((List<Fax__c>)newlist, (Map<Id, Fax__c>)oldmap); 
            }

            if( isUpdate && isAfter ){
                // Insert code for After Update here
                updateCasePriorityOnFaxFailure((List<Fax__c>)newlist, (Map<Id, Fax__c>)oldmap); 
            }

            if( isDelete && isBefore ){
                // Insert code for Before delete here
                preventOperationIfNotNew((List<Fax__c>)oldlist, null, OP_DELETE);
            }

            if( isDelete && isAfter ){
                // Insert code for After delete here
            }
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }finally{   
            //Reset the active function to null in case this class was first in stack
            //this indicates that the trigger has finished firing so it no longer needs to control flow
            OSM_CentralDispatcher.activeFunction = null;
        }
    }

    // Optional override, if this method is not specified then the base class method will fire the Main 
    // entry method of the appropriate controller
    public override void InProgressEntry(String TriggerObject, Boolean IsBefore, Boolean 
                                            IsDelete, Boolean IsAfter, Boolean IsInsert,
                                            Boolean IsUpdate, Boolean IsExecuting,
                                            List<SObject> newlist, Map<ID, SObject> newmap, 
                                            List<SObject> oldlist, Map<ID,SObject> oldmap){

        if(TriggerObject == 'OSM_FaxTrigger'){
            // Stop trigger here
        }else if(TriggerObject == 'OSM_CaseTrigger'){
            OSM_TriggerHandlerBase activeFunction = OSM_CentralDispatcher.getTriggerHandler(TriggerObject);

            //Update the dispatcher active function so that it references the new object
            OSM_CentralDispatcher.activeFunction = activeFunction;
            activeFunction.MainEntry(TriggerObject, IsBefore, IsDelete, IsAfter, IsInsert, IsUpdate,
                                        IsExecuting, newlist, newmap, oldlist, oldmap);
            OSM_CentralDispatcher.activeFunction = this;
        }
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Prevent deletion of Fax when status is not New
        @history:
            <date>                <author>                <description>
            JUN 4 2015            Rescian Rey              Created class
    ********************************************************************/
    public void preventOperationIfNotNew(List<Fax__c> faxRecords, Map<Id, Fax__c> oldFaxMap, String op){

        Id sysAdID = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        if(UserInfo.getProfileId() == sysAdID || bypassEditValidation){
            return;
        }

        for(Fax__c fax: faxRecords){
            if((oldFaxMap == null && fax.Status__c != 'New') ||
                (oldFaxMap != null && oldFaxMap.get(fax.Id).Status__c != 'New')){
                fax.addError('You do not have permission to ' + op + ' this Fax because the Fax Status is not New.');
            }
        }        
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Updates the priority of the Case to High/Urgent if fax fails. Updates
                            the follow up date to NOW() if follow up date is
                            greater than now.
                            
                        : Updates the status of the Specimen Retrieval Case 
                            to Communication Sent if fax succeeds.
        @history:
            <date>                <author>                <description>
            MAY 29 2015           Rescian Rey             Created method.
            JUN 12 2015           Jerome Liwanag          Modified method.
    ********************************************************************/
    public void updateCasePriorityOnFaxFailure(List<Fax__c> fax, Map<Id, Fax__c> oldFaxMap){
        // Filter those that have changed only
        Set<Id> casesToUpdateIDs = new Set<Id>();
        Map<Id,String> caseMap = new Map<Id,String>();
        Id specimenRetrievalId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Specimen Retrieval').getRecordTypeId();
        for(Fax__c f: fax){
            if(f.Case__c != null && (f.Status__c == 'Failure'||f.Status__c == 'Success') && f.Status__c != oldFaxMap.get(f.Id).Status__c){
                casesToUpdateIDs.add(f.Case__c);
                caseMap.put(f.Case__c,f.Status__c);
            }
        }
        
        // Return if nothing to check
        if(casesToUpdateIDs.isEmpty()){
            return;
        }

        final String URGENT = '4 - Urgent';
        final String HIGH = '3 - High';

        List<Case> casesToUpdate = new List<Case>();
        Boolean updated;
        for(Case c: [SELECT Id, Priority, OSM_Follow_Up_Date_Time__c,Type, OSM_Specimen_Request_Date_Time__c, Status,RecordTypeId FROM Case WHERE Id IN :casesToUpdateIDs]){
            updated = false;
            
            if(caseMap.containsKey(c.Id)){
                if(caseMap.get(c.Id) == 'Failure'){
                     // If Priority is not high
                    if(c.Priority != URGENT && c.Priority != HIGH){
                        c.Priority = HIGH;
                        updated = true;
                    }
    
                    // If Follow up date time is greater than now
                    Datetime now = Datetime.now();
                    if(c.OSM_Follow_Up_Date_Time__c == null || c.OSM_Follow_Up_Date_Time__c > now){
                        c.OSM_Follow_Up_Date_Time__c = now;
                        updated = true;
                    }
                }
                if(caseMap.get(c.Id) == 'Success' && c.Status != 'Communication Sent'){
                     //If RecordTypeId and Type is Specimen Retrieval
                    if(c.RecordTypeId == specimenRetrievalId && c.Type == 'Specimen Retrieval'){
                        c.Status = 'Communication Sent';
                        c.OSM_Specimen_Request_Date_Time__c = Datetime.now();
                        updated = true;
                    }
                }
                if(updated){
                    casesToUpdate.add(c);
                }
            }
        }

        if(!casesToUpdate.isEmpty()){
            update casesToUpdate;
        }
    }
}