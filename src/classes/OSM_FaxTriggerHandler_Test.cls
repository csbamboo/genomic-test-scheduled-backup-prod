/********************************************************************
    @author         : Mark Cagurong
    @description    : FaxTriggerHandler Test class
    @history:
        <date>                <author>                <description>
        July 20 2015           Mark Cagurong             Created class.
********************************************************************/
@isTest
private class OSM_FaxTriggerHandler_Test {

    private static final String TEST_USER_LASTNAME = 'Rep123';
    private static final String TEST_SYSAD_LASTNAME = 'Admin123';
    private static final String TEST_ACCOUNT_NAME1 = 'Test123';
    private static final String TEST_ACCOUNT_NAME2 = 'Test456';
    private static final String FAX_NUMBER = '18622351052';
    private static final String CASE_TRACKING_NUMBER = '12345678';

    /*
        @author: Mark Cagurong
        @description: initialization data
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @testSetup static void Setup_Data(){
    
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<UserRole> role =  [select Id from UserRole where Name = 'Finance'];
        thisUser.UserRoleId = role[0].Id;
        update thisUser;
        
        System.runAs(thisUser)
        {
        User u1 = OSM_DataFactory.createUser(TEST_SYSAD_LASTNAME);
        User u2 = OSM_DataFactory.createCustomerServiceUser(TEST_USER_LASTNAME);

        insert u1;
        insert u2;      

        Account a1 = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME1);
        Account a2 = OSM_DataFactory.createAccount(TEST_ACCOUNT_NAME2);

        insert a1;
        insert a2;
    }
    }

    /*
        @author: Mark Cagurong
        @description: retrieve test non-admin user
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    private static User getTestUser(){
        return getUser(TEST_USER_LASTNAME);
    }

    /*
        @author: Mark Cagurong
        @description: retrieve test Admin user
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    private static User getSysAdUser(){
        return getUser(TEST_SYSAD_LASTNAME);
    }

    /*
        @author: Mark Cagurong
        @description: helper method to fetch user info
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    private static User getUser(String alias){
        User u = [SELECT Id, Name, Email FROM User WHERE Alias = :alias LIMIT 1];
        return u;
    }

    /*
        @author: Mark Cagurong
        @description: retrieve account object by account number
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    private static Account getAccount(String accountNumber){
        Account a = [SELECT Id, Name from Account WHERE Name = :accountNumber LIMIT 1];
        return a;
    }

    /*
        @author: Mark Cagurong
        @description: preventOperationIfNotNew unit test, ensure that a non-admin user will be prompted with validation error if he attempts to delete fax that is not in 'New' status
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void preventOperationIfNotNew_DeleteNonAdmin() {

        User testUser = getTestUser();
        
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c(Fax_Trigger__c=true);
        insert triggerSwitch;
        
        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');

        insert fax;

        fax.Status__c = 'Success'; // fake status

        update fax;

        // force running as non-admin user
        System.runAs(testUser){

            String errorMessage = '';

            try{

                Test.startTest();

                delete fax;

                Test.stopTest();
            }
            catch(DmlException dmx){
                errorMessage = dmx.getDmlMessage(0);
            }

            
        }
    }

    /*
        @author: Mark Cagurong
        @description: preventOperationIfNotNew unit test, ensure that a non-admin user will be prompted with validation error if he attempts to update fax that is not in 'New' status
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void preventOperationIfNotNew_UpdateNonAdmin() {

        User testUser = getTestUser();
        
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c(Fax_Trigger__c=true);
        insert triggerSwitch;
        
        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');

        insert fax;

        fax.Status__c = 'Success'; // fake status

        update fax;

        Account testAccount = getAccount(TEST_ACCOUNT_NAME1);

        // force running as non-admin user
        System.runAs(testUser){

            String errorMessage = '';

            try{
                fax.Account__c = testAccount.Id;

                Test.startTest();

                update fax;

                Test.stopTest();
            }
            catch(DmlException dmx){
                errorMessage = dmx.getDmlMessage(0);
            }

            
        }
    }

    /*
        @author: Mark Cagurong
        @description: preventOperationIfNotNew unit test, ensure that an Admin user will be allowed to delete a fax record regardless of status
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void preventOperationIfNotNew_DeleteAdminUser() {

        User testUser = getSysAdUser();
        
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c(Fax_Trigger__c=true);
        insert triggerSwitch;
        
        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');

        insert fax;

        fax.Status__c = 'Success'; // fake status

        update fax;

        // force running as non-admin user
        System.runAs(testUser){
            
            Test.startTest();

            delete fax;

            Test.stopTest();

            List<Fax__c> faxes = [SELECT Id, Name FROM Fax__c WHERE Fax_Number__c = :FAX_NUMBER];

            System.assertEquals(0, faxes.size());
        }
    }

    /*
        @author: Mark Cagurong
        @description: preventOperationIfNotNew unit test, ensure that an Admin user will be allowed to update a fax record regardless of status
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void preventOperationIfNotNew_UpdateAdminUser() {

        User testUser = getSysAdUser();
        
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c(Fax_Trigger__c=true);
        insert triggerSwitch;
        
        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');

        insert fax;

        fax.Status__c = 'Success'; // fake status

        update fax;

        Account testAccount = getAccount(TEST_ACCOUNT_NAME1);

        // force running as non-admin user
        System.runAs(testUser){

            fax.Account__c = testAccount.Id;

            Test.startTest();

            update fax;

            Test.stopTest();

            // rehydrate
            fax = [SELECT Id, Name, Account__c FROM Fax__c WHERE Fax_Number__c = :FAX_NUMBER LIMIT 1];

            System.assertEquals(testAccount.Id, fax.Account__c);
            
        }
    }


    /*
        @author: Mark Cagurong
        @description: updateCasePriorityOnFaxFailure unit test, ensure that the associated case lookup object is updated with a status of 'Communication Sent' once the fax status is updated to a 'Success' state
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void updateCasePriorityOnFaxFailure_FaxStatusSuccessUpdatesCase() {

        User testUser = getTestUser();

        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c(Fax_Trigger__c=true);
        insert triggerSwitch;
        
        Id specimenRetrievalId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Specimen Retrieval').getRecordTypeId();
        Case cse = OSM_DataFactory.createCase('Open', 'Specimen Retrieval');
        cse.OSM_Tracking_Number__c = CASE_TRACKING_NUMBER;
        cse.RecordTypeId  = specimenRetrievalId;

        insert cse;

        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');
        fax.Case__c = cse.Id;

        insert fax;

        Test.startTest();

        fax.Status__c = 'Success'; 
        update fax;

        Test.stopTest();

        // re-hydrate from repository
        cse = [SELECT Id, Priority, OSM_Follow_Up_Date_Time__c,Type, Status, RecordTypeId, OSM_Tracking_Number__c FROM Case WHERE OSM_Tracking_Number__c = :CASE_TRACKING_NUMBER LIMIT 1];
         
        //System.assertEquals('Communication Sent', cse.Status );
    }

    /*
        @author: Mark Cagurong
        @description: updateCasePriorityOnFaxFailure unit test, ensure that the associated case lookup object is updated to a High Priority with corresponding follow-up date update once the fax status is set to Failure
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void updateCasePriorityOnFaxFailure_FaxStatusFailureUpdatesCase() {

        User testUser = getTestUser();

        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c(Fax_Trigger__c=true);
        insert triggerSwitch;
        
        Case cse = OSM_DataFactory.createCase('Open', 'Specimen Retrieval');
        cse.OSM_Tracking_Number__c = CASE_TRACKING_NUMBER;

        insert cse;

        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');
        fax.Case__c = cse.Id;

        insert fax;
        Datetime today = Date.today();

        Test.startTest();

        fax.Status__c = 'Failure'; 
        update fax;

        Test.stopTest();

        // re-hydrate from repository
        cse = [SELECT Id, Priority, OSM_Follow_Up_Date_Time__c,Type,Status,RecordTypeId, OSM_Tracking_Number__c FROM Case WHERE OSM_Tracking_Number__c = :CASE_TRACKING_NUMBER LIMIT 1];

        System.assertEquals('3 - High', cse.Priority);

        //System.assertEquals(today, cse.OSM_Follow_Up_Date_Time__c.date()); // instead of testing time component, use the time snapshot before trigger update to compare against

    }
    
}