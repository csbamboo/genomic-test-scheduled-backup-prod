/********************************************************************
    @author         : Rescian Rey
    @description    : Fax utilities
    @history:
        <date>                <author>                <description>
        MAY 28 2015           Rescian Rey             Created class.
********************************************************************/
public with sharing class OSM_FaxUtilities {

    /********************************************************************
        @author         : Rescian Rey
        @description    : Method for updating fax status.
        @history:
            <date>                <author>                <description>
            MAY 28 2015           Rescian Rey             Created method.
    ********************************************************************/
    public static Fax__c updateFaxStatus(Fax__c faxRecord){
        // Prepare web service call
        OSM_EasyLink_API_Services.SendFaxManagerService service = new OSM_EasyLink_API_Services.SendFaxManagerService();

        // Set the 15-character Fax record ID as the Usercode parameter
        String userCode = ((String)faxRecord.Id).substring(0, 15);

        // Actual callout
        OSM_EasyLink_API_Wrappers.QueryFaxResponse_element response = new OSM_EasyLink_API_Wrappers.QueryFaxResponse_element();
        try{
            response = service.QueryFax(userCode);
        }catch(CalloutException ex){
            System.debug(ex.getMessage());
            faxRecord.Status_Description__c = ex.getMessage();
            return faxRecord;
        }

        if(response != null){
            faxRecord.Status_Description__c = '';
            if(response.Status == OSM_EasyLink_API_Services.QUERY_FAX_SUCCESS_MSG){
                faxRecord.Status__c = 'Success';
            }else if(response.Status == OSM_EasyLink_API_Services.QUERY_FAX_FAILURE_MSG){
                if(response.FailureReason == 'Fax not found'){
                        faxRecord.Status__c = 'Pending';
                }else{
                    faxRecord.Status__c = 'Failure';
                    faxRecord.Status_Description__c = response.FailureReason;
                }
            }else{
                faxRecord.Status__c = 'Pending';
            }
        }
        return faxRecord;
    }
}