/********************************************************************
    @author         : Mark Cagurong
    @description    : Fax utilities Test class
    @history:
        <date>                <author>                <description>
        July 20 2015           Mark Cagurong             Created class.
********************************************************************/
@isTest
private class OSM_FaxUtilities_Test {

    private static final String FAX_NUMBER = '18622351052';
    
    /*
        @author: Mark Cagurong
        @description: initialization data
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @testSetup static void Setup_Data(){
        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');

        insert fax;
    }

    /*
        @author: Mark Cagurong
        @description: retrieve test fax data
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    static Fax__c getTestFax(){
        Fax__c fax = [SELECT Id, Fax_Number__c, Status__c FROM Fax__c WHERE Fax_Number__c = :FAX_NUMBER  LIMIT 1];
        return fax;
    }

     /*
        @author: Mark Cagurong
        @description: test to ensure that a call to updateFaxStatus updates the status field to success of the fax object when the api service call resulted in a successful invocation
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void updateFaxStatus_Success() {     

        Fax__c fax = getTestFax();

        // force success status
        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = OSM_EasyLink_API_Services.QUERY_FAX_SUCCESS_MSG;

        Test.startTest();

        OSM_FaxUtilities.updateFaxStatus(fax);

        Test.stopTest();

        system.assertEquals('Success', fax.Status__c);
    }

    /*
        @author: Mark Cagurong
        @description: test to ensure that a call to updateFaxStatus updates the status field to Failure of the fax object when the api service call resulted in a failed invocation
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void updateFaxStatus_Fail() {        

        Fax__c fax = getTestFax();

        // force success status
        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = OSM_EasyLink_API_Services.QUERY_FAX_FAILURE_MSG;

        Test.startTest();

        OSM_FaxUtilities.updateFaxStatus(fax);

        Test.stopTest();

        system.assertEquals('Failure', fax.Status__c);
    }

    /*
        @author: Mark Cagurong
        @description: test to ensure that a call to updateFaxStatus updates the status field to Pending of the fax object when the api service call resulted in a failed invocation with a failure reason
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void updateFaxStatus_FailPending() {     

        Fax__c fax = getTestFax();

        // force success status
        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = OSM_EasyLink_API_Services.QUERY_FAX_FAILURE_MSG;
        OSM_EasyLink_API_Services.FAX_FAILURE_REASON_OVERRIDE = 'Fax not found';

        Test.startTest();

        OSM_FaxUtilities.updateFaxStatus(fax);

        Test.stopTest();

        system.assertEquals('Pending', fax.Status__c);
    }

    /*
        @author: Mark Cagurong
        @description: test to ensure that a call to updateFaxStatus updates the status field to Pending of the fax object when the api service call resulted in a failed invocation with an unknown error
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void updateFaxStatus_FailPendingUnknownStatus() {        

        Fax__c fax = getTestFax();

        // force success status
        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = 'Test Unknown Status';
        OSM_EasyLink_API_Services.FAX_FAILURE_REASON_OVERRIDE = '';

        Test.startTest();

        OSM_FaxUtilities.updateFaxStatus(fax);

        Test.stopTest();

        system.assertEquals('Pending', fax.Status__c);
    }       
}