/*
    @author: Michiel Patricia M. Robrigado
    @description: Generate default ptc for Account and Plan
    @date: 9/22/2015
*/
global class OSM_GenerateDefaultPTC {
    /*
        @author: Michiel Patricia M. Robrigado
        @description: Generate default ptc for Account and Plan
        @date: 9/22/2015
    */
    webservice static void generateDefaultPTCAccount(Id accountId, Date endDate){
        //get the record type settings
        Record_Type__c recType = Record_Type__c.getOrgDefaults();

        //get the ptc related list in the payor that is selected, make sure it is payor
        //List<OSM_Criteria__c> ptcList = [SELECT Id, OSM_Line_of_Business__c, OSM_Type__c, RecordTypeId, RecordType.Name FROM OSM_Criteria__c WHERE OSM_Payor__c =: accountId AND OSM_Payor__r.RecordTypeId =: recType.Account_Payor_Record_Type__c];
        Set<String> recPTC = new Set<String>();
        for(String prod : Label.OSM_Product_List_3.split('::')){
           recPTC.add(prod);
        }
        
        List<RecordType> recList = [SELECT ID, Name from RecordType where SObjectType = 'OSM_Criteria__c' AND Name IN: recPTC];
        Set<Id> takenIDs = new Set<Id>();
        //List<OSM_Criteria__c> ptcListUpdate = new List<OSM_Criteria__c>();
        List<OSM_Criteria__c> ptcListUpsert = new List<OSM_Criteria__c>();
        //DateTime dt = DateTime.parse(System.Label.OSM_Eff_End_Year);

        //var for the record type and the type of ptc
        Map<String, Id> critMap = new Map<String, Id>();
        Map<String, Set<Id>> critMapND = new Map<String, Set<Id>>();
        //system.debug('test ptcList.size() ' + ptcList.size());
       
        //system.debug('test enter ptc in');
        /*
        for(String str :recPTC){
            for(String typ: 'CT::MP::AD'.split('::')){
                if(critMap.containsKey(str+'::'+typ)){
                    critMap.get(str+'::'+typ).add(ptc.OSM_Type__c);
                } else {
                    critMap.put(str+'::'+typ, new Set<String>{ptc.OSM_Type__c});
                }
            }
        }*/
        //for(OSM_Criteria__c defPTC :[SELECT Id, OSM_Line_of_Business__c, OSM_Type__c, RecordTypeId, RecordType.Name FROM OSM_Criteria__c WHERE OSM_Line_of_Business__c = 'Default' AND OSM_Payor__c =: accountId AND OSM_Payor__r.RecordTypeId =: recType.Account_Payor_Record_Type__c]){
           // critMap.put( defPTC.RecordType.Name+'::'+defPTC.OSM_Type__c, defPTC.Id);
       // }
        for(OSM_Criteria__c ndefPTC :[SELECT Id, OSM_Line_of_Business__c, OSM_Type__c, RecordTypeId, RecordType.Name FROM OSM_Criteria__c WHERE OSM_Payor__c =: accountId AND OSM_Payor__r.RecordTypeId =: recType.Account_Payor_Record_Type__c]){
            //critMapND.put(ndefPTC.RecordType.Name+'::'+ndefPTC.OSM_Type__c+'::'+ndefPTC.OSM_Line_of_Business__c, ndefPTC.Id);
            if(critMapND.containsKey(ndefPTC.RecordType.Name+'::'+ndefPTC.OSM_Type__c)){
                 critMapND.get(ndefPTC.RecordType.Name+'::'+ndefPTC.OSM_Type__c).add(ndefPTC.Id);
            }
            else{
                critMapND.put(ndefPTC.RecordType.Name+'::'+ndefPTC.OSM_Type__c, new Set<Id>{ndefPTC.Id});
            }
        }
         system.debug('test critMapVals ' + critMap);
        for(RecordType str : recList){
                //populate the payor
                if(!critMapND.containsKey(str.Name+'::CT')){
                    OSM_Criteria__c crit = new OSM_Criteria__c();
                    //crit.Id = critMap.containsKey(str.Name+'::CT') ? critMap.get(str.Name+'::CT') : null;
                    crit.OSM_Payor__c = accountId;
                    crit.OSM_Line_of_Business__c = 'Default';
                    crit.OSM_Type__c = 'CT';
                    crit.OSM_Eff_Start_Clinical_Criteria__c = system.today();
                    //crit.OSM_Eff_End_Clinical_Criteria__c = date.newinstance(dT.year(), dT.month(), dT.day());
                    crit.OSM_Eff_End_Clinical_Criteria__c = endDate;
                    crit.RecordTypeId = str.Id;
                    ptcListUpsert.add(crit);
                }
                
        }
            
        for(RecordType str : recList){
                //populate the payor
                if(!critMapND.containsKey(str.Name+'::MP')){
                    OSM_Criteria__c crit = new OSM_Criteria__c();
                    //crit.Id = critMap.containsKey(str.Name+'::MP') ? critMap.get(str.Name+'::MP') : null;
                    crit.OSM_Payor__c = accountId;
                    crit.OSM_Line_of_Business__c = 'Default';
                    crit.OSM_Type__c = 'MP';
                    crit.OSM_Eff_Start_Clinical_Criteria__c = system.today();
                    //crit.OSM_Eff_End_Clinical_Criteria__c = date.newinstance(dT.year(), dT.month(), dT.day());
                    crit.OSM_Eff_End_Clinical_Criteria__c = endDate;
                    crit.RecordTypeId = str.Id;
                    ptcListUpsert.add(crit);
                }
        }
            
            for(RecordType str : recList){
                //populate the payor
                if(!critMapND.containsKey(str.Name+'::AD')){
                    OSM_Criteria__c crit = new OSM_Criteria__c();
                    //crit.Id = critMap.containsKey(str.Name+'::AD') ? critMap.get(str.Name+'::AD') : null;
                    crit.OSM_Payor__c = accountId;
                    crit.OSM_Line_of_Business__c = 'Default';
                    crit.OSM_Type__c = 'AD';
                    crit.OSM_Eff_Start_Clinical_Criteria__c = system.today();
                    //crit.OSM_Eff_End_Clinical_Criteria__c = date.newinstance(dT.year(), dT.month(), dT.day());
                    crit.OSM_Eff_End_Clinical_Criteria__c = endDate;
                    crit.RecordTypeId = str.Id;
                    ptcListUpsert.add(crit);
                }
               
            }
                
          system.debug('test ptcListInsertValues ' + ptcListUpsert);
        
        
        if(ptcListUpsert.size()>0){
            insert ptcListUpsert;
        }
        
      
    }
    
     webservice static void generateDefaultPTCPlan(Id planId, Date endDate){
        //get the record type settings
        Record_Type__c recType = Record_Type__c.getOrgDefaults();

        //get the ptc related list in the payor that is selected, make sure it is payor
        //List<OSM_Criteria__c> ptcList = [SELECT Id, OSM_Line_of_Business__c, OSM_Type__c, RecordTypeId, RecordType.Name FROM OSM_Criteria__c WHERE OSM_Payor__c =: accountId AND OSM_Payor__r.RecordTypeId =: recType.Account_Payor_Record_Type__c];
        Set<String> recPTC = new Set<String>();
        for(String prod : Label.OSM_Product_List_3.split('::')){
           recPTC.add(prod);
        }
        
        List<RecordType> recList = [SELECT ID, Name from RecordType where SObjectType = 'OSM_Criteria__c' AND Name IN: recPTC];
        Set<Id> takenIDs = new Set<Id>();
        //List<OSM_Criteria__c> ptcListUpdate = new List<OSM_Criteria__c>();
        List<OSM_Criteria__c> ptcListUpsert = new List<OSM_Criteria__c>();
        //DateTime dt = DateTime.parse(System.Label.OSM_Eff_End_Year);

        //var for the record type and the type of ptc
        Map<String, Id> critMap = new Map<String, Id>();
        Map<String, Set<Id>> critMapND = new Map<String, Set<Id>>();
        //system.debug('test ptcList.size() ' + ptcList.size());
       
        //system.debug('test enter ptc in');
        /*
        for(String str :recPTC){
            for(String typ: 'CT::MP::AD'.split('::')){
                if(critMap.containsKey(str+'::'+typ)){
                    critMap.get(str+'::'+typ).add(ptc.OSM_Type__c);
                } else {
                    critMap.put(str+'::'+typ, new Set<String>{ptc.OSM_Type__c});
                }
            }
        }*/
        //for(OSM_Criteria__c defPTC :[SELECT Id, OSM_Line_of_Business__c, OSM_Type__c, RecordTypeId, RecordType.Name FROM OSM_Criteria__c WHERE OSM_Line_of_Business__c = 'Default' AND OSM_Payor__c =: accountId AND OSM_Payor__r.RecordTypeId =: recType.Account_Payor_Record_Type__c]){
           // critMap.put( defPTC.RecordType.Name+'::'+defPTC.OSM_Type__c, defPTC.Id);
       // }
        for(OSM_Criteria__c ndefPTC :[SELECT Id, OSM_Line_of_Business__c, OSM_Type__c, RecordTypeId, RecordType.Name FROM OSM_Criteria__c WHERE OSM_Plan__c =: planId]){
            //critMapND.put(ndefPTC.RecordType.Name+'::'+ndefPTC.OSM_Type__c+'::'+ndefPTC.OSM_Line_of_Business__c, ndefPTC.Id);
            if(critMapND.containsKey(ndefPTC.RecordType.Name+'::'+ndefPTC.OSM_Type__c)){
                 critMapND.get(ndefPTC.RecordType.Name+'::'+ndefPTC.OSM_Type__c).add(ndefPTC.Id);
            }
            else{
                critMapND.put(ndefPTC.RecordType.Name+'::'+ndefPTC.OSM_Type__c, new Set<Id>{ndefPTC.Id});
            }
        }
         system.debug('test critMapVals ' + critMap);
        for(RecordType str : recList){
                //populate the payor
                if(!critMapND.containsKey(str.Name+'::CT')){
                    OSM_Criteria__c crit = new OSM_Criteria__c();
                    //crit.Id = critMap.containsKey(str.Name+'::CT') ? critMap.get(str.Name+'::CT') : null;
                    crit.OSM_Plan__c = planId;
                    crit.OSM_Line_of_Business__c = 'Default';
                    crit.OSM_Type__c = 'CT';
                    crit.OSM_Eff_Start_Clinical_Criteria__c = system.today();
                    //crit.OSM_Eff_End_Clinical_Criteria__c = date.newinstance(dT.year(), dT.month(), dT.day());
                    crit.OSM_Eff_End_Clinical_Criteria__c = endDate;
                    crit.RecordTypeId = str.Id;
                    ptcListUpsert.add(crit);
                }
                
        }
            
        for(RecordType str : recList){
                //populate the payor
                if(!critMapND.containsKey(str.Name+'::MP')){
                    OSM_Criteria__c crit = new OSM_Criteria__c();
                    //crit.Id = critMap.containsKey(str.Name+'::MP') ? critMap.get(str.Name+'::MP') : null;
                    crit.OSM_Plan__c = planId;
                    crit.OSM_Line_of_Business__c = 'Default';
                    crit.OSM_Type__c = 'MP';
                    crit.OSM_Eff_Start_Clinical_Criteria__c = system.today();
                    //crit.OSM_Eff_End_Clinical_Criteria__c = date.newinstance(dT.year(), dT.month(), dT.day());
                    crit.OSM_Eff_End_Clinical_Criteria__c = endDate;
                    crit.RecordTypeId = str.Id;
                    ptcListUpsert.add(crit);
                }
        }
            
            for(RecordType str : recList){
                //populate the payor
                if(!critMapND.containsKey(str.Name+'::AD')){
                    OSM_Criteria__c crit = new OSM_Criteria__c();
                    //crit.Id = critMap.containsKey(str.Name+'::AD') ? critMap.get(str.Name+'::AD') : null;
                    crit.OSM_Plan__c = planId;
                    crit.OSM_Line_of_Business__c = 'Default';
                    crit.OSM_Type__c = 'AD';
                    crit.OSM_Eff_Start_Clinical_Criteria__c = system.today();
                    //crit.OSM_Eff_End_Clinical_Criteria__c = date.newinstance(dT.year(), dT.month(), dT.day());
                    crit.OSM_Eff_End_Clinical_Criteria__c = endDate;
                    crit.RecordTypeId = str.Id;
                    ptcListUpsert.add(crit);
                }
               
            }
                
          system.debug('test ptcListInsertValues ' + ptcListUpsert);
        
        
        if(ptcListUpsert.size()>0){
            insert ptcListUpsert;
        }
        
      
    }
}