/*
  @author: Amanpreet Singh Sidhu 
  @date: 23 September 2015
  @description: Test Class for OSM_GenerateDefaultPTC apex class
  @history: 23 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest
public class OSM_GenerateDefaultPTC_Test 
{
      static testMethod void testgenerateDefaultPTCAccount()
      {
          TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
          triggerSwitch.Account_Trigger__c = false;
          insert triggerSwitch;
          
          Id rtAccPayor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
          
          //Account 
          Account account = OSM_DataFactory.createAccount('New Account');
          account.RecordTypeId = rtAccPayor;
          insert account;
          
          Date endDate = date.today().addDays(2);
          Test.startTest();
          
          OSM_GenerateDefaultPTC.generateDefaultPTCAccount(account.Id, endDate);
          Test.stopTest();
      }
      
      static testMethod void testgenerateDefaultPTCPlan()
      {
          TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
          triggerSwitch.Account_Trigger__c = false;
          insert triggerSwitch;
          
          Id rtAccPayor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
          
          //Account 
          Account account = OSM_DataFactory.createAccount('New Account');
          account.RecordTypeId = rtAccPayor;
          account.OSM_Payor_Hierarchy__c = '2';
          account.OSM_Payor_Category__c = 'Self-Pay';
          insert account;
          
          Date endDate = date.today().addDays(2);
          
          //Plan
          OSM_Plan__c testPlan = OSM_DataFactory.createPlan(account.Id, 'New Plan', 'COMM', 'Active');
          insert testPlan;
          
          Test.startTest();
          
          OSM_GenerateDefaultPTC.generateDefaultPTCPlan(testPlan.Id, endDate);
          Test.stopTest();
      }
}