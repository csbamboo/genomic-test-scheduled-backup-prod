/*
    @author: patrick lorilla
    @date: 27 Oct 2014
    @description: HCP Territory Assignment Trigger Handler
    @history: 27 Oct 2014 - Patrick Lorilla - Created

*/
@isTest
public class OSM_HCPAssignmentTriggerHandler_Test{
    /*
    @author: patrick lorilla
    @date: 27 Oct 2014
    @description: Test Create Contact Share
    @history: 27 Oct 2014 - Patrick Lorilla - Created

    */    
    static testmethod void createContactShare(){
        //Prepare users and recordtypes
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        
        User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        insert testUsr;
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
        
        //Prepare parent territories
        List<OSM_Territory__c> pterrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            pterrList.add(OSM_DataFactory.createTerritory('My ParentTerr'+a));  
        }
        
        insert pterrList;
        //Prepare parent territories
        List<OSM_Territory__c> terrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            terrList.add(OSM_DataFactory.createTerritory('My Terr'+a));  
            terrList[a].OSM_Parent_Territory__c = pterrList[a].ID;
        }
        insert terrList ;
        
        //Prepare Sales Reps for TRSA
        List<User> srList = new List<User>();
        for(Integer a=0; a<20; a++){
            srList.add(OSM_DataFactory.createSalesRep('srep'+a));
        }
        insert srList;
        //Insert TRSAList
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<20; a++){
              if(a<5){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, null,'Read'));
                }
                else if (a<10){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, null,'Edit'));
                }
                else if(a<15){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, null, 'Read'));
                }
                else{
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, null, 'Edit'));
                }
        }
        insert tsraList ;
        test.startTest();
        System.runAs(testUsr){
            //Insert HTA
            List<OSM_HCP_Territory_Assignment__c> htaList = new List<OSM_HCP_Territory_Assignment__c>();
            for(Integer a=0; a<20; a++){
                if(a<10){
                    htaList.add(OSM_DataFactory.createHTA(hcp.ID, pterrList[a].ID));
                }
                else{
                    htaList.add(OSM_DataFactory.createHTA(hcp.ID, terrList[a-10].ID));
                }
            }
            
            insert htaList;
        }
        test.stopTest();
        //Assert Contact Share
        Map<ID, User> userMap = new Map<ID,User> ([SELECT ID from User where Alias != 'TestUser']);
        for(ContactShare cShare : [SELECT UserorGroupId from ContactShare Where ContactAccessLevel = 'Read']){
            System.assert(userMap.containsKey(cShare.UserorGroupId));
        }
        
    }
    /*
    @author: patrick lorilla
    @date: 27 Oct 2014
    @description: Test Update Contact Share
    @history: 27 Oct 2014 - Patrick Lorilla - Created

    */
    static testmethod void updateContactShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        
        User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        insert testUsr;
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
        
        //Prepare parent territories
        List<OSM_Territory__c> pterrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            pterrList.add(OSM_DataFactory.createTerritory('My ParentTerr'+a));  
        }
        
        insert pterrList;
        //Prepare parent territories
        List<OSM_Territory__c> terrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            terrList.add(OSM_DataFactory.createTerritory('My Terr'+a));  
            terrList[a].OSM_Parent_Territory__c = pterrList[a].ID;
        }
        insert terrList ;
        
        //Prepare Sales Reps for TRSA
        List<User> srList = new List<User>();
        for(Integer a=0; a<20; a++){
            srList.add(OSM_DataFactory.createSalesRep('srep'+a));
        }
        insert srList;
        //Inse
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<20; a++){
              if(a<5){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, null,'Read'));
                }
                else if (a<10){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, null,'Edit'));
                }
                else if(a<15){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, null, 'Read'));
                }
                else{
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, null, 'Edit'));
                }
        }
        insert tsraList ;
        
        List<OSM_HCP_Territory_Assignment__c> htaList = new List<OSM_HCP_Territory_Assignment__c>();
        for(Integer a=0; a<20; a++){
            if(a<10){
                htaList.add(OSM_DataFactory.createHTA(hcp.ID, pterrList[a].ID));
                htaList[a].OSM_Override__c = true;
            }
            else{
                htaList.add(OSM_DataFactory.createHTA(hcp.ID, terrList[a-10].ID));
                htaList[a].OSM_Override__c = false;
            }
         }
            
        insert htaList;
        test.startTest();
        System.runAs(testUsr){
            for(OSM_HCP_Territory_Assignment__c hta: htaList){
                if(!hta.OSM_Override__c){
                    hta.OSM_Override__c = true;
                }
                else{
                    hta.OSM_Override__c = false;
                }
            }    
            
            update htaList;
        }
        test.stopTest();
        Map<ID, User> userMap = new Map<ID,User> ([SELECT ID from User where Alias != 'TestUser']);
        for(ContactShare cShare : [SELECT UserorGroupId from ContactShare Where ContactAccessLevel = 'Read']){
            System.assert(userMap.containsKey(cShare.UserorGroupId));
        }
    }
    /*
    @author: patrick lorilla
    @date: 27 Oct 2014
    @description: Test Delete Contact Share
    @history: 27 Oct 2014 - Patrick Lorilla - Created

    */
    static testmethod void deleteContactShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Contact_Trigger__c = false;
        insert triggerSwitch;
        
        User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        insert testUsr;
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
        
        //Prepare parent territories
        List<OSM_Territory__c> pterrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            pterrList.add(OSM_DataFactory.createTerritory('My ParentTerr'+a));  
        }
        
        insert pterrList;
        //Prepare parent territories
        List<OSM_Territory__c> terrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            terrList.add(OSM_DataFactory.createTerritory('My Terr'+a));  
            terrList[a].OSM_Parent_Territory__c = pterrList[a].ID;
        }
        insert terrList ;
        
        //Prepare Sales Reps for TRSA
        List<User> srList = new List<User>();
        for(Integer a=0; a<20; a++){
            srList.add(OSM_DataFactory.createSalesRep('srep'+a));
        }
        insert srList;
        
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<20; a++){
              if(a<5){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, null,'Read'));
                }
                else if (a<10){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, pterrList[a].ID, null,'Edit'));
                }
                else if(a<15){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, null, 'Read'));
                }
                else{
                    tsraList.add(OSM_DataFactory.createTerritorySRA(srList[a].ID, terrList[a-10].ID, null, 'Edit'));
                }
        }
        insert tsraList ;
        
        List<OSM_HCP_Territory_Assignment__c> htaList = new List<OSM_HCP_Territory_Assignment__c>();
        for(Integer a=0; a<20; a++){
            if(a<10){
                htaList.add(OSM_DataFactory.createHTA(hcp.ID, pterrList[a].ID));
            }
            else{
                htaList.add(OSM_DataFactory.createHTA(hcp.ID, terrList[a-10].ID));
            }
         }
            
        insert htaList;
        test.startTest();
        //System.runAs(testUsr){    
            delete htaList;
        //}
        test.stopTest();
        Map<ID, User> userMap = new Map<ID,User> ([SELECT ID from User where Alias != 'TestUser']);
        for(ContactShare cShare : [SELECT UserorGroupId from ContactShare Where ContactAccessLevel = 'Read']){
            System.assert(!userMap.containsKey(cShare.UserorGroupId));
        }
    }


}