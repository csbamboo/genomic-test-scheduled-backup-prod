/*
    @author: patrick lorilla
    @date: 2 October 2014
    @description: HCP Territory Assignment Trigger Handler
    @history:
        <date>          <change author>     <description>
        20-AUG-2015     Rescian Rey         Removed unnecessary exception handling
*/

public class OSM_HCPTerritoryAssignmentTriggerHandler{
    
     public static boolean updateNumberofTerritoriesRunOnce = false;
     public static boolean createContactShareRunOnce = false;
     public static boolean deleteContactShareRunOnce = false;
     
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newHTA - new HTA List, newHTAMap - new HTA Map oldHTAMAP- old HTA Map
    @description: Before Update Method
    */
    public static void OnBeforeUpdate(List<OSM_HCP_Territory_Assignment__c> newHTA , Map<ID, OSM_HCP_Territory_Assignment__c> newHTAMap, Map<ID, OSM_HCP_Territory_Assignment__c> oldHTAMap){
         //Set override user and override date
         for(OSM_HCP_Territory_Assignment__c hta :newHTA){
             if(hta.OSM_Override__c && !oldHTAMap.get(hta.ID).OSM_Override__c){
                 hta.OSM_Overrided_By__c = userinfo.getUserId();
                 Datetime today = Datetime.now();
                 hta.OSM_Date_Override__c= Date.parse(today.format('MM/dd/yyyy'));
             }    
         }
    }
    
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newHTA - new HTA List, newHTAMap - new HTA Map
    @description: After Insert Method
    */
    public static void OnAfterInsert(List<OSM_HCP_Territory_Assignment__c> newHTA , Map<ID, OSM_HCP_Territory_Assignment__c> newHTAMap){
        Set<ID> territoryIDS = new Set<ID>();
        for(OSM_HCP_Territory_Assignment__c hta :newHTA){
            territoryIDS.add(hta.OSM_Territory_Name__c);         
        }    
        if(!newHTA.isEmpty() && !territoryIDS.isEmpty() && !createContactShareRunOnce){
            if(!OSM_TerritoryAlignmentBatchContact.batchIsRunnng){
                createContactShareRunOnce = true;
            }
            createContactShare(newHTA,territoryIDS);
        }  
        
        if(!updateNumberofTerritoriesRunOnce){
            updateNumberofTerritoriesRunOnce = true;
            updateNumberofTerritories(newHTA);
        }
    }
    /*
    @author: patrick lorilla
    @date: 16 June 2015
    @param: delAAT- AAT List, delAATMap- AAT Map
    @description: After Delete Method
    */
    public static void OnAfterDelete(List<OSM_HCP_Territory_Assignment__c> delHTA, Map<ID, OSM_HCP_Territory_Assignment__c> delHTAMap){
        if(!updateNumberofTerritoriesRunOnce){
            updateNumberofTerritoriesRunOnce = true;
            updateNumberofTerritories(delHTAMap.values());
        }
    }
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newHTA - new HTA List, newHTAMap - new HTA Map, oldHTAMap - old HTA Map
    @description: After Update Method

    */
    public static void OnAfterUpdate(List<OSM_HCP_Territory_Assignment__c> newHTA, Map<ID, OSM_HCP_Territory_Assignment__c> newHTAMap, Map<ID, OSM_HCP_Territory_Assignment__c> oldHTAMap){     
        Set<ID> territoryIDS = new Set<ID>();
        Set<ID> territoryIDSDelete = new Set<ID>();
        List<OSM_HCP_Territory_Assignment__c> overHTA = new List<OSM_HCP_Territory_Assignment__c>();
        List<OSM_HCP_Territory_Assignment__c> delHTA = new List<OSM_HCP_Territory_Assignment__c>();
        for(OSM_HCP_Territory_Assignment__c hta :newHTA){
            if(!hta.OSM_Override__c && oldHTAMap.get(hta.ID).OSM_Override__c ){
                overHTA.add(hta);
                territoryIDS.add(hta.OSM_Territory_Name__c);         
            }
            else if(hta.OSM_Override__c && !oldHTAMap.get(hta.ID).OSM_Override__c){
                delHTA.add(hta);
                territoryIDSDelete.add(hta.OSM_Territory_Name__c);     
            }
            
            if(hta.OSM_Manual_Assignment__c && !oldHTAMap.get(hta.ID).OSM_Manual_Assignment__c ){
                overHTA.add(hta);
                territoryIDS.add(hta.OSM_Territory_Name__c);         
            }
        }    
        if(!newHTA.isEmpty() && !territoryIDS.isEmpty() && !createContactShareRunOnce){
            createContactShareRunOnce = true;
            createContactShare(overHTA,territoryIDS);
        }
        if(!delHTA.isEmpty() && !territoryIDSDelete .isEmpty() && !deleteContactShareRunOnce){
            deleteContactShare(delHTA,territoryIDSDelete);
        }
        
        if(!updateNumberofTerritoriesRunOnce){
            updateNumberofTerritoriesRunOnce = true;
            updateNumberofTerritories(newHTA);
        }
    }
    
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newATT - new AAT List, terrID - territory ID Set
    @description: delete contact share record
    */
    public static void deleteContactShare(List<OSM_HCP_Territory_Assignment__c> delHTA, Set<ID> terrID){
        //Retrieve child and parents/ancestors territories
        Map<Id, Set<Id>> childGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();

        for(OSM_Territory__c loopTerritory : [SELECT Id, OSM_Parent_Territory__c FROM OSM_Territory__c WHERE OSM_Parent_Territory__c != null LIMIT 25000]){
            childParentMap.put(loopTerritory.Id,loopTerritory.OSM_Parent_Territory__c);
            parentChildMap.put(loopTerritory.OSM_Parent_Territory__c,loopTerritory.Id);
            childGroupMap.put(loopTerritory.Id, new Set<Id>{loopTerritory.OSM_Parent_Territory__c});
        }

        for(Integer counter = 0; counter < 50; counter++){
           for(Id loopChildId : childGroupMap.keySet()){
               for(Id loopParentId : childGroupMap.get(loopChildId)){
                   if(childParentMap.get(loopParentId) != null){
                        childGroupMap.get(loopChildId).add(childParentMap.get(loopParentId));
                   }
                }
             }
        }
        
        for(OSM_HCP_Territory_Assignment__c loopAssignment : delHTA){
            if(childGroupMap.containsKey(loopAssignment.OSM_Territory_Name__c)){
                terrID.addAll(childGroupMap.get(loopAssignment.OSM_Territory_Name__c));
            }
        }
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = [SELECT OSM_Sales_Rep__c, OSM_Contact_Access_Level__c, OSM_Territory_ID__c  from OSM_Territory_Sales_Rep_Assignment__c where OSM_Territory_ID__c IN: terrID AND OSM_Inactive__c = false AND OSM_Sales_Rep__r.IsActive = true];
        //Prepare CA List
        List<ContactAccessLevel> caList = prepareCAList(tsraList, delHTA, childGroupMap);
        Set<ID> userIDSet = new Set<ID>();
        Set<ID> conIDSet = new Set<ID>();
        //prepare wrapper object
        for(ContactAccessLevel cal : caList ){
            userIDSet.add(cal.userID);
            conIDSet.add(cal.conID);
        }
         //Prepare acct share list to be inserted
        List<ContactShare> csList = [SELECT ID, UserOrGroupId, ContactAccessLevel, ContactID from ContactShare where UserorGroupId IN: userIDSet AND ContactID IN: conIDSet AND RowCause = 'Manual'];
     
        if(csList != null && !csList.isEmpty()){
            delete csList;
        }
    }
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: delHTA- HTA List, delHTAMap- HTA Map
    @description: Before Delete Method
    */
    public static void OnBeforeDelete(List<OSM_HCP_Territory_Assignment__c> delHTA, Map<ID, OSM_HCP_Territory_Assignment__c> delHTAMap){
        Set<ID> territoryIDS = new Set<ID>();
        for(OSM_HCP_Territory_Assignment__c hta :delHTA){
            territoryIDS.add(hta.OSM_Territory_Name__c);         
        }    
        if(!delHTA.isEmpty() && !territoryIDS.isEmpty()){
            deleteContactShare(delHTA,territoryIDS);
        }
        createTerritoryHistory(delHTA);
    }
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: newHTA - newHTA List, terrID - territory ID Set
    @description: prepare and create contact share record

    */
    public static void createContactShare(List<OSM_HCP_Territory_Assignment__c> newHTA, Set<ID> terrID){
        //Retrieve child and parents/ancestors territories
        Map<Id, Set<Id>> childGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();
        Set<String> contactCountryCodes = new Set<String>();
        Set<String> contactPostalCodes = new Set<String>();
        
        System.debug('\n\nnewHTA: ' + newHTA);
        
        for(OSM_Territory__c loopTerritory : [SELECT Id, OSM_Parent_Territory__c FROM OSM_Territory__c WHERE OSM_Parent_Territory__c != null LIMIT 25000]){
            childParentMap.put(loopTerritory.Id,loopTerritory.OSM_Parent_Territory__c);
            parentChildMap.put(loopTerritory.OSM_Parent_Territory__c,loopTerritory.Id);
            childGroupMap.put(loopTerritory.Id, new Set<Id>{loopTerritory.OSM_Parent_Territory__c});
        }

        for(Integer counter = 0; counter < 50; counter++){
            for(Id loopChildId : childGroupMap.keySet()){
                for(Id loopParentId : childGroupMap.get(loopChildId)){
                    if(childParentMap.get(loopParentId) != null){
                        childGroupMap.get(loopChildId).add(childParentMap.get(loopParentId));
                    }
                }
            }
        }
        
        for(OSM_HCP_Territory_Assignment__c loopAssignment : newHTA){
            if(childGroupMap.containsKey(loopAssignment.OSM_Territory_Name__c)){
                terrID.addAll(childGroupMap.get(loopAssignment.OSM_Territory_Name__c));
            }
        }
        //Prepare Sales Rep
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = [SELECT OSM_Sales_Rep__c, OSM_Contact_Access_Level__c, OSM_Territory_ID__c 
                                                                FROM OSM_Territory_Sales_Rep_Assignment__c
                                                                WHERE OSM_Territory_ID__c IN: terrID 
                                                                    AND OSM_Inactive__c = false AND OSM_Sales_Rep__r.IsActive = true];
                                                
        System.debug('\n\ntsraList: ' + tsraList);
                                                
        List<ContactShare> csList = new List<ContactShare>();   
        //prepare wrapper object
        List<ContactAccessLevel> caList = prepareCAList(tsraList, newHTA, childGroupMap);
        //Prepare acct share list to be inserted
        if(caList != null && !caList.isEmpty()){
            for(ContactAccessLevel cal: caList){
                 csList.add(new ContactShare(ContactId = cal.conID, ContactAccessLevel = cal.conAccess, UserorGroupId = cal.userId));     
            }
        } 
        
        Set<Id> contactShareContactIds = new Set<Id>();
        Set<Id> contactShareGroupIds = new Set<Id>();
        String queryContactShare = 'SELECT Id, ContactId, UserOrGroupId FROM ContactShare WHERE ';
        for(ContactShare loopShare : csList){
            if(loopShare.contactId != null && loopShare.UserOrGroupId != null){
                contactShareContactIds.add(loopShare.contactId);
                contactShareGroupIds.add(loopShare.UserOrGroupId);
            }
        }
        if(contactShareContactIds.size() > 0 && contactShareGroupIds.size() > 0){
            queryContactShare += '(ContactId IN :contactShareContactIds AND UserOrGroupId IN :contactShareGroupIds AND RowCause != \'Manual\')';
        }
        
        Set<ContactShare> toInsert = new Set<ContactShare>();
        for(ContactShare loopShare : csList){
            if(loopShare.ContactId != null && loopShare.UserOrGroupId != null){
                toInsert.add(loopShare);
            }
        }
        
        //Collect Query AccountShare
        Map<String,ContactShare> contactShareMap = new Map<String,ContactShare>();
        if(contactShareContactIds.size() > 0 && contactShareGroupIds.size() > 0){
            for(ContactShare loopContactShare : database.query(queryContactShare)){
                contactShareMap.put(loopContactShare.ContactId + ':::' + loopContactShare.UserOrGroupId, loopContactShare);
            }
        }
        
        Set<ContactShare> toRemove = new Set<ContactShare>();
        for(ContactShare loopShare : toInsert){
            //Bug 39247:Exceed query length: Needs to be addressed before full copy org Fix
            if(contactShareMap.containsKey(loopShare.ContactId + ':::' + loopShare.UserOrGroupId)){
                toRemove.add(loopShare);
            }
            
            //Old Code
            //for(AccountShare loopAccountShare : database.query(queryAccountShare)){
            //    if(loopAccountShare.AccountId == loopShare.AccountId && loopAccountShare.UserOrGroupId == loopShare.UserOrGroupId){
            //        toRemove.add(loopShare);
            //    }
            //}
        }
        
        for(ContactShare loopShare : toRemove){
            toInsert.remove(loopShare);
        }
        
        System.debug('\n\ntoInsert: ' + toInsert);
        csList = new List<ContactShare>();   
        for(ContactShare loopShare : toInsert){
            if(loopShare.ContactId != null && loopShare.UserOrGroupId != null){
                csList.add(loopShare);
            }
        }
        
        if(csList != null && !csList.isEmpty()){
            insert csList;
        }
    }
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @param: tsraList - Territory Sales Rep List newATT - new AAT List
    @description: prepare wrapper object
    */  
    public static List<ContactAccessLevel> prepareCAList(List<OSM_Territory_Sales_Rep_Assignment__c> tsraList, List<OSM_HCP_Territory_Assignment__c> newHTA, Map<ID, Set<ID>> childMap ){
        List<ContactAccessLevel> caList = new List<ContactAccessLevel>();   
        
        //Prepare list for user id, territoryID, account access and account id
        for(OSM_Territory_Sales_Rep_Assignment__c tsra: tsraList){
            for(OSM_HCP_Territory_Assignment__c hta: newHTA){
                if(tsra.OSM_Territory_ID__c == hta.OSM_Territory_Name__c){
                    caList.add(new ContactAccessLevel(tsra.OSM_Sales_Rep__c, hta.OSM_HCP_Name__c, tsra.OSM_Contact_Access_Level__c));
                }
            }
        }
        if(!childMap.isEmpty()){
            for(OSM_Territory_Sales_Rep_Assignment__c tsra: tsraList){
                for(ID loopId: childMap.keySet()){
                    if(childMap.get(loopId).contains(tsra.OSM_Territory_ID__c)){
                        for(OSM_HCP_Territory_Assignment__c hta: newHTA){
                            if(loopId == hta.OSM_Territory_Name__c){
                                caList.add(new ContactAccessLevel(tsra.OSM_Sales_Rep__c, hta.OSM_HCP_Name__c, tsra.OSM_Contact_Access_Level__c));
                            }
                        }   
                    } 
                }          
            }
        }
        return caList ;
    }
    
    /*
    @author: patrick lorilla
    @date: 2 October 2014
    @description: Contact Share data list
    */    
    public class ContactAccessLevel{
        public ID userID {get; set;}
        public ID conID {get; set;}
        public String conAccess {get; set;}
        /*
        @author: patrick lorilla
        @date: 2 October 2014
        @description: constructor
        */
        public ContactAccessLevel(ID user, ID con, String access){
            userID = user;
            conID = con;
            conAccess = access;           
        } 
    }
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 23 Jan 2015
    @description: Create Territory History records
   */   
    public static void createTerritoryHistory(List<OSM_HCP_Territory_Assignment__c> newHTA){
        Set<Id> territoryIDS = new Set<Id>();
        for(OSM_HCP_Territory_Assignment__c htaLoop : newHTA){
            territoryIDS.add(htaLoop.OSM_Territory_Name__c);
        }
        
        Map<Id, OSM_Territory__c> mapTerritoryInfo = new Map<Id, OSM_Territory__c>();
        for(OSM_Territory__c terrLoop : [Select Id, Name, OSM_Area__c, OSM_Business_Unit__c, OSM_Description__c, OSM_Division__c, OSM_GHI_Region_ID__c, OSM_Global__c,  
                                        OSM_International_Area__c, OSM_Number_of_Parents__c, Partner_Account__c, Partner_Account_Id__c, OSM_Parent_Territory__c, 
                                        OSM_Region__c, OSM_Territory_Franchise__c, OSM_Territory_ID__c, OSM_Territory_Market__c, OSM_Territory_Type__c,
                                        OSM_Effective_Ending_Date__c, OSM_Effective_Start_Date__c From OSM_Territory__c Where Id IN: territoryIDS]){
            mapTerritoryInfo.put(terrLoop.Id, terrLoop);
        }
        List<OSM_Territory_History__c> insertTerritoryHistory = new List<OSM_Territory_History__c>();
        for(OSM_HCP_Territory_Assignment__c htaLoop : newHTA){
            if(mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c) != null) {
            OSM_Territory_History__c newTH = new OSM_Territory_History__c();
            //newTH.OSM_Account_Name__c = htaLoop.
            newTH.OSM_Area__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Area__c;
            newTH.OSM_Business_Unit__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Business_Unit__c;
            newTH.OSM_Deleted_Date__c = system.today();
            newTH.OSM_Description__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Description__c;
            newTH.OSM_Division__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Division__c;
            newTH.OSM_Effective_Ending_Date__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Effective_Ending_Date__c;
            newTH.OSM_Effective_Start_Date__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Effective_Start_Date__c;
            newTH.OSM_GHI_Region_ID__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_GHI_Region_ID__c;
            newTH.OSM_Global__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Global__c;
            newTH.OSM_HCP_Name__c = htaLoop.OSM_HCP_Name__c;
            newTH.OSM_International_Area__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_International_Area__c;
            newTH.OSM_Number_of_Parents__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Number_of_Parents__c;
            //newTH.OSM_OrderName__c = htaLoop.OSM_OrderName__c;
            newTH.OSM_Partner_Account__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).Partner_Account__c;
            //newTH.OSM_Partner_Account_ID__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).Partner_Account_Id__c;
            newTH.OSM_Parent_Territory__c= mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Parent_Territory__c;
            newTH.OSM_Region__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Region__c;
            newTH.OSM_Territory_name__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).Id;
            newTH.OSM_Territory_Franchise__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Territory_Franchise__c;
            newTH.OSM_Territory_ID__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Territory_ID__c;
            newTH.OSM_Territory_Market__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Territory_Market__c;
            newTH.OSM_Territory_Type__c = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).OSM_Territory_Type__c;
            newTH.OSM_Territory_Nametext__c  = mapTerritoryInfo.get(htaLoop.OSM_Territory_Name__c).Name;
            insertTerritoryHistory.add(newTH);
            }
        }
        if(insertTerritoryHistory.size()>0){
            insert insertTerritoryHistory;
        }
    }
    
    /*
    @author: patrick lorilla
    @date: 16 June 2015
    @description: Update Number of Territories
    @param: newAAT- new  territory assignmenbts
   */   
    public static void updateNumberofTerritories(List<OSM_HCP_Territory_Assignment__c> newhTA){
        Set<ID> hcpIds = new Set<ID>();
        //Set<ID> aatIDS= new Set<ID>();
        Map<Id, Integer> hcpTerrCtrVal = new Map<Id, Integer>();
        System.debug('\n\n\n newhtaval'+ newhTA);
        for(OSM_HCP_Territory_Assignment__c hta: newhTA){ 
            hcpIds.add(hta.OSM_HCP_Name__c);   
        }
        List<OSM_HCP_Territory_Assignment__c> hcpList = [SELECT ID, OSM_HCP_Name__c from OSM_HCP_Territory_Assignment__c where OSM_HCP_Name__c IN: hcpIds];
        for(OSM_HCP_Territory_Assignment__c hta: hcpList){ 
            Integer ctr = 1;
            if(hcpTerrCtrVal.containsKey(hta.OSM_HCP_Name__c)){
                ctr = hcpTerrCtrVal.get(hta.OSM_HCP_Name__c);
                ++ctr;
            }
            hcpTerrCtrVal.put(hta.OSM_HCP_Name__c, ctr);
        }
        
        List<Contact> hcpUpdate = new List<Contact>();
        for(ID aid: hcpIds){
            Contact con = new Contact(Id = aid, OSM_Number_of_Territories__c = hcpTerrCtrVal.get(aid));
            hcpUpdate.add(con);
        }
        
        update hcpUpdate;
    }

}