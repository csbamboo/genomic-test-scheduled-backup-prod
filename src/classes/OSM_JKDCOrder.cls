/********************************************************************
    @author         : Rescian Rey
    @description    : JK Data Corp ordering utility class.
    @history:
        <date>                <author>                <description>
        MAY 25 2015           Rescian Rey             Created class
********************************************************************/
public with sharing class OSM_JKDCOrder {
    private OSM_JKDC_Ordering_Settings__c settings;
    private OSM_Kit_Order__c order;
    private User submitter;
    private OSM_Kit_Order_Line_Item__c item;

    private static final Map<String, List<String>> SHIPPING_METHOD_MAP = new Map<String, List<String>>{
        'FedEx Priority' => new List<String>{'FedEx Priority Overnight', '45'},
        'FedEx Standard' => new List<String>{'FedEx Standard Overnight', '46'},
        'FedEx 2 Day' => new List<String>{'FedEx 2nd Day', '41'},
        'FedEx International Economy' => new List<String>{'FedEx International Economy', '49'},
        'FedEx International Priority' => new List<String>{'FedEx International Priority', '48'}
    };

    private static final Map<String, String> PREPRINT_MAP = new Map<String, String>{
        'Practice Preprinted' => '1',
        'Pathology Preprinted' => '2',
        'Practice and Pathology Preprinted' => '3'
    };

    /********************************************************************
        @author         : Rescian Rey
        @description    : Initialization. Fetches the correct settings based on
                            the region. Sets the order and the submitter.
        @history:
            <date>                <author>                <description>
            MAY 25 2015           Rescian Rey             Created method
    ********************************************************************/
    public OSM_JKDCOrder(OSM_Kit_Order__c order, User submitter, OSM_JKDC_Ordering_Settings__c settings, OSM_Kit_Order_Line_Item__c koli) {
        this.submitter = submitter;
        this.order = order;
        this.settings = settings;
        item = koli;
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Sends the Http request to JKData Corp. Returns whether
                            the submission is successful or not.
        @history:
            <date>                <author>                <description>
            MAY 25 2015           Rescian Rey              Created method
    ********************************************************************/
    public Boolean submitOrder(){
        Boolean success = false;

        // Form/Post Data
        String testMode = settings.OSM_Test_Mode__c!=null?(settings.OSM_Test_Mode__c?'1':'0'):'1';
        String returnReceipt = settings.OSM_Return_Receipt__c!=null?(settings.OSM_Return_Receipt__c?'1':'0'):'0';
        String priorityCode = order.OSM_Rush_Order__c?'2':'1';
        String shippingMethodName = SHIPPING_METHOD_MAP.get(order.OSM_Shipping_Method__c)[0];
        String shippingMethodID = SHIPPING_METHOD_MAP.get(order.OSM_Shipping_Method__c)[1];
        String preprinting = item.OSM_Account_Information__c==null?'4':PREPRINT_MAP.get(item.OSM_Account_Information__c);
        String qty = item.OSM_Quantity__c==null?'0':String.valueOf(item.OSM_Quantity__c);
        Map<String, String> parameters = new Map<String, String>{
            'debug' => testMode,
            'transaction_key' => settings.OSM_Transaction_Key__c,
            'return_receipt' => returnReceipt,
            'ghi_ordernum' => order.Name,
            'processing_priority' => priorityCode,
            'notes' => order.OSM_Description__c,
            'submitter_name' => submitter.Name,
            'ship_method_name' => shippingMethodName,
            'shipping_method_id' => shippingMethodID,
            'preprinting_type' => preprinting,
            'account_number' => order.OSM_Account_Number__c,
            'account_phone' => order.OSM_Main_Phone__c,
            'account_fax' => order.OSM_Main_Fax__c,
            'account_company' => order.OSM_Account__r.Name,
            'account_address1' => order.OSM_Main_Address_Line_1__c,
            'account_address2' => order.OSM_Main_Address_Line_2__c,
            'account_address3' => '',
            'account_city' => order.OSM_Main_City__c,
            'account_state_province' => order.OSM_Main_State__c,
            'account_postalcode' => order.OSM_Main_Zip__c,
            'account_country' => order.OSM_Main_Country_Code__c,
            'ship_company' => order.OSM_Account__r.Name,
            'ship_attn_to' => order.OSM_Ship_To_Address_Contact__c,
            'ship_address1' => order.OSM_Ship_To_Address_Line_1__c,
            'ship_address2' => order.OSM_Ship_To_Address_Line_2__c,
            'ship_address3' => '',
            'ship_city' => order.OSM_Ship_To_City__c,
            'ship_state_province' => order.OSM_Ship_To_State__c,
            'ship_postalcode' => order.OSM_Ship_To_Zip__c,
            'ship_country' => order.OSM_Ship_To_Country_Code__c,
            'ship_phone' => order.OSM_Ship_To_Phone__c,
            'ship_fax' => order.OSM_Ship_To_Fax__c,
            'product_id' => item.OSM_Product__r.Name,
            'quantity' => qty
        };

        // Form body construction
        String boundary = '__8LA4YWxkTrZl0gW__';
        String delimiter = '\n';

        String formData = '';
        for(String k: parameters.keySet()){
            String value = parameters.get(k)==null?'':parameters.get(k);

            formData += '--' + boundary + delimiter;
            formData += 'Content-Disposition: form-data; name="'+ k +'"' + delimiter;
            formData += delimiter;
            formData += value + delimiter;
        }

        formData += '--' + boundary + '--';

        // Construct the request
        HttpRequest request = new HttpRequest();
        request.setTimeout(20000); // 20 second timeout, phew
        request.setEndpoint(settings.OSM_JKDC_Post_URL__c);
        //request.setEndpoint('http://requestb.in/qbw5l0qb');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);

        Blob authBlob = Blob.valueOf(settings.OSM_Username__c + ':' + settings.OSM_Password__c);
        String auth = EncodingUtil.base64Encode(authBlob);
        request.setHeader('Authorization', 'Basic ' + auth);

        request.setBody(formData);
        request.setHeader('Content-Length', String.valueof(formData.length()));

        HTTPResponse response;

        try{
            Http http = new Http();
            response = http.send(request);
            String body = response.getBody();
            System.debug(body);

            if(testMode == '1' && response.getStatusCode() == 200){
                success = true;
            }else{
                if(body.contains('Transaction received')){
                    success = true;
                }
            }
            
        }catch(CalloutException ex){
            System.debug(ex.getMessage());
        }

        return success;
    }
}