/*
    @author: Rescian Rey
    @description: Extension of the Kit Order Edit Override
    @createdDate: MAY 04 2015
    @history:
    <date>     <author>    <change_description>
*/
public with sharing class OSM_KitOrderAddressSelect {
    public OSM_Kit_Order__c kitOrder {get; set;}
    public String selected {get; set;}
    public boolean disableSave {get; set;}

    public OSM_KitOrderAddressSelect(ApexPages.StandardController std) {
        disableSave = false;
        if(std.getId() != null){
            kitOrder = [SELECT Id, Name, OSM_Shipping_Address__c, OSM_Account__c FROM OSM_Kit_Order__c WHERE Id = :std.getId()];
        }
    }

    public List<OSM_Address_Affiliation__c> getAddresses() {
        List<OSM_Address_Affiliation__c> options = new List<OSM_Address_Affiliation__c>();
        if(kitOrder.OSM_Account__c != null){
            for(OSM_Address_Affiliation__c address: [SELECT Id,
                    Name,
                    OSM_Type__c,
                    OSM_Address_Line_1__c,
                    OSM_Address_Line_2__c,
                    OSM_City__c,
                    OSM_State__c,
                    OSM_Zip__c,
                    OSM_Country__c,
                    OSM_Phone_Number__c,
                    OSM_Fax__c,
                    OSM_Email__c
                    FROM OSM_Address_Affiliation__c
                    WHERE OSM_Account__c = :kitOrder.OSM_Account__c]){
                if(address.OSM_Type__c != null && address.OSM_Type__c.contains('Ship To')){
                    options.add(address);
                }
                
            }
        }

        if(options.isEmpty()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,
                'No shipping address found. Please check the related account.'));
            disableSave = true;
        }
        return options;
    }

    public PageReference save(){
        kitOrder.OSM_Shipping_Address__c = selected;
        try{
            update kitOrder;
        }catch(DmlException e){
            // do nothing, just display the error
        }

        return null;
    }
}