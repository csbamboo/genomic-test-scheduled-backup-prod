/*
    @author: Rescian Rey
    @description:
    @createdDate:
    @history:
    <date>          <author>         <change_description>
    08-11-2015    Jerome Liwanag    Modified Class for Test Code Coverage
*/
@isTest(seeAllData=true)
private class OSM_KitOrderAddressSelect_Test {
    
	private static testmethod void Testmethod1() {
	    
	    PageReference pageRef = Page.OSM_KitOrderAddressSelect;
        Test.setCurrentPage(pageRef);
	    
	    Account acct = OSM_DataFactory.createAccount('Test Account');
	    insert acct;
		
		OSM_Kit_Order__c kitOrder = new OSM_Kit_Order__c(OSM_Account__c = acct.Id);
		insert kitOrder;
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(kitOrder);
		ApexPages.currentPage().getParameters().put('Id',kitOrder.id);
		OSM_KitOrderAddressSelect cont = new OSM_KitOrderAddressSelect(sc);  
		
		Test.startTest();
		cont.getAddresses();
		cont.save();
		Test.stopTest();
		
		
	}
	
	private static testmethod void Testmethod2() {
	    Record_Type__c recordLists = OSM_DataFactory.recordLists();
	    
	    PageReference pageRef = Page.OSM_KitOrderAddressSelect;
        Test.setCurrentPage(pageRef);
	    
	    Account acct = OSM_DataFactory.createAccount('Test Account');
	    insert acct;
	    
	    OSM_Address__c address1 = OSM_DataFactory.createAddress('Address Line 1', 'Address Line 2', 'New York', 'United States', 'New York', '1242');
	    insert address1;
	    
	    OSM_Address_Affiliation__c testAffiliation = OSM_DataFactory.createAddressAffiliation(acct.Id, 
                                                                                              address1.Id, 
                                                                                              'Test Attention', 
                                                                                              false, 
                                                                                              'testEmail@test.com', 
                                                                                              'Test Fax', 
                                                                                              null,
                                                                                              null, 
                                                                                              true, 
                                                                                              '123123', 
                                                                                              'Ship To', 
                                                                                              recordLists.HCO_Address_Affiliation__c);
                                                                                              
        insert testAffiliation;
        
		OSM_Kit_Order__c kitOrder = new OSM_Kit_Order__c(OSM_Account__c = acct.Id);
		insert kitOrder;
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(kitOrder);
		ApexPages.currentPage().getParameters().put('Id',kitOrder.id);
		OSM_KitOrderAddressSelect cont = new OSM_KitOrderAddressSelect(sc);  
		
		Test.startTest();
		cont.getAddresses();
		cont.save();
		Test.stopTest();
		
	}
}