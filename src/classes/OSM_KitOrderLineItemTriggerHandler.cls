/********************************************************************
    @author         : Rescian Rey
    @description    : Kit Order Line Item Trigger Handler
        


    @history:
        <date>                <author>                <description>
        JUN 5 2015            Rescian Rey             Created Class
********************************************************************/
public with sharing class OSM_KitOrderLineItemTriggerHandler extends OSM_TriggerHandlerBase {
    public static Boolean bypassEditValidation = false;

    public override void MainEntry(String TriggerObject, Boolean IsBefore, Boolean IsDelete, Boolean 
                                    IsAfter, Boolean IsInsert, Boolean IsUpdate, Boolean IsExecuting, List<SObject> newlist, Map<ID, 
                                    SObject> newmap, List<SObject> oldlist, Map<ID, SObject> oldmap){
        
        try{
            if( isInsert && isBefore ){
                // Insert code for Before Insert here
            }

            if( isInsert && isAfter ){
                // Insert code for After Insert here
            }

            if( isUpdate && isBefore ){
                // Insert code for Before Update here
                preventEditIfKitOrderNotActive((List<OSM_Kit_Order_Line_Item__c>)newlist);
            }

            if( isUpdate && isAfter ){
                // Insert code for After Update here
            }

            if( isDelete && isBefore ){
                // Insert code for Before delete here
            }

            if( isDelete && isAfter ){
                // Insert code for After delete here
            }
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }finally{   
            //Reset the active function to null in case this class was first in stack
            //this indicates that the trigger has finished firing so it no longer needs to control flow
            OSM_CentralDispatcher.activeFunction = null;
        }
    }

    // Optional override, if this method is not specified then the base class method will fire the Main 
    // entry method of the appropriate controller
    public override void InProgressEntry(String TriggerObject, Boolean IsBefore, Boolean 
                                            IsDelete, Boolean IsAfter, Boolean IsInsert,
                                            Boolean IsUpdate, Boolean IsExecuting,
                                            List<SObject> newlist, Map<ID, SObject> newmap, 
                                            List<SObject> oldlist, Map<ID,SObject> oldmap){

        if(TriggerObject == 'OSM_KitOrderLineItemTrigger'){
            // Stop trigger here
        }
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Prevent edit of Kit Order Line Item if parent Kit Order is not active and 
                            user is not a Sys Ad.
        @history:
            <date>                <author>                <description>
            JUN 5 2015            Rescian Rey             Created Method
    ********************************************************************/
    public void preventEditIfKitOrderNotActive(List<OSM_Kit_Order_Line_Item__c> kolis){

        Id sysAdID = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        if(UserInfo.getProfileId() == sysAdID || bypassEditValidation){
            return;
        }

        // Get related Kit Order
        Map<Id, List<OSM_Kit_Order_Line_Item__c>> kitOrdersToCheck = new Map<Id, List<OSM_Kit_Order_Line_Item__c>>();
        for(OSM_Kit_Order_Line_Item__c koli: kolis){
            if(koli.OSM_Kit_Order__c != null){
                if(!kitOrdersToCheck.containsKey(koli.OSM_Kit_Order__c)){
                    kitOrdersToCheck.put(koli.OSM_Kit_Order__c, (new List<OSM_Kit_Order_Line_Item__c>()));
                }
                kitOrdersToCheck.get(koli.OSM_Kit_Order__c).add(koli);
            }
        }

        // CHeck if related Kit Order is Active, if not raise an error
        for(OSM_Kit_Order__c ko: [SELECT Id, OSM_Status__c FROM OSM_Kit_Order__c
                    WHERE Id IN :kitOrdersToCheck.keySet()
                        AND OSM_Kit_Order_Status__c != 'Active']){
            for(OSM_Kit_Order_Line_Item__c koli: kitOrdersToCheck.get(ko.Id)){
                koli.addError('You do not have permission to edit this Kit Order Line Item because the related Kid Order Status is not Active.');
            }
        }
    }
}