@isTest
private class OSM_KitOrderLineItemTriggerHandler_Test {

    private static testMethod void test() {
        //Manage trigger logic with custom setting
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Study_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = false;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Order_Role_Trigger__c = false;
        insert triggerSwitch;
        
        Id conRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        
        
        
         
        /*** Test Data Creation ***/
        //Account
        Account account1 = new Account(Name = 'Test Account1', Fax = '2412241224', BillingStreet = 'billingStreet', BillingCity = 'city',
                                      BillingCountry = 'US', BillingPostalCode = '94010', BillingState = 'CA', OSM_Billing_ID__c = '12345',
                                      CurrencyIsoCode = 'USD', OSM_Franchise__c = 'Breast', Website = 'www.salesforce.com', 
                                      OSM_Main_Phone_Extension__c = '4324', OSM_Preferred_Method_of_Contact__c = 'Any', 
                                      OSM_Delivery_method_of_report__c = 'Fax', OSM_Followup_Business_Days__c = 21,
                                      OSM_Type__c = 'Lab', OSM_Language_Preference__c = 'English', OSM_National_Provider_ID__c = 'asdasf',
                                      OSM_Oncologists_at_Account__c = 13, OSM_Order_Instructions__c = 'asfasf', OSM_Other_Instructions__c = 'asfafa',
                                      OSM_Other_specialty__c = 'asfasas', OSM_Patient_Potential__c = 12, OSM_Potential__c = 'High', OSM_Priority__c = 'Tier 100',
                                      OSM_Specialty__c = 'Hospital', OSM_Specimen_Instructions__c = 'DOS Required', OSM_Country_Code__c = 'US');
        insert account1;
        
        Account account2 = new Account(Name = 'Test Account2', Fax = '2412241224', BillingStreet = 'billingStreet', BillingCity = 'city',
                                      BillingCountry = 'US', BillingPostalCode = '94010', BillingState = 'CA', OSM_Billing_ID__c = '12345',
                                      CurrencyIsoCode = 'USD', OSM_Franchise__c = 'Breast', Website = 'www.salesforce.com', 
                                      OSM_Main_Phone_Extension__c = '4324', OSM_Preferred_Method_of_Contact__c = 'Any', 
                                      OSM_Delivery_method_of_report__c = 'Fax', OSM_Followup_Business_Days__c = 21,
                                      OSM_Type__c = 'Lab', OSM_Language_Preference__c = 'English', OSM_National_Provider_ID__c = 'asdasf',
                                      OSM_Oncologists_at_Account__c = 13, OSM_Order_Instructions__c = 'asfasf', OSM_Other_Instructions__c = 'asfafa',
                                      OSM_Other_specialty__c = 'asfasas', OSM_Patient_Potential__c = 12, OSM_Potential__c = 'High', OSM_Priority__c = 'Tier 100',
                                      OSM_Specialty__c = 'Hospital', OSM_Specimen_Instructions__c = 'DOS Required', OSM_Country_Code__c = 'US');
        insert account2;
        
        //Contact
        Contact contact1 = new Contact(LastName = 'testContactName' , RecordTypeId = conRecType, OSM_Status__c = 'Approved',
                                      OSM_Credentials__c = 'credentials', Email = 'email@testrest.com', OSM_Email_2__c  = 'email2@testrest.com',
                                      Fax = '1212343456', OSM_Gender__c = 'Male', OSM_Middle_Name__c = 'MiddleName', MobilePhone = '1234512345', OSM_HCP_NPI__c = 'HCP',
                                      OSM_Nickname__c = 'NickName', Description = 'Sample Description', OSM_Pager__c = '1234554321', FirstName = 'FirstName', MailingStreet = 'Chesapeake Drive',
                                      MailingCity = 'Redwood City', MailingState = 'CA', MailingCountry = 'US', MailingPostalCode = '94010', OSM_Specialty__c = 'Urologist', Salutation = 'Dr.');
        insert contact1;
        
        //Address
        OSM_Address__c address = OSM_DataFactory.createAddress('AddLine1', 'AddLine2', 'Redwood City', 'US', 'CA', '94010');
        insert address;
        
        //Address Affiliation
        OSM_Address_Affiliation__c  addressAff1 = new OSM_Address_Affiliation__c (OSM_Account__c =  account1.Id, OSM_Address__c = address.Id,
                                                        OSM_Attention_To__c = 'Attention', OSM_Billing__c = true,
                                                        OSM_Email__c = 'email@resttest.com', OSM_Fax__c = '2342432423',
                                                        OSM_HCP__c = contact1.Id, OSM_Main_Address__c = true, OSM_Type__c = 'Main');
        insert addressAff1;
        
        OSM_Address_Affiliation__c  addressAff2 = new OSM_Address_Affiliation__c (OSM_Account__c =  account1.Id, OSM_Address__c = address.Id,
                                                        OSM_Attention_To__c = 'Attention', OSM_Billing__c = true,
                                                        OSM_Email__c = 'email@resttest.com', OSM_Fax__c = '2342432423',
                                                        OSM_HCP__c = contact1.Id, OSM_Main_Address__c = true, OSM_Type__c = 'Ship To');
        insert addressAff2;
        
        OSM_Address_Affiliation__c  addressAff3 = new OSM_Address_Affiliation__c (OSM_Account__c =  account2.Id, OSM_Address__c = address.Id,
                                                        OSM_Attention_To__c = 'Attention', OSM_Billing__c = true,
                                                        OSM_Email__c = 'email@resttest.com', OSM_Fax__c = '2342432423',
                                                        OSM_HCP__c = contact1.Id, OSM_Main_Address__c = true, OSM_Type__c = 'Ship To');
        insert addressAff3;
        
        OSM_Address_Affiliation__c  addressAff4 = new OSM_Address_Affiliation__c (OSM_Account__c =  account2.Id, OSM_Address__c = address.Id,
                                                        OSM_Attention_To__c = 'Attention', OSM_Billing__c = true,
                                                        OSM_Email__c = 'email@resttest.com', OSM_Fax__c = '2342432423',
                                                        OSM_HCP__c = contact1.Id, OSM_Main_Address__c = true, OSM_Type__c = 'Ship To');
        insert addressAff4;
        
        Product2 prod2 = OSM_DataFactory.createProduct('Qty31|Qty49', true);
        prod2.OSM_Preprint__c = true;
        prod2.OSM_Region__c = 'United States';
        prod2.OSM_Kits__c = 'Block - English';
        prod2.OSM_Requisition_Forms__c = 'English';
        prod2.OSM_Shipping_Documents__c = 'FedEx Air Waybills (4 pack)';
        insert prod2;
        
        List<Product2> prod = [SELECT Id, Name FROM Product2 WHERE Name = :'Qty31|Qty49'];
        
         OSM_Kit_Order__c kitOrder1 = new OSM_Kit_Order__c (OSM_Account__c = account1.Id, OSM_Shipping_Address__c = addressAff1.Id);
        insert kitOrder1;
            
        OSM_Kit_Order__c kitOrder2 = new OSM_Kit_Order__c (OSM_Account__c = account2.Id);
        insert kitOrder2;
            
         Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser1231@testorg.com123123', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12321312@12321testorg.com');
         test.startTest();
        try{
           
        System.runAs(u) {                    
            
        
        OSM_Kit_Order_Line_Item__c kitOli2 = new OSM_Kit_Order_Line_Item__c(OSM_Kit_Order__c = kitOrder1.Id, 
        OSM_Quantity__c = '1', 
        //OSM_Region__c = 'EMEA', 
        //OSM_Kits__c = 'Block - German',
        OSM_Product__c = prod[0].Id, 
        OSM_Status__c = 'Success',
        OSM_Preprint__c = True,
        OSM_Region__c = 'United States',
        OSM_Kits__c = 'Block - English',
        OSM_Requisition_Forms__c = 'English',
        OSM_Shipping_Documents__c = 'FedEx Air Waybills (4 pack)'
        );
        
        insert kitOli2;
        
        kitOli2.OSM_Quantity__c = '2';
        
        
        update kitOli2;
        }
        System.assert(false, 'Exception expected');
        }
        catch(dmlException ex){
            
            System.assert(ex.getMessage().contains('You do not have permission to edit this Kit Order Line Item because the related Kid Order Status is not Active.'), 'message=' + ex.getMessage());
        }
        
        
        
        
        test.stopTest();
        

    }
}