/*
    @author: Rescian Rey
    @description: Trigger for OSM_Kit_Order__c
    @history:
        MAY 1 2015     Rescian Rey    Created.
        MAY 11 2015    Rescian Rey    Implemented Trigger Framework.

    On Before Triggers:
        Insert:
            - populateMainAddressFields
            - populateShippingAddressFields
        Update:
            - preventOperationIfNotActive
            - populateMainAddressFields
            - populateShippingAddressFields
        Delete
            - preventOperationIfNotActive
*/
public with sharing class OSM_KitOrderTriggerHandler extends OSM_TriggerHandlerBase {

    private static final String OP_DELETE = 'delete';
    private static final String OP_EDIT = 'edit';
    public static Boolean bypassEditValidation = false;

    public override void MainEntry(String TriggerObject, Boolean IsBefore, Boolean IsDelete, Boolean 
                                    IsAfter, Boolean IsInsert, Boolean IsUpdate, Boolean IsExecuting, List<SObject> newlist, Map<ID, 
                                    SObject> newmap, List<SObject> oldlist, Map<ID, SObject> oldmap){
        
        try{
            if( isInsert && isBefore ){
                // Insert code for Before Insert here
                populateMainAddressFields((List<OSM_Kit_Order__c>)newlist, null);
                populateShippingAddressFields((List<OSM_Kit_Order__c>)newlist, null);
            }

            if( isInsert && isAfter ){
                // Insert code for After Insert here
            }

            if( isUpdate && isBefore ){
                // Insert code for Before Update here
                preventOperationIfNotActive((List<OSM_Kit_Order__c>)newlist, (Map<Id, OSM_Kit_Order__c>)oldmap, OP_EDIT);
                populateMainAddressFields((List<OSM_Kit_Order__c>)newlist, (Map<Id, OSM_Kit_Order__c>)oldmap);
                populateShippingAddressFields((List<OSM_Kit_Order__c>)newlist, (Map<Id, OSM_Kit_Order__c>)oldmap);
            }

            if( isUpdate && isAfter ){
                // Insert code for After Update here
            }

            if( isDelete && isBefore ){
                // Insert code for Before delete here
                preventOperationIfNotActive((List<OSM_Kit_Order__c>)oldlist, null, OP_DELETE);
            }

            if( isDelete && isAfter ){
                // Insert code for After delete here
            }
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }finally{   
            //Reset the active function to null in case this class was first in stack
            //this indicates that the trigger has finished firing so it no longer needs to control flow
            OSM_CentralDispatcher.activeFunction = null;
        }
    }

    // Optional override, if this method is not specified then the base class method will fire the Main 
    // entry method of the appropriate controller
    public override void InProgressEntry(String TriggerObject, Boolean IsBefore, Boolean 
                                            IsDelete, Boolean IsAfter, Boolean IsInsert,
                                            Boolean IsUpdate, Boolean IsExecuting,
                                            List<SObject> newlist, Map<ID, SObject> newmap, 
                                            List<SObject> oldlist, Map<ID,SObject> oldmap){

        if(TriggerObject == 'OSM_KitOrderTrigger'){
            // Stop trigger here
        }
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Prevent edit/delete of Kit Order if not active and not a Sys Ad.
        @history:
            <date>                <author>                <description>
            JUN 2 2015            Rescian Rey             Created Method
    ********************************************************************/
    public void preventOperationIfNotActive(List<OSM_Kit_Order__c> orders, Map<Id, OSM_Kit_Order__c> oldOrders, String op){

        Id sysAdID = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        if(UserInfo.getProfileId() == sysAdID || bypassEditValidation){
            return;
        }

        for(OSM_Kit_Order__c order: orders){
            if((oldOrders == null && order.OSM_Kit_Order_Status__c != 'Active') ||
                (oldOrders != null && oldOrders.get(order.Id).OSM_Kit_Order_Status__c != 'Active')){
                order.addError('You do not have permission to ' + op + ' this Kit Order because the Kid Order Status is not Active.');
            }
        }
    }

    /*
        @author: Rescian Rey
        @description: Populates the Main Address Fields from the related Account's Address Affiliations.
                    Gets the Address Affiliation of type Main and 
                    copies it to the Kit Order's main address fields.

                    Trigger entry criteria:
                    - Account should have a value AND
                    - Main Address line 1 is null OR
                        - Main Address line 1 is not null, but is an empty string OR
                        - Previous Account is not equal to current Account (on update)

        @createdDate: MAY 1 2015
        @history:
            <date>     <author>    <change_description>
    */
    public void populateMainAddressFields(List<OSM_Kit_Order__c> orders, Map<Id, OSM_Kit_Order__c> oldOrderMap){
        Map<Id, Set<OSM_Kit_Order__c>> ordersToUpdate = new Map<Id, Set<OSM_Kit_Order__c>>();

        // Get orders with populated Account field
        // Check trigger entry condition here
        for(OSM_Kit_Order__c ord: orders){
            if(ord.OSM_Account__c != null && (ord.OSM_Main_Address_Line_1__c == null
                    || (ord.OSM_Main_Address_Line_1__c != null
                    && ord.OSM_Main_Address_Line_1__c == '')
                    || (oldOrderMap != null
                    && ord.OSM_Account__c != oldOrderMap.get(ord.Id).OSM_Account__c))){
                if(!ordersToUpdate.containsKey(ord.OSM_Account__c)){
                    ordersToUpdate.put(ord.OSM_Account__c, new Set<OSM_Kit_Order__c>());
                }

                ordersToUpdate.get(ord.OSM_Account__c).add(ord);
            }
        }

        // Get Main Address Affiliations related to accounts, and then populate order address fields
        if(!ordersToUpdate.isEmpty()){
            for(OSM_Address_Affiliation__c mainAddress: [SELECT Id,
                        OSM_Account__c,
                        OSM_Address_Line_1__c,
                        OSM_Address_Line_2__c,
                        OSM_City__c,
                        OSM_State__c,
                        OSM_Zip__c,
                        OSM_Country__c,
                        OSM_Country_Code__c,
                        OSM_Phone_Number__c,
                        OSM_Fax__c,
                        OSM_Email__c,
                        OSM_Type__c
                    FROM OSM_Address_Affiliation__c
                    WHERE OSM_Account__c IN :ordersToUpdate.keySet()]){

                if(mainAddress.OSM_Type__c == null || !mainAddress.OSM_Type__c.contains('Main')){
                    continue;
                }

                // Populate Main Address fields
                for(OSM_Kit_Order__c ord: ordersToUpdate.get(mainAddress.OSM_Account__c)){
                    ord.OSM_Main_Address_Line_1__c = mainAddress.OSM_Address_Line_1__c;
                    ord.OSM_Main_Address_Line_2__c = mainAddress.OSM_Address_Line_2__c;
                    ord.OSM_Main_City__c = mainAddress.OSM_City__c;
                    ord.OSM_Main_State__c = mainAddress.OSM_State__c;
                    ord.OSM_Main_Zip__c = mainAddress.OSM_Zip__c;
                    ord.OSM_Main_Country__c = mainAddress.OSM_Country__c;
                    ord.OSM_Main_Country_Code__c = mainAddress.OSM_Country_Code__c;
                    ord.OSM_Main_Phone__c = mainAddress.OSM_Phone_Number__c;
                    ord.OSM_Main_Email__c = mainAddress.OSM_Email__c;
                    ord.OSM_Main_Fax__c = mainAddress.OSM_Fax__c;
                }
            }
        }
    }


    /*
        @author: Rescian Rey
        @description: Populates the Shipping Address Fields from the related Account's Address Affiliations.
                    Gets the Address Affiliation of type Ship To and 
                    copies it to the Kit Order's shipping address fields.

                    Trigger entry criteria:
                    - Account should not be null AND
                    - Ship to Address Line 1 is null OR
                        - Ship to Address Line 1 is not null, but is an empty string OR
                        - Previous Account is not equal to current Account (on update) OR
                        - User has chosen a Shipping Address. That means, OSM_Shipping_Address__c field
                            of the Kit Order is populated.

                    Logic:
                    - If no shipping address is found, populate the shipping address
                        with the Main Address
                    - If there are multiple shipping addresses on the related Account, OSM_Shipping_Address__c
                        will be checked if the user has chosen a shipping address.
        @createdDate: MAY 1 2015
        @history:
            <date>     <author>    <change_description>
    */
    public void populateShippingAddressFields(List<OSM_Kit_Order__c> orders, Map<Id, OSM_Kit_Order__c> oldOrderMap){
        Map<Id, Set<OSM_Kit_Order__c>> ordersToUpdate = new Map<Id, Set<OSM_Kit_Order__c>>();

        //Get orders with populated Account field
        for(OSM_Kit_Order__c ord: orders){
            if(ord.OSM_Account__c != null && (ord.OSM_Ship_To_Address_Line_1__c == null
                    || (ord.OSM_Ship_To_Address_Line_1__c != null 
                    && ord.OSM_Ship_To_Address_Line_1__c.trim() == '')
                    || (oldOrderMap != null
                    && ord.OSM_Account__c != oldOrderMap.get(ord.Id).OSM_Account__c))
                    || (ord.OSM_Ship_To_Address_Line_1__c != null
                    && ord.OSM_Shipping_Address__c != null)){
                if(!ordersToUpdate.containsKey(ord.OSM_Account__c)){
                    ordersToUpdate.put(ord.OSM_Account__c, new Set<OSM_Kit_Order__c>());
                }

                ordersToUpdate.get(ord.OSM_Account__c).add(ord);
            }
        }

        if(!ordersToUpdate.isEmpty()){
            // Get Account Ship To Addesses and then populate order address fields
            Map<Id, List<OSM_Address_Affiliation__c>> shipToAddresses = new Map<Id, List<OSM_Address_Affiliation__c>>();
            Map<Id, OSM_Address_Affiliation__c> mainAddress = new Map<Id, OSM_Address_Affiliation__c>();
            for(OSM_Address_Affiliation__c address: [SELECT Id,
                        OSM_Account__c,
                        OSM_Address_Line_1__c,
                        OSM_Address_Line_2__c,
                        OSM_Country_Code__c,
                        OSM_City__c,
                        OSM_State__c,
                        OSM_Zip__c,
                        OSM_Country__c,
                        OSM_Phone_Number__c,
                        OSM_Fax__c,
                        OSM_Email__c,
                        OSM_Type__c,
                        Name
                    FROM OSM_Address_Affiliation__c
                    WHERE OSM_Account__c IN :ordersToUpdate.keySet()]){

                // Collect Shipping Addresses
                if(address.OSM_Type__c != null && address.OSM_Type__c.contains('Ship To')){
                    if(!shipToAddresses.containsKey(address.OSM_Account__c)){
                        shipToAddresses.put(address.OSM_Account__c, new List<OSM_Address_Affiliation__c>());
                    }
                    shipToAddresses.get(address.OSM_Account__c).add(address);
                }

                // Collect Main Addresses
                if(address.OSM_Type__c != null && address.OSM_Type__c.contains('Main')){
                    mainAddress.put(address.OSM_Account__c, address);
                }
            }

            for(Id accId: ordersToUpdate.keySet()){
                for(OSM_Kit_Order__c ord: ordersToUpdate.get(accId)){
                    OSM_Address_Affiliation__c address;
                    if(shipToAddresses.containsKey(accId)){

                        // if there's only one shipping address, use that as 
                        // the shipping address for this order
                        if(shipToAddresses.get(accId).size() == 1){
                            address = shipToAddresses.get(ord.OSM_Account__c).get(0);
                        }else if(ord.OSM_Shipping_Address__c != null){

                            // If multiple shipping addresses, 
                            // check if OSM_Shipping_Address__c is populated.
                            for(OSM_Address_Affiliation__c addr: shipToAddresses.get(accId)){
                                if(addr.Id == ord.OSM_Shipping_Address__c){
                                    address = addr;
                                    ord.OSM_Shipping_Address__c = null;
                                    break;
                                }
                            }
                        }else{
                            // Otherwise, clear the address fields
                            ord.OSM_Ship_To_Address_Line_1__c = null;
                            ord.OSM_Ship_To_Address_Line_2__c = null;
                            ord.OSM_Ship_To_City__c = null;
                            ord.OSM_Ship_To_State__c = null;
                            ord.OSM_Ship_To_Zip__c = null;
                            ord.OSM_Ship_To_Country__c = null;
                            ord.OSM_Ship_To_Country_Code__c = null;
                            ord.OSM_Ship_To_Phone__c = null;
                            ord.OSM_Ship_To_Email__c = null;
                            ord.OSM_Ship_To_Fax__c = null;
                        }
                    }else if(mainAddress.containsKey(accId)){
                        // If no shipping address on Account, use the this order's main address
                        // for shipping address
                        address = mainAddress.get(accId);
                    }

                    // Populate shipping address fields
                    if(address != null){
                        ord.OSM_Ship_To_Address_Line_1__c = address.OSM_Address_Line_1__c;
                        ord.OSM_Ship_To_Address_Line_2__c = address.OSM_Address_Line_2__c;
                        ord.OSM_Ship_To_City__c = address.OSM_City__c;
                        ord.OSM_Ship_To_State__c = address.OSM_State__c;
                        ord.OSM_Ship_To_Zip__c = address.OSM_Zip__c;
                        ord.OSM_Ship_To_Country__c = address.OSM_Country__c;
                        ord.OSM_Ship_To_Country_Code__c = address.OSM_Country_Code__c;
                        ord.OSM_Ship_To_Phone__c = address.OSM_Phone_Number__c;
                        ord.OSM_Ship_To_Email__c = address.OSM_Email__c;
                        ord.OSM_Ship_To_Fax__c = address.OSM_Fax__c;
                    }
                }
            }
        }
    }
}