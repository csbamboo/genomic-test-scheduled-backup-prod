/**    Description    :    Test class for OSM_KitOrderTriggerHandler. 
  *                          
  *    Created By      :    
  * 
  *    Created Date    :    08/04/2015
  *
  *    Revisison Log   :    V_1.0 - Created
  *
  *    Coverage        :    100%
**/
@isTest(seeAllData = false)
private class OSM_KitOrderTriggerHandler_Test{
  
    private static testMethod void testMainEntry() {
        //Manage trigger logic with custom setting
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Study_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = false;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Kit_Order__c = true;
        insert triggerSwitch;

        Id conRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
         
        /*** Test Data Creation ***/
        //Account
        Account account1 = new Account(Name = 'Test Account1', Fax = '2412241224', BillingStreet = 'billingStreet', BillingCity = 'city',
                                      BillingCountry = 'US', BillingPostalCode = '94010', BillingState = 'CA', OSM_Billing_ID__c = '12345',
                                      CurrencyIsoCode = 'USD', OSM_Franchise__c = 'Breast', Website = 'www.salesforce.com', 
                                      OSM_Main_Phone_Extension__c = '4324', OSM_Preferred_Method_of_Contact__c = 'Any', 
                                      OSM_Delivery_method_of_report__c = 'Fax', OSM_Followup_Business_Days__c = 21,
                                      OSM_Type__c = 'Lab', OSM_Language_Preference__c = 'English', OSM_National_Provider_ID__c = 'asdasf',
                                      OSM_Oncologists_at_Account__c = 13, OSM_Order_Instructions__c = 'asfasf', OSM_Other_Instructions__c = 'asfafa',
                                      OSM_Other_specialty__c = 'asfasas', OSM_Patient_Potential__c = 12, OSM_Potential__c = 'High', OSM_Priority__c = 'Tier 100',
                                      OSM_Specialty__c = 'Hospital', OSM_Specimen_Instructions__c = 'DOS Required', OSM_Country_Code__c = 'US');
        insert account1;
        
        Account account2 = new Account(Name = 'Test Account2', Fax = '2412241224', BillingStreet = 'billingStreet', BillingCity = 'city',
                                      BillingCountry = 'US', BillingPostalCode = '94010', BillingState = 'CA', OSM_Billing_ID__c = '12345',
                                      CurrencyIsoCode = 'USD', OSM_Franchise__c = 'Breast', Website = 'www.salesforce.com', 
                                      OSM_Main_Phone_Extension__c = '4324', OSM_Preferred_Method_of_Contact__c = 'Any', 
                                      OSM_Delivery_method_of_report__c = 'Fax', OSM_Followup_Business_Days__c = 21,
                                      OSM_Type__c = 'Lab', OSM_Language_Preference__c = 'English', OSM_National_Provider_ID__c = 'asdasf',
                                      OSM_Oncologists_at_Account__c = 13, OSM_Order_Instructions__c = 'asfasf', OSM_Other_Instructions__c = 'asfafa',
                                      OSM_Other_specialty__c = 'asfasas', OSM_Patient_Potential__c = 12, OSM_Potential__c = 'High', OSM_Priority__c = 'Tier 100',
                                      OSM_Specialty__c = 'Hospital', OSM_Specimen_Instructions__c = 'DOS Required', OSM_Country_Code__c = 'US');
        insert account2;
        
        //Contact
        Contact contact1 = new Contact(LastName = 'testContactName' , RecordTypeId = conRecType, OSM_Status__c = 'Approved',
                                      OSM_Credentials__c = 'credentials', Email = 'email@testrest.com', OSM_Email_2__c  = 'email2@testrest.com',
                                      Fax = '1212343456', OSM_Gender__c = 'Male', OSM_Middle_Name__c = 'MiddleName', MobilePhone = '1234512345', OSM_HCP_NPI__c = 'HCP',
                                      OSM_Nickname__c = 'NickName', Description = 'Sample Description', OSM_Pager__c = '1234554321', FirstName = 'FirstName', MailingStreet = 'Chesapeake Drive',
                                      MailingCity = 'Redwood City', MailingState = 'CA', MailingCountry = 'US', MailingPostalCode = '94010', OSM_Specialty__c = 'Urologist', Salutation = 'Dr.');
        insert contact1;
        
        //Address
        OSM_Address__c address = OSM_DataFactory.createAddress('AddLine1', 'AddLine2', 'Redwood City', 'US', 'CA', '94010');
        insert address;
        
        //Address Affiliation
        OSM_Address_Affiliation__c  addressAff1 = new OSM_Address_Affiliation__c (OSM_Account__c =  account1.Id, OSM_Address__c = address.Id,
                                                        OSM_Attention_To__c = 'Attention', OSM_Billing__c = true,
                                                        OSM_Email__c = 'email@resttest.com', OSM_Fax__c = '2342432423',
                                                        OSM_HCP__c = contact1.Id, OSM_Main_Address__c = true, OSM_Type__c = 'Main');
        insert addressAff1;
        
        OSM_Address_Affiliation__c  addressAff2 = new OSM_Address_Affiliation__c (OSM_Account__c =  account1.Id, OSM_Address__c = address.Id,
                                                        OSM_Attention_To__c = 'Attention', OSM_Billing__c = true,
                                                        OSM_Email__c = 'email@resttest.com', OSM_Fax__c = '2342432423',
                                                        OSM_HCP__c = contact1.Id, OSM_Main_Address__c = true, OSM_Type__c = 'Ship To');
        insert addressAff2;
        
        OSM_Address_Affiliation__c  addressAff3 = new OSM_Address_Affiliation__c (OSM_Account__c =  account2.Id, OSM_Address__c = address.Id,
                                                        OSM_Attention_To__c = 'Attention', OSM_Billing__c = true,
                                                        OSM_Email__c = 'email@resttest.com', OSM_Fax__c = '2342432423',
                                                        OSM_HCP__c = contact1.Id, OSM_Main_Address__c = true, OSM_Type__c = 'Ship To');
        insert addressAff3;
        
        OSM_Address_Affiliation__c  addressAff4 = new OSM_Address_Affiliation__c (OSM_Account__c =  account2.Id, OSM_Address__c = address.Id,
                                                        OSM_Attention_To__c = 'Attention', OSM_Billing__c = true,
                                                        OSM_Email__c = 'email@resttest.com', OSM_Fax__c = '2342432423',
                                                        OSM_HCP__c = contact1.Id, OSM_Main_Address__c = true, OSM_Type__c = 'Ship To');
        insert addressAff4;
        
        Test.StartTest();
            //Kit Order
            OSM_Kit_Order__c kitOrder1 = new OSM_Kit_Order__c (OSM_Account__c = account1.Id, OSM_Shipping_Address__c = addressAff1.Id);
            insert kitOrder1;
            
            OSM_Kit_Order__c kitOrder2 = new OSM_Kit_Order__c (OSM_Account__c = account2.Id);
            insert kitOrder2;
            
            kitOrder1.OSM_Account__c = account2.Id;
            kitOrder1.OSM_Ship_To_Address_Line_1__c = '';
            update kitOrder1;
            
            kitOrder2.OSM_Account__c = account2.Id;
            kitorder2.OSM_Shipping_Address__c = addressAff3.Id;
            kitOrder2.OSM_Ship_To_Address_Line_1__c = 'Test Address Line';
            update kitOrder2;
            
            kitOrder2.OSM_Account__c = account1.Id;
            kitorder2.OSM_Shipping_Address__c = addressAff1.Id;
            kitOrder2.OSM_Ship_To_Address_Line_1__c = ' ';
            update kitOrder2;
            
            delete kitOrder2;
        Test.StopTest();
        
    }
}