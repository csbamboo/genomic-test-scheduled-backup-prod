/**
 * File Info
 * ----------------------------------
 * @filename       OSM_ManageAddressExt.cls
 * @created        24.SEPT.2014
 * @author         Kristian Vegerano
 * @description    Extension class for OSM_AddressEdit Page. 
 * @history        24.SEPT.2014 - Kristian Vegerano - Created  
 */
public class OSM_ManageAddressExt{
    public OSM_Address__c pageAddress {get;set;}
    public Boolean doShowName {get;set;}
    public List<SelectOption> countryList {get;set;}
    public List<SelectOption> stateList {get;set;}
    
    /**
     * @author         Kristian Vegerano
     * @description    Constructor. 
     * @history        24.SEPT.2014 - Kristian Vegerano - Created  
     */
    public OSM_ManageAddressExt(ApexPages.StandardController stdController) {
        pageAddress = (OSM_Address__c)stdController.getRecord();
        
        Set<String> filterNames = new Set<String>();
        List<String> sortNames = new List<String>();
        
        if(pageAddress.Id != null){
            this.doShowName = true;
        }else{
            this.doShowName = false;
        }
        countryList = new List<SelectOption>();
        
        //Get Country Names
        countryList.add(new SelectOption('', '--None--'));
        for(OSM_Country_and_State__c loopCountryState : [SELECT Id, OSM_Country_Name__c 
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Active__c = true]){
            filterNames.add(loopCountryState.OSM_Country_Name__c);
        }
        if(filterNames.size() > 0){
            sortNames.addAll(filterNames);
            sortNames.sort();
            for(String loopCountryName : sortNames){
                countryList.add(new SelectOption(loopCountryName, loopCountryName));
            }
        }
        
        //Call dependent state values
        updateCountry();
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that updates the state picklist depending on the selected country value. 
     * @history        24.SEPT.2014 - Kristian Vegerano - Created  
     */
    public void updateCountry() {
        Set<String> filterNames = new Set<String>();
        List<String> sortNames = new List<String>();
        
        stateList = new List<SelectOption>();
        stateList.add(new SelectOption('', '--None--'));
        
        //Get Country Names
        for(OSM_Country_and_State__c loopCountryState : [SELECT Id, OSM_State_Name__c
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Active__c = true
                                                    AND OSM_Country_Name__c = :pageAddress.OSM_Country__c]){
            filterNames.add(loopCountryState.OSM_State_Name__c);
        }
        if(filterNames.size() > 1){
            //pageAddress.OSM_Zip__c = null;
            sortNames.addAll(filterNames);
            sortNames.sort();
            for(String loopStateName : sortNames){
                stateList.add(new SelectOption(loopStateName, loopStateName));
            }
        }else if(filterNames.size() == 1){
            updateState();
            
            // DQ - Fix added related to TFS Defect 26913: Reset value to null to remove the old state
            pageAddress.OSM_Country_and_State_ID__c = null;
            pageAddress.OSM_State__c = '';
            pageAddress.OSM_State_Code__c = '';
        }
        if(pageAddress.OSM_Country__c != null){
            OSM_Country_and_State__c countryState = [SELECT Id, OSM_State_Name__c, OSM_Country_Code__c, OSM_Customer_Service_Team__c
                                                     FROM OSM_Country_and_State__c
                                                     WHERE OSM_Active__c = true
                                                        AND OSM_Country_Name__c = :pageAddress.OSM_Country__c LIMIT 1];
            pageAddress.OSM_Country_and_State_ID__c = countryState.Id;
            pageAddress.OSM_Country_Code__c = countryState.OSM_Country_Code__c;
            pageAddress.OSM_Customer_Service_Team__c = countryState.OSM_Customer_Service_Team__c;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that updates the state picklist depending on the selected country value. 
     * @history        24.SEPT.2014 - Kristian Vegerano - Created  
     */
    public void updateState() {
        try{
            /*pageAddress.OSM_Zip__c = [SELECT Id, OSM_Zip_Code__c 
                                      FROM OSM_Country_and_State__c 
                                      WHERE OSM_Country_Name__c = :pageAddress.OSM_Country__c
                                          AND OSM_State_Name__c = :pageAddress.OSM_State__c].OSM_Zip_Code__c;*/
                                          
            if(pageAddress.OSM_State__c != null){
                OSM_Country_and_State__c countryState = [SELECT Id, OSM_Zip_Code__c, OSM_State_Code__c
                                                 FROM OSM_Country_and_State__c 
                                                 WHERE OSM_Country_Name__c = :pageAddress.OSM_Country__c
                                                     AND OSM_State_Name__c = :pageAddress.OSM_State__c LIMIT 1];
                                                     
                pageAddress.OSM_Country_and_State_ID__c = countryState.Id;
                pageAddress.OSM_State_Code__c = countryState.OSM_State_Code__c;
                 
            }                           
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
}