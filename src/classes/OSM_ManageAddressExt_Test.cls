/**
 * File Info
 * ----------------------------------
 * @filename       OSM_ManageAddressExt_Test.cls
 * @created        26.SEPT.2014
 * @author         Kristian Vegerano
 * @description    Test class for OSM_ManageAddressExt 
 * @history        26.SEPT.2014 - Kristian Vegerano - Created  
 */
 
@isTest
public class OSM_ManageAddressExt_Test{
    /**
     * @author         Kristian Vegerano
     * @description    Tests constructor for new Address
     * @history        26.SEPT.2014 - Kristian Vegerano - Created  
     */
    static testmethod void testPageConstructorWithoutId(){
        insert new List<OSM_Country_and_State__c>{OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12345'),
                                                  OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Quebec', 'AB', 'QA')};

        OSM_Address__c testAddress = new OSM_Address__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAddress);
        OSM_ManageAddressExt testExtension; 
        Test.startTest();
            testExtension = new OSM_ManageAddressExt(sc);
            testExtension.pageAddress.OSM_Country__c = testExtension.countryList[1].getValue();
            testExtension.updateCountry();
        Test.stopTest();

    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Tests constructor for update Address
     * @history        26.SEPT.2014 - Kristian Vegerano - Created  
     */
    static testmethod void testPageConstructorWithId(){
        insert new List<OSM_Country_and_State__c>{OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12345'),
                                                  OSM_DataFactory.createCountryAndState('Philippines', 'PH', '', '', '12354')};
        
        OSM_Address__c testAddress = new OSM_Address__c(Name = 'testName');
        //insert testAddress; 
        ApexPages.StandardController sc = new ApexPages.StandardController(testAddress);
        OSM_ManageAddressExt testExtension; 
        Test.startTest();
            testExtension = new OSM_ManageAddressExt(sc);
            testExtension.pageAddress.OSM_Country__c = testExtension.countryList[2].getValue();
            testExtension.updateCountry();
        Test.stopTest();
    }
    
    /**
    * @author           Jats Xyvenn Matres
    * @description      Tests constructor for update State
    * @history          13.Jan.2015 - Jats Xyvenn Matres - Created
    */
    
    
    static testMethod void testUpdateStateWithoutError(){
        
        Boolean error = false;
        Set<Id> stateID = new Set<Id>();
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;

        List<Account> acct = new List<Account>();
        for(Integer i=0; i<5; i++){
            acct.add(OSM_DataFactory.createAccountWithBillingAddress(i, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '12354', recordTypeIds.Account_HCO_Record_Type__c));
        }
        insert acct;
      
        List<OSM_Country_and_State__c> rec = new List<OSM_Country_and_State__c>();
        for(Integer i=0; i<5; i++){
            rec.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12354'));
            i++;
        }
        insert rec;
       

        OSM_Address__c testAddress = new OSM_Address__c(Name = 'testName', OSM_Country__c = 'Canada', OSM_State__c = 'BA');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testAddress);
         OSM_ManageAddressExt testExtension;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
        Test.startTest();
            testExtension = new OSM_ManageAddressExt(sc);
            testExtension.pageAddress.OSM_Country__c = testExtension.countryList[1].getValue();
            testExtension.updateCountry();
        Test.stopTest();
               
    }
}