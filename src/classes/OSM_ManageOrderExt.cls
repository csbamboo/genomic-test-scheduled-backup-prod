/*
  @author: patrick lorilla
  @date: 14 OCT 2014
  @description: Update Order Extension
  @history:
    OCT 14 2014     PL              Created
    MAY 12 2015     Rescian Rey     Added Service Console Override

*/
global with sharing class OSM_ManageOrderExt {
    private Order pageOrder = null;
    private String retURL;
    private String rType;
    private String cancelURL;
    private String ent;
    private String saveNewURL;
    private String contactId;
    private String contactName;
    private String patientFirstName;
    private String patientMiddleName;
    private String patientInitials;
    private String patientLastName;
    private String patientDOB;
    private String patientGender;
    private String patientStreet;
    private String patientCity;
    private String patientState;
    private String patientCountry;
    
    /*
      @author: patrick lorilla
      @date: 14 OCT 2014
      @description: Constructor
      @param: stdController - Standard Controller
    */
    public OSM_ManageOrderExt(ApexPages.StandardController stdController){
        Record_Type__c recType = Record_Type__c.getOrgDefaults();
        pageOrder = (Order)stdController.getRecord();
        
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        rType = ApexPages.currentPage().getParameters().get('RecordType');
        cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
        ent = ApexPages.currentPage().getParameters().get('ent');
        saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
        
        contactId = ApexPages.currentPage().getParameters().get('CF' + recType.CF_Order_Patient__c + '_lkid');
        contactName = ApexPages.currentPage().getParameters().get('CF' + recType.CF_Order_Patient__c);
        
        patientFirstName = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_First_Name__c);
        patientInitials = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_Initials__c);
        patientMiddleName = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_Middle_Name__c);
        patientLastName = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_Last_Name__c);
        patientDOB = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_DOB__c);
        patientGender = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_Gender__c);
        patientStreet = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_Street__c);
        patientCity = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_City__c);
        patientState = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_State__c);
        patientCountry = ApexPages.currentPage().getParameters().get(recType.OSM_CF_Order_Patient_Country__c);
        
        System.debug('************************************** order record type from ext: ' + rType);
    }
    
    /*
      @author: patrick lorilla
      @date: 14 OCT 2014
      @description: Update Orders
    */
    public PageReference updateOrder(){
        Order updateOrder = [SELECT Id, OSM_Patient__c, OSM_Patient_First_Name__c, OSM_Patient_Last_Name__c, OSM_Patient_Gender__c, OSM_Patient_State__c, OSM_Patient_Country__c, OSM_Patient_Initials__c, AccountId, Pricebook2Id  FROM Order WHERE Id = :pageOrder.Id];
        //Call on create logic - copies account/contact details to order role fields
        update OSM_OrderTriggerHandler.populatePatientFields(new List<Order>{updateOrder}, false);
  
        return new PageReference('/' + pageOrder.Id);
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that will pre populate order's Account and Start Date fields. 
     * @history        02.FEB.2014 - Kristian Vegerano - Created  
     */
    public PageReference createOrder(){
        Record_Type__c recType = Record_Type__c.getOrgDefaults();
        System.debug('!@#RecType ' + recType);
        String defaultOrderingAccounts = recType.OSM_Order_Account_Default__c;
	    String[] defaulOrdAccts = defaultOrderingAccounts.split(',');
        Id defActId = Id.valueOf(defaulOrdAccts[OSM_RandomUtility.getRandomInt(0,defaulOrdAccts.size()-1)]);
        Account defaultAccount = [SELECT Id, Name FROM Account WHERE Id = :defActId];
        System.debug('!@#DefAcc ' + defaultAccount);
        String todayDate = String.valueOf(System.TODAY().format());
        //Datetime today = Datetime.now();
        //String todayDate = String.valueOf(Date.parse(today.format('MM/dd/yyyy')));
        PageReference returnURL = new PageReference('/801/e');
        //PageReference returnURL = new PageReference('/apex/OSM_OrderOverridedNewEdit');
        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put('RecordType', rType);
        returnURL.getParameters().put('cancelURL', cancelURL);
        returnURL.getParameters().put('ent', ent);
        returnURL.getParameters().put('save_new_url', saveNewURL);
        returnURL.getParameters().put('nooverride', '1');
        returnURL.getParameters().put('accid', defaultAccount.Name);
        returnURL.getParameters().put('accid_lkid', defaultAccount.Id);
        returnURL.getParameters().put('EffectiveDate', todayDate);
        returnURL.getParameters().put('Status', 'New');
        returnURL.getParameters().put('CF' + recType.CF_Order_Patient__c + '_lkid', contactId);
        returnURL.getParameters().put('CF' + recType.CF_Order_Patient__c, contactName);
        
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_First_Name__c, patientFirstName);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_Initials__c, patientInitials);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_Middle_Name__c, patientMiddleName);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_Last_Name__c, patientLastName);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_DOB__c, patientDOB);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_Gender__c, patientGender);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_Street__c, patientStreet);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_City__c, patientCity);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_State__c, patientState);
        returnURL.getParameters().put(recType.OSM_CF_Order_Patient_Country__c, patientCountry);
        
        returnURL.setRedirect(true);
        return returnURL;
    }

    /*
        @author: Rescian Rey
        @description: For service console purposes, population of account fields.
                        This should replace createOrder(). Redirection will be handled on the VF page.
        @history:
            MAY 11 2015     Rescian Rey    Created method.
    */
    @RemoteAction
    public static String getCreateOrderURL(String currentParams){
        Record_Type__c recType = Record_Type__c.getOrgDefaults();
        // stroing Default Ordering Accounts from custom settings to  Array
        String defaultOrderingAccounts = recType.OSM_Order_Account_Default__c;
	    String[] defaulOrdAccts = defaultOrderingAccounts.split(',');
        Id defActId = Id.valueOf(defaulOrdAccts[OSM_RandomUtility.getRandomInt(0,defaulOrdAccts.size()-1)]);
        Account defaultAccount;
        
        
        String url = '/801/e?';
        try{
            defaultAccount = [SELECT Id, Name FROM Account WHERE Id = : defActId];
        }catch(QueryException ex){
            defaultAccount = new Account(Name='');
        }
        
        String today = String.valueOf(System.TODAY().format());
        Map<String, String> params = new Map<String, String>{
            'EffectiveDate' => today, // Why not just use default here?!
            'nooverride' => '1',
            'accid' => defaultAccount.Name,
            'accid_lkid' => defaultAccount.Id,
            'Status' => 'New' // Why not just use default here?!
        };

        // Remove from currentParams save_new paramater
        currentParams = currentParams.replace('save_new=1', '');

        List<String> defaults = new List<String>();
        for(String k: params.keySet()){
            defaults.add(k + '=' + EncodingUtil.urlEncode(params.get(k), 'UTF-8'));
        }

        url += currentParams + '&' + String.join(defaults, '&');
        return url;
    }
    
    /*
     *  @author             Raus Kenneth Ablaza
     *  @description        Populates patient information on create page
     *  @date               01 June 2015
     *
     */
    webService static String populatePatientInfo(Id patCon){
        
        Contact patInfo = [
            select Id, Name, RecordTypeId, FirstName, MiddleName, LastName, OSM_Patient_Initials__c, Birthdate, 
                    OSM_Gender__c, MailingStreet, MailingCity, MailingState, MailingCountry, AccountId, Account.Name 
            from Contact 
            where Id = :patCon
        ];
        
        Record_Type__c recType = Record_Type__c.getOrgDefaults();
        
        String retURL = '/801/e?';
        
        String todayDate = String.valueOf(System.TODAY().format());
        
        Map<String, String> urlMap = new Map <String, String> {
            'retURL' => '/801/', 
            'RecordType' => recType.Order_Record_Type__c, 
            'accid' => patInfo.Account.Name, 
            //'accid_lkid' => patInfo.Id, 
            'EffectiveDate' => todayDate, 
            'Status' => 'New', 
            'CF' + recType.CF_Order_Patient__c + '_lkid' => patInfo.Id, 
            'CF' + recType.CF_Order_Patient__c => patInfo.Name, 
            recType.OSM_CF_Order_Patient_First_Name__c => patInfo.FirstName, 
            recType.OSM_CF_Order_Patient_Initials__c => patInfo.OSM_Patient_Initials__c, 
            recType.OSM_CF_Order_Patient_Middle_Name__c => patInfo.MiddleName, 
            recType.OSM_CF_Order_Patient_Last_Name__c => patInfo.LastName, 
            recType.OSM_CF_Order_Patient_DOB__c => (patInfo.Birthdate == null ? '' : String.valueOf(patInfo.Birthdate.format())),  
            recType.OSM_CF_Order_Patient_Gender__c => patInfo.OSM_Gender__c, 
            recType.OSM_CF_Order_Patient_Street__c => patInfo.MailingStreet, 
            recType.OSM_CF_Order_Patient_City__c => patInfo.MailingCity, 
            recType.OSM_CF_Order_Patient_State__c => patInfo.MailingState, 
            recType.OSM_CF_Order_Patient_Country__c => patInfo.MailingCountry 
        };
        
        List<String> par = new List<String>();
        
        for(String k: urlMap.keySet()){
            if (urlMap.get(k) != null){
                par.add(k + '=' + EncodingUtil.urlEncode(urlMap.get(k), 'UTF-8'));
            }
        }

        retURL += String.join(par, '&');

        return retURL;
    }
    
    /*
     *  @author         Raus Kenneth Ablaza
     *  @description    Used for cloning objets (Order and OLI)
     *  @date           11 June 2015
     */
    webService static String cloneRecordObject(Id objId, String sObj){
        //Order origOrd = (Order)Schema.SObjectType.Order.getId(Id);
        //Order origOrd = pageOrder;
        
        // code taken from the internet : http://sfdc.arrowpointe.com/2011/03/28/cloning-records-in-apex/
        
        String selects = '';
        
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(sObj).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
        
        String queryBuilder = 'select ' + selects + ' from ' + sObj + ' where Id = :objId';
        
        sObject origObj = Database.query(queryBuilder);
        
        System.debug('***************************original object: '+origObj);
        
        sObject cloneObj = origObj.clone(false);
        
        System.debug('***************************cloned object: '+cloneObj);
        
        String origObjId = (String) origObj.get('Id');
        
        origObjId = origObjId.substring(0, 3);
        
        System.debug('***************************cloned object id: ' + origObjId);
        
        if(origObjId == '801'){
            if(origObj.get('OSM_Status__c') != 'Processing'){
                //OSM_OrderTriggerHandler.runCreateOrderRoleOnce = true;
                insert cloneObj;
            }
            else{
                return 'ORDER_CLONE_FAIL';
            }
        }
        else{
            insert cloneObj;
        }
        
        return String.valueOf(cloneObj.Id);
    }
}