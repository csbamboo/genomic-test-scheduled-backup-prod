/*
    @author: Daniel Quismorio
    @description: Unit tests for OSM_ManageOrderExt.
    @history:
        <date>                <author>                <change_description>
        FEB 23 2015            Daniel Quismorio        Creation
        MAY 12 2015            Rescian Rey             Added test for getCreateOrderURL
*/
@isTest(seeAllData = true)
private class OSM_ManageOrderExt_Test {

    static testMethod void pageRedirect(){
    
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        update triggerSwitch;
        
        //Custome Standard Price Book
        Pricebook2 customPB = new Pricebook2(Name='Test Class Price Book', isActive=true);
        insert customPB;
        
        Test.startTest();
            Account acct = new Account(
                Name='testName'
            );
            insert acct;
            
            Account defAccount = [SELECT Id FROM Account WHERE Name = 'testName'];
            
            String defAccValue = String.valueOf(defAccount.Id);
            
            Record_Type__c setting = new Record_Type__c(SetupOwnerId=Userinfo.getUserId());
            setting.OSM_Order_Account_Default__c = defAccValue;
            insert setting;
            
            Id recType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId(); 
            Contact con = new Contact(
                LastName='testLastName',
                RecordTypeId = recType
            );
            insert con;
            System.debug('!@#confromTest ' + con);
            
            Order od = new Order(
                Name = '14TPE000001',
                OSM_Patient__c = con.Id,
                AccountId = acct.Id,
                EffectiveDate = Date.Today(),
                Status = 'Active'
            );
            insert od;
            System.debug('!@#odfromTest ' + od);
        
            PageReference pageRef = Page.OSM_CreateOrder;
            PageReference pr;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(od);
            ApexPages.currentPage().getParameters().put('Id',od.id);
            System.debug('!@#PageLinkfromTest ' + pageRef);
            OSM_ManageOrderExt cons = new OSM_ManageOrderExt(sc);
            pr = cons.createOrder();
            pr = cons.updateOrder();
            
        Test.stopTest();    
    }
    
    /*
        @author: Rescian Rey
        @description: Unit test for getCreateOrderURL. Test includes checking if paramaters
                        were being passed in the URL.
        @history:
            <date>                <author>                <description>
            MAY 12 2015            Rescian Rey             Created test method
    */
    static testMethod void getCreateOrderURL_ParameterCheck(){
        Test.startTest();

        // Passing an empty string for testing purposes only
        // This should not affect the test
        String returnedURL = OSM_ManageOrderExt.getCreateOrderURL('');

        // Check if the address is correct
        System.assert(returnedURL.startsWith('/801/e'));
        
        // Check if the parameters are in the URL
        System.assert(returnedURL.contains('EffectiveDate'));
        System.assert(returnedURL.contains('nooverride'));
        System.assert(returnedURL.contains('accid'));
        System.assert(returnedURL.contains('accid_lkid'));
        System.assert(returnedURL.contains('Status'));

        Test.stopTest();
    }
    
    /*
     *  @author         Raus Kenneth Ablaza
     *  @description    Tests populatePatientInfo method
     *  @date           30 June 2015
     */
    static testMethod void testPopulatePatientInfo(){
    
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = false;
        update triggerSwitch;
        
        Account acct = new Account(
            Name='testName'
        );
        insert acct;
        
        Id recType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId(); 
        Contact con = new Contact(
            LastName='testLastName',
            RecordTypeId = recType
        );
        insert con;
        
        Test.startTest();
        
        OSM_ManageOrderExt.populatePatientInfo(con.Id);
        
        Test.stopTest();
    }
    
    /*
     *  @author         Raus Kenneth Ablaza
     *  @description    Tests cloneRecordObject method
     *  @date           30 June 2015
     */
    static testMethod void testCloneRecordObject(){
    
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        update triggerSwitch;
        
        Id pb2Standard = Test.getStandardPricebookId();
        
        Account acct = new Account(
            Name='testName'
        );
        insert acct;
        
        Id recType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId(); 
        Contact con = new Contact(
            LastName='testLastName',
            RecordTypeId = recType
        );
        insert con;
        
        Order od = new Order(
            Name = '14TPE000001',
            OSM_Patient__c = con.Id,
            AccountId = acct.Id,
            EffectiveDate = Date.Today(),
            Status = 'Active',
            PriceBook2Id = pb2Standard 
        );
        insert od;
        
        Test.startTest();
        
        OSM_ManageOrderExt.cloneRecordObject(od.Id, 'Order');
        
        Test.stopTest();
    }

}