/**
 * File Info
 * ----------------------------------
 * @filename       OSM_ManageOrderRoleExt.cls
 * @created        29.OCT.2014
 * @author         Kristian Vegerano
 * @description    Extension class for redirection to overridden buttons. 
 * @history        29.OCT2014 - Kristian Vegerano - Created  
 */
public class OSM_ManageOrderRoleExt{
    private OSM_Order_Role__c pageOrderRole = null;
    private String retURL;
    private String rType;
    private String cancelURL;
    private String ent;
    private String saveNewURL;
    public String orderName;
    public String orderId;
    
    /**
     * @author         Kristian Vegerano
     * @description    Constructor. 
     * @history        29.OCT.2014 - Kristian Vegerano - Created  
     */
    public OSM_ManageOrderRoleExt(ApexPages.StandardController stdController){
        Record_Type__c systemIds = Record_Type__c.getOrgDefaults();
        
        pageOrderRole = (OSM_Order_Role__c)stdController.getRecord();
        
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        rType = ApexPages.currentPage().getParameters().get('RecordType');
        cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
        ent = ApexPages.currentPage().getParameters().get('ent');
        saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
        orderName = ApexPages.currentPage().getParameters().get('CF' + systemIds.OSM_CF_Order_Role_Order__c);
        orderId = ApexPages.currentPage().getParameters().get('CF' + systemIds.OSM_CF_Order_Role_Order__c + '_lkid');
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that will pre populate order role's name fields. 
     * @history        29.OCT.2014 - Kristian Vegerano - Created  
     */
    public PageReference createOrderRole(){
        Record_Type__c systemIds = Record_Type__c.getOrgDefaults();
        
        Schema.DescribeSObjectResult objectDescribe = OSM_Order_Role__c.sObjectType.getDescribe();
        PageReference returnURL = new PageReference('/' + objectDescribe.getKeyPrefix() + '/e');
                
        returnURL.getParameters().put('Name', 'Default Order Role Name');
        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put('RecordType', rType);
        returnURL.getParameters().put('cancelURL', cancelURL);
        returnURL.getParameters().put('ent', ent);
        returnURL.getParameters().put('save_new_url', saveNewURL);
        returnURL.getParameters().put('nooverride', '1');
        returnURL.getParameters().put('CF' + systemIds.OSM_CF_Order_Role_Order__c, orderName);
        returnURL.getParameters().put('CF' + systemIds.OSM_CF_Order_Role_Order__c + '_lkid', orderId);
        
        returnURL.setRedirect(true);
        return returnURL;
    }
}