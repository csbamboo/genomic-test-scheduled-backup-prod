/*
    @author: patrick lorilla
    @description: Test manage order role extension
    @date: 07 November 2014
*/
@isTest
public class OSM_ManageOrderRoleExt_Test{
      /*
        @author: patrick lorilla
        @description: Test page redirect
        @date: 07 November 2014
      */
      static testMethod void pageRedirect(){
          TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
          triggerSwitch.Order_Trigger__c = false;
          triggerSwitch.Contact_Trigger__c = false;
          triggerSwitch.Order_Role_Trigger__c = false;
          insert triggerSwitch;
          
          Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
          Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
          Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
          Id pb2Standard = Test.getStandardPricebookId();
          
          Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'NorthTerr', '123', HCORecType) ;
          insert acct;
          Contact pat = OSM_DataFactory.createContact(1,acct.ID,PatientRecType);
          insert pat;
          Contact hcp = OSM_DataFactory.createContact(1,acct.ID,HCPRecType);
          insert hcp ;
          Order ord = OSM_DataFactory.createOrder(pat.ID, acct.ID);
          ord.Status = 'Active';
          ord.Pricebook2Id = pb2Standard;
          insert ord;
          OSM_Order_Role__c ordRole = OSM_DataFactory.createOrderRole(ord.ID, acct.ID , hcp.ID);
          insert ordRole;      
          //prepare apex page
          PageReference pageRef = Page.OSM_CreateOrderRole;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ordRole);
          ApexPages.currentPage().getParameters().put('RecordType',ordRole.RecordTypeId);
          //run vf page extension
          OSM_ManageOrderRoleExt cons = new OSM_ManageOrderRoleExt (sc);
          //assert page redirection
          System.assert(cons.createOrderRole() != null);
      }

}