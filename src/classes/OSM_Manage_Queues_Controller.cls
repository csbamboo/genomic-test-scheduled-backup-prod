/*
      @author: Rescian Rey
      @dateCreated: 10 APR 2015 - Created
      @description: Page Controller for the Manage Queue button for Cases
      @history:
*/
public with sharing class OSM_Manage_Queues_Controller {

    private Id uID;
    private Map<Id, QueueSobject> caseQueues;
    private Map<Id, String> selectedQueues;
    private Map<Id, String> unSelectedQueues;
    private Set<Id> originalQueues;

    public List<String> selected {get;set;}     
    public List<String> deselected {get;set;}

    /*
        @author: Rescian Rey
        @date: 10 APR 2015 - created
        @description: Constructor method. Initializes the 2 selected lists to be displayed.
        @history:
     */
    public OSM_Manage_Queues_Controller(){
        // get Case queues
        caseQueues = new Map<Id, QueueSobject>();
        for(QueueSobject obj: [SELECT Queue.Id, Queue.Name, Queue.DeveloperName
                FROM QueueSobject
                WHERE SobjectType = 'Case'
                ORDER BY Queue.Name]){
            caseQueues.put(obj.Queue.Id, obj);
        }
        System.debug('Case Queues: ' + caseQueues);
        
        // get user ID
        uID = UserInfo.getUserId();
        System.debug('User ID: ' + uID);

        Set<Id> caseQueueIDs = caseQueues.keyset();

        // Initialize selected queues and unselected queues
        selectedQueues = new Map<Id, String>();
        unSelectedQueues = new Map<Id, String>();
        selected = new List<String>();
        deselected = new List<String>();

        // Determine to which queue groups the user belongs
        originalQueues = new Set<Id>();
        for(GroupMember gm: [SELECT GroupId FROM GroupMember
                WHERE UserOrGroupId = :uID
                AND GroupId IN :caseQueues.keySet()]){
            originalQueues.add(gm.GroupId);
        }

        // Determine the selected and unSelected Queues based on user groups
        for(String qID: caseQueues.keySet()){
            if(!originalQueues.contains(qID)){
                unSelectedQueues.put(qID, caseQueues.get(qID).Queue.Name);
            }else{
                selectedQueues.put(qID, caseQueues.get(qID).Queue.Name);
            }
        }
        System.debug('Selected Queues: ' + selectedQueues);
        System.debug('Unselected Queues: ' + unSelectedQueues);
    }

    /*
        @author: Rescian Rey
        @description: Populates the options for the My Queue selection list (with sorting)
        @date: 12 APR 2015
        @history:
     */
    public List<SelectOption> getSelectedItems(){
        Map<String, SelectOption> nameToOption = new Map<String, SelectOption>();
        for(String qID: selectedQueues.keySet()){
            nameToOption.put(caseQueues.get(qID).Queue.Name, new SelectOption(qID, caseQueues.get(qID).Queue.Name));
        }

        List<String> sortedNames = new List<String>(nameToOption.keySet());
        sortedNames.sort();

        List<SelectOption> options = new List<SelectOption>();
        for(String name: sortedNames){
            options.add(nameToOption.get(name));
        }
        return options;
    }

    /*
        @author: Rescian Rey
        @description: Populates the options for the Available selection list (with sorting)
        @date: 12 APR 2015
        @history:
     */
    public List<SelectOption> getUnselectedItems(){
        Map<String, SelectOption> nameToOption = new Map<String, SelectOption>();
        for(String qID: unSelectedQueues.keySet()){
            nameToOption.put(caseQueues.get(qID).Queue.Name, new SelectOption(qID, caseQueues.get(qID).Queue.Name));
        }

        List<String> sortedNames = new List<String>(nameToOption.keySet());
        sortedNames.sort();

        List<SelectOption> options = new List<SelectOption>();
        for(String name: sortedNames){
            options.add(nameToOption.get(name));
        }
        return options;
    }

    /*
        @author: Rescian Rey
        @description: Adds the user to the selected Queue and corresponding group. Removes the 
                        user to the deselected Queue and corresponding group.
        @date: 12 APR 2015
        @history:
     */
    public PageReference save(){
        // determine which from the selected queues are in the original user queues
        Set<Id> newQueues = new Set<Id>();
        Set<String> newQueueNames = new Set<String>();
        for(Id qID: selectedQueues.keySet()){
            if(!originalQueues.contains(qID)){
                newQueues.add(qID);
                newQueueNames.add(caseQueues.get(qID).Queue.DeveloperName);
            }
        }

        // Insert new memberships
        List<GroupMember> membershipsToAdd = new List<GroupMember>();
        for(Group g: [SELECT Id FROM Group WHERE Id IN :newQueues
                OR (Type = 'Regular' AND DeveloperName IN :newQueueNames)]){
            GroupMember m = new GroupMember();
            m.UserOrGroupId = uID;
            m.GroupId = g.Id;
            membershipsToAdd.add(m);
        }
        insert membershipsToAdd;

        // determine which from the unselected queues are in the original user queues
        // delete user's membership to those queues
        Set<Id> oldQueues = new Set<Id>();
        Set<String> oldQueueNames = new Set<String>();
        for(Id qID: unSelectedQueues.keySet()){
            if(originalQueues.contains(qID)){
                oldQueues.add(qID);
                oldQueueNames.add(caseQueues.get(qID).Queue.DeveloperName);
            }
        }

        Set<Id> regularOldGroups = new Set<Id>();
        for(Group g: [SELECT Id FROM Group WHERE Type = 'Regular'
                AND DeveloperName IN :oldQueueNames]){
            regularOldGroups.add(g.Id);
        }

        // Remove membership
        delete [SELECT Id FROM GroupMember WHERE UserOrGroupId = :uID
            AND (GroupId IN :oldQueues
            OR GroupId IN :regularOldGroups)];
        return null;
    }
 
    /*
        @author: Rescian Rey
        @date: 10 APR 2015 - created
        @description: Transfers selected queues from Available Queue to My Queue
        @history:
     */
    public PageReference selectItem(){
        deselected.clear();
        for(String s: selected){
            selectedQueues.put(s, caseQueues.get(s).Queue.Name);
            unSelectedQueues.remove(s);
            deselected.add(s);
        }
        return null;
    }

    /*
        @author: Rescian Rey
        @date: 10 APR 2015 - created
        @description: Transfers selected queues from My Queue to Available Queue
        @history:
     */
    public PageReference deselectItem(){
        selected.clear();
        for(String s: deselected){
            unSelectedQueues.put(s, caseQueues.get(s).Queue.Name);
            selectedQueues.remove(s);
            selected.add(s);
        }
        return null;
    }
}