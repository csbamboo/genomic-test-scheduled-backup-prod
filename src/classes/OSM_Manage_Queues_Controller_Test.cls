/*
    @author: Rescian Rey
    @dateCreated: 13 APR 2015
    @description: Test class for OSM_Manage_Queues_Controller
    @history:
 */
@isTest
private class OSM_Manage_Queues_Controller_Test {
	private final static String QUEUE_NAME_PREFIX = 'Queue ';
    private final static Integer qCount = 10;
    private static User testUser;
    private static List<Group> queues;
    private static List<Group> regularGroups;

    /*
        @author: Rescian Rey
        @description: Creates Groups and Queues
        @createdDate: 13 APR 2015
        @history:
    */
    private static void setupTestData(){
        testUser = OSM_DataFactory.createUser('Test');
        insert testUser;

        queues = new List<Group>();
        regularGroups = new List<Group>();

        for(Integer i=0; i<qCount; i++){
            queues.add(OSM_DataFactory.createGroup(QUEUE_NAME_PREFIX + i, OSM_DataFactory.GROUP_TYPE_QUEUE));
            regularGroups.add(OSM_DataFactory.createGroup(QUEUE_NAME_PREFIX + i, OSM_DataFactory.GROUP_TYPE_REGULAR));
        }
        insert queues;
        insert regularGroups;
        System.debug('QUEUES: ' + queues);
        System.debug('REGULAR GROUPS: ' + regularGroups);

        List<QueueSobject> queueSobjects = new List<QueueSobject>();
        for(Integer i=0; i<qCount; i++){
            queueSobjects.add(OSM_DataFactory.createQueueSobject(queues.get(i).Id, 'Case'));
        }
        insert queueSobjects;
        System.debug('Queue SObjects: ' + queueSobjects);
    }

    /*
        @author: Rescian Rey
        @description: Constructor test for OSM_Manage_Queues_Controller. Tests if the selection lists
            are correctly populated.
        @createdDate: 13 APR 2015
        @history:
    */
    private testmethod static void initializationTest_QueuesPopulated(){
        setupTestData();
        List<QueueSobject> q = [SELECT Queue.Id FROM QueueSobject WHERE SobjectType = 'Case' ORDER BY Queue.Name];

        Test.startTest();

        OSM_Manage_Queues_Controller controller;

        // Just a simple test, if queue count matches the size of the unselected & selected items
        System.runAs(testUser){
            controller = new OSM_Manage_Queues_Controller();
        }
        
        System.assertEquals(q.size() - 1, controller.getUnselectedItems().size());
        System.assertEquals(0, controller.getSelectedItems().size());

        Test.stopTest();
    }

    /*
        @author: Rescian Rey
        @description: Selection test. If 'Add' button is clicked, selected item should transfer to My Queue
        @createdDate: 13 APR 2015
        @history:
    */
    private testmethod static void selectItem_OptionTransferredToMyQueue(){
        setupTestData();
        List<QueueSobject> q = [SELECT Queue.Id FROM QueueSobject WHERE SobjectType = 'Case' ORDER BY Queue.Name];
        
        Test.startTest();

        OSM_Manage_Queues_Controller controller;
        System.runAs(testUser){
            controller = new OSM_Manage_Queues_Controller();
            controller.selected.add(queues.get(qCount-1).Id);
            controller.selectItem();
        }

        System.assertEquals(q.size() - 2, controller.getUnselectedItems().size());
        System.assertEquals(1, controller.getSelectedItems().size());

        Test.stopTest();
    }

    /*
        @author: Rescian Rey
        @description: Selection test. If 'Remove' button is clicked, selected item should transfer to Available Queue
        @createdDate: 13 APR 2015
        @history:
    */
    private testmethod static void deSelectItem_OptionTransferredToAvailableQueue(){
        setupTestData();
        List<QueueSobject> q = [SELECT Queue.Id FROM QueueSobject WHERE SobjectType = 'Case' ORDER BY Queue.Name];

        Test.startTest();

        OSM_Manage_Queues_Controller controller;
        System.runAs(testUser){
            controller = new OSM_Manage_Queues_Controller();
            controller.selected.add(queues.get(qCount-1).Id);
            controller.selectItem();
            
            System.assertEquals(q.size() - 2, controller.getUnselectedItems().size());
            System.assertEquals(1, controller.getSelectedItems().size());

            controller.deselected.add(queues.get(qCount-1).Id);
            controller.deselectItem();

            System.assertEquals(q.size() - 1, controller.getUnselectedItems().size());
            System.assertEquals(0, controller.getSelectedItems().size());
        }

        Test.stopTest();
    }

    /*
        @author: Rescian Rey
        @description: Save test. User should be added to Queue and Group of the selected queues
        @createdDate: 13 APR 2015
        @history:
    */
    private testmethod static void onSave_UserAddedToGroupAndQueue(){
        setupTestData();
        List<QueueSobject> q = [SELECT Queue.Id FROM QueueSobject WHERE SobjectType = 'Case' ORDER BY Queue.Name];

        Test.startTest();

        OSM_Manage_Queues_Controller controller;
        System.runAs(testUser){
            controller = new OSM_Manage_Queues_Controller();
            controller.selected.add(queues.get(qCount-1).Id);
            controller.selectItem();
            controller.save();

            Id gID = queues.get(qCount-1).Id;
            String gName = QUEUE_NAME_PREFIX + (qCount-1);
            System.assert(![SELECT Id FROM GroupMember 
                WHERE GroupId = :gID AND UserOrGroupId = :testUser.Id].isEmpty());

            gID = regularGroups.get(qCount-1).Id;
            System.assert(![SELECT Id FROM GroupMember 
                WHERE GroupId = :gID AND UserOrGroupId = :testUser.Id].isEmpty());
        }

        Test.stopTest();
    }

    /*
        @author: Rescian Rey
        @description: Save test. User should be remove to Queue and Group if a previously selected queue is deselected
        @createdDate: 13 APR 2015
        @history:
    */
    private testmethod static void onSave_UserRemoveToGroupAndQueueOnDeselect(){
        setupTestData();
        List<QueueSobject> q = [SELECT Queue.Id FROM QueueSobject WHERE SobjectType = 'Case' ORDER BY Queue.Name];

        OSM_Manage_Queues_Controller controller;
        System.runAs(testUser){
            controller = new OSM_Manage_Queues_Controller();
            controller.selected.add(queues.get(qCount-1).Id);
            controller.selectItem();
            controller.save();
        }

        Test.startTest();

        
        System.runAs(testUser){
            controller = new OSM_Manage_Queues_Controller();
            controller.deselected.add(queues.get(qCount-1).Id);
            controller.deselectItem();
            controller.save();

            Id gID = queues.get(qCount-1).Id;
            String gName = QUEUE_NAME_PREFIX + (qCount-1);
            System.assert([SELECT Id FROM GroupMember 
                WHERE GroupId = :gID AND UserOrGroupId = :testUser.Id].isEmpty());

            gID = regularGroups.get(qCount-1).Id;
            System.assert([SELECT Id FROM GroupMember 
                WHERE GroupId = :gID AND UserOrGroupId = :testUser.Id].isEmpty());
        }

        Test.stopTest();
    }
}