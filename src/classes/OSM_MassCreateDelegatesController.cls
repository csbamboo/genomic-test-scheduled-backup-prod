/********************************************************************************************************************
@author        Patrick Lorilla
@date          14 September 2014
@description   Mass Create Delegates Controller
*******************************************************************************************************************/

public class OSM_MassCreateDelegatesController{

    public Contact pageContact {get;set;}
    public Account hco {get; set;}
    public List<Contact> hcpList {get; set;}
    public OSM_Delegate__c delegate{get; set;}
    public List<HCPWrapper> hcpWrapper {get;set;}
    public List<OSM_Delegate__c> delegateList;
    public boolean selectAll {get;set;}
    Id id = null;
    boolean isPartnerAccount = false;
    Record_Type__c recTypes = null; 
    public String filter {get;set;}
 
    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          14 September 2014
    @description   Constructor
    *******************************************************************************************************************/

    public OSM_MassCreateDelegatesController(){
        //Instantiate list and objects
        hcpList = new List<Contact>();
        delegate = new OSM_Delegate__c();
        recTypes = Record_Type__c.getOrgDefaults();
        //Get ID
        id = ApexPages.currentPage().getParameters().get('id');
        if(String.valueOf(id).left(3) == '003'){
            pageContact = [SELECT Id, AccountId FROM Contact WHERE Id = :id LIMIT 1][0];
        }else{
            pageContact = new Contact();
            pageContact.AccountId = id;
        }
        if(String.valueOf(id).left(3) == Account.sObjectType.getDescribe().getKeyPrefix()){
            //Account
            if(id != null){
                loadHCPHCOData(id);    
            }
            filter = 'AccountId = \\\''+id+'\\\' AND RecordTypeId != \\\''+recTypes.Contact_Patient_Record_Type__c+'\\\' AND OSM_Status__c != \\\'Inactive\\\'';
            
        }else if(String.valueOf(id).left(3) == Contact.sObjectType.getDescribe().getKeyPrefix()){
            //Contact
            loadContacHCPHCOData(id);
        }
        selectAll = false;
        
    }


    /**
     * @author         Kristian Vegerano
     * @description    Load HCP and HCO Data
     * @history        26.SEPT.2014 - Kristian Vegerano - Created  
     * @param          id - ID of the HCO
     */
    public void loadContacHCPHCOData(ID id){
        //Load HCO Data
        Contact hcp = [SELECT Id, Name, RecordTypeId, AccountId, Account.RecordTypeId, Account.Name FROM Contact WHERE Id = :id];
        delegate.OSM_HCO__c = hcp.AccountId;
        delegate.OSM_Contact__c = id;
        //Load Contact data
        ID recID = null;
        if(hcp.Account.RecordTypeId == recTypes.Account_Partner_Record_Type__c){
            hcpList = [SELECT ID, Name, AccountId from Contact where AccountID = :hcp.AccountId AND OSM_Status__c != 'Inactive'];
        }
        else{
            recID  = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
            hcpList = [SELECT ID, Name, AccountId from Contact where AccountID = :hcp.AccountId AND RecordTypeId =: recID AND OSM_Status__c != 'Inactive'];
        }
        hcpWrapper = new List<HCPWrapper>();
        //Save HCP List to Wrapper
        for(Contact con: hcpList){
            hcpWrapper.add(new HCPWrapper(false, con));
        } 
    }
    
    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          14 September 2014
    @param         id - ID of the HCO
    @description   Load HCP and HCO Data
    *******************************************************************************************************************/
    public void loadHCPHCOData(ID id){
        //Load HCO Data
      
        hco = [SELECT ID, Name, RecordTypeId from Account where ID =: id];
        delegate.OSM_HCO__c = id;
        //Load Contacts
        ID recID = null;
        if(hco.RecordTypeId == recTypes.Account_Partner_Record_Type__c){
            hcpList = [SELECT ID, Name, AccountId from Contact where AccountID = :hco.Id AND OSM_Status__c != 'Inactive'];
        }
        else{
            recID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
            hcpList = [SELECT ID, Name, AccountId from Contact where AccountID =: id And RecordTypeId =: recID AND OSM_Status__c != 'Inactive'];
        }
        hcpWrapper = new List<HCPWrapper>();
        //Save HCP List to Wrapper
        for(Contact con: hcpList){
            hcpWrapper.add(new HCPWrapper(false, con));
        } 
    }
            
    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          14 September 2014
    @description   Get delegate permission values for the select option
    *******************************************************************************************************************/
    
    public List<SelectOption> getDelegatePermissions(){
       
        List<SelectOption> options = new List<SelectOption>();
        //Get delegate permissions picklist values from Schema Object
        Schema.DescribeFieldResult fieldResult = OSM_Delegate__c.OSM_Delegate_Permissions__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //Add picklist values to list mapped to the vf select option    
        for(Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
     
    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          14 September 2014
    @description   Select all HCP checkboxes
    *******************************************************************************************************************/
    public void selectAll(){
    
        for(HCPWrapper hcw: hcpWrapper){
            hcw.checked = selectAll;
        }
    
    }
    
    /*
     *  @author         Raus Kenneth Ablaza
     *  @description    Changes the contact list based on selected account.
     *  @date           06 June 2015
     */
    public void refreshContactsList(){
        loadHCPHCOData(pageContact.AccountId); 
        selectAll = false;
    }

    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          14 September 2014
    @description   Create delegates based on wrapper values and redirect to details page
    *******************************************************************************************************************/
    public PageReference createDelegates(){
        try{
            delegateList = new List<OSM_Delegate__c>();
            for(HCPWrapper hcw: hcpWrapper){
                if(hcw.checked){
                    OSM_Delegate__c createDel = new OSM_Delegate__c();
                    createDel.OSM_HCO__c  = delegate.OSM_HCO__c;
                    createDel.OSM_Delegate_Permissions__c = hcw.delegatePerm;
                    createDel.OSM_HCP__c = hcw.cont.ID;
                    createDel.OSM_Contact__c = delegate.OSM_Contact__c;
                    //createDel.OSM_HCP__c = delegate.OSM_Contact__c;
                    //createDel.OSM_Contact__c = hcw.cont.ID;
                    
                    delegateList.add(createDel);
                }
            }
            
            pageReference pr = new PageReference('/'+delegate.OSM_HCO__c);
            pr.setRedirect(true);
            
            if(pageContact.AccountId != null){
                if(delegateList.size() > 0){
                    insert delegateList;
                }else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select a related contact.'));
                    return null;
                }
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select an account.'));
                return null;
            }
            
            return pr;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'You can not associate a delegate to itself.'));
            return null;
        }
    }
    
    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          15 September 2014
    @description   Cancel the page and redirects to the HCO Detail page
    *******************************************************************************************************************/
    public PageReference cancel(){
        try{
            return new PageReference('/' + id);
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
            return null;
        }
    }
    
    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          14 September 2014
    @description   Wrapper Class for HCP
    *******************************************************************************************************************/
    public class HCPWrapper{
         
         public Contact cont {get; set;}
         public boolean checked {get; set;}
         public String delegatePerm {get; set;}
        /********************************************************************************************************************
        @author        Patrick Lorilla
        @date          14 September 2014
        @description   Constructor
        *******************************************************************************************************************/
         public HCPWrapper(boolean chk, Contact con){
             this.delegatePerm = null;
             this.cont = con;
             this.checked = chk;
         }
    }
}