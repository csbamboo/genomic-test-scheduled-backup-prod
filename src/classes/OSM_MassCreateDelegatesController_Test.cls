/********************************************************************************************************************
@author        Patrick Lorilla
@date          15 September 2014
@description   Mass Create Delegates Controller Test
*******************************************************************************************************************/
@isTest
public class OSM_MassCreateDelegatesController_Test{

    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          25 September 2014
    @description   Test Create Delegate Functionality - Account
    *******************************************************************************************************************/
    testmethod static void testCreateDelegateAccount(){
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        //Prepare test HCO
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(0,'England','1st St.','London',null,'123',recordTypeIds.Account_HCO_Record_Type__c);
        insert acct;
 
        Account acct1 = OSM_DataFactory.createAccountWithBillingAddress(1,'England','1st St.','London',null,'123',recordTypeIds.Account_HCO_Record_Type__c);
        insert acct1;
        //Prepare test Contact
        Contact con = OSM_DataFactory.createContact(0,acct1.ID,null);
        insert con;
        
        //Prepare test HCPs
        List<Contact> hcpList = new List<Contact>();
        for(Integer a=0; a<20; a++){
            hcpList.add(OSM_DataFactory.createContact(a+20, acct.ID,recordTypeIds.Contact_HCP_Record_Type__c));
        }
        insert hcpList ;
        
        //Prepare page and ID parameter
        Test.setCurrentPageReference(new PageReference('Page.OSM_MassCreateDelegatesController')); 
        System.currentPageReference().getParameters().put('id', acct.ID);
        
        OSM_MassCreateDelegatesController mcdCont = new OSM_MassCreateDelegatesController();
           
        List<SelectOption> delegatePermissions = mcdCont.getDelegatePermissions();
        
        Test.startTest();
        //Select Contact  
        mcdCont.delegate.OSM_Contact__c = con.ID;
        //Select all HCPs
        mcdCont.selectAll = true;
        mcdCont.selectAll();
            
        //Set delegate permission to all HCPs
        for(Integer a=0; a<mcdCont.hcpWrapper.size(); a++){
            mcdCont.hcpWrapper[a].delegatePerm = delegatePermissions[0].getLabel() ;
        }
            
        //Create delegates
        mcdCont.createDelegates();
        Test.stopTest();
        
        //Retrieve HCP and place them on a map 
        //Map<ID, Contact> hcpMap = new Map<ID,Contact>([SELECT ID, LastName from Contact Where LastName !=: con.LastName]);
        //Assert that the delegates are properly created with no exceptions 

        for(OSM_Delegate__c del: [SELECT OSM_Contact__c, OSM_HCO__c, OSM_HCP__c, OSM_Delegate_Permissions__c from OSM_Delegate__c]){
            //System.assertEquals(del.OSM_Contact__c, con.ID);
            System.assertEquals(del.OSM_HCO__c, acct.ID);
            System.assertEquals(del.OSM_Delegate_Permissions__c, delegatePermissions[0].getLabel());
            //System.assert(hcpMap.keySet().contains(del.OSM_HCP__c));
        }

         System.assertEquals(ApexPages.hasMessages(), false);
    }
    
    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          25 September 2014
    @description   Test Create Delegate Functionality - Contact
    *******************************************************************************************************************/
    testmethod static void testCreateDelegateContact(){
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        //Prepare test HCO
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(0,'England','1st St.','London',null,'123',recordTypeIds.Account_HCO_Record_Type__c);
        insert acct;
         Account acct1 = OSM_DataFactory.createAccountWithBillingAddress(1,'England','1st St.','London',null,'123',recordTypeIds.Account_HCO_Record_Type__c);
        insert acct1;
        //Prepare test Contact
        Contact con = OSM_DataFactory.createContact(0,acct.ID,null);
        insert con;
        
        Contact con1 = OSM_DataFactory.createContact(0,acct1.ID,null);
        insert con1;
        
        //Prepare test HCPs
        List<Contact> hcpList = new List<Contact>();
       
        for(Integer a=0; a<20; a++){
            hcpList.add(OSM_DataFactory.createContact(a+20, acct.ID,recordTypeIds.Contact_HCP_Record_Type__c));
        }
        insert hcpList ;
        
        //Prepare page and ID parameter
        Test.setCurrentPageReference(new PageReference('Page.OSM_MassCreateDelegatesController')); 
        System.currentPageReference().getParameters().put('id', con1.ID);

        OSM_MassCreateDelegatesController mcdCont = new OSM_MassCreateDelegatesController();
        
        List<SelectOption> delegatePermissions = mcdCont.getDelegatePermissions();
           
        Test.startTest();
        //Select Contact  
        mcdCont.delegate.OSM_Contact__c = con.ID;
        //Select all HCPs
        mcdCont.selectAll = true;
        mcdCont.selectAll();
            
        //Set delegate permission to all HCPs
        for(Integer a=0; a<mcdCont.hcpWrapper.size(); a++){
            mcdCont.hcpWrapper[a].delegatePerm = delegatePermissions[0].getLabel() ;
        }
            
        //Create delegates
        mcdCont.createDelegates();
        mcdCont.refreshContactsList();
        
        Test.stopTest();
        
        //Retrieve HCP and place them on a map 
        //Map<ID, Contact> hcpMap = new Map<ID,Contact>([SELECT ID, LastName from Contact Where LastName !=: con.LastName]);
        //Assert that the delegates are properly created with no exceptions 
         mcdCont.cancel();
        for(OSM_Delegate__c del: [SELECT OSM_Contact__c, OSM_HCO__c, OSM_HCP__c, OSM_Delegate_Permissions__c from OSM_Delegate__c]){
            System.assertEquals(del.OSM_HCO__c, acct1.ID);
            //System.assertEquals(del.OSM_Contact__c, con.ID);
            System.assertEquals(del.OSM_Delegate_Permissions__c, delegatePermissions[0].getLabel());
            //System.assert(hcpMap.keySet().contains(del.OSM_HCP__c));
        }

         System.assertEquals(ApexPages.hasMessages(), false);
        
  
    }
    /********************************************************************************************************************
    @author        Patrick Lorilla
    @date          15 September 2014
    @description   Test Exception
    *******************************************************************************************************************/
    testmethod static void testException(){
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds;
        //Prepare test HCO
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(0,'England','1st St.','London',null,'123',recordTypeIds.Account_HCO_Record_Type__c);
        insert acct;
        //insert null list and trigger exception
         //Prepare page and ID parameter
        Test.setCurrentPageReference(new PageReference('Page.OSM_MassCreateDelegatesController')); 
        System.currentPageReference().getParameters().put('id', acct.Id);
        
        
        OSM_MassCreateDelegatesController mcdCont = new OSM_MassCreateDelegatesController();
        mcdCont.createDelegates();
        delete acct;
        mcdCont.cancel();
        //mcdCont.insertList(null);
        //System.assertEquals(ApexPages.hasMessages(), true);
      
        
    }

}