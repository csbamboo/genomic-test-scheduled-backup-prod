/*
 *  @author         Jerome Liwanag
 *  @description    Used for HTTP mock callout in test classes - OSM_SubmitTherapakOrderExt_Test ; 
 *  @date           04 August 2015
 *  @history        04 August 2015 - Created - Jerome liwanag
 */
@isTest(SeeAllData=true)
global class OSM_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    protected Integer code;
    protected String status;
    protected String bodyAsString;
    protected Blob bodyAsBlob;
    protected Map<String, String> responseHeaders;

    public OSM_MockHttpResponseGenerator(Integer code, String status, String body) {
        this.code = code;
        this.status = status;
        this.bodyAsString = body;
        this.bodyAsBlob = null;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(code);
        //resp.setStatus(status);
        resp.setHeader('Set-Cookie', 'application/json');
        if (bodyAsBlob != null) {
            resp.setBodyAsBlob(bodyAsBlob);
        } else {
            resp.setBody(bodyAsString);
        }
        return resp;
    }
}