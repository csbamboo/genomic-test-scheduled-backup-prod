/******************************************************
 * Description      : Apex Controller for OSM_MultipleTaskAssign page
 * @author          : Rulleth Decena
 * @since           : March 20, 2015 
 * @History - 20 Mar 2015 - rulleth decena - created
 ******************************************************/
public with sharing class OSM_MultipleTaskAssignController {

    //Capture the values for the standard Task
    public Task task{set;get;}
    //Capture the value for the reminder date/time
    public Contact contact{set;get;}
    //public String reminderTime{set;get;}
    //Other Form fields
    //public Boolean sendNotificationEmailCheckBox{set;get;}
    public Map <Id,String> displayNames{set;get;}
    public List <Case> cases {get;set;}
    private Map <Id,Id> ownerIds;
    //public Set <Id> caseAccountIds {get;set;}
    public Boolean showWhoId{set;get;}
    public Boolean showPage{set;get;}
    public Boolean assignToOwner{set;get;}
    public String userid{get;set;}
    //Page processing parameters
    //private List <Selectoption> selReminderOptions;
    private String [] arrObjIds;
    private String objName = null;
    public Boolean saveStatus {set;get;}
    public String objPrefix {get;set;}
    
    /**
    * Constructor
    * Initalize the values & generate the object list(Names)
    */
    public OSM_MultipleTaskAssignController(){
        //Initalize the variables
        showPage = false;
        task = new Task();
        contact = new Contact();
        displayNames = new Map<Id,String>();
        ownerIds = new Map<Id,Id>();
        //caseAccountIds = new Set <Id>();
        showWhoId = false;
        //sendNotificationEmailCheckBox = false;
        saveStatus = false;
        assignToOwner = false;
        userid = Userinfo.getUserId();
        task.OwnerId = userid;
        task.Type = 'Phone';
        objPrefix = null;
        try{
            //Get the object ids passed as parameter
            Map<String, String> params = ApexPages.currentPage().getParameters();
            String strObjIds = params.get('objIds');
            arrObjIds = strObjIds.split(',');
            
            system.debug('\n\n\n arrObjIds: ' +arrObjIds+'\n\n\n');

            //Identify the object name/type and get the names
            if(arrObjIds != null && !arrObjIds.isEmpty()){
                //Find the object name
                Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
                Set<String> keyPrefixSet = gd.keySet();
                String tPrefix = arrObjIds[0].subString(0,3);
                
                for(String sObj : keyPrefixSet){
                    Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
                    if(tPrefix.equals(r.getKeyPrefix())){
                        objName = r.getName();
                        objPrefix = tPrefix;
                        break;
                    }
                }
                //Query the name for all the objects
                if(objName != null){
                    String strFieldName = 'name';
                    String caseAccId = 'AccountId';
                    //String strQuery = '';
                    //Check if the object is a Contact or Lead
                    if(objName != 'Contact' && objName != 'Lead'){
                        showWhoId = true;
                    }
                    //Handle field that doesn't have a name field
                    if(objName == 'Case'){
                        strFieldName = 'CaseNumber';
                        //strQuery = 'select id,OwnerId,' + strFieldName + ',' + caseAccId + ' from ' + objName + ' ';
                    }
                    //else{
                    //    strQuery = 'select id,OwnerId,' + strFieldName + ' from ' + objName + ' ';
                    //}
                    String strQuery = 'select id,OwnerId,' + strFieldName + ' from ' + objName + ' ';
                    Boolean bFirst = true;
                    for(String strObjId:arrObjIds){
                        if(bFirst){
                            strQuery += ' where Id in (\'';
                            bFirst = false;
                        }else{
                            strQuery += ',\'';
                        }
                        strQuery += String.escapeSingleQuotes(strObjId) + '\'';
                    }
                    strQuery += ')';
                    try{
                        SObject [] sfdcObjects = Database.query(strQuery);
                        //Generate the name list
                        for(SObject sfdcObject:sfdcObjects){
                            displayNames.put((Id)sfdcObject.get('id'),(String)sfdcObject.get(strFieldName));
                            ownerIds.put((Id)sfdcObject.get('id'), (Id)sfdcObject.get('OwnerId'));
                            //if(objName == 'Case'){
                            //    if((Id)sfdcObject.get(caseAccId) != null ){//&& (Id)sfdcObject.get(caseAccId) != ''
                            //       caseAccountIds.add((Id)sfdcObject.get(caseAccId));
                            //    }
                            //}
                        }
                    }catch(Exception e){
                        strQuery = strQuery.replace(',' + strFieldName, ' ');
                        SObject [] sfdcObjects = Database.query(strQuery);
                        //Generate the Id list instead of name list
                        for(SObject sfdcObject:sfdcObjects){
                            displayNames.put((Id)sfdcObject.get('id'),(Id)sfdcObject.get('id'));
                        }
                    }
                }
            }
            showPage = true;
        }catch(Exception e){
            //Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, e.getMessage()));
            Apexpages.addMessages(e);
            showPage = false;
        }
        
    }
    //public Boolean acctNotSame {get;set;}
    //public void checkCaseAccount(){
    //    acctNotSame = false;
    //    if(caseAccountIds.size() > 1){
    //        acctNotSame = true;
    //    }
    //}
    /**
    * Save the new task and keep the existing page
    */
    public Boolean hasError {get;set;}
    public void saveNew(){
        //Create the new task list
        List <Task> lTask = new List<Task>();
        //Capture the status for save()
        saveStatus = false;
        hasError = false;
        try{
            //if(task.OSM_Follow_Up_Date_Time__c == null && task.status == 'Open'){
             //       task.OSM_Follow_Up_Date_Time__c.addError('Follow up Date/Time is required for "Open" Task');
             //   }
             //   else
             //       if(task.OSM_Direction__c == null){
             //           task.OSM_Direction__c.addError('Direction is required for the "Log a Call" Task');
             //       }
            //else{
            for(String strObjId:arrObjIds){
                Task taskNew = new Task();
                
                if(showWhoId){
                    //If the selected objests are not Contacts/Leads
                    taskNew.WhatId = strObjId;
                }else{
                    //If only Contacts/Leads
                    taskNew.WhoId = strObjId;
                }
                //Assign to the record owner based on selected options
                if(assignToOwner){
                    taskNew.OwnerId = ownerIds.get(strObjId);   
                }else{
                    taskNew.OwnerId = task.OwnerId;
                }
                taskNew.status = task.status;
                taskNew.subject = task.subject;
                taskNew.activityDate = task.activityDate;
                taskNew.priority = task.priority;
                taskNew.description = task.description;
                taskNew.OSM_Phone_Number__c = task.OSM_Phone_Number__c;
                taskNew.OSM_Follow_Up_Date_Time__c = task.OSM_Follow_Up_Date_Time__c;
                taskNew.OSM_Direction__c = task.OSM_Direction__c;
                taskNew.Type = task.Type;
                
                lTask.add(taskNew);
            }
            //Insert the new tasks to the SFDC Org
            insert lTask;

        //}
        }catch(Exception e){
            //system.debug(e.getMessage());
            //String error = e.getMessage();
            //String fudate = 'Follow up Date';
            //String direct = 'Direction';

            //if(error.containsNone(fudate) && error.containsNone(direct)){
                Apexpages.addMessages(e);
                hasError = true;
            //    }

            return;
        }
        if(lTask.size() > 0){
            // saveStatus = true;
        }
    }
    /**
    * Save the new task and to back to the previous page 
    * (If no errors)
    */
    public Pagereference save(){
        saveNew();
        if(saveStatus){
            //return null;
            return back();
            // return new PageReference('javascript:window.close()');
        }
        return null;
    }   
    /**
    * Go to the previous page
    */
    public Pagereference back(){
        Pagereference pageRef = new Pagereference('/' + objPrefix);
        //Pagereference pageRef = new Pagereference(ApexPages.currentPage().getParameters().get('retURL'));
        //pageRef.getParameters().put('nonce', ApexPages.currentPage().getParameters().get('nonce')); 
        //pageRef.getParameters().put('sfdcIFrameOrigin', ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin'));
        pageRef.setRedirect(true);

        return pageRef;
    }
    /**
    * Display the selected object names in front end
    */
    public List <String> getTableDisplayNames(){
        List <String> rtnList = new List<String>();
        Set <Id> rtnCaseListId = new Set<Id>();
        List <Case> rtnCaseList = new List<Case>();
        cases = new List<Case>();
        for(String displayName:displayNames.values()){
            rtnList.add('-' + displayName);
        }
        for(Id caseListId:displayNames.keySet()){
            rtnCaseListId.add(caseListId);
        }
        for (Case caseList: [Select Id, CaseNumber, Subject, Origin, Type, Status From Case Where Id IN: rtnCaseListId]){
            rtnCaseList.add(caseList);
        }
        cases.addAll(rtnCaseList);
        return rtnList;
        //return rtnCaseList;
    }    
    
}