/******************************************************
 * Description       : Test Coverage for OSM_MultipleTaskAssignController
 * @author           : Rulleth Decena
 * @history          : March 23, 2015
 ******************************************************/
@isTest
private class OSM_MultipleTaskAssignController_Test {

    static testMethod void testOnMultipleActivities() {
        
        //create test account
        //Account testAccount = DataBuilder.createAccount(1);
        //insert testAccount;

        Case testCase1 = OSM_DataFactory.createCase('Potential Duplicate','Open');
        insert testCase1;

        Case testCase2 = OSM_DataFactory.createCase('Missing Data','Open');
        insert testCase2;
        
        //Create Contacts
        Contact contact1 = new Contact();
        contact1.LastName = 'Test_Contact_01';
        insert contact1;
        
        Contact contact2 = new Contact();
        contact2.LastName = 'Test_Contact_01';
        insert contact2;
        
        //Get a profile from SFDC
        Profile profile = [select Id from Profile WHERE Name = 'System Administrator' limit 1];
        
        //Create a user
        User user = new User();
        user.Username = 'Test_user_name@test.com';
        user.LastName = 'Test_last_name';
        user.ProfileId = profile.Id;
        user.Alias = 'tst';
        user.Email = 'Test_email@email.com';
        user.CommunityNickname = 'Test_nick_name';
        user.TimeZoneSidKey = 'GMT';
        user.LocaleSidKey = 'en_US';
        user.LanguageLocaleKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        insert user;
        
        Test.startTest();
        //Simulate the page for What Id
        PageReference pPageReference = Page.OSM_MultipleTaskAssign;
        pPageReference.getParameters().put('objIds',testCase1.Id+','+testCase2.Id);
        pPageReference.getParameters().put('retUrl','');
        Test.setCurrentPage(pPageReference);
        
        OSM_MultipleTaskAssignController controller = new OSM_MultipleTaskAssignController();
        System.assertEquals(controller.showWhoId, true);
        controller.getTableDisplayNames();
        controller.saveNew();
        controller.save();
        controller.back();

        //Simulate the page for Who Id
        pPageReference = Page.OSM_MultipleTaskAssign;
        pPageReference.getParameters().put('objIds',contact1.Id+','+contact2.Id);
        pPageReference.getParameters().put('retUrl','');
        Test.setCurrentPage(pPageReference);
        controller = new OSM_MultipleTaskAssignController();
        System.assertEquals(controller.showWhoId, false);
        controller.getTableDisplayNames();
        //controller.getselReminderOptions();
        controller.saveNew();
        Pagereference pageRef = controller.save();
        System.assertEquals(pageRef, null);
        controller.back();

        controller.task.OwnerId = user.Id;
        controller.task.Subject = 'Test_Subject';
        controller.task.Status = 'Completed';
        controller.task.Priority = 'High';
        controller.task.Description = 'Test_Description';
        controller.task.OSM_Phone_Number__c = '1234567';
        //controller.task.OSM_Follow_Up_Date_Time__c = ;
        controller.task.OSM_Direction__c = 'Inbound';
        controller.task.Type = 'Phone';


    
    controller.saveNew();
    pageRef = controller.save();
    //System.assertNotEquals(pageRef, null);
    
        Test.stopTest();
    }
}