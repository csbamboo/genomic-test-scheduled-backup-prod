/*
 * File Info
 * ----------------------------------
 * @filename       OSM_ManageAddressExt.cls
 * @created        24.SEPT.2014
 * @author         Kristian Vegerano
 * @description    Extension class for OSM_AddressEdit Page. 
 * @history        24.SEPT.2014 - Kristian Vegerano - Created  
 */
public with sharing class OSM_NewAddressAffiliationExt {
    public OSM_Address__c pageAddress {get;set;}
    public OSM_Address_Affiliation__c pageAddressAffiliation {get;set;}
    public Boolean doShowName {get;set;}
    public Boolean saveRecord {get;set;}
    public List<SelectOption> countryList {get;set;}
    public List<SelectOption> stateList {get;set;}
    
    public List<Schema.FieldSetMember> getFields {
            get{
                    Id HCOrecType = Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('HCO Address Affiliation').getRecordTypeId(); 
                    Id HCPType = Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('HCP Address Affiliation').getRecordTypeId(); 
                    Id PayorecType = Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('Payor Address Affiliation').getRecordTypeId();
                    Id PlanrecType = Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('Plan Address Affiliation').getRecordTypeId();
                    
                    if(ApexPages.currentPage().getParameters().get('RecordType') == HCOrecType){
                        return SObjectType.OSM_Address_Affiliation__c.FieldSets.OSM_HCO_Layout_Fields.getFields();
                    } else if(ApexPages.currentPage().getParameters().get('RecordType') == HCPType){
                        return SObjectType.OSM_Address_Affiliation__c.FieldSets.OSM_HCP_Layout_Fields.getFields();
                    } else if(ApexPages.currentPage().getParameters().get('RecordType') == PayorecType){
                        return SObjectType.OSM_Address_Affiliation__c.FieldSets.OSM_Payor_Layout_Fields.getFields();
                    } else if(ApexPages.currentPage().getParameters().get('RecordType') == PlanrecType ){
                        return SObjectType.OSM_Address_Affiliation__c.FieldSets.OSM_Plan_Layout_Fields.getFields();
                    } else {
                        return SObjectType.OSM_Address_Affiliation__c.FieldSets.OSM_Study_Layout_Fields.getFields();
                    }
            }
            set;
    }
    
    /*
     * @author         Kristian Vegerano
     * @description    Constructor. 
     * @history        24.SEPT.2014 - Kristian Vegerano - Created  
     */
    public OSM_NewAddressAffiliationExt(ApexPages.StandardController stdController) {
        pageAddressAffiliation = (OSM_Address_Affiliation__c)stdController.getRecord();
        pageAddressAffiliation.name = 'Default Address Affiliation';
        pageAddress = new OSM_Address__c();
        Id accountId = ApexPages.currentPage().getParameters().get('accountId');
        Id planId = ApexPages.currentPage().getParameters().get('planId');
        pageAddressAffiliation.OSM_Account__c = accountId;
        pageAddressAffiliation.OSM_Plan__c = planId;
        system.debug('@#@#@' + pageAddressAffiliation.OSM_Account__c);
        saveRecord = false;
        
        Set<String> filterNames = new Set<String>();
        List<String> sortNames = new List<String>();
        
        if(pageAddress.Id != null){
            doShowName = true;
        }else{ 
            doShowName = false;
        }
        countryList = new List<SelectOption>();
        
        //Get Country Names
        countryList.add(new SelectOption('', '--None--'));
        for(OSM_Country_and_State__c loopCountryState : [SELECT Id, OSM_Country_Name__c 
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Active__c = true]){
            filterNames.add(loopCountryState.OSM_Country_Name__c);
        }
        if(filterNames.size() > 0){
            sortNames.addAll(filterNames);
            sortNames.sort();
            for(String loopCountryName : sortNames){
                if(loopCountryName != null){
                    countryList.add(new SelectOption(loopCountryName, loopCountryName));
                }
            }
        }
        
        //Call dependent state values
        updateCountry();
    }
    
    /*
     * @author         Kristian Vegerano
     * @description    Method that updates the state picklist depending on the selected country value. 
     * @history        24.SEPT.2014 - Kristian Vegerano - Created  
                       5  AUG 2015 - Patrick lorilla Modified
     */
    public void updateCountry() {
        Set<String> filterNames = new Set<String>();
        List<String> sortNames = new List<String>();
        
        stateList = new List<SelectOption>();
        stateList.add(new SelectOption('', '--None--'));
        system.debug('***pageAddress.OSM_Country__c' + pageAddress.OSM_Country__c);
        //Get Country Names

        pageAddress.OSM_State__c = stateList[0].getValue();
        for(OSM_Country_and_State__c loopCountryState : [SELECT Id, OSM_State_Name__c
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Active__c = true
                                                    AND OSM_Country_Name__c = :pageAddress.OSM_Country__c]){
            filterNames.add(loopCountryState.OSM_State_Name__c);
        }
        
         
        if(filterNames.size() > 1){
            //pageAddress.OSM_Zip__c = null;
            stateList.remove(0); //patrick lorilla remove --None-- to avoid validation error interruption
            sortNames.addAll(filterNames);
            sortNames.sort();
            for(String loopStateName : sortNames){
                if(loopStateName != null){
                    stateList.add(new SelectOption(loopStateName, loopStateName));
                }        
            }
        }else if(filterNames.size() == 1){
            updateState();
        }
        if(pageAddress.OSM_Country__c != null){
            OSM_Country_and_State__c countryState = [SELECT Id, OSM_State_Name__c, OSM_Country_Code__c
                                                     FROM OSM_Country_and_State__c
                                                     WHERE OSM_Active__c = true
                                                        AND OSM_Country_Name__c = :pageAddress.OSM_Country__c LIMIT 1];
            pageAddress.OSM_Country_and_State_ID__c = countryState.Id;
            pageAddress.OSM_Country_Code__c = countryState.OSM_Country_Code__c;
        }
    }
    
    /*
     * @author         Kristian Vegerano
     * @description    Method that updates the state picklist depending on the selected country value. 
     * @history        24.SEPT.2014 - Kristian Vegerano - Created  
     */


    public void updateState() {
        try{
            /*pageAddress.OSM_Zip__c = [SELECT Id, OSM_Zip_Code__c 
                                  FROM OSM_Country_and_State__c 
                                  WHERE OSM_Country_Name__c = :pageAddress.OSM_Country__c
                                      AND OSM_State_Name__c = :pageAddress.OSM_State__c].OSM_Zip_Code__c;*/ 
                                      
            if(pageAddress.OSM_State__c != null){
                OSM_Country_and_State__c countryState = [SELECT Id, OSM_Zip_Code__c, OSM_State_Code__c
                                                 FROM OSM_Country_and_State__c 
                                                 WHERE OSM_Country_Name__c = :pageAddress.OSM_Country__c
                                                     AND OSM_State_Name__c = :pageAddress.OSM_State__c];
                                                     
                pageAddress.OSM_Country_and_State_ID__c = countryState.Id;
                pageAddress.OSM_State_Code__c = countryState.OSM_State_Code__c;
            }  
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
    
    /*
     * @author         Daniel Quismorio
     * @description    Custom Saving for autolink of Address
     * @history        30.OCT.2014 - Daniel Quismorio - Created
                       5.15.2015 - Paul Angelo Added update on Customer Service Team Field
     */
    public pageReference save() {
        Record_Type__c systemSettings = Record_Type__c.getOrgDefaults();
        system.debug('@@@@@@@@' + pageAddress.OSM_Country__c);
        //System.debug('@@OSM_Address__c@@' + pageAddressAffiliation.OSM_Address__c);//ADDED
        //System.debug('@@OSM_Country__c@@' + pageAddress.OSM_Country__c);
        //System.debug('@@OSM_Address_Line_1__c@@' + pageAddress.OSM_Address_Line_1__c);
        //System.debug('@@OSM_City__c@@' + pageAddress.OSM_City__c);
        //System.debug('@@OSM_Zip__c@@' + pageAddress.OSM_Zip__c); //ADDED ENDS HERE
        
        OSM_Country_and_State__c csTeam = new OSM_Country_and_State__c();
        

       if(pageAddress.OSM_Country__c != null && (pageAddress.OSM_State__c != null || pageAddress.OSM_State__c != '--None--')){
           csTeam = [SELECT OSM_Customer_Service_Team__c FROM OSM_Country_and_State__c WHERE OSM_Country_Name__c =: pageAddress.OSM_Country__c AND
               OSM_State_Name__c =: pageAddress.OSM_State__c LIMIT 1]; 
       }
       
       else if(pageAddress.OSM_Country__c != null){
           csTeam = [SELECT OSM_Customer_Service_Team__c FROM OSM_Country_and_State__c WHERE OSM_Country_Name__c =: pageAddress.OSM_Country__c LIMIT 1];
       }
        
        Boolean createNewAddress = pageAddressAffiliation.OSM_Address__c == null && pageAddress.OSM_Country__c != null && pageAddress.OSM_Address_Line_1__c != null && pageAddress.OSM_City__c != null && pageAddress.OSM_Zip__c != null;
        if(createNewAddress){
            
            OSM_Address__c newAddress = new OSM_Address__c();
            newAddress.OSM_Address_Line_1__c = pageAddress.OSM_Address_Line_1__c;
            newAddress.OSM_Address_Line_2__c = pageAddress.OSM_Address_Line_2__c;
            newAddress.OSM_Country__c = pageAddress.OSM_Country__c;
            newAddress.OSM_State__c = pageAddress.OSM_State__c;
            newAddress.OSM_City__c = pageAddress.OSM_City__c;
            newAddress.OSM_Zip__c = pageAddress.OSM_Zip__c;
            newAddress.OSM_Country_and_State_ID__c = pageAddress.OSM_Country_and_State_ID__c;
            newAddress.OSM_Country_Code__c = pageAddress.OSM_Country_Code__c;
            newAddress.OSM_State_Code__c = pageAddress.OSM_State_Code__c;
            newAddress.Name = 'TempName';
            newAddress.OSM_Customer_Service_Team__c = csTeam.OSM_Customer_Service_Team__c;
            insert newAddress;
            
            pageAddressAffiliation.OSM_Address__c = newAddress.Id;
        }
        
        
        if(pageAddressAffiliation.OSM_Address__c == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select an Address or populate the Address Information (Required: Address Line 1, Country, City, Zip).'));
            return null;
        }
        
        try{   
            upsert pageAddressAffiliation;
        } catch (Exception e) {
            List<String> errMsg = e.getMessage().split(',');
            //system.debug('\n\n\n errMsg '+errMsg+'\n\n\n');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errMsg[1].substring(0, errMsg[1].length()-4)));
            return null;
        }
        
        Id retId;
        
        if(pageAddressAffiliation.OSM_Study__c != null) {
            retId = pageAddressAffiliation.OSM_Study__c;
        }
        else if(ApexPages.currentPage().getParameters().get('accountid') == null) {
            retId = pageAddressAffiliation.OSM_Contact__c;
        } else {
            retId = ApexPages.currentPage().getParameters().get('accountid');
        }
        //
        if(retId == null){
            if(ApexPages.currentPage().getParameters().get('CF'+systemSettings.CF_Address_Affiliation_Address__c+'_lkid') != null){
                retId = ApexPages.currentPage().getParameters().get('CF'+systemSettings.CF_Address_Affiliation_Address__c+'_lkid');
            }
            else if(ApexPages.currentPage().getParameters().get('planid') != null){
                retId = ApexPages.currentPage().getParameters().get('planid');
            } else {
                retId = ApexPages.currentPage().getParameters().get('retURL').substring(1, 16);
            }
        }
        System.debug('\n\n\n RETID'+retId);
        return new PageReference('/Apex/OSM_ConsoleRedirection?recordid=' + retId);
        
    }
}