/* @author: patrick lorilla & dane quismorio
   @date: 26 NOV 2014
   @description: New Address Affiliation Extension Test
*/

@isTest(seeAllData = TRUE)
public class OSM_NewAddressAffiliationExt_Test {

    /* @author: patrick lorilla & dane quismorio
       @date: 26 NOV 2014
       @description: New Address Affiliation Creation Test
    */
    static testMethod void newAddressAffiliationCreation(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = true;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        update triggerSwitch;
        //Prepare objects
        String saveVal = null;
        Account acct = new Account(
            Name='testName'
        );
        insert acct;
        
        OSM_Address_Affiliation__c addAffiliation = new OSM_Address_Affiliation__c();
        
        OSM_Address__c add = OSM_DataFactory.createAddress('test', 'test', 'test', 'test', 'test', 'test');
        insert add;
        //Prepare country and state
        List<OSM_Country_and_State__c> countryAndState = new List<OSM_Country_and_State__c>();
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12345'));
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Quebec', 'AB', 'QC'));
        insert countryAndState;
        //insert new List<OSM_Country_and_State__c>(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12345'),OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Quebec', 'AB', 'QC'));
        
        Test.startTest();
            //run page
            PageReference pageRef = Page.OSM_NewAddressAffiliation;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(addAffiliation);
            ApexPages.currentPage().getParameters().put('CF00NK0000001XNfv_lkid',acct.id);
            OSM_NewAddressAffiliationExt stdCtlr = new OSM_NewAddressAffiliationExt(sc);
            stdCtlr.pageAddress.OSM_Country__c = 'Canada';
            addAffiliation.Name = 'test';
            addAffiliation.OSM_Address__c = add.Id;
            addAffiliation.OSM_Account__c = acct.Id;
            stdCtlr.updateCountry();
            //saveVal = String.valueOf(stdCtlr.save());
        Test.stopTest();
        //assert pageref value
        //System.assert(saveVal!= null);
    }
    /* @author: 
       @date: 
       @description: New Address Affiliation Creation Test With Other RT
       @history: 
    */
    static testMethod void newAddressAffiliationCreationOtherWithState(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = true;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        update triggerSwitch;
        //Prepare objects
        String saveVal = null;
        Account acct = new Account(
            Name='testName'
        );
        insert acct;
        //Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        //insert recordTypeIds;
        
        OSM_Address_Affiliation__c addAffiliation = new OSM_Address_Affiliation__c();
        addAffiliation.OSM_Account__c = acct.ID;
        OSM_Address__c add = OSM_DataFactory.createAddress('test', 'test', 'test', 'test', 'test', 'test');
        OSM_Study__c stud = OSM_DataFactory.createStudy('TestName','BillToAddressTest');
        insert add;
        insert stud;
        //Prepare country and state
        List<OSM_Country_and_State__c> countryAndState = new List<OSM_Country_and_State__c>();
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12345'));
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Quebec', 'AB', 'QC'));
        countryAndState[0].OSM_Customer_Service_Team__c = 'Domestic';
        countryAndState[1].OSM_Customer_Service_Team__c = 'RWC';
        insert countryAndState;

        Test.startTest();
            //run page
            PageReference pageRef = Page.OSM_NewAddressAffiliation;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(addAffiliation);
            ApexPages.currentPage().getParameters().put('CF00NK0000001XNfv_lkid',acct.id);
            OSM_NewAddressAffiliationExt stdCtlr = new OSM_NewAddressAffiliationExt(sc);
            List<Schema.FieldSetMember> fldList = stdCtlr.getFields;
            stdCtlr.pageAddress.OSM_Country__c = 'Canada';
            addAffiliation.Name = 'test';
            addAffiliation.OSM_Address__c = add.Id;
            addAffiliation.OSM_Account__c = acct.Id;
            addAffiliation.OSM_Study__c = stud.id;
            stdCtlr.updateCountry();
            stdCtlr.pageAddressAffiliation.OSM_Address__c = null;
            stdCtlr.pageAddress.OSM_Address_Line_1__c = 'Line 1 Address';
            stdCtlr.pageAddress.OSM_City__c = 'CityDaw';
            stdCtlr.pageAddress.OSM_State__c = 'Alberta';
            stdCtlr.pageAddress.OSM_Zip__c = 'Zip1234';
            stdCtlr.updateState(); 
            saveVal= String.valueOf(stdCtlr.save());
        Test.stopTest();
        //assert pageref value
        //System.assert(saveVal!= null); //commented
    }

    /* @author: 
       @date: 
       @description: New Address Affiliation Creation Test With Other RT
       @history: 
    */
    static testMethod void newAddressAffiliationCreationOnSave(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = true;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        update triggerSwitch;
        //Prepare objects
        String saveVal = null;
        Account acct = new Account(
            Name='testName'
        );
        insert acct;

        Id HCOrecType = Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('HCO Address Affiliation').getRecordTypeId(); 

        //Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        //insert recordTypeIds;
        
        OSM_Study__c stud = OSM_DataFactory.createStudy('TestName','BillToAddressTest');
        insert stud;

        OSM_Address_Affiliation__c addAffiliation = new OSM_Address_Affiliation__c();
        addAffiliation.Name = 'Default Address Affiliation';
        addAffiliation.OSM_Account__c = acct.Id;
        addAffiliation.OSM_Study__c = stud.Id;
        addAffiliation.OSM_Address__c = null;
        addAffiliation.RecordTypeId = HCOrecType;

        List<OSM_Country_and_State__c> countryAndState = new List<OSM_Country_and_State__c>();
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12345'));
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Quebec', 'AB', 'QC'));
        countryAndState[0].OSM_Customer_Service_Team__c = 'Domestic';
        countryAndState[1].OSM_Customer_Service_Team__c = 'RWC';
        insert countryAndState;

        Test.startTest();
            //run page
            PageReference pageRef = Page.OSM_NewAddressAffiliation;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(addAffiliation);
            ApexPages.currentPage().getParameters().put('accountId',acct.id);
            ApexPages.currentPage().getParameters().put('RecordType',HCOrecType);
            OSM_NewAddressAffiliationExt stdCtlr = new OSM_NewAddressAffiliationExt(sc);
            stdCtlr.pageAddress.OSM_Country__c = 'Canada';
            stdCtlr.pageAddressAffiliation.OSM_Address__c = null;
            stdCtlr.pageAddressAffiliation.OSM_Account__c = acct.Id;
            stdCtlr.pageAddressAffiliation.OSM_Study__c = stud.id;
            stdCtlr.pageAddressAffiliation.OSM_Type__c = 'Main';
            stdCtlr.pageAddress.OSM_Address_Line_1__c = 'Line 1 Address';
            stdCtlr.pageAddress.OSM_City__c = 'CityDaw';
            stdCtlr.pageAddress.OSM_State__c = 'Alberta';
            stdCtlr.pageAddress.OSM_Zip__c = 'Zip1234';
            stdCtlr.save();
        Test.stopTest();
        //assert pageref value
        //System.assert(saveVal!= null); //commented
    }
    /* @author: 
       @date: 
       @description: New Address Affiliation Creation Test With Other RT
       @history: 
    */
    static testMethod void newAddressAffiliationCreationOnSave2(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = true;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        update triggerSwitch;
        //Prepare objects
        String saveVal = null;
        Account acct = new Account(
            Name='testName'
        );
        insert acct;

        Id HCOrecType = Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('HCO Address Affiliation').getRecordTypeId(); 

        //Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        //insert recordTypeIds;
        
        OSM_Study__c stud = OSM_DataFactory.createStudy('TestName','BillToAddressTest');
        insert stud;

        OSM_Address_Affiliation__c addAffiliation = new OSM_Address_Affiliation__c();
        addAffiliation.Name = 'Default Address Affiliation';
        addAffiliation.OSM_Account__c = acct.Id;
        addAffiliation.OSM_Address__c = null;
        addAffiliation.RecordTypeId = HCOrecType;
        insert addAffiliation;

        List<OSM_Country_and_State__c> countryAndState = new List<OSM_Country_and_State__c>();
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12345'));
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Quebec', 'AB', 'QC'));
        countryAndState[0].OSM_Customer_Service_Team__c = 'Domestic';
        countryAndState[1].OSM_Customer_Service_Team__c = 'RWC';
        insert countryAndState;

        Test.startTest();
            //run page
            PageReference pageRef = Page.OSM_NewAddressAffiliation;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(addAffiliation);
            ApexPages.currentPage().getParameters().put('CF00NK0000001XNfv_lkid',acct.id);
            ApexPages.currentPage().getParameters().put('RecordType',HCOrecType);
            OSM_NewAddressAffiliationExt stdCtlr = new OSM_NewAddressAffiliationExt(sc);
            stdCtlr.pageAddress.OSM_Country__c = 'Canada';
            stdCtlr.pageAddressAffiliation.OSM_Address__c = null;
            //stdCtlr.pageAddressAffiliation.OSM_Account__c = acct.Id;
            stdCtlr.pageAddressAffiliation.OSM_Study__c = stud.Id;
            stdCtlr.pageAddressAffiliation.OSM_Type__c = 'Main';
            stdCtlr.pageAddress.OSM_Address_Line_1__c = 'Line 1 Address';
            stdCtlr.pageAddress.OSM_City__c = 'CityDaw';
            stdCtlr.pageAddress.OSM_State__c = 'Alberta';
            stdCtlr.pageAddress.OSM_Zip__c = 'Zip1234';
            stdCtlr.save();
        Test.stopTest();
        //assert pageref value
        //System.assert(saveVal!= null); //commented
    }
    /* @author: 
       @date: 
       @description: New Address Affiliation Creation Test With Other RT
       @history: 
    */
    static testMethod void newAddressAffiliationCreationOnSave3(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Address_Affiliation_Trigger__c = true;
        triggerSwitch.Address_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        update triggerSwitch;
        //Prepare objects
        String saveVal = null;
        Account acct = new Account(
            Name='testName'
        );
        insert acct;
        Test.startTest();
        Id HCPType = Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('HCP Address Affiliation').getRecordTypeId(); 
        
        OSM_Study__c stud = OSM_DataFactory.createStudy('TestName','BillToAddressTest');
        insert stud;

        OSM_Address_Affiliation__c addAffiliation = new OSM_Address_Affiliation__c();
        addAffiliation.Name = 'Default Address Affiliation';
        addAffiliation.OSM_Account__c = acct.Id;
        addAffiliation.RecordTypeId = HCPType;
        

        List<OSM_Country_and_State__c> countryAndState = new List<OSM_Country_and_State__c>();
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Alberta', 'AB', '12345'));
        countryAndState.add(OSM_DataFactory.createCountryAndState('Canada', 'CA', 'Quebec', 'AB', 'QC'));
        //countryAndState[0].OSM_Customer_Service_Team__c = 'Domestic';
        //countryAndState[1].OSM_Customer_Service_Team__c = 'RWC';
        insert countryAndState;

        
            //run page
            insert addAffiliation;
            PageReference pageRef = Page.OSM_NewAddressAffiliation;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(addAffiliation);
            //ApexPages.currentPage().getParameters().put('CF00NK0000001XNfv_lkid',acct.id);
            //ApexPages.currentPage().getParameters().put('RecordType',HCPType);
            OSM_NewAddressAffiliationExt stdCtlr = new OSM_NewAddressAffiliationExt(sc);
            stdCtlr.pageAddress.OSM_Country__c = 'Canada';
            stdCtlr.pageAddressAffiliation.OSM_Address__c = null;
            //stdCtlr.pageAddressAffiliation.OSM_Account__c = acct.Id;
            //stdCtlr.pageAddressAffiliation.OSM_Study__c = stud.Id;
            stdCtlr.pageAddressAffiliation.OSM_Type__c = 'Main';
            stdCtlr.pageAddress.OSM_Address_Line_1__c = 'Line 1 Address';
            stdCtlr.pageAddress.OSM_City__c = 'CityDaw';
            stdCtlr.pageAddress.OSM_State__c = 'Alberta';
            stdCtlr.pageAddress.OSM_Zip__c = 'Zip1234';
            stdCtlr.save();
        Test.stopTest();

    }
}