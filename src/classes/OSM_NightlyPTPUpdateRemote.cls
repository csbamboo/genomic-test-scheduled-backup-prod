/*
@author: patrick lorilla
@description: nightly PTP update remote
@date:  19 June 2015

*/
global class OSM_NightlyPTPUpdateRemote {
     /*
        @author: patrick lorilla
        @description: delete real PTP
        @param: Plan Id
        @date:  19 June 2015
    
    */
     webService static void deleteRealPTP(ID planId) {
         OSM_Plan__c planData = [SELECT ID, OSM_Payor__r.CurrencyIsoCode  from OSM_Plan__c where ID =: planId];
         Apttus__APTS_Agreement__c dummyAgreement = [SELECT ID,  RecordTypeId, Apttus__Subtype__c from Apttus__APTS_Agreement__c where Name =: Label.OSM_Dummy_Agreement_Name];
         List<OSM_Payor_Test_Price__c> ptpList = [SELECT Id, OSM_Net_Price__c ,  OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c, OSM_Billing_Category__c, OSM_Billing_Cycle__c, OSM_Expected_Rate__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_CPT_Code__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Price_Method__c, OSM_List_Price__c, OSM_Start_Date__c,OSM_End_Date__c,OSM_Agreement_ID__c, OSM_Agreement_Line_Item_ID__c, OSM_Agreement_Record_Type__c, OSM_Agreement_Sub_Type__c, OSM_Currently_Active__c, OSM_Test__r.Name, OSM_Future_Effective__c, OSM_Active_Tiered_Rate__c, OSM_Pricing_Schema__c, OSM_Test__c, OSM_Plan__c FROM OSM_Payor_Test_Price__c WHERE OSM_Plan__c =: planId AND OSM_Currently_Active__c = false AND OSM_Future_Effective__c = false AND OSM_Agreement_ID__c !=: dummyAgreement.Id AND OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c !=: dummyAgreement.Id];
         List<OSM_Payor_Test_Price_History__c> ptpHistoryList = new List<OSM_Payor_Test_Price_History__c>();
         Map<String, OSM_Payor_Test_Price__c> ptpDummyMap = new Map<String, OSM_Payor_Test_Price__c>();
         Map<String, OSM_Payor_Test_Price__c> ptpDummyMapCreate = new Map<String, OSM_Payor_Test_Price__c>();
         Map<String, Decimal> ptpPriceMap = new Map<String, Decimal>();
         Map<String, Apttus__AgreementLineItem__c> dummyaliMap = new Map<String, Apttus__AgreementLineItem__c>();
         Set<ID> dummyAliIds = new Set<ID>();
         String currencyPayor = planData.OSM_Payor__r.CurrencyIsoCode;
         for(Apttus__AgreementLineItem__c ali: [SELECT Id, GHI_CPQ_Price_Method__c, Apttus__ProductId__r.Name, GHI_CPQ_Pricing_Schema__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, GHI_CPQ_CPT_Code__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus_CMConfig__StartDate__c, Apttus__ListPrice__c, GHI_CPQ_Billing_Cycle__c, GHI_CPQ_Billing_Category__c, CurrencyISOCode FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c =: dummyAgreement.Id]){
            if(!dummyAliMap.containsKey(ali.Apttus__ProductId__r.Name)){
                dummyAliMap.put(ali.Apttus__ProductId__r.Name, ali);  
                dummyAliIds.add(ali.Id);
             }
         }
         for(OSM_Payor_Test_Price__c ptp :[SELECT Id, OSM_Net_Price__c ,  OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c, OSM_Billing_Category__c, OSM_Billing_Cycle__c, OSM_Expected_Rate__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_CPT_Code__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Price_Method__c, OSM_List_Price__c, OSM_Start_Date__c,OSM_End_Date__c,OSM_Agreement_ID__c, OSM_Agreement_Line_Item_ID__c, OSM_Agreement_Record_Type__c, OSM_Agreement_Sub_Type__c, OSM_Currently_Active__c, OSM_Test__r.Name, OSM_Future_Effective__c, OSM_Active_Tiered_Rate__c, OSM_Pricing_Schema__c, OSM_Test__c, OSM_Plan__c FROM OSM_Payor_Test_Price__c WHERE OSM_Plan__c =: planId  AND OSM_Agreement_ID__c =: dummyAgreement.Id AND OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c =: dummyAgreement.Id]){
            String prodName =ptp.OSM_Test__r.Name;
            if(!ptpDummyMap.containsKey(prodName) ){
                 ptpDummyMap.put(prodName, ptp );  
            }
         }
         
         for(PriceBookEntry pbe: [SELECT Name, CurrencyIsoCode, Pricing_Category__c, UnitPrice, Pricebook2.Name, Product2.Name, ProductCode from PriceBookEntry where Pricing_Category__c = 'Private' AND Product2.Name IN: dummyaliMap.keySet() AND Pricebook2.Name =: currencyPayor + ' Private']){
            ptpPriceMap.put(pbe.Product2.Name, pbe.UnitPrice); 

             
         }
          Map<Id, RecordType> rtMap = new Map<Id, RecordType>([SELECT ID, Name from RecordType where ID =: dummyAgreement.RecordTypeId]);
         for(OSM_Payor_Test_Price__c del :ptpList){
             String prodName =del.OSM_Test__r.Name;
             if(!ptpDummyMap.containsKey(prodName) && !ptpDummyMapCreate.containsKey(prodName) ){
             
                            OSM_Payor_Test_Price__c ptpDummy1 = new OSM_Payor_Test_Price__c(       
                                                                                                    OSM_Plan__c = del.OSM_Plan__c,
                                                                                                    OSM_Test__c = del.OSM_Test__c,
                                                                                                    OSM_Start_Date__c = dummyAliMap.get(prodName).Apttus_CMConfig__StartDate__c,
                                                                                                    OSM_End_Date__c =  dummyAliMap.get(prodName).Apttus_CMConfig__EndDate__c,
                                                                                                    OSM_Agreement_ID__c =  dummyAgreement.Id ,
                                                                                                    OSM_Agreement_Line_Item_ID__c= dummyAliMap.get(prodName).Id,
                                                                                                    OSM_Agreement_Record_Type__c=  rtMap.get(dummyAgreement.RecordTypeId).Name,
                                                                                                    OSM_Agreement_Sub_Type__c =  dummyAgreement.Apttus__Subtype__c,
                                                                                                    //OSM_Net_Price__c =    dummyAliMap.get(ptpListUpdate[a].OSM_Test__r.Name).Apttus__NetPrice__c,
                                                                                                    OSM_List_Price__c =   ptpPriceMap.get(prodName),
                                                                                                    OSM_Price_Method__c =   dummyAliMap.get(prodName).GHI_CPQ_Price_Method__c,
                                                                                                    OSM_Pricing_Schema__c =   dummyAliMap.get(prodName).GHI_CPQ_Pricing_Schema__c,
                                                                                                    OSM_Quantity_End_Range__c =  dummyAliMap.get(prodName).GHI_CPQ_End_Range__c,
                                                                                                    OSM_Quantity_Start_Range__c =   dummyAliMap.get(prodName).GHI_CPQ_Start_Range__c,
                                                                                                    OSM_Assay_Score__c =   dummyAliMap.get(prodName).GHI_CPQ_Assay_Score__c,
                                                                                                    //OSM_Criteria__c = agreementliList[a].GHI_CPQ_Criteria__c,
                                                                                                    CurrencyIsoCode =   currencyPayor,
                                                                                                    OSM_Expected_Rate__c = dummyAliMap.get(prodName).GHI_CLM_Expected_Rate__c,
                                                                                                    OSM_Billing_Category__c =  dummyAliMap.get(prodName).GHI_CPQ_Billing_Category__c,
                                                                                                    OSM_CPT_Code__c =  dummyAliMap.get(prodName).GHI_CPQ_CPT_Code__c,
                                                                                                    OSM_Newly_Generated__c = true,
                                                                                                    OSM_Billing_Cycle__c =  dummyAliMap.get(prodName).GHI_CPQ_Billing_Cycle__c
                                                                                                   
                                                                                                 );
                            ptpDummyMapCreate.put(prodName, ptpDummy1 );  
             } 
             OSM_Payor_Test_Price_History__c ptpHistory1 = new OSM_Payor_Test_Price_History__c(
                                                                                                    OSM_Test__c = del.OSM_Test__c,
                                                                                                    OSM_Start_Date__c = del.OSM_Start_Date__c,
                                                                                                    OSM_End_Date__c = del.OSM_End_Date__c,
                                                                                                    OSM_Agreement_ID__c = del.OSM_Agreement_ID__c,
                                                                                                    OSM_Agreement_Line_Item_ID__c= del.OSM_Agreement_Line_Item_ID__c,
                                                                                                    OSM_Agreement_Record_Type__c=  del.OSM_Agreement_Record_Type__c,
                                                                                                    OSM_Agreement_Sub_Type__c =  del.OSM_Agreement_Sub_Type__c,
                                                                                                    OSM_Net_Price__c =   del.OSM_Net_Price__c,
                                                                                                    OSM_List_Price__c =  del.OSM_List_Price__c,
                                                                                                    OSM_Price_Method__c =  del.OSM_Price_Method__c,
                                                                                                    OSM_Pricing_Schema__c =  del.OSM_Pricing_Schema__c,
                                                                                                    OSM_Quantity_End_Range__c = del.OSM_Quantity_End_Range__c,
                                                                                                    OSM_Quantity_Start_Range__c = del.OSM_Quantity_Start_Range__c,
                                                                                                    OSM_Assay_Score__c =  del.OSM_Assay_Score__c,
                                                                                                    //OSM_Criteria__c = agreementliList[a].GHI_CPQ_Criteria__c,
                                                                                                    CurrencyIsoCode =  del.CurrencyIsoCode,
                                                                                                    OSM_Expected_Rate__c = del.OSM_Expected_Rate__c,
                                                                                                    OSM_Billing_Category__c = del.OSM_Billing_Category__c,
                                                                                                    OSM_Billing_Cycle__c = del.OSM_Billing_Cycle__c,
                                                                                                    OSM_CPT_Code__c = del.OSM_CPT_Code__c,
                                                                                                    OSM_Future_Effective__c = del.OSM_Future_Effective__c,
                                                                                                    OSM_Plan__c = del.OSM_Plan__c,
                                                                                                    OSM_Currently_Active__c = del.OSM_Currently_Active__c
                                                                                                 );  
               ptpHistoryList.add(ptpHistory1);
             
         
     }
     //Patrick L 8.4.2015 bug fix. Checks if there's an available CE PTP per test and it cancells its creation of Dummy PTP if its existent
     List<OSM_Payor_Test_Price__c> ptpListCE = [SELECT Id, OSM_Net_Price__c ,  OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c, OSM_Billing_Category__c, OSM_Billing_Cycle__c, OSM_Expected_Rate__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_CPT_Code__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Price_Method__c, OSM_List_Price__c, OSM_Start_Date__c,OSM_End_Date__c,OSM_Agreement_ID__c, OSM_Agreement_Line_Item_ID__c, OSM_Agreement_Record_Type__c, OSM_Agreement_Sub_Type__c, OSM_Currently_Active__c, OSM_Test__r.Name, OSM_Future_Effective__c, OSM_Active_Tiered_Rate__c, OSM_Pricing_Schema__c, OSM_Test__c,  OSM_Plan__c FROM OSM_Payor_Test_Price__c WHERE OSM_Plan__c =: planId AND OSM_Currently_Active__c = true  AND OSM_Future_Effective__c = false AND OSM_Agreement_ID__c !=: dummyAgreement.Id AND OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c !=: dummyAgreement.Id];
     //List<OSM_Payor_Test_Price__c> ptpListFE = [SELECT Id, OSM_Net_Price__c ,  OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c, OSM_Billing_Category__c, OSM_Billing_Cycle__c, OSM_Expected_Rate__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_CPT_Code__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Price_Method__c, OSM_List_Price__c, OSM_Start_Date__c,OSM_End_Date__c,OSM_Agreement_ID__c, OSM_Agreement_Line_Item_ID__c, OSM_Agreement_Record_Type__c, OSM_Agreement_Sub_Type__c, OSM_Currently_Active__c, OSM_Test__r.Name, OSM_Future_Effective__c, OSM_Active_Tiered_Rate__c, OSM_Pricing_Schema__c, OSM_Test__c,  OSM_Plan__c FROM OSM_Payor_Test_Price__c WHERE OSM_Plan__c =: planId AND OSM_Currently_Active__c = false  AND OSM_Future_Effective__c = true AND OSM_Agreement_ID__c !=: dummyAgreement.Id AND OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c !=: dummyAgreement.Id];
     //Integer ptpFEctr=0;
     for(OSM_Payor_Test_Price__c ptp :ptpListCE){
        //ptpFEctr = 0;
        //for(OSM_Payor_Test_Price__c pfe: ptpListFE){
            //if(pfe.OSM_Test__c == ptp.OSM_Test__c){
               // ptpFEctr++; 
            //}
        //}
        if(ptpDummyMapCreate.containsKey(ptp.OSM_Test__r.Name)){
            ptpDummyMapCreate.remove(ptp.OSM_Test__r.Name);
        }    
     }
     if(ptpList.size()>0){
            delete ptpList;
            
     }
     System.debug('\n\n\n ptpDUMMYMAPLIST'+ptpDummyMap.values());
     if(ptpHistoryList.size()>0){
        insert ptpHistoryList;
        insert ptpDummyMapCreate.values();
     }
     

  }
}