/*
@author: patrick lorilla
@description: Nightly PTP Update Remote
@date: 25-JUNE-2015
*/
@isTest(SeeAllData=true) 
public class OSM_NightlyPTPUpdateRemoteTest {
    
    testmethod static void deleteRealPTPTest(){
        List<Apttus__APTS_Agreement__c> newPTPListName = [SELECT Id from Apttus__APTS_Agreement__c where Name = 'Payor Pricing Agreement' LIMIT 1];
        List<Apttus__AgreementLineItem__c> dummyALIs =[SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement'];
        
        /*Apttus__APTS_Agreement__c newPTPListName = new Apttus__APTS_Agreement__c(
            Name = 'Payor Pricing Agreement'
        );
        
        insert newPTPListName;
        
        Apttus__AgreementLineItem__c dummyALIs = new Apttus__AgreementLineItem__c(
            Apttus__AgreementId__c = newPTPListName.Id,
            Apttus_CMConfig__StartDate__c = Date.today()
        );
        
        insert dummyALIs;
       */
        //prepare records
        List<OSM_Payor_Test_Price__c> contractedPTP = new List<OSM_Payor_Test_Price__c>();
        List<OSM_Payor_Test_Price__c> basicPTP = new List<OSM_Payor_Test_Price__c>();
        OSM_Plan__c plan = new OSM_Plan__c(Name ='myplan', OSM_QDX_Financial_Category__c = 'COMM');
        insert plan;
        Apttus__APTS_Agreement__c agreementTest = newPTPListName[0].clone(false, true, false, false);
        agreementTest.Name = 'Test Agreement';
        insert agreementTest;
        Apttus__AgreementLineItem__c realALI = dummyALIs[0].clone(false, true, false, false);
        realALI.Apttus__AgreementId__c = agreementTest.Id;
        insert realALI;
        
        Set<String> StringNames = new Set<String>();
        
        Product2 prod2 = [Select Id from Product2 where Name='IBC' and isActive=true limit 1];
        
        for(Integer a=0; a<20; a++){
            contractedPTP.add(OSM_DataFactory.createPTP(null, null, false,agreementTest.Id, realALI.Id, prod2.Id ));   
            contractedPTP[a].OSM_Net_Price__c = 1.50;
            contractedPTP[a].OSM_Plan__c = plan.Id;

        }
        insert contractedPTP;
        for(Integer a=0; a<20; a++){
            Datetime today = Datetime.now();
            Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
            Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(250 );   
            contractedPTP[a].OSM_Start_Date__c = defDateStart;
            contractedPTP[a].OSM_End_Date__c = defDateEnd;
        }
        
        update contractedPTP;
        //click button
        OSM_NightlyPTPUpdateRemote.deleteRealPTP(plan.Id);
        //assert if deleted 
        contractedPTP = [SELECT ID from OSM_Payor_Test_Price__c where OSM_Plan__c =: plan.Id];
        //System.assertEquals(contractedPTP.size(), 20);
    }

}