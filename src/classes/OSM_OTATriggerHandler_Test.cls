/*
      @author: David Catindoy
      @date: 28 JAN 2015 - Created
      @description: Territory Trigger Handler Test Class
      @history: 28 JAN 2015 - Updated (PL)

*/
@isTest
private class OSM_OTATriggerHandler_Test {
    
    static testmethod void TerritoryAssignTestMethod(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Territory_Assignment_Trigger__c = true;
        insert triggerSwitch;
        
        List<OSM_Territory__c> parentTerritoryList = new List<OSM_Territory__c>();
        List<OSM_Territory__c> territoryList = new List<OSM_Territory__c>();
        List<OSM_Order_Territory_Assignment__c> otaList = new List<OSM_Order_Territory_Assignment__c>();
        List<Order> orderList = new List<Order>();
        OSM_Territory__c territor1 = OSM_DataFactory.createTerritory('Territory Number1');
        parentTerritoryList.add(territor1);
        insert parentTerritoryList;
        OSM_Territory__c territor2 = OSM_DataFactory.createTerritory('Territory Number2');
        territor2.OSM_Parent_Territory__c = territor1.Id;
        territoryList.add(territor2);
        insert territoryList;
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id rtContact_HCP = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();

        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct = OSM_DataFactory.createContact(1,acct.Id,rtContactId);
        insert ctct;
        
        Order order1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'New');
		order1.RecordTypeId = rt;
        /*order1.OSM_Credit_Card__c = true;
        order1.OSM_Bill_Type__c = 'Patient Pre-Pay';*/
		insert order1;
		
		
		OSM_Order_Territory_Assignment__c ota1 = new OSM_Order_Territory_Assignment__c(OSM_OrderName__c =order1.Id,OSM_Territory_Name__c = territor1.Id);
		otaList.add(ota1);
		
		OSM_Territory_Sales_Rep_Assignment__c tsra = OSM_DataFactory.createTerritorySRA(UserInfo.getUserId() , territor1.Id , 'Read' , 'Read');
		insert tsra;
		
		Test.startTest();
		insert otaList;
		otaList.clear();
		ota1.OSM_Effective_Start_Date__c = system.today();
		ota1.OSM_Override__c = true;
		otaList.add(ota1);
		update otaList;
		otaList.clear();
		ota1.OSM_Override__c = false;
		otaList.add(ota1);
		OSM_OrderTerritoryAssignTriggerHandler.hasCreateOrderShareRun = false;
		OSM_OrderTerritoryAssignTriggerHandler.OTARollUpRunOnce = false;
		OSM_OrderTerritoryAssignTriggerHandler.updateNumberofTerritoriesRun = false;
		update otaList;
		OSM_OrderTerritoryAssignTriggerHandler.updateNumberofTerritories(otaList);
		OSM_OrderTerritoryAssignTriggerHandler.OTARollUpRunOnce = false;
		OSM_OrderTerritoryAssignTriggerHandler.hasDeleteOrderShareRun = false;
		delete otaList;
		Test.stopTest();
    }
}