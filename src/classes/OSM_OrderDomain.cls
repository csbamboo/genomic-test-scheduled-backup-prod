/*------------------------------------------------------------------------
    @author         Rajeev Reddy
    @company        Cloud Sherpas
    @description    Data domain class for the Order object
            https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>          <Brief Description of Change>
   06/04/2015      Rajeev Reddy         Created this class
---------------------------------------------------------------------------*/
public with sharing class OSM_OrderDomain {
  
  private static Set<Order> insertOrderSet;
  private static Set<Order> updateOrderSet;  
  private static List<Order> queriedOrders;
  private static Set<Id> CaseIdSet;
  private static Set<Id> OrderRoleIdSet;
  private static Set<Id> OrderIdSet;
   
  /********************************************************************
    @author       : Rescian Rey
    @description  : Order querying by Id
    @history:
      <date>        <author>        <description>
      JUN 9 2015    Rescian Rey     Created method.
      Jun 17 2015   Rao Meda        Added new fields and modified logic of filtering 
  ********************************************************************/
  public static List<Order> queryOrderByID(Set<Id> orderIDs) {
    
      List<Order> queriedOrdersByIds = new List<Order>();
      
    // check id Order IDs were passed
    if (orderIDs == null || orderIDs.size() == 0) {
             return queriedOrdersByIds;
    }
    
    // check if these Order have already beed queried with 
    if (queriedOrders != null && !queriedOrders.isEmpty() && OrderIdSet.containsAll(orderIDs)) {
        if (OrderIdSet.size() == orderIDs.size()) {
                return queriedOrders;
        } else {
            for( Order ord:queriedOrders) {
                if(orderIDs.contains(ord.id) ) {
                     queriedOrdersByIds.add(ord);
                }
            }
              return  queriedOrdersByIds;  
        }
    }
    else {
      OrderIdSet = orderIDs;
    }
    
    // query for Orders by Order ID
    queriedOrders = [SELECT 
              Id, OSM_Signature_Date__c, OSM_Patient__c, Status, OSM_Product__c, OSM_Order_Territory_Assignment_Count__c, OSM_Order_Set__c,  
              Order_Location__c, OSM_MockRunTrue__c, OSM_Oncology__c, OSM_Urology__c,  OSM_Study_Name__c, OSM_Ordering_HCO__c,
              OSM_Bill_Account__c,  OSM_Bill_Type__c,  OSM_Status__c , OSM_Specimen_Retrieval_Ready__c, OSM_Specimen_Retrieval_Option__c, OSM_Canceled_Reason__c 
            FROM Order 
            WHERE Id IN :orderIDs];
              
    return queriedOrders;
  }
    
    /*------------------------------------------------------------
       @author        Narasimharao Meda
       @company       Cloud Sherpas
       @description   Deletes the orders on he OSM_OrderDomain
       @date          08/11/2015

       History
       08/11/2015      Narasimharao Meda         Created this method
    ------------------------------------------------------------*/
    public static void deleteOrderList() {
        OrderIdSet = new Set<Id>();
        queriedOrders = new List<Order>();
    }
    
  /*------------------------------------------------------------
       @author        Don Ollila
       @company       Cloud Sherpas
       @description   Returns a list of Order object queried by Order Role ID.
       @date          06/05/2015
       @param         Set<Id> - list of Order Role IDs
       @return        List<Order> - list of Order objects
       
       History
      06/05/2015      Don Ollila         Created this method
    ------------------------------------------------------------
  public static List<Order> queryOrderByOrderRole(Set<Id> OrderRoleIDs) {
    
    // check id Order Role IDs were passed
    if (OrderRoleIDs == null || OrderRoleIDs.size() == 0) {
      OrderRoleIdSet = null;
      return null;
    }
    
    // check if these Order have already been queried with the same Order Role IDs
    if (queriedOrders != null && !queriedOrders.isEmpty() && OrderRoleIdSet.size() == OrderRoleIDs.size() && OrderRoleIdSet.containsAll(OrderRoleIDs)) {
      return queriedOrders;
    }
    else {
      OrderRoleIdSet = OrderRoleIDs;
    }
    
    // query for Orders by Order Role ID
    queriedOrders = [SELECT Id, OSM_Order_Role_Lab_Ready__c, OSM_Product__c, OSM_Order_Territory_Assignment_Count__c,  
                     OSM_Order_Set__c,  Order_Location__c, OSM_MockRunTrue__c,  OSM_Oncology__c, OSM_Urology__c,  OSM_Study_Name__c,  
                     OSM_Ordering_HCO__c
            FROM Order 
            WHERE Id IN: OrderRoleIdSet]; //pw - this will find none, don't you want 'where orderRoleid IN :orderRoleIdSet' ?
              
    return queriedOrders;
  }*/
   
}