/*
  @author: Amanpreet Singh Sidhu 
  @date: 28 June 2015
  @description: Test Class for OSM_OrderDomain apex class
  @history: 21 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest
public class OSM_OrderDomain_Test 
{
  
    static testMethod void orderWithCases() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Trigger__c = false;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id pricebookStandard = Test.getStandardPricebookId();
        
        Set<Id> setOrdercases = new Set<Id>();
        Set<Id> setOrder = new Set<Id>();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        order1.priceBook2Id = pricebookStandard;
        insert order1;
        
        setOrder.add(order1.Id);
                       
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(order1.id);
        insert workOrder;
        
        Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
        insert prod2;
        
        
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
        insert pbe;
        
        OrderItem ordItem = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, OSM_Lab_and_Report_Status__c = 'Pre-Processing');
        insert ordItem;
                        
        Case cs1 = new Case(RecordTypeId = caseRecordType,OSM_Primary_Order__c = order1.Id, OSM_Primary_Order_Line_Item__c = ordItem.Id,
                            OSM_Data_Transmission_Status__c = 'Complete', Status = 'Open', Type = 'TestType');
        insert cs1;
        
        setOrdercases.add(cs1.Id);
        
        Test.startTest();
        
        OSM_OrderDomain.queryOrderByID(setOrder);
        
        Test.stopTest();
    }
    
    static testMethod void emptySet() 
    {
        Set<Id> setOrder = new Set<Id>();
        
        Test.startTest();
        
        OSM_OrderDomain.queryOrderByID(setOrder);
        OSM_OrderDomain.deleteOrderList();
                   
        Test.stopTest();
    
    }
    
    static testMethod void testOrderDomain(){
        
        /* Initialization of variables */
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        Set<Id> ordIds = new Set<Id>();
        
        /* Id's */
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
        
        /* Custom Settings - TriggerSwitch */
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Trigger__c = true;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;

        /* Accounts */
        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;
        
        /* Contacts */
        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        /* Products */
        prodList.add(OSM_DataFactory.createProduct('IBC', true));
        prodList.add(OSM_DataFactory.createProduct('MMR and Colon', true));
        prodList.add(OSM_DataFactory.createProduct('MMR', true));
        prodList.add(OSM_DataFactory.createProduct('Colon', true));
        insert prodList;
        
        /* PricebookEntries */
        pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[0].Id, pb2Standard, 3));
        pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[1].Id, pb2Standard, 3));
        pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[2].Id, pb2Standard, 3));
        pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[3].Id, pb2Standard, 3));
        insert pbeList;

        /* Orders */
        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_Product__c = (i == 1 ? 'MMR and Colon' : 'IBC'), 
                Pricebook2Id = pb2Standard, 
                EffectiveDate = System.Today(), 
                OSM_Triage_Outcome__c = 'New',
                Status = 'New'
            ));        
        }
        insert ordList;
        
        /* Order Line Items */
        for(Integer i = 0; i < 2; i++){
            oliList.add(new OrderItem(
                OrderId = ordList[i].Id , 
                PricebookEntryId = pbeList[i].Id , 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        insert oliList;
        
        /* Work Orders */
        for(Integer i = 0; i < 2; i++){
            woList.add(OSM_DataFactory.createWorkOrder(ordList[i].Id));
        }
        
        for(Order ord : ordList){
            ordIds.add(ord.Id);
        }
        
        /* Test */
        Test.startTest();
        
        OSM_OrderDomain.queryOrderByID(ordIds);
        
        Test.stopTest();
    }

}