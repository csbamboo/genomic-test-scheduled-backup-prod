/*------------------------------------------------------------------------
    @author         Rao Meda
    @company        Cloud Sherpas
    @description    Data domain class for the OrderItem object
                    https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
                  
    Test Class:     n/a
    
    History
    <Date>          <Authors Name>   <Brief Description of Change>
    06/08/2015      Rao Meda         Created this class
---------------------------------------------------------------------------*/
public with sharing class OSM_OrderItemDomain {

    
    private static Set<OrderItem> insertOrderItemSet;
    private static Set<OrderItem> updateOrderItemSet;   
    private static List<OrderItem> queriedOrderItems;  
   // private static List<OrderItem> queriedOrderItemsByID;
    private static Set<Id> orIdSet;
  //  private static Set<Id> orItemIdSet;
    
    /*------------------------------------------------------------
       @author        Rao Meda
       @company       Cloud Sherpas
       @description   Returns a list of OrderItem object queried by Order.
       @date          06/08/2015
       @param         Set<Id> - list of IDs
       @return        List<OrderItem> - list of OrderItem objects
       
       History
       06/08/2015      Rao Meda         Created this method
       07/27/2015 patrick lorilla - added the OSM_Resulted_Order_Specimen_ID_Current__c and OSM_Resulted_Order_Specimen_ID_Current__r.OSM_Order__c in the query
    ------------------------------------------------------------*/
    public static List<OrderItem> queryOrderItemByOrder(Set<Id> orIDs) {
        Map<Id, OrderItem> orderItemMap = new Map<Id, OrderItem>();
        
        // check id Order IDs were passed
        if (orIDs == null || orIDs.size() == 0) {
            orIdSet = null;
            return new List<OrderItem>();
        }
        
        System.debug('Order Domain OrIDs:' + orIDs);
        System.debug('Order Domain OrIdSet:' + orIdSet);    
        // check if these OrderItem have already beed queried with the same Order IDs
        if (queriedOrderItems != null && !queriedOrderItems.isEmpty() && orIdSet != null && orIdSet.size() == orIDs.size() && orIdSet.containsAll(orIDs) ) {
         
            return queriedOrderItems;                         
        }
        else {
            orIdSet = orIDs;
        }
        
        // query for OrderItem by Order ID 
        // 7.27.2015 patrick lorilla - added the OSM_Resulted_Order_Specimen_ID_Current__c and OSM_Resulted_Order_Specimen_ID_Current__r.OSM_Order__c 
        queriedOrderItems = [SELECT Id, OSM_Bill_Type__c, OrderId, OSM_Bill_Account__c , Order.OSM_Triage_Outcome__c, OSM_Lab_and_Report_Status__c,  OSM_Missing_Data_Description__c,OSM_Order_Line_Item_Status__c,
                             OSM_Incompatible_Criteria_Desc1__c, OSM_Incompatible_Criteria_Desc2__c,  OSM_Node_Status_Determination_Desc__c, OSM_Order_Line_Item_ID__c, OSM_Primary_Payor__c , OSM_Resulted_Order_Specimen_ID_Current__c, OSM_Resulted_Order_Specimen_ID_Current__r.OSM_Order__c,
                             PricebookEntryId, PricebookEntry.Product2Id, PricebookEntry.Product2.Name 
                        FROM OrderItem 
                        WHERE OrderId IN: orIdSet];
         
        return queriedOrderItems;
    }
    
    /*------------------------------------------------------------
       @author        Andrew Castillo
       @company       Cloud Sherpas
       @description   Returns a list of OrderItem object queried by Order Item ID.
       @date          08/20/2015
       @param         Set<Id> - list of IDs
       @return        List<OrderItem> - list of OrderItem objects
       
       History
       08/20/2015     Andrew Castillo         Created this method
    ------------------------------------------------------------*/
    //@rao Sep 11 2015 commented this method, used existing method to get product Name and Pricebook information.
    /* public static List<OrderItem> queryOrderItemByOrderItem(Set<Id> orItemIDs) {
        
        // check id Order Item IDs were passed
        if (orItemIDs == null || orItemIDs.size() == 0) {
            orItemIdSet = null;
            return new List<OrderItem>();
        }
        
        System.debug('Order Domain OrIDs:' + orItemIDs);
        System.debug('Order Domain OrIdSet:' + orItemIDs);

        // check if these OrderItem have already beed queried with the same Order IDs
        if (queriedOrderItemsByID != null && !queriedOrderItemsByID.isEmpty() && orItemIdSet != null && orItemIdSet.size() == orItemIDs.size() && orItemIdSet.containsAll(orItemIDs) ) {
         
            return queriedOrderItemsByID;                         
        }
        else {
            orItemIdSet = orItemIDs;
        }
        
        // query for OrderItem by Order ID 
        // 7.27.2015 patrick lorilla - added the OSM_Resulted_Order_Specimen_ID_Current__c and OSM_Resulted_Order_Specimen_ID_Current__r.OSM_Order__c 
        queriedOrderItemsByID = [SELECT Id, PricebookEntryId, PricebookEntry.Product2Id, PricebookEntry.Product2.Name 
                             FROM OrderItem 
                             WHERE Id IN: orItemIdSet];
                            
        return queriedOrderItemsByID;
    }
    */
    /*------------------------------------------------------------
       @author        Rao Meda
       @company       Cloud Sherpas
       @description   Returns a list of OrderItem object queried by Order, and OLI Status.
       @date          06/08/2015
       @param         Set<Id> - list of Order IDs
       @param         String - Status
       @return        List<OrderItem> - list of OrderItem objects
       
       History
       06/08/2015      Rao Meda         Created this method
    ------------------------------------------------------------*/
    public static List<OrderItem> queryOrderItemByOLINotStatus(Set<Id> orIDs, String status) {
        
        // get OrderItem list queried by Order ID
        List<OrderItem> orderItemLst = queryOrderItemByOrder(orIDs);
        
        if (orderItemLst == null) {
            return null;
        }
        
        // loop through list of OrderItems and filter by statuses
        List<OrderItem> filteredOrderItems = new List<OrderItem>();
        for (OrderItem oli : orderItemLst) {
            
            // filter on status field
            if (oli.OSM_Order_Line_Item_Status__c != status) {
                filteredOrderItems.add(oli);
            }
        }
        
        return filteredOrderItems;
    }

    
    /*------------------------------------------------------------
       @author        Rao Meda
       @company       Cloud Sherpas
       @description   Add a OrderItem to the list of OrderItems to insert.
       @date          06/08/2015
       @param         OrderItem - OrderItem sObject
       @return        N/A
       
       History
       06/08/2015      Rao Meda         Created this method
    ------------------------------------------------------------*/
    public static void addInsertOrderItem(OrderItem oli) {
        
        // check if set is initiallized
        if (insertOrderItemSet == null) {
            insertOrderItemSet = new Set<OrderItem>();
        }
        
        if (!insertOrderItemSet.contains(oli)) {
            insertOrderItemSet.add(oli);
        }
    }
    
    /*------------------------------------------------------------
       @author        Rao Meda
       @company       Cloud Sherpas
       @description   Add a OrderItem to the list of OrderItems to update.
       @date          06/08/2015
       @param         OrderItem - OrderItem sObject
       @return        N/A
       
       History
       06/08/2015      Rao Meda         Created this method
    ------------------------------------------------------------*/
    public static void addUpdateOrderItem(OrderItem oli) {
        
        // check if set is initiallized
        if (updateOrderItemSet == null) {
            updateOrderItemSet = new Set<OrderItem>();
        }
        
        if (!updateOrderItemSet.contains(oli)) {
            updateOrderItemSet.add(oli);
        }
    }   
    
    /*------------------------------------------------------------
       @author        Rao Meda
       @company       Cloud Sherpas
       @description   Performs DML operations on lists of OrderItems.
       @date          06/08/2015
       @param         N/A
       @return        N/A
       
       History
       06/08/2015      Rao Meda         Created this method
    ------------------------------------------------------------*/
    public static void commitOrderItem() {
        
        // perform insert
        if (insertOrderItemSet.size() > 0) {
            insert new List<OrderItem>(insertOrderItemSet);
        }
        
        // perform update
        if (updateOrderItemSet.size() > 0) {
            update new List<OrderItem>(updateOrderItemSet);
        }
    }

}