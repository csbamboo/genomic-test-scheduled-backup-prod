/*
  @author: Amanpreet Singh Sidhu 
  @date: 28 June 2015
  @description: Test Class for OSM_OrderItemDomain apex class
  @history: 28 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest
public class OSM_OrderItemDomain_Test 
{
  
    static testMethod void orderItemWithOrder() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Trigger__c = false;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id pricebookStandard = Test.getStandardPricebookId();
        
        Set<Id> setOrder = new Set<Id>();
        //List<Case> lstCase = new List<Case>();
        List<OrderItem> lstOrderItem = new List<OrderItem>();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        order1.priceBook2Id = pricebookStandard;
        insert order1;
        
        setOrder.add(order1.Id);
                       
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(order1.id);
        insert workOrder;
        
        Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
        insert prod2;
                
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
        insert pbe;
        
        OrderItem ordItem = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , 
                                         UnitPrice = 2 , Quantity = 1, OSM_Lab_and_Report_Status__c = 'Pre-Processing', OSM_Order_Line_Item_Status__c = 'Processing');
              
        OrderItem ordItem2 = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id,
                                         UnitPrice = 5 , Quantity = 2, OSM_Lab_and_Report_Status__c = 'Pre-Processing', OSM_Order_Line_Item_Status__c = 'Processing');
        
        OrderItem ordItem3 = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id,
                                         UnitPrice = 5 , Quantity = 2, OSM_Lab_and_Report_Status__c = 'Pre-Processing', OSM_Order_Line_Item_Status__c = 'Processing');     
        
        lstOrderItem.add(ordItem);
        lstOrderItem.add(ordItem2);
                
        insert lstOrderItem;
             
                
        Test.startTest();
        
        OSM_OrderItemDomain.queryOrderItemByOrder(setOrder);
        OSM_OrderItemDomain.queryOrderItemByOLINotStatus(setOrder,'Submitting');
        OSM_OrderItemDomain.addInsertOrderItem(ordItem3);
        OSM_OrderItemDomain.addUpdateOrderItem(ordItem);
        OSM_OrderItemDomain.commitOrderItem();
        //OSM_OrderItemDomain.queryOrderItemByOrderItem(setOrder);
                   
        Test.stopTest();
    
    }
    
     static testMethod void emptySet() 
    {
        Set<Id> setOrder = new Set<Id>();
        
        Test.startTest();
        
        OSM_OrderItemDomain.queryOrderItemByOrder(setOrder);
                   
        Test.stopTest();
    
    }
    
}