public without sharing class OSM_OrderLineItemPageLayoutExt {
    private Boolean hardcodeList = true; // true: hardcoded fields   ||   false: dynamic fields
    public OrderItem oliDetails {get; set;}
    public String productLayout {get;set;}
    public Map<String,List<String>> layoutMapping {get;set;}
    public Boolean isEdit {get;set;}
    public Id oliId;
    
    public OSM_OrderLineItemPageLayoutExt(ApexPages.StandardController controller) {
        String oliId = ApexPages.currentPage().getParameters().get('id');
        this.oliId = oliId;
        oliDetails = new OrderItem();
        OrderItem tempOrderItem = [SELECT Id, Pricebookentry.Product2.OLI_Page_Layout__c FROM OrderItem WHERE Id = :oliId];
        productLayout = tempOrderItem.Pricebookentry.Product2.OLI_Page_Layout__c != null ? tempOrderItem.Pricebookentry.Product2.OLI_Page_Layout__c : null;
        
        layoutMapping = new Map<String,List<String>>();
        
        //Determine if page is from view or edit
        if(ApexPages.currentPage().getParameters().containsKey('retURL')){
            isEdit = true;
        }else{
            isEdit = false;
        }
        
        //Colon
        layoutMapping.put('Colon', new List<String>());
        layoutMapping.get('Colon').add('OSM_Colon_Clinical_Stage__c');

        layoutMapping.get('Colon').add('OSM_Order_Line_Item_ID__c');
        layoutMapping.get('Colon').add('OrderId'); // Order
        layoutMapping.get('Colon').add('Pricebookentry.Product2Id'); // Product
        layoutMapping.get('Colon').add('OSM_Product_Name__c');
        layoutMapping.get('Colon').add('Pricebookentry.Product2.ProductCode');
        layoutMapping.get('Colon').add('Quantity');
        layoutMapping.get('Colon').add('OSM_ICD_Coding_System__c');
        layoutMapping.get('Colon').add('OSM_ER_Status__c');
        layoutMapping.get('Colon').add('OSM_NCCN_Risk_Category__c');
        layoutMapping.get('Colon').add('OSM_PSA__c');
        layoutMapping.get('Colon').add('OSM_Positive_Cores__c');
        layoutMapping.get('Colon').add('OSM_Work_Order_ID__c');
        layoutMapping.get('Colon').add('OSM_Diagnosis_Description__c');
        layoutMapping.get('Colon').add('OSM_ICD_Code__c');
        layoutMapping.get('Colon').add('OSM_Case__c');
        layoutMapping.get('Colon').add('OSM_ABN_Required__c');
        layoutMapping.get('Colon').add('OSM_Exception_Criteria__c');
        layoutMapping.get('Colon').add('OSM_BI_Ready__c');
        layoutMapping.get('Colon').add('OSM_Multiple_Primaries_Confirmed__c');
        layoutMapping.get('Colon').add('OSM_Order_Created_On__c');
        layoutMapping.get('Colon').add('OSM_First_Successful_Date_and_Time__c');
        layoutMapping.get('Colon').add('OSM_Migration_ID_1__c');
        layoutMapping.get('Colon').add('OSM_Specimen_Retrieval_Ready__c');
        layoutMapping.get('Colon').add('OSM_Data_Entry_Status__c');
        layoutMapping.get('Colon').add('OSM_Lab_and_Report_Status__c');
        layoutMapping.get('Colon').add('OSM_Specimen_Retrieval_Option__c');
        layoutMapping.get('Colon').add('OSM_Billing_Policy_Qualification_Status__c');
        layoutMapping.get('Colon').add('OSM_Billing_Policy_Data_Status__c');
        layoutMapping.get('Colon').add('OSM_Canceled_Reason__c');
        layoutMapping.get('Colon').add('OSM_First_Specimen_Received_Date__c');
        layoutMapping.get('Colon').add('OSM_Pre_Billing_Complete_Date_Current__c');
        layoutMapping.get('Colon').add('OSM_Pre_Billing_Sent_Date_Current__c');
        layoutMapping.get('Colon').add('OSM_Billing_Request_Sent_Date_Current__c');
        layoutMapping.get('Colon').add('OSM_Billing_Void_Sent_Date_Current__c');
        layoutMapping.get('Colon').add('Results_Disposition_Current__c');
        layoutMapping.get('Colon').add('OSM_Billing_Policy_Bill_Entity__c');
        layoutMapping.get('Colon').add('OSM_Special_Handling_Comments__c');
        layoutMapping.get('Colon').add('OSM_Date_of_Service__c');
        layoutMapping.get('Colon').add('OSM_Date_Distributed_Current__c');
        layoutMapping.get('Colon').add('OSM_Resulted_Order_Specimen_ID_Current__c');
        layoutMapping.get('Colon').add('OSM_Lab_Ready__c');
        layoutMapping.get('Colon').add('OSM_Report_Distribution_Ready__c');
        layoutMapping.get('Colon').add('OSM_Primary_Sequence_Number__c');
        layoutMapping.get('Colon').add('OSM_OS_OLI_Billing_ready__c');
        layoutMapping.get('Colon').add('OSM_Billing_policy_N_A__c');
        layoutMapping.get('Colon').add('OSM_OLI_Data_Missing__c');
        layoutMapping.get('Colon').add('OSM_Purchase_Order_Number__c');
        //layoutMapping.get('Colon').add('OSM_Orderable_TEST__c');
        layoutMapping.get('Colon').add('OSM_KPI_Billed_Test_Date__c');
        //layoutMapping.get('Colon').add('OSM_ICD_System_TEST__c');
        //layoutMapping.get('Colon').add('OSM_ICD_Code_TEST__c');
        layoutMapping.get('Colon').add('OSM_KPI_Demand_by_Test_Date__c');
        layoutMapping.get('Colon').add('OSM_ER_Negative_Failure_Report_Override__c');
        layoutMapping.get('Colon').add('OSM_Verification_User__c');
        layoutMapping.get('Colon').add('OSM_Result_Current__c');
        layoutMapping.get('Colon').add('OSM_Failure_Code_Current__c');
        layoutMapping.get('Colon').add('OSM_Out_of_Criteria__c');
        layoutMapping.get('Colon').add('OSM_Chemotherapy__c');
        layoutMapping.get('Colon').add('OSM_FEC_Prescribed__c');
        layoutMapping.get('Colon').add('OSM_Date_of_Treatment_Decision__c');
        layoutMapping.get('Colon').add('Order.TotalAmount'); // Total Price
        layoutMapping.get('Colon').add('OSM_Test_Name__c');
        layoutMapping.get('Colon').add('OSM_Order_Line_Item_Status__c');
        layoutMapping.get('Colon').add('OSM_State__c');
        layoutMapping.get('Colon').add('OSM_Submitting_Diagnosis__c');
        layoutMapping.get('Colon').add('OSM_Nodal_Status__c');
        layoutMapping.get('Colon').add('OSM_Nodal_Status_Determination_Source__c');
        layoutMapping.get('Colon').add('OSM_Submitted_Gleason_Score__c');
        layoutMapping.get('Colon').add('OSM_GHI_Gleason_Score__c');
        layoutMapping.get('Colon').add('OSM_Result_Disposition__c');
        layoutMapping.get('Colon').add('UnitPrice');
        layoutMapping.get('Colon').add('OSM_Is_Billing__c');
        layoutMapping.get('Colon').add('OSM_10_Yr_Survival_w_Chemo_Adjuvant__c');
        layoutMapping.get('Colon').add('OSM_10_Yr_Survival_w_Chemo_PREDICT__c');
        layoutMapping.get('Colon').add('OSM_Treatment_Rec_Prior_to_DX_Test__c');
        layoutMapping.get('Colon').add('OSM_Verification_User_Team__c');
        layoutMapping.get('Colon').add('OSM_Verification_User_Sub_Team__c');
        layoutMapping.get('Colon').add('OSM_Verification_Last_Modified__c');
        layoutMapping.get('Colon').add('OSM_Estimated_Report_Date__c');
        layoutMapping.get('Colon').add('OSM_OSpecimen_Report_Distribution_Ready__c');
        layoutMapping.get('Colon').add('OSM_Primary_Payor__C');
        layoutMapping.get('Colon').add('OSM_Primary_Payor_ID__c');
        layoutMapping.get('Colon').add('OSM_Plan__c');
        layoutMapping.get('Colon').add('OSM_Billing_Status__c');
        layoutMapping.get('Colon').add('OSM_Bill_Type__c');
        layoutMapping.get('Colon').add('OSM_Bill_Account__c');
        
        //DCIS
        layoutMapping.put('DCIS', new List<String>());
        layoutMapping.get('DCIS').add('OSM_DCIS_Clinical_Stage__c');
        
        layoutMapping.get('DCIS').add('OSM_Order_Line_Item_ID__c');
        layoutMapping.get('DCIS').add('OrderId'); // Order
        layoutMapping.get('DCIS').add('Pricebookentry.Product2Id'); // Product
        layoutMapping.get('DCIS').add('OSM_Product_Name__c');
        layoutMapping.get('DCIS').add('Pricebookentry.Product2.ProductCode');
        layoutMapping.get('DCIS').add('Quantity');
        layoutMapping.get('DCIS').add('OSM_ICD_Coding_System__c');
        layoutMapping.get('DCIS').add('OSM_ER_Status__c');
        layoutMapping.get('DCIS').add('OSM_NCCN_Risk_Category__c');
        layoutMapping.get('DCIS').add('OSM_PSA__c');
        layoutMapping.get('DCIS').add('OSM_Positive_Cores__c');
        layoutMapping.get('DCIS').add('OSM_Work_Order_ID__c');
        layoutMapping.get('DCIS').add('OSM_Diagnosis_Description__c');
        layoutMapping.get('DCIS').add('OSM_ICD_Code__c');
        layoutMapping.get('DCIS').add('OSM_Case__c');
        layoutMapping.get('DCIS').add('OSM_ABN_Required__c');
        layoutMapping.get('DCIS').add('OSM_Exception_Criteria__c');
        layoutMapping.get('DCIS').add('OSM_BI_Ready__c');
        layoutMapping.get('DCIS').add('OSM_Multiple_Primaries_Confirmed__c');
        layoutMapping.get('DCIS').add('OSM_Order_Created_On__c');
        layoutMapping.get('DCIS').add('OSM_First_Successful_Date_and_Time__c');
        layoutMapping.get('DCIS').add('OSM_Migration_ID_1__c');
        layoutMapping.get('DCIS').add('OSM_Specimen_Retrieval_Ready__c');
        layoutMapping.get('DCIS').add('OSM_Data_Entry_Status__c');
        layoutMapping.get('DCIS').add('OSM_Lab_and_Report_Status__c');
        layoutMapping.get('DCIS').add('OSM_Specimen_Retrieval_Option__c');
        layoutMapping.get('DCIS').add('OSM_Billing_Policy_Qualification_Status__c');
        layoutMapping.get('DCIS').add('OSM_Billing_Policy_Data_Status__c');
        layoutMapping.get('DCIS').add('OSM_Canceled_Reason__c');
        layoutMapping.get('DCIS').add('OSM_First_Specimen_Received_Date__c');
        layoutMapping.get('DCIS').add('OSM_Pre_Billing_Complete_Date_Current__c');
        layoutMapping.get('DCIS').add('OSM_Pre_Billing_Sent_Date_Current__c');
        layoutMapping.get('DCIS').add('OSM_Billing_Request_Sent_Date_Current__c');
        layoutMapping.get('DCIS').add('OSM_Billing_Void_Sent_Date_Current__c');
        layoutMapping.get('DCIS').add('Results_Disposition_Current__c');
        layoutMapping.get('DCIS').add('OSM_Billing_Policy_Bill_Entity__c');
        layoutMapping.get('DCIS').add('OSM_Special_Handling_Comments__c');
        layoutMapping.get('DCIS').add('OSM_Date_of_Service__c');
        layoutMapping.get('DCIS').add('OSM_Date_Distributed_Current__c');
        layoutMapping.get('DCIS').add('OSM_Resulted_Order_Specimen_ID_Current__c');
        layoutMapping.get('DCIS').add('OSM_Lab_Ready__c');
        layoutMapping.get('DCIS').add('OSM_Report_Distribution_Ready__c');
        layoutMapping.get('DCIS').add('OSM_Primary_Sequence_Number__c');
        layoutMapping.get('DCIS').add('OSM_OS_OLI_Billing_ready__c');
        layoutMapping.get('DCIS').add('OSM_Billing_policy_N_A__c');
        layoutMapping.get('DCIS').add('OSM_OLI_Data_Missing__c');
        layoutMapping.get('DCIS').add('OSM_Purchase_Order_Number__c');
        //layoutMapping.get('DCIS').add('OSM_Orderable_TEST__c');
        layoutMapping.get('DCIS').add('OSM_KPI_Billed_Test_Date__c');
        //layoutMapping.get('DCIS').add('OSM_ICD_System_TEST__c');
        //layoutMapping.get('DCIS').add('OSM_ICD_Code_TEST__c');
        layoutMapping.get('DCIS').add('OSM_KPI_Demand_by_Test_Date__c');
        layoutMapping.get('DCIS').add('OSM_ER_Negative_Failure_Report_Override__c');
        layoutMapping.get('DCIS').add('OSM_Verification_User__c');
        layoutMapping.get('DCIS').add('OSM_Result_Current__c');
        layoutMapping.get('DCIS').add('OSM_Failure_Code_Current__c');
        layoutMapping.get('DCIS').add('OSM_Out_of_Criteria__c');
        layoutMapping.get('DCIS').add('OSM_Tumor_Size__c');
        layoutMapping.get('DCIS').add('OSM_Chemotherapy__c');
        layoutMapping.get('DCIS').add('OSM_FEC_Prescribed__c');
        layoutMapping.get('DCIS').add('OSM_Date_of_Treatment_Decision__c');
        layoutMapping.get('DCIS').add('Order.TotalAmount'); // Total Price
        layoutMapping.get('DCIS').add('OSM_Test_Name__c');
        layoutMapping.get('DCIS').add('OSM_Order_Line_Item_Status__c');
        layoutMapping.get('DCIS').add('OSM_State__c');
        layoutMapping.get('DCIS').add('OSM_Submitting_Diagnosis__c');
        layoutMapping.get('DCIS').add('OSM_Nodal_Status__c');
        layoutMapping.get('DCIS').add('OSM_Nodal_Status_Determination_Source__c');
        layoutMapping.get('DCIS').add('OSM_Submitted_Gleason_Score__c');
        layoutMapping.get('DCIS').add('OSM_GHI_Gleason_Score__c');
        layoutMapping.get('DCIS').add('OSM_Result_Disposition__c');
        layoutMapping.get('DCIS').add('UnitPrice');
        layoutMapping.get('DCIS').add('OSM_Is_Billing__c');
        layoutMapping.get('DCIS').add('OSM_10_Yr_Survival_w_Chemo_Adjuvant__c');
        layoutMapping.get('DCIS').add('OSM_10_Yr_Survival_w_Chemo_PREDICT__c');
        layoutMapping.get('DCIS').add('OSM_Treatment_Rec_Prior_to_DX_Test__c');
        layoutMapping.get('DCIS').add('OSM_Verification_User_Team__c');
        layoutMapping.get('DCIS').add('OSM_Verification_User_Sub_Team__c');
        layoutMapping.get('DCIS').add('OSM_Verification_Last_Modified__c');
        layoutMapping.get('DCIS').add('OSM_Estimated_Report_Date__c');
        layoutMapping.get('DCIS').add('OSM_OSpecimen_Report_Distribution_Ready__c');
        layoutMapping.get('DCIS').add('OSM_Primary_Payor__C');
        layoutMapping.get('DCIS').add('OSM_Primary_Payor_ID__c');
        layoutMapping.get('DCIS').add('OSM_Plan__c');
        layoutMapping.get('DCIS').add('OSM_Billing_Status__c');
        layoutMapping.get('DCIS').add('OSM_Bill_Type__c');
        layoutMapping.get('DCIS').add('OSM_Bill_Account__c');
        
        //IBC
        layoutMapping.put('IBC', new List<String>());
        //layoutMapping.get('IBC').add('OSM_ER_Status__c');
        //layoutMapping.get('IBC').add('OSM_Her_2_Status__c');
        layoutMapping.get('IBC').add('OSM_IBC_Clinical_Stage__c');
        //layoutMapping.get('IBC').add('OSM_Nodal_Status__c');
        //layoutMapping.get('IBC').add('OSM_Nodal_Status_Determination_Source__c');
        
        layoutMapping.get('IBC').add('OSM_Order_Line_Item_ID__c');
        layoutMapping.get('IBC').add('OrderId'); // Order
        layoutMapping.get('IBC').add('Pricebookentry.Product2Id'); // Product
        layoutMapping.get('IBC').add('OSM_Product_Name__c');
        layoutMapping.get('IBC').add('Pricebookentry.Product2.ProductCode');
        layoutMapping.get('IBC').add('Quantity');
        layoutMapping.get('IBC').add('OSM_ICD_Coding_System__c');
        layoutMapping.get('IBC').add('OSM_ER_Status__c');
        layoutMapping.get('IBC').add('OSM_NCCN_Risk_Category__c');
        layoutMapping.get('IBC').add('OSM_PSA__c');
        layoutMapping.get('IBC').add('OSM_Positive_Cores__c');
        layoutMapping.get('IBC').add('OSM_Work_Order_ID__c');
        layoutMapping.get('IBC').add('OSM_Diagnosis_Description__c');
        layoutMapping.get('IBC').add('OSM_ICD_Code__c');
        layoutMapping.get('IBC').add('OSM_Case__c');
        layoutMapping.get('IBC').add('OSM_ABN_Required__c');
        layoutMapping.get('IBC').add('OSM_Exception_Criteria__c');
        layoutMapping.get('IBC').add('OSM_BI_Ready__c');
        layoutMapping.get('IBC').add('OSM_Multiple_Primaries_Confirmed__c');
        layoutMapping.get('IBC').add('OSM_Order_Created_On__c');
        layoutMapping.get('IBC').add('OSM_First_Successful_Date_and_Time__c');
        layoutMapping.get('IBC').add('OSM_Migration_ID_1__c');
        layoutMapping.get('IBC').add('OSM_Specimen_Retrieval_Ready__c');
        layoutMapping.get('IBC').add('OSM_Data_Entry_Status__c');
        layoutMapping.get('IBC').add('OSM_Lab_and_Report_Status__c');
        layoutMapping.get('IBC').add('OSM_Specimen_Retrieval_Option__c');
        layoutMapping.get('IBC').add('OSM_Billing_Policy_Qualification_Status__c');
        layoutMapping.get('IBC').add('OSM_Billing_Policy_Data_Status__c');
        layoutMapping.get('IBC').add('OSM_Canceled_Reason__c');
        layoutMapping.get('IBC').add('OSM_First_Specimen_Received_Date__c');
        layoutMapping.get('IBC').add('OSM_Pre_Billing_Complete_Date_Current__c');
        layoutMapping.get('IBC').add('OSM_Pre_Billing_Sent_Date_Current__c');
        layoutMapping.get('IBC').add('OSM_Billing_Request_Sent_Date_Current__c');
        layoutMapping.get('IBC').add('OSM_Billing_Void_Sent_Date_Current__c');
        layoutMapping.get('IBC').add('Results_Disposition_Current__c');
        layoutMapping.get('IBC').add('OSM_Billing_Policy_Bill_Entity__c');
        layoutMapping.get('IBC').add('OSM_Special_Handling_Comments__c');
        layoutMapping.get('IBC').add('OSM_Date_of_Service__c');
        layoutMapping.get('IBC').add('OSM_Date_Distributed_Current__c');
        layoutMapping.get('IBC').add('OSM_Resulted_Order_Specimen_ID_Current__c');
        layoutMapping.get('IBC').add('OSM_Lab_Ready__c');
        layoutMapping.get('IBC').add('OSM_Report_Distribution_Ready__c');
        layoutMapping.get('IBC').add('OSM_Primary_Sequence_Number__c');
        layoutMapping.get('IBC').add('OSM_OS_OLI_Billing_ready__c');
        layoutMapping.get('IBC').add('OSM_Billing_policy_N_A__c');
        layoutMapping.get('IBC').add('OSM_OLI_Data_Missing__c');
        layoutMapping.get('IBC').add('OSM_Purchase_Order_Number__c');
        //layoutMapping.get('IBC').add('OSM_Orderable_TEST__c');
        layoutMapping.get('IBC').add('OSM_KPI_Billed_Test_Date__c');
        //layoutMapping.get('IBC').add('OSM_ICD_System_TEST__c');
        //layoutMapping.get('IBC').add('OSM_ICD_Code_TEST__c');
        layoutMapping.get('IBC').add('OSM_KPI_Demand_by_Test_Date__c');
        layoutMapping.get('IBC').add('OSM_ER_Negative_Failure_Report_Override__c');
        layoutMapping.get('IBC').add('OSM_Verification_User__c');
        layoutMapping.get('IBC').add('OSM_Result_Current__c');
        layoutMapping.get('IBC').add('OSM_Failure_Code_Current__c');
        layoutMapping.get('IBC').add('OSM_Tumor_Grade__c');
        layoutMapping.get('IBC').add('OSM_Out_of_Criteria__c');
        layoutMapping.get('IBC').add('OSM_Chemotherapy__c');
        layoutMapping.get('IBC').add('OSM_FEC_Prescribed__c');
        layoutMapping.get('IBC').add('OSM_Date_of_Treatment_Decision__c');
        layoutMapping.get('IBC').add('Order.TotalAmount'); // Total Price
        layoutMapping.get('IBC').add('OSM_Test_Name__c');
        layoutMapping.get('IBC').add('OSM_Order_Line_Item_Status__c');
        layoutMapping.get('IBC').add('OSM_State__c');
        layoutMapping.get('IBC').add('OSM_Submitting_Diagnosis__c');
        layoutMapping.get('IBC').add('OSM_Nodal_Status__c');
        layoutMapping.get('IBC').add('OSM_Nodal_Status_Determination_Source__c');
        layoutMapping.get('IBC').add('OSM_Submitted_Gleason_Score__c');
        layoutMapping.get('IBC').add('OSM_GHI_Gleason_Score__c');
        layoutMapping.get('IBC').add('OSM_Her_2_Status__c');
        layoutMapping.get('IBC').add('OSM_Result_Disposition__c');
        layoutMapping.get('IBC').add('UnitPrice');
        layoutMapping.get('IBC').add('OSM_Is_Billing__c');
        layoutMapping.get('IBC').add('OSM_10_Yr_Survival_w_Chemo_Adjuvant__c');
        layoutMapping.get('IBC').add('OSM_10_Yr_Survival_w_Chemo_PREDICT__c');
        layoutMapping.get('IBC').add('OSM_Treatment_Rec_Prior_to_DX_Test__c');
        layoutMapping.get('IBC').add('OSM_Verification_User_Team__c');
        layoutMapping.get('IBC').add('OSM_Verification_User_Sub_Team__c');
        layoutMapping.get('IBC').add('OSM_Verification_Last_Modified__c');
        layoutMapping.get('IBC').add('OSM_Estimated_Report_Date__c');
        layoutMapping.get('IBC').add('OSM_OSpecimen_Report_Distribution_Ready__c');
        layoutMapping.get('IBC').add('OSM_Primary_Payor__C');
        layoutMapping.get('IBC').add('OSM_Primary_Payor_ID__c');
        layoutMapping.get('IBC').add('OSM_Plan__c');
        layoutMapping.get('IBC').add('OSM_Billing_Status__c');
        layoutMapping.get('IBC').add('OSM_Bill_Type__c');
        layoutMapping.get('IBC').add('OSM_Bill_Account__c');
        
        //Prostate
        layoutMapping.put('Prostate', new List<String>());
        layoutMapping.get('Prostate').add('OSM_NCCN_Risk_Category__c');
        layoutMapping.get('Prostate').add('OSM_Neoadjuvant_Treated__c');
        layoutMapping.get('Prostate').add('OSM_Positive_Cores__c');
        layoutMapping.get('Prostate').add('OSM_Prostate_Clinical_Stage__c');
        layoutMapping.get('Prostate').add('OSM_PSA__c');
        layoutMapping.get('Prostate').add('OSM_Submitted_Gleason_Score__c');
        
        layoutMapping.get('Prostate').add('OSM_Order_Line_Item_ID__c');
        layoutMapping.get('Prostate').add('OrderId'); // Order
        layoutMapping.get('Prostate').add('Pricebookentry.Product2Id'); // Product
        layoutMapping.get('Prostate').add('OSM_Product_Name__c');
        layoutMapping.get('Prostate').add('Pricebookentry.Product2.ProductCode');
        layoutMapping.get('Prostate').add('Quantity');
        layoutMapping.get('Prostate').add('OSM_ICD_Coding_System__c');
        layoutMapping.get('Prostate').add('OSM_ER_Status__c');
        layoutMapping.get('Prostate').add('OSM_Work_Order_ID__c');
        layoutMapping.get('Prostate').add('OSM_Diagnosis_Description__c');
        layoutMapping.get('Prostate').add('OSM_ICD_Code__c');
        layoutMapping.get('Prostate').add('OSM_Case__c');
        layoutMapping.get('Prostate').add('OSM_ABN_Required__c');
        layoutMapping.get('Prostate').add('OSM_Exception_Criteria__c');
        layoutMapping.get('Prostate').add('OSM_BI_Ready__c');
        layoutMapping.get('Prostate').add('OSM_Multiple_Primaries_Confirmed__c');
        layoutMapping.get('Prostate').add('OSM_Order_Created_On__c');
        layoutMapping.get('Prostate').add('OSM_First_Successful_Date_and_Time__c');
        layoutMapping.get('Prostate').add('OSM_Migration_ID_1__c');
        layoutMapping.get('Prostate').add('OSM_Specimen_Retrieval_Ready__c');
        layoutMapping.get('Prostate').add('OSM_Data_Entry_Status__c');
        layoutMapping.get('Prostate').add('OSM_Lab_and_Report_Status__c');
        layoutMapping.get('Prostate').add('OSM_Specimen_Retrieval_Option__c');
        layoutMapping.get('Prostate').add('OSM_Billing_Policy_Qualification_Status__c');
        layoutMapping.get('Prostate').add('OSM_Billing_Policy_Data_Status__c');
        layoutMapping.get('Prostate').add('OSM_Canceled_Reason__c');
        layoutMapping.get('Prostate').add('OSM_First_Specimen_Received_Date__c');
        layoutMapping.get('Prostate').add('OSM_Pre_Billing_Complete_Date_Current__c');
        layoutMapping.get('Prostate').add('OSM_Pre_Billing_Sent_Date_Current__c');
        layoutMapping.get('Prostate').add('OSM_Billing_Request_Sent_Date_Current__c');
        layoutMapping.get('Prostate').add('OSM_Billing_Void_Sent_Date_Current__c');
        layoutMapping.get('Prostate').add('Results_Disposition_Current__c');
        layoutMapping.get('Prostate').add('OSM_Billing_Policy_Bill_Entity__c');
        layoutMapping.get('Prostate').add('OSM_Special_Handling_Comments__c');
        layoutMapping.get('Prostate').add('OSM_Date_of_Service__c');
        layoutMapping.get('Prostate').add('OSM_Date_Distributed_Current__c');
        layoutMapping.get('Prostate').add('OSM_Resulted_Order_Specimen_ID_Current__c');
        layoutMapping.get('Prostate').add('OSM_Lab_Ready__c');
        layoutMapping.get('Prostate').add('OSM_Report_Distribution_Ready__c');
        layoutMapping.get('Prostate').add('OSM_Primary_Sequence_Number__c');
        layoutMapping.get('Prostate').add('OSM_OS_OLI_Billing_ready__c');
        layoutMapping.get('Prostate').add('OSM_Billing_policy_N_A__c');
        layoutMapping.get('Prostate').add('OSM_OLI_Data_Missing__c');
        layoutMapping.get('Prostate').add('OSM_Purchase_Order_Number__c');
        //layoutMapping.get('Prostate').add('OSM_Orderable_TEST__c');
        layoutMapping.get('Prostate').add('OSM_KPI_Billed_Test_Date__c');
        //layoutMapping.get('Prostate').add('OSM_ICD_System_TEST__c');
        //layoutMapping.get('Prostate').add('OSM_ICD_Code_TEST__c');
        layoutMapping.get('Prostate').add('OSM_KPI_Demand_by_Test_Date__c');
        layoutMapping.get('Prostate').add('OSM_ER_Negative_Failure_Report_Override__c');
        layoutMapping.get('Prostate').add('OSM_Verification_User__c');
        layoutMapping.get('Prostate').add('OSM_Result_Current__c');
        layoutMapping.get('Prostate').add('OSM_Failure_Code_Current__c');
        layoutMapping.get('Prostate').add('OSM_Out_of_Criteria__c');
        layoutMapping.get('Prostate').add('OSM_Chemotherapy__c');
        layoutMapping.get('Prostate').add('OSM_FEC_Prescribed__c');
        layoutMapping.get('Prostate').add('OSM_Date_of_Treatment_Decision__c');
        layoutMapping.get('Prostate').add('Order.TotalAmount'); // Total Price
        layoutMapping.get('Prostate').add('OSM_Test_Name__c');
        layoutMapping.get('Prostate').add('OSM_Order_Line_Item_Status__c');
        layoutMapping.get('Prostate').add('OSM_State__c');
        layoutMapping.get('Prostate').add('OSM_Submitting_Diagnosis__c');
        layoutMapping.get('Prostate').add('OSM_Nodal_Status__c');
        layoutMapping.get('Prostate').add('OSM_Nodal_Status_Determination_Source__c');
        layoutMapping.get('Prostate').add('OSM_GHI_Gleason_Score__c');
        layoutMapping.get('Prostate').add('OSM_Result_Disposition__c');
        layoutMapping.get('Prostate').add('UnitPrice');
        layoutMapping.get('Prostate').add('OSM_Is_Billing__c');
        layoutMapping.get('Prostate').add('OSM_10_Yr_Survival_w_Chemo_Adjuvant__c');
        layoutMapping.get('Prostate').add('OSM_10_Yr_Survival_w_Chemo_PREDICT__c');
        layoutMapping.get('Prostate').add('OSM_Treatment_Rec_Prior_to_DX_Test__c');
        layoutMapping.get('Prostate').add('OSM_Verification_User_Team__c');
        layoutMapping.get('Prostate').add('OSM_Verification_User_Sub_Team__c');
        layoutMapping.get('Prostate').add('OSM_Verification_Last_Modified__c');
        layoutMapping.get('Prostate').add('OSM_Estimated_Report_Date__c');
        layoutMapping.get('Prostate').add('OSM_OSpecimen_Report_Distribution_Ready__c');
        layoutMapping.get('Prostate').add('OSM_Primary_Payor__C');
        layoutMapping.get('Prostate').add('OSM_Primary_Payor_ID__c');
        layoutMapping.get('Prostate').add('OSM_Plan__c');
        layoutMapping.get('Prostate').add('OSM_Billing_Status__c');
        layoutMapping.get('Prostate').add('OSM_Bill_Type__c');
        layoutMapping.get('Prostate').add('OSM_Bill_Account__c');
        
        layoutMapping.put('Order Line Layout', new List<String>());
        layoutMapping.get('Order Line Layout').add('OSM_Order_Line_Item_ID__c');
        layoutMapping.get('Order Line Layout').add('OrderId'); // Order
        layoutMapping.get('Order Line Layout').add('Pricebookentry.Product2Id'); // Product
        layoutMapping.get('Order Line Layout').add('OSM_Product_Name__c');
        layoutMapping.get('Order Line Layout').add('Pricebookentry.Product2.ProductCode');
        layoutMapping.get('Order Line Layout').add('Quantity');
        layoutMapping.get('Order Line Layout').add('OSM_ICD_Coding_System__c');
        layoutMapping.get('Order Line Layout').add('OSM_ER_Status__c');
        layoutMapping.get('Order Line Layout').add('OSM_NCCN_Risk_Category__c');
        layoutMapping.get('Order Line Layout').add('OSM_PSA__c');
        layoutMapping.get('Order Line Layout').add('OSM_Positive_Cores__c');
        layoutMapping.get('Order Line Layout').add('OSM_Work_Order_ID__c');
        layoutMapping.get('Order Line Layout').add('OSM_Diagnosis_Description__c');
        layoutMapping.get('Order Line Layout').add('OSM_ICD_Code__c');
        layoutMapping.get('Order Line Layout').add('OSM_Case__c');
        layoutMapping.get('Order Line Layout').add('OSM_ABN_Required__c');
        layoutMapping.get('Order Line Layout').add('OSM_Exception_Criteria__c');
        layoutMapping.get('Order Line Layout').add('OSM_BI_Ready__c');
        layoutMapping.get('Order Line Layout').add('OSM_Multiple_Primaries_Confirmed__c');
        layoutMapping.get('Order Line Layout').add('OSM_Order_Created_On__c');
        layoutMapping.get('Order Line Layout').add('OSM_First_Successful_Date_and_Time__c');
        layoutMapping.get('Order Line Layout').add('OSM_Migration_ID_1__c');
        layoutMapping.get('Order Line Layout').add('OSM_Specimen_Retrieval_Ready__c');
        layoutMapping.get('Order Line Layout').add('OSM_Data_Entry_Status__c');
        layoutMapping.get('Order Line Layout').add('OSM_Lab_and_Report_Status__c');
        layoutMapping.get('Order Line Layout').add('OSM_Specimen_Retrieval_Option__c');
        layoutMapping.get('Order Line Layout').add('OSM_Billing_Policy_Qualification_Status__c');
        layoutMapping.get('Order Line Layout').add('OSM_Billing_Policy_Data_Status__c');
        layoutMapping.get('Order Line Layout').add('OSM_Canceled_Reason__c');
        layoutMapping.get('Order Line Layout').add('OSM_First_Specimen_Received_Date__c');
        layoutMapping.get('Order Line Layout').add('OSM_Pre_Billing_Complete_Date_Current__c');
        layoutMapping.get('Order Line Layout').add('OSM_Pre_Billing_Sent_Date_Current__c');
        layoutMapping.get('Order Line Layout').add('OSM_Billing_Request_Sent_Date_Current__c');
        layoutMapping.get('Order Line Layout').add('OSM_Billing_Void_Sent_Date_Current__c');
        layoutMapping.get('Order Line Layout').add('Results_Disposition_Current__c');
        layoutMapping.get('Order Line Layout').add('OSM_Billing_Policy_Bill_Entity__c');
        layoutMapping.get('Order Line Layout').add('OSM_Special_Handling_Comments__c');
        layoutMapping.get('Order Line Layout').add('OSM_Date_of_Service__c');
        layoutMapping.get('Order Line Layout').add('OSM_Date_Distributed_Current__c');
        layoutMapping.get('Order Line Layout').add('OSM_Resulted_Order_Specimen_ID_Current__c');
        layoutMapping.get('Order Line Layout').add('OSM_Lab_Ready__c');
        layoutMapping.get('Order Line Layout').add('OSM_Report_Distribution_Ready__c');
        layoutMapping.get('Order Line Layout').add('OSM_Primary_Sequence_Number__c');
        layoutMapping.get('Order Line Layout').add('OSM_OS_OLI_Billing_ready__c');
        layoutMapping.get('Order Line Layout').add('OSM_Billing_policy_N_A__c');
        layoutMapping.get('Order Line Layout').add('OSM_OLI_Data_Missing__c');
        layoutMapping.get('Order Line Layout').add('OSM_Purchase_Order_Number__c');
        //layoutMapping.get('Order Line Layout').add('OSM_Orderable_TEST__c');
        layoutMapping.get('Order Line Layout').add('OSM_KPI_Billed_Test_Date__c');
        //layoutMapping.get('Order Line Layout').add('OSM_ICD_System_TEST__c');
        //layoutMapping.get('Order Line Layout').add('OSM_ICD_Code_TEST__c');
        layoutMapping.get('Order Line Layout').add('OSM_KPI_Demand_by_Test_Date__c');
        layoutMapping.get('Order Line Layout').add('OSM_ER_Negative_Failure_Report_Override__c');
        layoutMapping.get('Order Line Layout').add('OSM_Verification_User__c');
        layoutMapping.get('Order Line Layout').add('OSM_Result_Current__c');
        layoutMapping.get('Order Line Layout').add('OSM_Failure_Code_Current__c');
        layoutMapping.get('Order Line Layout').add('OSM_Out_of_Criteria__c');
        layoutMapping.get('Order Line Layout').add('OSM_Tumor_Grade__c');
        layoutMapping.get('Order Line Layout').add('OSM_Tumor_Size__c');
        layoutMapping.get('Order Line Layout').add('OSM_Chemotherapy__c');
        layoutMapping.get('Order Line Layout').add('OSM_FEC_Prescribed__c');
        layoutMapping.get('Order Line Layout').add('OSM_Date_of_Treatment_Decision__c');
        layoutMapping.get('Order Line Layout').add('Order.TotalAmount'); // Total Price
        layoutMapping.get('Order Line Layout').add('OSM_Test_Name__c');
        layoutMapping.get('Order Line Layout').add('OSM_Order_Line_Item_Status__c');
        layoutMapping.get('Order Line Layout').add('OSM_State__c');
        layoutMapping.get('Order Line Layout').add('OSM_Submitting_Diagnosis__c');
        layoutMapping.get('Order Line Layout').add('OSM_Nodal_Status__c');
        layoutMapping.get('Order Line Layout').add('OSM_Nodal_Status_Determination_Source__c');
        layoutMapping.get('Order Line Layout').add('OSM_Submitted_Gleason_Score__c');
        layoutMapping.get('Order Line Layout').add('OSM_GHI_Gleason_Score__c');
        layoutMapping.get('Order Line Layout').add('OSM_Result_Disposition__c');
        layoutMapping.get('Order Line Layout').add('UnitPrice');
        layoutMapping.get('Order Line Layout').add('OSM_Is_Billing__c');
        layoutMapping.get('Order Line Layout').add('OSM_10_Yr_Survival_w_Chemo_Adjuvant__c');
        layoutMapping.get('Order Line Layout').add('OSM_10_Yr_Survival_w_Chemo_PREDICT__c');
        layoutMapping.get('Order Line Layout').add('OSM_Treatment_Rec_Prior_to_DX_Test__c');
        layoutMapping.get('Order Line Layout').add('OSM_Verification_User_Team__c');
        layoutMapping.get('Order Line Layout').add('OSM_Verification_User_Sub_Team__c');
        layoutMapping.get('Order Line Layout').add('OSM_Verification_Last_Modified__c');
        layoutMapping.get('Order Line Layout').add('OSM_Estimated_Report_Date__c');
        layoutMapping.get('Order Line Layout').add('OSM_Primary_Payor__C');
        layoutMapping.get('Order Line Layout').add('OSM_Primary_Payor_ID__c');
        layoutMapping.get('Order Line Layout').add('OSM_Plan__c');
        layoutMapping.get('Order Line Layout').add('OSM_Billing_Status__c');
        layoutMapping.get('Order Line Layout').add('OSM_Bill_Type__c');
        layoutMapping.get('Order Line Layout').add('OSM_Bill_Account__c');
        
        //Order Line Item
        getFields(oliId);
        //getPageLayout(this.oliDetails, null, null);
    }
    
    public PageReference editRecord(){
        isEdit = true;
        return new PageReference('/' + this.oliId);
    }
    
    public PageReference saveRecord(){
        update oliDetails;
        getFields(oliDetails.Id);
        isEdit = false;
        return new PageReference('/' + oliDetails.Id);
    }
    
    public PageReference deleteRecord(){
        Id orderId = oliDetails.OrderId;
        delete oliDetails;
        return new PageReference('/' + orderId);
    }
    
    public PageReference orderUpdate(){
        return new PageReference('/' + oliDetails.OrderId);
    }
    
    public PageReference orderUpdateQDX(){
        OSM_Messaging.MakeOrderXML(oliDetails.OrderId, 'OSM_OrderUpdateToQDX', '');
        return new PageReference('/' + oliDetails.OrderId);
    }
    
    public PageReference orderCancelQDX(){
        OSM_Messaging.MakeOrderXML(oliDetails.OrderId, 'OSM_CancelledOrderToQDX', '');
        return new PageReference('/' + oliDetails.OrderId);
    }
    
    public PageReference orderFailureQDX(){
        OSM_Messaging.MakeOrderXML(oliDetails.OrderId, 'OSM_OrderFailureToQDX', '');
        return new PageReference('/' + oliDetails.OrderId);
    }
    
    public PageReference renderDistributeDataMessage(){
        OSM_Messaging.MakeOrderXML(oliDetails.OrderId, 'OSM_OrderDataMessage', '');
        return new PageReference('/' + oliDetails.OrderId);
    }
    
    public void cancelRecord(){
        Id oliId = ApexPages.currentPage().getParameters().get('id');
        String oliQuery = 'SELECT Id,Pricebookentry.Product2.OLI_Page_Layout__c' + this.getOLIFields('OrderItem') + ' FROM OrderItem WHERE Id = :oliId';
        if(!test.isrunningtest())
        {
            this.oliDetails = Database.query(oliQuery);
        }
        isEdit = false;
    }
    
    /*
    public void getPageLayout(OrderItem oli, String layout, Id recordId){
        try{
            DescribeLayoutResult oliLayout = Schema.describeLayout(oli, layout, recordId);
            System.debug('all oli layout : ' + oliLayout);
        }catch(ConnectionException ce){
        }
    }
    */
    
    public void getFields(String oliId){
        String oliQuery = 'SELECT Id,Pricebookentry.Product2.OLI_Page_Layout__c' + this.getOLIFields('OrderItem') + ' FROM OrderItem WHERE Id = :oliId';
        
        this.oliDetails = Database.query(oliQuery);
    }
    
    public String getOLIFields(String objName){
        Map <String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        List <String> oliFields = new List<String>();
        String fields = '';
        
        if(productLayout != null){
            fields += ',';
            if(hardcodeList){ // hardcoded fields
                for(String obj : layoutMapping.get(productLayout)){
                    fields += ' ' + obj + ',';
                    oliFields.add(obj);
                }
            }
            else{ // dynamic fields
                for(String obj : objectFields.keySet()) {
                    fields += ' ' + obj + ',';
                    oliFields.add((String)obj);
                }
            }
        
        
            //this.oliFieldNames = oliFields;
            
            if (fields.subString(fields.Length()-1,fields.Length()) == ','){
                fields = fields.subString(0,fields.Length()-1);
            }
        }
        return fields;
    }
}