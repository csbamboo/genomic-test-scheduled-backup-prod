/*
  @author: Amanpreet Singh Sidhu 
  @date: 28 June 2015
  @description: Test Class for OSM_OrderLineItemPageLayoutExt apex class
  @history: 28 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest(seeAllData = false)
public class OSM_OrderLineItemPageLayoutExt_Test 
{
  
    static testMethod void orderItem() 
    {
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = true;
        triggerSwitch.Work_Order_Trigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        //Id caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        
        Set<Id> setOrder = new Set<Id>();
        //List<Case> lstCase = new List<Case>();
        List<OrderItem> lstOrderItem = new List<OrderItem>();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;
        
        Id pricebookStandard = Test.getStandardPricebookId();
        
        Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        order1.PriceBook2Id = pricebookStandard;
        insert order1;
        
        setOrder.add(order1.Id);
                       
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(order1.id);
        insert workOrder;
        
        Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
        insert pbe;
        
        OrderItem ordItem = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , 
                                         UnitPrice = 2 , Quantity = 1, OSM_Lab_and_Report_Status__c = 'Pre-Processing', OSM_Order_Line_Item_Status__c = 'Processing');                
        insert ordItem ;
        
        PageReference pageRef = Page.OSM_OrderLineItemPageLayout;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ordItem);
        ApexPages.currentPage().getParameters().put('Id',ordItem.id);
        OSM_OrderLineItemPageLayoutExt cont = new OSM_OrderLineItemPageLayoutExt(sc);     
                
        Test.startTest();
        
        cont.editRecord();
        cont.saveRecord();
        cont.deleteRecord();
        cont.orderUpdate();
        cont.orderUpdateQDX();
        cont.orderCancelQDX();
        cont.orderFailureQDX();
        cont.renderDistributeDataMessage();
        cont.cancelRecord();
                   
        Test.stopTest();
    
    }
}