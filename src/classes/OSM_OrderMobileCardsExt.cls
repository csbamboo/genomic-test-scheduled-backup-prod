/*
        @author: Michiel Patricia M. Robrigado 
        @date: 30.June.2015
        @description: VF Page for order mobile cards
        
    */
public class OSM_OrderMobileCardsExt {

    Order ord;
    public OrderItem fieldSetsDetails {get;set;}
    public List<OrderItem> oliList {get;set;}
    
    //Standard controller
    public OSM_OrderMobileCardsExt(ApexPages.StandardController stdController){
        ord = (Order)stdController.getRecord();
        Id ordId = ApexPages.currentPage().getParameters().get('Id');
        
        oliList = [SELECT Id, OrderId, PricebookEntry.Product2.Name, PricebookEntry.Product2.Id, OSM_First_Specimen_Received_Date__c, OSM_Submission_Status__c, OSM_OLI_Start_Date__c, OSM_Submitting_Diagnosis__c,
                OSM_Exception_Criteria__c, OSM_ER_Status__c, OSM_Nodal_Status__c, OSM_NCCN_Risk_Category__c, GHI_Portal_Estimated_Report_Date__c, OSM_Report_Date__c, OSM_Order_Line_Item_ID__c,
                OSM_Submitted_Gleason_Score__c, OSM_Colon_Clinical_Stage__c, OSM_Positive_Cores__c, OSM_Billing_Status__c, OSM_State__c, OSM_Order_Line_Item_Status__c,
                OSM_DCIS_Clinical_Stage__c, OSM_IBC_Clinical_Stage__c, OSM_Prostate_Clinical_Stage__c FROM OrderItem WHERE OrderId =: ord.Id];
                                
    }
}