/*
 * @author         Rulleth Decena
 * @description    Test class for OSM_OrderMobileCardsExt class. 
 * @history        04.AUG.2015 - Rulleth Decena - Created  
 */
@isTest(seeAllData = true)
private class OSM_OrderMobileCardsExt_Test {
    /*
     * @author         Rulleth Decena
     * @description    Test the toggling of OrderMobileCardsExt
     * @history        04.AUG.2015 - Rulleth Decena - Created   
     */
    @isTest static void testOrderMobileCardsExt() {
    
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs(thisUser){
        //prepare custom settings
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Case_Trigger__c = true;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.GHI_Portal_UserTrigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        update triggerSwitch;
        //prepare record type ids
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id genId = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('General').getRecordTypeId();

        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', (Id) HCORecType) ;
        insert acct;

        User customer = OSM_DataFactory.createCustomerServiceUser('customer');
        insert customer;

        Contact pat = OSM_DataFactory.createContact(1,acct.ID, (Id)PatientRecType);
        pat.OSM_Status__c = 'Draft';
        insert pat;

        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];

        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;

        Order ord = OSM_DataFactory.createOrder('ordName',pat.Id, acct.ID, System.Today(), 'Active');
            ord.Pricebook2Id = pb2Standard.Id;
            ord.OSM_Bill_Account__c = acct.Id;
            ord.OSM_Bill_Type__c = 'Bill Account';
        insert ord;

        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard.Id, 7);
        insert pbe;

        Date oiDate = System.Today();
        Date newOIDate = oiDate.addDays(2);

        List<OrderItem> ordItemL = new List<OrderItem>();
        for(Integer b=0; b<10; b++){
            ordItemL.add(OSM_DataFactory.createOrderItem(ord.Id, pbe.Id, 2, 20, newOIDate));
        }

        Test.startTest();

        //System.runAs(customer){

            insert ordItemL;

            PageReference pageRef = Page.OSM_NewAddressAffiliation;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ord);
            //ApexPages.currentPage().getParameters().put('Id',ordItemL.Id);
            OSM_OrderMobileCardsExt stdCtlr = new OSM_OrderMobileCardsExt(sc);
            stdCtlr.fieldSetsDetails = ordItemL[0];
            //System.assertEquals(10, ordItemL.size());
        //}

        Test.stopTest();
        
        }
    }
        
}