public class OSM_OrderOverridedNewEditExtension{

    public String customLink {get;set;}
    public OSM_OrderOverridedNewEditExtension (ApexPages.StandardController controller){
        Order order = (Order) controller.getRecord();
        customLink = 'Custom Link';
        if(order.ID == null){
            order.AccountId = ApexPages.CurrentPage().getparameters().get('accid_lkid');
            
            order.EffectiveDate = Date.valueOf(ApexPages.CurrentPage().getparameters().get('EffectiveDate'));
        }
        
    }
}