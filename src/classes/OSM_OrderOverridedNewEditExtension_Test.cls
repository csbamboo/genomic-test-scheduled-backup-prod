/*
      @author: David Catindoy
      @date: 23 JAN 2015 - Created
      @description: Study Trigger Handler
      @history: 23 JAN 2015 - Updated (David E. Catindoy)

*/
@isTest
public class OSM_OrderOverridedNewEditExtension_Test{
    
    static testmethod void StudyTestMethod(){
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        insert order;
        
        Boolean error = false;

        Date effDate = System.Today();
        String effDateStr = String.valueOf(effDate);

        Test.startTest();
        try{
        
        PageReference pageRef = new PageReference('/' + order.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController standardController = new ApexPages.StandardController(order);
        OSM_OrderOverridedNewEditExtension controller = new OSM_OrderOverridedNewEditExtension(standardController);
        ApexPages.currentPage().getParameters().put('accid_lkid',account.Id);
        ApexPages.currentPage().getParameters().put('EffectiveDate',effDateStr);
        } catch(Exception e){
            error = true;
        }
        Test.stopTest();
        System.assert(!error);
    }

        static testmethod void StudyTestMethodNoOrder(){
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        insert order;
        
        Boolean error = false;

        Date effDate = System.Today();
        String effDateStr = String.valueOf(effDate);

        Test.startTest();
        try{
        order.Id = null;
        PageReference pageRef = new PageReference('/' + order.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController standardController = new ApexPages.StandardController(order);
        OSM_OrderOverridedNewEditExtension controller = new OSM_OrderOverridedNewEditExtension(standardController);
        ApexPages.currentPage().getParameters().put('accid_lkid',account.Id);
        ApexPages.currentPage().getParameters().put('EffectiveDate',effDateStr);
        } catch(Exception e){
            error = true;
        }
        Test.stopTest();
        System.assert(error);
    }
    
       
}