@isTest
private class OSM_OrderPayLoadComponentController_test {

    private static testMethod void test1() {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Case_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Role_Trigger__c = false;
       
        insert triggerSwitch;

        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id Payor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        String recordTypeIds = String.valueOf(Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('HCO Address Affiliation').getRecordTypeId()).substring(0,15);
        Id orderRoleRecType = Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Account & Contact').getRecordTypeId();
        //Account
        Account acc1 = OSM_DataFactory.createAccount('Test');
        insert acc1;
    
        Account acc2 = OSM_DataFactory.createAccount('New Account 1');
        acc2.RecordTypeId = HCORecType;
        insert acc2;
        
        Account acc3 = OSM_DataFactory.createAccount('Payor');
        acc3.RecordTypeId = Payor;
        insert acc3;
        
        //Contact
        Contact con1 = OSM_DataFactory.createContact(123, acc1.Id, Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        con1.OSM_Status__c = 'Draft';
        insert con1;
        Contact con2 = OSM_DataFactory.createContact(2, acc2.Id, (Id) HCPRecType);
        con2.OSM_Status__c = 'Draft';
        insert con2;
       
        //Address
        OSM_Address__c newAddress = OSM_DataFactory.createAddress('Sample', 'Result', 'Manila', 'England', null, '1226');
        insert newAddress;
        
        //Address Affiliation
        OSM_Address_Affiliation__c addAffil = OSM_DataFactory.createAddressAffiliation(acc2.Id, newAddress.Id, 'Test Attention', true, 'testEmail@test.com', 'Test Fax', null, con2.Id, true, 'Bill To', recordTypeIds);
        addAffil.OSM_Phone_Number__c = '1231234';
        insert addAffil;
        
        //Study
        OSM_Study__c study1= new OSM_Study__c(Name = 'New Study', OSM_Bill_To__c = 'Sponsor', OSM_Sponsor_HCO__c = acc2.Id);
        insert study1;
        
        //Order
        Order ord1 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'MMR and Colon', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord1;
        
        
        Order ord2 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'Breast', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord2;
        
        Order ord3 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'DCIS', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord3;
        
        Order ord4 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'Prostate', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord4;
        
        Order ord5 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'MMR', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord5;
        
        
        
        
        //Territory
         OSM_Territory__c parentTerritory = OSM_DataFactory.createTerritory('Parent Territory');
        insert parentTerritory;
        
        //Account Territory Assignment
        OSM_Account_Territory_Assignment__c ata = OSM_DataFactory.createATA(acc2.Id, parentTerritory.Id);
        ata.OSM_Effective_Start_Date__c = System.Today();
        ata.OSM_Effective_Ending_Date__c = System.Today();
        insert ata;
        
        OSM_Account_Territory_Assignment__c ata2 = OSM_DataFactory.createATA(acc3.Id, parentTerritory.Id);
        ata2.OSM_Effective_Start_Date__c = System.Today();
        ata2.OSM_Effective_Ending_Date__c = System.Today();
        insert ata2;
        
        //Orderable
        OSM_Orderable__c orderable1 = new OSM_Orderable__c(OSM_Active__c = true, OSM_CRM_Name__c = 'MMR and Colon', OSM_Long_Name__c = 'MMR and Colon', OSM_Short_Name__c = 'MMR and Colon' );
        insert orderable1;
        
        //Order Specimen
        OSM_Order_Specimen__c ordSpecimen = new OSM_Order_Specimen__c(Name = 'Wasd', OSM_Incoming_Tracking_Number__c = '1234', OSM_Order__c = ord1.Id, OSM_Date_of_Collection__c = System.Today(), OSM_Available_for_Processing__c = true, OSM_First_Available_Date__c = System.Today());
        insert ordSpecimen;
        
        //Product
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        
        //PricebookEntry
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, Test.getStandardPricebookId(), 7);
        insert pbe;
        
        //Result
        OSM_Result__c result1 = new OSM_Result__c();
        result1.OSM_Results_Approval_Date__c = System.Today();
        result1.OSM_Resulted_Order_Specimen_ID__c = ordSpecimen.Id;
        result1.OSM_Result_Disposition__c = 'Results';
        result1.CurrencyIsoCode = 'USD';
        result1.OSM_Distributed__c = true;
        result1.OSM_Current__c = true;
        result1.OSM_Result_Disposition__c = 'Result';
        result1.OSM_First_Distribution_Date__c = System.Today();
        insert result1;
        
        //order item
        OrderItem ordItem = new OrderItem(OrderId = ord1.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1);
        ordItem.OSM_Lab_And_Report_Status__c = 'Distributing';
        ordItem.OSM_ICD_Coding_System__c = 'ICD-9 - Colon/MMR';
        ordItem.OSM_ICD_Code__c = '174.3,Malignant neoplasm of lower-inner quadrant of female breast,C50.319';
        ordItem.OSM_ER_Status__c = 'Positive';
        ordItem.OSM_Nodal_Status__c = 'Node Negative';
        ordItem.OSM_Nodal_Status_Determination_Source__c = 'Phone Call';
        ordItem.OSM_Her_2_Status__c = 'Positive';
        ordItem.OSM_IBC_Clinical_Stage__c = 'IV';
        ordItem.OSM_DCIS_Clinical_Stage__c = 'T1mi, N0, M0 (microinvasive)';
        ordItem.OSM_Colon_Clinical_Stage__c = 'Stage III A/B';
        ordItem.OSM_Prostate_Clinical_Stage__c = 'T1/T2';
        ordItem.OSM_Submitted_Gleason_Score__c = '4+3';
        ordItem.OSM_Positive_Cores__c = '>=4 and Gleason >= 3+4';
        ordItem.OSM_PSA__c = '<= 20 ng/mL';
        ordItem.OSM_Out_of_Criteria__c = 'test';
        ordItem.OSM_T4__c = 'Yes';
        ordItem.OSM_MMR_D__c = 'Yes';
        ordItem.OSM_NCCN_Risk_Category__c = 'Intermediate Risk';
        ordItem.OSM_Result_Current__c = result1.Id;
        ordItem.OSM_Bill_Type__c = 'Bill Account';
        ordItem.OSM_Bill_Account__c = acc2.Id;
        ordItem.OSM_Primary_Payor__c = acc3.Id;
        insert ordItem;
        OrderItem ordItem2 = [SELECT Id, OSM_ICD_Coding_System_Formula__c FROM OrderItem WHERE Id =: ordItem.Id];
        //System.assertEquals('Object expected', ordItem2.OSM_ICD_Coding_System_Formula__c);
        
        //OrderSpecimenOLI
        OSM_Order_Specimen_OLI__c ordSpecimenOLI = new OSM_Order_Specimen_OLI__c(OSM_Order_Specimen_ID__c = ordSpecimen.Id, OSM_Billing_policy_N_A__c = false, OSM_OLI_ID__c = ordItem.Id);
        insert ordSpecimenOLI;
        
        //Work Order
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(ord1.Id);
        insert workOrder;
        
        //Work Order Line Item
        OSM_Work_Order_Line_Item__c workOrderLineItem = OSM_DataFactory.createWorkOrderLineItem(workOrder.Id, ordItem.Id);
        insert workOrderLineItem;
        
        //Order Territory Assignment
        OSM_Order_Territory_Assignment__c otAssignment = new OSM_Order_Territory_Assignment__c(OSM_Territory_Name__c = parentTerritory.Id, OSM_OrderName__c = ord1.Id, OSM_Order__c = ord1.Id, OSM_Effective_Start_Date__c = System.Today(), OSM_Effective_Ending_Date__c = System.Today());
        insert otAssignment;
        
        //Order Role
        OSM_Order_Role__c ordRole1 = OSM_DataFactory.createOrderRole(ord1.Id, acc2.Id, null, 'Ordering',orderRoleRecType);
        insert ordRole1;
        
        //HCP Territory Assignment
        OSM_HCP_Territory_Assignment__c hta = OSM_DataFactory.createHTA(con2.Id, parentTerritory.Id);
        insert hta;
        
        test.startTest();
        OSM_OrderPayLoadComponentController orderPayLoadController = new OSM_OrderPayLoadComponentController((String)ord1.Id, 'true');
        OSM_OrderPayLoadComponentController orderPayLoadController2 = new OSM_OrderPayLoadComponentController((String)ordItem.Id, 'true');
        OSM_OrderPayLoadComponentController orderPayLoadController3 = new OSM_OrderPayLoadComponentController((String)ord2.Id, 'true');
        // OSM_OrderPayLoadComponentController orderPayLoadController4 = new OSM_OrderPayLoadComponentController((String)ord3.Id, 'true');
        // OSM_OrderPayLoadComponentController orderPayLoadController5 = new OSM_OrderPayLoadComponentController((String)ord4.Id, 'true');
        // OSM_OrderPayLoadComponentController orderPayLoadController6 = new OSM_OrderPayLoadComponentController((String)ord5.Id, 'true');
        // OSM_OrderPayLoadComponentController orderPayLoadController7 = new OSM_OrderPayLoadComponentController((String)workOrder.Id, 'false');
        // OSM_OrderPayLoadComponentController.ataWrapper a = new OSM_OrderPayLoadComponentController.atawrapper(ata, '123','123','123','123');
        //OSM_OrderPayLoadComponentController.ordbleWrapper b = new OSM_OrderPayLoadComponentController.ordbleWrapper(orderable1,'123','123');
        OSM_OrderPayLoadComponentController.orderSpecimenWrapper c = new OSM_OrderPayLoadComponentController.orderSpecimenWrapper(ordSpecimen, '123', '123');
        OSM_OrderPayLoadComponentController.testCriteriaGroupWrapper d = new OSM_OrderPayLoadComponentController.testCriteriaGroupWrapper('asd','asd');
        OSM_OrderPayLoadComponentController.sponsorHCOWrapper e = new OSM_OrderPayLoadComponentController.sponsorHCOWrapper(acc2,'123','123');
        OSM_OrderPayLoadComponentController.oliWrapper f = new OSM_OrderPayLoadComponentController.oliWrapper(ordItem, '123', '123');
        OSM_OrderPayLoadComponentController.woliWrapper g =new OSM_OrderPayLoadComponentController.woliWrapper(workOrderLineItem, '123', '123');
        //orderPayLoadController.getStudyInformation(ord1.Id);
        //OSM_OrderPayLoadComponentController.getStudyInformation((ord1.Id)ord1.Id);;
        test.stopTest();
        
    }
    
        private static testMethod void test2() {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Case_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Role_Trigger__c = false;
        insert triggerSwitch;

        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id Payor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        String recordTypeIds = String.valueOf(Schema.SObjectType.OSM_Address_Affiliation__c.getRecordTypeInfosByName().get('HCO Address Affiliation').getRecordTypeId()).substring(0,15);
        Id orderRoleRecType = Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Account & Contact').getRecordTypeId();
        //Account
        Account acc1 = OSM_DataFactory.createAccount('Test');
        insert acc1;
    
        Account acc2 = OSM_DataFactory.createAccount('New Account 1');
        acc2.RecordTypeId = HCORecType;
        insert acc2;
        
        Account acc3 = OSM_DataFactory.createAccount('Payor');
        acc3.RecordTypeId = Payor;
        insert acc3;
        
        //Contact
        Contact con1 = OSM_DataFactory.createContact(123, acc1.Id, Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        con1.OSM_Status__c = 'Draft';
        insert con1;
        
        Contact con2 = OSM_DataFactory.createContact(2, acc2.Id, (Id) HCPRecType);
        con2.OSM_Status__c = 'Draft';
        insert con2;
       
        //Address
        OSM_Address__c newAddress = OSM_DataFactory.createAddress('Sample', 'Result', 'Manila', 'England', null, '1226');
        insert newAddress;
        
        //Address Affiliation
        OSM_Address_Affiliation__c addAffil = OSM_DataFactory.createAddressAffiliation(acc2.Id, newAddress.Id, 'Test Attention', true, 'testEmail@test.com', 'Test Fax', null, con2.Id, true, 'Bill To', recordTypeIds);
        addAffil.OSM_Phone_Number__c = '1231234';
        insert addAffil;
        
       // OSM_Address_Affiliation__c addAffil2 = OSM_DataFactory.createAddressAffiliation(acc2.Id, newAddress.Id, 'Test Attention', true, 'testEmail234@test.com', 'Test Fax', null, con1.Id, true, 'Main', recordTypeIds);
       // addAffil2.OSM_Phone_Number__c = '1231234';
       // insert addAffil2;
        
        //Study
        OSM_Study__c study1= new OSM_Study__c(Name = 'New Study', OSM_Bill_To__c = 'Sponsor', OSM_Sponsor_HCO__c = acc2.Id);
        insert study1;
        
        //Order
        Order ord1 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc2.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'MMR and Colon', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord1;
        
        
        Order ord2 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'Breast', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord2;
        
        Order ord3 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'DCIS', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord3;
        
        Order ord4 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'Prostate', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord4;
        
        Order ord5 =  new Order(OSM_Patient__c = con1.Id, 
        AccountId = acc1.Id,
        Pricebook2Id = Test.getStandardPricebookId(), 
        EffectiveDate = System.Today(), 
        Status = 'New', 
        Order_Location__c = 'International', 
        OSM_Triage_Outcome__c = 'Resubmission - Confirmed', 
        OSM_Status__c = 'Order Intake', 
        OSM_Product__c = 'MMR', 
        OSM_Work_Order_Status__c = 'Open', 
        OSM_State__c = 'Closed',
        OSM_Study_Name__c = study1.Id);
        insert ord5;
        
        
        
        
        //Territory
         OSM_Territory__c parentTerritory = OSM_DataFactory.createTerritory('Parent Territory');
        insert parentTerritory;
        
        //Account Territory Assignment
        OSM_Account_Territory_Assignment__c ata = OSM_DataFactory.createATA(acc2.Id, parentTerritory.Id);
        ata.OSM_Effective_Start_Date__c = System.Today();
        ata.OSM_Effective_Ending_Date__c = System.Today();
        insert ata;
        
        OSM_Account_Territory_Assignment__c ata2 = OSM_DataFactory.createATA(acc3.Id, parentTerritory.Id);
        ata2.OSM_Effective_Start_Date__c = System.Today();
        ata2.OSM_Effective_Ending_Date__c = System.Today();
        insert ata2;
        
        //Orderable
        OSM_Orderable__c orderable1 = new OSM_Orderable__c(OSM_Active__c = true, OSM_CRM_Name__c = 'MMR and Colon', OSM_Long_Name__c = 'MMR and Colon', OSM_Short_Name__c = 'MMR and Colon' );
        insert orderable1;
        
        //Order Specimen
        OSM_Order_Specimen__c ordSpecimen = new OSM_Order_Specimen__c(Name = 'Wasd', OSM_Incoming_Tracking_Number__c = '1234', OSM_Order__c = ord1.Id, OSM_Date_of_Collection__c = System.Today(), OSM_Available_for_Processing__c = true, OSM_First_Available_Date__c = System.Today());
        insert ordSpecimen;
        
        //Product
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        
        //PricebookEntry
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, Test.getStandardPricebookId(), 7);
        insert pbe;
        
        //Result
        OSM_Result__c result1 = new OSM_Result__c();
        result1.OSM_Results_Approval_Date__c = System.Today();
        result1.OSM_Resulted_Order_Specimen_ID__c = ordSpecimen.Id;
        result1.OSM_Result_Disposition__c = 'Results';
        result1.CurrencyIsoCode = 'USD';
        result1.OSM_Distributed__c = true;
        result1.OSM_Current__c = true;
        result1.OSM_Result_Disposition__c = 'Result';
        result1.OSM_First_Distribution_Date__c = System.Today();
        insert result1;
        
        //order item
        OrderItem ordItem = new OrderItem(OrderId = ord1.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1);
        ordItem.OSM_Lab_And_Report_Status__c = 'Distributing';
        ordItem.OSM_ICD_Coding_System__c = 'ICD-9 - Colon/MMR';
        ordItem.OSM_ICD_Code__c = '174.3,Malignant neoplasm of lower-inner quadrant of female breast,C50.319';
        ordItem.OSM_ER_Status__c = 'Positive';
        ordItem.OSM_Nodal_Status__c = 'Node Negative';
        ordItem.OSM_Nodal_Status_Determination_Source__c = 'Phone Call';
        ordItem.OSM_Her_2_Status__c = 'Positive';
        ordItem.OSM_IBC_Clinical_Stage__c = 'IV';
        ordItem.OSM_DCIS_Clinical_Stage__c = 'T1mi, N0, M0 (microinvasive)';
        ordItem.OSM_Colon_Clinical_Stage__c = 'Stage III A/B';
        ordItem.OSM_Prostate_Clinical_Stage__c = 'T1/T2';
        ordItem.OSM_Submitted_Gleason_Score__c = '4+3';
        ordItem.OSM_Positive_Cores__c = '>=4 and Gleason >= 3+4';
        ordItem.OSM_PSA__c = '<= 20 ng/mL';
        ordItem.OSM_Out_of_Criteria__c = 'test';
        ordItem.OSM_T4__c = 'Yes';
        ordItem.OSM_MMR_D__c = 'Yes';
        ordItem.OSM_NCCN_Risk_Category__c = 'Intermediate Risk';
        ordItem.OSM_Result_Current__c = result1.Id;
        ordItem.OSM_Bill_Type__c = 'Bill Account';
        ordItem.OSM_Bill_Account__c = acc2.Id;
        ordItem.OSM_Primary_Payor__c = acc3.Id;
        insert ordItem;
        OrderItem ordItem2 = [SELECT Id, OSM_ICD_Coding_System_Formula__c FROM OrderItem WHERE Id =: ordItem.Id];
        //System.assertEquals('Object expected', ordItem2.OSM_ICD_Coding_System_Formula__c);
        
        //OrderSpecimenOLI
        OSM_Order_Specimen_OLI__c ordSpecimenOLI = new OSM_Order_Specimen_OLI__c(OSM_Order_Specimen_ID__c = ordSpecimen.Id, OSM_Billing_policy_N_A__c = false, OSM_OLI_ID__c = ordItem.Id);
        insert ordSpecimenOLI;
        
        //Work Order
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(ord1.Id);
        insert workOrder;
        
        //Work Order Line Item
        OSM_Work_Order_Line_Item__c workOrderLineItem = OSM_DataFactory.createWorkOrderLineItem(workOrder.Id, ordItem.Id);
        insert workOrderLineItem;
        
        //Order Territory Assignment
        OSM_Order_Territory_Assignment__c otAssignment = new OSM_Order_Territory_Assignment__c(OSM_Territory_Name__c = parentTerritory.Id, OSM_OrderName__c = ord1.Id, OSM_Order__c = ord1.Id, OSM_Effective_Start_Date__c = System.Today(), OSM_Effective_Ending_Date__c = System.Today());
        insert otAssignment;
        
        //Order Role
        OSM_Order_Role__c ordRole1 = OSM_DataFactory.createOrderRole(ord1.Id, acc2.Id, con2.Id, 'Ordering',orderRoleRecType);
        insert ordRole1;
        
        //HCP Territory Assignment
        OSM_HCP_Territory_Assignment__c hta = OSM_DataFactory.createHTA(con2.Id, parentTerritory.Id);
        insert hta;
        
        test.startTest();
        // OSM_OrderPayLoadComponentController orderPayLoadController = new OSM_OrderPayLoadComponentController((String)ord1.Id, 'true');
        // OSM_OrderPayLoadComponentController orderPayLoadController2 = new OSM_OrderPayLoadComponentController((String)ordItem.Id, 'true');
        //OSM_OrderPayLoadComponentController orderPayLoadController3 = new OSM_OrderPayLoadComponentController((String)ord2.Id, 'true');
        OSM_OrderPayLoadComponentController orderPayLoadController4 = new OSM_OrderPayLoadComponentController((String)ord3.Id, 'true');
        OSM_OrderPayLoadComponentController orderPayLoadController5 = new OSM_OrderPayLoadComponentController((String)ord4.Id, 'true');
        OSM_OrderPayLoadComponentController orderPayLoadController6 = new OSM_OrderPayLoadComponentController((String)ord5.Id, 'true');
        OSM_OrderPayLoadComponentController orderPayLoadController7 = new OSM_OrderPayLoadComponentController((String)workOrder.Id, 'false');
        //OSM_OrderPayLoadComponentController.ataWrapper a = new OSM_OrderPayLoadComponentController.atawrapper(ata, '123','123','123','123');
        // //OSM_OrderPayLoadComponentController.ordbleWrapper b = new OSM_OrderPayLoadComponentController.ordbleWrapper(orderable1,'123','123');
        // OSM_OrderPayLoadComponentController.orderSpecimenWrapper c = new OSM_OrderPayLoadComponentController.orderSpecimenWrapper(ordSpecimen, '123', '123');
        // OSM_OrderPayLoadComponentController.testCriteriaGroupWrapper d = new OSM_OrderPayLoadComponentController.testCriteriaGroupWrapper('asd','asd');
        // OSM_OrderPayLoadComponentController.sponsorHCOWrapper e = new OSM_OrderPayLoadComponentController.sponsorHCOWrapper(acc2,'123','123');
        // OSM_OrderPayLoadComponentController.oliWrapper f = new OSM_OrderPayLoadComponentController.oliWrapper(ordItem, '123', '123');
        // OSM_OrderPayLoadComponentController.woliWrapper g =new OSM_OrderPayLoadComponentController.woliWrapper(workOrderLineItem, '123', '123');
        //orderPayLoadController.getStudyInformation(ord1.Id);
        //OSM_OrderPayLoadComponentController.getStudyInformation((ord1.Id)ord1.Id);;
        test.stopTest();
        
    }

}