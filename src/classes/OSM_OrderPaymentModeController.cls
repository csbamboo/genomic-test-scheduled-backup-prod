public with sharing class OSM_OrderPaymentModeController {
    private final Order ordr;
    private Order defaultOrdr;
          
    
    public boolean editButton{get; set;}
    public Boolean isWire {get;set;}
    public Boolean isCredit {get;set;}
    public Boolean isCheque {get;set;}
    public Boolean isCheckText {get;set;}
    public Boolean displayEmailText {get;set;}
    public boolean displayPopup {get; set;} 
    public string EnterEmail{get;set;}
    public string EmailValueEntered;
    public List<SelectOption> countryList{get; set;}
    
    public ApexPages.StandardController controller;
    
    Public List<string> leftselectedkeymes1 {get;set;}
    Public List<string> rightselectedkeymes1 {get;set;}
    Set<string> leftvalueskeymes1 = new Set<string>();
    Set<string> rightvalueskeymes1 = new Set<string>();
    Set<Id> ordItemsId = new Set<Id>();
    
    public List<OSM_Payment_Email_History__c> paymentEmailListDisplay {get;set;}
    public  List<order> ord = new List<order>();
    public List<OSM_Payment_Email_History__c> paymentEmailListDisplay1;   
    public List<rowWrapper> rowWrapperList{get;set;}
    
    public List<OSM_Payment_Email_History__c> getpaymentEmailListDisplay1() {
     List<OSM_Payment_Email_History__c> paymentEmailListDisplay2 = new List<OSM_Payment_Email_History__c>(); 
     ord = [SELECT OrderNumber from order where Id =:ApexPages.currentPage().getParameters().get('Id')];
     paymentEmailListDisplay2 = [SELECT  OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =:ord[0].OrderNumber];
     return paymentEmailListDisplay2 ;
    }
    
    public String checkedEmail {get;set;}
    
    Boolean boolChecker {get; set;}
    
    public OSM_OrderPaymentModeController(ApexPages.StandardController controller) {
        this.ordr = (Order)controller.getRecord();
        
        countryList = new List<SelectOption>(); 
        Set<String> filterNames = new Set<String>();
        List<String> sortNames = new List<String>();
        //Get Country Names
        countryList.add(new SelectOption('', '--None--'));
        for(OSM_Country_and_State__c loopCountryState : [SELECT Id, OSM_Country_Name__c 
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Active__c = true]){
            filterNames.add(loopCountryState.OSM_Country_Name__c);
        }
        if(filterNames.size() > 0){
            sortNames.addAll(filterNames);
            sortNames.sort();
            for(String loopCountryName : sortNames){
                countryList.add(new SelectOption(loopCountryName, loopCountryName));
            }
        }

        isCheckText = false;
        rowWrapperList = new List<rowWrapper>();

        ord = [SELECT OrderNumber from order where Id =:ApexPages.currentPage().getParameters().get('Id')];
        for(OSM_Payment_Email_History__c peh: [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =:ord[0].OrderNumber]){
            rowWrapperList.add(new rowWrapper(peh));
        }

         defaultOrdr = [Select  OrderNumber, OSM_Patient__c, OSM_Patient_Last_Name__c, OSM_Patient_First_Name__c, OSM_Patient_DOB__c, OSM_Name_on_Cheque__c, OSM_Cheque_Number__c, OSM_Amount_of_Cheque__c, OSM_Order_Line_Items__c, OSM_Wire_Transfer__c, OSM_Credit_Card__c, OSM_Cheque__c, OSM_Amount_Paid__c, OSM_Sending_Bank_Transfer_Number__c, OSM_Country__c From Order Where Id =:this.ordr.Id];
         system.debug('defaultOrdr  values are^^^^^^^^^^^^^^^^ : ' + defaultOrdr );
         editButton = false;

        
        displayEmailText = true;     
  
        paymentEmailListDisplay = [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =: this.ordr.id];
             
         if(defaultOrdr.OSM_Wire_Transfer__c) {
             isWire = true;
             this.ordr.OSM_Wire_Transfer__c = true;
             isCredit = false;
             isCheque = false;
         }
         
         else if(defaultOrdr.OSM_Credit_Card__c) {
             isCredit = true;
             this.ordr.OSM_Credit_Card__c = true;
             isWire = false;
             isCheque = false;
         }
         
          else if(defaultOrdr.OSM_Cheque__c) {
             isCheque = true;
             this.ordr.OSM_Cheque__c = true;
             isCredit = false;
             isWire = false;
         }
        
        leftselectedkeymes1 = new List<string>();
        rightselectedkeymes1 = new List<string>();
        
        getkeymessages1();
         
    }

    
    public PageReference makePayment(){
        ApexPages.StandardController controller = new ApexPages.StandardController(ordr);
        controller.save();
        
        OSM_Ext_Urls__c cs = OSM_Ext_Urls__c.getOrgDefaults();                        
        PageReference pr = new PageReference(cs.cybersource__c);      
        return pr; 
    } 
    
    public PageReference send(){
        try {
            ApexPages.StandardController controller = new ApexPages.StandardController(ordr);
            system.debug('***rightselectedkeymes1' + rightvalueskeymes1);
            system.debug('***leftselectedkeymes1' + leftvalueskeymes1);
            String tempVal = '';
            for(String sLoop : rightvalueskeymes1){
                if(sLoop != null) {
                tempVal += sLoop + ',';
                }
            }
            
            //return null;

            this.ordr.OSM_Order_Line_Items__c = tempVal == '' ? '' : tempVal.Substring(0,tempVal.length()-1);
            this.ordr.OSM_Payment_Received__c = true;
                      
            //Update related cases
            List<Case> updateCaseList = new List<Case>();
            Id preBillIdClose = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();
           
            
            List<Id> validOLIids = new List<Id>();
            Map<Id, Id> fieldMapOrderItemIdPriceBookId = new  Map<Id, Id>();
            
            for(OrderItem oliLoop : [Select Id, PricebookEntryId From OrderItem Where OSM_Order_Line_Item_ID__c IN: rightvalueskeymes1]){
              fieldMapOrderItemIdPriceBookId.put(oliLoop.PricebookEntryId, oliLoop.Id);
            }
            
            /*for(PricebookEntry  pbeLoop : [Select Id From PricebookEntry Where Id IN: fieldMapOrderItemIdPriceBookId.keySet() And Name IN: selectedOLI]){
                if(fieldMapOrderItemIdPriceBookId.keySet().contains(pbeLoop.Id)){
                    validOLIids.add(fieldMapOrderItemIdPriceBookId.get(pbeLoop.Id));
                }
            }*/
            System.debug('>>>>> rightvalueskeymes1: ' + rightvalueskeymes1);
            System.debug('>>>>> cases: ' + [Select OSM_Self_Pay_Status__c, Status From Case Where OSM_Primary_Order_Line_Item__c IN: fieldMapOrderItemIdPriceBookId.Values() And RecordTypeId =: preBillIdClose And Status = 'Open']);            
            System.debug('>>>>> cases: ' + [Select OSM_Self_Pay_Status__c, Status, Type From Case WHERE Type = 'Benefits Investigation' AND Status = 'Open' AND OSM_Primary_Order_Line_Item__c IN: fieldMapOrderItemIdPriceBookId.Values()]);
            for(Case caseLoop : [Select OSM_Self_Pay_Status__c, Status From Case Where OSM_Primary_Order_Line_Item__c IN: fieldMapOrderItemIdPriceBookId.Values() And RecordTypeId =: preBillIdClose And Status = 'Open']){
                caseLoop.OSM_Self_Pay_Status__c = 'Payment received';
                caseLoop.Status = 'Close';
                updateCaseList.add(caseLoop);
            }
            
            system.debug('***1');
           if(updateCaseList.size() > 0){
               update updateCaseList;
           }
               
                OSM_Payment_Email_History__c eml = new OSM_Payment_Email_History__c();
                //defaultOrdr = [Select  OrderNumber, OSM_Patient__c, OSM_Patient_Last_Name__c, OSM_Patient_First_Name__c, OSM_Patient_DOB__c From Order Where Id =:this.ordr.id];
                eml.OSM_Order_No__c = defaultOrdr.OrderNumber;
                eml.OSM_Order_Id__c = this.ordr.Id;
                eml.OSM_Amount_Paid__c = this.ordr.OSM_Amount_Paid__c;
                system.debug('Amount Paid is @@@@@@@@@@@@@ : ' + eml.OSM_Amount_Paid__c );
                eml.OSM_Country__c = this.ordr.OSM_Country__c;
                eml.OSM_Date_of_Birth__c = defaultOrdr.OSM_Patient_DOB__c;
                eml.OSM_Order_Line_ID__c = this.ordr.OSM_Order_Line_Items__c;
                eml.OSM_Patient_Name__c = defaultOrdr.OSM_Patient_First_Name__c + ' ' + defaultOrdr.OSM_Patient_Last_Name__c;
                eml.OSM_Sending_Bank_Transfer_No__c = this.ordr.OSM_Sending_Bank_Transfer_Number__c;
                insert eml;
                rowWrapperList.add(new rowWrapper(eml));
            System.debug('>>>>> OSM_Payment_Email_History__c: ' + eml);
            controller.save();
          
            editButton = false;
          
            PageReference pr = new PageReference('/Apex/OSM_OrderPaymentMode');
            return pr;
        }
        
        catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
      return controller.view();
 
    }
    
    
    public pageReference editDetails(){
        editButton = true;
        ApexPages.StandardController controller = new ApexPages.StandardController(ordr);
        this.ordr.OSM_Payment_Received__c = false;
        System.debug('>>>>> this.ordr.OSM_Order_Line_Items__c: ' + this.ordr.OSM_Order_Line_Items__c);
        if(this.ordr.OSM_Order_Line_Items__c != '' && this.ordr.OSM_Order_Line_Items__c != null){
            rightvalueskeymes1 = new Set<String>();
            List<String> tempString = String.valueOf(this.ordr.OSM_Order_Line_Items__c).split(',');
            rightvalueskeymes1.addAll(tempString);
        }
        
        controller.save();
        return null;
    }
    public pageReference customCancel(){
      defaultOrdr = new Order();
        defaultOrdr = [Select  OrderNumber, OSM_Patient__c, OSM_Patient_Last_Name__c, OSM_Patient_First_Name__c, OSM_Patient_DOB__c, OSM_Name_on_Cheque__c, OSM_Cheque_Number__c, OSM_Amount_of_Cheque__c, OSM_Order_Line_Items__c, OSM_Wire_Transfer__c, OSM_Credit_Card__c, OSM_Cheque__c, OSM_Amount_Paid__c, OSM_Sending_Bank_Transfer_Number__c, OSM_Country__c From Order Where Id =:this.ordr.Id];
        
        editButton = false;
        system.debug('***defaultOrdr.OSM_Amount_Paid__c' + defaultOrdr.OSM_Amount_Paid__c);
        this.ordr.OSM_Amount_Paid__c = defaultOrdr.OSM_Amount_Paid__c;
        this.ordr.OSM_Sending_Bank_Transfer_Number__c = defaultOrdr.OSM_Sending_Bank_Transfer_Number__c;
        this.ordr.OSM_Country__c = defaultOrdr.OSM_Country__c;
        this.ordr.OSM_Order_Line_Items__c = defaultOrdr.OSM_Order_Line_Items__c;
        PageReference pr = new PageReference('/Apex/OSM_OrderPaymentMode');
        return pr;
    }
    public pageReference customCancel2(){
      defaultOrdr = new Order();
        defaultOrdr = [Select  OrderNumber, OSM_Patient__c, OSM_Patient_Last_Name__c, OSM_Patient_First_Name__c, OSM_Patient_DOB__c, OSM_Name_on_Cheque__c, OSM_Cheque_Number__c, OSM_Amount_of_Cheque__c, OSM_Order_Line_Items__c, OSM_Wire_Transfer__c, OSM_Credit_Card__c, OSM_Cheque__c, OSM_Amount_Paid__c, OSM_Sending_Bank_Transfer_Number__c, OSM_Country__c From Order Where Id =:this.ordr.Id];
        
        editButton = false;
        this.ordr.OSM_Name_on_Cheque__c = defaultOrdr.OSM_Name_on_Cheque__c;
        this.ordr.OSM_Cheque_Number__c = defaultOrdr.OSM_Cheque_Number__c;
        this.ordr.OSM_Amount_of_Cheque__c = defaultOrdr.OSM_Amount_of_Cheque__c;
        PageReference pr = new PageReference('/Apex/OSM_OrderPaymentMode');
        return pr;
    }
    
    public void renderPaymentForm(){
        String formPayment = ApexPages.currentPage().getParameters().get('paymentMode');
        system.debug('***formPayment' + formPayment);
        
        if(formPayment == 'wire'){
          this.ordr.OSM_Name_on_Cheque__c='';
          this.ordr.OSM_Amount_of_Cheque__c=null;
          this.ordr.OSM_Cheque_Number__c=null;
      ordr.OSM_Wire_Transfer__c = true;
       ordr.OSM_Credit_Card__c = false;
       ordr.OSM_Cheque__c = false;
       isWire = true;
       isCredit = false;
       isCheque = false;
             
             //return null;
        }
        
        if(formPayment == 'credit'){
            
            this.ordr.OSM_Amount_Paid__c = null;
            this.ordr.OSM_Sending_Bank_Transfer_Number__c =  '';
            this.ordr.OSM_Country__c = '';
            this.ordr.OSM_Order_Line_Items__c = null;
            this.ordr.OSM_Payment_Received__c = false; 
            
            this.ordr.OSM_Name_on_Cheque__c='';
          this.ordr.OSM_Amount_of_Cheque__c=null;
          this.ordr.OSM_Cheque_Number__c=null;
        
        
             ordr.OSM_Credit_Card__c = true;
             ordr.OSM_Wire_Transfer__c = false;
             ordr.OSM_Cheque__c = false;
             isWire = false;
             isCredit = true;
             isCheque = false;
             
             ApexPages.StandardController controller = new ApexPages.StandardController(ordr);
             controller.save();
             
             //PageReference pr = new PageReference('/Apex/OSM_OrderPaymentMode?id=' + this.ordr.Id);
             //pr.setredirect(true);
             //return pr; 
        }
        
        if(formPayment == 'cheque'){
        
         this.ordr.OSM_Amount_Paid__c = null;
            this.ordr.OSM_Sending_Bank_Transfer_Number__c =  '';
            this.ordr.OSM_Country__c = '';
            this.ordr.OSM_Order_Line_Items__c = null;
            this.ordr.OSM_Payment_Received__c = false;
            
             ordr.OSM_Credit_Card__c = false;
             ordr.OSM_Wire_Transfer__c = false;
             ordr.OSM_Cheque__c = true;
             isWire = false;
             isCredit = false;
             isCheque = true;
             
             //return null;
        }
        
        //return null;
    }
    
    public PageReference getkeymessages1(){
        getkeymessagesList();
        return null;
    }

    public List<SelectOption> getkeymessagesList1() {
        return getkeymessagesList();
    }

    public List<SelectOption> getkeymessagesList() {
        
        system.debug('***rightvalueskeymes1' + rightvalueskeymes1);
        
        List<OrderItem> tempList = [Select Id, OSM_Order_Line_Item_ID__c From OrderItem Where OrderId =: this.ordr.Id];
        List<SelectOption> options = new List<SelectOption>();
        
        leftselectedkeymes1.clear();
        leftvalueskeymes1.clear();
        rightvalueskeymes1.clear();
        rightselectedkeymes1.clear();


        if(tempList.size() > 0){
            system.debug('***tempList' + tempList);
            for (OrderItem temp:  tempList) { 
                if(!rightvalueskeymes1.contains(temp.OSM_Order_Line_Item_ID__c)){
                    options.add(new SelectOption(temp.Id, temp.OSM_Order_Line_Item_ID__c));
                    leftvalueskeymes1.add(temp.OSM_Order_Line_Item_ID__c);
                }
            }  
        }
        return options;
    }

    public List<SelectOption> getunSelectedValues1(){
        return getunSelectedValues();
    }

    public List<SelectOption> getSelectedValues1(){
        return getSelectedValues();
    }


    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> unselectedoptions = new List<SelectOption>();
        List<string> tempList = new List<String>();

        //if(leftvalueskeymes1.size() < 1 && rightvalueskeymes1.size() < 1){
        //  return getkeymessagesList1();
        //}
            
        tempList.addAll(leftvalueskeymes1);   
        tempList.sort();
        for(string s : tempList){
            if(!rightvalueskeymes1.contains(s)){
            unselectedoptions.add(new SelectOption(s,s));
            }
        }
        return unselectedoptions;
    }
    
    public List<SelectOption> getSelectedValues(){
        
        
        
        List<SelectOption> selectedoptions = new List<SelectOption>();
        List<string> tempList = new List<String>();
          
        tempList.addAll(rightvalueskeymes1);
            
        tempList.sort();
        for(String s : tempList){
            selectedoptions.add(new SelectOption(s,s));
        }
        return selectedoptions;
    }

    public PageReference selectclick1(){
        rightselectedkeymes1.clear();
        set<String> tempname = new Set<String>();
        system.debug('leftselectedkeymes1: ' + leftselectedkeymes1);
        system.debug('rightselectedkeymes1: ' + rightselectedkeymes1);
        List<String> templist = leftselectedkeymes1.Clone();
        for(String s : templist){
            leftvalueskeymes1.remove(s);
            tempname.add(s);
            
            Integer j = 0;
            while (j < leftselectedkeymes1.size())
            {
                if(leftselectedkeymes1.get(j) == s){
                    leftselectedkeymes1.remove(j);
                } else {
                    j++;
                }
            }
        }
        map<String,Id> maplist = new Map<String,Id>();
        List<OrderItem> click1tempList = [Select Id, OSM_Order_Line_Item_ID__c From OrderItem Where OSM_Order_Line_Item_ID__c IN: tempname And OrderId =: this.ordr.Id];
        for(OrderItem a: click1tempList){
            rightvalueskeymes1.add(a.OSM_Order_Line_Item_ID__c);
        }
        return null;
    }
    
    public PageReference unselectclick1(){
        leftselectedkeymes1.clear();
        for(String s : rightselectedkeymes1){
            rightvalueskeymes1.remove(s);
            leftvalueskeymes1.add(s);
        }
        return null;
    }
    
    public void showPopup() {        
        displayPopup = true;    
    } 
    public void closePopup() {     
       // EmailValueEntered = EnterEmail;
       // system.debug('@@@@@@@'+store);
        displayPopup = false;    
    }     
    public PageReference SendEmail(){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        List<OSM_Payment_Email_History__c> selectedList = new List<OSM_Payment_Email_History__c>();

        system.debug('test checkbox ' + isCheckText );
        system.debug('test displayEmailText ' + displayEmailText);
        //for(OSM_Payment_Email_History__c paymentEmailListDisplay : [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_Id__c =: this.ordr.Id]){
        for(rowWrapper rw: rowWrapperList ){
            if(rw.rowId){
                selectedList.add(rw.e);
            }
        }
        
        system.debug('test selectedList ' + selectedList);
        
        /*List<OSM_Payment_Email_History__c> testList = [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =: this.ordr.Id];
        system.debug('test list ' + testList );
        for(OSM_Payment_Email_History__c paymentEmailListDisplay : [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_Id__c =: this.ordr.Id]){
        //List<OSM_Payment_Email_History__c> paymentEmailListDisplay = [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =: this.ordr.Id];
            */
        if(selectedList.size() > 0){
            for(OSM_Payment_Email_History__c paymentEmailListDisplay : selectedList){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                List<String> sendTo = new List<String>();
                //Map<String, Email__c> mcs = Email__c.getAll();
    
                 
                //send to:
                EmailValueEntered = EnterEmail;
                displayPopup = false;
                if(EmailValueEntered != null && EmailValueEntered != ''){
                    sendTo.add(EmailValueEntered );
                    mail.setToAddresses(sendTo);
                    system.debug('Email is sent to &&&&&&&&&&&&'+sendTo );
                          
                    //body
                    mail.setSubject('Wire Transfer');
                    //String body = 'Hi Veena';
                    String theDate = paymentEmailListDisplay.OSM_Date_of_Birth__c == null ? '--' : String.Valueof(paymentEmailListDisplay.OSM_Date_of_Birth__c);
                    String body = 'Order No. : ' + paymentEmailListDisplay.OSM_Order_No__c + '<br/>';
                    body += 'Order Line Id          : ' + paymentEmailListDisplay.OSM_Order_Line_ID__c + '<br/>';
                    body += 'Patient Name          : ' + paymentEmailListDisplay.OSM_Patient_Name__c + '<br/>';
                    body += 'Date of Birth          : ' + theDate + '<br/>';
                    body += 'Amount Paid          : ' + paymentEmailListDisplay.OSM_Amount_Paid__c + '<br/>';
                    body += 'Sending Bank/Transfer No.          : ' + paymentEmailListDisplay.OSM_Sending_Bank_Transfer_No__c + '<br/>';
                    body += 'Country          : ' + paymentEmailListDisplay.OSM_Country__c + '<br/>';
                    mail.setHtmlBody(body);
                    
                    
                    mails.add(mail);
                    system.debug('test email ' + mails);
                    system.debug('Email is sent&&&&&&&&&&&&'+mails );
                }
            }
            if(!Test.IsRunningTest()) {
                Messaging.sendEmail(mails);
            }
        }
        /*else{
            for(OSM_Payment_Email_History__c paymentEmailListDisplay : [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =: this.ordr.Id]){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                List<String> sendTo = new List<String>();
                Map<String, Email__c> mcs = Email__c.getAll();
    
                 
                //send to:
                EmailValueEntered = EnterEmail;
                displayPopup = false;
                sendTo.add(EmailValueEntered );
                mail.setToAddresses(sendTo);
                system.debug('Email is sent to &&&&&&&&&&&&'+sendTo );
                      
                //body
                mail.setSubject('Wire Transfer');
                String body = 'Hi Veena';
                String body = 'Order No. : ' + paymentEmailListDisplay.OSM_Order_No__c + '<br/>';
                body += 'Order Line Id          : ' + paymentEmailListDisplay.OSM_Order_Line_ID__c + '<br/>';
                body += 'Patient Name          : ' + paymentEmailListDisplay.OSM_Patient_Name__c + '<br/>';
                body += 'Date of Birth          : ' + paymentEmailListDisplay.OSM_Date_of_Birth__c + '<br/>';
                body += 'Amount Paid          : ' + paymentEmailListDisplay.OSM_Amount_Paid__c + '<br/>';
                body += 'Sending Bank/Transfer No.          : ' + paymentEmailListDisplay.OSM_Sending_Bank_Transfer_No__c + '<br/>';
                body += 'Country          : ' + paymentEmailListDisplay.OSM_Country__c + '<br/>';
                mail.setHtmlBody(body);
                
                
                mails.add(mail);
                system.debug('test email ' + mails);
                Messaging.sendEmail(mails);
                system.debug('Email is sent&&&&&&&&&&&&'+mails );
            }
        }*/
       
        
        system.debug('Email is sent#########' );
        PageReference pr = new PageReference('/Apex/OSM_OrderPaymentMode');
            return pr;
    }
    public class rowWrapper{
        public Boolean rowId {get; set;}
        public OSM_Payment_Email_History__c e {get; set;}
        
        public rowWrapper(OSM_Payment_Email_History__c e){
            this.rowId = false;
            this.e = e;
        }
    }
}