/**
 * File Info
 * ----------------------------------
 * @filename       OSM_OrderPaymentModeController_Test.cls
 * @author         Francis Nasalita
 * @description    Test class for OSM_OrderPaymentModeController. 
 * @history        12.JAN.2015 - Francis Nasalita - Created  
 */
@isTest(seeAllData = false)
private class OSM_OrderPaymentModeController_Test {

    private static testMethod void makePayment() {
        /* START SETUP */
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();

        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;

        Id recTypeOrdrId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Work Order').getRecordTypeId(); 
        Order order1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        //order1.RecordTypeId = recTypeOrdrId;
        order1.OSM_Credit_Card__c = true;
        order1.OSM_Bill_Type__c = 'Patient Pre-Pay';
        order1.Pricebook2Id = pb2Standard;
        insert order1;
        
        Order od1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od1.Pricebook2Id = pb2Standard;
        insert od1;

        PageReference pageRef = new PageReference('https://c.cs9.visual.force.com/apex/OSM_OrderPaymentMode?id=' + order1.Id + '&paymentMode=credit');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(order1);
        OSM_OrderPaymentModeController controller = new OSM_OrderPaymentModeController(sCon);
        /* END SETUP */
        
        /* START TEST */
        Test.startTest();
            OSM_Ext_Urls__c cs = OSM_Ext_Urls__c.getOrgDefaults();
            cs.cybersource__c = 'https://ebc.cybersource.com/ebc/login/Login.do;jsessionid=5EA6FE6E3D508E6741AEF00FD0E1B6FA.elpw001-eb';
            insert cs;
            PageReference pagRef = controller.makePayment();
            controller.renderPaymentForm();
        Test.stopTest();
        /* END TEST */
        
        /* START VERIFICATION */
        System.assertEquals('https://ebc.cybersource.com/ebc/login/Login.do;jsessionid=5EA6FE6E3D508E6741AEF00FD0E1B6FA.elpw001-eb',pagRef.getUrl(),'');
        /* END VERIFICATION */
    }

    private static testMethod void testWire() {
        /* START SETUP */
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();
        //Id rtCase = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();

        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;

        Id recTypeOrdrId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Work Order').getRecordTypeId(); 
        Order od = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od.Pricebook2Id = pb2Standard;
        insert od;
        //od.RecordTypeId = recTypeOrdrId;
        od.OSM_Wire_Transfer__c = true;
        update od;
        
        Order od1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od1.Pricebook2Id = pb2Standard;
        insert od1;
        
        od1.OSM_Order__c = od.Id;
        update od1;
        
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard, 7);
        insert pbe;
        
        List<OrderItem> orderItemList = new List<OrderItem>();
        OrderItem ordrItm1 = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today());
        orderItemList.add(ordrItm1);
        OrderItem ordrItm2 = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today());
        orderItemList.add(ordrItm2);
        OrderItem ordrItm3 = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today());
        orderItemList.add(ordrItm3);
        insert orderItemList;

        od.OSM_Order_Line_Items__c = orderItemList[2].OSM_Order_Line_Item_ID__c + ',' + orderItemList[2].OSM_Order_Line_Item_ID__c;
        update od;

        Case caseObj = OSM_DataFactory.createCase('Open','Benefits Investigation');
        caseObj.RecordTypeId = rtCase;
        caseObj.OSM_Primary_Order_Line_Item__c = orderItemList[2].Id;
        insert caseObj;

        PageReference pageRef = new PageReference('https://c.cs9.visual.force.com/apex/OSM_OrderPaymentMode?id=' + od.Id + '&paymentMode=wire');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(od);
        OSM_OrderPaymentModeController controller = new OSM_OrderPaymentModeController(sCon);
        /* END SETUP */
        
        /* START TEST */
        Test.startTest();
        
                List<OSM_Payment_Email_History__c> pehList =  controller.getpaymentEmailListDisplay1();
                List<OrderItem> click1tempList = [Select Id, OSM_Order_Line_Item_ID__c From OrderItem Where OrderId =: od.Id];
                
                controller.editDetails();
                controller.getSelectedValues1();
                controller.getunSelectedValues1();
                controller.leftselectedkeymes1.add(click1tempList[0].OSM_Order_Line_Item_ID__c);
                controller.rightselectedkeymes1.add(click1tempList[2].OSM_Order_Line_Item_ID__c);
                controller.selectclick1();
                controller.send();
                controller.rightselectedkeymes1.add(click1tempList[0].OSM_Order_Line_Item_ID__c);
                controller.unselectclick1();
                controller.editDetails();
                controller.leftselectedkeymes1.add(click1tempList[0].OSM_Order_Line_Item_ID__c);
                controller.selectclick1();
                controller.send();
                controller.rowWrapperList[0].rowId = true;
                controller.EnterEmail = 'francis.nasalita@cloudsherpas.com';
                controller.SendEmail();
                controller.showPopup();
                controller.closePopup();
                controller.customCancel();
                controller.renderPaymentForm();
                
        Test.stopTest();
        /* END TEST */
        
        /* START VERIFICATION */
        System.AssertEquals(true,od.OSM_Wire_Transfer__c);
        System.AssertEquals(false,od.OSM_Credit_Card__c);
        System.AssertEquals(false,od.OSM_Cheque__c);
        System.AssertEquals(true,controller.isWire);
        System.AssertEquals(false,controller.isCredit);
        System.AssertEquals(false,controller.isCheque);
        /* END VERIFICATION */
    }

    private static testMethod void testCheque() {
        /* START SETUP */
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
        
        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;

        Id recTypeOrdrId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Work Order').getRecordTypeId(); 
        Order order1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        //order1.RecordTypeId = recTypeOrdrId;
        order1.OSM_Cheque__c = true;
        order1.OSM_Bill_Type__c = 'Patient Pre-Pay';
        order1.Pricebook2Id = pb2Standard;
        insert order1;
        
        Order od1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od1.Pricebook2Id = pb2Standard;
        insert od1;

        PageReference pageRef = new PageReference('/apex/OSM_OrderPaymentMode?id=' + order1.Id + '&paymentMode=cheque');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(order1);
        OSM_OrderPaymentModeController controller = new OSM_OrderPaymentModeController(sCon);
        /* END SETUP */
        
        /* START TEST */
        Test.startTest();
                controller.editDetails();
                controller.customCancel2();
                controller.editDetails();
                controller.send();
                controller.renderPaymentForm();
        Test.stopTest();
        /* END TEST */
        
        /* START VERIFICATION */
        System.AssertEquals(false,order1.OSM_Wire_Transfer__c);
        System.AssertEquals(false,order1.OSM_Credit_Card__c);
        System.AssertEquals(true,order1.OSM_Cheque__c);
        System.AssertEquals(false,controller.isWire);
        System.AssertEquals(false,controller.isCredit);
        System.AssertEquals(true,controller.isCheque);
        
        /* END VERIFICATION */
    }
}