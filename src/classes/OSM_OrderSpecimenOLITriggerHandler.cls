/**
 * File Info
 * ----------------------------------
 * @filename       OSM_OrderSpecimenOLITriggerHandler.cls
 * @created        10.APR.2015
 * @author         Kristian Vegerano
 * @description    Class for Order Specimen OLI Trigger changes. 
 * @history        10.APR.20154 - Kristian Vegerano - Created  
 */
public class OSM_OrderSpecimenOLITriggerHandler{
    public static Boolean runPullBillPolicyReadyOnce = true;
    public static Boolean runBillingPolicyQualificationCheckOnce = true;

    /**
     * @author         Kristian Vegerano
     * @description    Method for any changes that should happen before an OSM_Order_Specimen_OLI__c record is created. 
     * @history        10.APR.2014 - Kristian Vegerano - Created  
     */
    public static void onBeforeInsert(List<OSM_Order_Specimen_OLI__c> orderSpecimenOLIList){
        Set<Id> osIds = new Set<Id>();
        
        for(OSM_Order_Specimen_OLI__c os : orderSpecimenOLIList){
            osIds.add(os.Id);
        }
        
        if(runPullBillPolicyReadyOnce){
            runPullBillPolicyReadyOnce = false;
            /*Commented out per Rao - Paul Wittmeyer 8/7/2015
            pullBillPolicyReady(orderSpecimenOLIList);
            if(OSM_OrderTriggerHandler.globalIsFuture){
                pullBillPolicyReady(osIds);
            }*/
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for any changes that should happen after an OSM_Order_Specimen_OLI__c record is created. 
     * @history        10.APR.2014 - Kristian Vegerano - Created  
     */
    public static void onAfterInsert(List<OSM_Order_Specimen_OLI__c> orderSpecimenOLIList){
        Set<Id> osIds = new Set<Id>();
        
        for(OSM_Order_Specimen_OLI__c os : orderSpecimenOLIList){
            osIds.add(os.Id);
        }
        
        if(runBillingPolicyQualificationCheckOnce){
            runBillingPolicyQualificationCheckOnce = false;
            //billingPolicyQualificationCheck(orderSpecimenOLIList);
            billingPolicyQualificationCheck(osIds);
        }
    }

    /**
     * @author         Kristian Vegerano
     * @description    Method for any changes that should happen before an OSM_Order_Specimen_OLI__c record is updated. 
     * @history        10.APR.2014 - Kristian Vegerano - Created  
     */
    public static void onBeforeUpdate(List<OSM_Order_Specimen_OLI__c> orderSpecimenOLIList, Map<Id,OSM_Order_Specimen_OLI__c> orderSpecimenOLIMap){
        Set<Id> osIds = new Set<Id>();
        
        for(OSM_Order_Specimen_OLI__c os : orderSpecimenOLIList){
            osIds.add(os.Id);
        }
        if(runPullBillPolicyReadyOnce){
            runPullBillPolicyReadyOnce = false;
            /*Commented out per Rao - Paul Wittmeyer 8/7/2015
            pullBillPolicyReady(orderSpecimenOLIList);
            if(OSM_OrderTriggerHandler.globalIsFuture){
                pullBillPolicyReady(osIds);
            }*/
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for any changes that should happen after an OSM_Order_Specimen_OLI__c record is updated. 
     * @history        10.APR.2014 - Kristian Vegerano - Created  
     */
    public static void onAfterUpdate(List<OSM_Order_Specimen_OLI__c> orderSpecimenOLIList, Map<Id,OSM_Order_Specimen_OLI__c> orderSpecimenOLIMap){
        Set<Id> osIds = new Set<Id>();
        
        for(OSM_Order_Specimen_OLI__c os : orderSpecimenOLIList){
            osIds.add(os.Id);
        }
        
        if(runBillingPolicyQualificationCheckOnce){
            runBillingPolicyQualificationCheckOnce = false;
            //billingPolicyQualificationCheck(orderSpecimenOLIList);
            if(OSM_OrderTriggerHandler.globalIsFuture){
                billingPolicyQualificationCheck(osIds);
            }
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that pulls the value of the OLI OSM_Billing_Policy_Qualification_ready to the Order Specimen OLI Billing_Policy_Qualification_ready__c
     * @history        10.APR.2014 - Kristian Vegerano - Created  
     */
     /*Commented out per Rao - Paul Wittmeyer 8/7/2015
     @future
    public static void pullBillPolicyReady(Set<id> orderSpecimenOLIIds){
        OSM_OrderTriggerHandler.globalIsFuture = false;
        if(UserInfo.getUserType() == 'Standard'){
            Set<Id> orderItemIds = new Set<Id>();
            
            List<OSM_Order_Specimen_OLI__c> orderSpecimenOLIList = [
                select Id, OSM_OLI_ID__c 
                from OSM_Order_Specimen_OLI__c 
                where Id in :orderSpecimenOLIIds
            ];
            
            for(OSM_Order_Specimen_OLI__c loopOrderSpecimenOLI : orderSpecimenOLIList){
                orderItemIds.add(loopOrderSpecimenOLI.OSM_OLI_ID__c);
            }
            
            Map<Id,OrderItem> orderItemMap = new Map<Id,OrderItem>([SELECT Id, OSM_OS_OLI_Billing_ready__c 
                                                                    FROM OrderItem 
                                                                    WHERE Id IN :orderItemIds]);
                                                                    
            for(OSM_Order_Specimen_OLI__c loopOrderSpecimenOLI : orderSpecimenOLIList){
                if(orderItemMap.containsKey(loopOrderSpecimenOLI.OSM_OLI_ID__c)){
                    //loopOrderSpecimenOLI.OSM_Billing_Policy_Qualification_ready__c = orderItemMap.get(loopOrderSpecimenOLI.OSM_OLI_ID__c).OSM_OS_OLI_Billing_ready__c;
                }
            }
        }
    }*/
    
    /*
     * @author         Kristian Vegerano
     * @description    Method for User Story 16859: Billing Policy Qualification Status Check, OLI
     * @parameter      List of Order Item records 
     * @history        10.APRIL.2015 - Kristian Vegerano - Created  
     */
     @future
    public static void billingPolicyQualificationCheck(Set<Id> orderSpecimenOLIIds){
        OSM_OrderTriggerHandler.globalIsFuture = false;
        if(UserInfo.getUserType() == 'Standard'){
            Set<Id> orderItemIds = new Set<Id>();
            Set<Id> orderSpecimenIds = new Set<Id>();   
            List<OrderItem> orderItemList = new List<OrderItem>();
            
            List<OSM_Order_Specimen_OLI__c> orderSpecimenOLIList = [
                select Id, OSM_OLI_ID__c 
                from OSM_Order_Specimen_OLI__c 
                where Id in :orderSpecimenOLIIds
            ];
            
            Map<Id,List<OSM_Order_Specimen__c>> orderItemSpecimenMap = new Map<Id,List<OSM_Order_Specimen__c>>();
            
            //Collect OrderItem Ids
            for(OSM_Order_Specimen_OLI__c loopOrderSpecimenItem : orderSpecimenOLIList){
                orderSpecimenIds.add(loopOrderSpecimenItem.OSM_OLI_ID__c);
            }
            
            //Collect and associate Order Specimen records to Order Line Item records
            for(OSM_Order_Specimen_OLI__c loopOrderSpecimenOLI : [SELECT Id, OSM_Order_Specimen_ID__c, OSM_OLI_ID__c, OSM_Order_Specimen_ID__r.OSM_Billing_Policy_Qualification_status__c
                                                                  FROM OSM_Order_Specimen_OLI__c
                                                                  WHERE OSM_OLI_ID__c IN :orderSpecimenIds
                                                                    AND OSM_OLI_ID__r.OSM_State__c = 'Active']){
                OSM_Order_Specimen__c tempOrderSpecimen;
                
                if(!orderItemIds.contains(loopOrderSpecimenOLI.OSM_OLI_ID__c)){
                    orderItemIds.add(loopOrderSpecimenOLI.OSM_OLI_ID__c);
                    orderItemList.add(new OrderItem(Id = loopOrderSpecimenOLI.OSM_OLI_ID__c));
                }
                if(!orderItemSpecimenMap.containsKey(loopOrderSpecimenOLI.OSM_OLI_ID__c)){
                    
                    tempOrderSpecimen = new OSM_Order_Specimen__c(Id = loopOrderSpecimenOLI.OSM_Order_Specimen_ID__c, 
                                                                                        OSM_Billing_Policy_Qualification_status__c = loopOrderSpecimenOLI.OSM_Order_Specimen_ID__r.OSM_Billing_Policy_Qualification_status__c);
                    orderItemSpecimenMap.put(loopOrderSpecimenOLI.OSM_OLI_ID__c, new List<OSM_Order_Specimen__c>{tempOrderSpecimen});
                    
                }else{
                    tempOrderSpecimen = new OSM_Order_Specimen__c(Id = loopOrderSpecimenOLI.OSM_Order_Specimen_ID__c, 
                                                                                        OSM_Billing_Policy_Qualification_status__c = loopOrderSpecimenOLI.OSM_Order_Specimen_ID__r.OSM_Billing_Policy_Qualification_status__c);
                    orderItemSpecimenMap.get(loopOrderSpecimenOLI.OSM_OLI_ID__c).add(tempOrderSpecimen);
                }
            }
            
            //Set OSM_Billing_Policy_Qualification_status__c field
            for(OrderItem loopOrderItem : orderItemList){
                Boolean isAllPopulated = true;
                Boolean hasDisqualified = false;
                if(orderItemSpecimenMap.containsKey(loopOrderItem.Id)){
                    for(OSM_Order_Specimen__c loopOrderSpecimen : orderItemSpecimenMap.get(loopOrderItem.Id)){
                        if(loopOrderSpecimen.OSM_Billing_Policy_Qualification_status__c == null){
                            isAllPopulated = false;
                        }
                        if(loopOrderSpecimen.OSM_Billing_Policy_Qualification_status__c == 'Disqualified'){
                            hasDisqualified = true;
                        }
                    }
                }
                
                if(orderItemSpecimenMap.containsKey(loopOrderItem.Id)){
                    if(isAllPopulated){
                        if(hasDisqualified){
                            loopOrderItem.OSM_Billing_Policy_Qualification_status__c = 'Cancellation Recommended';
                        }else{
                            loopOrderItem.OSM_Billing_Policy_Qualification_status__c = 'OK to Proceed';
                        }
                    }
                }
            }
            
            if(orderItemList.size() > 0){
            	try{
                	update orderItemList;
            	}catch(Exception e){
            		logger.debugExceptionForAsync(e);
            	}
            }
        }
    }
}