/*
    @author: Jerome Liwanag
    @description: Order Specimen OLI Trigger Handler Test
    @date: 30 June 2015 - Create
*/
@isTest
private class OSM_OrderSpecimenOLITriggerHandler_Test {

	static testMethod void testMethod1() {
	    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        insert triggerSwitch;
        // PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        Id pb2Standard = test.getStandardPricebookId();
        OSM_OrderTriggerHandler.globalIsFuture = true;
        Account accList = OSM_DataFactory.createAccount('Dasdwe');
        insert accList;
        Contact conList = OSM_DataFactory.createContact(123, accList.Id, Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        insert conList;
        Order ordList =  new Order(OSM_Patient__c = conList.Id, AccountId = accList.Id ,Pricebook2Id = pb2Standard, EffectiveDate = System.Today(), Status = 'New');
        insert ordList;
        //Pricebook2 pb2 = [SELECT Id FROM Pricebook2 WHERE isStandard = true];
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
         insert prod2;
         PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard, 7);
         insert pbe;
        OrderItem ordItem = new OrderItem(OrderId = ordList.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, OSM_State__c = 'Active');
        insert ordItem;
	    OSM_Order_Specimen__c ordSpecimen = new OSM_Order_Specimen__c(Name = 'Wasd', OSM_Billing_Policy_Qualification_status__c = 'Disqualified');
	    insert ordSpecimen;
        OSM_Order_Specimen_OLI__c ordSpecimenOLI = new OSM_Order_Specimen_OLI__c(OSM_Order_Specimen_ID__c = ordSpecimen.Id, OSM_Billing_policy_N_A__c = false, OSM_OLI_ID__c = ordItem.Id);
        insert ordSpecimenOLI;
        Set<Id> orderSpecimenOLIIds = new Set<Id>();
        orderSpecimenOLIIds.add(ordSpecimenOLI.Id);
        Test.startTest();
        OSM_OrderSpecimenOLITriggerHandler.runPullBillPolicyReadyOnce = true;
        OSM_OrderSpecimenOLITriggerHandler.runBillingPolicyQualificationCheckOnce = true;
        OSM_OrderTriggerHandler.globalIsFuture = true;  
        OSM_OrderSpecimenOLITriggerHandler.billingPolicyQualificationCheck(orderSpecimenOLIIds);
        //OSM_OrderSpecimenOLITriggerHandler.pullBillPolicyReady(orderSpecimenOLIIds);
        ordSpecimenOLI.OSM_Billing_policy_N_A__c = true;
        update ordSpecimenOLI;
        
        Test.stopTest();
	}

}