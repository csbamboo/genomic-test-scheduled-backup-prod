/*
  @author: Karla Reytas/ Roden Songco
  @date: 20 APR 2015
  @description:Order Specimen Result Trigger Handler
  @history:20 APR 2015 - Created (KR/RS)
            - Commented out, transfer logic in OSM_ResultTriggerHandler this trigger is obsolete (DQ)

*/
public with sharing class OSM_OrderSpecimenResultTriggerHandler {
    /*
    public static Boolean autoNumGenRunOnce = false;

    public static void onBeforeInsert(List<OSM_Result__c> newResult){
        if(!autoNumGenRunOnce){
            autoNumGen(newResult);
        }
        newCurrentCheck(newResult);

    }
    */

    /*
    public static void onAfterUpdate(List<OSM_Result__c> currentResult) { 
        updateCurrentCheck(currentResult);
   
    }
    
    public static void autoNumGen(List<OSM_Result__c> newResult){
        //************
        
        String orderNumber = '';
        String a = '';
        Integer aLength;
        String tempNumber = '';
        Integer num;
        
        //***************
        
        Record_Type__c systemSet = Record_Type__c.getOrgDefaults();
        
        Integer autoInt = Integer.valueOf(systemSet.Auto_Number_Generator__c);
        String autoNumTemp;
        
        Set<Id> tempOrder = new Set<Id>();
        
        Map<Id, Order> ordIdMap = new Map<Id, Order>([SELECT Id, OrderNumber FROM Order limit 1]);
        
        
        for(OSM_Result__c getOrderLoop : newResult){
            tempOrder.add(getOrderLoop.Order_ID__c);
        }
        
        List<OSM_Result__c> res = new List<OSM_Result__c>();
        
        res = [SELECT id, Name, Order_ID__c, OSM_Report_ID__c FROM OSM_Result__c Where Order_ID__c =: tempOrder ORDER BY Name DESC];

        Map<Id, String> mapIdName = new Map<Id, String>();
        for(Order orLoop : [Select Id, OrderNumber From Order Where Id IN: tempOrder]){
            mapIdName.put(orLoop.Id, orLoop.OrderNumber);
        }

        Map<Id, List<String>> mapOrIdOrName = new Map<Id, List<String>>();
        
        //Query all results in the Order
        for(OSM_Result__c resLoop : res){
            if(!mapOrIdOrName.keySet().contains(resLoop.Order_ID__c)){
                mapOrIdOrName.put(resLoop.Order_ID__c, new List<String>{resLoop.OSM_Report_ID__c});
            } else {
                mapOrIdOrName.get(resLoop.Order_ID__c).add(resLoop.OSM_Report_ID__c);
            }
        }

        //Query all results with the same Report ID as Order ID
        Map<Id, List<String>> resFilterMap = new Map<Id, List<String>>();
        
        for(Id oridLoop : mapOrIdOrName.keySet()){
            for(String reportLoop : mapOrIdOrName.get(oridLoop)){
                if(reportLoop != null){
                    a = reportLoop.left(11);
                    if(mapIdName.get(oridLoop) == a){
                        if(!resFilterMap.keySet().contains(oridLoop)){
                            resFilterMap.put(oridLoop, new List<String>{reportLoop});
                        } else {
                            resFilterMap.get(oridLoop).add(reportLoop);
                        }
                    }
                }
            }
        }

        //Getting Report Name 
        Map<Id, Integer> curReportNumberMap = new Map<Id, Integer>();
        for(Id resIdLoop : resFilterMap.keySet()){
            
            
            
            aLength = resFilterMap.get(resIdLoop)[0].length();
            a = String.ValueOf(resFilterMap.get(resIdLoop)[0]).substring(12,aLength);
            
            if(!curReportNumberMap.keySet().contains(resIdLoop)){
                curReportNumberMap.put(resIdLoop, Integer.ValueOf(a));
            }
        }

        //Populate Report ID if report ID is null
        for(OSM_Result__c resLoop : newResult){
                    
                if(resLoop.OSM_Report_ID__c == null || resLoop.OSM_Report_ID__c == ''){
                    orderNumber = String.ValueOf(mapIdName.get(resLoop.Order_ID__c));
                    if(curReportNumberMap.get(resLoop.Order_ID__c) != null){
                        num = curReportNumberMap.get(resLoop.Order_ID__c) + 1;
                        if(String.ValueOf(curReportNumberMap.get(resLoop.Order_ID__c)).length() == 1){
                           autoNumTemp =  orderNumber + '-0' + String.ValueOf(num);
                        }else{
                            autoNumTemp =  orderNumber + '-' + String.ValueOf(num);
                        }
                    }else{
                        autoNumTemp =  orderNumber + '-01';
                    }
                    /*
                    else if(String.ValueOf(autoInt).length() == 4){
                       autoNumTemp =  'RS00000' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 5){
                       autoNumTemp =  'RS0000' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 6){
                       autoNumTemp =  'RS000' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 7){
                       autoNumTemp =  'RS00' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 8){
                       autoNumTemp =  'RS0' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 9){
                       autoNumTemp =  'RS' + String.ValueOf(autoInt++);
                    } 
                    
                    
                    resLoop.OSM_Report_ID__c = autoNumTemp;
                }
            
        }
        
        //systemSet.Auto_Number_Generator__c =  String.ValueOf(autoInt);
        //update systemSet;
        
        autoNumGenRunOnce = true;
    }
    */
        

/*
    public static void autoNumGen(List<OSM_Result__c> newResult){
        //************
        
        Set<Id> ordSpecIdSet = new Set<Id>();
        String orderNumber = '';
        
        for(OSM_Result__c osId : newResult){
            ordSpecIdSet.add(osId.OSM_Resulted_Order_Specimen_ID__c);
        }
        
        Map<Id, OSM_Order_Specimen__c> ordSpecIdMap = new Map<Id, OSM_Order_Specimen__c>([SELECT OSM_Order__c, OSM_Order__r.OrderNumber, OSM_External_Specimen_ID__c 
                                                                                          FROM OSM_Order_Specimen__c]);
        
        //***************
        
        Record_Type__c systemSet = Record_Type__c.getOrgDefaults();
        
        Integer autoInt = Integer.valueOf(systemSet.Auto_Number_Generator__c);
        String autoNumTemp;
        
        
        for(OSM_Result__c resLoop : newResult){
                    
                if(resLoop.OSM_Report_ID__c == null || resLoop.OSM_Report_ID__c == ''){
                    orderNumber = ordSpecIdMap.get(resLoop.OSM_Resulted_Order_Specimen_ID__c).OSM_Order__r.OrderNumber;
                    autoNumTemp = '';
                    if(String.ValueOf(autoInt).length() == 1){
                       autoNumTemp =  orderNumber + '-0' + String.ValueOf(autoInt++);

                    }
                    else if(String.ValueOf(autoInt).length() == 2){
                       autoNumTemp =  orderNumber + '-' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 3){
                        autoInt = 0;
                        autoNumTemp =  orderNumber + '-0' + String.ValueOf(autoInt++);
                    }
                    /*
                    else if(String.ValueOf(autoInt).length() == 4){
                       autoNumTemp =  'RS00000' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 5){
                       autoNumTemp =  'RS0000' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 6){
                       autoNumTemp =  'RS000' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 7){
                       autoNumTemp =  'RS00' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 8){
                       autoNumTemp =  'RS0' + String.ValueOf(autoInt++);
                    }
                    else if(String.ValueOf(autoInt).length() == 9){
                       autoNumTemp =  'RS' + String.ValueOf(autoInt++);
                    } 

                    
                    resLoop.OSM_Report_ID__c = autoNumTemp;
                }
            
        }
        
        systemSet.Auto_Number_Generator__c =  String.ValueOf(autoInt);
        update systemSet;
        
        autoNumGenRunOnce = true;
    }
    */
    
    /*
    public static void newCurrentCheck(List<OSM_Result__c> newResult){
        
        Set<Id> resultSets = new Set<Id>();
        
        for(OSM_Result__c res : newResult){
            if(res.OSM_Current__c == true){
                resultSets.add(res.OSM_Resulted_Order_Specimen_ID__c);
            }
        }
        
        List<OSM_Result__c> currentResultList = new List<OSM_Result__c>();
        currentResultList = [SELECT OSM_Resulted_Order_Specimen_ID__c, OSM_Current__c FROM OSM_Result__c
        WHERE (OSM_Resulted_Order_Specimen_ID__c IN :resultSets AND OSM_Current__c = true)];
        
        for(OSM_Result__c r: currentResultList){
                r.OSM_Current__c = false;
        }
            update currentResultList;
        
    }
    
    public static void updateCurrentCheck(List<OSM_Result__c> currentResult){
        
        Set<String> resultSets = new Set<String>();
        
        for(OSM_Result__c res : currentResult){
            if(res.OSM_Current__c == true){
                resultSets.add(res.Id);
                resultSets.add(res.OSM_Resulted_Order_Specimen_ID__c);
            }
        }
        
        List<OSM_Result__c> currentResultList = new List<OSM_Result__c>();
        currentResultList = [SELECT Id, OSM_Current__c, OSM_Resulted_Order_Specimen_ID__c FROM OSM_Result__c
        WHERE (OSM_Resulted_Order_Specimen_ID__c IN:resultSets AND Id NOT IN :resultSets AND OSM_Current__c = true)];
        
        for(OSM_Result__c r: currentResultList){
                
                r.OSM_Current__c = false;
            
        }
            update currentResultList;
        
    }
    */

}