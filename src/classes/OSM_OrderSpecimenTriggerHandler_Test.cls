/*
    @author: Jerome Liwanag
    @description: Order Specimen Trigger Handler Test
    @date: 30 June 2015 - Create
           18 Sept 2015 - Modified
*/
@isTest(seeAllData = false)
private class OSM_OrderSpecimenTriggerHandler_Test {

    private static testMethod void testMethod1() {
        TriggerSwitch__c triggerSwitch = new TriggerSwitch__c();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        triggerSwitch.order_Role_Trigger__c = false;
        insert triggerSwitch;
        //PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        Id orderRoleRecType = Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Account & Contact').getRecordTypeId();
        
        OSM_OrderTriggerHandler.globalIsFuture = true;
        Account hcoAcct = OSM_DataFactory.createAccountWithBillingAddress(1234, 'test', 'test', 'test', 'test', 'test', String.valueOf(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId()).substring(0,15));
        Account accList = OSM_DataFactory.createAccount('Dasdwe');
        
        insert accList;
        Contact conList = OSM_DataFactory.createContact(123, accList.Id, Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        insert conList;
        Order ordList =  new Order(OSM_Patient__c = conList.Id, AccountId = accList.Id ,Pricebook2Id = Test.getStandardPricebookId(), EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic' );
        insert ordList;
        Order ordList2 =  new Order(OSM_Patient__c = conList.Id, OSM_Signature_Date__c = System.today(),AccountId = accList.Id ,Pricebook2Id = Test.getStandardPricebookId(), EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'International', OSM_Triage_Outcome__c = 'New', OSM_Status__c = 'Order Intake');
        insert ordList2;
        Order ordList4 =  new Order(OSM_Patient__c = conList.Id, OSM_Signature_Date__c = System.today(),AccountId = accList.Id ,Pricebook2Id = Test.getStandardPricebookId(), EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic', OSM_Triage_Outcome__c = 'New', OSM_Status__c = 'Order Intake');
        insert ordList4;
        Order ordList5 =  new Order(OSM_Patient__c = conList.Id, OSM_Signature_Date__c = System.today(),AccountId = accList.Id ,Pricebook2Id = Test.getStandardPricebookId(), EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic', OSM_Triage_Outcome__c = 'New', OSM_Status__c = 'Order Intake');
        insert ordList5;
        Order ordList6 =  new Order(OSM_Patient__c = conList.Id, OSM_Signature_Date__c = System.today(),AccountId = accList.Id ,Pricebook2Id = Test.getStandardPricebookId(), EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic', OSM_Triage_Outcome__c = 'New', OSM_Status__c = 'Order Intake');
        insert ordList6;
        
        Order ordList3 =  new Order(OSM_Patient__c = conList.Id, OSM_Signature_Date__c = System.today(),AccountId = accList.Id ,Pricebook2Id = Test.getStandardPricebookId(), EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'International', OSM_Triage_Outcome__c = 'New', OSM_Status__c = 'Order Intake');
        insert ordList3;
        
        OSM_Order_Role__c ordRole = OSM_DataFactory.createOrderRole(ordList.Id, accList.Id, null, 'Ordering',orderRoleRecType);
        OSM_Order_Role__c ordRole4 = OSM_DataFactory.createOrderRole(ordList.Id, accList.Id, null, 'Ordering',orderRoleRecType);
        OSM_Order_Role__c ordRole2 = OSM_DataFactory.createOrderRole(ordList2.Id, accList.Id, null, 'Ordering',orderRoleRecType);
        OSM_Order_Role__c ordRole3 = OSM_DataFactory.createOrderRole(ordList.Id, accList.Id, null, 'Material Return',orderRoleRecType);
        ordRole.OSM_Letter_of_Agreement__c = 'LOA in Place';
        ordRole4.OSM_Letter_of_Agreement__c = 'LOA in Place';
        ordRole2.OSM_Letter_of_Agreement__c = 'LOA Refused';
        
        insert ordRole3;
        insert ordRole;
        insert ordRole2;
        
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
         insert prod2;
         PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, Test.getStandardPricebookId(), 7);
         insert pbe;
         
        OrderItem orditem3 = OSM_DataFactory.createOrderItem(ordList3.Id, pbe.Id, 1, 1, System.today()+1);
        insert orditem3;
        
        ordList3.OSM_Triage_Outcome__c = 'New';
        ordList3.OSM_Status__c = 'Processing';
        update ordList3;
        
        ordRole.OSM_All_Roles__c = 'Additional';
        update ordRole;
        
        // Order ordList2 =  new Order(OSM_Patient__c = conList.Id, AccountId = accList.Id ,Pricebook2Id = Test.getStandardPricebookId(), EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'International');
        // insert ordList2;
        
        //Pricebook2 pb2 = [SELECT Id FROM Pricebook2 WHERE isStandard = true];
        
        
        OSM_OrderSpecimenTriggerHandler.hasEnsureUniqueOrderAndMasterSpecimenRun = false;
        OSM_OrderSpecimenTriggerHandler.hasRun = false;
        OSM_OrderSpecimenTriggerHandler.hasPopulateFirstAvailableDateRun = false;
        OSM_OrderSpecimenTriggerHandler.runUpdatePRAOnce = false;
        OSM_OrderSpecimenTriggerHandler.hasCheckReadyOrderSpecimenRun = false;
        OSM_OrderSpecimenTriggerHandler.hasCountNonReadyOrderSpecimenRun = false;
        OSM_OrderSpecimenTriggerHandler.hasAutoCloseParentRun = false;
        OSM_OrderSpecimenTriggerHandler.hasValidationBuilderRun = false;
        OSM_Order_Specimen__c ordSpecimen = new OSM_Order_Specimen__c(Name = 'Wasd', 
        OSM_Incoming_Tracking_Number__c = '1234', 
        OSM_Order__c = ordList.Id, 
        OSM_Date_of_Collection__c = System.Today(), 
        OSM_Available_for_Processing__c = true, 
        OSM_Billing_Policy_Data_Status__c = 'Data Available', 
        //OSM_Potentially_Responsible_Account__c = ordRole.Id, 
        OSM_First_Available_Date__c = System.Today(), 
        OSM_Hospital_Status__c = 'Non-Hospital',
        OSM_Discharge_Date__c = System.Today(),
        OSM_Storage_Retrieval_Date__c = System.Today());
        
        OSM_Order_Specimen__c ordSpecimen2 = new OSM_Order_Specimen__c(Name = 'Wasda', 
        OSM_Incoming_Tracking_Number__c = '1234', 
        OSM_Order__c = ordList2.Id,
        OSM_Available_for_Processing__c = true,
        OSM_Potentially_Responsible_Account__c = ordRole2.Id,
        OSM_Hospital_Status__c = 'InPatient',
        OSM_Discharge_Date__c = System.Today(),
        OSM_Date_of_Collection__c = System.Today(),
        OSM_Storage_Retrieval_Date__c = System.Today(),
        OSM_Billing_Policy_Data_Status__c = 'Data Missing');
        
        OSM_Order_Specimen__c ordSpecimen8 = new OSM_Order_Specimen__c(Name = 'Wasda321', 
        OSM_Incoming_Tracking_Number__c = '1234', 
        OSM_Order__c = ordList2.Id,
        OSM_Available_for_Processing__c = false,
        OSM_Potentially_Responsible_Account__c = ordRole2.Id,
        OSM_Hospital_Status__c = 'InPatient',
        OSM_Discharge_Date__c = System.Today(),
        OSM_Date_of_Collection__c = System.Today(),
        OSM_Storage_Retrieval_Date__c = System.Today(),
        OSM_Billing_Policy_Data_Status__c = 'Data Available',
        OSM_Billing_Policy_Qualification_status__c = null);
        
        OSM_Order_Specimen__c ordSpecimen9 = new OSM_Order_Specimen__c(Name = 'Wasda321', 
        OSM_Incoming_Tracking_Number__c = '1234', 
        OSM_Order__c = ordList3.Id,
        OSM_Available_for_Processing__c = false,
        OSM_Potentially_Responsible_Account__c = ordRole4.Id,
        OSM_Hospital_Status__c = '',
        OSM_Discharge_Date__c = null,
        OSM_Date_of_Collection__c = null,
        OSM_Storage_Retrieval_Date__c = System.Today(),
        OSM_Billing_Policy_Data_Status__c = 'Data Missing',
        OSM_Billing_Policy_Qualification_status__c = null);
        
        OSM_Order_Specimen__c ordSpecimen3 = new OSM_Order_Specimen__c(Name = 'Wasdae', OSM_Incoming_Tracking_Number__c = '1234', OSM_Order__c = ordList.Id, OSM_Date_of_Collection__c = System.Today(), OSM_Available_for_Processing__c = true);
        
        OSM_Order_Specimen__c ordSpecimen4 = new OSM_Order_Specimen__c(Name = 'Wasdaf', OSM_Incoming_Tracking_Number__c = '1234', OSM_Order__c = ordList.Id, OSM_Date_of_Collection__c = System.Today(), OSM_Available_for_Processing__c = false, OSM_Master_Specimen_ID_text__c = 'awdsads');
        
        OSM_Order_Specimen__c ordSpecimen5 = new OSM_Order_Specimen__c(Name = 'Wasdag', OSM_Incoming_Tracking_Number__c = '1234', OSM_Order__c = ordList.Id, OSM_Available_for_Processing__c = false, OSM_Master_Specimen_ID_text__c = ordSpecimen.Id );
        List<OSM_Order_Specimen__c> ordSpecimenList = new List<OSM_Order_Specimen__c>();
        List<OSM_Order_Specimen__c> ordSpecimenList2 = new List<OSM_Order_Specimen__c>();
        ordSpecimenList.add(ordSpecimen);
        ordSpecimenList.add(ordSpecimen2);
        ordSpecimenList.add(ordSpecimen3);
        ordSpecimenList.add(ordSpecimen4);
        ordSpecimenList.add(ordSpecimen5);
        ordSpecimenList.add(ordSpecimen8);
        ordSpecimenList.add(ordSpecimen9);
        insert ordSpecimenList;
        ordSpecimenList2.add(ordSpecimen2);
        OSM_Order_Specimen__c ordSpecimen7 = new OSM_Order_Specimen__c(Name = 'Wasdag', OSM_Incoming_Tracking_Number__c = '1234', OSM_Order__c = ordList.Id, OSM_Available_for_Processing__c = false, OSM_Master_Specimen_ID_text__c = ordSpecimen.Id );
        insert ordSpecimen7;
        
        Id caseRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Specimen Arrival').getRecordTypeId();
        Id caseRecType2 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Specimen Retrieval').getRecordTypeId();
        
        Case case2 = OSM_DataFactory.createCaseWithTrackingNumber('Open', 'Package Issue', null , caseRecType2);
        
        insert case2;
        OSM_Package__c pkg = OSM_DataFactory.createPackage('1234', case2.CaseNumber + '\n\r' + 'SR Barcode Test', true);
        insert pkg;
        Case case1 = OSM_DataFactory.createCaseWithTrackingNumber('Open', 'Package Issue', pkg.Id, caseRecType);
        case1.OSM_Primary_Order__c = ordList.Id;
        case1.OSM_Tracking_Number__c = '1234';
        case1.OSM_Tracking_Number_lookup__c = pkg.Id;
        case1.OSM_Material_Return_HCO__c = ordRole3.Id;
        case1.Type='Specimen Arrival';
        //case1.OSM_Material_Return_Address_Option__c = 'Material Return Different Per Specimen';
        //insert case1;
        
        OrderItem ordItem = new OrderItem(OrderId = ordList.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, OSM_State__c = 'Active', OSM_Resulted_Order_Specimen_ID_Current__c = ordSpecimen.Id);
        insert ordItem;
        OrderItem ordItem2 = new OrderItem(OrderId = ordList2.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, OSM_State__c = 'Active', OSM_Resulted_Order_Specimen_ID_Current__c = ordSpecimen.Id);
        insert ordItem2;
        OSM_Order_Specimen_OLI__c ordSpecimenOLI = new OSM_Order_Specimen_OLI__c(OSM_Order_Specimen_ID__c = ordSpecimen.Id, OSM_Billing_policy_N_A__c = false, OSM_OLI_ID__c = ordItem2.Id);
        insert ordSpecimenOLI;
        OSM_Order_Specimen_OLI__c ordSpecimenOLI2 = new OSM_Order_Specimen_OLI__c(OSM_Order_Specimen_ID__c = ordSpecimen2.Id, OSM_Billing_policy_N_A__c = false, OSM_OLI_ID__c = ordItem2.Id);
        insert ordSpecimenOLI2;
        OSM_Order_Specimen_OLI__c ordSpecimenOLI3 = new OSM_Order_Specimen_OLI__c(OSM_Order_Specimen_ID__c = ordSpecimen8.Id, OSM_Billing_policy_N_A__c = false, OSM_OLI_ID__c = ordItem2.Id);
        insert ordSpecimenOLI3;
        OSM_Order_Specimen_OLI__c ordSpecimenOLI4 = new OSM_Order_Specimen_OLI__c(OSM_Order_Specimen_ID__c = ordSpecimen9.Id, OSM_Billing_policy_N_A__c = false, OSM_OLI_ID__c = ordItem2.Id);
        insert ordSpecimenOLI4;
        Set<Id> ordSpecimenIds = new Set<Id>();
        ordSpecimenIds.add(ordSpecimen.Id);
        Set<Id> ordSpecimenIds2 = new Set<Id>();
        ordSpecimenIds2.add(ordSpecimen2.Id);
        Set<Id> orderSpecimenOLIIds = new Set<Id>();
        ordList2.OSM_Status__c = 'Processing';
        update ordList2;
        
        orderSpecimenOLIIds.add(ordSpecimenOLI.Id);
        Test.startTest();
        
        OSM_OrderSpecimenTriggerHandler.hasEnsureUniqueOrderAndMasterSpecimenRun = false;
        OSM_OrderSpecimenTriggerHandler.hasRun = false;
        OSM_OrderSpecimenTriggerHandler.hasPopulateFirstAvailableDateRun = false;
        OSM_OrderSpecimenTriggerHandler.runUpdatePRAOnce = false;
        OSM_OrderSpecimenTriggerHandler.hasCheckReadyOrderSpecimenRun = false;
        OSM_OrderSpecimenTriggerHandler.hasCountNonReadyOrderSpecimenRun = false;
        OSM_OrderSpecimenTriggerHandler.hasAutoCloseParentRun = false;
        OSM_OrderSpecimenTriggerHandler.hasValidationBuilderRun = false;
        OSM_OrderTriggerHandler.globalIsFuture = true;  
        
        
        //OSM_OrderSpecimenTriggerHandler.populateOLISpecimenReceivedDate(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.checkReadyOrderSpecimen(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.updatePRA(ordSpecimenIds);
        OSM_OrderSpecimenTriggerHandler.updatePRA(ordSpecimenIds2);
        OSM_OrderSpecimenTriggerHandler.billingPolicyQualificationCheck(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.determineBillingPolicyQualificationStatus(ordSpecimenList2);
        OSM_OrderSpecimenTriggerHandler.postPatientReportDistibution(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.pullBillPolicyReady(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.determineBillingPolicyQualificationStatus(ordSpecimenList);
        
        insert case1;
        OSM_Order_Specimen__c ordSpecimen6 = new OSM_Order_Specimen__c(Name = 'Wasd', OSM_Incoming_Tracking_Number__c = '1234', OSM_Order__c = ordList.Id, OSM_Date_of_Collection__c = System.Today(), OSM_Available_for_Processing__c = true, OSM_First_Available_Date__c = System.Today(), OSM_Specimen_Arrival_Case__c = case1.Id);
        ordSpecimenList.add(ordSpecimen6);
        //OSM_OrderSpecimenTriggerHandler.autoCloseParent(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.associateSpecimenToCase(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.copyMaterialReturnHCO(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.runUpdatePRAOnce = false;
        OSM_OrderSpecimenTriggerHandler.onAfterInsert(ordSpecimenList);
        OSM_OrderSpecimenTriggerHandler.ensureUniqueOrderAndMasterSpecimen(ordSpecimenList);
        ordSpecimen.Name = 'dasw';
        ordSpecimen.OSM_Potentially_Responsible_Account__c = ordRole.Id;
        ordSpecimen.OSM_Order__c = ordList.Id;
        ordSpecimen.OSM_Discharge_Date__c = System.Today()+10;
        update ordSpecimen;
        
        ordSpecimenList.clear();
        ordSpecimenList.add(ordSpecimen);
        ordSpecimenList.add(ordSpecimen2);
        ordSpecimenList.add(ordSpecimen3);
        ordSpecimenList.add(ordSpecimen4);
        ordSpecimenList.add(ordSpecimen5);
        ordSpecimenList.add(ordSpecimen8);
        ordSpecimenList.add(ordSpecimen9);
        
        OSM_OrderSpecimenTriggerHandler.determineBillingPolicyQualificationStatus(ordSpecimenList);
        Test.stopTest();
    }

}