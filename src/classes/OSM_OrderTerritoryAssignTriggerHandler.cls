/*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @description: Order Territory Assignment Trigger Handler

*/
public class OSM_OrderTerritoryAssignTriggerHandler{
    public static Boolean OTARollUpRunOnce = false;
    public static Boolean hasCreateTerritoryHistoryRun = false;
    public static Boolean hasDeleteOrderShareRun = false;
    public static Boolean hasCreateOrderShareRun = false;
    public static Boolean updateNumberofTerritoriesRun = false;
    
     /*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @param: newOTA- new Order Assign List, newHTAMap - new Order Assign Map oldHTAMAP- old Order Assign Map
    @description: Before Update Method
    */
    public static void OnBeforeUpdate(List<OSM_Order_Territory_Assignment__c> newOTA, Map<ID, OSM_Order_Territory_Assignment__c> otaMapNew, Map<ID, OSM_Order_Territory_Assignment__c> otaMapOld){   
         //Set override user and override date
         
         for(OSM_Order_Territory_Assignment__c ota :newOTA){
             if(ota.OSM_Override__c && !otaMapOld.get(ota.ID).OSM_Override__c){
                 ota.OSM_Overrided_By__c = userinfo.getUserId();
                 Datetime today = Datetime.now();
                 ota.OSM_Date_Override__c= Date.parse(today.format('MM/dd/yyyy'));
             }    
         }
    }
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @param: newATT - new ota List, otaMapNew - new ota Map
    @description: After Insert Method
    */
    public static void OnAfterInsert(List<OSM_Order_Territory_Assignment__c> newOTA, Map<ID, OSM_Order_Territory_Assignment__c> otaMapNew){
        system.debug('test OTA enter aft ins');
        Set<ID> territoryIDS = new Set<ID>();
        for(OSM_Order_Territory_Assignment__c ota :newOTA){
            territoryIDS.add(ota.OSM_Territory_Name__c);         
        }      
        if(!newOTA.isEmpty() && !territoryIDS.isEmpty()){
            if(!hasCreateOrderShareRun){
                hasCreateOrderShareRun = true;
                createOrderShare(newOTA,territoryIDS); 
            }
        }
        
        if(!OTARollUpRunOnce){
            system.debug('test enter ins OTARollUpRunOnce ');
            OTARollUpRunOnce = true;
            OTARollUp(newOTA);
        }
        /*
        if(!updateNumberofTerritoriesRun){
            updateNumberofTerritoriesRun = true;
            updateNumberofTerritories(newOTA);
        }*/
    }
    
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @param: newATT - new ota List, otaMapNew - new ota Map, otaMapOld - old ota Map
    @description: After Update Method
    */
    public static void OnAfterUpdate(List<OSM_Order_Territory_Assignment__c> newOTA, Map<ID, OSM_Order_Territory_Assignment__c> otaMapNew, Map<ID, OSM_Order_Territory_Assignment__c> otaMapOld){
        system.debug('test OTA enter aft up');
        Set<ID> territoryIDS = new Set<ID>();
        Set<ID> territoryIDSDelete = new Set<ID>();
        List<OSM_Order_Territory_Assignment__c> overOTA = new List<OSM_Order_Territory_Assignment__c>();
        List<OSM_Order_Territory_Assignment__c> delOTA = new List<OSM_Order_Territory_Assignment__c>();
        for(OSM_Order_Territory_Assignment__c ota :newOTA){
            if(!ota.OSM_Override__c && otaMapOld.get(ota.ID).OSM_Override__c ){
                overOTA.add(ota);
                territoryIDS.add(ota.OSM_Territory_Name__c);         
            }
            else if(ota.OSM_Override__c && !otaMapOld.get(ota.ID).OSM_Override__c){
                delOTA.add(ota);
                territoryIDSDelete.add(ota.OSM_Territory_Name__c);     
            }
            
            //if(ota.OSM_Manual_Assignment__c && !otaMapOld.get(ota.ID).OSM_Manual_Assignment__c ){
               overOTA.add(ota);
               territoryIDS.add(ota.OSM_Territory_Name__c);         
            //}
        }    
        system.debug('*@*@*@*$$$' + newOTA.isEmpty() + ':::' + territoryIDS.isEmpty());
        if(!newOTA.isEmpty() && !territoryIDS.isEmpty()){
            if(!hasCreateOrderShareRun){
                hasCreateOrderShareRun = true;
                createOrderShare(overOTA,territoryIDS);
            }
        }
        system.debug('*@*@*@*$$$' + delOTA.isEmpty() + ':::' + territoryIDSDelete.isEmpty());
        if(!delOTA.isEmpty() && !territoryIDSDelete.isEmpty()){
            if(!hasDeleteOrderShareRun){
                hasDeleteOrderShareRun = true;
                deleteOrderShare(delOTA,territoryIDSDelete);
            }
        }
        
        if(!OTARollUpRunOnce){
           system.debug('test enter up OTARollUpRunOnce ');
            OTARollUpRunOnce = true;
            OTARollUp(newOTA);
        }
       /* 
        if(!updateNumberofTerritoriesRun){
            updateNumberofTerritoriesRun = true;
            updateNumberofTerritories(newOTA);
        } */
    }
    
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @param: delOTA- ota List, delOTAMap- ota Map
    @description: Before Delete Method
    */
    
    public static void OnBeforeDelete(List<OSM_Order_Territory_Assignment__c> delOTA, Map<ID, OSM_Order_Territory_Assignment__c> delOTAMap){
        
        Set<ID> territoryIDS = new Set<ID>();
        for(OSM_Order_Territory_Assignment__c ota :delOTA){
            territoryIDS.add(ota.OSM_Territory_Name__c);         
        }    
        if(!delOTA.isEmpty() && !territoryIDS.isEmpty()){
            if(!hasDeleteOrderShareRun){
                hasDeleteOrderShareRun = true;
                deleteOrderShare(delOTA,territoryIDS);
            }
        }
        
        if(!hasCreateTerritoryHistoryRun){
            hasCreateTerritoryHistoryRun = true;
            createTerritoryHistory(delOTA);
        }
    }
    
    /*
    @author: Daniel Quismorio
    @date: 27 Jan 2015
    @param: delOTA- ota List, delOTAMap- ota Map
    @description: After Delete Method
    */
    
    public static void OnAfterDelete(List<OSM_Order_Territory_Assignment__c> delOTA){
        system.debug('test OTA enter aft del');
         if(!OTARollUpRunOnce){
            system.debug('test enter del OTARollUpRunOnce ');
            OTARollUpRunOnce = true;
            OTARollUp(delOTA);
        }
    }
    
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @param: newATT - new ota List, terrID - territory ID Set
    @description: delete order share record
    */
    public static void deleteOrderShare(List<OSM_Order_Territory_Assignment__c> delOTA, Set<ID> terrID){
        //Retrieve child and parents/ancestors territories
        Map<Id, Set<Id>> childGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();

        for(OSM_Territory__c loopTerritory : [SELECT Id, OSM_Parent_Territory__c FROM OSM_Territory__c WHERE OSM_Parent_Territory__c != null LIMIT 25000]){
            childParentMap.put(loopTerritory.Id,loopTerritory.OSM_Parent_Territory__c);
            parentChildMap.put(loopTerritory.OSM_Parent_Territory__c,loopTerritory.Id);
            childGroupMap.put(loopTerritory.Id, new Set<Id>{loopTerritory.OSM_Parent_Territory__c});
        }
        for(Integer counter = 0; counter < 50; counter++){
           for(Id loopChildId : childGroupMap.keySet()){
               for(Id loopParentId : childGroupMap.get(loopChildId)){
                   if(childParentMap.get(loopParentId) != null){
                        childGroupMap.get(loopChildId).add(childParentMap.get(loopParentId));
                   }
                }
             }
        }
        for(OSM_Order_Territory_Assignment__c loopAssignment : delOTA){
            if(childGroupMap.containsKey(loopAssignment.OSM_Territory_Name__c)){
                terrID.addAll(childGroupMap.get(loopAssignment.OSM_Territory_Name__c));
            }
        }
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = [SELECT OSM_Sales_Rep__c, OSM_Sales_Rep__r.IsActive, OSM_Order_Access_Level__c, OSM_Territory_ID__c  from OSM_Territory_Sales_Rep_Assignment__c where OSM_Territory_ID__c IN: terrID AND OSM_Inactive__c = false AND OSM_Sales_Rep__r.IsActive = true];
        //Prepare OA List
        List<OrderAccessLevel> oaList = prepareOAList(tsraList, delOTA, childGroupMap);
        Set<ID> userIDSet = new Set<ID>();
        Set<ID> ordIDSet = new Set<ID>();
        //prepare wrapper object
        for(OrderAccessLevel oal : oaList){
            userIDSet.add(oal.userID);
            ordIDSet.add(oal.ordID);
        }
         //Prepare order share list to be deleted
        List<OrderShare> osList = [SELECT ID, OrderAccessLevel, UserOrGroupId, OrderId  from OrderShare where UserOrGroupId IN: userIDSet AND OrderId IN: ordIDSet AND RowCause = 'Manual'];
        
        if(osList != null && !osList.isEmpty()){
            try{
                delete osList;
            }
            catch(Exception ex){
                delOTA[0].addError(ex.getMessage());
            }
        }
    }
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @param: newATT - new ota List, terrID - territory ID Set
    @description: prepare and create order share record

    */    
    public static void createOrderShare(List<OSM_Order_Territory_Assignment__c> newOTA, Set<ID> terrID){
        //Retrieve child and parents/ancestors territories
        
        Map<Id, Set<Id>> childGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();

        for(OSM_Territory__c loopTerritory : [SELECT Id, OSM_Parent_Territory__c FROM OSM_Territory__c WHERE OSM_Parent_Territory__c != null LIMIT 25000]){
            childParentMap.put(loopTerritory.Id,loopTerritory.OSM_Parent_Territory__c);
            parentChildMap.put(loopTerritory.OSM_Parent_Territory__c,loopTerritory.Id);
            childGroupMap.put(loopTerritory.Id, new Set<Id>{loopTerritory.OSM_Parent_Territory__c});
        }
        for(Integer counter = 0; counter < 50; counter++){
           for(Id loopChildId : childGroupMap.keySet()){
               for(Id loopParentId : childGroupMap.get(loopChildId)){
                   if(childParentMap.get(loopParentId) != null){
                        childGroupMap.get(loopChildId).add(childParentMap.get(loopParentId));
                   }
                }
             }
        }
        for(OSM_Order_Territory_Assignment__c loopAssignment : newOTA){
            if(childGroupMap.containsKey(loopAssignment.OSM_Territory_Name__c)){
                terrID.addAll(childGroupMap.get(loopAssignment.OSM_Territory_Name__c));
            }
        }
        //Prepare Sales Rep
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = [SELECT OSM_Sales_Rep__c, OSM_Sales_Rep__r.IsActive, OSM_Order_Access_Level__c, OSM_Territory_ID__c from OSM_Territory_Sales_Rep_Assignment__c where OSM_Territory_ID__c IN: terrID AND OSM_Inactive__c = false AND OSM_Sales_Rep__r.IsActive = true];   
        List<OrderShare> osList = new List<OrderShare>();   
        //prepare wrapper object
        List<OrderAccessLevel> oaList = prepareOAList(tsraList, newOTA, childGroupMap);
        //Prepare order share list to be inserted
        if(oaList != null && !oaList.isEmpty()){
            for(OrderAccessLevel oal: oaList){
                 osList.add(new OrderShare(OrderId = oal.ordID, OrderAccessLevel = oal.ordAccess, UserorGroupId = oal.userId));     
            }
        }

        Set<Id> orderShareOrderIds = new Set<Id>();
        Set<Id> orderShareGroupIds = new Set<Id>();
        String queryOrderShare = 'SELECT Id, OrderId, UserOrGroupId FROM OrderShare WHERE ';
        for(OrderShare loopShare : osList){
            if(loopShare.orderId != null && loopShare.UserOrGroupId != null){
                orderShareOrderIds.add(loopShare.orderId);
                orderShareGroupIds.add(loopShare.UserOrGroupId);
            }
        }
        if(orderShareOrderIds.size() > 0 && orderShareGroupIds.size() > 0){
            queryOrderShare += '(OrderId IN :orderShareOrderIds AND UserOrGroupId IN :orderShareGroupIds AND RowCause != \'Manual\')';
        }
        
        Set<OrderShare> toInsert = new Set<OrderShare>();
        for(OrderShare loopShare : osList){
            if(loopShare.OrderId != null && loopShare.UserorGroupId != null){
                toInsert.add(loopShare);
            }
        }
        
        //Collect Query AccountShare
        Map<String,OrderShare> orderShareMap = new Map<String,OrderShare>();
        if(orderShareOrderIds.size() > 0 && orderShareGroupIds.size() > 0){
            for(OrderShare loopOrderShare : database.query(queryOrderShare)){
                orderShareMap.put(loopOrderShare.OrderId + ':::' + loopOrderShare.UserOrGroupId, loopOrderShare);
            }
        }
        
        Set<OrderShare> toRemove = new Set<OrderShare>();
        for(OrderShare loopShare : toInsert){
            //Bug 39247:Exceed query length: Needs to be addressed before full copy org Fix
            if(orderShareMap.containsKey(loopShare.OrderId + ':::' + loopShare.UserOrGroupId)){
                toRemove.add(loopShare);
            }
        }
        
        for(OrderShare loopShare : toRemove){
            toInsert.remove(loopShare);
        }
        
        osList = new List<OrderShare>();   
        for(OrderShare loopShare : toInsert){
            system.debug('test loopShare ' + loopShare);
            if(loopShare.OrderId != null && loopShare.UserorGroupId != null){
                osList.add(loopShare);
            }
        }
        system.debug('test osList size ' + osList.size());
        
        if(osList != null && !osList.isEmpty()){
            try{
                // AC 4/7/2015: Added check to ignore portal users
                // Needed because of SFDC bug where OrderRole is not accessible by portal users                
                System.debug('****GHI_Portal_Utilities.isPortalUser: ' + GHI_Portal_Utilities.isPortalUser());
                if (!GHI_Portal_Utilities.isPortalUser())
                {
                    insert osList;
                }
            }
            catch(Exception ex){
                String errorMsg = Label.OSM_InsufficientShareError;
                if(ex.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, insufficient access rights on cross-reference id: []')){
                    newOTA[0].addError(errorMsg);    
                }
                else{
                    newOTA[0].addError(ex.getMessage());
                }
            }
        }    
        system.debug('test osList before insert ' + osList);
    }
    
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @param: tsraList - Territory Sales Rep List newATT - new ota List
    @description: prepare wrapper object
    */
    public static List<OrderAccessLevel> prepareOAList(List<OSM_Territory_Sales_Rep_Assignment__c> tsraList, List<OSM_Order_Territory_Assignment__c> newOTA, Map<ID, Set<ID>> childMap){
        List<OrderAccessLevel> oaList = new List<OrderAccessLevel>();   
        //Prepare list for user id, territoryID, order access and order id
        for(OSM_Territory_Sales_Rep_Assignment__c trsa: tsraList){
            for(OSM_Order_Territory_Assignment__c ota: newOTA){
                if(trsa.OSM_Sales_Rep__r.IsActive == true){
                if(trsa.OSM_Territory_ID__c == ota.OSM_Territory_Name__c){
                    oaList.add(new OrderAccessLevel(trsa.OSM_Sales_Rep__c, ota.OSM_OrderName__c, trsa.OSM_Order_Access_Level__c));
                }
                }
            }
        }    
        if(!childMap.isEmpty()){
            for(OSM_Territory_Sales_Rep_Assignment__c tsra: tsraList){
                for(ID loopId: childMap.keySet()){
                    if(childMap.get(loopId).contains(tsra.OSM_Territory_ID__c)){
                        for(OSM_Order_Territory_Assignment__c ota: newOTA){
                            if(tsra.OSM_Sales_Rep__r.IsActive == true){
                            if(loopId == ota.OSM_Territory_Name__c){
                                oaList.add(new OrderAccessLevel(tsra.OSM_Sales_Rep__c, ota.OSM_OrderName__c, tsra.OSM_Order_Access_Level__c));
                            }
                            }
                        }  
                    } 
                }       
            }
        }
        return oaList;
    }  
    /*
    @author: Michiel Patricia M. Robrigado
    @date: 21 Jan 2015
    @description: Order Share data list
   */    
    public class OrderAccessLevel{
        public ID userID {get; set;}
        public ID ordID {get; set;}
        public String ordAccess {get; set;} 
         /*
        @author: Michiel Patricia M. Robrigado
        @date: 21 Jan 2015
        @param: user- user id, ord- ord id, access- access value
        @description: constructor
         */   
        public OrderAccessLevel(ID user, ID ord, String access){
            userID = user;
            ordID = ord;
            ordAccess = access;           
        } 
    }
    
    /*
    @author: Daniel Quismorio
    @date: 23 Jan 2015
    @description: Create Territory History records
   */   
    public static void createTerritoryHistory(List<OSM_Order_Territory_Assignment__c> newOTA){
        Set<Id> territoryIDS = new Set<Id>();
        for(OSM_Order_Territory_Assignment__c otaLoop : newOTA){
            territoryIDS.add(otaLoop.OSM_Territory_Name__c);
        }
        
        Map<Id, OSM_Territory__c> mapTerritoryInfo = new Map<Id, OSM_Territory__c>();
        for(OSM_Territory__c terrLoop : [Select Id, Name,OSM_Area__c, OSM_Business_Unit__c, OSM_Description__c, OSM_Division__c, OSM_GHI_Region_ID__c, OSM_Global__c,  
                                        OSM_International_Area__c, OSM_Number_of_Parents__c, Partner_Account__c, Partner_Account_Id__c, OSM_Parent_Territory__c, 
                                        OSM_Region__c, OSM_Territory_Franchise__c, OSM_Territory_ID__c, OSM_Territory_Market__c, OSM_Territory_Type__c,
                                        OSM_Effective_Ending_Date__c, OSM_Effective_Start_Date__c From OSM_Territory__c Where Id IN: territoryIDS]){
            mapTerritoryInfo.put(terrLoop.Id, terrLoop);
        }
        
        List<OSM_Territory_History__c> insertTerritoryHistory = new List<OSM_Territory_History__c>();
        for(OSM_Order_Territory_Assignment__c otaLoop : newOTA){
            OSM_Territory_History__c newTH = new OSM_Territory_History__c();
            //newTH.OSM_Account_Name__c = otaLoop.
            if(mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c) != null){
            newTH.OSM_Area__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Area__c;
            newTH.OSM_Business_Unit__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Business_Unit__c;
            newTH.OSM_Deleted_Date__c = system.today();
            newTH.OSM_Description__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Description__c;
            newTH.OSM_Division__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Division__c;
            newTH.OSM_Effective_Ending_Date__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Effective_Ending_Date__c;
            newTH.OSM_Effective_Start_Date__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Effective_Start_Date__c;
            newTH.OSM_GHI_Region_ID__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_GHI_Region_ID__c;
            newTH.OSM_Global__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Global__c;
            //newTH.OSM_HCP_Name__c = otaLoop.
            newTH.OSM_International_Area__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_International_Area__c;
            newTH.OSM_Number_of_Parents__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Number_of_Parents__c;
            newTH.OSM_OrderName__c = otaLoop.OSM_OrderName__c;
            newTH.OSM_Partner_Account__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).Partner_Account__c;
            //newTH.OSM_Partner_Account_ID__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).Partner_Account_Id__c;
            newTH.OSM_Parent_Territory__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Parent_Territory__c;
            newTH.OSM_Region__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Region__c;
            newTH.OSM_Territory_name__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).Id;
            newTH.OSM_Territory_Franchise__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Territory_Franchise__c;
            newTH.OSM_Territory_ID__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Territory_ID__c;
            newTH.OSM_Territory_Market__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Territory_Market__c;
            newTH.OSM_Territory_Type__c = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).OSM_Territory_Type__c;
            newTH.OSM_Territory_Nametext__c  = mapTerritoryInfo.get(otaLoop.OSM_Territory_Name__c).Name;
            insertTerritoryHistory.add(newTH);
            }
        }
        
        if(insertTerritoryHistory.size()>0){
            insert insertTerritoryHistory;
        }
    }
    
    /*
    @author: Daniel Quismorio
    @date: 27 Jan 2015
    @description: Trigger Roll up for OTA Count
   */  
    public static void OTARollUp(list<OSM_Order_Territory_Assignment__c> newOTAList){
   
        Set<Id> ordrIds = new Set<Id>();
        for(OSM_Order_Territory_Assignment__c otaLoop: newOTAList){
            ordrIds.add(otaLoop.OSM_OrderName__c);
        }
        
        map<Id, Integer> otaCount = new map<Id, Integer>();
        for(AggregateResult ar: [Select OSM_OrderName__c, Count(Id) From OSM_Order_Territory_Assignment__c Where OSM_OrderName__c IN: ordrIds Group BY OSM_OrderName__c]){
            otaCount.put((Id)ar.get('OSM_OrderName__c'),(Integer)ar.get('expr0'));
        }
         
     /*   map<Id, Integer> otaCountOver = new map<Id, Integer>();
        for(AggregateResult ar: [Select OSM_OrderName__c, Count(Id) From OSM_Order_Territory_Assignment__c Where OSM_OrderName__c IN: ordrIds   Group BY OSM_OrderName__c]){
            otaCountOver.put((Id)ar.get('OSM_OrderName__c'),(Integer)ar.get('expr0'));
        }
      */  
        list<Order> otaListUpdate = new list<Order>();
       // list<Order> otaListUpdateOverride = new list<Order>();
         //refactored to eliminate unnecessary query - Paul Wittmeyer 7/3/2015
        List<Order> orderList = new List<Order>();
        for(Id oID : ordrIds){
            Order o = new Order(Id = oId);
            orderList.add(o);
        }
        //for(Order o: [Select Id, OSM_Order_Territory_Assignment_Count__c From Order Where Id IN: ordrIds]){
        for(Order o : orderlist){
            Integer otaCounts = otaCount.get(o.Id);
         //   system.debug('*****otaCounts' + otaCounts);
            o.OSM_Order_Territory_Assignment_Count__c = otaCounts == null ? 0 : otaCounts;
            o.OSM_Number_of_Territories__c = o.OSM_Order_Territory_Assignment_Count__c;
            system.debug('test o.OSM_Order_Territory_Assignment_Count__c ' + o.OSM_Order_Territory_Assignment_Count__c);
            //o.OSM_Number_of_Territories__c = otaCounts;
            otaListUpdate.add(o);
        }
        //for(Order o: [Select Id, OSM_Number_of_Territories__c From Order Where Id IN: ordrIds ]){
    /*    for(Order o : orderList){
            Integer otaCounts = otaCountOver.get(o.Id);
            system.debug('*****otaCounts' + otaCountOver);
            o.OSM_Number_of_Territories__c = otaCountOver == null ? 0 : otaCounts;
            otaListUpdateOverride.add(o);
        } */
        try{
            update otaListUpdate;
        }
        catch(Exception ex){
            newOTAList[0].addError(ex);    
        }
       // update otaListUpdateOverride;
    }
    
     /*
    @author: patrick lorilla
    @date: 16 June 2015
    @description: Update Number of Territories
    @param: newAAT- new  territory assignmenbts
   */   
    public static void updateNumberofTerritories(List<OSM_Order_Territory_Assignment__c> newOTA){
        Set<ID> ordIds = new Set<ID>();
        //Set<ID> aatIDS= new Set<ID>();
        Map<Id, Integer> ordTerrCtrVal = new Map<Id, Integer>();
        for(OSM_Order_Territory_Assignment__c ota: newOTA){ 
            //ordIds.add(ota.OSM_Order__c);
            ordIds.add(ota.OSM_OrderName__c);
        }
        
        /*
        List<OSM_Order_Territory_Assignment__c> ordList = [SELECT ID, OSM_OrderName__c from OSM_Order_Territory_Assignment__c where OSM_OrderName__c IN: ordIds ];
        for(OSM_Order_Territory_Assignment__c ota: ordList){ 
            Integer ctr = 1;
            if(ordTerrCtrVal.containsKey(ota.OSM_OrderName__c)){
                ctr = ordTerrCtrVal.get(ota.OSM_OrderName__c);
                ++ctr;
            }
            ordTerrCtrVal.put(ota.OSM_OrderName__c, ctr);
        }
        
        List<Order> ordUpdate = new List<Order>();
        for(ID oid: ordIds){
            if(ordTerrCtrVal.containsKey(oid)){
                Order ord = new Order(Id = oid, OSM_Number_of_Territories__c = ordTerrCtrVal.get(oid));
                ordUpdate.add(ord);
            }
        }*/
        Map<Id, Order> ordMap =new Map<Id, Order>([Select Id, (Select Id from Order_Territory_Assignments__r) from Order where Id IN: ordIds]);
        
       // for(OSM_Order_Territory_Assignment__c ota: newOTA){ 
       //     if(ordMap.containsKey(ota.OSM_Order__c)){
       //         ordMap.get(ota.OSM_Order__c).OSM_Number_of_Territories__c = ordMap.get(ota.OSM_Order__c).Order_Territory_Assignments__r.size();
       //     }
        List<Order> updateOrders = new List<Order>();
        for(Id ordId: ordMap.keySet()) {
            updateOrders.add(new Order(Id=ordId, OSM_Number_of_Territories__c=ordMap.get(ordId).Order_Territory_Assignments__r.size()));
        }
       
        
        try{
        //    update ordMap.values();
              update updateOrders;
        }
        catch(Exception ex){
            newOTA[0].addError(ex.getMessage());
        }
    }
}