/**
 * File Info
 * ----------------------------------
 * @filename        OSM_Order_BannerExt.cls
 * @created         09.JAN.2015
 * @author          Kristian Vegerano
 * @description     Extension class for OSM_Order_Banner page. 
 * @history         09.JAN.2015 - Kristian Vegerano - Created  
 */  
public class OSM_Order_BannerExt{
    private Order pageOrder;
    public String missingDataDetails {get;set;}
    public Boolean hasMissingData {get;set;}
    
    /**
     * @author         Kristian Vegerano
     * @description    Constructor. 
     * @history        09.JAN.2015 - Kristian Vegerano - Created  
     */
    public OSM_Order_BannerExt(ApexPages.StandardController stdController){
        pageOrder = (Order)stdController.getRecord();
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that scans the fields to be monitored for missing data field. 
     * @history        09.JAN.2015 - Kristian Vegerano - Created  
     */
    public void scanMissingFields(){
        Set<String> includedType = new Set<String>();
        List<Case> missingCases = new List<Case>();
        missingCases = [SELECT Id, Type, Status
                        FROM Case 
                        WHERE (OSM_Primary_Order__c = :pageOrder.Id AND Type NOT IN ('Problem', 'Feature Request', 'Question','Pre-Billing','Billing','General') AND IsClosedOnCreate = False AND 
                              Status NOT IN ('Communication Sent', 'Closed') )];
                     //   OR
                     //   (OSM_Primary_Order__c = :pageOrder.Id AND RecordType.DeveloperName = 'OSM_Specimen_Retrieval' AND Status NOT IN ('Communication Sent', 'Closed'))];
          
                    // WHERE (OSM_Primary_Order__c = :pageOrder.Id AND Type IN ('Missing Data', 'Clinical Experience', 'Node Status Determination') AND Status != 'Closed') OR    
        missingDataDetails = '';
        hasMissingData = false;
        if(missingCases.size() > 0){
            hasMissingData = true;
            for(Case loopCase : missingCases){
                if(!includedType.contains(loopCase.Type)){
                    includedType.add(loopCase.Type);
                       missingDataDetails = missingDataDetails + loopCase.Type + ': Status = ' + loopCase.Status + ' | ';
                }
            }
            missingDataDetails = missingDataDetails.left(missingDataDetails.length() - 2);
        }
    }
}