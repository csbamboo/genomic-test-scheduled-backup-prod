/*
@author: David E. Catindoy
@date: 26 January 2015
@description: Test OSM_Order_BannerExt
@history: 30 January 2015 - Updated (David E. Catindoy)

*/
@isTest
private class OSM_Order_BannerExt_Test {
    
    static testMethod void methodOne() {

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Role_Trigger__c = false;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Validation_Builder__c = false;
        insert triggerSwitch;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Generic').getRecordTypeId();
        //Id recTypeOrdrId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Work Order').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        account.RecordTypeId = HCORecType;
        insert account;

        Contact contact = OSM_DataFactory.createContact('David','Catindoy', PatientRecType);
        contact.AccountId = account.Id;
        insert contact;
        Contact contact1 = OSM_DataFactory.createContact(1, account.Id, (Id) HCPRecType);
        contact1.AccountId = account.Id;
        contact1.Fax = '5402240008';
        insert contact1;

        Order workOrder = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        workOrder.PriceBook2Id = pb2Standard;
        insert workOrder;

        Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'New');
        //Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Processing');
        order.OSM_Order_Data_Confirmation_Needed__c = true;
        order.OSM_Order_Data_Confirmation_Details__c = null;
        order.OSM_Patient_Data_Confirmation_Needed__c = true;
        order.OSM_Patient_Data_Confirmation_Details__c = null;
        order.Pricebook2Id = pb2Standard;
        //order.OSM_Order__c = workOrder.Id;
        order.OSM_Patient_DOB__c = System.today();
        order.OSM_Patient_Gender__c = 'Male';
        order.OSM_Signature_Date__c = System.today().addDays(-5);
        insert order;

        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
    
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard, 7);
        insert pbe;

        OrderItem ordrItm = OSM_DataFactory.createOrderItem(order.Id, pbe.Id, 7, 7, System.today()+1);
        ordrItm.OSM_NCCN_Risk_Category__c = 'Unset';
        insert ordrItm;

        List<OSM_Order_Role__c> orderRoleList = new List<OSM_Order_Role__c>();
        for(Integer i = 0; i<2; i++){
          orderRoleList.add(OSM_DataFactory.createOrderRole(order.Id, account.Id, contact1.Id));
          orderRoleList[i].OSM_Order_Role_Data_Confirmation_Needed__c = true;
          orderRoleList[i].OSM_Order_Role_Data_Confirmation_Details__c = null;
          if(i==0){
                orderRoleList[i].OSM_Account_Fax__c = '1234';
                orderRoleList[i].OSM_Role__c = 'Specimen Submitting';
            } else {
                orderRoleList[i].OSM_Account_Name__c = account.Name;
                orderRoleList[i].OSM_Account_Postal_Address__c = '1226';
                orderRoleList[i].OSM_Account_Country__c = 'USA';
                orderRoleList[i].OSM_Contact_Full_Name__c = 'David Catindoy';
                orderRoleList[i].OSM_Account_Fax__c = '1234';
                orderRoleList[i].OSM_Role__c = 'Ordering';
            }
        } 
        insert orderRoleList;
        
        Case case1 = OSM_DataFactory.createCase('Open', 'Generic', order.Id, null, caseRecTypeId);
        insert case1;
        PageReference pageRef = Page.OSM_Order_Banner;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController standardController = new ApexPages.StandardController(order);
        OSM_Order_BannerExt controller = new OSM_Order_BannerExt(standardController);

        Test.startTest();
        System.debug('Order: '+order);
        controller.scanMissingFields();
        order.OSM_Status__c = 'Processing';
        order.OSM_Triage_Outcome__c = 'Other';
        update order;

        orderRoleList[1].OSM_Account__c = null;
        update orderRoleList[1];
        controller.scanMissingFields();

        orderRoleList[1].OSM_Account__c = account.Id;
        orderRoleList[1].OSM_Contact__c = null;
        update orderRoleList[1];
        controller.scanMissingFields();

        order.OSM_Patient_Gender__c = null;
        update order;
        orderRoleList[1].OSM_Contact__c = contact1.Id;
        orderRoleList[1].OSM_Order_Role_Data_Confirmation_Details__c = 'Confirm';
        update orderRoleList[1];
        controller.scanMissingFields();

        order.OSM_Patient_Gender__c = 'Male';
        update order;
        controller.scanMissingFields();

        order.OSM_Order_Data_Confirmation_Details__c = 'Confirm';
        update order;
        orderRoleList[1].OSM_Order_Role_Data_Confirmation_Details__c = null;
        //orderRoleList[0].OSM_Account_Fax__c = null;
        update orderRoleList;
        controller.scanMissingFields();

        orderRoleList[0].OSM_Order_Role_Data_Confirmation_Details__c = 'Confirm';
        orderRoleList[0].OSM_Account_Fax__c = '12345';
        update orderRoleList[0];
        controller.scanMissingFields();

        order.OSM_Order_Data_Confirmation_Details__c = null;
        update order;
        controller.scanMissingFields();

        ordrItm.OSM_NCCN_Risk_Category__c = null;
        update ordrItm;
        controller.scanMissingFields();

        order.OSM_Patient_Data_Confirmation_Details__c = 'Confirm';
        update order;
        controller.scanMissingFields();

        Test.stopTest();

        delete ordrItm;
        controller.scanMissingFields();

        order.OSM_Patient_Data_Confirmation_Details__c = null;
        update order;
        controller.scanMissingFields();

        delete orderRoleList;
        controller.scanMissingFields();

        //System.assert(controller.hasMissingData);
        //System.assert(!controller.missingDataFields.equals('Missing Data: '));
    }
}