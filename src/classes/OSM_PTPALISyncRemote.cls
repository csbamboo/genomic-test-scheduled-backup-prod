/*
    @author: patrick lorilla
    @description: PTP and ALI Sync Remote Class
    @date: 6/7/2015

*/

global class OSM_PTPALISyncRemote {
    /*
    @author: patrick lorilla
    @description: PTP and ALI Sync 
    @param: agreement Id and the account Id
    @date: 6/7/2015

   */
    webservice static void ptpALISync(Id agreementId, Id accountId){
         Account myAcct = [SELECT OSM_Payor_Category__c, CurrencyIsoCode from Account where Id =: accountId];
         List<Apttus__AgreementLineItem__c> aliList = [SELECT Id, GHI_CPQ_Price_Method__c, GHI_CPQ_Billing_Cycle__c, GHI_CPQ_Pricing_Schema__c, Active_Rate__c, Apttus__ProductId__c, Apttus__ProductId__r.Name, GHI_CPQ_Billing_Category__c, GHI_CPQ_CPT_Code__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  Apttus_CMConfig__StartDate__c, GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus__ListPrice__c FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c =: agreementId];
         Set<Id> aliIds = new Set<Id>();
         List<OSM_Payor_Test_Price__c> dummyPTPs = new List<OSM_Payor_Test_Price__c>();
         Set<Id> takenaliIds = new Set<Id>();
         Set<Id> dupProdIds = new Set<Id>();
         
         Map<String, List<Apttus__AgreementLineItem__c>> ProdALIMap = new Map<String, List<Apttus__AgreementLineItem__c>>();
         for(Apttus__AgreementLineItem__c ali: aliList){
            aliIds.add(ali.Id);    
         }
         List<OSM_Payor_Test_Price__c> ptpList = [SELECT OSM_Agreement_ID__c, OSM_Billing_Cycle__c, OSM_Billing_Category__c, OSM_Assay_Score__c, CurrencyISOCode, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c =: accountId AND OSM_Agreement_ID__c =: agreementId AND OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c  =: agreementId]; 
         for(OSM_Payor_Test_Price__c ptp: ptpList){
            takenaliIds.add(ptp.OSM_Agreement_Line_Item_ID__c);    
         }
         //for(OSM_Payor_Test_Price__c ptp: ptpList){
            for(Apttus__AgreementLineItem__c ali: aliList){
                if(!takenaliIds.contains(ali.Id)){
                    List<Apttus__AgreementLineItem__c> paALIs = new List<Apttus__AgreementLineItem__c>();
                    if(ProdALIMap.containsKey(ali.Apttus__ProductId__r.Name)){
                        paALIs =  ProdALIMap.get(ali.Apttus__ProductId__r.Name);
                    }   
                    
                    paALIs.add(ali);
                    ProdALIMap.put(ali.Apttus__ProductId__r.Name, paALIs);
                }
            }
         //}
         Set<ID> dummyaliIds = new Set<ID>();
         Map<String, Apttus__AgreementLineItem__c> dummyaliMap = new Map<String, Apttus__AgreementLineItem__c>();
         Map<String, Id> prodIdMap = new Map<String,Id>();
         List<String> productList = new List<String>();
         for(String str: Label.OSM_Product_List.split('::')){
            if(str.length() > 0){
                productList.add(str);  
            }
        }
         for(Product2 prod: [SELECT Id, Name from Product2 Where Name IN: productList]){
            prodIdMap.put(prod.Name, prod.Id);    
         }
         Apttus__APTS_Agreement__c dummyAgreement = [SELECT ID, RecordTypeId, Apttus__Subtype__c from Apttus__APTS_Agreement__c where Name =: Label.OSM_Dummy_Agreement_Name];
         for(Apttus__AgreementLineItem__c ali: [SELECT Id, GHI_CPQ_Price_Method__c, Apttus__ProductId__r.Name, GHI_CPQ_Pricing_Schema__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, GHI_CPQ_CPT_Code__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus_CMConfig__StartDate__c, Apttus__ListPrice__c, GHI_CPQ_Billing_Cycle__c, GHI_CPQ_Billing_Category__c, CurrencyISOCode FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c =: dummyAgreement.Id]){
            if(!dummyAliMap.containsKey(ali.Apttus__ProductId__r.Name)){
                dummyAliMap.put(ali.Apttus__ProductId__r.Name, ali);  
                dummyaliIds.add(ali.Id);
            }
         }
         
          //8.7.2015 patrick l fix (allow price book entries)
        String pbname = myAcct.CurrencyIsoCode + ' ' + myAcct.OSM_Payor_Category__c;
        List<PriceBookEntry> pbEntList = [SELECT Name, CurrencyIsoCode, Pricing_Category__c, UnitPrice, Pricebook2.Name, Product2.Name, ProductCode from PriceBookEntry where Pricing_Category__c =: myAcct.OSM_Payor_Category__c AND Pricebook2.Name =: pbname];
        Map<String, PriceBookEntry> pbTestMap = new Map<String, PriceBookEntry>();
        for(PriceBookEntry pbe: pbEntList){
            pbTestMap.put(pbe.Name, pbe);
        }
        
        List<RecordType> rtList = [SELECT Name from RecordType where ID =: dummyAgreement.RecordTypeId LIMIT 1];
        String rtname = '';
        if(!rtList.isEmpty()){
            rtname = rtList[0].Name;
        }
        try{
             for(String prod: ProdALIMap.keySet()){
                 for(Apttus__AgreementLineItem__c ali :ProdALIMap.get(prod)){
                    OSM_Payor_Test_Price__c ptpDummy1 = new OSM_Payor_Test_Price__c(OSM_Payor__c = accountId,
                                                                                                OSM_Test__c = prodIdMap.get(prod),
                                                                                                OSM_Start_Date__c = dummyAliMap.get(prod).Apttus_CMConfig__StartDate__c,
                                                                                                OSM_End_Date__c =  dummyAliMap.get(prod).Apttus_CMConfig__EndDate__c,
                                                                                                OSM_Agreement_ID__c =  dummyAgreement.Id ,
                                                                                                OSM_Agreement_Line_Item_ID__c= dummyAliMap.get(prod).Id,
                                                                                                OSM_Agreement_Record_Type__c= rtname,
                                                                                                OSM_Agreement_Sub_Type__c =  dummyAgreement.Apttus__Subtype__c,
                                                                                                //OSM_Net_Price__c =    dummyAliMap.get(prod).Apttus__NetPrice__c,
                                                                                                OSM_List_Price__c =   pbTestMap.get(prod).UnitPrice,//dummyAliMap.get(prod).Apttus__ListPrice__c,
                                                                                                OSM_Price_Method__c =   dummyAliMap.get(prod).GHI_CPQ_Price_Method__c,
                                                                                                OSM_Pricing_Schema__c =   dummyAliMap.get(prod).GHI_CPQ_Pricing_Schema__c,
                                                                                                OSM_Quantity_End_Range__c =  dummyAliMap.get(prod).GHI_CPQ_End_Range__c,
                                                                                                OSM_Quantity_Start_Range__c =   dummyAliMap.get(prod).GHI_CPQ_Start_Range__c,
                                                                                                OSM_Assay_Score__c =   dummyAliMap.get(prod).GHI_CPQ_Assay_Score__c,
                                                                                                //OSM_Criteria__c = agreementliList[a].GHI_CPQ_Criteria__c,
                                                                                                CurrencyISOCode =   myAcct.CurrencyIsoCode,//dummyAliMap.get(prod).CurrencyISOCode,
                                                                                                OSM_Expected_Rate__c = dummyAliMap.get(prod).GHI_CLM_Expected_Rate__c,
                                                                                                OSM_Billing_Category__c =  dummyAliMap.get(prod).GHI_CPQ_Billing_Category__c,
                                                                                                OSM_CPT_Code__c =  dummyAliMap.get(prod).GHI_CPQ_CPT_Code__c,
                                                                                                OSM_Newly_Generated__c = true,
                                                                                                OSM_Billing_Cycle__c =  dummyAliMap.get(prod).GHI_CPQ_Billing_Cycle__c
                                                                                             ); 
                        dummyPTPs.add(ptpDummy1);
                                                                            
                 }
             }
         
             if(!dummyPTPs.isEmpty()){
                insert dummyPTPs;
             }
        }
        catch(Exception ex){
            System.debug(ex);
        }
         System.debug('\n\n\n DUMMYPTPS'+dummyPTPs);
         System.debug('\n\n\n TAKENALIIDSVAL'+takenaliIds);
         aliList = [SELECT Id, GHI_CPQ_Billing_Cycle__c, GHI_CPQ_Pricing_Schema__c, Active_Rate__c, Apttus__ProductId__c, Apttus__ProductId__r.Name, GHI_CPQ_Billing_Category__c, GHI_CPQ_CPT_Code__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  Apttus_CMConfig__StartDate__c, GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus__ListPrice__c FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c =: agreementId AND ID NOT IN: takenaliIds];
         ptpList = [SELECT OSM_Agreement_ID__c, OSM_Billing_Cycle__c, OSM_CPT_Code__c, OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c, OSM_Future_Effective__c, OSM_Billing_Category__c, OSM_Assay_Score__c, CurrencyISOCode, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c =: accountId];
         Map<Id, Apttus__AgreementLineItem__c> ptpMapAllAgreement = new Map<Id, Apttus__AgreementLineItem__c>([SELECT ID FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c =: agreementId]);
         Map<Id, OSM_Payor_Test_Price__c> ptpMapDelete = new Map<Id, OSM_Payor_Test_Price__c>();
         System.debug('\n\n\n NEWALILIST'+aliList);
         Set<ID> aliCheckDone = new  Set<ID>();
         Set<ID> ptpcheckDone = new Set<ID>();
         Map<Id, OSM_Payor_Test_Price__c> ptpMapExclude = new Map<Id, OSM_Payor_Test_Price__c>();
         Map<String, List<OSM_Payor_Test_Price__c>> ProdPTPMap = new Map<String, List<OSM_Payor_Test_Price__c>>();
         
         /*delete ali that is synced to a ptp and click this button
         List<OSM_Payor_Test_Price__c> ptpNonDummy = [SELECT OSM_Agreement_ID__c, OSM_Billing_Cycle__c, OSM_Billing_Category__c, OSM_Assay_Score__c, CurrencyISOCode, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c =: accountId AND OSM_Agreement_ID__r.Name !=: Label.OSM_Dummy_Agreement_Name]; 
         Map<ID, Apttus__AgreementLineItem__c> aliMap = new Map<ID, Apttus__AgreementLineItem__c>([SELECT Id, GHI_CPQ_Billing_Cycle__c, GHI_CPQ_Pricing_Schema__c, Active_Rate__c, Apttus__ProductId__c, Apttus__ProductId__r.Name, GHI_CPQ_Billing_Category__c, GHI_CPQ_CPT_Code__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  Apttus_CMConfig__StartDate__c, GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus__ListPrice__c FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c =: agreementId]);
         System.debug('\n\n\n NONDUMMY' + ptpNonDummy);
         System.debug('\n\n\n NONDUMMYALIKEYSET' + aliMap.keySet());
         System.debug('\n\n\n NONDUMMYALIVAL' + aliMap.values());
         for(OSM_Payor_Test_Price__c ptp: ptpNonDummy){
             if(!aliMap.containsKey(ptp.OSM_Agreement_Line_Item_ID__c) && !ptpMapDelete.containsKey(ptp.Id)){
                ptpMapDelete.put(ptp.Id, ptp);    
             }
         }*/
         
         for(OSM_Payor_Test_Price__c ptp: ptpList){
             //if(ptp.OSM_Agreement_ID__c == dummyAgreement.Id){
                List<OSM_Payor_Test_Price__c> ptpALIs = new List<OSM_Payor_Test_Price__c>();
                if(ProdPTPMap.containsKey(ptp.OSM_Test__r.Name)){
                    ptpALIs =  ProdPTPMap.get(ptp.OSM_Test__r.Name);
                }   
                    
                ptpALIs.add(ptp);
                ProdPTPMap.put(ptp.OSM_Test__r.Name, ptpALIs);
            // }
         }
         System.debug('\n\n\n ProdPTPMapVALS'+ProdPTPMap);
         for(OSM_Payor_Test_Price__c ptp: ptpList){
             
                 if(ptp.OSM_Agreement_Line_Item_ID__c == null ){
                     System.debug('\n\n\n syncnull');
                     System.debug('\n\n\n DELETE0');
                    if(!ptpMapDelete.containsKey(ptp.Id)){
                        ptpMapDelete.put(ptp.Id,ptp);  
                        //ptpcheckDone.add(ptp.Id);
                        
                    }
                    
                 }
                 else  if(ptp.OSM_Agreement_ID__c == dummyAgreement.Id){
                     
                     if(aliList.isEmpty()){
                         System.debug('\n\n\n DELETE1');
                        ptpMapDelete.put(ptp.Id,ptp);      
                        
                     }
                      else{
                         for(Apttus__AgreementLineItem__c ali: aliList){  
                            if(ali.Apttus__ProductId__c == ptp.OSM_Test__c && !aliCheckDone.contains(ali.Id) && !ptpcheckDone.contains(ptp.Id) ){
                                ptpMapExclude.put(ptp.Id, ptp);
                                aliCheckDone.add(ali.Id); System.debug('\n\n\n EXCLUDED' + ptp.Id);
                                ptpcheckDone.add(ptp.Id);
                            }
                            else if(!ptpMapDelete.containsKey(ptp.Id)){
                                System.debug('\n\n\n DELETE2');
                                ptpMapDelete.put(ptp.Id,ptp);       
                            }
                             
                         }
                     }
                 }
                 System.debug('\n\n\n PTPCONTAINS'+ProdPTPMap.containsKey(ptp.OSM_Test__r.Name));
                 System.debug('\n\n\n PTPSIZE'+ProdPTPMap.get(ptp.OSM_Test__r.Name).size());
                 if(ProdPTPMap.containsKey(ptp.OSM_Test__r.Name) && ProdPTPMap.get(ptp.OSM_Test__r.Name).size() <= 1){
                     System.debug('\n\n\n PTPEXCLUDE'+ptp.Id);
                     ptpMapExclude.put(ptp.Id, ptp);
                 }    
  
                
           }
         System.debug('\n\n\n EXCLUDEDMAP'+ptpMapExclude);
         for(OSM_Payor_Test_Price__c ptp: ptpList){
             if(ptp.OSM_Agreement_ID__c == dummyAgreement.Id){
                 if(ptpMapExclude.containsKey(ptp.Id) && ptpMapDelete.containsKey(ptp.Id)){
                    ptpMapDelete.remove(ptp.Id);       
                 }
             }
         }
         List<OSM_Payor_Test_Price_History__c> ptphList= new List<OSM_Payor_Test_Price_History__c>();
         System.debug('\n\n\n DELETEMAP'+ptpMapDelete);
         for(OSM_Payor_Test_Price__c ptp :ptpMapDelete.values()){
              if(ptp.OSM_Agreement_ID__c != dummyAgreement.Id && ptp.OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c != dummyAgreement.Id){
                  OSM_Payor_Test_Price_History__c ptpHistory1 = new OSM_Payor_Test_Price_History__c(OSM_Payor__c = ptp.OSM_Payor__c,
                                                                                                    OSM_Test__c = ptp.OSM_Test__c,
                                                                                                    OSM_Start_Date__c = ptp.OSM_Start_Date__c,
                                                                                                    OSM_End_Date__c = ptp.OSM_End_Date__c,
                                                                                                    OSM_Agreement_ID__c = ptp.OSM_Agreement_ID__c,
                                                                                                    OSM_Agreement_Line_Item_ID__c= ptp.OSM_Agreement_Line_Item_ID__c,
                                                                                                    OSM_Agreement_Record_Type__c=  ptp.OSM_Agreement_Record_Type__c,
                                                                                                    OSM_Agreement_Sub_Type__c =  ptp.OSM_Agreement_Sub_Type__c,
                                                                                                    OSM_Net_Price__c =   ptp.OSM_Net_Price__c,
                                                                                                    OSM_List_Price__c =  ptp.OSM_List_Price__c,
                                                                                                    OSM_Price_Method__c =  ptp.OSM_Price_Method__c,
                                                                                                    OSM_Pricing_Schema__c =  ptp.OSM_Pricing_Schema__c,
                                                                                                    OSM_Quantity_End_Range__c = ptp.OSM_Quantity_End_Range__c,
                                                                                                    OSM_Quantity_Start_Range__c = ptp.OSM_Quantity_Start_Range__c,
                                                                                                    OSM_Assay_Score__c =  ptp.OSM_Assay_Score__c,
                                                                                                    //OSM_Criteria__c = agreementliList[a].GHI_CPQ_Criteria__c,
                                                                                                    CurrencyIsoCode =  ptp.CurrencyIsoCode,
                                                                                                    OSM_Expected_Rate__c = ptp.OSM_Expected_Rate__c,
                                                                                                    OSM_Billing_Category__c = ptp.OSM_Billing_Category__c,
                                                                                                    OSM_Billing_Cycle__c = ptp.OSM_Billing_Cycle__c,
                                                                                                    OSM_CPT_Code__c = ptp.OSM_CPT_Code__c,
                                                                                                    OSM_Future_Effective__c = false,
                                                                                                    OSM_Currently_Active__c = false
                                                                                                    //OSM_Plan__c = ptp.OSM_Plan__c
                                                                                                
                                                                                                 );  
                 ptphList.add(ptpHistory1);
             }
         }
         if(!ptpMapDelete.values().isEmpty()){
           delete ptpMapDelete.values();
           if(!ptphList.isEmpty()){
               insert ptphList;
           }
         }
         
        
         //delete ali through this button of a certain test till it runs out
        List<OSM_Payor_Test_Price__c> ptpGenList = [SELECT OSM_Agreement_ID__c, OSM_Billing_Cycle__c, OSM_Billing_Category__c, OSM_Assay_Score__c, CurrencyISOCode, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c =: accountId AND OSM_Future_Effective__c = false]; 
        dummyPTPs = new List<OSM_Payor_Test_Price__c>();
        for(String prod: productList){
            Integer ctr = 0;
            for(OSM_Payor_Test_Price__c ptp: ptpGenList){
                if(ptp.OSM_Test__r.Name == prod){
                    ++ctr;
                }
               
            }
            if(ctr == 0){
                OSM_Payor_Test_Price__c ptpDummy1 = new OSM_Payor_Test_Price__c(OSM_Payor__c = accountId,
                                                                                            OSM_Test__c = prodIdMap.get(prod),
                                                                                            OSM_Start_Date__c = dummyAliMap.get(prod).Apttus_CMConfig__StartDate__c,
                                                                                            OSM_End_Date__c =  dummyAliMap.get(prod).Apttus_CMConfig__EndDate__c,
                                                                                            OSM_Agreement_ID__c =  dummyAgreement.Id ,
                                                                                            OSM_Agreement_Line_Item_ID__c= dummyAliMap.get(prod).Id,
                                                                                            OSM_Agreement_Record_Type__c= rtname,
                                                                                            OSM_Agreement_Sub_Type__c =  dummyAgreement.Apttus__Subtype__c,
                                                                                            //OSM_Net_Price__c =    dummyAliMap.get(prod).Apttus__NetPrice__c,
                                                                                            OSM_List_Price__c =   pbTestMap.get(prod).UnitPrice,
                                                                                            OSM_Price_Method__c =   dummyAliMap.get(prod).GHI_CPQ_Price_Method__c,
                                                                                            OSM_Pricing_Schema__c =   dummyAliMap.get(prod).GHI_CPQ_Pricing_Schema__c,
                                                                                            OSM_Quantity_End_Range__c =  dummyAliMap.get(prod).GHI_CPQ_End_Range__c,
                                                                                            OSM_Quantity_Start_Range__c =   dummyAliMap.get(prod).GHI_CPQ_Start_Range__c,
                                                                                            OSM_Assay_Score__c =   dummyAliMap.get(prod).GHI_CPQ_Assay_Score__c,
                                                                                            //OSM_Criteria__c = agreementliList[a].GHI_CPQ_Criteria__c,
                                                                                            CurrencyISOCode =    myAcct.CurrencyIsoCode,
                                                                                            OSM_Expected_Rate__c =  dummyAliMap.get(prod).GHI_CLM_Expected_Rate__c,
                                                                                            OSM_Billing_Category__c =  dummyAliMap.get(prod).GHI_CPQ_Billing_Category__c,
                                                                                            OSM_CPT_Code__c =  dummyAliMap.get(prod).GHI_CPQ_CPT_Code__c,
                                                                                            OSM_Newly_Generated__c = true,
                                                                                            OSM_Billing_Cycle__c =  dummyAliMap.get(prod).GHI_CPQ_Billing_Cycle__c
                                                                                         ); 
                    dummyPTPs.add(ptpDummy1);    
            }
        }
        
        if(!dummyPTPs.isEmpty()){
            insert dummyPTPs;
        }
        /*
         for(OSM_Payor_Test_Price__c ptp: ptpList){
             if(ptp.OSM_Agreement_ID__c == dummyAgreement.Id){
                 if(ptpMapExclude.containsKey(ptp.Id) && ptpMapDelete.containsKey(ptp.Id)){
                    ptpMapDelete.remove(ptp.Id);       
                 }
             }
         }
         if(!ptpMapDelete.values().isEmpty()){
           delete ptpMapDelete.values();
         }
         System.debug('\n\n\n PTPPROD'+ptp.OSM_Test__c);
                         System.debug('\n\n\n ALIPROD'+ali.Apttus__ProductId__c);
                         System.debug('\n\n\n ALICHECKDONEVAL'+aliCheckDone.contains(ali.Id));
                         if(ali.Apttus__ProductId__c == ptp.OSM_Test__c){
                             if(!aliCheckDone.contains(ali.Id)){
                                 aliCheckDone.add(ali.Id); 
                                 ptpcheckDone.add(ptp.Id);
                             }
                             else{
                                 ctr++;
                             }
                         }
                         else{
                            
                         }
                         
                         System.debug('\n\n\n CTRVAL'+ctr);
                    System.debug('\n\n\n ALILISTSIZE'+aliList.size());
                     if(ctr == aliList.size() && !ptpMapDelete.containsKey(ptp.Id) && !ptpcheckDone.contains(ptp.Id)){
                        ptpMapDelete.put(ptp.Id,ptp);   
                        ptpcheckDone.add(ptp.Id);
         Map<Id, OSM_Payor_Test_Price__c> ptpMapDelete = new Map<Id, OSM_Payor_Test_Price__c>();
         aliList = [SELECT Id, GHI_CPQ_Billing_Cycle__c, GHI_CPQ_Pricing_Schema__c, Active_Rate__c, Apttus__ProductId__c, Apttus__ProductId__r.Name, GHI_CPQ_Billing_Category__c, GHI_CPQ_CPT_Code__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  Apttus_CMConfig__StartDate__c, GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus__ListPrice__c FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c =: agreementId];
         ptpList = [SELECT OSM_Agreement_ID__c, OSM_Billing_Cycle__c, OSM_Billing_Category__c, OSM_Assay_Score__c, OSM_Currency__c, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c =: accountId];
         
         
        ProdALIMap = new Map<String, List<Apttus__AgreementLineItem__c>>();
        for(Apttus__AgreementLineItem__c ali: aliList){
                List<Apttus__AgreementLineItem__c> paALIs = new List<Apttus__AgreementLineItem__c>();
                if(ProdALIMap.containsKey(ali.Apttus__ProductId__r.Name)){
                    paALIs =  ProdALIMap.get(ali.Apttus__ProductId__r.Name);
                }   
                    
                paALIs.add(ali);
                ProdALIMap.put(ali.Apttus__ProductId__r.Name, paALIs);
         }
         Set<ID> aliCheckDone = new  Set<ID>();
         Set<ID> ptpcheckDone = new Set<ID>();
         for(OSM_Payor_Test_Price__c ptp: ptpList){
             
                 if(ptp.OSM_Agreement_Line_Item_ID__c == null){
                    ptpMapDelete.put(ptp.Id,ptp);  
                    ptpCheckDone.add(ptp.Id);
                 }
                 else{
                    Integer ctr=0;
                    for(Apttus__AgreementLineItem__c ali: aliList){
                        System.debug('\n\n\n syncAG'+ptp.OSM_Agreement_ID__c);
                        if(ptp.OSM_Agreement_ID__c == agreementId){
                            if(ali.Apttus__ProductId__c == ptp.OSM_Test__c && !aliCheckDone.contains(ali.Id)){
                                 aliCheckDone.add(ali.Id);   
                                 System.debug('\n\n\n syncnondummy');
                            }
                        }
                        else if(ptp.OSM_Agreement_ID__c == dummyAgreement.Id) {
                            System.debug('\n\n\n syncALIProdId'+ali.Apttus__ProductId__c);
                            System.debug('\n\n\n syncPTPProdId'+ptp.OSM_Test__c);
                            System.debug('\n\n\n syncCheckDone'+aliCheckDone.contains(ali.Id));
                            if(ali.Apttus__ProductId__c == ptp.OSM_Test__c && !aliCheckDone.contains(ali.Id)){
                                aliCheckDone.add(ali.Id);   
                                ptpCheckDone.add(ptp.Id);
                                System.debug('\n\n\n sync');
                            }
                            else if (ali.Apttus__ProductId__c == ptp.OSM_Test__c ){
                                ctr++;
                            }
                        }
                        
                    
                    }
                    if(ptp.OSM_Agreement_ID__c == dummyAgreement.Id){
                    System.debug('\n\n\n CTRVAL'+ctr);
                    System.debug('\n\n\n PRODALIVAL'+ProdALIMap.get(ptp.OSM_Test__r.Name).size());
                    }
                    if(ctr == ProdALIMap.get(ptp.OSM_Test__r.Name).size() &&  !ptpMapDelete.containsKey(ptp.Id) && ptp.OSM_Agreement_ID__c == dummyAgreement.Id){
                        ptpMapDelete.put(ptp.Id,ptp);    
                    }
                 }
         }
        
         if(!ptpMapDelete.values().isEmpty()){
             delete ptpMapDelete.values();
         }*/
        
    }
}