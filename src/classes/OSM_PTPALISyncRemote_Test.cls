/*
    @author: patrick lorilla
    @description: test class for PTP ALI Sync
    @date: July-3-2015
*/

@isTest(SeeAllData=true) 
public class OSM_PTPALISyncRemote_Test {
        /*
        @author: patrick lorilla
        @description: test class for PTP ALI Sync main functionality
        @date: July-3-2015
       */

     testmethod static void ptpALISyncTest(){
         
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgdefaults();
        triggerSwitch.Account_Trigger__c = false;
        update triggerSwitch;
         
         
        List<Apttus__APTS_Agreement__c> dummyAgreement = [SELECT Id from Apttus__APTS_Agreement__c where Name = 'Payor Pricing Agreement' LIMIT 1];
        List<Apttus__AgreementLineItem__c> dummyALIs =[SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement'];
        //prepare data and necessary records. Perform the necessary changes
        Account payor = OSM_DataFactory.createPayorAccountWithBillingAddress(0,'2', 'Private','United States', 'street', 'city','province','111', Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId());
        payor.OSM_Status__c = 'Draft';
        payor.CurrencyIsoCode = 'USD';
        payor.OSM_Payor_Category__c = 'Private';
        insert payor;
        
        payor.OSM_Status__c = 'Approved';
        update payor;
        
        List<Product2> testProdList = new List<Product2>();
        for(String str: Label.OSM_Product_List.split('::')){
            if(str.length() > 0){
                testProdList.add(OSM_DataFactory.createProduct(str, true));  
            }
        }
        insert testProdList;
        
        PriceBook2 prBook = new PriceBook2();
        prBook.Name = 'USD Private';
        prBook.IsActive = True;
        prBook.Pricing_Category__c = 'Private';
        insert prBook;
        
        List<PricebookEntry> lstStdPbe = new List<PricebookEntry>();
        for (Product2 prod : testProdList)
        {
            PricebookEntry pbe = new PricebookEntry(pricebook2id=Test.getStandardPricebookId(), product2id=prod.id,unitprice=1.0, isActive=true, Pricing_Category__c = 'Private');
            lstStdPbe.add(pbe);
        }
        insert lstStdPbe;
        
        List<PricebookEntry> lstPbe = new List<PricebookEntry>();
        for (Product2 prod : testProdList)
        {
            PricebookEntry pbe = new PricebookEntry(pricebook2id=prBook.id, product2id=prod.id,unitprice=1.0, isActive=true, Pricing_Category__c = 'Private');
            lstPbe.add(pbe);
        }
        insert lstPbe;
        
        Apttus__APTS_Agreement__c agreementTest = dummyAgreement[0].clone(false, true, false, false);
        agreementTest.Name = 'Test Agreement';
        agreementTest.Apttus__Account__c = payor.Id;
        
        insert agreementTest;
        
        List<Product2> testProd = [SELECT ID, name from Product2 where name = 'IBC'];
        
        
        
        Apttus__AgreementLineItem__c realALI1 = dummyALIs[0].clone(false, true, false, false);
        for(Product2 prod : testProdList){
        realALI1.Apttus__AgreementId__c = dummyAgreement[0].Id;
        realALI1.Apttus__ProductId__c = prod.Id;
        realALI1.Apttus_CMConfig__StartDate__c =System.today();
        }
        insert realALI1;
        Apttus__AgreementLineItem__c realALI2 = dummyALIs[0].clone(false, true, false, false);
        realALI2.Apttus__AgreementId__c = agreementTest.Id;
        //realALI2.Apttus__ProductId__c = testProd[0].Id;
        realALI2.Apttus__ProductId__c = testProdList[0].Id;
        realALI2.Apttus_CMConfig__StartDate__c =System.today();
        
        insert realALI2;
        
        Date ptpStartDate = Date.today().addDays(10);
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
        OSM_Agreement_ID__c = dummyAgreement[0].Id,
        OSM_Newly_Generated__c = true,
        OSM_Payor__c = payor.Id,
        OSM_Start_Date__c  = ptpStartDate,
        OSM_Agreement_Line_Item_ID__c = realALI2.Id,
        OSM_List_Price__c = 3.00,
        OSM_Billing_Category__c = 'Monthly',
        OSM_Billing_Cycle__c = 'Monthly',
        OSM_Price_Method__c ='Dummy Method',
        OSM_CPT_Code__c ='samplecode123',
        CurrencyIsoCode = 'USD',
        OSM_Pricing_Schema__c = 'Dummy Schema',
        OSM_Net_Price__c = 2.00
        );
        insert ptp;
        
        OSM_Payor_Test_Price__c ptp2 = new OSM_Payor_Test_Price__c(
        OSM_Agreement_ID__c = agreementTest.Id,
        OSM_Newly_Generated__c = true,
        OSM_Payor__c = payor.Id,
        OSM_Start_Date__c  = ptpStartDate,
        OSM_Agreement_Line_Item_ID__c = null,
        OSM_List_Price__c = 3.00,
        OSM_Billing_Category__c = 'Monthly',
        OSM_Billing_Cycle__c = 'Monthly',
        OSM_Price_Method__c ='Dummy Method',
        OSM_CPT_Code__c ='samplecode123',
        CurrencyIsoCode = 'USD',
        OSM_Pricing_Schema__c = 'Dummy Schema',
        OSM_Net_Price__c = 2.00
        );
        insert ptp2;
        
        Test.startTest();
        OSM_PTPALISyncRemote.ptpALISync(agreementTest.Id, payor.Id);
        Test.stopTest();
        dummyALIs = [SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement' AND Apttus__ProductId__c =: testProd[0].Id];
        //System.assertEquals(dummyALIs.size(), 3);
        
     }

}