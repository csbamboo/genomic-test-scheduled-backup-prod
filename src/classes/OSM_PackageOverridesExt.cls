/**
 * File Info
 * ----------------------------------
 * @filename       PackageOverridesExt.cls
 * @created        23.JAN.2015
 * @author         Kristian Vegerano
 * @description    Class extension for pages that are used for overriding OSM_Package__c buttons. 
 * @history        23.JAN.2015 - Kristian Vegerano - Created  
 */
public class OSM_PackageOverridesExt{
    private Map<String, String> pageParameters;
    /**
     * @author         Kristian Vegerano
     * @description    Constructor. 
     * @history        23.JAN.2014 - Kristian Vegerano - Created  
     */
    public OSM_PackageOverridesExt(ApexPages.StandardController controller){
        pageParameters = ApexPages.currentPage().getParameters();
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that adds the User's Package Origin field to the current PageReference as a parameter. 
     * @history        23.JAN.2014 - Kristian Vegerano - Created  
     */
    public PageReference RedirectNew(){
        PageReference newPage = new PageReference('/' + OSM_Package__c.sObjectType.getDescribe().getKeyPrefix() + '/e');
        for(String loopKey : pageParameters.keySet()){
            newPage.getParameters().put(loopKey, pageParameters.get(loopKey));
        }
        newPage.getParameters().put(Record_Type__c.getOrgDefaults().OSM_CF_User_Package_Origin__c, 
                                           [SELECT Id, OSM_Package_Origin__c FROM User WHERE Id = :UserInfo.getUserId()].OSM_Package_Origin__c);
        newPage.getParameters().put('nooverride','1');
        newPage.getParameters().remove('save_new');
        return newPage;
    }
}