/*
 *  @author         Jerome Liwanag
 *  @description    Test Class for OSM_PackageOverridesExt
 *  @date           03 August 2015
 *  @history        03 August 2015 - Created - Jerome liwanag
 */
@isTest(seeAllData=true)
private class OSM_PackageOverridesExt_Test {

	private static testMethod void testMethod1() {
	    OSM_Package__c package1 = OSM_DataFactory.createPackage('test',null,true);
	    insert package1;
	    
        PageReference pageRef = Page.OSM_NewPackageRedirect;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(package1);
        ApexPages.currentPage().getParameters().put('Id',package1.id);
        OSM_PackageOverridesExt cont = new OSM_PackageOverridesExt(sc);  
        
        Test.startTest();
        cont.RedirectNew();
        Test.stopTest();
	}

}