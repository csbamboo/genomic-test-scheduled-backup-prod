/**
 * File Info
 * ----------------------------------
 * @filename       OSM_PackageTriggerHandler.cls
 * @created        23.JAN.2014
 * @author         Kristian Vegerano
 * @description    Class for OSM_Package__c dml actions. 
 * @history        23.JAN.2014 - Kristian Vegerano - Created  
 */
public class OSM_PackageTriggerHandler{
    public static Boolean hasEnsurePackageOriginRun = false;
    public static Boolean hasCreateGenericCaseRun = false;
    public static Boolean hasBarcodeCloseRelatedSRRun = false;
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for all actions that will take place before a package will be inserted. 
     * @history        23.JAN.2014 - Kristian Vegerano - Created  
     */
    public static void onBeforeInsert(List<OSM_Package__c> packageList){
        if(!hasEnsurePackageOriginRun){
            hasEnsurePackageOriginRun = true;
            ensurePackageOrigin(packageList);
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for all actions that will take place before a package will be updated. 
     * @history        23.JAN.2014 - Kristian Vegerano - Created  
     */
    public static void onBeforeUpdate(List<OSM_Package__c> packageList){
        if(!hasEnsurePackageOriginRun){
            hasEnsurePackageOriginRun = true;
            ensurePackageOrigin(packageList);
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for all actions that will take place after a package is inserted. 
     * @history        28.JAN.2014 - Kristian Vegerano - Created  
     */
    public static void onAfterInsert(Map<Id,OSM_Package__c> packageMap){
        if(!hasCreateGenericCaseRun){
            hasCreateGenericCaseRun = true;
            createGenericCase(packageMap);
        }
        if(!hasBarcodeCloseRelatedSRRun){
            hasBarcodeCloseRelatedSRRun = true;
            barcodeCloseRelatedSR(packageMap.values());
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for all actions that will take place after a package is updated. 
     * @history        28.JAN.2014 - Kristian Vegerano - Created  
     */
    public static void onAfterUpdate(Map<Id,OSM_Package__c> packageMap){
        if(!hasCreateGenericCaseRun){
            hasCreateGenericCaseRun = true;
            createGenericCase(packageMap);
        }
        if(!hasBarcodeCloseRelatedSRRun){
            hasBarcodeCloseRelatedSRRun = true;
            barcodeCloseRelatedSR(packageMap.values());
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that ensures that the package will have its origin field same as the current user's package origin field. 
     * @history        23.JAN.2014 - Kristian Vegerano - Created  
     */
    public static void ensurePackageOrigin(List<OSM_Package__c> packageList){
        User currentUser = [SELECT Id, OSM_Package_Origin__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        String currentValue = currentUser.OSM_Package_Origin__c;
        
        for(OSM_Package__c loopPackage : packageList){
            currentUser.OSM_Package_Origin__c = loopPackage.OSM_Origin__c;
        }
        
        String newValue = currentUser.OSM_Package_Origin__c;
        
        if(currentValue != newValue){
            update currentUser;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that creates a case with a record type of generic where its type and subtype fields are dependent on the package record. 
     * @history        28.JAN.2014 - Kristian Vegerano - Created  
     */
    public static void createGenericCase(Map<Id,OSM_Package__c> packageMap){
        Set<Id> excludePackage = new Set<Id>();
        Map<Id, Case> packageCaseMap = new Map<Id, Case>();
        List<Case> insertCase = new List<Case>();
        List<Case> updateCase = new List<Case>();
        Boolean processGeneralCase = false;
        
        for(OSM_Package__c loopPackage : packageMap.values()){
            if(loopPackage.OSM_Order_Screening__c || loopPackage.OSM_Specimen_Screening__c){
         		processGeneralCase = true;
            }
         }
        
        if (processGeneralCase) {
            for(Case loopCase : [SELECT Id, OSM_Tracking_Number_lookup__c FROM Case WHERE RecordType.DeveloperName = 'OSM_Generic' AND OSM_Tracking_Number_lookup__c IN :packageMap.keySet()]){
                excludePackage.add(loopCase.OSM_Tracking_Number_lookup__c);
                packageCaseMap.put(loopCase.OSM_Tracking_Number_lookup__c, loopCase);
            }
            
            Case newCase;
            Id genericRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Generic').getRecordTypeId();
            for(OSM_Package__c loopPackage : packageMap.values()){
                if(loopPackage.OSM_Order_Screening__c || loopPackage.OSM_Specimen_Screening__c){
                    if(!excludePackage.contains(loopPackage.Id)){
                        newCase = new Case();
                        newCase.OSM_Tracking_Number_lookup__c = loopPackage.Id;
                        newCase.Type = 'Package Issue';
                        newCase.RecordTypeId = genericRecordTypeId;
                        if(loopPackage.OSM_Order_Screening__c == true && loopPackage.OSM_Specimen_Screening__c == true){
                            newCase.OSM_Subtype__c = 'Package - Order Screening Failed;Package - Specimen Screening Failed';
                        }else if(loopPackage.OSM_Order_Screening__c == true){
                            newCase.OSM_Subtype__c = 'Package - Order Screening Failed';
                        }else if(loopPackage.OSM_Specimen_Screening__c == true){
                            newCase.OSM_Subtype__c = 'Package - Specimen Screening Failed';
                        }
                        insertCase.add(newCase);
                    }else{
                        newCase = new Case(Id = packageCaseMap.get(loopPackage.Id).Id);
                        newCase.OSM_Tracking_Number_lookup__c = loopPackage.Id;
                        newCase.Type = 'Package Issue';
                        newCase.RecordTypeId = genericRecordTypeId;
                        if(loopPackage.OSM_Order_Screening__c == true && loopPackage.OSM_Specimen_Screening__c == true){
                            newCase.OSM_Subtype__c = 'Package - Order Screening Failed;Package - Specimen Screening Failed';
                        }else if(loopPackage.OSM_Order_Screening__c == true){
                            newCase.OSM_Subtype__c = 'Package - Order Screening Failed';
                        }else if(loopPackage.OSM_Specimen_Screening__c == true){
                            newCase.OSM_Subtype__c = 'Package - Specimen Screening Failed';
                        }
                        updateCase.add(newCase);
                    }
                }
            }
        }
            
        if(insertCase.size() > 0){
            insert insertCase;
        }
        if(updateCase.size() > 0){
            update updateCase;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that closes related specimen retrieval cases if the barcode is not null and creates a specimen arrival cases for each updated specimen retrieval case
     * @history        23.JAN.2014 - Kristian Vegerano - Created  
     */
    public static void barcodeCloseRelatedSR(List<OSM_Package__c> packageList){
        Set<String> caseNumberSet = new Set<String>();
        Set<String> filterSet = new Set<String>();
        Set<Id> parentOrderIds = new Set<Id>();
        List<Case> insertCases = new List<Case>();
        List<Case> updateCases = new List<Case>();
        Map<Id,String> packageIdToNumberMap = new Map<Id,String>();
        Map<String,Set<Id>> caseToPackagesMap = new Map<String,Set<Id>>();   
        Map<Id,Set<Id>> packageToPrimaryOrdersMap = new Map<Id,Set<Id>>();   
        Map<String, List<Case>> orderPackageCasesMap = new Map<String, List<Case>>();
        Map<Id,OSM_Package__c> packageMap = new Map<Id,OSM_Package__c>();
        Map<Integer,String> counterCaseNumberMap = new Map<Integer,String>();
        
        Id saRecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Specimen Arrival').RecordTypeId;
        for(OSM_Package__c loopPackage : packageList){
            if(loopPackage.OSM_SR_Barcode__c != null){
                for(String loopString : loopPackage.OSM_SR_Barcode__c.split('[\n\r]')){
                    if(!caseToPackagesMap.containsKey(loopString.trim())){
                        caseToPackagesMap.put(loopString.trim(), new Set<Id>{loopPackage.Id});
                    }else{
                        caseToPackagesMap.get(loopString.trim()).add(loopPackage.Id);
                    }
                    caseNumberSet.add(loopString);
                }
                packageMap.put(loopPackage.Id, loopPackage);
            }
        }
        
        Integer counterInsertCase = 0;
         
        //Create Cases
        for(Case loopCase : [SELECT Id, CaseNumber, OSM_Tracking_Number_lookup__c, OSM_Primary_Order__c, OSM_Specimen_Request_Date_Time__c, OSM_Primary_Customer__c, OSM_Specimen_Retrieval_Case__c 
                             FROM Case 
                             WHERE CaseNumber IN :caseNumberSet
                                AND RecordType.DeveloperName = 'OSM_Specimen_Retrieval']){
            for(Id packageId : caseToPackagesMap.get(loopCase.CaseNumber)){
                if(loopCase.OSM_Tracking_Number_lookup__c == null){
                    updateCases.add(new Case(Id = loopCase.Id, Status = 'Closed', OSM_Tracking_Number_lookup__c = packageId));
                }
                insertCases.add(new Case(OSM_Specimen_Request_Date_Time__c = loopCase.OSM_Specimen_Request_Date_Time__c,
                                         OSM_Primary_Customer__c = loopCase.OSM_Primary_Customer__c,
                                         OSM_Specimen_Retrieval_Case__c = loopCase.Id,
                                         OSM_Tracking_Number_lookup__c = packageId,
                                         OSM_Primary_Order__c = loopCase.OSM_Primary_Order__c,
                                         Status = 'Open',
                                         RecordTypeId = saRecordTypeId));
                counterCaseNumberMap.put(counterInsertCase, loopCase.CaseNumber);
                counterInsertCase = counterInsertCase + 1;
                parentOrderIds.add(loopCase.OSM_Primary_Order__c);
            }
        }
        
        if(updateCases.size() > 0){
            update updateCases;
        }
        if(insertCases.size() > 0){
            Integer counter = 0;
            Database.SaveResult[] lsr = Database.insert(insertCases, false);
            for(Database.SaveResult sr : lsr){
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        String error;
                        error = err.getMessage();
                        packageMap.get(insertCases[counter].OSM_Tracking_Number_lookup__c).addError(' Case Number: ' + counterCaseNumberMap.get(counter) + ' - ' + error);
                    }
                }
                counter = counter + 1;
            }
        }
    }
}