/*
    @author: Jerome Liwanag
    @description: Package Trigger Handler Test
    @date: 30 June 2015 - Create
*/
@isTest(seeAllData=true)
private class OSM_PackageTriggerHandler_Test {

	static testMethod void testMethod1() {
	    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Case_Trigger__c = false;
        triggerSwitch.Package_Trigger__c = true;
        update triggerSwitch;
        Id caseRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Generic').getRecordTypeId();
        Id caseRecType2 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Specimen Retrieval').getRecordTypeId();
        AssignmentRule defaultAssignmentRule = new AssignmentRule();
        defaultAssignmentRule = [SELECT id FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true LIMIT 1];
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId = defaultAssignmentRule.id;
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        //prepare default accounts
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(1,'United States', 'Street 1', 'City 1', 'California', '123', recordTypeIds.Account_HCO_Record_Type__c) ;
        insert acct;
        List<Order> orderList = new List<Order>();
        List<Contact> patList = new List<Contact>();
        for(Integer a=0; a<20; a++){
            patList.add(OSM_DataFactory.createContact(a, acct.ID,  (ID) recordTypeIds.Contact_Patient_Record_Type__c));
        }
        
        insert patList ;
        for(Integer a=0; a<20; a++){
            orderList.add(OSM_DataFactory.createOrder('ord'+a, patList[a].ID, acct.ID, System.Today(), 'New'));
            orderList[a].OSM_Bill_Type__c = null;
            orderList[a].OSM_Status__c = 'Order Intake';/*
            orderList[a].OSM_Signature_Date__c = System.Today();
            orderList[a].OSM_Product__c = 'IBC';*/
        }
            
        insert orderList;
        
        Case case2 = OSM_DataFactory.createCaseWithTrackingNumber('Open', 'Package Issue', null , caseRecType2);
        insert case2;
       
        
        case2 = [select Id, CaseNumber,OSM_Tracking_Number_lookup__c,OSM_Primary_Order__c,Status from Case where id = :case2.id];
        case2.setOptions(dmlOpts);
        OSM_Package__c pkg = OSM_DataFactory.createPackage('12313224', null, true);
        insert pkg;
        
        case2.OSM_Tracking_Number_lookup__c = pkg.Id;
        case2.OSM_Primary_Order__c = orderList[1].Id;
        update case2;
        
        Case case1 = OSM_DataFactory.createCaseWithTrackingNumber('Open', 'Package Issue', pkg.Id , caseRecType);
        insert case1;
        
        OSM_Package__c pkg2 = OSM_DataFactory.createPackage('12313224', null, false);
        pkg2.OSM_Specimen_Screening__c = true;
        
        OSM_PackageTriggerHandler.hasCreateGenericCaseRun = false;
        insert pkg2;
        
        Case case3 = OSM_DataFactory.createCaseWithTrackingNumber('Open', 'Package Issue', pkg2.Id , caseRecType);
        insert case3;
        
        OSM_Package__c pkg3 = OSM_DataFactory.createPackage('12313224', null, true);
        pkg3.OSM_Specimen_Screening__c = true;
        
        OSM_PackageTriggerHandler.hasCreateGenericCaseRun = false;
        insert pkg3;
        
        Case case4 = OSM_DataFactory.createCaseWithTrackingNumber('Open', 'Package Issue', pkg3.Id , caseRecType);
        insert case4;
        
        Test.startTest();
        pkg.Name = '123213';
        pkg.OSM_SR_Barcode__c = case2.CaseNumber + '\n\r' + 'SR Barcode Test';
        OSM_PackageTriggerHandler.hasEnsurePackageOriginRun = false;
        OSM_PackageTriggerHandler.hasCreateGenericCaseRun = false;
        OSM_PackageTriggerHandler.hasBarcodeCloseRelatedSRRun = false;
        update pkg;
        Test.stopTest();
	}

}