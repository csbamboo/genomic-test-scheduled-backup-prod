public class OSM_Patient_Redirect_Controller{
    public String recordTypeName {get;set;}
    public OSM_Patient_Redirect_Controller(ApexPages.StandardController controller) {
        recordTypeName = [SELECT Id, RecordType.DeveloperName FROM Contact WHERE Id = :ApexPages.CurrentPage().getparameters().get('id') LIMIT 1].RecordType.DeveloperName;
    }
}