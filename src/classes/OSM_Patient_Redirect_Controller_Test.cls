/*
@author: Patrick Lorilla
@date: 24 September 2014
@description: Test Patient Redirect 

*/

@isTest
public class OSM_Patient_Redirect_Controller_Test{
    /*
    @author: Patrick Lorilla
    @date: 24 September 2014
    @description: Test redirect for NonPatients

    */

    static testmethod void testRedirectNonPatient(){
        //Prepare recordtypes, account and contacts
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        
        Contact con = OSM_DataFactory.createContact(0,acct.ID,null);
       
        
        insert con;
        Contact pat = OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_Patient_Record_Type__c);
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
       
        //Test Patient
        // Add parameters to page URL
        PageReference pageRef = Page.OSM_Patient_Redirect;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
       
        Test.startTest();
       
        OSM_Patient_Redirect_Controller prController = new OSM_Patient_Redirect_Controller(sc);
        // prController.redirectToPage();
        
        //Assert nonpatient display is inline
        // System.assertEquals(prController.nonpatient, 'inline');
        // System.assertEquals(prController.patient, 'none');
        
        Test.stopTest();
        
        String recName = [SELECT Id, RecordType.DeveloperName FROM Contact WHERE Id = :ApexPages.CurrentPage().getparameters().get('id') LIMIT 1].RecordType.DeveloperName;
        System.assertEquals(prController.recordTypeName, recName);
        
        // prController = new OSM_Patient_Redirect_Controller();
    }
    
    static testmethod void testRedirectPatient(){
        //Prepare recordtypes, account and contacts
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;

        
        Contact pat = OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_Patient_Record_Type__c);
        insert pat ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(pat);
       
        //Test Patient
        // Add parameters to page URL
        PageReference pageRef = Page.OSM_Patient_Redirect;
        pageRef.getParameters().put('id', String.valueOf(pat.Id));
        Test.setCurrentPage(pageRef);
       
        Test.startTest();
        //Redirect
        OSM_Patient_Redirect_Controller prController = new OSM_Patient_Redirect_Controller(sc);
        // prController.redirectToPage();
        
        //Assert patient display is inline
        // System.assertEquals(prController.patient, 'inline');
        // System.assertEquals(prController.nonpatient, 'none');
        
        Test.stopTest();
        
        String recName = [SELECT Id, RecordType.DeveloperName FROM Contact WHERE Id = :ApexPages.CurrentPage().getparameters().get('id') LIMIT 1].RecordType.DeveloperName;
        System.assertEquals(prController.recordTypeName, recName);
    }
    

}