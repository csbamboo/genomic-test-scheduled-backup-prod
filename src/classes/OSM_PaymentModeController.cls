public with sharing class OSM_PaymentModeController {
    private final OrderItem ordrItem;
    private OrderItem defaultordrItem;
    
    public boolean editButton{get; set;}
    public Boolean isWire {get;set;}
    public Boolean isCredit {get;set;}
    public Boolean isCheque {get;set;}
    
    public List<OSM_Payment_Email_History__c> paymentEmailListDisplay {get;set;}
    public  List<OrderItem> ordItem = new List<OrderItem>();
    public List<OSM_Payment_Email_History__c> paymentEmailListDisplay1;   
    public List<rowWrapper> rowWrapperList{get;set;}
    public List<SelectOption> countryList{get; set;}
    
    public Boolean isCheckText {get;set;}
    public Boolean displayEmailText {get;set;}
    public boolean displayPopup {get; set;} 
    public string EnterEmail{get;set;}
    public string EmailValueEntered;
    public String checkedEmail {get;set;}
    Boolean boolChecker {get; set;}

    public List<OSM_Payment_Email_History__c> getpaymentEmailListDisplay1() {
     List<OSM_Payment_Email_History__c> paymentEmailListDisplay2 = new List<OSM_Payment_Email_History__c>(); 
     ordItem = [SELECT OrderItemNumber FROM OrderItem WHERE Id =:ApexPages.currentPage().getParameters().get('Id')];
         paymentEmailListDisplay2 = [SELECT  OSM_Order_Item_No__c, OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =:ordItem[0].OrderItemNumber];
     return paymentEmailListDisplay2 ;
    }
    
    public OSM_PaymentModeController(ApexPages.StandardController controller) {
        this.ordrItem = (OrderItem)controller.getRecord();

        countryList = new List<SelectOption>(); 
        Set<String> filterNames = new Set<String>();
        List<String> sortNames = new List<String>();
        //Get Country Names
        countryList.add(new SelectOption('', '--None--'));
        for(OSM_Country_and_State__c loopCountryState : [SELECT Id, OSM_Country_Name__c 
                                                FROM OSM_Country_and_State__c
                                                WHERE OSM_Active__c = true]){
            filterNames.add(loopCountryState.OSM_Country_Name__c);
        }
        if(filterNames.size() > 0){
            sortNames.addAll(filterNames);
            sortNames.sort();
            for(String loopCountryName : sortNames){
                countryList.add(new SelectOption(loopCountryName, loopCountryName));
            }
        }
         
         defaultordrItem = [Select OrderItemNumber, OSM_Patient_Last_Name__c, OSM_Patient_First_Name__c, OSM_Patient_DOB__c, OSM_Name_on_Cheque__c, OSM_Cheque_Number__c, OSM_Amount_of_Cheque__c, OSM_Wire_Transfer__c, OSM_Credit_Card__c, OSM_Cheque__c, OSM_Amount_Paid__c, OSM_Sending_Bank_Transfer_Number__c, OSM_Country__c From OrderItem Where Id = :  this.ordrItem.Id];
         editButton = false;
         displayEmailText = true;    
         
         rowWrapperList = new List<rowWrapper>();
         
         ordItem = [SELECT OrderItemNumber FROM OrderItem WHERE Id =:ApexPages.currentPage().getParameters().get('Id')];
         
         System.debug('>>>>> OrderItem.OrderItemNumber: ' + ordItem[0].OrderItemNumber);
         System.debug('>>>>> OSM_Payment_Email_History__c: ' + [SELECT OSM_Order_Item_No__c, OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_Item_No__c =:ordItem[0].OrderItemNumber]);
         for(OSM_Payment_Email_History__c peh: [SELECT OSM_Order_Item_No__c, OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_Item_No__c =:ordItem[0].OrderItemNumber]){
            rowWrapperList.add(new rowWrapper(peh));
        }
         
         if(defaultordrItem.OSM_Wire_Transfer__c) {
             isWire = true;
             this.ordrItem.OSM_Wire_Transfer__c = true;
             isCredit = false;
             isCheque = false;
         }
         
         else if(defaultordrItem.OSM_Credit_Card__c) {
             isCredit = true;
             this.ordrItem.OSM_Credit_Card__c = true;
             isWire = false;
             isCheque = false;
         }
         
          else if(defaultordrItem.OSM_Cheque__c) {
             isCheque = true;
             this.ordrItem.OSM_Cheque__c = true;
             isCredit = false;
             isWire = false;
         } 
    }
    
    public PageReference makePayment(){
        ApexPages.StandardController controller = new ApexPages.StandardController(ordrItem);
        controller.save();
        OSM_Ext_Urls__c cs = OSM_Ext_Urls__c.getOrgDefaults();                        
        PageReference pr = new PageReference(cs.cybersource__c); 
        return pr;
    }
    
    public PageReference send(){
        ApexPages.StandardController controller = new ApexPages.StandardController(ordrItem);
        try {
            this.ordrItem.OSM_Payment_Received__c = true;
            
            //Update related cases
            List<Case> updateCaseList = new List<Case>();
            Id preBillIdClose = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();
            for(Case caseLoop : [Select OSM_Self_Pay_Status__c, Status From Case Where OSM_Primary_Order_Line_Item__c =: this.ordrItem.Id And RecordTypeId =: preBillIdClose And Status = 'Open']){
                caseLoop.OSM_Self_Pay_Status__c = 'Payment received';
                caseLoop.Status = 'Close';
                updateCaseList.add(caseLoop);
            }
            
            if(updateCaseList.size() > 0){
                update updateCaseList;
            }
            
            OSM_Payment_Email_History__c eml = new OSM_Payment_Email_History__c();
            //defaultordrItem = [Select  Id, OrderItemNumber, OSM_Patient_Last_Name__c, OSM_Patient_First_Name__c, OSM_Patient_DOB__c From OrderItem Where Id =:this.ordrItem.id];
            eml.OSM_Order_Item_No__c = defaultordrItem.OrderItemNumber;
            eml.OSM_Amount_Paid__c = this.ordrItem.OSM_Amount_Paid__c;
            system.debug('Amount Paid is @@@@@@@@@@@@@ : ' + eml.OSM_Amount_Paid__c );
            eml.OSM_Country__c = this.ordrItem.OSM_Country__c;
            eml.OSM_Date_of_Birth__c = defaultordrItem.OSM_Patient_DOB__c;
            eml.OSM_Order_Line_ID__c = this.ordrItem.Id;
            eml.OSM_Patient_Name__c = defaultordrItem.OSM_Patient_First_Name__c + ' ' + defaultordrItem.OSM_Patient_Last_Name__c;
            eml.OSM_Sending_Bank_Transfer_No__c = this.ordrItem.OSM_Sending_Bank_Transfer_Number__c;
            
            insert eml;
            rowWrapperList.add(new rowWrapper(eml));
            
            controller.save();
            
            editButton = false;
            PageReference pr = new PageReference('/Apex/OSM_PaymentMode');
            return pr;
        }
        
        catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
      return controller.view();
 
    }
    public PageReference save(){
        ApexPages.StandardController controller = new ApexPages.StandardController(ordrItem);
        try {
            controller.save();
            
            editButton = false;
            PageReference pr = new PageReference('/Apex/OSM_PaymentMode');
            return pr;
        }
        
        catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
      return controller.view();
 
    }
    public pageReference editDetails(){
        editButton = true;
        ApexPages.StandardController controller = new ApexPages.StandardController(ordrItem);
        this.ordrItem.OSM_Payment_Received__c = false;
        controller.save();
        return null;
    }
    public pageReference customCancel(){
        defaultordrItem = new OrderItem();
        defaultordrItem = [Select OrderItemNumber, OSM_Patient_Last_Name__c, OSM_Patient_First_Name__c, OSM_Patient_DOB__c, OSM_Name_on_Cheque__c, OSM_Cheque_Number__c, OSM_Amount_of_Cheque__c, OSM_Wire_Transfer__c, OSM_Credit_Card__c, OSM_Cheque__c, OSM_Amount_Paid__c, OSM_Sending_Bank_Transfer_Number__c, OSM_Country__c, OSM_Card_Holder_Name__c, OSM_Card_Number__c, OSM_Card_Expiration_Date__c, OSM_Card_Type__c From OrderItem Where Id = :  this.ordrItem.Id];
        
        editButton = false;
        this.ordrItem.OSM_Amount_Paid__c = defaultordrItem.OSM_Amount_Paid__c;
        this.ordrItem.OSM_Sending_Bank_Transfer_Number__c = defaultordrItem.OSM_Sending_Bank_Transfer_Number__c;
        this.ordrItem.OSM_Country__c = defaultordrItem.OSM_Country__c;

        this.ordrItem.OSM_Card_Holder_Name__c = defaultordrItem.OSM_Card_Holder_Name__c;
        this.ordrItem.OSM_Card_Number__c = defaultordrItem.OSM_Card_Number__c;
        this.ordrItem.OSM_Card_Expiration_Date__c = defaultordrItem.OSM_Card_Expiration_Date__c;
        this.ordrItem.OSM_Card_Type__c = defaultordrItem.OSM_Card_Type__c;
        
        this.ordrItem.OSM_Name_on_Cheque__c = defaultordrItem.OSM_Name_on_Cheque__c;
        this.ordrItem.OSM_Cheque_Number__c = defaultordrItem.OSM_Cheque_Number__c;
        this.ordrItem.OSM_Amount_of_Cheque__c = defaultordrItem.OSM_Amount_of_Cheque__c;

        PageReference pr = new PageReference('/Apex/OSM_PaymentMode');
        return pr;
    }
    
    public void renderPaymentForm(){
        String formPayment = ApexPages.currentPage().getParameters().get('paymentMode');
        system.debug('***formPayment' + formPayment);
        
        if(formPayment == 'wire'){
             ordrItem.OSM_Wire_Transfer__c = true;
             ordrItem.OSM_Credit_Card__c = false;
             ordrItem.OSM_Cheque__c = false;
             isWire = true;
             isCredit = false;
             isCheque = false;
        }
        
        if(formPayment == 'credit'){
            this.ordrItem.OSM_Amount_Paid__c = null;
            this.ordrItem.OSM_Sending_Bank_Transfer_Number__c =  '';
            this.ordrItem.OSM_Country__c = '';
            this.ordrItem.OSM_Payment_Received__c = false; 
            
             ordrItem.OSM_Credit_Card__c = true;
             ordrItem.OSM_Wire_Transfer__c = false;
             ordrItem.OSM_Cheque__c = false;
             isWire = false;
             isCredit = true;
             isCheque = false;
        }
        
        if(formPayment == 'cheque'){
             ordrItem.OSM_Credit_Card__c = false;
             ordrItem.OSM_Wire_Transfer__c = false;
             ordrItem.OSM_Cheque__c = true;
             isWire = false;
             isCredit = false;
             isCheque = true;
        }
    }
     public void showPopup() {        
        displayPopup = true;    
    } 
    public void closePopup() {     
       // EmailValueEntered = EnterEmail;
       // system.debug('@@@@@@@'+store);
        displayPopup = false;    
    }     
    public PageReference SendEmail(){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        List<OSM_Payment_Email_History__c> selectedList = new List<OSM_Payment_Email_History__c>();

        system.debug('test checkbox ' + isCheckText );
        system.debug('test displayEmailText ' + displayEmailText);
        //for(OSM_Payment_Email_History__c paymentEmailListDisplay : [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_Id__c =: this.ordr.Id]){
        for(rowWrapper rw: rowWrapperList ){
            if(rw.rowId){
                selectedList.add(rw.e);
            }
        }
        
        system.debug('test selectedList ' + selectedList);
        
        /*List<OSM_Payment_Email_History__c> testList = [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =: this.ordr.Id];
        system.debug('test list ' + testList );
        for(OSM_Payment_Email_History__c paymentEmailListDisplay : [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_Id__c =: this.ordr.Id]){
        //List<OSM_Payment_Email_History__c> paymentEmailListDisplay = [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =: this.ordr.Id];
            */
        if(selectedList.size() > 0){
            for(OSM_Payment_Email_History__c paymentEmailListDisplay : selectedList){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                List<String> sendTo = new List<String>();
                //Map<String, Email__c> mcs = Email__c.getAll();
    
                 
                //send to:
                system.debug('test EnterEmail ' + EnterEmail);
                EmailValueEntered = EnterEmail;
                displayPopup = false;
                system.debug('test EmailValueEntered '+ EmailValueEntered );
                if(EmailValueEntered != null && EmailValueEntered != ''){
                    system.debug('test enter email');
                    sendTo.add(EmailValueEntered );
                
                    mail.setToAddresses(sendTo);
                    system.debug('Email is sent to &&&&&&&&&&&&'+sendTo );
                          
                    //body
                    mail.setSubject('Wire Transfer');
                    //String body = 'Hi Veena';
                    String theDate = paymentEmailListDisplay.OSM_Date_of_Birth__c == null ? '--' : String.Valueof(paymentEmailListDisplay.OSM_Date_of_Birth__c);
                    String body = 'Order Line Id          : ' + paymentEmailListDisplay.OSM_Order_Item_No__c+ '<br/>';
                    body += 'Patient Name          : ' + paymentEmailListDisplay.OSM_Patient_Name__c + '<br/>';
                    body += 'Date of Birth          : ' + theDate + '<br/>';
                    body += 'Amount Paid          : ' + paymentEmailListDisplay.OSM_Amount_Paid__c + '<br/>';
                    body += 'Sending Bank/Transfer No.          : ' + paymentEmailListDisplay.OSM_Sending_Bank_Transfer_No__c + '<br/>';
                    body += 'Country          : ' + paymentEmailListDisplay.OSM_Country__c + '<br/>';
                    mail.setHtmlBody(body);
                    
                    
                    mails.add(mail);
                    system.debug('test email ' + mails);
                }
            }
            if(!Test.IsRunningTest()) {
                Messaging.sendEmail(mails);
            }
            system.debug('Email is sent&&&&&&&&&&&&'+mails );
        }
        /*else{
            for(OSM_Payment_Email_History__c paymentEmailListDisplay : [SELECT OSM_Amount_Paid__c, OSM_Country__c, OSM_Date_of_Birth__c, OSM_Order_Line_ID__c, OSM_Order_No__c, OSM_Patient_Name__c, OSM_Sending_Bank_Transfer_No__c FROM OSM_Payment_Email_History__c WHERE OSM_Order_No__c =: this.ordr.Id]){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                List<String> sendTo = new List<String>();
                Map<String, Email__c> mcs = Email__c.getAll();
    
                 
                //send to:
                EmailValueEntered = EnterEmail;
                displayPopup = false;
                sendTo.add(EmailValueEntered );
                mail.setToAddresses(sendTo);
                system.debug('Email is sent to &&&&&&&&&&&&'+sendTo );
                      
                //body
                mail.setSubject('Wire Transfer');
                String body = 'Hi Veena';
                String body = 'Order No. : ' + paymentEmailListDisplay.OSM_Order_No__c + '<br/>';
                body += 'Order Line Id          : ' + paymentEmailListDisplay.OSM_Order_Line_ID__c + '<br/>';
                body += 'Patient Name          : ' + paymentEmailListDisplay.OSM_Patient_Name__c + '<br/>';
                body += 'Date of Birth          : ' + paymentEmailListDisplay.OSM_Date_of_Birth__c + '<br/>';
                body += 'Amount Paid          : ' + paymentEmailListDisplay.OSM_Amount_Paid__c + '<br/>';
                body += 'Sending Bank/Transfer No.          : ' + paymentEmailListDisplay.OSM_Sending_Bank_Transfer_No__c + '<br/>';
                body += 'Country          : ' + paymentEmailListDisplay.OSM_Country__c + '<br/>';
                mail.setHtmlBody(body);
                
                
                mails.add(mail);
                system.debug('test email ' + mails);
                Messaging.sendEmail(mails);
                system.debug('Email is sent&&&&&&&&&&&&'+mails );
            }
        }*/
       
        
        system.debug('Email is sent#########' );
        PageReference pr = new PageReference('/Apex/OSM_PaymentMode');
            return pr;
    }
    public class rowWrapper{
        public Boolean rowId {get; set;}
        public OSM_Payment_Email_History__c e {get; set;}
        
        public rowWrapper(OSM_Payment_Email_History__c e){
            this.rowId = false;
            this.e = e;
        }
    }
}