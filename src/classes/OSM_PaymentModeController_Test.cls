/**
 * File Info
 * ----------------------------------
 * @filename       OSM_PaymentModeController_Test.cls
 * @author         Francis Nasalita
 * @description    Test class for OSM_PaymentModeController. 
 * @history        12.JAN.2015 - Francis Nasalita - Created  
 */
@isTest(seeAllData=false)
private class OSM_PaymentModeController_Test {
    
    private static testMethod void voidCheque() {
        /* START SETUP */
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        //Id rtCase = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();

        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;

        Order od = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od.Pricebook2Id=Test.getStandardPriceBookId();
        insert od;
        od.RecordTypeId = rt;
        od.OSM_Credit_Card__c = true;
        od.OSM_Bill_Type__c = 'Patient Pre-Pay';
        update od;
        
        Order od1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od1.Pricebook2Id=Test.getStandardPriceBookId();
        insert od1;
        
        od1.OSM_Order__c = od.Id;
        update od1;
        
        OSM_Work_Order__c workOrder = new OSM_Work_Order__c(OSM_Order__c = od.Id);
        insert workOrder;
        
        //PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, Test.getStandardPriceBookId(), 7);
        insert pbe;
        
        OrderItem ordrItm1 = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today(), workOrder.Id);
        insert ordrItm1;
        ordrItm1.OSM_Cheque__c = true;
        update ordrItm1;
    
        PageReference pageRef = new PageReference('/apex/OSM_PaymentMode?Id=' + ordrItm1.Id + '&paymentMode=cheque');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ordrItm1);
        OSM_PaymentModeController controller = new OSM_PaymentModeController(sCon);
        
        //Custom Setting
        OSM_Ext_Urls__c extURL = new OSM_Ext_Urls__c(clerisofturl__c='www.test.com',Cybersource__c='www.test2.com');
        insert extURL;
        /* END SETUP */
        
        /* START TEST */
        Test.startTest();
            controller.renderPaymentForm();
            PageReference pagRef = controller.makePayment();
        Test.stopTest();
        /* END TEST */
        
        /* START VERIFICATION */
        //System.assertEquals('https://ebc.cybersource.com/ebc/login/Login.do;jsessionid=5EA6FE6E3D508E6741AEF00FD0E1B6FA.elpw001-eb',pagRef.getUrl(),'');
        /* END VERIFICATION */
    }
    
    private static testMethod void TestMakePayment() {
        /* START SETUP */
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        //Id rtCase = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();

        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;

        Order od = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od.Pricebook2Id=Test.getStandardPriceBookId();
        insert od;
        od.RecordTypeId = rt;
        od.OSM_Credit_Card__c = true;
        od.OSM_Bill_Type__c = 'Patient Pre-Pay';
        update od;
        
        Order od1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od1.Pricebook2Id=Test.getStandardPriceBookId();
        insert od1;
        
        od1.OSM_Order__c = od.Id;
        update od1;
        
        OSM_Work_Order__c workOrder = new OSM_Work_Order__c(OSM_Order__c = od.Id);
        insert workOrder;
        
        //PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, Test.getStandardPriceBookId(), 7);
        insert pbe;
        
        OrderItem ordrItm1 = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today(), workOrder.Id);
        insert ordrItm1;
        ordrItm1.OSM_Credit_Card__c = true;
        update ordrItm1;
    
        PageReference pageRef = new PageReference('/apex/OSM_PaymentMode?Id=' + ordrItm1.Id + '&paymentMode=credit');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ordrItm1);
        OSM_PaymentModeController controller = new OSM_PaymentModeController(sCon);
        //Custom Setting
        OSM_Ext_Urls__c extURL = new OSM_Ext_Urls__c(clerisofturl__c='www.test.com',Cybersource__c='www.test2.com');
        insert extURL;
        /* END SETUP */
        
        /* START TEST */
        Test.startTest();
            controller.renderPaymentForm();
            PageReference pagRef = controller.makePayment();
        Test.stopTest();
        /* END TEST */
        
        /* START VERIFICATION */
        //System.assertEquals('https://ebc.cybersource.com/ebc/login/Login.do;jsessionid=5EA6FE6E3D508E6741AEF00FD0E1B6FA.elpw001-eb',pagRef.getUrl(),'');
        /* END VERIFICATION */
    }
    
    private static testMethod void TestWireTransfer() {
        /* START SETUP */
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();

        Account acct = OSM_DataFactory.createAccount('Test');
        insert acct;
        
        Contact ctct = OSM_DataFactory.createContact('Test', 'Test', rtContact);
        insert ctct;
        
        Order od = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od.Pricebook2Id=Test.getStandardPriceBookId();
        insert od;
        od.RecordTypeId = rt;
        od.OSM_Wire_Transfer__c = true;
        update od;
        
        Order od1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        od1.Pricebook2Id=Test.getStandardPriceBookId();
        insert od1;
        
        od1.OSM_Order__c = od.Id;
        update od1;
        
        OSM_Work_Order__c workOrder = new OSM_Work_Order__c(OSM_Order__c = od.Id);
        insert workOrder;
        
        //PriceBook2 pb2 = [SELECT Id FROM PriceBook2 WHERE isStandard = true];
        
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, Test.getStandardPriceBookId(), 7);
        insert pbe;
        
        OrderItem odItem = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today(), workOrder.Id);
        insert odItem;
        
        odItem.OSM_Wire_Transfer__c = true;
        update odItem;
        
        List<OrderItem> orderItemList = new List<OrderItem>();
        OrderItem ordrItm1 = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today(), workOrder.Id);
        orderItemList.add(ordrItm1);
        OrderItem ordrItm2 = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today(), workOrder.Id);
        orderItemList.add(ordrItm2);
        OrderItem ordrItm3 = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today(), workOrder.Id);
        orderItemList.add(ordrItm3);
        insert orderItemList;

        od.OSM_Order_Line_Items__c = orderItemList[2].OSM_Order_Line_Item_ID__c + ',' + orderItemList[2].OSM_Order_Line_Item_ID__c;
        update od;
        
        
        OSM_Payment_Email_History__c eml = new OSM_Payment_Email_History__c();
        eml.OSM_Order_Item_No__c = orderItemList[0].OrderItemNumber;
        eml.OSM_Amount_Paid__c = orderItemList[0].OSM_Amount_Paid__c;
        eml.OSM_Country__c = orderItemList[0].OSM_Country__c;
        eml.OSM_Date_of_Birth__c = orderItemList[0].OSM_Patient_DOB__c;
        eml.OSM_Order_Line_ID__c = orderItemList[0].Id;
        eml.OSM_Patient_Name__c = orderItemList[0].OSM_Patient_First_Name__c + ' ' + orderItemList[0].OSM_Patient_Last_Name__c;
        eml.OSM_Sending_Bank_Transfer_No__c = orderItemList[0].OSM_Sending_Bank_Transfer_Number__c;
        
        insert eml;
        
        Case caseObj = OSM_DataFactory.createCase('Open','Benefits Investigation');
        caseObj.RecordTypeId = rtCase;
        caseObj.OSM_Primary_Order_Line_Item__c = odItem.Id;
        insert caseObj;
        
        PageReference pageRef = new PageReference('/apex/OSM_PaymentMode?Id=' + orderItemList[0].Id + '&paymentMode=wire');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(odItem);
        OSM_PaymentModeController controller = new OSM_PaymentModeController(sCon);
        /* END SETUP */
        
        /* START TEST */
        Test.startTest();
        List<OSM_Payment_Email_History__c> pehList =  controller.getpaymentEmailListDisplay1();
        
        controller.editDetails();
        controller.customCancel();
        controller.editDetails();
        controller.send();
        controller.rowWrapperList[0].rowId = true;
        controller.EnterEmail = 'francis.nasalita@cloudsherpas.com';
        controller.SendEmail();
        controller.SendEmail();
        controller.showPopup();
        controller.closePopup();
        controller.renderPaymentForm();
        controller.save();
        Test.stopTest();
        /* END TEST */
        
        /* START VERIFICATION 
        System.AssertEquals(true,odItem.OSM_Wire_Transfer__c);
        System.AssertEquals(false,odItem.OSM_Credit_Card__c);
        System.AssertEquals(false,odItem.OSM_Cheque__c);
        System.AssertEquals(true,controller.isWire);
        System.AssertEquals(false,controller.isCredit);
        System.AssertEquals(false,controller.isCheque);
         END VERIFICATION */
    }
    
}