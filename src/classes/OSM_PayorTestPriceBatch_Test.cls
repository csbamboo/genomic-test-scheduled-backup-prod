/*
  @author: Jerome Liwanag
  @date: 30 July 2015
  @description: Test Class for OSM_PayorTestPriceBatch apex class
  @history: 30 July 2015 - Created (Jerome Liwanag)

*/
@isTest(seeAllData=false)
private class OSM_PayorTestPriceBatch_Test {

    private static testMethod void testMethod1() 
    {
        Id agreementRecordTypeId  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('International Private').getRecordTypeId();
        Id payorAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Account payorAccount = OSM_DataFactory.createPayorAccountWithBillingAddress(2,'2','Private','United States','Test Street','Test City','Alaska', 
                                                                                    '123',payorAccountId);
        payorAccount.Name = 'Acct1234';
        insert payorAccount;
        
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(Apttus__Account__c=payorAccount.Id,GHI_CLM_Agreement_SubType__c='Asia Pacific',recordTypeId = agreementRecordTypeId);
        
        insert agreement;
        Apttus__AgreementLineItem__c agreementOLI = new Apttus__AgreementLineItem__c(Apttus_CMConfig__ItemSequence__c = 2, Apttus_CMConfig__LineNumber__c = 1, Apttus__AgreementId__c = agreement.Id);
        insert agreementOLI;
        
        Account payorAccount2 = OSM_DataFactory.createPayorAccountWithBillingAddress(3,'2','Private','United States','Test Street','Test City','Alaska', 
                                                                                    '123',payorAccountId);
        payorAccount2.Name = 'Acct1234';
        payorAccount2.OSM_Status__c = 'Approved';
        payorAccount2.GHI_CLM_Agreement_ID__c = agreement.Id;
        insert payorAccount2;
        system.debug('/n/npayorAccount2' + payorAccount2);
        
        Date ptpStartDate = Date.today().addDays(10);
        
        OSM_Payor_Test_Price__c ptp = OSM_DataFactory.createPTP(payorAccount.Id, null, false, null, null, null );
        ptp.OSM_Start_Date__c  = ptpStartDate;
        ptp.OSM_Agreement_Line_Item_ID__c = agreementOLI.Id;
        insert ptp;
        
        String payorTestPrice = ptp.id;
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'OSM_Payor_Test_Price__c', Record_Ids__c = payorTestPrice);
        insert errInBatch;
        
        OSM_PayorTestPriceBatch tr = new OSM_PayorTestPriceBatch();
        tr.IsReRun = true;
        
        OSM_PayorTestPriceBatch.MyWrapper a = new OSM_PayorTestPriceBatch.MyWrapper(ptp.Id, ptp.Name,'errors');
        tr.wrapperList.add(a);
        
        tr.parseErrorReturnId('String errorMsgString errorMsgString errorMsgString errorMsgString errorMsg');
        Test.startTest();
        Database.executeBatch(tr);
        Test.stopTest();  
     }
     
     private static testMethod void testMethod2() 
    {
        Id agreementRecordTypeId  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('International Private').getRecordTypeId();
        Id agreementRecordTypeId2  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('US Private').getRecordTypeId();
        Id payorAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
       
        Account payorAccount = OSM_DataFactory.createPayorAccountWithBillingAddress(2,'2','Private','United States','Test Street','Test City','Alaska', 
                                                                                    '123',payorAccountId);
        payorAccount.Name = 'Acct1234';
        insert payorAccount;
        
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(Apttus__Account__c=payorAccount.Id,GHI_CLM_Agreement_SubType__c='Asia Pacific',recordTypeId = agreementRecordTypeId);
        //agreement.Name = 'Payor Pricing Agreement';
        insert agreement;
        
        Record_Type__c recordTypes = new Record_Type__c();
        recordTypes.Default_Agreement_Pricing_ID__c = agreement.Id;
        insert recordTypes;
        Product2 testProd2 = OSM_DataFactory.createProduct('IBC', true);
        insert testProd2;
        Apttus__AgreementLineItem__c agreementOLI = new Apttus__AgreementLineItem__c(Apttus_CMConfig__ItemSequence__c = 2, Apttus_CMConfig__LineNumber__c = 1, Apttus__AgreementId__c = agreement.Id,
        Apttus__ProductId__c = testProd2.Id);
        insert agreementOLI;
        
        
        
        agreement.Name = 'Payor Pricing Agreement';
        update agreement;
        
        Account payorAccount2 = OSM_DataFactory.createPayorAccountWithBillingAddress(3,'2','Private','United States','Test Street','Test City','Alaska', 
                                                                                    '123',payorAccountId);
        payorAccount2.Name = 'Acct1234';
        payorAccount2.OSM_Status__c = 'Approved';
        payorAccount2.GHI_CLM_Agreement_ID__c = agreement.Id;
        insert payorAccount2;
        
        Apttus__APTS_Agreement__c agreement2 = new Apttus__APTS_Agreement__c(Apttus__Account__c=payorAccount2.Id,GHI_CLM_Agreement_SubType__c='Asia Pacific',recordTypeId = agreementRecordTypeId);
        //agreement.Name = 'Payor Pricing Agreement';
        insert agreement2;
        Apttus__AgreementLineItem__c agreementOLI2 = new Apttus__AgreementLineItem__c(Apttus_CMConfig__ItemSequence__c = 2, Apttus_CMConfig__LineNumber__c = 1, Apttus__AgreementId__c = agreement2.Id,
        Apttus__ProductId__c = testProd2.Id);
        insert agreementOLI2;
        
        Date ptpStartDate = Date.today().addDays(10);
        
        OSM_Payor_Test_Price__c ptp2 = OSM_DataFactory.createPTP(payorAccount2.Id, null, false, null, null, null );
        ptp2.OSM_Start_Date__c  = ptpStartDate;
        ptp2.OSM_Agreement_ID__c  = agreement.Id;
        ptp2.OSM_Agreement_Line_Item_ID__c = agreementOLI.Id;
        ptp2.OSM_Payor__c = payorAccount2.Id;
        ptp2.OSM_Test__c = testProd2.Id;
        insert ptp2;
        
        OSM_Payor_Test_Price__c ptp = OSM_DataFactory.createPTP(payorAccount2.Id, null, false, null, null, null );
        ptp.OSM_Start_Date__c  = ptpStartDate;
        ptp.OSM_Agreement_ID__c  = agreement.Id;
        ptp.OSM_Agreement_Line_Item_ID__c = agreementOLI.Id;
        ptp.OSM_Payor__c = payorAccount.Id;
        ptp.OSM_Test__c = testProd2.Id;
        ptp.OSM_Newly_Generated__c  = true;
        insert ptp;
        
        OSM_Payor_Test_Price__c ptp3 = OSM_DataFactory.createPTP(payorAccount2.Id, null, false, null, null, null );
        ptp3.OSM_Start_Date__c  = ptpStartDate;
        ptp3.OSM_Agreement_ID__c  = null;
        ptp3.OSM_Agreement_Line_Item_ID__c = agreementOLI2.Id;
        ptp3.OSM_Payor__c = payorAccount2.Id;
        //ptp3.OSM_Test__c = testProd2.Id;
        ptp3.OSM_Newly_Generated__c  = true;
        insert ptp3;
               
        OSM_PayorTestPriceBatch tr = new OSM_PayorTestPriceBatch();
        tr.IsReRun = false;
        
        Test.startTest();
        Database.executeBatch(tr);
        Test.stopTest();  
     }
     private static testMethod void testMethod3() 
    {
        Id agreementRecordTypeId  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('International Private').getRecordTypeId();
        Id agreementRecordTypeId2  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('US Private').getRecordTypeId();
        Id payorAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
       
        Account payorAccount = OSM_DataFactory.createPayorAccountWithBillingAddress(2,'2','Private','United States','Test Street','Test City','Alaska', 
                                                                                    '123',payorAccountId);
        payorAccount.Name = 'Acct1234';
        insert payorAccount;
        
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(Apttus__Account__c=payorAccount.Id,GHI_CLM_Agreement_SubType__c='Asia Pacific',recordTypeId = agreementRecordTypeId);
        //agreement.Name = 'Payor Pricing Agreement';
        insert agreement;
        
        Record_Type__c recordTypes = new Record_Type__c();
        recordTypes.Default_Agreement_Pricing_ID__c = agreement.Id;
        insert recordTypes;
        
        agreement.Name = 'Payor Pricing Agreement';
        update agreement;
        
        Account payorAccount2 = OSM_DataFactory.createPayorAccountWithBillingAddress(3,'2','Private','United States','Test Street','Test City','Alaska', 
                                                                                    '123',payorAccountId);
                                                                                    
        
        
        agreement.Apttus__Account__c = payorAccount2.Id;
        update agreement;   
        
        Product2 testProd2 = OSM_DataFactory.createProduct('IBC', true);
        insert testProd2;
        
        Apttus__AgreementLineItem__c agreementOLI = new Apttus__AgreementLineItem__c(
        Apttus_CMConfig__ItemSequence__c = 2, 
        Apttus_CMConfig__LineNumber__c = 1, 
        Apttus__AgreementId__c = agreement.Id,
        Apttus__ProductId__c = testProd2.Id
        );
        insert agreementOLI;
    
        payorAccount2.Name = 'Acct1234';
        payorAccount2.OSM_Status__c = 'Approved';
        payorAccount2.GHI_CLM_Agreement_ID__c = agreement.Id;
        insert payorAccount2;
        
        Apttus__APTS_Agreement__c agreement2 = new Apttus__APTS_Agreement__c(Apttus__Account__c=payorAccount2.Id,GHI_CLM_Agreement_SubType__c='Asia Pacific',recordTypeId = agreementRecordTypeId);
        insert agreement2;
        
        Apttus__AgreementLineItem__c agreementOLI2 = new Apttus__AgreementLineItem__c(
        Apttus_CMConfig__ItemSequence__c = 2,
        Apttus_CMConfig__LineNumber__c = 1,
        Apttus__AgreementId__c = agreement2.Id,
        Apttus__ProductId__c = testProd2.Id
        );
        insert agreementOLI2;
        
        Apttus__AgreementLineItem__c agreementOLI3 = new Apttus__AgreementLineItem__c(
        Apttus_CMConfig__ItemSequence__c = 2,
        Apttus_CMConfig__LineNumber__c = 1,
        Apttus__AgreementId__c = agreement2.Id,
        Apttus__ProductId__c = testProd2.Id
        );
        insert agreementOLI3;
        
        Apttus__AgreementLineItem__c agreementOLI4 = new Apttus__AgreementLineItem__c(
        Apttus_CMConfig__ItemSequence__c = 2,
        Apttus_CMConfig__LineNumber__c = 1,
        Apttus__AgreementId__c = agreement.Id
        //Apttus__ProductId__c = testProd2.Id
        );
        insert agreementOLI4;
        
        Date ptpStartDate = Date.today().addDays(10);
        
        OSM_Payor_Test_Price__c ptp2 = OSM_DataFactory.createPTP(payorAccount2.Id, null, false, null, null, null );
        ptp2.OSM_Start_Date__c  = ptpStartDate;
        ptp2.OSM_Agreement_ID__c  = agreement2.Id;
        ptp2.OSM_Agreement_Line_Item_ID__c = agreementOLI.Id;
        ptp2.OSM_Payor__c = payorAccount2.Id;
        ptp2.OSM_Test__c = testProd2.Id;
        insert ptp2;
        
        OSM_Payor_Test_Price__c ptp = OSM_DataFactory.createPTP(payorAccount2.Id, null, false, null, null, null );
        ptp.OSM_Start_Date__c  = ptpStartDate;
        ptp.OSM_Agreement_ID__c  = agreement2.Id;
        ptp.OSM_Agreement_Line_Item_ID__c = agreementOLI.Id;
        ptp.OSM_Payor__c = payorAccount2.Id;
        ptp.OSM_Test__c = testProd2.Id;
        insert ptp;
        
        OSM_Payor_Test_Price__c ptp3 = OSM_DataFactory.createPTP(payorAccount2.Id, null, false, null, null, null );
        ptp3.OSM_Start_Date__c  = ptpStartDate;
        ptp3.OSM_Agreement_ID__c  = null;
        ptp3.OSM_Agreement_Line_Item_ID__c = agreementOLI3.Id;
        ptp3.OSM_Payor__c = payorAccount2.Id;
        ptp3.OSM_Test__c = testProd2.Id;
        insert ptp3;
        
        OSM_Payor_Test_Price__c ptp4 = OSM_DataFactory.createPTP(payorAccount2.Id, null, false, null, null, null );
        ptp4.OSM_Start_Date__c  = ptpStartDate;
        ptp4.OSM_Agreement_ID__c  = agreement.Id;
        ptp4.OSM_Agreement_Line_Item_ID__c = agreementOLI4.Id;
        ptp4.OSM_Payor__c = payorAccount2.Id;
        ptp4.OSM_Test__c = testProd2.Id;
        insert ptp4;
        
        OSM_Payor_Test_Price__c ptp5 = OSM_DataFactory.createPTP(payorAccount2.Id, null, false, null, null, null );
        ptp5.OSM_Start_Date__c  = ptpStartDate;
        ptp5.OSM_Agreement_ID__c  = null;
        ptp5.OSM_Agreement_Line_Item_ID__c = null;
        ptp5.OSM_Payor__c = payorAccount2.Id;
        ptp5.OSM_Test__c = testProd2.Id;
        insert ptp5;
               
        OSM_PayorTestPriceBatch tr = new OSM_PayorTestPriceBatch();
        tr.IsReRun = false;
        
        Test.startTest();
        Database.executeBatch(tr);
        Test.stopTest();  
     }

}