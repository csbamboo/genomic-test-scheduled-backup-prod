/*
@author: patrick lorilla
@description: Payor Test Price Trigger Handler
@date: 28-APR-2015
*/

public class OSM_PayorTestPriceTriggerHandler{

    public static boolean makeFieldsRequiredRunOnce = false;
    public static boolean makeRecordStaleRunOnce = false;
    
    public static void onBeforeUpdate(List<OSM_Payor_Test_Price__c> newPTPList, Map<Id, OSM_Payor_Test_Price__c> oldPTPMap ){
        List<OSM_Payor_Test_Price__c> makeStaleList = new List<OSM_Payor_Test_Price__c>();
        for(OSM_Payor_Test_Price__c ptp: newPTPList){
            if(ptp.OSM_Start_Date__c != oldPTPMap.get(ptp.Id).OSM_Start_Date__c || ptp.OSM_End_Date__c != oldPTPMap.get(ptp.Id).OSM_End_Date__c ){
                makeStaleList.add(ptp);    
            }
        }
        if(!newPTPList.isEmpty()){
            if(!makeFieldsRequiredRunOnce){
                makeFieldsRequiredRunOnce = true;
                makeFieldsRequired(newPTPList);
            }
       
            if(!makeRecordStaleRunOnce){
                makeRecordStaleRunOnce =true;
                System.debug('\n\n\n chk1');
                makeRecordStale(makeStaleList) ;   
            }
        }
    }
    
    public static void onBeforeInsert(List<OSM_Payor_Test_Price__c> newPTPList){
        if(!newPTPList.isEmpty()){
            if(!makeFieldsRequiredRunOnce){
                makeFieldsRequiredRunOnce = true;
                makeFieldsRequired(newPTPList);
            }
        }
        
    }
    public static void makeRecordStale(List<OSM_Payor_Test_Price__c> newPTPList){
        System.debug('\n\n\n chk2');
        for(OSM_Payor_Test_Price__c ptp: newPTPList){
            ptp.OSM_Newly_Generated__c = false;
        }        
    }
    public static void makeFieldsRequired(List<OSM_Payor_Test_Price__c> newPTPList){
        Set<ID> ptpIds = new Set<ID>();
        boolean contReqFError = false;
        boolean basicReqFError = false;
        for(OSM_Payor_Test_Price__c ptp: newPTPList){
            ptpIds.add(ptp.Id);
        }
        List<Apttus__APTS_Agreement__c> newPTPListName = [SELECT Id from Apttus__APTS_Agreement__c where Name = 'Payor Pricing Agreement' LIMIT 1];
        for(OSM_Payor_Test_Price__c ptp: newPTPList){
            if(ptp.OSM_Agreement_ID__c == newPTPListName[0].Id){
               if(ptp.OSM_Agreement_Line_Item_ID__c == null){
                   basicReqFError = true;
               }
               //if(ptp.OSM_List_Price__c == null){
                  //basicReqFError = true;
              // }
               if(ptp.OSM_Billing_Category__c == null){
                   
                   basicReqFError = true;
               }
               if(ptp.OSM_CPT_Code__c == null){
                   basicReqFError = true;
               }
               if(ptp.CurrencyIsoCode == null){
                  basicReqFError = true;
               }
               if(ptp.OSM_Billing_Cycle__c== null){
                   basicReqFError = true;
               }
               if(ptp.OSM_Price_Method__c == null){
                  basicReqFError = true;
               }
            }
            else{
                if(ptp.OSM_Agreement_Line_Item_ID__c == null){
                    System.debug('\n\n\n\n conterr1');
                    contReqFError = true;
               }
               if(ptp.OSM_List_Price__c == null){
                   System.debug('\n\n\n\n conterr2');
                    contReqFError = true;
               }
               if(ptp.OSM_Billing_Category__c == null){
                   system.debug('\n\n\n\n conterr3');
                   contReqFError = true;
               }
               if(ptp.OSM_CPT_Code__c == null){
                   system.debug('\n\n\n\n conterr4');
                   contReqFError = true;
               }
               if(ptp.CurrencyIsoCode == null){
                   system.debug('\n\n\n\n conterr5');
                   contReqFError = true;
               }
               if(ptp.OSM_Billing_Cycle__c == null){
                   system.debug('\n\n\n\n conterr6');
                   contReqFError = true;
               }
               if(ptp.OSM_Pricing_Schema__c == null){
                   system.debug('\n\n\n\n conterr7');
                   contReqFError = true;
               }
               if(ptp.OSM_Net_Price__c == null){
                   system.debug('\n\n\n\n conterr8');
                  contReqFError = true;
               }
               if(ptp.OSM_Start_Date__c == null){
                   system.debug('\n\n\n\n conterr9');
                   contReqFError = true;
               }
               //if(ptp.OSM_End_Date__c == null){
                 // contReqFError = true;
               //}
               if(ptp.OSM_Price_Method__c == null){
                   system.debug('\n\n\n\n conterr10');
                   contReqFError = true;
               }
            }
            
            if(contReqFError){
              ptp.addError(Label.OSM_Contracted_Required_Field);
            }else if(basicReqFError){
              ptp.addError(Label.OSM_Basic_Required_Field);
            }
        }
    }

}