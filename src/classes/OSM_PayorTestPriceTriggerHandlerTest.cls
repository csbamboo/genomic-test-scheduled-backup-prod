/*
@author: patrick lorilla
@description: Payor Test Price Trigger Handler Test
@date: 24-JUNE-2015
*/

@isTest(SeeAllData=true) 
public class OSM_PayorTestPriceTriggerHandlerTest {
    
   /* @testSetup static void setup() {
       
        for(Integer a=0;){
            
        }
    }*/
     static testmethod void testPTPValidationContracted() {
        List<Apttus__APTS_Agreement__c> newPTPListName = [SELECT Id from Apttus__APTS_Agreement__c where Name = 'Payor Pricing Agreement' LIMIT 1];
        List<Apttus__AgreementLineItem__c> dummyALIs =[SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement'];
        List<Apttus__APTS_Agreement__c> realALIs = new List<Apttus__APTS_Agreement__c>();
        List<OSM_Payor_Test_Price__c> contractedPTP = new List<OSM_Payor_Test_Price__c>();
        List<OSM_Payor_Test_Price__c> basicPTP = new List<OSM_Payor_Test_Price__c>();
        Apttus__APTS_Agreement__c agreementTest = newPTPListName[0].clone(false, true, false, false);
        agreementTest.Name = 'Test Agreement';
        insert agreementTest;
        for(Integer a=0; a<20; a++){
            contractedPTP.add(OSM_DataFactory.createPTP(null, null, false,agreementTest.Id, dummyALIs[0].Id, null ));    
            if(a < 2){
                contractedPTP[a].OSM_Agreement_Line_Item_ID__c = null;
            }
            else if (a< 4){
                contractedPTP[a].OSM_Billing_Category__c = null;
            }
            else if (a< 6){
                contractedPTP[a].OSM_CPT_Code__c = null;
            }   
            else if (a< 8){
                contractedPTP[a].CurrencyIsoCode = null;
            }  
            else if (a< 10){
                contractedPTP[a].OSM_Billing_Cycle__c = null;
            } 
            else if (a< 12){
                contractedPTP[a].OSM_Pricing_Schema__c = null;
            } 
            else if (a< 14){
                contractedPTP[a].OSM_Net_Price__c = null;
            } 
            else if (a< 16){
                contractedPTP[a].OSM_Start_Date__c = null;
            } 
             else if (a< 18){
                contractedPTP[a].OSM_List_Price__c = null;
            } 
            else if (a< 20){
                contractedPTP[a].OSM_Price_Method__c = null;
            } 
        }
        try{
            insert contractedPTP;
        }
        catch(Exception ex){
            System.assert(ex.getMessage().indexOf('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, ALI ID, List Price, Billing Category, Billing Cycle, Pricing Method, CPT Code, Currency, Pricing Schema, Contracted Price as required fields for a contracted PTP') != -1);
        }
    }
    
    static testmethod void testPTPValidationBasic() {
        List<Apttus__APTS_Agreement__c> newPTPListName = [SELECT Id from Apttus__APTS_Agreement__c where Name = 'Payor Pricing Agreement' LIMIT 1];
        List<Apttus__AgreementLineItem__c> dummyALIs =[SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement'];

         List<OSM_Payor_Test_Price__c> basicPTP = new List<OSM_Payor_Test_Price__c>();

        for(Integer a=0; a<20; a++){
            basicPTP.add(OSM_DataFactory.createPTP(null, null, false,newPTPListName[0].Id,dummyALIs[0].Id, null ));    
            if(a < 2){
                basicPTP[a].OSM_Agreement_Line_Item_ID__c = null;
            }
            else if (a< 4){
                basicPTP[a].OSM_Billing_Category__c = null;
            }
            else if (a< 6){
                basicPTP[a].OSM_CPT_Code__c = null;
            }   
            else if (a< 8){
                basicPTP[a].CurrencyIsoCode = null;
            }  
            else if (a< 10){
                basicPTP[a].OSM_Billing_Cycle__c = null;
            } 
            else if (a< 12){
                basicPTP[a].OSM_Pricing_Schema__c = null;
            } 
            else if (a< 14){
                basicPTP[a].OSM_Net_Price__c = null;
            } 
            else if (a< 16){
                basicPTP[a].OSM_Start_Date__c = null;
            } 
             else if (a< 18){
                basicPTP[a].OSM_List_Price__c = null;
            } 
            else if (a< 20){
                basicPTP[a].OSM_Price_Method__c = null;
            } 
        }
        try{
            insert basicPTP;
        }
        catch(Exception ex){
            System.assert(ex.getMessage().indexOf('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, ALI ID, List Price, Billing Category, Billing Cycle,Pricing Method, CPT Code, and Currency are required fields for a basic PTP') != -1);
        }
    }
    
    static testmethod void testPTPValidationBasicUpdate() {
        List<Apttus__APTS_Agreement__c> newPTPListName = [SELECT Id from Apttus__APTS_Agreement__c where Name = 'Payor Pricing Agreement' LIMIT 1];
        List<Apttus__AgreementLineItem__c> dummyALIs =[SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement'];

        List<OSM_Payor_Test_Price__c> basicPTP = new List<OSM_Payor_Test_Price__c>();
        for(Integer a=0; a<20; a++){
             basicPTP.add(OSM_DataFactory.createPTP(null, null, false,newPTPListName[0].Id,dummyALIs[0].Id, null ));    
        }
        
        insert basicPTP;
        
        for(Integer a=0; a<20; a++){
           
            if(a < 2){
                basicPTP[a].OSM_Agreement_Line_Item_ID__c = null;
            }
            else if (a< 4){
                basicPTP[a].OSM_Billing_Category__c = null;
            }
            else if (a< 6){
                basicPTP[a].OSM_CPT_Code__c = null;
            }   
            else if (a< 8){
                basicPTP[a].CurrencyIsoCode = null;
            }  
            else if (a< 10){
                basicPTP[a].OSM_Billing_Cycle__c = null;
            } 
            else if (a< 12){
                basicPTP[a].OSM_Pricing_Schema__c = null;
            } 
            else if (a< 14){
                basicPTP[a].OSM_Net_Price__c = null;
            } 
            else if (a< 16){
                basicPTP[a].OSM_Start_Date__c = null;
            } 
             else if (a< 18){
                basicPTP[a].OSM_List_Price__c = null;
            } 
            else if (a< 20){
                basicPTP[a].OSM_Price_Method__c = null;
            } 
        }
        try{
            update basicPTP;
        }
        catch(Exception ex){
            System.assert(ex.getMessage().indexOf('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, ALI ID, List Price, Billing Category, Billing Cycle,Pricing Method, CPT Code, and Currency are required fields for a basic PTP') != -1);
        }
    }

}