public class OSM_Payor_Dashboard_Controller{
    
    public OSM_Payor_Dashboard_Controller(){
        
    }
    
    //for the button Update Pricing
    public PageReference runUpdate(){
        OSM_PayorTestPriceBatch runPTPBatch = new OSM_PayorTestPriceBatch();
        runPTPBatch.isReRun = false;
        Database.executeBatch(runPTPBatch, 100);
        system.debug('test run');
        return null;
    } 
    
     //for the button Update DefaultPTP
    public PageReference runUpdateDefaultPTP(){
        OSM_UpdateDefaultPTPBatch runPTPBatch = new OSM_UpdateDefaultPTPBatch();
        runPTPBatch.isReRun = false;
        Database.executeBatch(runPTPBatch, 100);
        system.debug('test run');
        return null;
    } 
     
    //for the button Re - Update Pricing
    public PageReference rerunUpdate(){
        OSM_PayorTestPriceBatch runPTPBatch = new OSM_PayorTestPriceBatch();
        runPTPBatch.isReRun = true;
        Database.executeBatch(runPTPBatch, 100);
        system.debug('test run');
        return null;
    }
    
    //for the button Update Pricing To Plan
    public PageReference runUpdatePricingToPlan(){
        OSM_UpdatePricingToPlanBatch runPTPToPlanBatch = new OSM_UpdatePricingToPlanBatch();
        runPTPToPlanBatch.isReRun = false;
        Database.executeBatch(runPTPToPlanBatch, 100);
        system.debug('test run update pricing');
        return null;
    }
    
    //for the button Re-Update Pricing To Plan
    public PageReference reRunUpdatePricingToPlan(){
        OSM_UpdatePricingToPlanBatch reRunPTPToPlanBatch = new OSM_UpdatePricingToPlanBatch();
        reRunPTPToPlanBatch.isReRun = true;
        Database.executeBatch(reRunPTPToPlanBatch, 100);
        system.debug('test Rerun update pricing');
        return null;
    }
}