/*
 *  @author         Jerome Liwanag
 *  @description    Test Class for OSM_Payor_Dashboard_Controller 
 *  @date           04 August 2015
 *  @history        04 August 2015 - Created - Jerome liwanag
 */
@isTest
private class OSM_Payor_Dashboard_Controller_Test {

	private static testMethod void testMethod1() {
        PageReference pageRef = Page.OSM_Payor_Dashboard;
        Test.setCurrentPage(pageRef);
        OSM_Payor_Dashboard_Controller cont = new OSM_Payor_Dashboard_Controller();  
        
        Test.startTest();
        cont.runUpdate();
        cont.runUpdateDefaultPTP();
        cont.runUpdatePricingToPlan();
        cont.reRunUpdatePricingToPlan();
        cont.rerunUpdate();
        Test.stopTest();
	}

}