/*
  @author: Daniel Quismorio
  @date: 14 NOV 2014
  @description: Plan Handler
  @history: 14 NOV 2014 - Created (DQ)

*/
public class  OSM_PlanTriggerHandler {
	
	/*
      @author: Daniel Quismorio
      @date: 14 NOV 2014
      @description: On after Insert Method
      @param:  List of new Plans
    */
	public static void onAfterInsert(List<OSM_Plan__c> newPlans){
		numberOfPlansRollUp(newPlans);
	}
	
	
	/*
      @author: Daniel Quismorio
      @date: 14 NOV 2014
      @description: On after Delete Method
      @param:  List of old Plans
    */
	public static void onAfterDelete(List<OSM_Plan__c> oldPlans) {
		numberOfPlansRollUp(oldPlans);
	}
	
	/*
      @author: Daniel Quismorio
      @date: 14 NOV 2014
      @description: This will rollup the record cound of plan linked to the account
      @param:  List of old and new Plans
    */
	public static void numberOfPlansRollUp(List<OSM_Plan__c> oldNewPlans){
		Set<Id> accountIds = new Set<Id>();
		for(OSM_Plan__c planLoop : oldNewPlans){
			accountIds.add(planLoop.OSM_Payor__c);
		}
		
		Map<Id, Integer> mapIdCount = new Map<Id, Integer>();
		for(AggregateResult ar : [Select OSM_Payor__c, Count(Name) FROM OSM_Plan__c WHERE OSM_Payor__c IN: accountIds GROUP BY OSM_Payor__c]){
            mapIdCount.put((Id)ar.get('OSM_Payor__c'),(Integer)ar.get('expr0'));
        }
        
        List<Account> updateAccount = new List<Account>();
        for(Account acctLoop : [Select Id, OSM_Number_of_Plans__c From Account Where Id IN: accountIds]){
        	acctLoop.OSM_Number_of_Plans__c = mapIdCount.get(acctLoop.Id) == null ? 0 : mapIdCount.get(acctLoop.Id);
        	updateAccount.add(acctLoop);
        }
        
        if(updateAccount.size() > 0){
        	update updateAccount;
        }
         
	}

}