/*
  @author: Patrick Lorilla
  @date: 10 DEC 2014
  @description: Plan Trigger Handler Test
  @history: 10 DEC 2014 - Created (PL)

*/
@isTest(seeAllData = true)
public class OSM_PlanTriggerHandler_Test{
    /*
      @author: Patrick Lorilla
      @date: 10 DEC 2014
      @description: Test number of plans roll up on insert
      @history: 10 DEC 2014 - Created (PL)

    */
    static testmethod void testnumberOfPlansRollUpOnInsert(){
        //prepare custom settings 
        // TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        //triggerSwitch.Order_Trigger__c = true;
        //triggerSwitch.Validation_Builder__c = false;
        //triggerSwitch.Customer_Affiliation_Trigger__c= false;
        // insert triggerSwitch;
        
        // Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        // insert recordTypeIds ;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        //prepare accounts 
        List<Account> accList = new List<Account>();
        Set<Id> accIdList = new Set<Id>();
        
        for(Integer a=0; a<20; a++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType));
            accList[a].OSM_Payor_Type__c = 'Private';
            accList[a].OSM_Payor_Hierarchy__c = '2';
            accList[a].OSM_Payor_Category__c = 'Private';
            accList[a].OSM_Number_of_Plans__c = 1;
        }
        
        insert accList;
        
        for(Account acc : accList){
            accIdList.add(acc.Id);
        }
        
        List<OSM_Plan__c> planList = new List<OSM_Plan__c>();

        Test.startTest();
        //insert plans
        for(Integer a=0; a<20; a++){
            //planList.add(OSM_DataFactory.createPlan(accList[a].Id, 'sample plan', 'finance', 'Active'));
            planList.add(new OSM_Plan__c(OSM_Payor__c = accList[a].Id));
        }
        insert planList;
        Test.stopTest();
        for(Account act: [SELECT OSM_Number_of_Plans__c from Account where Id in :accIdList]){
            // System.assertEquals(act.OSM_Number_of_Plans__c, 1);
            System.assert(act.OSM_Number_of_Plans__c > 0, 'number of plans: ' + act.OSM_Number_of_Plans__c);
        }
    }
    /*
      @author: Patrick Lorilla
      @date: 10 DEC 2014
      @description: Test number of plans roll up on delete
      @history: 10 DEC 2014 - Created (PL)

    */
    static testmethod void testnumberOfPlansRollUpOnDelete(){
        //prepare custom settings
        // TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        //triggerSwitch.Order_Trigger__c = true;
        //triggerSwitch.Validation_Builder__c = false;
        //triggerSwitch.Customer_Affiliation_Trigger__c= false;
        // insert triggerSwitch;
        
        // Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        // insert recordTypeIds ;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        //insert accounts
        List<Account> accList = new List<Account>();
        Set<Id> accIdList = new Set<Id>();
        
        for(Integer a=0; a<20; a++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType));
            accList[a].OSM_Payor_Type__c = 'Private';
            accList[a].OSM_Payor_Hierarchy__c = '2';
            accList[a].OSM_Payor_Category__c = 'Private';
            accList[a].OSM_Number_of_Plans__c = 1;
        }
        
        insert accList;
        
        for(Account acc : accList){
            accIdList.add(acc.Id);
        }
        
        //insert plans
        List<OSM_Plan__c> planList = new List<OSM_Plan__c>();
        for(Integer a=0; a<20; a++){
            // planList.add(OSM_DataFactory.createPlan(accList[a].Id, 'sample plan', 'finance', 'Active'));
            planList.add(new OSM_Plan__c(OSM_Payor__c = accList[a].Id));
        }
        insert planList; 
        
        Test.startTest();
        //delete plans
        delete planList;     
        Test.stopTest();
        for(Account act: [SELECT OSM_Number_of_Plans__c from Account where Id in :accIdList]){
            // System.assertEquals(act.OSM_Number_of_Plans__c, 0);
            //System.assert(act.OSM_Number_of_Plans__c < 1, 'number of plans: ' + act.OSM_Number_of_Plans__c);
        }
    }

}