/*
  @author: Daniel Quismorio
  @date: 9 DEC 2014
  @description: Class for Order Update, this call several webservice that will return and display in the javscript Form
  @history: 9 DEC 2014 - Created (DQ)
           20 AUG 2015 - Updated checkCreateCasa, modified validation - Rescian Rey

*/
global class OSM_PopUpController{       
    /*
      @author: Daniel Quismorio
      @date: 9 DEC 2014
      @description: Get the avaiable Quadax Update Reason picklist values
      @param:  none
    */
    webService static List<String> GetQuadaxUpdateReasonPicklist() {
        system.debug('test GetQuadaxUpdateReasonPicklist');
        List<String> values = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = OrderItem.OSM_Quadax_Update_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        values.add('');
        for( Schema.PicklistEntry f : ple)
        {
            values.add(f.getValue());
        }       
        
        return values; 
    }
    
     /*
      @author: Patrick Lorilla
      @date: 15 APRIL 2015
      @description: Get the avaiable Bill type picklist values
      @param:  none
    */
    webService static List<String> GetBillTypePicklist() {
        
        List<String> values = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = OrderItem.OSM_Bill_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        values.add('');
        for( Schema.PicklistEntry f : ple)
        {
            values.add(f.getValue());
        }       
        
        return values; 
    }
    
    /*
      @author: Daniel Quismorio
      @date: 9 DEC 2014
      @description: Update the Order Line Item
      @param:  String OSM_Quadax_Update_Reason__c Value, String OSM_Other_Update_Reason__c Value, Record Id
      @history: 17 FEB 2015 Updated (David E. Catindoy)
    */
    webService static String actionResult(String quadaxUR, String otherUR, String recordId) {
        system.debug('test actionResult');
        system.debug('recordId ' + recordId);
        OrderItem ordrItem = [Select OSM_Other_Update_Reason__c, OSM_Quadax_Update_Reason__c From OrderItem Where Id =: recordId];
        ordrItem.OSM_Quadax_Update_Reason__c = quadaxUR;
        ordrItem.OSM_Other_Update_Reason__c = quadaxUR.contains('Other/Noted')==true ? otherUR : '';
        update ordrItem;
        
        Id generalRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('General').getRecordTypeId();
        List<Case> genCaseList = [Select Id From Case Where OSM_Primary_Order_Line_Item__c =: recordId And RecordTypeId =: generalRecType And Status = 'Open']; 
        List<OrderItem> oliList = [SELECT Id, OrderId FROM OrderItem WHERE Id =: recordId];
        List<Case> caseListInsert = new List<Case>();
        
        system.debug('test oliList ' + oliList);
        system.debug('genCaseListVals ' + genCaseList );
        if(!genCaseList.isEmpty()){
            return 'Case Updated, Order Update Sent test';
        } else {
            for(OrderItem oli : oliList){
                Case newGenCase = new Case();
                newGenCase.Status = 'Open';
                newGenCase.OwnerId = UserInfo.getUserId();
                newGenCase.RecordTypeId = generalRecType;
                newGenCase.OSM_Primary_Order_Line_Item__c = oli.Id;
                newGenCase.OSM_Primary_Order__c = oli.OrderId;
                caseListInsert.add(newGenCase);
            }
            
            if(caseListInsert.size()>0){
                insert caseListInsert;
            }
            system.debug('test caseListInsert ' + caseListInsert);
            return 'Case Created, Order Update Sent';
        }
    }
    
    /******************************************************************************
        Author         : Rescian Rey
        Description    : Checks if the OLI record with the specified OLI ID has
                         a prebilling case with prebilling sent date populated
        History
            <date>                <author>                <description>
            21-AUG-2015           Rescian Rey             Created method
    ********************************************************************************/
    webService static Boolean checkIfValid(String recIdId) {
        Id prebilRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();
        List<Case> preBillCaseList = [Select Id From Case Where OSM_Primary_Order_Line_Item__c =: recIdId AND RecordTypeId =: prebilRecType AND OSM_BI_Sent_Date__c != null]; 
        
        Boolean isValid = True;
        if(preBillCaseList.IsEmpty()){
            isValid = False;
        } 
        
        return isValid;
    }
    
  }