/*
 *Author: Michiel Patricia M. Robrigado and David E. Catindoy
 *Date created: December 2, 2014
 *Test for OSM_PopUpController
 @History: 17 Feb. 2015 Edited (David Catindoy)
 */
@isTest(seeAllData=true)
private class OSM_PopUpController_Test{

    public static testMethod void OSM_PopUpControllerTest() {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Case_Trigger__c = false;
        update triggerSwitch;

        Id recTypeCon = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId(); 
        Id recTypeOrdrId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Work Order').getRecordTypeId();
        
        Account acct = OSM_DataFactory.createAccount('test');
        insert acct;
        Contact con = OSM_DataFactory.createContact('fName', 'lName', recTypeCon);
        insert con;

        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
 
        Order od = OSM_DataFactory.createOrder('test', con.Id, acct.Id, system.today(), 'Active');
        //od.RecordTypeId = recTypeOrdrId;
        insert od;
        Order od1 = OSM_DataFactory.createOrder('test', con.Id, acct.Id, system.today(), 'Active');
        od1.OSM_Order__c = od.Id;
        od1.Pricebook2Id = pb2Standard.Id;
        insert od1;

        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard.Id, 7);
        insert pbe;
        OrderItem ordrItm = OSM_DataFactory.createOrderItem(od1.Id, pbe.Id, 7, 7, System.today().addDays(5));
        insert ordrItm;
        
        Id caseId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('General').getRecordTypeId();
        Id preBillingId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();
        
        Test.startTest();
        OSM_PopUpController.GetQuadaxUpdateReasonPicklist();
        OSM_PopUpController.GetBillTypePicklist();
        //OSM_PopUpController.checkCreateCase();
        //OSM_PopUpController.actionResult('Other/Noted', 'test', ordrItm);
        //OSM_PopUpController.checkCreateCase(ordrItm.Id);
        /*List<Case> newCase = new List<Case>();
        for(Integer i=0; i<2; i++){
            if(i==0){
                newCase.add(new Case(Status = 'Open', RecordTypeId = caseId, OwnerId = UserInfo.getUserId(), OSM_Primary_Order_Line_Item__c = ordrItm.Id, OSM_Primary_Order__c = ordrItm.OrderId));
            } else {
                newCase.add(new Case(Status = 'Open', RecordTypeId = preBillingId, OwnerId = UserInfo.getUserId(), OSM_Primary_Order_Line_Item__c = ordrItm.Id, OSM_Primary_Order__c = ordrItm.OrderId, OSM_Data_Transmission_Status__c = 'Complete'));
            }
        }
        insert newCase;*/
        String actionResult = OSM_PopUpController.actionResult('Other/Noted', 'test', ordrItm.Id);
        //String checkCreateCase = OSM_PopUpController.checkCreateCase(ordrItm.Id);
        Boolean checkIfValid = OSM_PopUpController.checkIfValid(ordrItm.Id);
        Test.stopTest();

        //System.assert(actionResult.equals('Case Updated, Order Update Sent'));
        //System.assert(checkCreateCase.equals(''));
    }
}