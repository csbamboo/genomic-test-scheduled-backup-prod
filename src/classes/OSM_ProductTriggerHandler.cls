/*
  @author: Daniel Quismorio
  @date: 06 JAN 2015
  @description:Product Trigger Handler Class
  @history:06 JAN 2015 - Created (DQ)

*/
public class OSM_ProductTriggerHandler {
	/**
     * @author         Daniel Quismorio
     * @description    Method for all actions that will take place after a product has been inserted. 
     * @history        06 JAN 2015 - Daniel Quismorio - Created  
     */
	public static void afterInsert(List<Product2> newProduct){
		retouchOLIRecords(newProduct);
    }
    
    /**
     * @author         Daniel Quismorio
     * @description    Method for all actions that will take place after a product has been updated. 
     * @history        06 JAN 2015 - Daniel Quismorio - Created  
     */
    public static void afterUpdate(List<Product2> newProduct){
    	retouchOLIRecords(newProduct);
    }
    
    /**
     * @author         Daniel Quismorio
     * @description    Retouch OLI to rerun workflow
     * @parameter      List of Product2 records 
     * @history        06.JAN.2015 - Daniel Quismorio - Created  
     */
    public static void retouchOLIRecords(List<Product2> newProduct){
    	Set<Id> productIds = new Set<Id>();
    	for(Product2 prodLoop : newProduct){
    		productIds.add(prodLoop.Id);
    	}
    	
    	Set<Id> pbeIds = new Set<Id>();
    	for(PricebookEntry pbeLoop : [Select Id From PricebookEntry Where Product2Id IN: productIds]){
    		pbeIds.add(pbeLoop.Id);
    	}
    	
    	List<OrderItem> updateOrderitemList = new List<OrderItem>();
    	for(OrderItem ordrItemLoop : [Select Id From OrderItem Where PricebookEntryId IN: pbeIds]){
    		updateOrderitemList.add(ordrItemLoop);
    	}
    	
    	if(updateOrderitemList.size() > 0){
    		//update updateOrderitemList;
    		Database.update(updateOrderitemList, false);
    	}
    }
}