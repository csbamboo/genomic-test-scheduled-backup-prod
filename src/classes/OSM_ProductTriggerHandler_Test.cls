/*
  @author: Michiel Patricia M. Robrigado
  @date: 23 FEB 2015
  @description:Test for Product Trigger Handler Class
  @history:23 FEB 2015 - Created (Michiel Patricia M. Robrigado)

*/
@isTest
public class OSM_ProductTriggerHandler_Test
{
    static testmethod void createProduct()
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Trigger__c = false;
        triggerSwitch.Product2__c = true;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        order1.PriceBook2Id = pb2Standard;
        insert order1;
                
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(order1.id);
        insert workOrder;
        
        List<Product2> prodList = new List<Product2>{OSM_DataFactory.createProduct('IBC', true)}; 
        insert prodList;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prodList[0].Id, pb2Standard, 3);
        insert pbe;
        
        OrderItem ordItem = new OrderItem(OSM_Product_Name__c = 'DCIS', OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, OSM_Lab_and_Report_Status__c = 'Pre-Processing');
        insert ordItem;
        
        //List<Product2> lstProduct = new List<Product2>([Select id, Name from Product2 where id = : prod2.Id]);
        //for(Product2 prod : lstProduct)
        
        
        Test.startTest();
        //OSM_ProductTriggerHandler.hasRetouchOLIRecordsRun = false;
        for(Product2 prod : prodList)
        {    
            prod.Name = 'DCIS';
        }
        update prodList;
        
        Test.stopTest();
    }
}