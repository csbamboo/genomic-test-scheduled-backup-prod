/*
  @author: Rao
  @date: Aug 08 2015
  @description: Generate Random Number Utility
  @history: Aug 08 2015- Rao -Created 
            
*/
public class OSM_RandomUtility {
/**
	 * Gets a random integer number between lower (inclusive) and upper (exclusive)
	 */
	public static Integer getRandomInt (Integer lower, Integer upper)
	{
		return Math.round(Math.random() * (upper - lower)) + lower;
	}
}