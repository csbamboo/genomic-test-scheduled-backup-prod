global class OSM_RealignAccountTerritory implements Database.Batchable<sObject>{
    global final String query;
    ID tID = null;
    global OSM_RealignAccountTerritory (ID terrID){
        query = 'SELECT ID, OSM_Account_Name__c, OSM_Account_Name__r.OSM_Oncology__c, OSM_Account_Name__r.OSM_Urology__c, OSM_Account_Name__r.OSM_Standing_Order_Account__c, OSM_Account_Name__r.OSM_Managed_Care__c, OSM_Account_Name__r.OSM_Strategic_Account__c,  OSM_Account_Name__r.OSM_Submits_Specimen__c, OSM_Account_Name__r.BillingPostalCode, OSM_Account_Name__r.BillingCountry, OSM_Territory_Name__c FROM OSM_Account_Territory_Assignment__c WHERE OSM_Territory_Name__c = \'' + terrID + '\'';
        tID = terrID;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
   
    global void execute(Database.BatchableContext BC, List<OSM_Account_Territory_Assignment__c> scope){
        Set<Id> accountIds = new Set<Id>();
        List<OSM_Account_Territory_Assignment__c> oldList1 = new List<OSM_Account_Territory_Assignment__c>();

        for(OSM_Account_Territory_Assignment__c loopAccountTerritoryAssignment : scope){
            oldList1.add(loopAccountTerritoryAssignment);
            accountIds.add(loopAccountTerritoryAssignment.OSM_Account_Name__c);
        }
        
        Map<ID, Account> accountMap = new Map<ID,Account>([SELECT Id, OSM_Oncology__c, OSM_Urology__c, OSM_Standing_Order_Account__c, OSM_Managed_Care__c, OSM_Strategic_Account__c, 
                                                                  OSM_Submits_Orders__c, OSM_Submits_Specimen__c, BillingPostalCode, BillingCountry, OSM_Country_Code__c 
                                                           FROM Account 
                                                           WHERE Id IN :accountIds]);
        
        List<AccountShare> asList = [SELECT ID from AccountShare WHERE AccountId IN: accountMap.keySet() and rowcause ='Manual'];
        List<OSM_Account_Territory_Assignment__c> newList1 = new List<OSM_Account_Territory_Assignment__c>();
        List<OSM_Account_Territory_Assignment__c> oldListFilt= new List<OSM_Account_Territory_Assignment__c>();
        if(!asList.isEmpty()){
            delete asList;
        }
       
        Set<Id> usAccounts = new Set<Id>();
        Set<Id> internationalAccounts = new Set<Id>();
        Set<String> postalCodes = new Set<String>();
        Set<String> countryPostalCodes = new Set<String>();
        Set<String> countryNames = new Set<String>();        
        //Collect Accounts
        for(Id loopAccountId : accountMap.keySet()){
            postalCodes.add(accountMap.get(loopAccountId).BillingPostalCode);
            if(accountMap.get(loopAccountId).BillingCountry.toLowerCase() == 'united states' || 
                accountMap.get(loopAccountId).BillingCountry.toLowerCase() == 'usa' || 
                accountMap.get(loopAccountId).BillingCountry.toLowerCase() == 'us'){
                usAccounts.add(loopAccountId);
                postalCodes.add(accountMap.get(loopAccountId).BillingPostalCode);
            }else{
                internationalAccounts.add(loopAccountId);
                countryPostalCodes.add(accountMap.get(loopAccountId).BillingPostalCode);
                countryNames.add(accountMap.get(loopAccountId).BillingCountry);
            }
        }
        
        //Run US Logic
        //--UNCOMMENT THIS LINE--newList1.addAll(OSM_AccountTriggerHandler.alignCountriesToTerritory(accountMap, usAccounts, postalCodes));
        //Run International Logic
        newList1.addAll(OSM_AccountTriggerHandler.alignCountriesToTerritory(accountMap, internationalAccounts, countryPostalCodes, countryNames));
      
        /*If there are duplicate records i.e. (AccountID, Territory ID) combination 
        in the new list which already exists in the OLD LIST 1,
        we will remove this from the new list.
        */
        List<OSM_Account_Territory_Assignment__c> deleteAccountAssignments = new List<OSM_Account_Territory_Assignment__c>();
        List<OSM_Account_Territory_Assignment__c> cloneAccountAssignments = new List<OSM_Account_Territory_Assignment__c>();
        Map<String,OSM_Account_Territory_Assignment__c> oldMap = new Map<String,OSM_Account_Territory_Assignment__c>();
        Map<String,OSM_Account_Territory_Assignment__c> newMap = new Map<String,OSM_Account_Territory_Assignment__c>();
        
        for(OSM_Account_Territory_Assignment__c loopAccountAssignment : oldList1){
            oldMap.put(loopAccountAssignment.OSM_Territory_Name__c + '::' + loopAccountAssignment.OSM_Account_Name__c,loopAccountAssignment);
        }
        
        for(OSM_Account_Territory_Assignment__c loopAccountAssignment : newList1){
            newMap.put(loopAccountAssignment.OSM_Territory_Name__c + '::' + loopAccountAssignment.OSM_Account_Name__c,loopAccountAssignment);
        }
        
        List<OSM_Account_Territory_Assignment__c> deleteAssignments = new List<OSM_Account_Territory_Assignment__c>();
        for(String loopOld : oldMap.keySet()){
            if(!newMap.containsKey(loopOld)){
                deleteAssignments.add(oldMap.get(loopOld));
            }else{
                cloneAccountAssignments.add(oldMap.get(loopOld));
            }
        }

        if(!deleteAssignments.isEmpty()){
            delete deleteAssignments;
        }
     
        Set<String> removeKeys = new Set<String>();
        
        for(Integer a=0; a<newList1.size(); a++){
            for(OSM_Account_Territory_Assignment__c ol: oldList1){
                if(newList1[a].OSM_Account_Name__c == ol.OSM_Account_Name__c && 
                   newList1[a].OSM_Territory_Name__c == ol.OSM_Territory_Name__c){
                    oldListFilt.add(ol);
                    removeKeys.add(newList1[a].OSM_Territory_Name__c + '::' + newList1[a].OSM_Account_Name__c);
                }
            }            
        }
         
        List<OSM_Account_Territory_Assignment__c> nonRemovalsMembers = new List<OSM_Account_Territory_Assignment__c>();
        for(OSM_Account_Territory_Assignment__c nl: newList1){ 
            if(!removeKeys.contains(nl.OSM_Territory_Name__c + '::' + nl.OSM_Account_Name__c)){
                 nonRemovalsMembers.add(nl);
            }   
        }
        
         /*
         for(OSM_Account_Territory_Assignment__c rem: removalsMap.values()){
             for(OSM_Account_Territory_Assignment__c nl: newList1Map.values()){ 
                 if(newList1Map.get(rem.ID) != null){
                     newList1Map.remove(rem.ID);
                 }   
             }  
         }*/
           
        if(!nonRemovalsMembers.isEmpty()){   
            insert nonRemovalsMembers; 
        }
       
        List<OSM_Account_Territory_Assignment__c> clonedAT = cloneAccountAssignments.deepClone(false,true,true);  
        if(!clonedAT.isEmpty()){
            delete cloneAccountAssignments;
            insert clonedAT;
        }
        /*
        List<OSM_Account_Territory_Assignment__c> clonedAT = new List<OSM_Account_Territory_Assignment__c>();
        for(OSM_Account_Territory_Assignment__c caa: cloneAccountAssignments){
            clonedAT.add(caa);
        }
        
        if(!clonedAT.isEmpty()){
            delete cloneAccountAssignments;
            insert clonedAT;
        }*/
        
        
   }
  
   global void finish(Database.BatchableContext BC){
       OSM_RealignContactTerritory batch = new OSM_RealignContactTerritory(tID);
       Database.executeBatch(batch);
   }

}