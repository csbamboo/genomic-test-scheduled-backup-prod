@isTest
public class OSM_RealignAccountTerritory_Test{

    /*
        @author: patrick lorilla
        @date: 17 November 2014
        @description: test execution of realign account batch class
    */
    static testmethod void testExecute() {
        //Prepare user, record types and territories
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        User testUsr = OSM_DataFactory.createUser('Systad');
        insert testUsr;
        OSM_Territory__c terr = OSM_DataFactory.createTerritory('My Terr');
        insert terr;
        //prepare accounts
        List<Account> acctList = new List<Account>();
        for(Integer a=0; a<20; a++){
            if(a<10){
                acctList.add(OSM_DataFactory.createAccountWithBillingAddress(a,'United States', 'Street 1', 'Los Angeles', 'California', '3500', recTypes.Account_HCO_Record_Type__c));
                acctList[a].OSM_Oncology__c = true;
                acctList[a].OSM_Country_Code__c = 'US';
            }
            else{
                acctList.add(OSM_DataFactory.createAccountWithBillingAddress(a,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c));
                acctList[a].OSM_Oncology__c = true;
                acctList[a].OSM_Country_Code__c = 'CA';
            }
        }
        insert acctList;
        //prepare contacts
        List<Contact> conList = new List<Contact>();
        for(Integer a=0; a<20; a++){
            conList.add(OSM_DataFactory.createContact(a,acctList[a].ID,recTypes.Contact_HCP_Record_Type__c));
        }
        insert conList;
        
        //Prepare account share and alignment rule
        AccountShare aShare = new AccountShare (UserorGroupId = testUsr.ID, AccountAccessLevel = 'Edit', AccountId =acctList[0].ID, OpportunityAccessLevel = 'Edit' );
        insert aShare;
        
        OSM_Alignment_Rule__c alRule1 = OSM_DataFactory.createAlignmentRule(0, 'United States', '3500', 'Oncology',terr.ID);
        alRule1.Rule_Type__c = 'Country';
        alRule1.OSM_Country_Code__c  = 'US';
        OSM_Alignment_Rule__c alRule2 = OSM_DataFactory.createAlignmentRule(0, 'Canada', '123', 'Urology',terr.ID);
        alRule2.Rule_Type__c = 'Country';
        alRule2.OSM_Country_Code__c  = 'CA';
        
        insert alRule1 ;
        insert alRule2 ;
        
        List<OSM_Account_Territory_Assignment__c> ataList = new List<OSM_Account_Territory_Assignment__c>();
        for(Integer a=0; a<20; a++){
             ataList.add(OSM_DataFactory.createATA(acctList[a].ID, terr.ID));
        }
        insert ataList;
        List<OSM_Account_Territory_Assignment__c> clonedATAList= ataList.deepClone(false,true,true);  
        test.startTest();
        System.runAs(testUsr){
            //execute
            OSM_RealignAccountTerritory batch = new OSM_RealignAccountTerritory(terr.ID);
            Database.executeBatch(batch);
        }
        test.stopTest();
        
        //assert created dates
        Map<ID, OSM_Account_Territory_Assignment__c> newATA = new Map<ID, OSM_Account_Territory_Assignment__c>([SELECT CreatedDate from OSM_Account_Territory_Assignment__c]);
        for(OSM_Account_Territory_Assignment__c ata: clonedATAList){
            System.assert(!newATA.containsKey(ata.ID));
        }
     }

}