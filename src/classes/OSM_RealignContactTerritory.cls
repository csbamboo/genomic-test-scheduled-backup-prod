global class OSM_RealignContactTerritory implements Database.Batchable<sObject>{
    global final String query;
    ID tID = null;
    global OSM_RealignContactTerritory (ID terrID){
        query='SELECT Id, OSM_Oncology__c, OSM_Urology__c, MailingCountry, MailingPostalCode FROM Contact WHERE Id IN (SELECT OSM_HCP_Name__c FROM OSM_HCP_Territory_Assignment__c WHERE OSM_Territory_Name__c =\''+terrID+'\')';
        tID = terrID;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
   
    global void execute(Database.BatchableContext BC, List<OSM_HCP_Territory_Assignment__c> scope){
      Set<Id> contactIds = new Set<Id>();
        List<OSM_HCP_Territory_Assignment__c> oldList1 = new List<OSM_HCP_Territory_Assignment__c>();
        
        for(OSM_HCP_Territory_Assignment__c loopContactTerritoryAssignment : scope){
            oldList1.add(loopContactTerritoryAssignment);
            contactIds.add(loopContactTerritoryAssignment.OSM_HCP_Name__c);
        }
        
        Map<ID, Contact> contactMap = new Map<ID,Contact>([SELECT Id, OSM_Oncology__c, OSM_Urology__c, MailingCountry, MailingPostalCode, OSM_Country_Code__c 
                                                           FROM Contact 
                                                           WHERE Id IN :contactIds]);
        
        List<ContactShare> asList = [SELECT ID from ContactShare WHERE ContactId IN: contactMap.keySet() and rowcause ='Manual'];
        List<OSM_HCP_Territory_Assignment__c> newList1 = new List<OSM_HCP_Territory_Assignment__c>();
        List<OSM_HCP_Territory_Assignment__c> oldListFilt= new List<OSM_HCP_Territory_Assignment__c>();
        if(!asList.isEmpty()){
            delete asList;
        }
       
        Set<Id> usContacts = new Set<Id>();
        Set<Id> internationalContacts = new Set<Id>();
        Set<String> postalCodes = new Set<String>();
        Set<String> countryPostalCodes = new Set<String>();
        Set<String> countryNames = new Set<String>();        
     
        
        //Collect Contacts
        for(Id loopContactId : contactMap.keySet()){
            postalCodes.add(contactMap.get(loopContactId).MailingPostalCode);
            if(contactMap.get(loopContactId).MailingCountry.toLowerCase() == 'united states' || 
                contactMap.get(loopContactId).MailingCountry.toLowerCase() == 'usa' || 
                contactMap.get(loopContactId).MailingCountry.toLowerCase() == 'us'){
                usContacts.add(loopContactId);
                postalCodes.add(contactMap.get(loopContactId).MailingPostalCode);
            }else{
                internationalContacts.add(loopContactId);
                countryPostalCodes.add(contactMap.get(loopContactId).MailingPostalCode);
                countryNames.add(contactMap.get(loopContactId).MailingCountry);
            }
        }
        
        //Run US Logic
        //Veena: Commented below line since it is not called
       // newList1.addAll(OSM_ContactTriggerHandler.alignCountriesToTerritory(contactMap, usContacts, postalCodes));
        //Run International Logic
        newList1.addAll(OSM_ContactTriggerHandler.alignCountriesToTerritory(contactMap, internationalContacts, countryPostalCodes, countryNames));
      
        /*If there are duplicate records i.e. (ContactID, Territory ID) combination 
        in the new list which already exists in the OLD LIST 1,
        we will remove this from the new list.
        */
        List<OSM_HCP_Territory_Assignment__c> deleteContactAssignments = new List<OSM_HCP_Territory_Assignment__c>();
        List<OSM_HCP_Territory_Assignment__c> cloneContactAssignments = new List<OSM_HCP_Territory_Assignment__c>();
        Map<String,OSM_HCP_Territory_Assignment__c> oldMap = new Map<String,OSM_HCP_Territory_Assignment__c>();
        Map<String,OSM_HCP_Territory_Assignment__c> newMap = new Map<String,OSM_HCP_Territory_Assignment__c>();
        
        for(OSM_HCP_Territory_Assignment__c loopContactAssignment : oldList1){
            oldMap.put(loopContactAssignment.OSM_Territory_Name__c + '::' + loopContactAssignment.OSM_HCP_Name__c,loopContactAssignment);
        }
        
        for(OSM_HCP_Territory_Assignment__c loopContactAssignment : newList1){
            newMap.put(loopContactAssignment.OSM_Territory_Name__c + '::' + loopContactAssignment.OSM_HCP_Name__c,loopContactAssignment);
        }
        
        List<OSM_HCP_Territory_Assignment__c> deleteAssignments = new List<OSM_HCP_Territory_Assignment__c >();
        for(String loopOld : oldMap.keySet()){
            if(!newMap.containsKey(loopOld)){
                deleteAssignments.add(oldMap.get(loopOld));
            }else{
                cloneContactAssignments.add(oldMap.get(loopOld));
            }
        }


        if(!deleteAssignments.isEmpty()){
            delete deleteAssignments;
        }
     
        Set<String> removeKeys = new Set<String>();
        for(Integer a=0; a<newList1.size(); a++){
            for(OSM_HCP_Territory_Assignment__c ol: oldList1){
                if(newList1[a].OSM_HCP_Name__c == ol.OSM_HCP_Name__c && 
                   newList1[a].OSM_Territory_Name__c == ol.OSM_Territory_Name__c){
                    oldListFilt.add(ol);
                    removeKeys.add(newList1[a].OSM_Territory_Name__c + '::' + newList1[a].OSM_HCP_Name__c);
                }
            }            
        }
        /*
        List<OSM_HCP_Territory_Assignment__c> clonedAT = cloneContactAssignments.deepClone(false,true,true);     
        System.debug('\n\n\n clonedAT:'+ clonedAT);      
        System.debug('\n\n\n delete Cloned');    
        if(!clonedAT.isEmpty()){
            delete cloneContactAssignments;
            insert clonedAT;
        }*/
        
        List<OSM_HCP_Territory_Assignment__c> nonRemovalsMembers = new List<OSM_HCP_Territory_Assignment__c>();
        for(OSM_HCP_Territory_Assignment__c nl: newList1){ 
            if(!removeKeys.contains(nl.OSM_Territory_Name__c + '::' + nl.OSM_HCP_Name__c)){
                 nonRemovalsMembers.add(nl);
            }   
        }
        
        if(!nonRemovalsMembers.isEmpty()){   
            insert nonRemovalsMembers; 
        }
       
        List<OSM_HCP_Territory_Assignment__c> clonedAT = cloneContactAssignments.deepClone(false,true,true);  
        if(!clonedAT.isEmpty()){
            delete cloneContactAssignments;
            insert clonedAT;
        }
    }
  
    global void finish(Database.BatchableContext BC){
    
    }

}