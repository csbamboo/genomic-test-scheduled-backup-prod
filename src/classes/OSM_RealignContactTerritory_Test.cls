/*
    @author: patrick lorilla
    @date: 28 Oct 2014
    @description: Realign Contact Territory Trigger Handler Test
    @history: 28 Oct 2014 - Patrick Lorilla - Created

*/
@isTest
public class OSM_RealignContactTerritory_Test{
        /*
        @author: patrick lorilla
        @date: 28 Oct 2014
        @description: Test Execute Batch Class
        @history: 28 Oct 2014 - Patrick Lorilla - Created
        @history: 27 Jan 2015 - Jats Xyvenn Matres - Updated
       */
    static testmethod void testExecute() {
        /*TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Contact_Trigger__c = false;        
        insert triggerSwitch;*/

        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        insert recTypes;
        //Prepare test user, territories
        //User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        User testUsr = OSM_DataFactory.createUser('TestUser');
        insert testUsr;
        OSM_Territory__c terr = OSM_DataFactory.createTerritory('My Terr');
        terr.OSM_Territory_Market__c = 'Oncology;Urology';
        insert terr;
        OSM_Territory__c terr2 = OSM_DataFactory.createTerritory('My Terr2');
        terr2.OSM_Territory_Market__c = 'Oncology;Urology';
        insert terr2;
        //prepare contact with mailing addresses
        List<Contact> conList = new List<Contact>();
        for(Integer a=0; a<20; a++){
            if(a<10){
                conList.add(OSM_DataFactory.createContactWithMailingAddress(a, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', recTypes.Contact_HCP_Record_Type__c));
                conList[a].OSM_Oncology__c = true;
                conList[a].OSM_Urology__c= true;
                conList[a].OSM_Country_Code__c = 'US';

            }
            else{
                conList.add(OSM_DataFactory.createContactWithMailingAddress(a, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Contact_HCP_Record_Type__c));
                conList[a].OSM_Oncology__c = true;
                conList[a].OSM_Urology__c= true;
                conList[a].OSM_Country_Code__c = 'CA';

            }
        }
        insert conList;
        
        
        //Prepare contact share and alignment rule
        ContactShare cShare = new ContactShare (UserorGroupId = testUsr.ID, ContactAccessLevel = 'Edit', ContactId =conList[0].ID );
        insert cShare;
        
        OSM_Alignment_Rule__c alRule1 = OSM_DataFactory.createAlignmentRule(0, 'United States', '3500', 'Oncology',terr.ID);
        alRule1.Rule_Type__c = 'Country';
        alRule1.OSM_Country_Code__c  = 'US';
        OSM_Alignment_Rule__c alRule2 = OSM_DataFactory.createAlignmentRule(0, 'Canada', '123', 'Urology',terr.ID);
        alRule2.Rule_Type__c = 'Country';
        alRule2.OSM_Country_Code__c  = 'CA';
        insert alRule1 ;
        insert alRule2 ;
       
        List<OSM_HCP_Territory_Assignment__c> hcpList = new List<OSM_HCP_Territory_Assignment__c>();
        for(Integer a=0; a<20; a++){   
            hcpList.add(OSM_DataFactory.createHTA(conList[a].ID, terr.ID));  
        }
        insert hcpList;
         List<OSM_HCP_Territory_Assignment__c> clonedHTAList = hcpList .deepClone(false,true,true);  
        test.startTest();
        System.runAs(testUsr){
            //run batch
            OSM_RealignContactTerritory batch = new OSM_RealignContactTerritory(terr.ID);
            Database.executeBatch(batch);
        }
        test.stopTest();
        
        //assert created dates
        Map<ID, OSM_HCP_Territory_Assignment__c> newHTA = new Map<ID, OSM_HCP_Territory_Assignment__c>([SELECT CreatedDate from OSM_HCP_Territory_Assignment__c]);
        for(OSM_HCP_Territory_Assignment__c hta: clonedHTAList){
            System.assert(!newHTA.containsKey(hta.ID));
        }
     }
}