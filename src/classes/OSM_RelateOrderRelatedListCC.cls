/**
 * File Info
 * ----------------------------------
 * @filename       OSM_RelateOrderRelatedListCC.cls
 * @author         Kristian Vegerano
 * @description    Controller class for OSM_RelateOrderRelatedList Visualforce page. 
 * @history        03.DEC.2014 - Kristian Vegerano - Created  
 */
public class OSM_RelateOrderRelatedListCC{
    private Order pageOrder;
    private Map<Id, Set<Id>> parentGroupMap;
    private Map<Id, Set<Id>> childGroupMap;
    private Map<Id, Order> relatedOrdersMap;
    public List<Order> relatedOrders {get;set;}
    public Id selectedRow {get;set;}
    
    //Pagination
    public Integer pageSize {get;set;}
    public Integer currentPage {get;set;}
    public Integer totalPage {get;set;}
    public Integer currentRecord {get;set;}
    public List<sObject> pagedIIList {get;set;}
    public Boolean hasPageError {get;set;}
    public Boolean hasNext {get;set;}
    public Boolean hasPrevious {get;set;}
    public Boolean addOnly {get;set;}
    public Integer addRowCount {get;set;}
    public String accountName {get;set;}
    public String contactName {get;set;}
    public String opportunityName {get;set;}
    public List<sObject> managedIIList {get;set;}

    
    /**
     * @author         Kristian Vegerano
     * @description    Constructor. 
     * @history        03.DEC.2014 - Kristian Vegerano - Created  
     */
    public OSM_RelateOrderRelatedListCC(ApexPages.StandardController stdController){
        pageOrder = (Order)stdController.getRecord();
        Id patientId = [SELECT Id, OSM_Patient__c FROM Order WHERE Id = :pageOrder.Id].OSM_Patient__c;
        childGroupMap = getChildRecords('OSM_Related_Order__c', 'Order', 'OSM_Related_Order__c != null and OSM_Patient__c = \'' + patientId + '\'');
        parentGroupMap = getParentRecords('OSM_Related_Order__c', 'Order', 'OSM_Related_Order__c != null and OSM_Patient__c = \'' + patientId + '\'');
                
        relatedOrders = new List<Order>();
        relatedOrdersMap = new Map<ID,Order>();
        relatedOrdersMap = new Map<ID,Order>([SELECT Id, Name, RecordTypeId, RecordType.Name, OSM_Patient__c, OSM_Patient__r.Name, AccountId, Account.Name, Status, OSM_Patient_Number_of_Orders_F__c, LastModifiedById, LastModifiedBy.Name, OrderNumber
                                             FROM Order WHERE (Id IN: childGroupMap.get(pageOrder.Id) OR Id IN: parentGroupMap.get(pageOrder.Id))]);
                         
        for(Order ord: [SELECT Id, Name, RecordTypeId, RecordType.Name, OSM_Patient__c, OSM_Patient__r.Name, AccountId, Account.Name, Status, OSM_Patient_Number_of_Orders_F__c, LastModifiedById, LastModifiedBy.Name, OrderNumber From Order Where OSM_Related_Order__c =: pageOrder.Id]){
            if(!relatedOrdersMap.containsKey(ord.ID)){
                relatedOrdersMap.put(ord.ID,ord);
            }
        }
        
        relatedOrders = relatedOrdersMap.values();
        
        //Pagination Initialization
        pageSize = 5;
        currentPage = 1;
        currentRecord = 1;
        hasNext = true;
        hasPrevious = false;
        pagedIIList = new List<sObject>();
        managedIIList = new List<sObject>();
        for(Order loopOrder : relatedOrders){
            managedIIList.add(loopOrder);
        }
        for(Integer counter = 0; counter < (managedIIList.size() < pageSize ? managedIIList.size() : pageSize); counter++){
            pagedIIList.add(managedIIList[counter]);
        }
        totalPage = (Integer)Math.ceil((Double)managedIIList.size()/pageSize);
        if(managedIIList.size() <= pageSize){
            hasNext = false;
        }
    }
     /**
     * @author         Patrick Lorilla
     * @description    Update order count value. 
     * @history        12.DEC.2014 - Patrick Lorilla - Created  
     */
    public void updateCounter(){
        if(pageOrder.Id != null && managedIIList!= null){
            Order ord = [SELECT OSM_Related_Order_Count__c from Order where ID =: pageOrder.Id];
            
            if(ord.ID != null){
                if(ord.OSM_Related_Order_Count__c  != managedIIList.size()){
                    ord.OSM_Related_Order_Count__c  = managedIIList.size();
                    update ord;
                }
            }
        }
    }
    /*
     * @author         Kristian Vegerano
     * @description    Method that deletes the order selected by the user. 
     * @history        03.DEC.2014 - Kristian Vegerano - Created  
     
    public void deleteOrder(){
        delete [SELECT Id FROM Order WHERE Id =:selectedRow];
        relatedOrders = new List<Order>();
        relatedOrders = [SELECT Id, Name, RecordTypeId, RecordType.Name, OSM_Patient__c, OSM_Patient__r.Name, AccountId, Account.Name, Status, OSM_Patient_Number_of_Orders_F__c, LastModifiedById, LastModifiedBy.Name, OrderNumber
                         FROM Order WHERE Id IN: parentGroupMap.get(pageOrder.Id)];
        for(Order loopOrder : relatedOrders){
            managedIIList.add(loopOrder);
        }
        for(Integer counter = 0; counter < (managedIIList.size() < pageSize ? managedIIList.size() : pageSize); counter++){
            pagedIIList.add(managedIIList[counter]);
        }
        totalPage = (Integer)Math.ceil((Double)managedIIList.size()/pageSize);
        if(managedIIList.size() <= pageSize){
            hasNext = false;
        }
    }*/
    
    /**
     * @author         Kristian Vegerano
     * @description    Method retrieves all child records of the order.
     * @history        03.DEC.2014 - Kristian Vegerano - Created  
     */
    public Map<Id, Set<Id>> getChildRecords(String parentField, String sObjectAPIName, String whereClause){
        Map<Id, Set<Id>> parentGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();
        
        String queryString = 'SELECT Id,' + parentField + ' FROM ' + sObjectAPIName + ' WHERE ' + whereClause + ' LIMIT 20000';
        for(sObject loopObject : Database.query(queryString)){
            childParentMap.put((Id)loopObject.get('Id'), (Id)loopObject.get(parentField));
            parentChildMap.put((Id)loopObject.get(parentField), (Id)loopObject.get('Id'));
            if(!parentGroupMap.containsKey((Id)loopObject.get(parentField))){
                parentGroupMap.put((Id)loopObject.get(parentField), new Set<Id>{(Id)loopObject.get('Id')});
            }else{
                parentGroupMap.get((Id)loopObject.get(parentField)).add((Id)loopObject.get('Id'));
            }
        }
        if (!parentGroupMap.isEmpty()){
            for(Integer counter = 0; counter < 25; counter++){
                for(Id loopParentId : parentGroupMap.keySet()){
                    for(Id loopChildId : parentGroupMap.get(loopParentId)){
                        if(parentChildMap.get(loopChildId) != null){
                            parentGroupMap.get(loopParentId).add(parentChildMap.get(loopChildId));
                        }
                    }
                }
            }
        }
        
        return parentGroupMap;
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method retrieves all parent records of the order.
     * @history        03.DEC.2014 - Kristian Vegerano - Created  
     */
    public Map<Id, Set<Id>> getParentRecords(String parentField, String sObjectAPIName, String whereClause){
        Map<Id, Set<Id>> childGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();
        
        String queryString = 'SELECT Id,' + parentField + ' FROM ' + sObjectAPIName + ' WHERE ' + whereClause + ' LIMIT 20000';
        for(sObject loopObject : Database.query(queryString)){
            childParentMap.put((Id)loopObject.get('Id'),(Id)loopObject.get(parentField));
            parentChildMap.put((Id)loopObject.get(parentField),(Id)loopObject.get('Id'));
            if(!childGroupMap.containsKey((Id)loopObject.get('Id'))){
                childGroupMap.put((Id)loopObject.get('Id'), new Set<Id>{(Id)loopObject.get(parentField)});
            }else{
                childGroupMap.get((Id)loopObject.get('Id')).add((Id)loopObject.get(parentField));
            }
        }
        if (!childGroupMap.isEmpty()){
            for(Integer counter = 0; counter < 50; counter++){
                for(Id loopChildId : childGroupMap.keySet()){
                    for(Id loopParentId : childGroupMap.get(loopChildId)){
                        if(childParentMap.get(loopParentId) != null){
                            childGroupMap.get(loopChildId).add(childParentMap.get(loopParentId));
                        }
                    }
                }
            }
        }
        
        return childGroupMap;
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the next 10 records.
     * @history        09 June,2014 - Kristian Vegerano - Created  
     */
    public PageReference nextPage(){
         try{
            currentPage += 1;
            currentRecord += pageSize;
            pagedIIList.clear();
            for(Integer counter = currentRecord; counter < (managedIIList.size()+1 < (currentRecord + pageSize) ? managedIIList.size()+1 : (currentRecord + pageSize)); counter++){
                pagedIIList.add(managedIIList[counter-1]); 
            }
            hasPrevious = true;
            if(currentPage == (Integer)Math.ceil((Double)managedIIList.size()/pageSize)){//currentRecord is not the value
                hasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**********************************************************************************************
    @author       Kristian Vegerano
    @date         09 June,2014
    @description: Method that shows the previous 10 records.
    **********************************************************************************************/
    public PageReference previousPage(){
        try{
            currentPage -= 1;
            currentRecord -= pageSize;
            pagedIIList.clear();
            for(Integer counter = currentRecord; counter < (managedIIList.size() < (currentRecord + pageSize) ? managedIIList.size() : (currentRecord + pageSize)); counter++){
                pagedIIList.add(managedIIList[counter-1]);
            }
            hasNext = true;
            if(currentPage == 1){
                hasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**********************************************************************************************
    @author       Kristian Vegerano
    @date         09 June,2014
    @description: Method that shows the last page.
    **********************************************************************************************/
    public PageReference lastPage(){
        try{
            currentPage = (Integer)Math.ceil((Double)managedIIList.size()/pageSize) - 1;
            currentRecord = currentPage * pageSize + 1;
            pagedIIList.clear();
            for(Integer counter = currentRecord; counter < (managedIIList.size()+1 < (currentRecord + pageSize) ? managedIIList.size()+1 : (currentRecord + pageSize)); counter++){
                pagedIIList.add(managedIIList[counter-1]);
            }
            hasPrevious = true;
            currentPage = (Integer)Math.ceil((Double)managedIIList.size()/pageSize);
            if(currentPage == (Integer)Math.ceil((Double)managedIIList.size()/pageSize)){
                hasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**********************************************************************************************
    @author       Kristian Vegerano
    @date         09 June,2014
    @description: Method that shows the first page.
    **********************************************************************************************/
    public PageReference firstPage(){
        try{
            currentPage = 1;
            currentRecord = 1;
            pagedIIList.clear();
            for(Integer counter = 0; counter < pageSize; counter++){
                pagedIIList.add(managedIIList[counter]);
            }
            hasNext = true;
            if(currentPage == 1){
                hasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
}