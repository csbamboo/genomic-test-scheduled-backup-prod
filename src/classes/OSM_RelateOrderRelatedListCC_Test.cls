/**
 * File Info
 * ----------------------------------
 * @filename       OSM_RelateOrderRelatedListCC.cls
 * @created        14.JAN.2015
 * @author         Jats Xyvenn Matres
 * @description    Controller class for OSM_RelateOrderRelatedList Visualforce page - test class. 
 * @history        14.JAN.2015 - Jats Xyvenn Matres - Created  
 */

@isTest
private class OSM_RelateOrderRelatedListCC_Test {
    
    private static testMethod void testRelatedOrderRelatedList(){
        /* START SETUP */
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Trigger__c = false;
        triggerSwitch.Product2__c = true;
        insert triggerSwitch;
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
        
        Boolean hasNext;
        Boolean hasPrevious;
        Account acct = OSM_DataFactory.createAccount('Jats Matres');
        insert acct;
        Contact ctct = OSM_DataFactory.createContact('Jats','Matres',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;
        Order ords = new Order(OSM_Patient__c = ctct.Id, AccountId = acct.Id , RecordTypeId = rt, EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic');
        ords.PriceBook2Id = pb2Standard;
        insert ords;
        
        List<Order> orderLists = new List<Order>();
        Order order1 = new Order(OSM_Patient__c = ctct.Id, AccountId = acct.Id , RecordTypeId = rt, EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic');
        order1.OSM_Related_Order__c = ords.Id;
        order1.PriceBook2Id = pb2Standard;
        orderLists.add(order1);
        Order order2 = new Order(OSM_Patient__c = ctct.Id, AccountId = acct.Id , RecordTypeId = rt, EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic');
        order2.PriceBook2Id = pb2Standard;
        orderLists.add(order2);
        Order order3 = new Order(OSM_Patient__c = ctct.Id, AccountId = acct.Id , RecordTypeId = rt, EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic');
        order3.PriceBook2Id = pb2Standard;
        orderLists.add(order3);

        insert orderLists;
        PageReference pageRef = new PageReference('/' + ctct.Id);
        /* END SETUP */
        
        /* START TESTING */
        Test.startTest();
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ords);
        OSM_RelateOrderRelatedListCC controller = new OSM_RelateOrderRelatedListCC(sCon);
        controller.nextPage();
        controller.previousPage();
        hasNext = controller.hasNext;
        controller.lastPage();
        controller.firstPage();
        hasPrevious = controller.hasPrevious;
        Test.stopTest();
        /* END TESTING */
        /* START VERIFICATION */
        Id conID = ApexPages.CurrentPage().getparameters().get('id');
        orderLists = [SELECT RecordTypeId,OrderNumber, OSM_Work_Order_ID__c, OSM_Product__c, OSM_Ordering_HCP_Name__c, CreatedDate, Status, OSM_Multiple_Primaries__c, OSM_Triage_Outcome__c
                    FROM Order
                    Where OSM_Patient__c = :conID AND RecordTypeId = :rt];
        for(Order ord :orderLists) {
            System.assertEquals(rt,ord.RecordTypeId,'The record type of order must be Order.');
        }
        /* END VERIFICATION */
    }
    
    private static testMethod void testContactPatientOrderRList_EmptyList() {
        /* START SETUP */
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Trigger__c = false;
        triggerSwitch.Product2__c = true;
        insert triggerSwitch;
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
        
        Boolean error = false;
        Boolean hasNext;
        Boolean hasPrevious;
        Account acct = OSM_DataFactory.createAccount('Jats Matres');
        insert acct;
        Contact ctct = OSM_DataFactory.createContact('Jats','Matres',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;
        Order ords = new Order(OSM_Patient__c = ctct.Id, AccountId = acct.Id , RecordTypeId = rt, EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic');
        ords.PriceBook2Id = pb2Standard;
        insert ords;
        
        PageReference pageRef = new PageReference('/' + ctct.Id);
        /* END SETUP */
        
        Id conID;
        
        /* START TESTING */
        Test.startTest();
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ords);
        ApexPages.currentPage().getParameters().put('Id',ords.id);
        conID = (Id) ApexPages.CurrentPage().getparameters().get('Id');
        OSM_RelateOrderRelatedListCC controller = new OSM_RelateOrderRelatedListCC(sCon);
        controller.previousPage();
        controller.lastPage();
        Test.stopTest();
        /* END TESTING */
        
        /* START VERIFICATION */
        List<Order> orderLists = [SELECT RecordTypeId,OrderNumber, OSM_Work_Order_ID__c, OSM_Product__c, OSM_Ordering_HCP_Name__c, CreatedDate, Status, OSM_Multiple_Primaries__c, OSM_Triage_Outcome__c
                    FROM Order
                    Where Id = :conID AND RecordTypeId = :rt];
        System.assertNotEquals(orderLists.isEmpty(),true,'error');
         
        
        /* END VERIFICATION */
    }
    
    private static testMethod void testRelatedOrderRelatedListCopy(){
        /* START SETUP */
        //this is for the hasNext = false;
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Trigger__c = false;
        triggerSwitch.Product2__c = true;
        insert triggerSwitch;
        
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
        
        Account acct = OSM_DataFactory.createAccount('Jats Matres');
        insert acct;
        Contact ctct = OSM_DataFactory.createContact('Jats','Matres',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;
        Order ords = new Order(OSM_Patient__c = ctct.Id, AccountId = acct.Id , RecordTypeId = rt, EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic');
        ords.PriceBook2Id = pb2Standard;
        insert ords;
        
        List<Order> orderLists = new List<Order>();
        for(Integer i=0; i<10; i++){
            orderLists.add(new Order(OSM_Patient__c = ctct.Id, AccountId = acct.Id , RecordTypeId = rt, EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'Domestic'));
            orderLists[i].OSM_Related_Order__c = ords.Id;
            orderLists[i].PriceBook2Id = pb2Standard;
        }
        insert orderLists;
        Boolean hasNext;
        Boolean hasPrevious;
        //PageReference pageRef = new PageReference('https://cs9.salesforce.com/?id=' + ctct.Id);
        PageReference pageRef = page.OSM_RelateOrderRelatedList;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sCon = new ApexPages.StandardController(ords);
        OSM_RelateOrderRelatedListCC controller = new OSM_RelateOrderRelatedListCC(sCon);
        /* END SETUP */
        
        /* START TESTING */
        Test.startTest();
        controller.nextPage();
        hasPrevious = controller.hasPrevious;
        controller.nextPage();
        controller.previousPage();
        hasNext = controller.hasNext;
        controller.lastPage();
        controller.nextPage();
        controller.firstPage();
        controller.updateCounter();
        Test.stopTest();
        /* END TESTING */
        /* START VERIFICATION */
        Id conID = ApexPages.CurrentPage().getparameters().get('id');
        //System.assert(hasNext);
        System.assert(hasPrevious);
        
    }
}