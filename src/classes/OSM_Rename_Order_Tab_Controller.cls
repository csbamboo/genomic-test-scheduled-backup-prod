/*
    @author: Rescian Rey
    @description: Controller for the console component which will the Tab name for Order
    @createdDate: 15 APR 2015
    @history:
*/
public with sharing class OSM_Rename_Order_Tab_Controller {
    private Order ord;
    public String tabName {get; set;}
    public final static String DATE_FORMAT = 'MMM-dd-yy';
    public final static String DELIMITER = ', ';

    /*
        @author: Rescian Rey
        @description: Initializes the Tab name. Should be in this format:
            Patient - Initials, Patient - DOB, Product
        @createdDate: 15 APR 2015
        @history:
    */
	public OSM_Rename_Order_Tab_Controller(ApexPages.StandardController controller) {
        // Get Order record
        Id orderId = controller.getRecord().Id;
        tabName = 'New Order';
        try{
            ord = [SELECT OSM_Patient_DOB__c, Work_Order_Patient_DOB__c, OSM_Patient_Initials__c, OSM_Product__c FROM Order
                WHERE Id = :orderId];
        }catch(System.QueryException e){
            return;
        }

        // Format DOB if not null
        Date d = ord.OSM_Patient_DOB__c==null?ord.Work_Order_Patient_DOB__c:ord.OSM_Patient_DOB__c;
        String formattedDOB;
        if(d == null){
            formattedDOB = '<none>';
        }else{
            Datetime dt = datetime.newInstance(d.year(), d.month(), d.day());
            formattedDOB = dt.format(DATE_FORMAT);
        }

        // Construct tab name
        List<String> tabNameComponents = new List<String>{
                ord.OSM_Patient_Initials__c==null?'<none>':ord.OSM_Patient_Initials__c,
                formattedDOB,
                ord.OSM_Product__c
            };

		tabName = String.join(tabNameComponents, DELIMITER);
        System.debug('TAB NAME: ' + tabName);
	}
}