/*
    @author: Rescian Rey
    @description: Test Class for OSM_Rename_Order_Tab_Controller
    @createdDate: 15 APR 2015
    @history:
*/
@isTest
private class OSM_Rename_Order_Tab_Controller_Test {

    /*
        @author: Rescian Rey
        @description: null test. Tests if the Order tab name is constructed according to this format:
            Patient - Initials, Patient - DOB, Product
        @createdDate: 15 APR 2015
        @history:
    */
    static testmethod void tabTitleConstruction_nullTest(){
        Account acc = OSM_DataFactory.createAccount('Sample Account');
        insert acc;

        Id patientRecordTypeID = OSM_DataFactory.recordLists().Contact_Patient_Record_Type__c;
        Contact patient = OSM_DataFactory.createContact(1, acc.Id, patientRecordTypeID);
        insert patient;

        Order ord = OSM_DataFactory.createOrder('New Order', patient.Id, acc.Id, System.Today(), 'Active');
        insert ord;

        ApexPages.StandardController stdcontroller = new ApexPages.StandardController(ord);
        OSM_Rename_Order_Tab_Controller controller = new OSM_Rename_Order_Tab_Controller(stdcontroller);

        System.assertEquals('<none>, <none>, Unknown', controller.tabName);
    }

    /*
        @author: Rescian Rey
        @description: Tests if the Order tab name is constructed according to this format:
            Patient - Initials, Patient - DOB, Product
        @createdDate: 15 APR 2015
        @history:
    */
    static testmethod void tabTitleConstruction_normalTest(){
        Account acc = OSM_DataFactory.createAccount('Sample Account');
        insert acc;

        Id patientRecordTypeID = OSM_DataFactory.recordLists().Contact_Patient_Record_Type__c;
        Contact patient = OSM_DataFactory.createContact('John', 'Smith', patientRecordTypeID);
        insert patient;

        Order ord = OSM_DataFactory.createOrder('New Order', patient.Id, acc.Id, System.Today(), 'Active');
        ord.OSM_Patient_DOB__c = Date.today();
        ord.OSM_Patient_Initials__c = 'JS';
        ord.OSM_Product__c = 'MMR';
        insert ord;

        ApexPages.StandardController stdcontroller = new ApexPages.StandardController(ord);
        OSM_Rename_Order_Tab_Controller controller = new OSM_Rename_Order_Tab_Controller(stdcontroller);


        DateTime dt = datetime.newInstance(ord.OSM_Patient_DOB__c.year(), ord.OSM_Patient_DOB__c.month(), ord.OSM_Patient_DOB__c.day());

        List<String> tabNameComponents = new List<String>{
                ord.OSM_Patient_Initials__c,
                dt.format(OSM_Rename_Order_Tab_Controller.DATE_FORMAT),
                ord.OSM_Product__c
            };
        String expected = String.join(tabNameComponents, OSM_Rename_Order_Tab_Controller.DELIMITER);
        System.assertEquals(expected, controller.tabName);
    }
}