/**
 * File Info
 * ----------------------------------
 * @filename       OSM_ResultTriggerHandler.cls
 * @created        29.MAY.2015
 * @author         Vince Canlas
 * @description    Class for Result Trigger changes. 
 * @history        29.MAY.2015 - Vince Canlas - Created  
 */ 
public class OSM_ResultTriggerHandler {
    public static Boolean runLabReportStatusUpdate = false;
    public static Boolean updateCurrentCheckRunOnce = false;
    public static Boolean autoNumGenRunOnce = false;
    
    /**
    * @author         Jerome Liwanag
    * @description    Method for any changes that should happen after an OSM_Result__c record is updated. 
    * @history        1.JUNE.2015 - Jerome Liwanag - Created  
    */
    public static void onAfterUpdate(List<OSM_Result__c> newResultList, Map<Id,OSM_Result__c> oldResultMap){
        if(!runLabReportStatusUpdate){
            runLabReportStatusUpdate = true;
            labReportStatusUpdate(newResultList,oldResultMap);
        }
        
        if(!updateCurrentCheckRunOnce){
            updateCurrentCheckRunOnce = true;
            updateCurrentCheck(newResultList);
        }
        
    }
    
    /**
    * @author         Daniel QUismorio
    * @description    Method for any changes that should happen after an OSM_Result__c record is inserted. 
    * @history        17.JULY.2015 - Janiel QUismorio - Created  
    */
    public static void onAfterInsert(List<OSM_Result__c> newResultList){
        
        if(!updateCurrentCheckRunOnce){
            updateCurrentCheckRunOnce = true;
            updateCurrentCheck(newResultList);
        }
        
    }
    
    
    /**
    * @author         Daniel QUismorio
    * @description    Method for any changes that should happen after an OSM_Result__c record is inserted. 
    * @history        17.JULY.2015 - Janiel QUismorio - Created  
    */
    public static void onBeforeInsert(List<OSM_Result__c> newResult){
        //if(!autoNumGenRunOnce &&  !OSM_Utilities.ProcessResultsReadyMessageIsRunning){
            //autoNumGen(newResult);
        //}
    }
    
     /**
     * @author         Jerome Liwanag
     * @description    
     * @history        1.JUNE.2015 - Jerome Liwanag - Created  
     */
    public static void labReportStatusUpdate(List<OSM_Result__c> resultList, Map<Id,OSM_Result__c> oldResultMap){
        Set<Id> resultIds = new Set<Id>();
        Map<Id,OrderItem> orderItemMap = new Map<Id,OrderItem>();
        List<OrderItem> orderItemList = new List<OrderItem>();
        
        for(OSM_Result__c loopResult : resultList){
            resultIds.add(loopResult.Id);
        }
        
        for(OrderItem loopOrderItem : [SELECT Id, OrderId, OSM_Incompatible_Criteria_Desc1__c, OSM_Incompatible_Criteria_Desc2__c, 
                                              OSM_Missing_Data_Description__c, OSM_Node_Status_Determination_Desc__c, OSM_Order_Line_Item_ID__c, OSM_Lab_and_Report_Status__c, 
                                              OSM_Work_Order__c, OSM_Work_Order__r.OSM_State__c, OSM_Result_Current__c, OSM_Result_Current__r.OSM_Current__c,
                                              OSM_Result_Current__r.OSM_Distributed__c, OSM_Result_Current__r.OSM_Result_Disposition__c
                                       FROM OrderItem WHERE OSM_Result_Current__c IN :resultIds]){
            orderItemMap.put(loopOrderItem.OSM_Result_Current__c,loopOrderItem);
        }
        
        for(OSM_Result__c loopResult : resultList){
            if(orderItemMap.containsKey(loopResult.Id)){
                orderItemMap.get(loopResult.Id).OSM_Result_Current__c = loopResult.Id;
                
                if(orderItemMap.get(loopResult.Id).OSM_Work_Order__r.OSM_State__c == 'Closed' && orderItemMap.get(loopResult.Id).OSM_Lab_and_Report_Status__c != 'Canceled' && 
                   orderItemMap.get(loopResult.Id).OSM_Result_Current__r.OSM_Current__c && orderItemMap.get(loopResult.Id).OSM_Result_Current__r.OSM_Distributed__c &&
                   (orderItemMap.get(loopResult.Id).OSM_Result_Current__r.OSM_Result_Disposition__c == 'Results' || orderItemMap.get(loopResult.Id).OSM_Result_Current__r.OSM_Result_Disposition__c == 'Failure')){
                    orderItemMap.get(loopResult.Id).OSM_Lab_and_Report_Status__c = 'Distributed';
                }
                
                orderItemList.add(orderItemMap.get(loopResult.Id));
            }
        }
        
        if(!orderItemList.isEmpty()){
            update orderItemList;
        }
    }
        
    /**
     * @author         Kristian Vegerano
     * @description    Method that updates the Lab and Report Status field to Distributed
     * @parameter      List of Order Line Item records 
     * @history        29.MAY.2015 - Kristian Vegerano - Created 
     */    
    public static void updateToDistributed(List<OrderItem> newOrderLineItem){
        Set<Id> orderItemIds = new Set<Id>();
        Map<Id, OrderItem> orderItemMap = new Map<Id, OrderItem>();
        
        //Collect OrderItem Ids
        for(OrderItem loopOrderItem : newOrderLineItem){
            orderItemIds.add(loopOrderItem.Id);
        }
        
        //Collect cross-reference fields of the OrderItem
        orderItemMap = new Map<Id, OrderItem>([SELECT Id, OSM_Lab_and_Report_Status__c, OSM_Work_Order__c, OSM_Work_Order__r.OSM_State__c, OSM_Result_Current__c, OSM_Result_Current__r.OSM_Current__c,
                                                      OSM_Result_Current__r.OSM_Distributed__c, OSM_Result_Current__r.OSM_Result_Disposition__c
                                               FROM OrderItem
                                               WHERE Id IN :orderItemIds]);
    
        for(OrderItem loopOrderItem : newOrderLineItem){
            if(orderItemMap.get(loopOrderItem.Id).OSM_Work_Order__r.OSM_State__c == 'Closed' && orderItemMap.get(loopOrderItem.Id).OSM_Lab_and_Report_Status__c != 'Canceled' && 
               orderItemMap.get(loopOrderItem.Id).OSM_Result_Current__r.OSM_Current__c && orderItemMap.get(loopOrderItem.Id).OSM_Result_Current__r.OSM_Distributed__c &&
               (orderItemMap.get(loopOrderItem.Id).OSM_Result_Current__r.OSM_Result_Disposition__c == 'Results' || orderItemMap.get(loopOrderItem.Id).OSM_Result_Current__r.OSM_Result_Disposition__c == 'Failure')){
                loopOrderItem.OSM_Lab_and_Report_Status__c = 'Distributed';
            }
        }
    }
    
    /*
      @author: Karla Reytas/ Roden Songco
      @date: 20 APR 2015
      @description:Order Specimen Result Trigger Handler
      @history:20 APR 2015 - Created (KR/RS)
    
    */
    public static void autoNumGen(List<OSM_Result__c> newResult){
        //************
        
        String orderNumber = '';
        String a = '';
        Integer aLength;
        String tempNumber = '';
        Integer num;
        
        //***************
        
        Record_Type__c systemSet = Record_Type__c.getOrgDefaults();
        
        Integer autoInt = Integer.valueOf(systemSet.Auto_Number_Generator__c);
        String autoNumTemp;
        
        Set<Id> tempOrder = new Set<Id>();
        
        //Map<Id, Order> ordIdMap = new Map<Id, Order>([SELECT Id, OrderNumber FROM Order limit 1]);
        
        
        for(OSM_Result__c getOrderLoop : newResult){
            tempOrder.add(getOrderLoop.Order_ID__c);
        }
        
        List<OSM_Result__c> res = new List<OSM_Result__c>();
        res = [SELECT id, Name, Order_ID__c, OSM_Report_ID__c, OSM_Report_Date__c, Order_Line_Item_ID__c FROM OSM_Result__c Where Order_ID__c IN: tempOrder ORDER BY Name DESC];
        system.debug('************'+res);
        Map<Id, String> mapIdName = new Map<Id, String>();
        for(Order orLoop : [Select Id, OrderNumber From Order Where Id IN: tempOrder]){
            mapIdName.put(orLoop.Id, orLoop.OrderNumber);
        }

        Map<Id, List<String>> mapOrIdOrName = new Map<Id, List<String>>();
        
        //Query all results in the Order
        Map<String, Date> mapReportDate = new Map<String, Date>();
        String uniqueId = '';
        for(OSM_Result__c resLoop : res){
            if(!mapOrIdOrName.keySet().contains(resLoop.Order_ID__c)){
                mapOrIdOrName.put(resLoop.Order_ID__c, new List<String>{resLoop.OSM_Report_ID__c});
            } else {
                mapOrIdOrName.get(resLoop.Order_ID__c).add(resLoop.OSM_Report_ID__c);
            }
            
            uniqueId = resLoop.Order_ID__c + '|' + resLoop.Order_Line_Item_ID__c; 
            if(mapReportDate.get(uniqueId) == null){
               mapReportDate.put(uniqueId, resLoop.OSM_Report_Date__c); 
            }
        }

        //Query all results with the same Report ID as Order ID
        Map<Id, List<String>> resFilterMap = new Map<Id, List<String>>();
        
        for(Id oridLoop : mapOrIdOrName.keySet()){
            for(String reportLoop : mapOrIdOrName.get(oridLoop)){
                if(reportLoop != null){
                    a = reportLoop.left(11);
                    if(mapIdName.get(oridLoop) == a){
                        if(!resFilterMap.keySet().contains(oridLoop)){
                            resFilterMap.put(oridLoop, new List<String>{reportLoop});
                        } else {
                            resFilterMap.get(oridLoop).add(reportLoop);
                        }
                    }
                }
            }
        }

        //Getting Report Name 
        Map<Id, Integer> curReportNumberMap = new Map<Id, Integer>();
        for(Id resIdLoop : resFilterMap.keySet()){
            
            
            
            aLength = resFilterMap.get(resIdLoop)[0].length();
            a = String.ValueOf(resFilterMap.get(resIdLoop)[0]).substring(12,aLength);
            
            if(!curReportNumberMap.keySet().contains(resIdLoop)){
                curReportNumberMap.put(resIdLoop, Integer.ValueOf(a));
            }
        }
        
        List <OSM_Result__c> resultToUpdate = new List <OSM_Result__c>();
        //Populate Report ID if report ID is null
        for(OSM_Result__c resLoop : newResult){
                    
                if(resLoop.OSM_Report_ID__c == null || resLoop.OSM_Report_ID__c == ''){
                    orderNumber = String.ValueOf(mapIdName.get(resLoop.Order_ID__c));
                    if(curReportNumberMap.get(resLoop.Order_ID__c) != null){
                        num = curReportNumberMap.get(resLoop.Order_ID__c) + 1;
                        if(String.ValueOf(curReportNumberMap.get(resLoop.Order_ID__c)).length() == 1){
                           autoNumTemp =  orderNumber + '-0' + String.ValueOf(num);
                        }else{
                            autoNumTemp =  orderNumber + '-' + String.ValueOf(num);
                        }
                    }else{
                        autoNumTemp =  orderNumber + '-01';
                    }
                    
                    resLoop.OSM_Report_ID__c = autoNumTemp;
                }
                    if(resLoop.OSM_Report_Date__c == null){
                        
                        uniqueId = resLoop.Order_ID__c + '|' + resLoop.Order_Line_Item_ID__c;
                        
                        if(mapReportDate.get(uniqueId) == null){
                            resLoop.OSM_Report_Date__c = system.today();
                        } else {
                            resLoop.OSM_Report_Date__c = mapReportDate.get(uniqueId);
                        }
                        
                    }
                    if(resLoop.OSM_Corrected_Type__c != '' && resLoop.OSM_Corrected_Type__c != null){
                        resLoop.OSM_Modified_Date__c = system.today();
                    }
                    resultToUpdate.add(resLoop);
        }
        
        update resultToUpdate;
        
        autoNumGenRunOnce = true;
    }
    
    /*
    public static void newCurrentCheck(List<OSM_Result__c> newResult){
        
        Set<Id> resultSets = new Set<Id>();
        
        for(OSM_Result__c res : newResult){
            if(res.OSM_Current__c == true){
                resultSets.add(res.OSM_Resulted_Order_Specimen_ID__c);
            }
        }
        
        List<OSM_Result__c> currentResultList = new List<OSM_Result__c>();
        currentResultList = [SELECT OSM_Resulted_Order_Specimen_ID__c, OSM_Current__c FROM OSM_Result__c
        WHERE (OSM_Resulted_Order_Specimen_ID__c IN :resultSets AND OSM_Current__c = true)];
        
        for(OSM_Result__c r: currentResultList){
                r.OSM_Current__c = false;
        }
            update currentResultList;
        
    }
    */
    
     /**
    * @author         Daniel QUismorio
    * @description    Method for flagging the latest current Result
    */
    public static void updateCurrentCheck(List<OSM_Result__c> currentResult){
        
        Map<Id, Id> resultOSMAp = new Map<Id, Id>();
        Set<String> uniqueOS = new Set<String>();
        Set<String> oliIds = new Set<String>();
        
        for(OSM_Result__c res : currentResult){
            if(res.OSM_Current__c == true){
                resultOSMAp.put(res.Id, res.Order_ID__c);
                uniqueOS.add(res.Order_ID__c + '|' + res.Order_Line_Item_ID__c);
                oliIds.add(res.Order_Line_Item_ID__c);
            }
        }
        
        List<OSM_Result__c> currentResultList = new List<OSM_Result__c>();
        String uniqueId = '';
        for(OSM_Result__c r: [SELECT Id, OSM_Current__c, OSM_Resulted_Order_Specimen_ID__c, Order_Line_Item_ID__c, Order_ID__c FROM OSM_Result__c Where Order_ID__c IN: resultOSMAp.values() And Order_Line_Item_ID__c IN: oliIds]){
            
            uniqueId = r.Order_ID__c + '|' + r.Order_Line_Item_ID__c;
            
            if(resultOSMAp.get(r.Id) == null && r.OSM_Current__c && uniqueOS.contains(uniqueId)){
                r.OSM_Current__c = false;
                currentResultList.add(r);
            } 
                
        }
            if(!currentResultList.isEmpty()){
                update currentResultList;
            }
        
    }
    
}