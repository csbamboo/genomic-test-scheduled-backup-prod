@isTest
private class OSM_ResultTriggerHandler_Test {
    
	private static testMethod void test() {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Work_Order_Trigger__c = false;
        insert triggerSwitch;
        
        Record_Type__c recType = new Record_Type__c();
        recType.Auto_Number_Generator__c ='21';
        insert recType;
        
        // PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        Id pb2Standard = Test.getStandardPricebookId();
        
        Account accList = OSM_DataFactory.createAccount('Dasdwe');
        insert accList;
        
        Contact conList = OSM_DataFactory.createContact(123, accList.Id, Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        insert conList;
        
        Order ordList =  new Order(OSM_Patient__c = conList.Id, AccountId = accList.Id ,Pricebook2Id = pb2Standard, EffectiveDate = System.Today(), Status = 'New');
        insert ordList;
        ordList = [select Id, OrderNumber from Order limit 1];
        
        System.debug('@*********** test class ordernumber debug: ' + ordList.OrderNumber);
        //Pricebook2 pb2 = [SELECT Id FROM Pricebook2 WHERE isStandard = true];
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard, 7);
        insert pbe;
        
        OSM_Order_Specimen__c oSpecimenList = new OSM_Order_Specimen__c();
        oSpecimenList.Name = 'Name';
        oSpecimenList.CurrencyIsoCode = 'USD';
        insert oSpecimenList;
        
        List<OSM_Result__c> resultList = new List<OSM_Result__c>();
        OSM_Result__c result1 = new OSM_Result__c();
        result1.OSM_Results_Approval_Date__c = System.Today();
        result1.OSM_Resulted_Order_Specimen_ID__c = oSpecimenList.Id;
        result1.OSM_Result_Disposition__c = 'Results';
        result1.CurrencyIsoCode = 'USD';
        //result1.Order_Line_Item_ID__c = ordItem.Id;
        result1.OSM_Distributed__c = true;
        result1.OSM_Current__c = true;
        //result1.OSM_Result_Disposition__c = 'Result';
        result1.OSM_First_Distribution_Date__c = System.Today();
        resultList.add(result1);
        
        OSM_Result__c result2 = new OSM_Result__c();
        result2.OSM_Results_Approval_Date__c = System.Today();
        result2.OSM_Resulted_Order_Specimen_ID__c = oSpecimenList.Id;
        result2.OSM_Result_Disposition__c = 'Failure';
        result2.CurrencyIsoCode = 'USD';
        result2.OSM_Distributed__c = true;
        result2.OSM_Current__c = true;
        result2.Order_Id__c = ordList.Id;
        result2.OSM_Report_ID__c = ordList.OrderNumber + '-0';
        result2.OSM_First_Distribution_Date__c = System.Today();
        result2.OSM_Failure_Code__c = 'MISSING_OR_DAMAGED_SPECIMEN';
        resultList.add(result2);
        insert resultList;
        
        
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(ordList.Id);
        insert workOrder;
        workOrder.OSM_State__c = 'Closed';
        update workOrder;
        
        OrderItem ordItem = new OrderItem(OrderId = ordList.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, 
                                          OSM_Lab_and_Report_Status__c = 'Pre-Processing', OSM_Result_Current__c = result2.Id, OSM_Work_Order__c = workOrder.Id );
        OrderItem ordItem2 = new OrderItem(OrderId = ordList.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, 
                                          OSM_Lab_and_Report_Status__c = 'Pre-Processing', OSM_Result_Current__c = result1.Id, OSM_Work_Order__c = workOrder.Id );
        List<OrderItem> ordItemList = new List<OrderItem>();
        ordItemList.add(ordItem);
        ordItemList.add(ordItem2);
        // ordItemList.add(ordItem2);
        // ordItem.OSM_Lab_and_Report_Status__c = 'Pre-Processing';
        // ordItem.OSM_Result_Current__c = result1.Id;
        // ordItem.OSM_Work_Order__c = workOrder.Id;
        
        
        //ordItem.OSM_Result_Current__r.OSM_Result_Disposition__c = 'Result';
        insert ordItemList;
        
        result1.Order_Id__c = ordList.Id;
        result1.Order_Line_Item_ID__c = ordItem.Id;
        result1.OSM_Result_Disposition__c = 'Results';
        update result1;
        
        ordItem.OSM_Result_Current__c = result1.Id;
        update ordItem;
        
        test.startTest();
        OSM_ResultTriggerHandler.autoNumGen(resultList);
        // OSM_ResultTriggerHandler.newCurrentCheck(resultList);
        OSM_ResultTriggerHandler.updateToDistributed(ordItemList);
        OSM_ResultTriggerHandler.updateCurrentCheck(resultList);
        
        
        OSM_ResultTriggerHandler.updateToDistributed(ordItemList);
        test.stopTest();
        
        System.assertEquals(ordItem.OSM_Result_Current__r.OSM_Current__c, ordItem.OSM_Result_Current__r.OSM_Distributed__c, 'error');
        //System.assertEquals('Results', ordItem.OSM_Result_Current__r.OSM_Result_Disposition__c, 'error');
	    
	    
	}
    
}