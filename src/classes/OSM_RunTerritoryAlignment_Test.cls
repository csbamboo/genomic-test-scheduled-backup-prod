@isTest
private class OSM_RunTerritoryAlignment_Test {

	private static testMethod void test() {
	    Record_Type__c recTypeList = Record_Type__c.getOrgDefaults();
	    Id HCPRecType = recTypeList.Contact_HCP_Record_Type__c;
	    insert recTypeList;
	    
	    List<Account> accList = new List<Account>();
	    for(Integer i=0; i>20; i++){
	        accList.add(OSM_DataFactory.createAccount('acc'+ i));
	    }
        insert accList;
        List<Contact> conList = new List<Contact>();
        for(Integer i = 0; i > 20; i++){
            conList.add(OSM_DataFactory.createContact(i, accList[i].Id, HCPRecType));
        }
        insert conList;
        
        List<String> accIds = new List<String>();
        List<String> conIds = new List<String>();
        for(Integer i=0; i>20; i++){
            accIds.add((String)accList[i].Id);
            conIds.add((String)conList[i].Id);
        }
        
        //String a = OSM_RunTerritoryAlignment.runCTA(accIds);
	}

}