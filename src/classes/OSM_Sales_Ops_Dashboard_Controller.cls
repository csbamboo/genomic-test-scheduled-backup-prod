public class OSM_Sales_Ops_Dashboard_Controller {
    
    public OSM_Sales_Ops_Dashboard_Controller(){
        
    }
    
    public PageReference runATA(){
        /*
        OSM_TerritoryAlignmentBatchAccount runATABatch = new OSM_TerritoryAlignmentBatchAccount();
        runATABatch.isReRun = false;
        Database.executeBatch(runATABatch, 10);
         
        OSM_TerritoryAlignmentBatchContact runCTABatch = new OSM_TerritoryAlignmentBatchContact();
        runCTABatch.isReRun = false;
        Database.executeBatch(runCTABatch, 10);
        
        OSM_TerritoryAlignmentBatchOrder runOTABatch = new OSM_TerritoryAlignmentBatchOrder();
        runOTABatch.isReRun = false;
        Database.executeBatch(runOTABatch, 10);
          */
        return null;
    } 
    
    public PageReference runATABatch(){
        OSM_TerritoryAlignmentBatchAccount runATABatch = new OSM_TerritoryAlignmentBatchAccount();
        runATABatch.isReRun = false;
        Database.executeBatch(runATABatch, 100);
        
        return null;
    }
    
    public PageReference runCTABatch(){
        OSM_TerritoryAlignmentBatchContact runCTABatch = new OSM_TerritoryAlignmentBatchContact();
        runCTABatch.isReRun = false;
        Database.executeBatch(runCTABatch, 100);
        
         return null;
    }
    
     public PageReference runOTABatch(){
         OSM_TerritoryAlignmentBatchOrder runOTABatch = new OSM_TerritoryAlignmentBatchOrder();
        runOTABatch.isReRun = false;
        Database.executeBatch(runOTABatch, 100);
          
        return null;
    }
    
    
      
    public PageReference reRunATA(){
        OSM_TerritoryAlignmentBatchAccount runATABatch = new OSM_TerritoryAlignmentBatchAccount();
        runATABatch.isReRun = true;
        Database.executeBatch(runATABatch, 100);
  
        return null;
    }
    public PageReference reRunCTA(){
        OSM_TerritoryAlignmentBatchContact runCTABatch = new OSM_TerritoryAlignmentBatchContact();
        runCTABatch.isReRun = true;
        Database.executeBatch(runCTABatch, 100);
        
         return null;
    }
    public PageReference reRunOTA(){
        OSM_TerritoryAlignmentBatchOrder runOTABatch = new OSM_TerritoryAlignmentBatchOrder();
        runOTABatch.isReRun = true;
        Database.executeBatch(runOTABatch, 100);
        
         return null;
    }
}