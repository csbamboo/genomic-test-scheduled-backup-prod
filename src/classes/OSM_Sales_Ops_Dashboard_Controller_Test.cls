/*
      @author: David Catindoy
      @date: 5 FEB 2015 - Created
      @description: OSM_Sales_Ops_Dashboard_Controller (Test Class)
      @history: 5 FEB 2015 - Created
                6 AUG 2015 - Modified (Jerome liwanag)

*/
@isTest
private class OSM_Sales_Ops_Dashboard_Controller_Test {
	
	static testMethod void testATA() {
		
		PageReference pageRef = Page.OSM_Sales_Ops_Dashboard;
        Test.setCurrentPage(pageRef);
        OSM_Sales_Ops_Dashboard_Controller controller = new OSM_Sales_Ops_Dashboard_Controller();
        
        Test.startTest();
        controller.runATA();
        controller.runATABatch();
        controller.runCTABatch();
        controller.runOTABatch();
        
        Test.stopTest();
        

	}
		static testMethod void testReRunATA() {
		
		PageReference pageRef = Page.OSM_Sales_Ops_Dashboard;
        Test.setCurrentPage(pageRef);
        OSM_Sales_Ops_Dashboard_Controller controller = new OSM_Sales_Ops_Dashboard_Controller();

        Test.startTest();
        controller.reRunATA();
        controller.reRunCTA();
        controller.reRunOTA();
        
        Test.stopTest();
        

	}
	
}