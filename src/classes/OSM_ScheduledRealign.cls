global class OSM_ScheduledRealign implements Schedulable {

   ID triggerID = null;
   global OSM_ScheduledRealign(ID tID){
       triggerID = tID;    
   }
   global void execute(SchedulableContext SC) {
       OSM_RealignAccountTerritory batch = new OSM_RealignAccountTerritory(triggerID);
       Database.executeBatch(batch);   
   }
}