/*
    @author: patrick lorilla
    @date: 29 Oct 2014
    @description: Schedule Realign test
    @history: 29 Oct 2014 - Patrick Lorilla - Created

*/
@isTest
public class OSM_ScheduledRealign_Test{
    /*
     @author: patrick lorilla
     @date: 29 Oct 2014
     @description: Test Scheduler
     @history: 29 Oct 2014 - Patrick Lorilla - Created

    */
    static testmethod void testScheduler() {
        //prepare territory
        OSM_Territory__c pterr = OSM_DataFactory.createTerritory('My ParentTerr');
        insert pterr;
        //schedule job
        String sched = '0 0 1 ? * FRI';
        String jobId = System.schedule('testScheduler', sched , new OSM_ScheduledRealign(pterr.ID));
          
        // Retrieve CronTrigger data
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

        // assert if expressions are the same
        System.assertEquals(sched , ct.CronExpression);
             
        // assert if 0 times triggered
        System.assertEquals(0, ct.TimesTriggered);
    }


}