global with sharing class OSM_SchedulerClass implements Schedulable {
	global void execute(SchedulableContext sc) {
		OSM_TerritoryAlignmentBatchAccount runATABatch = new OSM_TerritoryAlignmentBatchAccount();
        Database.executeBatch(runATABatch, 50);
        
        OSM_TerritoryAlignmentBatchContact runCTABatch = new OSM_TerritoryAlignmentBatchContact();
        Database.executeBatch(runCTABatch, 50);
        
        OSM_TerritoryAlignmentBatchOrder runOTABatch = new OSM_TerritoryAlignmentBatchOrder();
        Database.executeBatch(runOTABatch, 50);
	}
	/* 
	public static void SchedulerMethod() {
		String con_exp = '0 0 1 * * ?';
		System.schedule('RunBatchApexTest', con_exp, new OSM_SchedulerClass());
	}
	*/
}