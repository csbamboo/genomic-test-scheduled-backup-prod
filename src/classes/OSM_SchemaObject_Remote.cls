/*
@author: Patrick Lorilla
@date: 21 October 2014
@description: Remote class for the javascript buttons of the address affilitation object.

*/

global class OSM_SchemaObject_Remote{ 

    /*
        @author: Patrick Lorilla
        @date: 21 October 2014
        @param: obj - objectname, exclusions - excluded fields;
        @description: Retrieve all fields.
   */
   Webservice static List<String> getFields(String obj, String excludeStr ){
        List<String> filtered = new List<String>();
        /*Set<String> exclusions = new Set<String>();
        for(String exc: excludeStr.split('::')){
            exclusions.add(exc);
        }
        
        //Retrieve all fields
        List<Schema.SObjectField> retrieved = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap().values();
        //Filter values 
        for(Schema.SObjectField ret: retrieved){
            if(!exclusions.contains(String.valueOf(ret)) && !ret.getDescribe().isAutoNumber()){
                filtered.add(String.valueOf(ret));
            }
        }*/
        
        Map<String, Schema.SobjectField> orderMap = Schema.SObjectType.Order.fields.getMap();
       
        for (String fieldKey : orderMap.keySet()) {
            Schema.SObjectField fsObj = orderMap.get(fieldKey);
            Schema.DescribeFieldResult f = fsObj.getDescribe();
            String fieldName = f.getName();
            
            if (f.isAccessible() && f.isUpdateable() && !f.isCalculated() && !f.isAutoNumber() && !f.isDeprecatedAndHidden() &&
                fieldName != 'OwnerId' && 
                fieldName != 'ActivatedById' && 
                fieldName != 'ActivatedDate' && 
                fieldName != 'StatusCode' && 
                fieldName != 'OSM_Related_Order__c' &&
                fieldName != 'OpportunityId' && 
                fieldName != 'QuoteId'){
                filtered.add(fieldName);
            }
        }
        return filtered;   
    }

    /**
     * @author         Kristian Vegerano
     * @description    Checks if all needed fields of Specimen Retrieval case have value, close the Specimen Retrieval Case and create a Specimen Arrival Case if so
     * @parameter      List of Case records 
     * @history        22.JAN.2014 - Kristian vegerano - Created  
     
    Webservice static String createSpecimenArrivalCase(Id caseId){
        String createdId = '';
        List<Case> newCase = [SELECT Id, RecordTypeId, OSM_Primary_Order__c, OSM_Primary_Customer__c, OSM_Specimen_Request_Date_Time__c, OSM_Tracking_Number_lookup__c
                              FROM Case 
                              WHERE Id = :caseId];
        Id specimenRetrievalId = [SELECT Id FROM RecordType WHERE DeveloperName = 'OSM_Specimen_Retrieval' LIMIT 1][0].Id;
        Id specimenArrivalId = [SELECT Id FROM RecordType WHERE DeveloperName = 'OSM_Specimen_Arrival' LIMIT 1][0].Id;
        Set<Id> specimenRetrievalIds = new Set<Id>();
        Set<Id> excludedSpecimenRetrievalIds = new Set<Id>();
        List<Case> insertCasesList = new List<Case>();
        List<Case> updateCasesList = new List<Case>();
        
        for(Case loopCase : newCase){
            if(loopCase.RecordTypeId == specimenRetrievalId && 
               loopCase.OSM_Primary_Order__c != null && 
               loopCase.OSM_Primary_Customer__c != null && 
               loopCase.OSM_Specimen_Request_Date_Time__c != null &&
               loopCase.OSM_Tracking_Number_lookup__c != null){
                specimenRetrievalIds.add(loopCase.Id);
                updateCasesList.add(new Case(Id = loopCase.Id, Status = 'Closed'));
            }
        }
        
        //Collect Specimen Retrieval Cases
        for(Case loopCase : [SELECT Id, OSM_Specimen_Retrieval_Case__c
                             FROM Case
                             WHERE OSM_Specimen_Retrieval_Case__c IN :specimenRetrievalIds]){
            excludedSpecimenRetrievalIds.add(loopCase.OSM_Specimen_Retrieval_Case__c);
        }
        
        //Create Specimen Arrival Cases
        for(Case loopCase : newCase){
            if(loopCase.RecordTypeId == specimenRetrievalId && 
               loopCase.OSM_Primary_Order__c != null && 
               loopCase.OSM_Primary_Customer__c != null && 
               loopCase.OSM_Specimen_Request_Date_Time__c != null &&
               loopCase.OSM_Tracking_Number_lookup__c != null){
                if(!excludedSpecimenRetrievalIds.contains(loopCase.Id)){
                    insertCasesList.add(new Case(OSM_Specimen_Retrieval_Case__c = loopCase.Id,
                                                 OSM_Primary_Order__c = loopCase.OSM_Primary_Order__c,
                                                 OSM_Primary_Customer__c = loopCase.OSM_Primary_Customer__c,
                                                 OSM_Specimen_Request_Date_Time__c = loopCase.OSM_Specimen_Request_Date_Time__c,
                                                 OSM_Tracking_Number_lookup__c = loopCase.OSM_Tracking_Number_lookup__c,
                                                 RecordTypeId = specimenArrivalId));
                }
            }
        }
        
        //Insert Arrival Case
        if(insertCasesList.size() > 0){
            insert insertCasesList;
            createdId += '' + insertCasesList[0].Id;
        }
        
        //Update Retrieval Case
        if(updateCasesList.size() > 0){
            update updateCasesList;
        }
        return createdId;
    }*/
}