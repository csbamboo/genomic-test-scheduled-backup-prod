/*
@author: David E. Catindoy
@date: 23 January 2015
@description: Test OSM_SchemaObject_Remote

*/

@isTest
public class OSM_SchemaObject_Remote_Test{ 

    static testmethod void testCreateAccountShare(){
        User testUsr = OSM_DataFactory.createUser('TestUser');
        Set<String> fields = new Set<String>();
        Test.startTest();
        System.runAs(testUsr){
            List<String> retrieveList = OSM_SchemaObject_Remote.getFields('OSM_Order_Role__c', 'CreatedById::CreatedByDate');
            for(String str: retrieveList){
                fields.add(str);
            }
        }
        Test.stopTest();
        System.assert(!fields.isEmpty());
        System.assert(!fields.contains('CreatedById'));
        System.assert(!fields.contains('CreatedByDate'));
    }
}