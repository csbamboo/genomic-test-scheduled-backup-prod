/**
 * File Info
 * ----------------------------------
 * @filename        OSM_SearchPage.cls
 * @created         27.JAN.2015
 * @author          Kristian Vegerano
 * @description     Controller class for OSM_SearchPage visualforce page. 
 * @history         27.JAN.2015 - Kristian Vegerano - Created  
 * @user Story      #9541 - Order Intake - Object Search
 * @user Story      #9545 - SA Case - Auto creation - SR Barcode Exists
 * @user Story      #9637 - SA Case - Auto Creation - No SR Barcode, Search Results
 *                  #66188 - Added error handling - Rescian Rey
 *                  10.SEP.2015 - Renamed case erro message variables - Rescian Rey
 */  
public class OSM_SearchPageController{
    public Order pageOrder {get;set;}
    
    public String orderNumber {get;set;}
    public String caseNumber {get;set;}
    public String patientFN {get;set;}
    public String patientLN {get;set;}
    public String patientDOB {get;set;}
    public String caseTrackingNumber {get;set;}
    public Boolean hasRun {get;set;}
    
    public Transient List<Case> caseList {get;set;}
    public Integer caseListSize {get;set;}
    public Transient List<Order> orderList {get;set;}
    public Integer orderListSize {get;set;}
    public Transient List<OrderItem> orderItemList {get;set;}
    public Integer orderItemListSize {get;set;}
    public List<Contact> patientList {get;set;}
    public Integer patientListSize {get;set;}
     
    
    //Pagination
    public Integer pageSize {get;set;}
    public List<Case> pagedCaseList {get;set;}
    public List<Order> pagedOrderList {get;set;}
    public List<OrderItem> pagedOrderItemList {get;set;}
    public List<Contact> pagedPatientList {get;set;}
    
    public Integer caseCurrentPage {get;set;}
    public Integer caseTotalPage {get;set;}
    public Integer caseCurrentRecord {get;set;}
    public Boolean caseHasNext {get;set;}
    public Boolean caseHasPrevious {get;set;}
    
    public Integer orderCurrentPage {get;set;}
    public Integer orderTotalPage {get;set;}
    public Integer orderCurrentRecord {get;set;}
    public Boolean orderHasNext {get;set;}
    public Boolean orderHasPrevious {get;set;}
    
    public Integer orderItemCurrentPage {get;set;}
    public Integer orderItemTotalPage {get;set;}
    public Integer orderItemCurrentRecord {get;set;}
    public Boolean orderItemHasNext {get;set;}
    public Boolean orderItemHasPrevious {get;set;}
    
    public Integer patientCurrentPage {get;set;}
    public Integer patientTotalPage {get;set;}
    public Integer patientCurrentRecord {get;set;}
    public Boolean patientHasNext {get;set;}
    public Boolean patientHasPrevious {get;set;}
    public Boolean showProcessSelectedCaseErrorOverride {get; set;}
    public String processSelectedCaseErrorMessageOverride {get; set;}
    

     /* @author         Kristian Vegerano
     * @description    Constructor. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public OSM_SearchPageController(){
        
        pageSize = 10;
        
        caseList = new List<Case>();
        pagedCaseList = new List<Case>();
        caseCurrentPage = 1;
        caseCurrentRecord = 1;
        caseHasNext = true;
        caseHasPrevious = false;
        
        orderList = new List<Order>();
        pagedOrderList = new List<Order>();
        orderCurrentPage = 1;
        orderCurrentRecord = 1;
        orderHasNext = true;
        orderHasPrevious = false;
        
        orderItemList = new List<OrderItem>();
        pagedOrderItemList = new List<OrderItem>();
        orderItemCurrentPage = 1;
        orderItemCurrentRecord = 1;
        orderItemHasNext = true;
        orderItemHasPrevious = false;
        
        patientList = new List<Contact>();
        pagedPatientList = new List<Contact>();
        patientCurrentPage = 1;
        patientCurrentRecord = 1;
        patientHasNext = true;
        patientHasPrevious = false;
        
        pageOrder = new Order();
        hasRun = false;
        
        caseListSize = 0;
        orderListSize = 0;
        orderItemListSize = 0;
        patientListSize = 0;

        showProcessSelectedCaseErrorOverride = false;
        processSelectedCaseErrorMessageOverride = '';
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that searches case, order, orderItem and patient records. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public PageReference search(){
        hasRun = true;
     //   OSM_OrderTriggerHandler.runUpdateAllChild = true;
        if(pageOrder.EndDate != null){
               patientDOB = pageOrder.EndDate.year() + '-' + (Integer.valueOf(pageOrder.EndDate.month()) < 10 ? '0' + pageOrder.EndDate.month() : '' + pageOrder.EndDate.month()) + '-' + (Integer.valueOf(pageOrder.EndDate.day()) < 10 ? '0' + pageOrder.EndDate.day() : '' + pageOrder.EndDate.day());
        }else{
            patientDOB = '';
        }
        
        pageSize = 10;
        caseCurrentPage = 1;
        caseCurrentRecord = 1;
        caseHasNext = true;
        caseHasPrevious = false;
        orderHasNext = true;
        orderHasPrevious = false;
        orderItemHasNext = true;
        orderItemHasPrevious = false;
        patientHasNext = true;
        patientHasPrevious = false;
        
        caseList = new List<Case>();
        pagedCaseList = new List<Case>();
        orderList = new List<Order>();
        pagedOrderList = new List<Order>();
        orderItemList = new List<OrderItem>();
        pagedOrderItemList = new List<OrderItem>();
        patientList = new List<Contact>();
        pagedPatientList = new List<Contact>();
        List<String> errorList = new List<String>();
        caseList = new List<Case>();
        orderList = new List<Order>();
        
        //Case Search
        caseList.addAll(searchCases());
        caseListSize = caseList.size();
        for(Integer counter = 0; counter < (caseList.size() < pageSize ? caseList.size() : pageSize); counter++){
            pagedCaseList.add(caseList[counter]);
        }
        caseTotalPage = (Integer)Math.ceil((Double)caseList.size()/pageSize);
        if(caseList.size() <= pageSize){
            caseHasNext = false;
        }
        
        //Order Search
        orderList.addAll(searchOrders());
        orderListSize = orderList.size();
        for(Integer counter = 0; counter < (orderList.size() < pageSize ? orderList.size() : pageSize); counter++){
            pagedOrderList.add(orderList[counter]);
        }
        orderTotalPage = (Integer)Math.ceil((Double)orderList.size()/pageSize);
        if(orderList.size() <= pageSize){
            orderHasNext = false;
        }
        
        //OrderItem Search
        orderItemList.addAll(searchOrderItems());
        orderItemListSize = orderItemList.size();
        for(Integer counter = 0; counter < (orderItemList.size() < pageSize ? orderItemList.size() : pageSize); counter++){
            pagedOrderItemList.add(orderItemList[counter]);
        }
        orderItemTotalPage = (Integer)Math.ceil((Double)orderItemList.size()/pageSize);
        if(orderItemList.size() <= pageSize){
            orderItemHasNext = false;
        }
        
        //Patient Search
        patientList.addAll(searchPatients());
        patientListSize =  patientList.size();
        for(Integer counter = 0; counter < (patientList.size() < pageSize ? patientList.size() : pageSize); counter++){
            pagedPatientList.add(patientList[counter]);
            
        }
        patientTotalPage = (Integer)Math.ceil((Double)patientList.size()/pageSize);
        if(patientList.size() <= pageSize){
            patientHasNext = false;
        }
        
            
        //Reset All
        caseNumber = caseNumber.trim();
        orderNumber = orderNumber.trim();
        orderItemList = new List<OrderItem>();
        patientList = new List<Contact>();
        
        if(caseList.size() == 1 && caseNumber != null && caseNumber != ''){
            Case processCase = caseList[0];
            Record_Type__c systemSettings = Record_Type__c.getOrgDefaults();
            if(processCase.Status == 'Closed'){
                // retreive recent Open SA Case
                List<Case> saCaseList = [SELECT Id FROM Case WHERE OSM_Specimen_Retrieval_Case__c = :processCase.Id and Status = 'Open' order by CreatedDate DESC LIMIT 1];
                    if(saCaseList.size() == 1){
                       //open SA case in edit mode for user to do data entry
                        return new PageReference('/' + saCaseList[0].Id + '/e?retURL=%2F' + saCaseList[0].Id );
                    } 
                
            } 
     
        }

        return null;
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that processes the selected specimen retrieval case. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public Id processSRCaseId {get;set;}
    public String casePageUrl {get;set;}

    public PageReference processSelectedCaseOutCon(){
        processSelectedCase();
        return new Pagereference(casePageUrl);
    }
    public PageReference processSelectedCase(){
   //     OSM_OrderTriggerHandler.runUpdateAllChild = true;
        Case processCase = [SELECT Id, Status, CaseNumber, OSM_Specimen_Request_Date_Time__c, OSM_Tracking_Number__c, OSM_Tracking_Number_lookup__c, OSM_Tracking_Number_lookup__r.OSM_Package_ID__c, OSM_Primary_Customer__c, OSM_Primary_Customer__r.Name, OSM_Primary_Order__c, OSM_Primary_Order__r.OrderNumber, OSM_Patient__c, OSM_Patient__r.Name, OSM_Patient_DOB_Formula__c, OSM_Product__c FROM Case WHERE Id  = :processSRCaseId];
        Record_Type__c systemSettings = Record_Type__c.getOrgDefaults();
        
        if(caseTrackingNumber != null && caseTrackingNumber != ''){
        
            List<OSM_Package__c> trackedPackage = [SELECT Id, OSM_SR_Barcode__c FROM OSM_Package__c WHERE name = :caseTrackingNumber];
            
            if(trackedPackage.size() == 1){
                if (trackedPackage[0].OSM_SR_Barcode__c != null && trackedPackage[0].OSM_SR_Barcode__c.trim() != '' ) {
                     trackedPackage[0].OSM_SR_Barcode__c +=  '\n' + processCase.CaseNumber; 
                } else {
                     trackedPackage[0].OSM_SR_Barcode__c = processCase.CaseNumber;
                }
                try{
                    update trackedPackage;
                }catch(DmlException exp){
                    // Override error message, show only one error message
                    processSelectedCaseErrorMessageOverride = exp.getDmlMessage(0);
                    showProcessSelectedCaseErrorOverride = true;
                    return null;
                }
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There are multiple package records that are associated to the entered Tracking Number.'));
                return null;
            }
        }
        //retreive recent open SA Cases related to given SR case
        List<Case> saCaseList = [SELECT Id FROM Case WHERE OSM_Specimen_Retrieval_Case__c = :processCase.Id AND status = 'Open' order by CreatedDate DESC LIMIT 1 ];
        if(saCaseList.size() == 1){
           //open SA case in edit mode when SR case has one SA case for user to do data entry
            casePageUrl = '/' + saCaseList[0].Id + '/e?retURL=%2F' + saCaseList[0].Id;
        }
        return null;
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that processes the selected order and creates a specimen arrival case. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public Id processOrderId {get;set;}
    public String orderPageUrl {get;set;}

    public PageReference processSelectedOrderOutCon(){
        processSelectedOrder();
        return new Pagereference(orderPageUrl);
    }
    public PageReference processSelectedOrder(){
    
        Order processOrder = [SELECT Id, OrderNumber FROM Order WHERE Id = :processOrderId][0];
        Record_Type__c systemSettings = Record_Type__c.getOrgDefaults();
        orderPageUrl = '/500/e?RecordType=' + systemSettings.RT_Case_Specimen_Arrival__c + '&ent=Case&retURL=/apex/OSM_SearchPage&CF' + systemSettings.OSM_CF_Case_Primary_Order__c + '=' + processOrder.OrderNumber + '&CF' + systemSettings.OSM_CF_Case_Primary_Order__c + '_lkid=' + processOrder.Id;
               
        if(caseTrackingNumber != null && caseTrackingNumber != ''){
                List<OSM_Package__c> trackedPackage = [SELECT Id, OSM_SR_Barcode__c FROM OSM_Package__c WHERE name = :caseTrackingNumber];
                if(trackedPackage.size() == 1){
                    orderPageUrl +=  '&CF' + systemSettings.OSM_CF_Case_Tracking_Number__c + '=' + caseTrackingNumber;
                 }
        }
 
        return null;
     }    
    
       
    /**
     * @author         Kristian Vegerano
     * @description    Method that resets the values all the fields shown on the page. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public void reset(){
        orderNumber = null;
        caseNumber = null;
        patientFN = null;
        patientLN = null;
        patientDOB = null;
        caseTrackingNumber = null;
       
        
        pageSize = 10;
        
        caseList = new List<Case>();
        pagedCaseList = new List<Case>();
        caseCurrentPage = 1;
        caseCurrentRecord = 1;
        caseHasNext = true;
        caseHasPrevious = false;
        
        orderList = new List<Order>();
        pagedOrderList = new List<Order>();
        orderCurrentPage = 1;
        orderCurrentRecord = 1;
        orderHasNext = true;
        orderHasPrevious = false;
        
        orderItemList = new List<OrderItem>();
        pagedOrderItemList = new List<OrderItem>();
        orderItemCurrentPage = 1;
        orderItemCurrentRecord = 1;
        orderItemHasNext = true;
        orderItemHasPrevious = false;
        
        patientList = new List<Contact>();
        pagedPatientList = new List<Contact>();
        patientCurrentPage = 1;
        patientCurrentRecord = 1;
        patientHasNext = true;
        patientHasPrevious = false;
        
        pageOrder = new Order();
        hasRun = false;
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that resets the value of the tracking number. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public void resetTrackingNumber(){
        caseTrackingNumber = null;
        
        
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that searches case records. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public List<Case> searchCases(){
        if((caseNumber != '' && caseNumber != null) || (orderNumber != '' && orderNumber != null) || (patientFN != '' && patientFN != null) || (patientLN != null && patientLN != '') || (patientDOB != '' && patientDOB != null)){
            String caseQuery = 'SELECT Id, Status, CaseNumber, OSM_Specimen_Request_Date_Time__c, OSM_Tracking_Number__c, OSM_Tracking_Number_lookup__c, OSM_Tracking_Number_lookup__r.OSM_Package_ID__c, OSM_Primary_Customer__c, OSM_Primary_Customer__r.Name, OSM_Primary_Order__c, OSM_Primary_Order__r.OrderNumber, OSM_Patient__c, OSM_Patient__r.Name, OSM_Patient_DOB_Formula__c, OSM_Product__c FROM Case WHERE RecordType.DeveloperName = \'OSM_Specimen_Retrieval\' ' + (caseNumber != '' && caseNumber != null ? ' AND CaseNumber LIKE \'%' + String.escapeSingleQuotes(CaseNumber) + '%\'' : '') + (orderNumber != '' && orderNumber != null ? 'AND OSM_Primary_Order__r.OrderNumber LIKE \'%' + String.escapeSingleQuotes(orderNumber) + '%\'' : '') + (patientFN != '' && patientFN != null ? ' AND OSM_Primary_Order__r.OSM_Patient__r.FirstName LIKE \'%' + String.escapeSingleQuotes(PatientFN) + '%\'' : '') + (patientLN != null && patientLN != '' ? ' AND OSM_Primary_Order__r.OSM_Patient__r.LastName LIKE \'%' + String.escapeSingleQuotes(PatientLN) + '%\'' : '') + (patientDOB != '' && patientDOB != null ? ' AND OSM_Primary_Order__r.OSM_Patient__r.BirthDate = ' + String.escapeSingleQuotes(PatientDOB) + ' AND OSM_Primary_Order__r.OSM_Patient__r.BirthDate != null' : '') + ' ORDER BY CaseNumber DESC';
            return Database.query(caseQuery);
        }
        return new List<Case>();
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that searches order records. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public List<Order> searchOrders(){
        if((orderNumber != null && orderNumber != '') || (patientFN != null && patientFN != '') || (patientLN != null && patientLN != '') || (patientDOB != '' && patientDOB != null)){
            String orderQuery = 'SELECT Id, OrderNumber, OSM_Patient__c, OSM_Patient__r.Name, OSM_Patient_DOB__c, OSM_Product__c FROM Order WHERE RecordType.DeveloperName = \'OSM_Order\'' + (orderNumber != null && orderNumber != '' ? ' AND OrderNumber LIKE \'%' + String.escapeSingleQuotes(orderNumber) + '%\'' : '') + (patientFN != null && patientFN != '' ? ' AND OSM_Patient_First_Name__c LIKE \'%' + String.escapeSingleQuotes(PatientFN) + '%\'' : '') + (patientLN != null && patientLN != '' ? ' AND OSM_Patient_Last_Name__c LIKE \'%' + String.escapeSingleQuotes(PatientLN) + '%\'' : '') + (patientDOB != '' && patientDOB != null ? ' AND OSM_Patient_DOB__c = ' + String.escapeSingleQuotes(PatientDOB) + ' AND OSM_Patient_DOB__c != null' : '') + ' ORDER BY OrderNumber DESC';
            return Database.query(orderQuery);
        }
        return new List<Order>();
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that searches orderItem records. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public List<OrderItem> searchOrderItems(){
        if((orderNumber != null && orderNumber != '') || (patientFN != null && patientFN != '') || (patientLN != null && patientLN != '') || (patientDOB != '' && patientDOB != null)){
            String orderItemQuery = 'SELECT Id, OrderId, Order.OrderNumber, OSM_Order_Line_Item_ID__c, Order.OSM_Patient__c, Order.OSM_Patient__r.Name, Order.OSM_Product__c, Order.OSM_Patient__r.BirthDate FROM OrderItem' + ' WHERE OrderItemNumber != null' + (orderNumber != null && orderNumber != '' ? ' AND Order.OrderNumber LIKE \'%' + String.escapeSingleQuotes(orderNumber) + '%\'' : '') + (patientFN != null && patientFN != '' ? ' AND Order.OSM_Patient__r.FirstName LIKE \'%' + String.escapeSingleQuotes(PatientFN) + '%\'' : '') + (patientLN != null && patientLN != '' ? ' AND Order.OSM_Patient__r.LastName LIKE \'%' + String.escapeSingleQuotes(PatientLN) + '%\'' : '') + (patientDOB != '' && patientDOB != null ? ' AND Order.OSM_Patient__r.BirthDate = ' + String.escapeSingleQuotes(PatientDOB) + ' AND Order.OSM_Patient__r.BirthDate != null' : '') + ' ORDER BY OSM_Order_Line_Item_ID__c DESC';
            return Database.query(orderItemQuery);
        }
        return new List<OrderItem>();
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that searches patient records. 
     * @history        27.JAN.2015 - Kristian Vegerano - Created  
     */
    public List<Contact> searchPatients(){
      // Name field is used for search in query instead of LastName and FirstName as these fields are not Indexed.
        if ((patientFN != null && patientFN != '') || (patientLN != null && patientLN != '') || (patientDOB != '' && patientDOB != null ) ) {
            String patientQuery = 'SELECT Id, Name, BirthDate FROM Contact WHERE RecordType.DeveloperName = \'OSM_Patient\'' + (patientFN != null && patientFN != '' ? ' AND Name LIKE \'%' + String.escapeSingleQuotes(PatientFN) + '%\'' : '') + (patientLN != null && patientLN != '' ? ' AND Name LIKE \'%' + String.escapeSingleQuotes(patientLN) + '%\'' : '') + (patientDOB != '' && patientDOB != null ? ' AND BirthDate = ' + String.escapeSingleQuotes(PatientDOB) + ' AND BirthDate != null' : '') + ' ORDER BY Name DESC';
            return Database.query(patientQuery);
        }
        return new List<Contact>();
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the next 10 case records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference caseNextPage(){
         try{
            caseList = new List<Case>();
            caseList.addall(searchCases());
            caseCurrentPage += 1;
            caseCurrentRecord += pageSize;
            pagedCaseList.clear();
            for(Integer counter = caseCurrentRecord; counter < (caseList.size()+1 < (caseCurrentRecord + pageSize) ? caseList.size()+1 : (caseCurrentRecord + pageSize)); counter++){
                pagedCaseList.add(caseList[counter-1]); 
            }
            caseHasPrevious = true;
            if(caseCurrentPage == (Integer)Math.ceil((Double)caseList.size()/pageSize)){
                caseHasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the previous 10 case records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference casePreviousPage(){
        try{
            caseList = new List<Case>();
            caseList.addall(searchCases());
            caseCurrentPage -= 1;
            caseCurrentRecord -= pageSize;
            pagedCaseList.clear();
            for(Integer counter = caseCurrentRecord; counter < (caseList.size() < (caseCurrentRecord + pageSize) ? caseList.size() : (caseCurrentRecord + pageSize)); counter++){
                pagedCaseList.add(caseList[counter-1]);
            }
            caseHasNext = true;
            if(caseCurrentPage == 1){
                caseHasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the last 10 case records.
     * @history        09 June,2014 - Kristian Vegerano - Created  
     */
    public PageReference caseLastPage(){
        try{
            caseList = new List<Case>();
            caseList.addall(searchCases());
            caseCurrentPage = (Integer)Math.ceil((Double)caseList.size()/pageSize) - 1;
            caseCurrentRecord = caseCurrentPage * pageSize + 1;
            pagedCaseList.clear();
            for(Integer counter = caseCurrentRecord; counter < (caseList.size()+1 < (caseCurrentRecord + pageSize) ? caseList.size()+1 : (caseCurrentRecord + pageSize)); counter++){
                pagedCaseList.add(caseList[counter-1]);
            }
            caseHasPrevious = true;
            caseCurrentPage = (Integer)Math.ceil((Double)caseList.size()/pageSize);
            if(caseCurrentPage == (Integer)Math.ceil((Double)caseList.size()/pageSize)){
                caseHasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the first 10 case records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference caseFirstPage(){
        try{
            caseList = new List<Case>();
            caseList.addall(searchCases());
            caseCurrentPage = 1;
            caseCurrentRecord = 1;
            pagedCaseList.clear();
            for(Integer counter = 0; counter < pageSize; counter++){
                pagedCaseList.add(caseList[counter]);
            }
            caseHasNext = true;
            if(caseCurrentPage == 1){
                caseHasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }   
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the next 10 order records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference orderNextPage(){
         try{
            orderList = new List<Order>();
            orderList.addAll(searchOrders());
            orderCurrentPage += 1;
            orderCurrentRecord += pageSize;
            pagedOrderList.clear();
            for(Integer counter = orderCurrentRecord; counter < (orderList.size()+1 < (orderCurrentRecord + pageSize) ? orderList.size()+1 : (orderCurrentRecord + pageSize)); counter++){
                pagedOrderList.add(orderList[counter-1]); 
            }
            orderHasPrevious = true;
            if(orderCurrentPage == (Integer)Math.ceil((Double)orderList.size()/pageSize)){//orderCurrentRecord is not the value
                orderHasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the previous 10 order records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference orderPreviousPage(){
        try{
            orderList = new List<Order>();
            orderList.addAll(searchOrders());
            orderCurrentPage -= 1;
            orderCurrentRecord -= pageSize;
            pagedOrderList.clear();
            for(Integer counter = orderCurrentRecord; counter < (orderList.size() < (orderCurrentRecord + pageSize) ? orderList.size() : (orderCurrentRecord + pageSize)); counter++){
                pagedOrderList.add(orderList[counter-1]);
            }
            orderHasNext = true;
            if(orderCurrentPage == 1){
                orderHasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the last 10 order records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference orderLastPage(){
        try{
            orderList = new List<Order>();
            orderList.addAll(searchOrders());
            orderCurrentPage = (Integer)Math.ceil((Double)orderList.size()/pageSize) - 1;
            orderCurrentRecord = orderCurrentPage * pageSize + 1;
            pagedOrderList.clear();
            for(Integer counter = orderCurrentRecord; counter < (orderList.size()+1 < (orderCurrentRecord + pageSize) ? orderList.size()+1 : (orderCurrentRecord + pageSize)); counter++){
                pagedOrderList.add(orderList[counter-1]);
            }
            orderHasPrevious = true;
            orderCurrentPage = (Integer)Math.ceil((Double)orderList.size()/pageSize);
            if(orderCurrentPage == (Integer)Math.ceil((Double)orderList.size()/pageSize)){
                orderHasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the first 10 order records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference orderFirstPage(){
        try{
            orderList = new List<Order>();
            orderList.addAll(searchOrders());
            orderCurrentPage = 1;
            orderCurrentRecord = 1;
            pagedOrderList.clear();
            for(Integer counter = 0; counter < pageSize; counter++){
                pagedOrderList.add(orderList[counter]);
            }
            orderHasNext = true;
            if(orderCurrentPage == 1){
                orderHasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the first 10 orderitem records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference orderItemNextPage(){
         try{
            orderItemList = new List<OrderItem>();
            orderItemList.addAll(searchOrderItems());
            orderItemCurrentPage += 1;
            orderItemCurrentRecord += pageSize;
            pagedOrderItemList.clear();
            for(Integer counter = orderItemCurrentRecord; counter < (orderItemList.size()+1 < (orderItemCurrentRecord + pageSize) ? orderItemList.size()+1 : (orderItemCurrentRecord + pageSize)); counter++){
                pagedOrderItemList.add(orderItemList[counter-1]); 
            }
            orderItemHasPrevious = true;
            if(orderItemCurrentPage == (Integer)Math.ceil((Double)orderItemList.size()/pageSize)){//orderItemCurrentRecord is not the value
                orderItemHasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the first 10 orderitem records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference orderItemPreviousPage(){
        try{
            orderItemList = new List<OrderItem>();
            orderItemList.addAll(searchOrderItems());
            orderItemCurrentPage -= 1;
            orderItemCurrentRecord -= pageSize;
            pagedOrderItemList.clear();
            for(Integer counter = orderItemCurrentRecord; counter < (orderItemList.size() < (orderItemCurrentRecord + pageSize) ? orderItemList.size() : (orderItemCurrentRecord + pageSize)); counter++){
                pagedOrderItemList.add(orderItemList[counter-1]);
            }
            orderItemHasNext = true;
            if(orderItemCurrentPage == 1){
                orderItemHasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the first 10 orderitem records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference orderItemLastPage(){
        try{
            orderItemList = new List<OrderItem>();
            orderItemList.addAll(searchOrderItems());
            orderItemCurrentPage = (Integer)Math.ceil((Double)orderItemList.size()/pageSize) - 1;
            orderItemCurrentRecord = orderItemCurrentPage * pageSize + 1;
            pagedOrderItemList.clear();
            for(Integer counter = orderItemCurrentRecord; counter < (orderItemList.size()+1 < (orderItemCurrentRecord + pageSize) ? orderItemList.size()+1 : (orderItemCurrentRecord + pageSize)); counter++){
                pagedOrderItemList.add(orderItemList[counter-1]);
            }
            orderItemHasPrevious = true;
            orderItemCurrentPage = (Integer)Math.ceil((Double)orderItemList.size()/pageSize);
            if(orderItemCurrentPage == (Integer)Math.ceil((Double)orderItemList.size()/pageSize)){
                orderItemHasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the first 10 orderitem records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference orderItemFirstPage(){
        try{
            orderItemList = new List<OrderItem>();
            orderItemList.addAll(searchOrderItems());
            orderItemCurrentPage = 1;
            orderItemCurrentRecord = 1;
            pagedOrderItemList.clear();
            for(Integer counter = 0; counter < pageSize; counter++){
                pagedOrderItemList.add(orderItemList[counter]);
            }
            orderItemHasNext = true;
            if(orderItemCurrentPage == 1){
                orderItemHasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the next 10 patient records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference patientNextPage(){
         try{
            patientList = new List<Contact>();
            patientList.addAll(searchPatients());
            patientCurrentPage += 1;
            patientCurrentRecord += pageSize;
            pagedPatientList.clear();
            for(Integer counter = patientCurrentRecord; counter < (patientList.size()+1 < (patientCurrentRecord + pageSize) ? patientList.size()+1 : (patientCurrentRecord + pageSize)); counter++){
                pagedPatientList.add(patientList[counter-1]); 
            }
            patientHasPrevious = true;
            if(patientCurrentPage == (Integer)Math.ceil((Double)patientList.size()/pageSize)){//patientCurrentRecord is not the value
                patientHasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the previous 10 patient records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference patientPreviousPage(){
        try{
            patientList = new List<Contact>();
            patientList.addAll(searchPatients());
            patientCurrentPage -= 1;
            patientCurrentRecord -= pageSize;
            pagedPatientList.clear();
            for(Integer counter = patientCurrentRecord; counter < (patientList.size() < (patientCurrentRecord + pageSize) ? patientList.size() : (patientCurrentRecord + pageSize)); counter++){
                pagedPatientList.add(patientList[counter-1]);
            }
            patientHasNext = true;
            if(patientCurrentPage == 1){
                patientHasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the last 10 patient records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference patientLastPage(){
        try{
            patientList = new List<Contact>();
            patientList.addAll(searchPatients());
            patientCurrentPage = (Integer)Math.ceil((Double)patientList.size()/pageSize) - 1;
            patientCurrentRecord = patientCurrentPage * pageSize + 1;
            pagedPatientList.clear();
            for(Integer counter = patientCurrentRecord; counter < (patientList.size()+1 < (patientCurrentRecord + pageSize) ? patientList.size()+1 : (patientCurrentRecord + pageSize)); counter++){
                pagedPatientList.add(patientList[counter-1]);
            }
            patientHasPrevious = true;
            patientCurrentPage = (Integer)Math.ceil((Double)patientList.size()/pageSize);
            if(patientCurrentPage == (Integer)Math.ceil((Double)patientList.size()/pageSize)){
                patientHasNext = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that shows the first 10 patient records.
     * @history        27.JAN.2015 - Kristian Vegerano - Created   
     */
    public PageReference patientFirstPage(){
        try{
            patientList = new List<Contact>();
            patientList.addAll(searchPatients());
            patientCurrentPage = 1;
            patientCurrentRecord = 1;
            pagedPatientList.clear();
            for(Integer counter = 0; counter < pageSize; counter++){
                pagedPatientList.add(patientList[counter]);
            }
            patientHasNext = true;
            if(patientCurrentPage == 1){
                patientHasPrevious = false;
            }
            return null;
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
            return null;
        }
    }
}