@isTest
private class OSM_SearchPageController_Test {

    private static testMethod void testSearchController() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id SRType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Specimen Retrieval').getRecordTypeId();
        //Id SRType = '012K00000000ih0';
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Order_Role_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;
        
        OSM_SearchPageController spc = new OSM_SearchPageController();
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        Set<Id> idList;
        Id pb2Standard = Test.getStandardPricebookId();
        
        

        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;
        

        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;
        idList = new Set<Id>();
        for(Contact con : conList){
            idList.add(con.Id);
        }
        conList = (List<Contact>) getAllFields('Contact', idList, null);
        

        for(Integer i = 0; i < 2; i++){
            prodList.add(OSM_DataFactory.createProduct('IBC', true));
        }
        insert prodList;
        
        
        for(Integer i = 0; i < 2; i++){
            pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, pb2Standard, 3));
        }
        insert pbeList;
        

        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
                Pricebook2Id = pb2Standard, 
                EffectiveDate = System.Today(), 
                Status = 'New'
            ));        
        }
        insert ordList;
        idList = new Set<Id>();
        for(Order ord : ordList){
            idList.add(ord.Id);
        }
        ordList = (List<Order>) getAllFields('Order', idList, null);
        System.debug('@**************************************** order list: ' + ordList);
        
        for(Integer i = 0; i < 2; i++){
            oliList.add(new OrderItem(
                OrderId = ordList[i].Id , 
                PricebookEntryId = pbeList[i].Id , 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        insert oliList;
        System.debug('@**************************************** oli list: ' + oliList);
        
        OSM_Package__c testPack = new OSM_Package__c(
            OSM_Notes__c = 'sample package',
            Name = '00900023'
        );
        insert testPack;
        System.debug('@**************************************** test package: ' + testPack);
        
        Case testCase = new Case(
            RecordTypeId = SRType, 
            OSM_Tracking_Number_lookup__c = testPack.Id
        );
        insert testCase;
        System.debug('@**************************************** test case: ' + testCase);
        
        idList = new Set<Id>();
        idList.add(testCase.Id);
        List<Case> caseList = (List<Case>) getAllFields('Case', idList, 1);
        System.debug('@**************************************** case list query: ' + caseList);
        
        spc.processSRCaseId = caseList[0].Id;
       // spc.pagePackageId = testPack.Id;
        spc.pageOrder = ordList[0];
        spc.processOrderId = ordList[0].Id;
        spc.caseTrackingNumber = testPack.Name;
        spc.caseNumber = caseList[0].CaseNumber;
        spc.orderNumber = ordList[0].OrderNumber;
        spc.casePageUrl = caseList[0].Id;
        spc.patientFN = conList[0].FirstName;
        
        Test.startTest();
        
        System.debug('@**************************************** order number: ' + spc.orderNumber);
        
        spc.search();
        spc.processSelectedCaseOutCon();
        spc.processSelectedOrderOutCon();
        // spc.searchPackage();
        spc.reset();
        spc.resetTrackingNumber();
        spc.searchCases();
        spc.searchOrders();
        spc.searchOrderItems();
        spc.searchPatients();
        spc.caseNextPage();
        spc.casePreviousPage();
        spc.caseLastPage();
        spc.caseFirstPage();
        spc.orderNextPage();
        spc.orderPreviousPage();
        spc.orderLastPage();
        spc.orderFirstPage();
        spc.orderItemNextPage();
        spc.orderItemPreviousPage();
        spc.orderItemLastPage();
        spc.orderItemFirstPage();
        spc.patientNextPage();
        spc.patientPreviousPage();
        spc.patientLastPage();
        spc.patientFirstPage();
        
        spc.pageSize = 0;
        spc.caseNextPage();
        spc.casePreviousPage();
        spc.caseLastPage();
        spc.caseFirstPage();
        spc.orderNextPage();
        spc.orderPreviousPage();
        spc.orderLastPage();
        spc.orderFirstPage();
        spc.orderItemNextPage();
        spc.orderItemPreviousPage();
        spc.orderItemLastPage();
        spc.orderItemFirstPage();
        spc.patientNextPage();
        spc.patientPreviousPage();
        spc.patientLastPage();
        spc.patientFirstPage();
        
        spc.pageSize = null;
        spc.caseNextPage();
        spc.casePreviousPage();
        spc.caseLastPage();
        spc.caseFirstPage();
        spc.orderNextPage();
        spc.orderPreviousPage();
        spc.orderLastPage();
        spc.orderFirstPage();
        spc.orderItemNextPage();
        spc.orderItemPreviousPage();
        spc.orderItemLastPage();
        spc.orderItemFirstPage();
        spc.patientNextPage();
        spc.patientPreviousPage();
        spc.patientLastPage();
        spc.patientFirstPage();
        
        Test.stopTest();
    }
    
    public static List<SObject> getAllFields(String sObj, Set<Id> idList, Integer recLimit){
        String selects = '';
        
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(sObj).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ 
                Schema.DescribeFieldResult fd = ft.getDescribe(); 
                // if (fd.isCreateable()){ 
                    selectFields.add(fd.getName());
                // }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
        
        String queryBuilder = 'select ' + selects + ' from ' + sObj + (idList != null ? ' where Id in :idList ' : '') + (recLimit != null ? ' limit :recLimit ' : '');
        
        return Database.query(queryBuilder);
    }
}