/*
    @author: Rescian Rey
    @description: Controller for the Send Fax button.
                    Uses the Send Fax web service on first send.
                    Uses the Resend Fax web service when the fax record has failed sending (Status=Failure).
    @createdDate: APR 20, 2015
    @history:
        APR 21 2015     Rescian Rey     Added constructor and sendFax method
        MAY 7 2015      Rescian Rey     Added resend functionality
        MAY 21 2015     Rescian Rey     Tracking of last sent attachments
        SEP 10 2015     Rescian Rey     Added fail safe on file type classification
*/
public with sharing class OSM_SendFaxController {
    private ApexPages.StandardController controller;
    private Fax__c faxRecord;
    private List<Attachment> attachments;
    private Boolean useResendWS = false;
    private static final String FAX_ATTACHMENT_DELIMITER = ', ';
    private static final Map<String, String> FILE_TYPE_MAPPING = new Map<String, String>{
            'text' => 'txt',
            'word' => 'doc',
            'pdf' => 'pdf',
            'excel' => 'xls',
            'spreadsheet' => 'xls',
            'powerpoint' => 'ppt',
            'presentation' => 'ppt',
            'jpeg' => 'jpg',
            'png' => 'png'
        };

    /*
        @author: Rescian Rey
        @description: Initialization.
                      - Queries the Fax Record.
                      - Determines if to resend or not.
        @createdDate: APR 21, 2015
        @history:
            MAY 7 2015     Rescian Rey     Added Resend call for Status=Failure
            MAY 14 2015    Rulleth Decena  Added new query for Fax Attachments Custom Object
    */
    public OSM_SendFaxController(ApexPages.StandardController cx){
        controller = cx;

        // Get the fax record.
        try{
            faxRecord = [SELECT Id,
                            Account__c,
                            Case__c,
                            Case__r.CaseNumber, 
                            Order__c,
                            Order__r.OrderNumber,
                            Fax_Number__c,
                            Status__c,
                            OSM_Fax_Attachments_Tracking__c,
                            Resent__c,
                            (SELECT Id, OSM_Attachment_Id__c, OSM_File_Name__c FROM Fax_Attachments__r ORDER BY CreatedDate)
                        FROM Fax__c WHERE Id = :cx.getRecord().Id];
        }catch(QueryException ex){
            System.debug(ex.getMessage());
            return;
        }

        // Check if the fax attachments have been changed since last sent
        List<String> faxAttachmentIDs = new List<String>();
        for(OSM_Fax_Attachment__c faxAttachment: faxRecord.Fax_Attachments__r){
            faxAttachmentIDs.add(faxAttachment.Id);
        }
        String faxAttachmentString = String.join(faxAttachmentIDs, FAX_ATTACHMENT_DELIMITER);

        // Use the Resend web service only when last sending was a failure AND the files to be sent have not changed
        useResendWS = (faxRecord.Status__c == 'Failure' && 
            faxRecord.OSM_Fax_Attachments_Tracking__c != null && 
            faxRecord.OSM_Fax_Attachments_Tracking__c == faxAttachmentString);

        // Fetch the attachments if not using the Resend WS
        if(!useResendWS){
            Set<Id> attachmentIds = new Set<Id>();
            for(OSM_Fax_Attachment__c faxAttachment: faxRecord.Fax_Attachments__r){
                attachmentIds.add(faxAttachment.OSM_Attachment_Id__c);
            }
            attachments = [SELECT Body, Name, ContentType FROM Attachment WHERE Id IN :attachmentIds];
        }
    }

    /*
        @author: Rescian Rey
        @description: Calls the Easylink Fax Web Services. If useResendWS=true, the ResendFax web service
                        will be used, the SendFax otherwise.
        @createdDate: APR 21, 2015
        @history:
    */
    public PageReference sendFax(){
        try{
            if(!useResendWS){
                callSendFaxWS();
            }else{
                callResendFaxWS();
            }
        }catch(Exception ex){
            return null;
        }
        return controller.view();
    }


    /*
        @author: Rescian Rey
        @description: The ResendFax fax web service call.
        @history:
            MAY 7 2015     Rescian Rey   Created method.
    */
    private void callResendFaxWS(){
        // Prepare web service call
        OSM_EasyLink_API_Services.SendFaxManagerService service = new OSM_EasyLink_API_Services.SendFaxManagerService();
        
        // Set the 15-character Fax record ID as the Usercode parameter
        String userCode = ((String)faxRecord.Id).substring(0, 15);

        // Actual callout
        OSM_EasyLink_API_Wrappers.ResendFaxResponse_element response = new OSM_EasyLink_API_Wrappers.ResendFaxResponse_element();
        try{
            response = service.ResendFax(userCode, faxRecord.Fax_Number__c);
        }catch(CalloutException ex){
            faxRecord.Status_Description__c = ex.getMessage();
            update faxRecord;

            System.debug(ex.getMessage());
            return;
        }

        // Populate Status based on response.
        if(response != null){
            faxRecord.Status_Description__c = '';
            if(response.Status == OSM_EasyLink_API_Services.QUERY_FAX_SUCCESS_MSG){
                faxRecord.Status__c = 'Success';
            }else if(response.Status == OSM_EasyLink_API_Services.QUERY_FAX_FAILURE_MSG){
                faxRecord.Status__c = 'Failure';
                faxRecord.Status_Description__c = response.FailureReason;
            }else{
                faxRecord.Status__c = 'Pending';
            }
            faxRecord.Resent__c = true;
            update faxRecord;
        }

        faxRecord = [SELECT Id, Case__c, LastModifiedBy.Name FROM Fax__c WHERE Id = :faxRecord.Id];

        Task faxTask = [SELECT Id, Description FROM Task WHERE Subject = 'Send Fax' AND WhatId = :faxRecord.Case__c 
                                    AND OSM_Fax__c = :faxRecord.Id LIMIT 1];

        faxTask.Description += '\n';
        faxTask.Description += 'Resent By: ' + faxRecord.LastModifiedBy.Name;
        faxTask.Description += '\n';
        update faxTask;

        return;
    }

   
    /*****************************************************************************************************************
        @author         : Niraj Patel
        @description    : Determines Easylink file type according to the Attachment Content Type or Name.
        @parameter      : contentType - Attachment content type
                          filename - Attachment filename
        @return         : file type - A string that conforms to Easylink file type options
        @history:
            <date>                <author>                <description>
            01-JUL-2015           Niraj Patel             Created method.
            01-JUL-2015           Rescian Rey             Put options in a Map, so that it will be easier to add/remove/edit
                                                          file type options in the future.
            10-SEP-2015           Rescian Rey             Added fail-safe. If type is unknown just return it.
    *****************************************************************************************************************/
    private String getFileType(String contentType, String filename){
        String type = '';
        for(String typeName: FILE_TYPE_MAPPING.keySet()){
            if((contentType != null && contentType.contains(typeName)) ||
                    filename.contains(typeName)){
                type = FILE_TYPE_MAPPING.get(typeName);
                break;
            }
        }

        // If it's a bizarred file type, well... just return the content type whatever it is.
        if(contentType != null && type == ''){
            type = contentType;
        }

        return type;
    }


    /*
        @author: Rescian Rey
        @description: The SendFax web service call.
        @history:
            MAY 7 2015     Rescian Rey    Created method.
            May 12 2015     Paul Angelo     Added fields to be updated in Task. OSM_Fax__c and Description
    */
    private void callSendFaxWS(){
        // Get the attachments that will be sent
        OSM_EasyLink_API_Wrappers.FaxDataFiles_element dataFiles = new OSM_EasyLink_API_Wrappers.FaxDataFiles_element();
        dataFiles.Data = new List<OSM_EasyLink_API_Wrappers.Data_element>();
        for(Attachment att: attachments){
            OSM_EasyLink_API_Wrappers.Data_element dataElem = new OSM_EasyLink_API_Wrappers.Data_element();
            dataElem.FileContent = EncodingUtil.base64Encode(att.Body);
            dataElem.FileType = getFileType(att.ContentType, att.Name);
            dataFiles.Data.add(dataElem);
        }

        // Prepare web service call
        OSM_EasyLink_API_Services.SendFaxManagerService service = new OSM_EasyLink_API_Services.SendFaxManagerService();
        
        OSM_EasyLink_API_Wrappers.FaxDataFiles_element data = new OSM_EasyLink_API_Wrappers.FaxDataFiles_element();
        String transactionID;

        // Set the 15-character Fax record ID as the Usercode parameter
        String userCode = ((String)faxRecord.Id).substring(0, 15);

        // Set the Case number as the account code, and order ID as the custBuf
        String accountCode = faxRecord.Case__r.CaseNumber==null?'':faxRecord.Case__r.CaseNumber;
        String custBuf = faxRecord.Order__r.OrderNumber==null?'':faxRecord.Order__r.OrderNumber;

        // Actual callout
        try{
            transactionID = service.SendFax(faxRecord.Fax_Number__c, accountCode, userCode, dataFiles, UserInfo.getUserName(), '', 'NONE', 'Fine', '', '', custBuf);
        }catch(CalloutException ex){
            faxRecord.Status_Description__c = ex.getMessage() + 'Try sending again later.';
            update faxRecord;

            System.debug(ex.getMessage());
            return;
        }

        // Set the transaction ID on the Fax record
        faxRecord.Easylink_Fax_ID__c = transactionID;
        faxRecord.Status__c = 'Pending';
        faxRecord.Status_Description__c = '';

        // Check if has already sent before (Check if the OSM_Fax_Attachments_Tracking__c is already populated)
        Boolean alreadySentBefore = faxRecord.OSM_Fax_Attachments_Tracking__c != null;
        //faxRecord.Resent__c = alreadySentBefore;

        // Keep track of the fax attachments sent
        faxRecord.OSM_Fax_Attachments_Tracking__c = '';

        List<String> faxAttachmentIDs = new List<String>();
        for(OSM_Fax_Attachment__c att: faxRecord.Fax_Attachments__r){
            faxAttachmentIDs.add(att.Id);
        }
        faxRecord.OSM_Fax_Attachments_Tracking__c = String.join(faxAttachmentIDs, FAX_ATTACHMENT_DELIMITER);
        update faxRecord;
        
        //Query updated faxRecord to get the LastModifiedBy.Name and CreatedBy.Name
        faxRecord = [SELECT Id, Case__c, Account__c, Fax_Number__c, Resent__c, LastModifiedBy.Name, CreatedBy.Name FROM Fax__c WHERE Id = :faxRecord.Id];

        // Get previous fax task if present
        Task faxTask;
        try{
            faxTask = [SELECT Id, Description FROM Task WHERE Subject = 'Send Fax' AND WhatId = :faxRecord.Case__c 
                            AND OSM_Fax__c = :faxRecord.Id LIMIT 1];
        }catch(QueryException exp){
            // Insert fax task
            faxTask = new Task();
            faxTask.Subject = 'Send Fax';
            faxTask.Status = 'Completed';
            faxTask.Type = 'Fax';
            faxTask.WhatId = faxRecord.Case__c;
            faxTask.OSM_Fax__c = faxRecord.Id;
            faxTask.OSM_Account__c = faxRecord.Account__c;
            faxTask.Description = '\n';
        }

        faxTask.Description += 'Fax Number: ' + faxRecord.Fax_Number__c + '\n';
        faxTask.Description += 'File Name: ';
        
        //Loop in list of attachments and parse the name in the Description.
        List<String> filenames = new List<String>();
        for(Attachment a : attachments){
            filenames.add(a.Name);
        }

        faxTask.Description += String.join(filenames, ', ');
        faxTask.Description += (alreadySentBefore?'(modified)':'') + '\n';

        if(faxRecord.Resent__c != null && faxRecord.Resent__c){
            faxTask.Description += 'Resent By: ' + faxRecord.LastModifiedBy.Name + '\n';
        }else{
            faxTask.Description += 'Sent By: ' + faxRecord.CreatedBy.Name + '\n';
        }
        upsert faxTask;
    }
}