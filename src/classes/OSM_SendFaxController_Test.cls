/*
    @author: Rescian Rey
    @description: Test class for the OSM_SendFaxController
    @createdDate: APR 21, 2015
    @history:
*/

@isTest
private class OSM_SendFaxController_Test {

    // simple text file containing the text Hello World
    private static final String TEST_ATTACHMENT_FILENAME = 'Test File';
    private static final String TEST_ATTACHMENT_CONTENT = 'Hello World';
    private static final String TEST_FAX_SUBJECT = 'Test Fax';
    private static final String FAX_NUMBER = '18622351052';
    private static final String TEST_OVERRIDE_REASON = 'Test override reason';

    /*
        @author: Mark Cagurong
        @description: initialization of test data
        @createdDate: July 22, 2015
        @history:
    */
    @testsetup static void Setup_Data(){

        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');
        fax.Fax_Subject__c = TEST_FAX_SUBJECT;
        insert fax;

        Attachment att = OSM_DataFactory.createAttachment(fax.Id);
        att.Name = TEST_ATTACHMENT_FILENAME;
        att.Body = Blob.toPdf(TEST_ATTACHMENT_CONTENT);
        att.ContentType = 'pdf';
        insert att;

        OSM_Fax_Attachment__c oattch = new OSM_Fax_Attachment__c();
        oattch.OSM_Attachment_Id__c = att.Id;
        oattch.OSM_Fax__c = fax.Id;
        oattch.OSM_File_Name__c = TEST_ATTACHMENT_FILENAME;

        insert oattch;
    }

    /*
        @author: Rescian Rey
        @description: sendFax() should populate the Easylink_Fax_ID__c and
                    should have a task related to it with subject='Send Fax',
                    status=completed and type=fax.
        @createdDate: APR 21, 2015
        @history:
    */
    private static testmethod void sendFax_FaxIDPopulatedAndTaskCreated(){
        // Create test fax record
        Fax__c fax = OSM_DataFactory.createFax();
        fax.Easylink_Fax_ID__c = null;
        insert fax;

        // Insert attachment
        Attachment att = OSM_DataFactory.createAttachment(fax.Id);
        insert att;

        Test.startTest();

        ApexPages.StandardController std = new ApexPages.StandardController(fax);
        OSM_SendFaxController controller = new OSM_SendFaxController(std);

        // send fax
        controller.sendFax();

        fax = [SELECT Id, Easylink_Fax_ID__c FROM Fax__c WHERE Id = :fax.Id];
        // Easylink_Fax_ID__c should not be null by now
        System.assert((fax.Easylink_Fax_ID__c != null));

        // There should be a task with status=completed, type=fax and subject='Send Fax' related to the fax
        List<Task> completedTask = [SELECT Id FROM Task WHERE Subject='Send Fax' AND
            Type='Fax' AND Status='Completed'];

        System.assert(!completedTask.isEmpty());
        Test.stopTest();
    }

    /*
        @author: Mark Cagurong
        @description: unit text for SendFax with fax record containing attachments
        @createdDate: July 22, 2015
        @history:
    */
    private static testmethod void sendFax_WithAttachment(){
        
        Fax__c fax = [SELECT Id FROM Fax__c where Fax_Number__c = :FAX_NUMBER LIMIT 1];        

        Test.startTest();

        ApexPages.StandardController std = new ApexPages.StandardController(fax);
        OSM_SendFaxController controller = new OSM_SendFaxController(std);

        // send fax
        controller.sendFax();

        Test.stopTest();

        fax = [SELECT Id, Easylink_Fax_ID__c, Status__c, Status_Description__c, Resent__c, OSM_Fax_Attachments_Tracking__c   FROM Fax__c where Fax_Number__c = :FAX_NUMBER LIMIT 1];
        Task faxTask = [SELECT Id, Status, Type, Description FROM Task WHERE Type = 'Fax' AND  OSM_Fax__c = :fax.iD LIMIT 1];
        
        System.assert((fax.Easylink_Fax_ID__c != null));
        system.assert(faxTask != null);
    }

     /*
        @author: Mark Cagurong
        @description: sendFax with resent flag forced for code coverage
        @createdDate: July 22, 2015
        @history:
    */
    private static testmethod void sendFax_WithAttachmentResent(){
        
        Fax__c fax = [SELECT Id FROM Fax__c where Fax_Number__c = :FAX_NUMBER LIMIT 1];

        // force resent logic flag for code coverage
        fax.Resent__c = true;
        update fax;

        Test.startTest();

        ApexPages.StandardController std = new ApexPages.StandardController(fax);
        OSM_SendFaxController controller = new OSM_SendFaxController(std);

        // send fax
        controller.sendFax();

        Test.stopTest();

        fax = [SELECT Id, Easylink_Fax_ID__c, Status__c, Status_Description__c, Resent__c, OSM_Fax_Attachments_Tracking__c   FROM Fax__c where Fax_Number__c = :FAX_NUMBER LIMIT 1];
        Task faxTask = [SELECT Id, Status, Type, Description FROM Task WHERE Type = 'Fax' AND  OSM_Fax__c = :fax.iD LIMIT 1];

        System.assert((fax.Easylink_Fax_ID__c != null));
        system.assert(faxTask != null);
    }


    /*
        @author: Mark Cagurong
        @description: Common test routines for the resend alternative flow
        @createdDate: July 22, 2015
        @history:
    */
    private static Fax__c callResendFaxWS_ResendCommonTests(){
        Fax__c fax = [SELECT Id, Status__c, OSM_Fax_Attachments_Tracking__c  FROM Fax__c where Fax_Number__c = :FAX_NUMBER LIMIT 1];
        OSM_Fax_Attachment__c oattch = [SELECT Id FROM OSM_Fax_Attachment__c where OSM_Fax__c = :fax.Id LIMIT 1];

        // update the flag to check if this is resent mode
        fax.Status__c = 'Failure';
        fax.OSM_Fax_Attachments_Tracking__c = oattch.Id;

        update fax;

        // simulate task created from a previous flow
        Task faxTask = new Task();
        faxTask.OSM_Fax__c = fax.Id;
        faxTask.Subject = 'Send Fax';
        faxTask.Status = 'Completed';
        faxTask.Type = 'Fax';
        faxTask.OSM_Follow_Up_Date_Time__c = Date.Today()+2;

        insert faxTask;        

        Test.startTest();

        ApexPages.StandardController std = new ApexPages.StandardController(fax);
        OSM_SendFaxController controller = new OSM_SendFaxController(std);

        // send fax
        controller.sendFax();

        Test.stopTest();

        fax = [SELECT Id, Easylink_Fax_ID__c, Status__c, Status_Description__c, Resent__c, OSM_Fax_Attachments_Tracking__c   FROM Fax__c where Fax_Number__c = :FAX_NUMBER LIMIT 1];
        faxTask = [SELECT Id, Status, Type, Description FROM Task WHERE Type = 'Fax' AND  OSM_Fax__c = :fax.iD LIMIT 1];        
        
        System.assertEquals(true, fax.Resent__c);
        System.assert(faxTask.Description.contains('Resent'));

        return fax; // return to caller for additinoal assertions
    }

    /*
        @author: Mark Cagurong
        @description: resend flow with successful status forced
        @createdDate: July 22, 2015
        @history:
    */
    private static testmethod void callResendFaxWS_ResendFlowSuccess(){
        
        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = OSM_EasyLink_API_Services.QUERY_FAX_SUCCESS_MSG;

        Fax__c fax = callResendFaxWS_ResendCommonTests();
        
        System.assertEquals('Success', fax.Status__c);
    }


    /*
        @author: Mark Cagurong
        @description: resend flow with faliure status forced
        @createdDate: July 22, 2015
        @history:
    */
    private static testmethod void callResendFaxWS_ResendFlowFailure(){

        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = OSM_EasyLink_API_Services.QUERY_FAX_FAILURE_MSG;
        OSM_EasyLink_API_Services.FAX_FAILURE_REASON_OVERRIDE = TEST_OVERRIDE_REASON;

        Fax__c fax = callResendFaxWS_ResendCommonTests();
        
        System.assertEquals('Failure', fax.Status__c);
        System.assertEquals(TEST_OVERRIDE_REASON, fax.Status_Description__c );
    }

    /*
        @author: Mark Cagurong
        @description: resend flow with pending status forced
        @createdDate: July 22, 2015
        @history:
    */
    private static testmethod void callResendFaxWS_ResendFlowPending(){

        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = 'Test Unknown Status';
        OSM_EasyLink_API_Services.FAX_FAILURE_REASON_OVERRIDE = '';

        Fax__c fax = callResendFaxWS_ResendCommonTests();

        System.assertEquals('Pending', fax.Status__c);
    }
   
}