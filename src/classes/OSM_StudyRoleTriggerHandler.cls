/*
 *Author: Michiel Patricia M. Robrigado
 *Date created: November 11, 2014
 *TriggerHandler for the OSM_Study_Role__c
 */

public class OSM_StudyRoleTriggerHandler{
    
    //Method for the after insert in the trigger
    public static void afterInsert(OSM_Study_Role__c[] newStudyRole){
        checkStudyRoleValue(newStudyRole);
    }
    public static void afterUpdate(OSM_Study_Role__c[] newStudyRole){
        checkStudyRoleValue(newStudyRole);
    }

    //Method for checking the value of the picklist
    public static void checkStudyRoleValue(OSM_Study_Role__c[] newStudyRole){
        Set<Id> studSiteId = new Set<Id>();
        Set<Id> deleteStudRoleId = new Set<Id>();
        //adds the Id of the Study Site
        for(OSM_Study_Role__c std : newStudyRole){
            studSiteId.add(std.OSM_Study_Site__c);
        }
 
        Set<Id> studyRoleId = new  Set<Id>();
        List<OSM_Study_Role__c> deleteStudyRole = new List<OSM_Study_Role__c>();
        List<OSM_Study_Role__c> updateStudyRole = new List<OSM_Study_Role__c>();
        for(OSM_Study_Role__c std : [SELECT Id, OSM_Study_Role__c, OSM_Study_Site__c FROM OSM_Study_Role__c WHERE OSM_Study_Site__c in : studSiteId AND OSM_Study_Role__c = 'Principal Investigator' Order By LastModifiedDate DESC]){
            //checks if it already checked for Study Site
            if(!studyRoleId.contains(std.OSM_Study_Site__c)) {
                studyRoleId.add(std.OSM_Study_Site__c);
            }
            else{
                //deleteStudyRole.add(std);
                deleteStudRoleId.add(std.Id);
            } 
        }
        //refactored to eliminate unecessary query - Paul Wittmeyer 7/3/2015
        List<OSM_Study_Role__c> deleteStudRoleIdList = new List<OSM_Study_Role__c>();
        for(Id srId : deleteStudRoleId){
        	OSM_Study_Role__c sr = new OSM_Study_Role__c(Id = srId);
        	deleteStudRoleIdList.add(sr);
        }
        //for(OSM_Study_Role__c std : [SELECT Id, OSM_Study_Role__c, OSM_Study_Site__c FROM OSM_Study_Role__c WHERE Id in : deleteStudRoleId AND OSM_Study_Role__c = 'Principal Investigator' Order By LastModifiedDate DESC]){
        for(OSM_Study_Role__c std : deleteStudRoleIdList){
            std.OSM_Study_Role__c  = 'Sub-Investigator';
            updateStudyRole.add(std);
        }

        if(updateStudyRole.size() > 0){
            update updateStudyRole;
        }

        /*if(deleteStudyRole.size() > 0){
          //delete deleteStudyRole;
          for(OSM_Study_Role__c std : deleteStudyRole){
              std.OSM_OSM_Study_Role__c  = 'Sub-Investigator';
          }
        }*/
    }
}