/*
      @author: Patrick Lorilla and David E. Catindoy
      @date: 10 DEC 2014 - Created
      @description: Study Role Trigger Handler
      @history: 13 JAN 2015 - Updated (David E. Catindoy)

    */
@isTest
public class OSM_StudyRoleTriggerHandler_Test{
    
    static testmethod void testcheckStudyRoleValueOnInsert(){
        //prepare custom settings 
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Study_Role_Trigger__c = true;
        triggerSwitch.Study_Trigger__c = false;
        //triggerSwitch.Validation_Builder__c = false;
        //triggerSwitch.Customer_Affiliation_Trigger__c= false;
        insert triggerSwitch;
        
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds ;
       
        //prepare hcps
        List<Contact> testHCPs = new List<Contact>();
        
        for(Integer a=0; a<20; a++){
            testHCPs.add(OSM_DataFactory.createContactWithMailingAddress(a, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', recordTypeIds.Contact_HCP_Record_Type__c));
        }
        insert testHCPs;
        
         //prepare hcos
        List<Account> testHCOs = new List<Account>();
        for(Integer a=0; a<20; a++){
            testHCOs.add(OSM_DataFactory.createAccountWithBillingAddress(a, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recordTypeIds.Account_HCO_Record_Type__c));
        }
        insert testHCOs;
        
        List<OSM_Study__c> testStudy = new List<OSM_Study__c>();
        for(Integer a=0; a<20; a++){
            testStudy.add(OSM_DataFactory.createStudy('Stud'+a,'Sponsor',testHCOs[a].Id));
            testStudy[a].OSM_Sponsor_HCO__c = testHCOs[a].Id;
            testStudy[a].OSM_Sponsor_HCOX__c = testHCPs[a].Id;
        }
        insert testStudy;
        List<OSM_Study_Site__c> studSiteList = new List<OSM_Study_Site__c>();
        for(Integer a=0; a<20; a++){
            studSiteList.add(OSM_DataFactory.createStudySite(testHCOs[a].ID, testStudy[a]));
            studSiteList[a].OSM_Study__c = testStudy[a].Id;
        }
        insert studSiteList;
        
        List<OSM_Study_Role__c> studRoleList = new List<OSM_Study_Role__c>();
        Test.startTest();
            for(Integer a=0; a<20; a++){
                studRoleList.add(OSM_DataFactory.createStudyRole(testHCPs[a].ID));
                studRoleList[a].OSM_Study_Site__c = studSiteList[a].ID;
                studRoleList[a].OSM_Study_Role__c = 'Principal Investigator';
            }
            insert studRoleList;
        Test.stopTest();
    }
    
    static testmethod void testcheckStudyRoleValueOnDelete(){
        //prepare custom settings 
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Study_Role_Trigger__c = true;
        triggerSwitch.Study_Trigger__c = false;
        //triggerSwitch.Validation_Builder__c = false;
        //triggerSwitch.Customer_Affiliation_Trigger__c= false;
        insert triggerSwitch;
        
        Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds ;
       
        //prepare hcps
        List<Contact> testHCPs = new List<Contact>();
        for(Integer a=0; a<20; a++){
            testHCPs.add(OSM_DataFactory.createContactWithMailingAddress(a, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', recordTypeIds.Contact_HCP_Record_Type__c));
        }
        insert testHCPs;
         //prepare hcos
        List<Account> testHCOs = new List<Account>();
        for(Integer a=0; a<20; a++){
            testHCOs.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recordTypeIds.Account_HCO_Record_Type__c));
        }
        insert testHCOs;
        List<OSM_Study__c> testStudy = new List<OSM_Study__c>();
        for(Integer a=0; a<20; a++){
            // testStudy.add(OSM_DataFactory.createStudy('Stud'+a,'Payor but no balance to Patient',testHCOs[a].Id));
            testStudy.add(OSM_DataFactory.createStudy('Stud'+a,'Sponsor',testHCOs[a].Id));
           //testStudy[a].OSM_Sponsor_HCO__c = testHCOs[a].Id;
        }
        insert testStudy;
        
        List<OSM_Study_Site__c> studSiteList = new List<OSM_Study_Site__c>();
        for(Integer a=0; a<20; a++){
            studSiteList.add(OSM_DataFactory.createStudySite(testHCOs[a].ID, testStudy[a]));
            studSiteList[a].OSM_Study__c = testStudy[a].Id;
        }
        insert studSiteList;
        List<OSM_Study_Role__c> studRoleList = new List<OSM_Study_Role__c>();
        for(Integer a=0; a<20; a++){
            studRoleList.add(OSM_DataFactory.createStudyRole(testHCPs[a].ID));
            studRoleList[a].OSM_Study_Site__c = studSiteList[a].ID;
            studRoleList[a].OSM_Study_Role__c = 'Principal Investigator';
        }
        insert studRoleList;
        Test.startTest();
        for(Integer a=0; a<20; a++){
            studRoleList[a].OSM_Study_Site__c = null;
        }
        update studRoleList;    
        Test.stopTest();
    }

}