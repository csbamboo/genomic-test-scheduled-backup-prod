/*
  @author: Daniel Quismorio
  @date: 11 NOV 2014
  @description: Study Site Handler
  @history: 11 NOV 2014 - Created (DQ)

*/
public class OSM_StudySiteTriggerHandler {
    
    /*
      @author: Daniel Quismorio
      @date: 11 NOV 2014
      @description: On after Insert Method
      @param:  List of new Study Sites
    */
    public static void onAfterInsert(List<OSM_Study_Site__c> newStudySites){
       updateStudySiteParent(newStudySites);
    }
    
    /*
      @author: Daniel Quismorio
      @date: 11 NOV 2014
      @description: On after Update Method
      @param:  List of new Study Sites
    */
    public static void onAfterUpdate(List<OSM_Study_Site__c> newStudySites){
        updateStudySiteParent(newStudySites);
    }
    
    
    /*
      @author: Daniel Quismorio
      @date: 11 NOV 2014
      @description: On after Update Method
      @param:  List of new Study Sites
    */
    public static void updateStudySiteParent(List<OSM_Study_Site__c> newStudySites){
        
        //Get Parent Ids
        Set<Id> studyIds = new Set<Id>();
        Set<Id> studySiteIds = new Set<Id>();
        for(OSM_Study_Site__c studySiteLoop : newStudySites){
            studyIds.add(studySiteLoop.OSM_Study__c);
        }
        
        //This will get the old record with a Study Site Role = 'Parent Study Site' to prevent duplidate Parent Study Site in the Study Record
        Set<Id> studySitesWithDupParent = new  Set<Id>();
        //Set<Id> studySitesIds = new  Set<Id>();
        //List<OSM_Study_Site__c> deleteStudySites = new List<OSM_Study_Site__c>();
        //List<Study_Role__c> deleteStudyRoles = new List<Study_Role__c>();
        List<OSM_Study_Site__c> updateStudySites = new List<OSM_Study_Site__c>();
        for(OSM_Study_Site__c studySiteLoopGetRelevantRecords : [Select Id, OSM_Study__c From OSM_Study_Site__c Where OSM_Study__c IN: studyIds AND OSM_Study_Site_Role__c = 'Parent Study Site' Order By LastModifiedDate DESC]){
            if(!studySitesWithDupParent.contains(studySiteLoopGetRelevantRecords.OSM_Study__c)) {
                studySitesWithDupParent.add(studySiteLoopGetRelevantRecords.OSM_Study__c);
            } else {
                //deleteStudySites.add(studySiteLoopGetRelevantRecords);
                //studySitesIds.add(studySiteLoopGetRelevantRecords.Id);
                studySiteIds.add(studySiteLoopGetRelevantRecords.Id);
            }
        }
        
        for(OSM_Study_Site__c std : [Select Id, OSM_Study__c From OSM_Study_Site__c Where Id IN: studySiteIds AND OSM_Study_Site_Role__c = 'Parent Study Site' Order By LastModifiedDate DESC]){
            std.OSM_Study_Site_Role__c  = 'Study Site';
            updateStudySites.add(std);
        }

        if(updateStudySites.size() > 0){
            update updateStudySites;
        }
        
        //This will get the Study Role which Parent record(Study Site) will be deleted
        //deleteStudyRoles = [Select Id From Study_Role__c Where OSM_OSM_Study_Site__c IN: studySitesIds];
        
        //This will delete the Study Site and Study Role records
        /*if(deleteStudySites.size() > 0){
            delete deleteStudySites;
            delete deleteStudyRoles;
        }*/
    }
}