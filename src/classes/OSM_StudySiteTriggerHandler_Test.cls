/*
      @author: Patrick Lorilla and David E. Catindoy
      @date: 16 DEC 2014 - Created
      @description: Study Site Trigger Handler
      @history: 13 JAN 2015 - Updated (David E. Catindoy)

*/
@isTest
public class OSM_StudySiteTriggerHandler_Test{

    static testmethod void updateStudySiteParent(){
        //prepare custom settings 
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Study_Site_Trigger__c = true;
        //triggerSwitch.Validation_Builder__c = false;
        //triggerSwitch.Customer_Affiliation_Trigger__c= false;
        insert triggerSwitch;
        
         Record_Type__c recordTypeIds = OSM_DataFactory.recordLists();
        insert recordTypeIds ;
        
        //prepare hcos
        List<Account> testHCOs = new List<Account>();
        for(Integer a=0; a<20; a++){
            testHCOs.add(OSM_DataFactory.createAccountWithBillingAddress(a, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recordTypeIds.Account_HCO_Record_Type__c));
        }
        insert testHCOs;
        //prepare hcps
        List<Contact> testHCPs = new List<Contact>();
        for(Integer a=0; a<20; a++){
            testHCPs.add(OSM_DataFactory.createContactWithMailingAddress(a, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', recordTypeIds.Contact_HCP_Record_Type__c));
        }
        
        insert testHCPs;
        // List<OSM_Study__c> studyList = new List<OSM_Study__c>();
        //  for(Integer a=0; a<20; a++){
            OSM_Study__c study1 = OSM_DataFactory.createStudy('a', null);
        //} 
        insert study1;
        
        List<OSM_Study_Role__c> studRoleList = new List<OSM_Study_Role__c>();
        
        for(Integer a=0; a<20; a++){
            studRoleList.add(OSM_DataFactory.createStudyRole(testHCPs[a].ID));
            //studRoleList[a].OSM_OSM_Study_Site__c = studSiteList[a].ID;
            studRoleList[a].OSM_Study_Role__c = 'Principal Investigator';
        } 
        
        insert studRoleList;
        Test.startTest();
        //insert study sites
        List<OSM_Study_Site__c> studSiteList = new List<OSM_Study_Site__c>();
        for(Integer a=0; a<20; a++){
            studSiteList.add(OSM_DataFactory.createStudySite(testHCOs[a].ID, study1));
            studSiteList[a].OSM_Study_Site_Role__c = 'Parent Study Site';

        }
        
        insert studSiteList;
        for(OSM_Study_Site__c studySite : studSiteList){
            studySite.OSM_Study_Site_Role__c = 'Study Site';
        }
        upsert studSiteList;
        Test.stopTest();
        //assert study site role values
        for(OSM_Study_Site__c sc: [Select Id, OSM_Study_Site_Role__c from OSM_Study_Site__c]){
            System.assertEquals(sc.OSM_Study_Site_Role__c , 'Study Site');
        }
    }

}