/********************************************************************
    @author         : Rescian Rey
    @description    : Controller for the Submit Order button for Kit Order. Previously named
                      OSM_SubmitTherapakOrderExt, but is no longer appropriate because this
                      button will now also send JKDataCorp Orders.

                      Function:
                      1. Therapak Ordering
                         - sends Kit Order Line Items (KOLI's) which vendor is Therapak.
                      2. JKDataCorp Ordering
                         - sends KOLI's which vendor is JKDataCorp.
    @history:
        <date>                <author>                <description>
        MAY 25 2015           Rescian Rey             Created class.
********************************************************************/
public with sharing class OSM_SubmitKitOrderExt {
    private ApexPages.StandardController controller;
    private OSM_Kit_Order__c order;
    private User submitter;

    private final static String VENDOR_THERAPAK = 'Therapak';
    private final static String VENDOR_JKDC = 'JKDataCorp';

    private final static String REGION_EMEA = 'EMEA';
    private final static Integer JKDC_ORDER_LIMIT = 3; // Only 3 items allowed to be sent for JKDC to prevent callout exception

    private final static String RTYPE_REQUISITION = 'Requisition Forms';

    /********************************************************************
        @author         : Rescian Rey
        @description    : Initialization. Initialize the controller, order and submitter.
        @history:
            <date>                <author>                <description>
            MAY 25 2015           Rescian Rey              Created class.
    ********************************************************************/
	public OSM_SubmitKitOrderExt(ApexPages.StandardController std) {
		controller = std;
        try{
            order = [SELECT Id, Name,
                            OSM_Description__c,
                            OSM_Main_Address_Line_1__c,
                            OSM_Main_Address_Line_2__c,
                            OSM_Main_City__c,
                            OSM_Main_Country__c,
                            OSM_Main_Country_Code__c,
                            OSM_Main_Phone__c,
                            OSM_Main_State__c,
                            OSM_Main_Zip__c,
                            OSM_Main_Fax__c,
                            OSM_Rush_Order__c,
                            OSM_Shipping_Method__c,
                            OSM_Ship_To_Address_Line_1__c,
                            OSM_Ship_To_Address_Line_2__c,
                            OSM_Ship_To_Address_Contact__c,
                            OSM_Account__r.Name,
                            OSM_Account_Number__c,
                            OSM_Ship_To_City__c,
                            OSM_Ship_To_State__c,
                            OSM_Ship_To_Zip__c,
                            OSM_Ship_To_Country__c,
                            OSM_Ship_To_Country_Code__c,
                            OSM_Ship_To_Phone__c,
                            OSM_Ship_To_Fax__c,
                            OwnerId,
                            (SELECT OSM_Product__r.Name,
                                        OSM_Account_Information__c,
                                        OSM_Quantity__c,
                                        OSM_Region__c,
                                        OSM_Product__r.OSM_Vendor__r.Name,
                                        RecordTypeID
                                FROM Kit_Order_Line_Items__r
                                WHERE OSM_Status__c != 'Success')
                        FROM OSM_Kit_Order__c
                        WHERE Id = :controller.getId()];

            submitter = [SELECT Name, Email, Fax, Phone FROM User WHERE Id = :order.OwnerId];
        }catch(QueryException ex){
            System.debug(ex.getMessage());
            return;
        }catch(NullPointerException nexp){
            System.debug(nexp.getMessage());
            return;
        }
        // Bypass edit validations
        OSM_KitOrderTriggerHandler.bypassEditValidation = true;
        OSM_KitOrderLineItemTriggerHandler.bypassEditValidation = true;
	}

    /********************************************************************
        @author         : Rescian Rey
        @description    : Submits the order onto their corresponding sites,
                            whether Therapak or JKDataCorp
        @history:
            <date>                <author>                <description>
            MAY 25 2015           Rescian Rey             Created method.
    ********************************************************************/
    public PageReference submitOrder(){
        // Separate Therapak KOLI's from JKData KOLI's
        List<OSM_Kit_Order_Line_Item__c> therapakKOLI = new List<OSM_Kit_Order_Line_Item__c>();
        List<OSM_Kit_Order_Line_Item__c> jkdcKOLI = new List<OSM_Kit_Order_Line_Item__c>();
        for(OSM_Kit_Order_Line_Item__c koli: order.Kit_Order_Line_Items__r){
            if(koli.OSM_Product__r.OSM_Vendor__r.Name == VENDOR_THERAPAK){
                therapakKOLI.add(koli);
            }else if(koli.OSM_Product__r.OSM_Vendor__r.Name == VENDOR_JKDC){
                jkdcKOLI.add(koli);
            }
        }

        // Separate Therapak KOLI's further. EMEA KOLI should be submitted separately from other regions.
        List<OSM_Kit_Order_Line_Item__c> emeaKOLI = new List<OSM_Kit_Order_Line_Item__c>();
        List<OSM_Kit_Order_Line_Item__c> usKOLI = new List<OSM_Kit_Order_Line_Item__c>();
        for(OSM_Kit_Order_Line_Item__c koli: therapakKOLI){
            if(koli.OSM_Region__c == REGION_EMEA){
                emeaKOLI.add(koli);
            }else{
                usKOLI.add(koli);
            }
        }

        List<OSM_Kit_Order_Line_Item__c> kolisToUpdate = new List<OSM_Kit_Order_Line_Item__c>();

        // US Region KOLI submission
        if(!usKOLI.isEmpty()){
            OSM_Therapak_Ordering_Settings__c settings = OSM_Therapak_Ordering_Settings__c.getInstance('United States');
            OSM_TherapakOrder therapakOrder = new OSM_TherapakOrder(order, submitter, settings);

            // Add KOLIs to order
            for(OSM_Kit_Order_Line_Item__c koli: usKOLI){
                List<String> variables = koli.OSM_Product__r.Name.split('\\|');
                String qty = koli.OSM_Quantity__c==null?'0':String.valueOf(koli.OSM_Quantity__c);

                for(String variable: variables){
                    therapakOrder.addItem(variable, qty);
                }

                // If requisition form, add account info
                String rtype = Schema.SObjectType.OSM_Kit_Order_Line_Item__c.getRecordTypeInfosById().get(koli.RecordTypeID).getName();
                if(rtype == RTYPE_REQUISITION){
                    therapakOrder.addRequisitionLocation(koli.OSM_Account_Information__c);
                }
            }

            Boolean success = therapakOrder.submitOrder();
            // Update Status to Success or Failure
            for(OSM_Kit_Order_Line_Item__c koli: usKOLI){
                koli.OSM_Status__c = success?'Success':'Failure';
                kolisToUpdate.add(koli);
            }
        }

        // EMEA Region KOLI submission
        if(!emeaKOLI.isEmpty()){
            OSM_Therapak_Ordering_Settings__c settings = OSM_Therapak_Ordering_Settings__c.getInstance('EMEA');
            OSM_TherapakOrder therapakOrder = new OSM_TherapakOrder(order, submitter, settings);

            // Add KOLIs to order
            for(OSM_Kit_Order_Line_Item__c koli: emeaKOLI){
                List<String> variables = koli.OSM_Product__r.Name.split('\\|');
                String qty = koli.OSM_Quantity__c==null?'0':String.valueOf(koli.OSM_Quantity__c);

                for(String variable: variables){
                    therapakOrder.addItem(variable, qty);
                }

                // If requisition form, add account info
                String rtype = Schema.SObjectType.OSM_Kit_Order_Line_Item__c.getRecordTypeInfosById().get(koli.RecordTypeID).getName();
                if(rtype == RTYPE_REQUISITION){
                    therapakOrder.addRequisitionLocation(koli.OSM_Account_Information__c);
                }
            }

            Boolean success = therapakOrder.submitOrder();
            // Update Status to Success or Failure
            for(OSM_Kit_Order_Line_Item__c koli: emeaKOLI){
                koli.OSM_Status__c = success?'Success':'Failure';
                kolisToUpdate.add(koli);
            }
        }

        if(!jkdcKOLI.isEmpty()){
            OSM_JKDC_Ordering_Settings__c settings = OSM_JKDC_Ordering_Settings__c.getInstance('Main');

            Integer loopCounter = 0;
            for(OSM_Kit_Order_Line_Item__c koli: jkdcKOLI){

                if(loopCounter == JKDC_ORDER_LIMIT){
                    break;
                }

                OSM_JKDCOrder jkdcOrder = new OSM_JKDCOrder(order, submitter, settings, koli);
                Boolean success = jkdcOrder.submitOrder();

                koli.OSM_Status__c = success?'Success':'Failure';
                kolisToUpdate.add(koli);

                loopCounter++;
            }
        }

        if(!kolisToUpdate.isEmpty()){
            update kolisToUpdate;
        }

        order.OSM_Submission_Date__c = Datetime.now();
        update order;

        return controller.view();
    }
}