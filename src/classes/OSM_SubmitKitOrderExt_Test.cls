/*
  @author: Amanpreet Singh Sidhu 
  @date: 31 June 2015
  @description: Test Class for OSM_SubmitKitOrderExt apex class
  @history: 31 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest(seeAllData = true)
//@isTest
public class OSM_SubmitKitOrderExt_Test 
{
  
    static testMethod void testExt() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults(); 
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Kit_Order__c = true;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Role_Trigger__c = false;
        update triggerSwitch;
               
        Id usrId = UserInfo.getUserId();
        Id recTypeProKits = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Kits').getRecordTypeId();
        Id recTypeKOLI = Schema.SObjectType.OSM_Kit_Order_Line_Item__c.getRecordTypeInfosByName().get('Requisition Forms').getRecordTypeId();
        
        List<Account> lstAcc = new List<Account>();
        List<Product2> lstProd = new List<Product2>();
        List<OSM_Kit_Order_Line_Item__c> lstKOLI = new List<OSM_Kit_Order_Line_Item__c>();
        
        Account accVENDOR_THERAPAK = OSM_DataFactory.createAccount('Therapak');
        Account accVENDOR_JKDC = OSM_DataFactory.createAccount('JKDataCorp');
        
        lstacc.add(accVENDOR_THERAPAK);
        lstacc.add(accVENDOR_JKDC);
        
        insert lstAcc;
    
        OSM_Kit_Order__c kitOrder = new OSM_Kit_Order__c(OSM_Account__c = accVENDOR_THERAPAK.Id);
        insert kitOrder;
        
        Product2 prodVENDOR_THERAPAKEmea = OSM_DataFactory.createProduct('IBC', true);
        prodVENDOR_THERAPAKEmea.RecordTypeId = recTypeProKits;
        prodVENDOR_THERAPAKEmea.OSM_Vendor__c = accVENDOR_THERAPAK.Id;
        prodVENDOR_THERAPAKEmea.OSM_Region__c = 'EMEA';
        prodVENDOR_THERAPAKEmea.OSM_Preprint__c = true;
        prodVENDOR_THERAPAKEmea.OSM_Kits__c = 'Block - English';
        prodVENDOR_THERAPAKEmea.OSM_Requisition_Forms__c = 'English';
        prodVENDOR_THERAPAKEmea.OSM_Shipping_Documents__c = 'FedEx Air Waybills (4 pack)';
        
        
        Product2 prodVENDOR_THERAPAKUsa = OSM_DataFactory.createProduct('IBC', true);
        prodVENDOR_THERAPAKUsa.RecordTypeId = recTypeProKits;
        prodVENDOR_THERAPAKUsa.OSM_Vendor__c = accVENDOR_THERAPAK.Id;
        prodVENDOR_THERAPAKUsa.OSM_Region__c = 'United States';
        prodVENDOR_THERAPAKUsa.OSM_Preprint__c = true;
        prodVENDOR_THERAPAKUsa.OSM_Kits__c = 'Block (4 pack)';
        prodVENDOR_THERAPAKUsa.OSM_Requisition_Forms__c = 'Prostate (25 forms)';
        prodVENDOR_THERAPAKUsa.OSM_Shipping_Documents__c = 'FedEx International Air Waybills (4 pack)';
        
        Product2 prodVENDOR_JKDC = OSM_DataFactory.createProduct('IBC', true);
        prodVENDOR_JKDC.RecordTypeId = recTypeProKits;
        prodVENDOR_JKDC.OSM_Vendor__c = accVENDOR_JKDC.Id;
        prodVENDOR_JKDC.OSM_Region__c = 'EMEA';
        prodVENDOR_JKDC.OSM_Preprint__c = true;
        prodVENDOR_JKDC.OSM_Kits__c = 'Block - English';
        prodVENDOR_JKDC.OSM_Requisition_Forms__c = 'English';
        prodVENDOR_JKDC.OSM_Shipping_Documents__c = 'FedEx Air Waybills (4 pack)';
        
        lstProd.add(prodVENDOR_THERAPAKEmea);
        lstProd.add(prodVENDOR_THERAPAKUsa);
        lstProd.add(prodVENDOR_JKDC);
        
        insert lstProd;
        
        /*Id pricebookStandard = Test.getStandardPricebookId();
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
        insert pbe;*/
        
        OSM_Kit_Order_Line_Item__c kitOLI1 = new OSM_Kit_Order_Line_Item__c(OSM_Kit_Order__c = kitOrder.Id, OSM_Region__c = 'EMEA',
                                                          OSM_Status__c = 'Failure', OSM_Product__c = prodVENDOR_THERAPAKEmea.Id,
                                                          OSM_Preprint__c = true,OSM_Kits__c = 'Block - English',RecordTypeId = recTypeKOLI,
                                                          OSM_Requisition_Forms__c = 'English',OSM_Shipping_Documents__c = 'FedEx Air Waybills (4 pack)'); 
        
        OSM_Kit_Order_Line_Item__c kitOLI2 = new OSM_Kit_Order_Line_Item__c(OSM_Kit_Order__c = kitOrder.Id, OSM_Region__c = 'United States',
                                                          OSM_Status__c = 'Failure', OSM_Product__c = prodVENDOR_THERAPAKUsa.Id,
                                                          OSM_Preprint__c = true,OSM_Kits__c = 'Block (4 pack)',RecordTypeId = recTypeKOLI,
                                                          OSM_Requisition_Forms__c = 'Prostate (25 forms)',OSM_Shipping_Documents__c = 'FedEx International Air Waybills (4 pack)');
      
        OSM_Kit_Order_Line_Item__c kitOLI3 = new OSM_Kit_Order_Line_Item__c(OSM_Kit_Order__c = kitOrder.Id, OSM_Region__c = 'EMEA',
                                                          OSM_Status__c = 'Failure', OSM_Product__c = prodVENDOR_JKDC.Id,
                                                          OSM_Preprint__c = true,OSM_Kits__c = 'Block - English',RecordTypeId = recTypeKOLI,
                                                          OSM_Requisition_Forms__c = 'English',OSM_Shipping_Documents__c = 'FedEx Air Waybills (4 pack)');
        
        lstKOLI.add(kitOLI1);
        lstKOLI.add(kitOLI2);
        lstKOLI.add(kitOLI3);
        
        insert lstKOLI;
        
                
        Test.startTest();
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(kitOrder);
        OSM_SubmitKitOrderExt cont = new OSM_SubmitKitOrderExt(sc);
        Test.setMock(HttpCalloutMock.class, new OSM_MockHttpResponseGenerator(200,'Complete','{"apex":"test"}')); 
        cont.submitOrder();        
        Test.stopTest(); 
        
    }
    
    static testMethod void testExtForException() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults(); 
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Kit_Order__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Role_Trigger__c = false;
        update triggerSwitch;
        
        Account accVENDOR_THERAPAK = OSM_DataFactory.createAccount('Therapak');
        
        Test.startTest();
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(accVENDOR_THERAPAK);
        OSM_SubmitKitOrderExt cont = new OSM_SubmitKitOrderExt(sc);        
        Test.stopTest();
    }
    
}