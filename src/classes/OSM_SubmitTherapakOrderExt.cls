/*
    @author: Rescian Rey
    @description: Controller for the Submit Therapak Button.
    @history:
        <date>                <author>                <description>
        MAY 12 2015           Rescian Rey             Created class
*/
public with sharing class OSM_SubmitTherapakOrderExt {
    private ApexPages.StandardController controller;
    private OSM_Therapak_Ordering_Settings__c settings;
    private OSM_Kit_Order__c order;
    private User submitter;
    
    public string sessionID;
    
    private static final Map<String, String> REGION_SETTINGS_MAP = new Map<String, String>{
        'United States' => 'United States',
        'EMEA' => 'EMEA',
        'Canada' => 'United States',
        'Asia Pacific and Japan (AP)' => 'United States',
        'Latin America and the Caribbean (LAC)' => 'United States'
    };
    private static final String DEFAULT_SETTING = 'United States';

    /*
        @author: Rescian Rey
        @description: Initialization. Get the Therapak settings, the order to be sent, and
                        the owner's details.
        @history:
            <date>                <author>                <description>
    */
    public OSM_SubmitTherapakOrderExt(ApexPages.StandardController std) {
        controller = std;
        try{
            order = [SELECT Id, Name,
                            OSM_Main_Address_Line_1__c,
                            OSM_Main_Address_Line_2__c,
                            OSM_Main_City__c,
                            OSM_Main_Country__c,
                            OSM_Main_Phone__c,
                            OSM_Main_State__c,
                            OSM_Main_Zip__c,
                            OSM_Main_Fax__c,
                            OSM_Rush_Order__c,
                            OSM_Shipping_Method__c,
                            OSM_Ship_To_Address_Line_1__c,
                            OSM_Ship_To_Address_Line_2__c,
                            OSM_Ship_To_Address_Contact__c,
                            OSM_Account__r.Name,
                            OSM_Account_Number__c,
                            OSM_Ship_To_City__c,
                            OSM_Ship_To_State__c,
                            OSM_Ship_To_Zip__c,
                            OSM_Ship_To_Country__c,
                            OSM_Ship_To_Phone__c,
                            OwnerId,
                            (SELECT OSM_Product__r.Name,
                                        OSM_Quantity__c,
                                        OSM_Region__c
                                FROM Kit_Order_Line_Items__r
                                WHERE OSM_Status__c != 'Success' AND 
                                    OSM_Product__r.OSM_Vendor__r.Name = 'Therapak')
                        FROM OSM_Kit_Order__c
                        WHERE Id = :controller.getId()];
            // There should be at least one order line item, so we could check the region
            String region = DEFAULT_SETTING;
            if(!order.Kit_Order_Line_Items__r.isEmpty()){
                region = order.Kit_Order_Line_Items__r[0].OSM_Region__c;
            }
             
            String settingName = REGION_SETTINGS_MAP.get(region);

            // Get the settings which Name is same with Region
            settings = OSM_Therapak_Ordering_Settings__c.getInstance(settingName);

            if(settings == null){
                settings = OSM_Therapak_Ordering_Settings__c.getInstance(DEFAULT_SETTING);
            }
            
            submitter = [SELECT Name, Email, Fax, Phone FROM User WHERE Id = :order.OwnerId];
        }catch(QueryException ex){
            System.debug(ex.getMessage());
            return;
        }catch(NullPointerException nexp){
            System.debug(nexp.getMessage());
            return;
        }
    }

    /*
        @author: Rescian Rey
        @description: Constructs and submits the request to Therapak.
        @history:
            <date>                <author>                <description>
            MAY 12 2015           Rescian Rey             Created method.
    */
    public PageReference submitOrder(){

        // if no settings, then go back to detail page.
        if(settings == null){
            return controller.view();
        }

        // Form/Post Data to send to Therapak
        String rush = order.OSM_Rush_Order__c?'Yes':'No';
        Map<String, String> parameters = new Map<String, String>{
            'RequestorName'=> submitter.Name,
            'PhoneNo'=> submitter.Phone,
            'FaxNo'=> submitter.Fax,
            'Email'=> submitter.Email,
            'RUSH'=> rush,
            'ShippingMethod'=> order.OSM_Shipping_Method__c,
            'Samples'=> 'Yes',
            'ClientAcctNo' => order.OSM_Account_Number__c,
            'ClientName' => order.OSM_Account__r.Name,
            'ClientAddress1' => order.OSM_Main_Address_Line_1__c,
            'ClientAddress2' => order.OSM_Main_Address_Line_2__c,
            'ClientCity' => order.OSM_Main_City__c,
            'ClientState' => order.OSM_Main_State__c,
            'ClientZip' => order.OSM_Main_Zip__c,
            'ClientPhone' => order.OSM_Main_Phone__c,
            'ClientFax' => order.OSM_Main_Fax__c,
            'ClientCountry' => order.OSM_Main_Country__c,
            'ShipToCompany' => order.OSM_Account__r.Name,
            'ShipToName' => order.OSM_Ship_To_Address_Contact__c,
            'ShipToAddress1' => order.OSM_Ship_To_Address_Line_1__c,
            'ShipToAddress2' => order.OSM_Ship_To_Address_Line_2__c,
            'ShipToCity' => order.OSM_Ship_To_City__c,
            'ShipToState' => order.OSM_Ship_To_State__c,
            'ShipToZip' =>  order.OSM_Ship_To_Zip__c,
            'ShipToPhone' =>  order.OSM_Ship_To_Phone__c,
            'ShipToCountry' =>  order.OSM_Ship_To_Country__c,
            'PO' => order.Name
        };

        // Add order line items to the form data
        for(OSM_Kit_Order_Line_Item__c koli: order.Kit_Order_Line_Items__r){
            List<String> items = koli.OSM_Product__r.Name.split('\\|');
            String qty = koli.OSM_Quantity__c==null?'0':String.valueOf(koli.OSM_Quantity__c);
            
            for(String item: items){
                parameters.put(item, qty);
            }
        }

        // Form body construction
        String boundary = '__8LA4YWxkTrZl0gW__';
        String delimiter = '\n';

        String formData = '';
        for(String k: parameters.keySet()){
            String value = parameters.get(k)==null?'':parameters.get(k);

            formData += '--' + boundary + delimiter;
            formData += 'Content-Disposition: form-data; name="'+ k +'"' + delimiter;
            formData += delimiter;
            formData += value + delimiter;
        }

        // Login to Therapak site to obtain a session ID.
        //String sessionID = therapakLogin();
        sessionID = therapakLogin();
        if(sessionID == ''){ return controller.view(); }

        // Construct the request
        HttpRequest request = new HttpRequest();
        request.setEndpoint(settings.OSM_Therapak_Post_Order__c);
        //request.setEndpoint('http://requestb.in/y6erhay6');
        request.setMethod('POST');
        request.setHeader('Cookie', sessionID);
        request.setHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);


        request.setBody(formData);
        request.setHeader('Content-Length', String.valueof(formData.length()));

        HTTPResponse response;
        Boolean success = false;

        try{
            Http http = new Http();
            response = http.send(request);
            System.debug('responseresponse:::: ' +response.getBody());

            // Parse for a thank you note to ensure ordering is successful
            if(response.getBody().contains('Thank you for your order')){
                success = true;
            }
        }catch(CalloutException ex){
            System.debug(ex.getMessage());
        }
        
        // Update Status to Success or Failure
        for(OSM_Kit_Order_Line_Item__c koli: order.Kit_Order_Line_Items__r){
            koli.OSM_Status__c = success?'Success':'Failure';
        }

        try{
            order.OSM_Submission_Date__c = Datetime.now();
            update order;
            update order.Kit_Order_Line_Items__r;
        }catch(DmlException ex){
            System.debug(ex);
        }
        
        return controller.view();
    }


    /********************************************************************
        @author: Rescian Rey
        @description: Logins to the Therapak site. Returns the session ID.
        @history:
            <date>                <author>                <description>
    ********************************************************************/
    public String therapakLogin(){
        HttpRequest request = new HttpRequest();
        request.setEndpoint(settings.OSM_Therapak_Login__c);
        //request.setEndpoint();
        request.setMethod('POST');

        Map<String, String> parameters = new Map<String, String>{
            'Username'=> settings.OSM_Username__c,
            'Password'=> settings.OSM_Password__c,
            'login'=> '1'
        };

        List<String> parameterList = new List<String>();
        for(String k: parameters.keySet()){
            parameterList.add(k + '=' + parameters.get(k));
        }

        request.setBody(String.join(parameterList, '&'));

        String sessionID = '';
        HTTPResponse response;

        try{
            Http http = new Http();//
            response = http.send(request);
            sessionID = response.getHeader('Set-Cookie').split(';')[0];
        }catch(CalloutException ex){
            System.debug(ex.getMessage());
        }catch(NullPointerException nexp){
            System.debug(nexp.getMessage());
        }

        return sessionID;
    }
}