/*
  @author: Amanpreet Singh Sidhu 
  @date: 31 June 2015
  @description: Test Class for OSM_SubmitTherapakOrderExt apex class
  @history: 31 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest(seeAllData = true)
public class OSM_SubmitTherapakOrderExt_Test 
{
     static testMethod void testExt() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults(); 
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Kit_Order__c = true;
        triggerSwitch.Product2__c = false;
        update triggerSwitch;
               
        Id recTypeProKits = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Kits').getRecordTypeId();
        Id recTypeKOLI = Schema.SObjectType.OSM_Kit_Order_Line_Item__c.getRecordTypeInfosByName().get('Requisition Forms').getRecordTypeId();
        
        List<Account> lstAcc = new List<Account>();
        List<Product2> lstProd = new List<Product2>();
        List<OSM_Kit_Order_Line_Item__c> lstKOLI = new List<OSM_Kit_Order_Line_Item__c>();
        
        Account accVENDOR_THERAPAK = OSM_DataFactory.createAccount('Therapak');
        
        lstacc.add(accVENDOR_THERAPAK);        
        insert lstAcc;
    
        OSM_Kit_Order__c kitOrder = new OSM_Kit_Order__c(OSM_Account__c = accVENDOR_THERAPAK.Id);
        insert kitOrder;
        
        Product2 prodVENDOR_THERAPAKEmea = OSM_DataFactory.createProduct('IBC', true);
        prodVENDOR_THERAPAKEmea.RecordTypeId = recTypeProKits;
        prodVENDOR_THERAPAKEmea.OSM_Vendor__c = accVENDOR_THERAPAK.Id;
        prodVENDOR_THERAPAKEmea.OSM_Region__c = 'EMEA';
        prodVENDOR_THERAPAKEmea.OSM_Preprint__c = true;
        prodVENDOR_THERAPAKEmea.OSM_Kits__c = 'Block - English';
        prodVENDOR_THERAPAKEmea.OSM_Requisition_Forms__c = 'English';
        prodVENDOR_THERAPAKEmea.OSM_Shipping_Documents__c = 'FedEx Air Waybills (4 pack)';
                
        lstProd.add(prodVENDOR_THERAPAKEmea);        
        insert lstProd;
        
        
        OSM_Kit_Order_Line_Item__c kitOLI1 = new OSM_Kit_Order_Line_Item__c(OSM_Kit_Order__c = kitOrder.Id, OSM_Region__c = 'EMEA',
                                                          OSM_Status__c = 'Failure', OSM_Product__c = prodVENDOR_THERAPAKEmea.Id,
                                                          OSM_Preprint__c = true,OSM_Kits__c = 'Block - English',RecordTypeId = recTypeKOLI,
                                                          OSM_Requisition_Forms__c = 'English',OSM_Shipping_Documents__c = 'FedEx Air Waybills (4 pack)'); 
        
        lstKOLI.add(kitOLI1);        
        insert lstKOLI;
        
        
        Map<String,String> headerMap = new Map<String,String>();
        headerMap.put('Cookie', UserInfo.getSessionId());
        
        
        Test.startTest();
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(kitOrder);
        OSM_SubmitTherapakOrderExt cont = new OSM_SubmitTherapakOrderExt(sc); 
        Test.setMock(HttpCalloutMock.class, new OSM_MockHttpResponseGenerator(200,'Complete','{"apex":"test"}'));
        cont.therapakLogin(); 
        cont.submitOrder();
        /*Test.setMock(HttpCalloutMock.class, new OSM_MockHttpResponseGenerator(200,'Complete','{"apex":"test"}'));
        cont.submitOrder();*/
        Test.stopTest(); 
        
    }
    
    static testMethod void testExtForException() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults(); 
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Kit_Order__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Role_Trigger__c = false;
        update triggerSwitch;
        
        Account accVENDOR_THERAPAK = OSM_DataFactory.createAccount('Therapak');
        
        Test.startTest();
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(accVENDOR_THERAPAK);
        OSM_SubmitTherapakOrderExt cont = new OSM_SubmitTherapakOrderExt(sc);        
        Test.stopTest();
    }

}