/**
 * File Info
 * ----------------------------------
 * @filename       OSM_TaskTriggerHandler.cls
 * @created        30.March.2015
 * @author         Paul Angelo
 * @description    Class for Task dml actions. 
 * @history        30.March.2015 - Paul Angelo - Created
 *                 4.June.2015 - Rescian Rey - Added method preventOperationOfFaxTask() on before delete/edit
 */
public class OSM_TaskTriggerHandler{
    
    public static Boolean checkRecordOwnerRunOnce = false;
    public static Boolean updateCaseRunOnce = false;

    private static final String OP_DELETE = 'delete';
    private static final String OP_EDIT = 'edit';

    /**
     * @author         Paul Angelo
     * @description    Method for all actions that will take place before deleting a task.
     * @history        30.March.2015 - Paul Angelo - Created  
     */
    public static void onBeforeDelete(Map<Id,Task> taskMap){
        if(!updateCaseRunOnce){
            updateCaseRunOnce = true;
            checkRecordOwner(taskMap);
        }

        preventOperationOfFaxTask(taskMap.values(), OP_DELETE);
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : On Before Updates
        @history:
            <date>                <author>                <description>
            JUN 4 2015            Rescian Rey             created method
    ********************************************************************/
    public static void onBeforeUpdate(List<Task> tasks){
        preventOperationOfFaxTask(tasks, OP_EDIT);
    }
    
    /**
     * @author         Paul Angelo
     * @description    Method for all actions that will take place after insert a task.
     * @history        13.April.2015 - Paul Angelo - Created  
     */
    public static void onAfterInsert(Map<Id,Task> taskMap){
        if(!checkRecordOwnerRunOnce){
            checkRecordOwnerRunOnce = true;
            updateCase(taskMap);
        }
    }
    
    
    /**
     * @author         Paul Angelo
     * @description    Method that check if the currently login user is a System Admin, Customer Service Manager or the Owner of Task.
     * @history        30.March.2015 - Paul Angelo - Created  
     */
    
    public static void checkRecordOwner(Map<Id,Task> taskMap){
    
        String adminProfileId = [SELECT Id FROM Profile WHERE Name=:'System Administrator'].Id;
        String customerServiceManagerRoleId = [SELECT Id FROM UserRole WHERE Name=:'Customer Service Manager'].Id;
            try{
                for(Task t: taskMap.values()){
    
                    //'00eU0000000j1sQ'
                    
                    System.Debug('00eU0000000j1sQ ************************adminProfileId: ' +adminProfileId);
                    System.Debug('00eU0000000j1sQ ************************UserInfo.getProfileId(): ' +UserInfo.getProfileId());
    
                    
                    System.Debug('00eU0000000j1sQ ************************t.OwnerId: ' +t.OwnerId);
                    System.Debug('00eU0000000j1sQ ************************UserInfo.getUserId(): ' +UserInfo.getUserId());
                    
    
                    
                    // if(t.Status == 'Completed'){
      
                        if(t.OwnerId == UserInfo.getUserId()){
                            break;
                        }
                        
                        else if(t.LastModifiedById == UserInfo.getUserId()){
                            break;
                        }
                        
                        else if(UserInfo.getProfileId() == adminProfileId){
                            break;
                        }
                        
                        else if(UserInfo.getUserRoleId() == customerServiceManagerRoleId){
                            break;
                        }
                        
                        //else if(t.OwnerId != UserInfo.getUserId() && t.LastModifiedById != UserInfo.getUserId() && UserInfo.getProfileId() != adminProfileId && UserInfo.getUserRoleId() != customerServiceManagerRoleId){
                        //    t.addError('You do not have permission to edit this completed task because you are not the owner!');
                        //}
                        
                        else{
                            t.addError('You do not have permission to edit this completed task because you are not the owner!');
                        }
                        
                    // }
                    
                    
                    
                }
            }catch(Exception e){
                System.debug('The following exception has occurred: ' + e.getMessage());
                return;
            }
        
    }
    
    
    /**
     * @author         Paul Angelo
     * @description    Method that update Case when task is inserted.
     * @history        13.April.2015 - Paul Angelo - Created  
     */
    public static void updateCase(Map<Id,Task> taskMap){
        
    
        Set<Id> caseIds = new Set<Id>();
        Task tsk = new Task();
        List<Case> caseList = new List<Case>();
        Map<Id,Task> caseIdTaskMap = new Map<Id,Task>();
        String rtName = '';
        System.Debug('*************************** caseList: '+caseList);
        try{
            Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Task.getRecordTypeInfosById();
            for(Task t: taskMap.values()){
                if(t.Type != 'Fax'){
                    // if(t.OSM_Follow_Up_Date_Time__c != null){
                        caseIds.add(t.WhatId);
                        caseIdTaskMap.put(t.WhatId, t);
                    // }
                }
            }
            
            
            for(Case c : [SELECT Id, OSM_Follow_Up_Date_Time__c, Priority, RecordTypeId FROM Case WHERE Id IN : caseIds]){
                Id caseTaskRecTypeId = caseIdTaskMap.get(c.Id).RecordTypeId;
                rtName = rtMap.get(caseTaskRecTypeId).getName();

                if(rtName == 'Task - Domestic'){
                    if(caseIdTaskMap.get(c.Id).OSM_Follow_Up_Date_Time__c != null && (caseIdTaskMap.get(c.Id).Type != 'Other' && caseIdTaskMap.get(c.Id).Type != 'Email' && caseIdTaskMap.get(c.Id).Type != null)){
                        c.OSM_Follow_Up_Date_Time__c = caseIdTaskMap.get(c.Id).OSM_Follow_Up_Date_Time__c;
                    }
                    if(caseIdTaskMap.get(c.Id).Priority != null && (caseIdTaskMap.get(c.Id).Type != 'Other' && caseIdTaskMap.get(c.Id).Type != 'Email' && caseIdTaskMap.get(c.Id).Type != null)){
                        c.Priority = caseIdTaskMap.get(c.Id).Priority;
                    }
                }
    
                //remove the condition and replace the condition for the Task - Domestic record type
                //if(caseIdTaskMap.get(c.Id).Type == 'Phone' || caseIdTaskMap.get(c.Id).Type == 'Note'){
                //    c.OSM_Follow_Up_Date_Time__c = caseIdTaskMap.get(c.Id).OSM_Follow_Up_Date_Time__c;
                //}
                //c.Priority = caseIdTaskMap.get(c.Id).Priority;
                caseList.add(c);
            }
            
            System.Debug('*************************** caseList: '+caseList);
            update caseList;
        }catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            return;
        }
    }
    

    /********************************************************************
        @author         : Rescian Rey
        @description    : Prevent deletion of Task (Type=Fax)
        @history:
            <date>                <author>                <description>
            JUN 4 2015            Rescian Rey              Created method
    ********************************************************************/
    public static void preventOperationOfFaxTask(List<Task> tasks, String op){
        Id sysAdID = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        if(UserInfo.getProfileId() == sysAdID){
            return;
        }

        for(Task t: tasks){
            if(t.Type == 'Fax'){
                t.addError('You do not have permission to ' + op + ' this Fax Task.');
            }
        }         
    }

}