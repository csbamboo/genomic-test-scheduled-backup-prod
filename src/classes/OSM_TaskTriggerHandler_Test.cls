/**
 * File Info
 * ----------------------------------
 * @filename       OSM_TaskTriggerHandler_Test
 * @created        15.April.2015
 * @author         Paul Angelo
 * @description    Task Trigger Handler Test Class 
 * @history        15.April.2015 - Paul Angelo - Created  
 *                 21.July.2015 - Mark Cagurong - Added additional unit tests
 */
 @isTest
public class OSM_TaskTriggerHandler_Test{
   
    public static Id adminProfileId = [Select Id From Profile WHERE Name=:'System Administrator'].Id;
    public static Id genericRT = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;
    public static Id taskDomesticRT = Schema.SObjectType.Task.RecordTypeInfosByName.get('Task - Domestic').RecordTypeId;
    private final static Integer qCount = 10;
    private final static String TEST_FAX_SUBJECT = 'Subject123';
    
    public static testMethod void updateCase(){
        
        //Custom Setting - Trigger Switch
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Task_Trigger__c = false;
        insert triggerSwitch;
        
        List<Task> taskList = new List<Task>();
        Case cse = OSM_DataFactory.createCase('Potential Duplicate','Open');
        cse.RecordTypeId = genericRT;
        
        insert cse;
        
        for(Integer i = 0; i<3; i++){
            Task tsk = new Task();
            tsk.RecordTypeId = taskDomesticRT;
            tsk.Priority = '1 - Low';
            tsk.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
            tsk.WhatId = cse.Id;
            tsk.Type = 'Phone';
            tsk.OSM_Direction__c = 'Inbound';
            taskList.add(tsk);
        }
        
        Test.startTest();
        
            insert taskList;
            
        Test.stopTest();
        
    }
    
    
    //3
    public static testMethod void updateCaseOwnedByOthers2(){
    
        //Custom Setting - Trigger Switch
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Task_Trigger__c = false;
        insert triggerSwitch;
        
        List<Task> taskList = new List<Task>();

        
        User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        insert testUsr;
        
        Case cse = OSM_DataFactory.createCase('Potential Duplicate','Open');
        cse.RecordTypeId = genericRT;
        
        insert cse;
        
        
        Task tsk = new Task();
        tsk.RecordTypeId = taskDomesticRT;
        tsk.Priority = '1 - Low';
        tsk.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
        tsk.WhatId = cse.Id;
        tsk.Type = 'Phone';
        tsk.OSM_Direction__c = 'Inbound';
        
         Task tsk1 = new Task();
        tsk1.RecordTypeId = taskDomesticRT;
        tsk1.Priority = '1 - Low';
        tsk1.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
        tsk1.WhatId = cse.Id;
        tsk1.Type = 'Phone';
        tsk1.OSM_Direction__c = 'Inbound';
        
        System.runAs(testUsr){
            insert tsk;
            insert tsk1;
        }
           
        Test.startTest();
        
        delete tsk;
            
        Test.stopTest();
        
    }
    
    //2
    public static testMethod void updateCaseOwnedByOthers(){
        List<Task> taskList = new List<Task>();

        
        User testUsr = OSM_DataFactory.createSalesRep('TestUser');
        insert testUsr;
        
        Case cse = OSM_DataFactory.createCase('Potential Duplicate','Open');
        cse.RecordTypeId = genericRT;
        
        insert cse;
        
        
        Task tsk = new Task();
        tsk.RecordTypeId = taskDomesticRT;
        tsk.Priority = '1 - Low';
        tsk.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
        tsk.WhatId = cse.Id;
        tsk.Type = 'Phone';
        tsk.OSM_Direction__c = 'Inbound';
        tsk.OwnerId = testUsr.Id;
        
        insert tsk;
        
        
        Test.startTest();
        
        delete tsk;
            
        Test.stopTest();
        
        Case cse2 = new Case();
        
        cse2 = [SELECT Id, OSM_Follow_Up_Date_Time__c, Priority FROM Case WHERE Id = :cse.Id LIMIT 1];
        
    }
    
    
    public static testMethod void deleteCompletedTask(){
        List<Task> taskList = new List<Task>();
        Case cse = OSM_DataFactory.createCase('Potential Duplicate','Completed');
        cse.RecordTypeId = genericRT;
        
        insert cse;
        
        for(Integer i = 0; i<3; i++){
            Task tsk = new Task();
            tsk.RecordTypeId = taskDomesticRT;
            tsk.Priority = '1 - Low';
            tsk.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
            tsk.WhatId = cse.Id;
            tsk.Type = 'Phone';
            tsk.OSM_Direction__c = 'Inbound';
            taskList.add(tsk);
        }
        
        Test.startTest();
        
            insert taskList;
            delete taskList.get(0);
            
        Test.stopTest();
        
        Case cse2 = new Case();
        
        cse2 = [SELECT Id, OSM_Follow_Up_Date_Time__c, Priority FROM Case WHERE Id = :cse.Id LIMIT 1];
        
    }

    public static testMethod void updateCaseOwnedByCustomerService(){
        try
        {        
            List<Task> taskList = new List<Task>();

            
            User testCSUsr = OSM_DataFactory.createCustomerServiceUser('CSUser');
            insert testCSUsr;

            User testUsr = OSM_DataFactory.createSalesRep('TestUser');
            insert testUsr;
            
            Case cse = OSM_DataFactory.createCase('Potential Duplicate','Open');
            cse.RecordTypeId = genericRT;
            
            insert cse;
            
            
            Task tsk = new Task();
                tsk.RecordTypeId = taskDomesticRT;
                tsk.Priority = '1 - Low';
                tsk.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
                tsk.WhatId = cse.Id;
                tsk.Type = 'Fax';
                tsk.OSM_Direction__c = 'Inbound';
                tsk.OwnerId = testUsr.Id;
            
            insert tsk;
            
            Test.startTest();
            
            System.runAs(testCSUsr){
                delete tsk;
            }
                
            Test.stopTest();
            
            Case cse2 = new Case();
            
            cse2 = [SELECT Id, OSM_Follow_Up_Date_Time__c, Priority FROM Case WHERE Id = :cse.Id LIMIT 1];
        }
        catch(Exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission') ? true : false;
            //System.AssertEquals(expectedExceptionThrown, false);
        }
        
    }

    public static testMethod void deleteCompletedTaskOfCS(){
        List<Task> taskList = new List<Task>();
        User testCSUsr;
        User testUsr;
        Case cse;
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
                testCSUsr = OSM_DataFactory.createCustomerServiceUser('CSUser');
                insert testCSUsr;
    
                testUsr = OSM_DataFactory.createSalesRep('TestUser');
                insert testUsr;
                
                cse = OSM_DataFactory.createCase('Potential Duplicate','Completed');
                cse.RecordTypeId = genericRT;
                
                insert cse;
                
                for(Integer i = 0; i<3; i++){
                    Task tsk = new Task();
                    tsk.RecordTypeId = taskDomesticRT;
                    tsk.Priority = '1 - Low';
                    tsk.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
                    tsk.WhatId = cse.Id;
                    tsk.Type = 'Phone';
                    tsk.OSM_Direction__c = 'Inbound';
                    tsk.OwnerId = testCSUsr.Id;
                    taskList.add(tsk);
                }
            }
            
            Test.startTest();

                System.runAs(testUsr){
                    insert taskList;
                    delete taskList.get(0);
                }  
                
            Test.stopTest();
            
            Case cse2 = new Case();
            
            cse2 = [SELECT Id, OSM_Follow_Up_Date_Time__c, Priority FROM Case WHERE Id = :cse.Id LIMIT 1];
         
        
    }

    /*
        @author: Mark Cagurong
        @description: test case validation for non admin user preventing editing of task with type = fax
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    public static testMethod void preventOperationOfFaxTask_NonAdminUser(){
    
        //Custom Setting - Trigger Switch
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Task_Trigger__c = false;
        insert triggerSwitch;

        User testUsr1 = OSM_DataFactory.createSalesRep('Rep123');
        User testUsr2 = OSM_DataFactory.createSalesRep('Rep456');

        insert testUsr1;        
        insert testUsr2;

        Case cse = OSM_DataFactory.createCase('Potential Duplicate','Completed');
        cse.RecordTypeId = genericRT;

        insert cse;

        Task tsk = new Task();
        tsk.RecordTypeId = taskDomesticRT;
        tsk.Priority = '1 - Low';
        tsk.Type = 'Fax';
        tsk.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
        tsk.Subject = TEST_FAX_SUBJECT;
        tsk.OwnerId = testUsr1.Id;
        tsk.WhatId = cse.Id;                    
        tsk.OSM_Direction__c = 'Inbound';

        insert tsk;       

        Boolean expectedExceptionThrown = false;

        System.runAs(testUsr2){            

            Test.startTest();

            try{
                tsk.Subject = 'zzz';

                update tsk;

                system.debug('preventOperationOfFaxTask_NonAdminUser - ...');
            }
            catch(DmlException dmx){
                system.debug('preventOperationOfFaxTask_NonAdminUser.dmx.getDmlMessage(0): ' + dmx.getDmlMessage(0));
                expectedExceptionThrown =  dmx.getDmlMessage(0).contains('You do not have permission') ? true : false;                
            }

            Test.stopTest();
        }

        //System.AssertEquals(true, expectedExceptionThrown);
    }

   


    /*
        @author: Mark Cagurong
        @description: test case validation for non admin user preventing editing of task with type = fax
        @createdDate: July 20 2015
        @history:
        <date>     <author>    <change_description>
    */
    public static testMethod void checkRecordOwner_NonRecordOwner(){
        
        //Custom Setting - Trigger Switch
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Task_Trigger__c = false;
        insert triggerSwitch;

        User testUsr1 = OSM_DataFactory.createSalesRep('Rep123');
        User testUsr2 = OSM_DataFactory.createSalesRep('Rep456');

        insert testUsr1;        
        insert testUsr2;

        Case cse = OSM_DataFactory.createCase('Potential Duplicate','Completed');
        cse.RecordTypeId = genericRT;

        insert cse;

        Task tsk = new Task();
        tsk.RecordTypeId = taskDomesticRT;
        tsk.Priority = '1 - Low';
        tsk.Type = 'Fax';
        tsk.OSM_Follow_Up_Date_Time__c = Date.Today()+2;
        tsk.Subject = TEST_FAX_SUBJECT;
        tsk.OwnerId = testUsr1.Id;
        tsk.WhatId = cse.Id;                    
        tsk.OSM_Direction__c = 'Inbound';

        insert tsk;

        //Task tsk2 = [SELECT Id, Type, Subject FROM Task WHERE Id= :tsk.Id LIMIT 1];
        //system.debug('tsk2: - ' + JSON.serialize(tsk2));        

        Boolean expectedExceptionThrown = false;

        System.runAs(testUsr2){            

            Test.startTest();

            try{
                delete tsk;

                system.debug('checkRecordOwner_NonRecordOwner - ...');
            }
            catch(DmlException dmx){
                system.debug('checkRecordOwner_NonRecordOwner.dmx.getDmlMessage(0): ' + dmx.getDmlMessage(0));
                expectedExceptionThrown =  dmx.getDmlMessage(0).contains('You do not have permission') ? true : false;                
            }

            Test.stopTest();
        }

    }
}