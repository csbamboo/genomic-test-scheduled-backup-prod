global class  OSM_TerritoryAlignmentBatchAccount implements Database.Batchable<sObject>, Database.Stateful {
    global Boolean isReRun {get;set;}
    public static Boolean batchIsRunnng = false;
    global List<MyWrapper> wrapperList = new List<MyWrapper>();
    Map<Id, Account> myAcctScope = new Map<Id, Account>();
      public static boolean alignAccountToTerritoryDeleteRunOnce  = false;
    public Class MyWrapper{
        String id;
        String name;
        String accountNumber;
        String errors;
        public MyWrapper(String id, String name, String accountNumber, String errors){
            this.id = id;
            this.name = name;
            this.accountNumber = accountNumber;
            this.errors = errors;
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String soqlString = '';
        if(!isReRun) {
            soqlString = 'Select Id, Name, OSM_Account_Number__c, OSM_Status__c, OSM_Managed_Care__c, OSM_Strategic_Account__c, BillingPostalCode, OSM_Country_Code__c, OSM_Oncology__c, OSM_Urology__c, RecordTypeId From Account Where OSM_Country_Code__c != null AND OSM_Status__c = \'Approved\' Order By OSM_Country_Code__c, BillingPostalCode';
        } else {
            Set<String> errorIds = new Set<String>();
            List<Error_in_Batch__c> eb = [Select Record_Ids__c From Error_in_Batch__c Where Object__c = 'Account' Order By CreatedDate DESC Limit 1];
            
            if(eb.size()>0) {
                for(String idLop : eb[0].Record_Ids__c.split(',')){
                    errorIds.add(idLop);
                }
                soqlString = 'Select Id, Name, OSM_Account_Number__c, OSM_Status__c,  OSM_Managed_Care__c, OSM_Strategic_Account__c, BillingPostalCode, OSM_Country_Code__c, OSM_Oncology__c, OSM_Urology__c, RecordTypeId From Account Where  OSM_Country_Code__c != null AND Id IN: errorIds AND OSM_Status__c = \'Approved\' Order By OSM_Country_Code__c, BillingPostalCode ';
            } else {
                soqlString = 'Select Id, Name, OSM_Account_Number__c, OSM_Status__c,  OSM_Managed_Care__c, OSM_Strategic_Account__c, BillingPostalCode, OSM_Country_Code__c, OSM_Oncology__c, OSM_Urology__c, RecordTypeId From Account Where  OSM_Country_Code__c != null AND OSM_Status__c = \'Approved\' Order By OSM_Country_Code__c, BillingPostalCode';
            }
        }
        return Database.getQueryLocator(soqlString);
    }
    
    global void execute(Database.BatchableContext BC,List<Account> scope){
        batchIsRunnng = true;
        Set<Id> accId = new Set<Id>();
        for(Account sc : scope){
             accId.add(sc.Id);
            
        }
        for(Account acct: [SELECT Id, Name,OSM_Account_Number__c from Account where ID IN: accId]){
            myAcctScope.put(acct.Id, acct);    
        }
        Integer j=0;
        try{
            updateAccounts(scope);
            
        }
        catch(DMLException ex){
             for (Integer i = 0; i < ex.getNumDml(); i++) {
                 if (ex.getDmlId(i) != null && myAcctScope.containsKey(ex.getDmlId(i))){
                    wrapperList.add(new MyWrapper(ex.getDmlId(i), myAcctScope.get(ex.getDmlId(i)).Name,  myAcctScope.get(ex.getDmlId(i)).OSM_Account_Number__c,ex.getDmlMessage(i) ));         
                 }
                 else if(ex.getDmlId(i) != null && !myAcctScope.containsKey(ex.getDmlId(i))){
                    wrapperList.add(new MyWrapper(ex.getDmlId(i), 'Unable to retrieve name', 'Unable to retrieve number', ex.getDmlMessage(i) ));         
                 }
                 
             }

        }
        catch(Exception ex){
                 wrapperList.add(new MyWrapper('N/A', 'N/A', 'N/A', ex.getMessage()));    
        }
    }
    
    public static void updateAccounts(List<Account> updatedAccounts){
        Set<Id> deleteAccountTerritorIds = new Set<Id>();
        Map<Id,Account> accountMap = new Map<Id,Account>();
        Map<Id,Account> nullZipAccountMap = new Map<Id,Account>();
         List<Account> payorPTPList = new List<Account>();
        Record_Type__c rt = Record_Type__c.getOrgDefaults();
        //Check if account status values are updated from draft approved and save them in variables
        for(Account loopAccount : updatedAccounts){
            if(loopAccount.OSM_Status__c == 'Approved'){
                  if(loopAccount.BillingPostalCode != null || loopAccount.OSM_Country_Code__c != null){
                     accountMap.put(loopAccount.Id, loopAccount);
                  }
                    deleteAccountTerritorIds.add(loopAccount.Id);
            }
            else{
                deleteAccountTerritorIds.add(loopAccount.Id);
            }
            
        }
        
        //Delete Assignments
        if(deleteAccountTerritorIds.size() > 0 && !alignAccountToTerritoryDeleteRunOnce){
            OSM_AccountTerritoryAssignTriggerHandler.updateNumberofTerritoriesRunOnce = true;
            delete [SELECT Id FROM OSM_Account_Territory_Assignment__c WHERE (OSM_Override__c = false AND OSM_Manual_Assignment__c = false) AND OSM_Account_Name__c IN :deleteAccountTerritorIds];
            alignAccountToTerritoryDeleteRunOnce = true;
            
        }
        //Re-Assign Accounts to Territories
        if(accountMap.size() > 0){    
            OSM_AccountTriggerHandler.alignAccountToTerritory(accountMap);
        }
        
        List<Account> acctTerrCount = [SELECT Id, OSM_Number_of_Territories__c, (Select Id from Account_Territory_Assignments__r) from Account where ID In: accountMap.keySet()];
        for(Account acct: acctTerrCount){
            acct.OSM_Number_of_Territories__c = acct.Account_Territory_Assignments__r.size();
        }
        if(!acctTerrCount.isEmpty()){
            update acctTerrCount;
        }
    }
    /*
      @author: David Catindoy
      @date: 10 FEB 2015  
      @history: 10 FEB 2015 - Created (David Catindoy)
    */
    global void finish(Database.BatchableContext BC){
        if(wrapperList.size() != 0){
          Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
          //String[] toAddress = new String[]{'dquismorio@cloudsherpas.com', 'david.catindoy@cloudsherpas.com', 'michielpatricia.robrigado@cloudsherpas.com', 'veena.naidu@cloudsherpas.com', 'dazell.calica@cloudsherpas.com'};
          String[] toAddress = Label.OSM_Account_Territory_Batch_Emails.split('::');
          String body = '<table border="1" style="width:100%"><tr><th>Account Name</th><th>Account Number</th><th>Error</th></tr>';
          email.setSubject('Territory Alignment Batch Account Failed Record');
          
          String errorIds = '';
          
          for(Integer i=0; i<wrapperList.size(); i++){
            if(!wrapperList.get(i).errors.contains('Trigger')){
                body += '<tr><td align="center"><a href="'+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +wrapperList.get(i).id+'" target="_blank">'+wrapperList.get(i).name+'</a></td><td align="center">'+wrapperList.get(i).accountNumber+'</td><td align="center">'+wrapperList.get(i).errors+'</td></tr>';
            } else {
                body += '<tr><td align="center" width="30%"><a href="' +System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +parseErrorReturnId(wrapperList.get(i).errors)+'" target="_blank">'+'View Record'+'</a></td><td align="center">'+'Apex Error'+'</td></tr>';
            }
            
            errorIds += wrapperList.get(i).id + ',';
          }
          
          Error_in_Batch__c eb = new Error_in_Batch__c();
          eb.Object__c = 'Account';
          eb.Record_Ids__c = errorIds;
          insert eb;
           
          body += '</table><br/>';
          email.setHtmlBody(body);
          email.setToAddresses(toAddress);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email}); 
        }
    }
    
    public String parseErrorReturnId(String errorMsg){
        Integer indexCount = errorMsg.lastIndexOf('with id')+8;
        return errorMsg.substring(indexCount, indexCount+18);
    }
}