/**
  *  @author: David Catindoy
  *  @date: 5 FEB 2015 - Created
  *  @description: OSM_TerritoryAlignmentBatchAccount (Test Class)
*/
@isTest
private class OSM_TerritoryAlignmentBatchAccount_Test {
    
    static testMethod void testBatchAccount() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        acct.OSM_Country_Code__c = 'US';
        acct.OSM_Status__c = 'Approved';
        acct.BillingCountry = 'United States';
        acct.OSM_Specialty__c = 'Hospital';
        acct.Type = 'HCO';
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        String accid = acct.id;
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Contact', Record_Ids__c = accid);
        insert errInBatch;
        
        errInBatch.Object__c = 'Account';
        update errInBatch;
        
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;
        
        OSM_TerritoryAlignmentBatchAccount tr = new OSM_TerritoryAlignmentBatchAccount();
        tr.IsReRun = true;
        
        OSM_TerritoryAlignmentBatchAccount.MyWrapper a = new OSM_TerritoryAlignmentBatchAccount.MyWrapper(acct.Id, acct.Name, '12321', 'errors');
        tr.wrapperList.add(a);
        
        tr.parseErrorReturnId('String errorMsgString errorMsgString errorMsgString errorMsgString errorMsg');
        Test.startTest();
        Database.executeBatch(tr);
        Test.stopTest();
    }
    
    
    static testMethod void testBatchAccount2() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        List<Account> accList = new List<Account>();
        accList.add(acct);
        
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;
        
        OSM_TerritoryAlignmentBatchAccount tr = new OSM_TerritoryAlignmentBatchAccount();
        tr.IsReRun = false;
        
        Test.startTest();
        Database.executeBatch(tr, 150);
        Test.stopTest();
    }
    
    static testMethod void testBatchAccount3() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        
                      
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;
        
        OSM_TerritoryAlignmentBatchAccount tr = new OSM_TerritoryAlignmentBatchAccount();
        tr.IsReRun = true;
        
        Test.startTest();
        Database.executeBatch(tr);
        Test.stopTest();
    }
    
    static testMethod void testBatchAccount4() {
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        acct.OSM_Country_Code__c = 'US';
        //acct.OSM_Status__c = 'Approved';
        acct.BillingCountry = 'United States';
        acct.OSM_Specialty__c = 'Hospital';
        acct.Type = 'HCO';
        insert acct;
        
        List<Account> accountList = new List<Account>();
        accountList.add(acct);
        Test.startTest();
        OSM_TerritoryAlignmentBatchAccount.updateAccounts(accountList);
        Test.stopTest();
        
    }
    
    
}