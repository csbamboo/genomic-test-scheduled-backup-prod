global class  OSM_TerritoryAlignmentBatchContact implements Database.Batchable<sObject>, Database.Stateful {

    global Boolean isReRun {get;set;}
    public static Boolean batchIsRunnng = false;
    global List<MyWrapper> wrapperList = new List<MyWrapper>();
    public static boolean alignContactToTerritoryDeleteRunOnce  = false;
    public static boolean alignContactToTerritoryRunOnce = false; 
    Map<Id, Contact> myContScope = new Map<Id, Contact>();
    public Class MyWrapper{
        String id;
        String name;
        String errors;
        public MyWrapper(String id, String name, String errors){
            this.id = id;
            this.name = name;
            this.errors = errors;
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String soqlString = '';
        if(!isReRun) {
         soqlString = 'Select Id, Name, OSM_Status__c, MailingPostalCode, OSM_Country_Code__c, OSM_Oncology__c, OSM_Urology__c, RecordTypeId  From Contact Where OSM_Country_Code__c != null AND OSM_Status__c = \'Approved\' Order By OSM_Country_Code__c, MailingPostalCode';
            
        } else {
            Set<String> errorIds = new Set<String>();
            List<Error_in_Batch__c> eb = [Select Record_Ids__c From Error_in_Batch__c Where Object__c = 'Contact' Order By CreatedDate DESC Limit 1];
            
            if(eb.size()>0) {
                for(String idLop : eb[0].Record_Ids__c.split(',')){
                    errorIds.add(idLop);
                }
                soqlString = 'Select Id, Name, OSM_Status__c, MailingPostalCode, OSM_Country_Code__c, OSM_Oncology__c, OSM_Urology__c, RecordTypeId  From Contact Where Id IN: errorIds';
            } else {
                soqlString = 'Select Id, Name, OSM_Status__c, MailingPostalCode, OSM_Country_Code__c, OSM_Oncology__c, OSM_Urology__c, RecordTypeId From Contact';
            }
        }
        return Database.getQueryLocator(soqlString);
    }
    /*
      @author: Daniel Quismorio
      @history: 10 FEB 2015 - Updated (David Catindoy)
    */
    global void execute(Database.BatchableContext BC,List<Contact> scope){
        batchIsRunnng = true;
        Set<Id> conId = new Set<Id>();
        for(Contact sc : scope){
             conId.add(sc.Id);
            
        }
        for(Contact con: [SELECT Id, Name from Contact where ID IN: conId]){
            myContScope.put(con.Id, con);    
        }
        Integer j=0;
        try{
            updateContacts(scope);
        }
        catch(DMLException ex){
             for (Integer i = 0; i < ex.getNumDml(); i++) {
                 if (ex.getDmlId(i) != null && myContScope.containsKey(ex.getDmlId(i))){
                    wrapperList.add(new MyWrapper(ex.getDmlId(i), myContScope.get(ex.getDmlId(i)).Name, ex.getDmlMessage(i) ));         
                 }
                 else if(ex.getDmlId(i) != null && !myContScope.containsKey(ex.getDmlId(i))){
                    wrapperList.add(new MyWrapper(ex.getDmlId(i), 'Unable to retrieve name', ex.getDmlMessage(i) ));         
                 }
                 
             }

        }
        catch(Exception ex){
                 wrapperList.add(new MyWrapper('N/A', 'N/A', ex.getMessage()));    
        }

    }
    
    public static void updateContacts(List<Contact> updatedContacts){
         Id hcpId = Schema.SObjectType.Contact.RecordTypeInfosByName.get('HCP').RecordTypeId;
         Id hcpDEId = Schema.SObjectType.Contact.RecordTypeInfosByName.get('HCP DE').RecordTypeId;
         Id hcpUKId = Schema.SObjectType.Contact.RecordTypeInfosByName.get('HCP UK').RecordTypeId;
         Set<Id> deleteContactTerritorIds = new Set<Id>();
         Map<Id,Contact> contactMap = new Map<Id,Contact>();
         Map<Id,Contact> nullZipContactMap = new Map<Id,Contact>();
         ID patientID = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Patient').RecordTypeId;
         Map<Id, String> conOrderWorkflowMap = new Map<Id, String>();
       
         for(Contact loopContact : updatedContacts){
            
            if(loopContact.OSM_Status__c == 'Approved'){
                if((loopContact.recordTypeId == hcpId || loopContact.recordTypeId == hcpDEId || loopContact.recordTypeId == hcpUKId)){
                    if(loopContact.MailingPostalCode != null || loopContact.OSM_Country_Code__c != null){
                        contactMap.put(loopContact.Id, loopContact);
                    }
                    deleteContactTerritorIds.add(loopContact.Id);
                }
            }
            else{
                deleteContactTerritorIds.add(loopContact.Id);
            }
            
        }
        
         //Delete Assignments
        if(deleteContactTerritorIds.size() > 0 && !alignContactToTerritoryDeleteRunOnce){
            OSM_HCPTerritoryAssignmentTriggerHandler.updateNumberofTerritoriesRunOnce = true;
            delete [SELECT Id FROM OSM_HCP_Territory_Assignment__c WHERE  (OSM_Override__c = false AND OSM_Manual_Assignment__c = false) AND OSM_HCP_Name__c IN :deleteContactTerritorIds LIMIT 10000];
            alignContactToTerritoryDeleteRunOnce = true;
        }
        
         if(contactMap.size() > 0){    
            OSM_ContactTriggerHandler.alignContactToTerritory(contactMap);
        }
        
        List<Contact> conTerrCount = [SELECT Id, OSM_Number_of_Territories__c, (Select Id from HCP_Territory_Managements__r) from Contact where ID In: contactMap.keySet()];
        List<Contact> updateContact = new List<Contact>();
        for(Contact con: conTerrCount){
            Contact updCon = new Contact(Id=con.Id, OSM_Number_of_Territories__c = con.HCP_Territory_Managements__r.size());
            updateContact.add(updCon);
         //   con.OSM_Number_of_Territories__c = con.HCP_Territory_Managements__r.size();
        }
        if(!updateContact.isEmpty()){
            system.debug('Update number of territories here');
            update updateContact;
        }
    }
    /*
      @author: David Catindoy
      @date: 10 FEB 2015  
      @history: 10 FEB 2015 - Created (David Catindoy)
    */
    global void finish(Database.BatchableContext BC){
        if(wrapperList.size() != 0){
          Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
          //String[] toAddress = new String[]{'dquismorio@cloudsherpas.com', 'david.catindoy@cloudsherpas.com', 'michielpatricia.robrigado@cloudsherpas.com', 'veena.naidu@cloudsherpas.com', 'dazell.calica@cloudsherpas.com'};
          String[] toAddress = Label.OSM_Contact_Territory_Batch_Emails.split('::');
          String body = '<table border="1" style="width:100%"><tr><th>Contact Name</th><th>Error</th></tr>';
          email.setSubject('Territory Alignment Batch Contact Failed Record');
          
          String errorIds = '';
          
          for(Integer i=0; i<wrapperList.size(); i++){
            if(!wrapperList.get(i).errors.contains('Trigger')){
                body += '<tr><td align="center"><a href="' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +wrapperList.get(i).id+'" target="_blank">'+wrapperList.get(i).name+'</a></td><td align="center">'+wrapperList.get(i).errors+'</td></tr>';
            } else {
                body += '<tr><td align="center" width="30%"><a href="'  + System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +parseErrorReturnId(wrapperList.get(i).errors)+'" target="_blank">'+'View Record'+'</a></td><td align="center">'+'Apex Error'+'</td></tr>';
            }
            
            errorIds += wrapperList.get(i).id + ',';
          }
          
          Error_in_Batch__c eb = new Error_in_Batch__c();
          eb.Object__c = 'Contact';
          eb.Record_Ids__c = errorIds;
          insert eb;
          
          body += '</table><br/>';
          email.setHtmlBody(body);
          email.setToAddresses(toAddress);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
        }
    }
    
    public String parseErrorReturnId(String errorMsg){
        Integer indexCount = errorMsg.lastIndexOf('with id')+8;
        return errorMsg.substring(indexCount, indexCount+18);
    }
}