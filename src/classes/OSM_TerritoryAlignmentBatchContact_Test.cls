/**
  *  @author: David Catindoy
  *  @date: 5 FEB 2015 - Created
  *  @description: OSM_TerritoryAlignmentBatchContact (Test Class)
*/
@isTest
private class OSM_TerritoryAlignmentBatchContact_Test {
     
    
     static testMethod void testBatchContact1() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;
        
        OSM_TerritoryAlignmentBatchContact tr = new OSM_TerritoryAlignmentBatchContact();
        tr.IsReRun = true;
      
        Test.startTest();
        Database.executeBatch(tr);
        
        Test.stopTest();
        
    }
     static testMethod void testBatchContact2() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        contact.OSM_Status__c = 'Approved';
        insert contact;
        
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Contact contact2 = OSM_DataFactory.createContact('David', 'Catindoy', HCPRecType);
        contact2.MailingPostalCode = '2123';
        contact2.Salutation = 'Dr.';
        contact2.MailingCountry = 'Philippines';
        contact2.OSM_Specialty__c = 'Gynecologist';
        contact2.OSM_Status__c = 'Approved';
        insert contact2;
        
        
        
        Error_in_Batch__c eb = new Error_in_Batch__c(Object__c = 'Contact',Record_Ids__c=(String)contact.Id);
        insert eb;
        
        OSM_TerritoryAlignmentBatchContact tr = new OSM_TerritoryAlignmentBatchContact();
        tr.IsReRun = true;
        
        OSM_TerritoryAlignmentBatchContact.MyWrapper a = new OSM_TerritoryAlignmentBatchContact.MyWrapper((String)contact.Id, contact.Name, 'errorserrorserrorserrorserrorserrorserrorserrors');
        tr.wrapperList.add(a);
        
        tr.parseErrorReturnId('Saaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
        Test.startTest();
        Database.executeBatch(tr);
        
        Test.stopTest();
        
    }
    
    static testMethod void testBatchContact3() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        contact.OSM_Status__c = 'Approved';
        insert contact;
        
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Contact contact2 = OSM_DataFactory.createContact('David', 'Catindoy', HCPRecType);
        contact2.MailingPostalCode = '2123';
        contact2.Salutation = 'Dr.';
        contact2.MailingCountry = 'Philippines';
        contact2.OSM_Specialty__c = 'Gynecologist';
        contact2.OSM_Status__c = 'Approved';
        insert contact2;
        
        OSM_TerritoryAlignmentBatchContact tr = new OSM_TerritoryAlignmentBatchContact();
        tr.IsReRun = false;
        
        OSM_TerritoryAlignmentBatchContact.MyWrapper a = new OSM_TerritoryAlignmentBatchContact.MyWrapper((String)contact.Id, contact.Name, 'TriggerTriggerTriggerTriggerTriggerTriggerTriggerTriggerTrigger');
        tr.wrapperList.add(a);
        
        tr.parseErrorReturnId('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
        Test.startTest();
        Database.executeBatch(tr);
        
        Test.stopTest();
        
    }
    
    static testMethod void testBatchContact4() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        contact.OSM_Status__c = 'Approved';
        insert contact;
        
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Contact contact2 = OSM_DataFactory.createContact('David', 'Catindoy', HCPRecType);
        contact2.MailingPostalCode = '2123';
        contact2.Salutation = 'Dr.';
        contact2.MailingCountry = 'Philippines';
        contact2.OSM_Specialty__c = 'Gynecologist';
        contact2.OSM_Status__c = 'Approved';
        insert contact2;
        
        OSM_TerritoryAlignmentBatchContact tr = new OSM_TerritoryAlignmentBatchContact();
        tr.IsReRun = true;
        
        OSM_TerritoryAlignmentBatchContact.MyWrapper a = new OSM_TerritoryAlignmentBatchContact.MyWrapper((String)contact.Id, contact.Name, 'TriggerTriggerTriggerTriggerTriggerTriggerTriggerTriggerTrigger');
        tr.wrapperList.add(a);
        
        tr.parseErrorReturnId('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
        Test.startTest();
        Database.executeBatch(tr);
        
        Test.stopTest();
        
    }
    
    
   
}