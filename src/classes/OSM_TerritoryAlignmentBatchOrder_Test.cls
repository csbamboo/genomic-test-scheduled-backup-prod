/**
  *  @author: David Catindoy
  *  @date: 5 FEB 2015 - Created
  *  @description: OSM_TerritoryAlignmentBatchOrder (Test Class)
*/
@isTest
private class OSM_TerritoryAlignmentBatchOrder_Test {
    
    static testMethod void testBatchOrder() {
    
        TriggerSwitch__c trigSwitch = TriggerSwitch__c.getOrgDefaults(); 
        trigSwitch.Order_Trigger__c = false;
        trigSwitch.Work_Order_Trigger__c = false;
        trigSwitch.Order_Line_ItemTrigger__c = false;
        trigSwitch.Order_Role_Trigger__c = false;
        insert trigSwitch;
        
        Account account1 = OSM_DataFactory.createAccount('New Account');
        insert account1;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact1 = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact1;

        Order order1 = new Order(
            Order_Location__c = 'Domestic',
            Pricebook2Id = Test.getStandardPriceBookId(),
            OSM_Patient__c = contact1.Id,
            AccountId = account1.Id,
            OSM_Status__c = 'Order Intake',
            OSM_Product__c = 'DCIS',
            EffectiveDate = System.Today(),
            Status = 'New');
        insert order1;
        
        OSM_Order_Role__c ordRole2 = OSM_DataFactory.createOrderRole(order1.Id, account1.Id, null);
        ordRole2.OSM_Role__c='Ordering';
        insert ordRole2;
        
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Order', Record_Ids__c = (String)Order1.Id);
        insert errInBatch;
        
        
        OSM_TerritoryAlignmentBatchOrder tr = new OSM_TerritoryAlignmentBatchOrder();
        
        
        tr.isReRun = true;
        OSM_TerritoryAlignmentBatchOrder.MyWrapper a = new OSM_TerritoryAlignmentBatchOrder.MyWrapper('Order',(String)order1.Id, 'error');
        tr.WrapperList.add(a);
        
        Test.startTest();
        Database.executeBatch(tr);
        Test.stopTest();

        //System.assert(OSM_TerritoryAlignmentBatchOrder.batchIsRunnng);
    }
    
        static testMethod void testWithTrigger() {
        
        TriggerSwitch__c trigSwitch = TriggerSwitch__c.getOrgDefaults(); 
        trigSwitch.Order_Trigger__c = false;
        trigSwitch.Work_Order_Trigger__c = false;
        trigSwitch.Order_Line_ItemTrigger__c = false;
        insert trigSwitch;
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        insert order;
        
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Order', Record_Ids__c = (String)Order.Id);
        insert errInBatch;
        
        
        OSM_TerritoryAlignmentBatchOrder tr = new OSM_TerritoryAlignmentBatchOrder();
        
        
        tr.isReRun = true;
        OSM_TerritoryAlignmentBatchOrder.MyWrapper a = new OSM_TerritoryAlignmentBatchOrder.MyWrapper('Order',(String)order.Id, 'TriggerTriggerTriggerTriggerTriggerTriggerTriggerTriggerTrigger');
        tr.WrapperList.add(a);
        tr.wrapperListSuccess.add(1);
        Test.startTest();
        Database.executeBatch(tr);
        Test.stopTest();


        //System.assert(OSM_TerritoryAlignmentBatchOrder.batchIsRunnng);
    }
    
    static testMethod void testOtherMethods() {
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        insert order;
        
        
        Id orderRoleRecType = Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Account & Contact').getRecordTypeId();
        OSM_Order_Role__c orderRole = OSM_DataFactory.createOrderRole(order.Id, account.Id, null, 'Ordering', orderRoleRecType);
        insert orderRole;
        List<OSM_Order_Role__c> orderRoleList = new List<OSM_Order_Role__c>{orderRole};
        
        OSM_Territory__c territory = OSM_DataFactory.createTerritory('Parent');
        insert territory;
        
        OSM_Order_Territory_Assignment__c ota = new OSM_Order_Territory_Assignment__c(OSM_Territory_Name__c = territory.Id, OSM_OrderName__c = order.Id, OSM_Override__c = false, OSM_Manual_Assignment__c = false);
        insert ota;
        
        OSM_TerritoryAlignmentBatchOrder tr = new OSM_TerritoryAlignmentBatchOrder();
        
        Test.startTest();
        tr.orderterritoryAlignment(orderRoleList);
        tr.parseErrorReturnId('String errorMsgString errorMsgString errorMsgString errorMsgString errorMsg');
        Test.stopTest();
        
        //System.assert(OSM_TerritoryAlignmentBatchOrder.batchIsRunnng);
    }
    
    
}