/*
            @author: patrick lorilla
            @date:28 SEPT 2015
            @description: batch class for terr alignments
            @history:28 SEPT 2015 Created (Patrick Lorilla)
        
 */
global  class OSM_TerritoryAlignmentBatchOrder_V2 implements Database.Batchable<sObject>, Database.Stateful {
    global Boolean isReRun {get;set;}
    public static Boolean batchIsRunnng = false;
    global List<MyWrapper> wrapperList = new List<MyWrapper>();
     global List<Integer> wrapperListSuccess = new List<Integer>();
    Map<Id, Account> myAcctScope = new Map<Id, Account>();
    public static Boolean flagDeleteOnce = true;
    public static boolean alignOrderToTerritoryDeleteRunOnce  = false;
    /*
            @author: patrick lorilla
            @date:28 SEPT 2015
            @description: wrapper class
            @history: 28 SEPT 2015 - Created (Patrick Lorilla)
        
    */
    public Class MyWrapper{
        String name;
        String id;
        String errors;
        /*
            @author: patrick lorilla
            @date:28 SEPT 2015
            @description: wrapper class const
            @history: 28 SEPT 2015 - Created (Patrick Lorilla)
        
        */
        public MyWrapper(String name, String id, String errors){
            this.name = name;
            this.id = id;
            this.errors = errors;
        }
    }
    /*
            @author: patrick lorilla
            @date:28 SEPT 2015
            @description: start batch
            @history: 28 SEPT 2015 - Created (Patrick Lorilla)
        
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
         String soqlString = '';
        
        if(!isReRun) {
            soqlString = 'Select Id, OrderNumber From Order';
        } else {
            Set<String> errorIds = new Set<String>();
            List<Error_in_Batch__c> eb = [Select Record_Ids__c From Error_in_Batch__c Where Object__c = 'Order' Order By CreatedDate DESC Limit 1];
            
            if(eb.size()>0) {
                for(String idLop : eb[0].Record_Ids__c.split(',')){
                    errorIds.add(idLop);
                }
                soqlString = 'Select Id, OrderNumber From Order Where Id IN: errorIds';
            } else {
                soqlString = 'Select Id, OrderNumber From Order';
            } 
        }
        return Database.getQueryLocator(soqlString);
    }
    /*
            @author: patrick lorilla
            @date:28 SEPT 2015
            @description: execute batch
            @history: 28 SEPT 2015 - Created (Patrick Lorilla)
        
    */
    global void execute(Database.BatchableContext BC,List<Order> scope){
        System.debug ('\n\n\n EXECUTEORD');
    
        batchIsRunnng = true;
        List<Order> orderList = new List<Order>();
        Set<ID> orIds = new Set<ID>();
        for(Order s : scope){
          Order ordr = (Order) s;
          orderList.add(ordr);
          orIds.add(ordr.Id);
        }
        List<OSM_Order_Role__c> ordRoleList = [SELECT Id, OSM_Role__c, OSM_Account__c, OSM_Order__r.OrderNumber, OSM_Order__c, OSM_Country_Code__c, OSM_Account_Postal_Address__c from OSM_Order_Role__c where OSM_Order__c IN: orIds AND OSM_Role__c = 'Ordering' order by OSM_Country_Code__c, OSM_Account_Postal_Address__c];
        Integer successcount = 0;
        Integer j=0;
        
        try{
            orderterritoryAlignment(ordRoleList);
        }
        catch(DMLException ex){
             for (Integer i = 0; i < ex.getNumDml(); i++) {
                 if (ex.getDmlId(i) != null){
                     wrapperList.add(new MyWrapper(ordRoleList.get(i).OSM_Order__r.OrderNumber, ordRoleList.get(i).Id, ex.getDmlMessage(i)));
                 }

             }

        }
        catch(Exception ex){
                 wrapperList.add(new MyWrapper('N/A', 'N/A', ex.getMessage()));    
        }
    }
    public void orderterritoryAlignment(List<OSM_Order_Role__c> newOrderRoleList){
       
        List<OSM_Order_Role__c> updatedOrderRoleList = new List<OSM_Order_Role__c>();
        List<OSM_Order_Role__c> validORForTA = new List<OSM_Order_Role__c>();
        List<OSM_Order_Role__c> ordIdList = new List<OSM_Order_Role__c>();
        Set<Id> ordId = new Set<Id>();
        List<OSM_Order_Role__c> delordIdOTAList = new List<OSM_Order_Role__c>();
         Set<Id> delordIdOTA = new Set<Id>();
        Boolean isHCO;
        for(OSM_Order_Role__c ord: newOrderRoleList){
            ordId.add(ord.OSM_Order__c);
            ordIdList.add(ord);
            if (ord.OSM_Role__c == 'Ordering'){
                    validORForTA.add(ord);
             }

        }
        if (ordId.size() > 0) {
            List<OSM_Order_Territory_Assignment__c> deleteOTAList = [SELECT Id, OSM_Order__c FROM OSM_Order_Territory_Assignment__c WHERE (OSM_Override__c = false AND OSM_Manual_Assignment__c = false) AND OSM_OrderName__c IN :ordId];
            for(OSM_Order_Territory_Assignment__c ota : deleteOTAList){
                delordIdOTA.add(ota.OSM_Order__c);
            }
            isHCO = true;
           // validORForTA =  delordIdOTAList;
             System.debug('++++ del territory Size'  + deleteOTAList.size());
             if(deleteOTAList.size()>0 && flagDeleteOnce == true){
                  flagDeleteOnce = false;
                  delete deleteOTAList;
             }
         }

        system.debug('test validORForTA ' + validORForTA);
        if(validORForTA.size() > 0){
            OSM_OrderRoleTriggerHandler.alignmentRule(validORForTA, isHCO);
            flagDeleteOnce = false;
        }
    }
    /*
      @author: David Catindoy
      @date: 10 FEB 2015  
      @history: 10 FEB 2015 - Created (David Catindoy)
    */
    global void finish(Database.BatchableContext BC){
       AsyncApexJob aaj = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        
        
        if(wrapperList.size() != 0){
          Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
          String[] toAddress = Label.OSM_Order_Territory_Batch_Emails.split('::');
          String body = '<style>span.dropt:hover {text-decoration: none; background: #ffffff; z-index: 6; }span.dropt span {position: absolute; left: -9999px;margin: 20px 0 0 0px; padding: 1px 1px 1px 1px;border-style:solid; border-color:black; border-width:1px; z-index: 6;}span.dropt:hover span {left: 0%; background: #ffffff;} span.dropt span {position: absolute; left: -9999px;margin: 4px 0 0 0px; padding: 1px 1px 1px 1px; border-style:solid; border-color:black; border-width:1px;}span.dropt:hover span {margin: 10px 0 0 170px; background: #ffffff; z-index:6;} </style><table border="1" style="width:100%"><tr><th>Order Name</th><th>Error</th></tr>';
          email.setSubject('Territory Alignment Batch Order Failed Record');
          
          String errorIds = '';
          
          for(Integer i=0; i<wrapperList.size(); i++){
            if(!wrapperList.get(i).errors.contains('Trigger')){
                body += '<tr><td align="center" width="30%"><a href="'+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +wrapperList.get(i).id+'" target="_blank">'+wrapperList.get(i).name+'</a></td><td align="center">'+wrapperList.get(i).errors+'</td></tr>';
            } else {
                
                body += '<tr><td align="center" width="30%"><a href="'+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +wrapperList.get(i).id+'" target="_blank">'+wrapperList.get(i).name+'</a></td><td align="center">'+'<span class="dropt" title="Title for the pop-up">Apex Error'+'</td></tr>';
            }
            
            errorIds += wrapperList.get(i).id + ',';
          }
          System.debug('\n\n\n ERRORINFO'+wrapperList);
          Error_in_Batch__c eb = new Error_in_Batch__c();
          eb.Object__c = 'Order';
          eb.Record_Ids__c = errorIds;
          insert eb;
          
          body += '</table><br/> Status'+aaj.Status+'<br/>Number Of Errors'+aaj.NumberofErrors+'<br/>JobItemsProcessed'+aaj.JobItemsProcessed+'<br/>TotalJobItems'+aaj.TotalJobItems;
          email.setHtmlBody(body);
          email.setToAddresses(toAddress);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
        }
        
        if(wrapperListSuccess.size() != 0){
             Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
             String[] toAddress = Label.OSM_Order_Territory_Batch_Emails.split('::');
          String body = '<style>span.dropt:hover {text-decoration: none; background: #ffffff; z-index: 6; }span.dropt span {position: absolute; left: -9999px;margin: 20px 0 0 0px; padding: 1px 1px 1px 1px;border-style:solid; border-color:black; border-width:1px; z-index: 6;}span.dropt:hover span {left: 0%; background: #ffffff;} span.dropt span {position: absolute; left: -9999px;margin: 4px 0 0 0px; padding: 1px 1px 1px 1px; border-style:solid; border-color:black; border-width:1px;}span.dropt:hover span {margin: 10px 0 0 170px; background: #ffffff; z-index:6;} </style><table border="1" style="width:100%"><tr><th>Order Name</th><th>Status</th></tr>';
          email.setSubject('Territory Alignment Batch Order Success Records');
          
          Integer successcount= 0;
         for(Integer i :  wrapperListSuccess){
          //  body += '<tr><td align="center" width="30%"><a href="'+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +wrapperListSuccess.get(i).id+'" target="_blank">'+wrapperListSuccess.get(i).name+'</a></td><td align="center">Success</td></tr>';
            successcount = successcount + i;
          }
        // body += '<tr><td align="center" width="30%">'+wrapperListSuccess.size()+'</td><td align="center">Success Number of Records</td></tr>';
        body += ''+wrapperList.size()+' '+'Number of Error Records';
        body += ''+wrapperListSuccess.size()+' '+'Success List size';
        body += '\n'+successcount+' '+' Number of Success Records';
          email.setHtmlBody(body);
          email.setToAddresses(toAddress);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});    
        }
    }
    
    public String parseErrorReturnId(String errorMsg){
        Integer indexCount = errorMsg.lastIndexOf('with id')+8;
        return errorMsg.substring(indexCount, indexCount+18);
    }
    
}