/*
    @author: patrick lorilla and David E. Catindoy
    @date: 27 Oct 2014
    @description: Territory Sales Rep Assignment Trigger Handler Test
    @history: 27 Oct 2014 - Patrick Lorilla - Created
    @history: 23 Jan 2015 - David E. Catindoy - Updated

*/

@isTest(seeAllData = true)
public class OSM_TerritorySRAssignTriggerHandler_Test{
    
    static testmethod void testCreateAccountShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Sales_Rep_Assignment_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Account_Territory_Assignment_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        TriggerSwitch.GHI_Portal_UserTrigger__c = false;
        update triggerSwitch;
        //Prepare account
        User testUsr = OSM_DataFactory.createUser('Catindoy');
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        //Prepare parent territory
        OSM_Territory__c pterr = OSM_DataFactory.createTerritory('My ParentTerr');
       
        insert pterr;
        //Prepare territory
        OSM_Territory__c terr = OSM_DataFactory.createTerritory('My Terr');
        
        terr.OSM_Parent_Territory__c = pterr.ID;
        insert terr;
        
        
        //Prepare Sales Reps for TRSA
        List<User> sr = new List<User>();
        for(Integer a=0; a<40; a++){
            sr.add(OSM_DataFactory.createUser('srep'+a));
        }
        
        insert sr;
        
        //Prepare ATA 
        OSM_Account_Territory_Assignment__c ata = OSM_DataFactory.createATA(acct.ID, terr.ID);
        insert ata;
        Test.startTest();
        System.runAs(testUsr){
            //Prepare TSRA
            List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
            for(Integer a=0; a<40; a++){
                if(a<10){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, 'Read', null ));
                }
                else if (a<20){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, 'Edit', null ));
                }
                else if(a<30){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, 'Read', null ));
                }
                else{
                    tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, 'Edit', null ));
                }
            }
            
            insert tsraList;

            //Prepare TSRA 2
            List<OSM_Territory_Sales_Rep_Assignment__c> tsraList2 = new List<OSM_Territory_Sales_Rep_Assignment__c>();
            for(Integer a=0; a<40; a++){
                if(a<10){
                    tsraList2.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, 'Read', null ));
                }
                else if (a<20){
                    tsraList2.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, 'Edit', null ));
                }
                else if(a<30){
                    tsraList2.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, 'Read', null ));
                }
                else{
                    tsraList2.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, 'Edit', null ));
                }
            }

        }
        Test.stopTest();
       
        //Assert account share and users
        /*Aman Comment Out
        Map<ID, User> userMap = new Map<ID,User>([SELECT ID, LastName, Alias from User where Alias != 'TestUser']);
        for(AccountShare aShare : [SELECT UserorGroupId from AccountShare Where AccountAccessLevel = 'Read']){
            //System.assertEquals(userMap.containsKey(aShare.UserorGroupId), true, 'error');
            System.debug('aShare: '+ aShare);
        }*/
        
    }
   
   static testmethod void testUpdateAccountShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Sales_Rep_Assignment_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Account_Territory_Assignment_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        TriggerSwitch.GHI_Portal_UserTrigger__c = false;
        update triggerSwitch;
        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        //Prepare account
        User testUsr = OSM_DataFactory.createUser('Catindoy');
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        //Prepare parent territory
        OSM_Territory__c pterr = OSM_DataFactory.createTerritory('My ParentTerr');
        insert pterr;
        //Prepare territory
        OSM_Territory__c terr = OSM_DataFactory.createTerritory('My Terr');
        terr.OSM_Parent_Territory__c = pterr.ID;
        insert terr;
        Contact testContact1 = OSM_DataFactory.createContactWithMailingAddress(1, 'USA', 'Test Street', 'Test City', 'Test Province', '12345', null);
        testContact1.OSM_Status__c = 'Approved';
        testContact1.OSM_Specialty__c = 'Hospital';
        testContact1.OSM_Oncology__c = true;
        testContact1.OSM_Urology__c = true;
        testContact1.OSM_Country_Code__c = 'US';
        
        Order ord1 =  new Order(OSM_Patient__c = testContact1.Id, AccountId = acct.Id ,Pricebook2Id = pb2Standard.Id, EffectiveDate = System.Today(), Status = 'New', Order_Location__c = 'International', OSM_Triage_Outcome__c = 'Resubmission - Confirmed', OSM_Status__c = 'Order Intake', OSM_Product__c = 'IBC', OSM_Work_Order_Status__c = 'Open');
        insert ord1;
        OSM_Order_Territory_Assignment__c Ota1 = new OSM_Order_Territory_Assignment__c(OSM_Territory_Name__c = pterr.Id, OSM_OrderName__c = ord1.Id, OSM_Order__c = ord1.Id);
        insert Ota1;
        
        //Prepare Sales Reps for TRSA
        List<User> sr = new List<User>();
        for(Integer a=0; a<40; a++){
            sr.add(OSM_DataFactory.createUser('srep'+a));
        }
        
        insert sr;
        
        //Prepare ATA 
        OSM_Account_Territory_Assignment__c ata = OSM_DataFactory.createATA(acct.ID, terr.ID);
        insert ata;
        //Insert TSRA List
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        OSM_Territory_Sales_Rep_Assignment__c tsraAdd = new OSM_Territory_Sales_Rep_Assignment__c();
        for(Integer a=0; a<40; a++){
            if(a<10){
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, 'Read', null )); 
            }
            else if (a<20){
                tsraAdd = OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, 'Edit', null );
                tsraAdd.OSM_Inactive__c = false;
                tsraList.add(tsraAdd);
            }
            else if(a<30){
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, 'Read', null ));
            }
            else{
                tsraAdd = OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, 'Edit', null );
                tsraAdd.OSM_Inactive__c = false;
                tsraList.add(tsraAdd);
            }
         }
            
        insert tsraList;
        
        
        test.startTest();
        System.runAs(testUsr){
            //Update account access level
            for(OSM_Territory_Sales_Rep_Assignment__c tsra: tsraList){
                if(tsra.OSM_Account_Access_Level__c == 'Edit'){
                    tsra.OSM_Account_Access_Level__c = 'Read';    
                }
                else { //Set inactive
                    tsra.OSM_Account_Access_Level__c = 'Edit';  
                    tsra.OSM_Inactive__c = true;
                }
            }
            update tsraList;
        }
        Map<ID, User> userMap = new Map<ID,User>([SELECT ID, LastName, Alias from User where Alias != 'TestUser']);
        test.stopTest();
        
        /*Aman Comment Out
        List<AccountShare> aShareList = new List<AccountShare>();
        aShareList = [SELECT UserorGroupId from AccountShare Where AccountAccessLevel = 'Read'];
        for(AccountShare aShare : aShareList){
            //System.assertEquals(userMap.containsKey(aShare.UserorGroupId), true, 'error');
        }*/

    }
     
     static testmethod void testDeleteAccountShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Sales_Rep_Assignment_Trigger__c = true;
        TriggerSwitch.GHI_Portal_UserTrigger__c = false;
        update triggerSwitch;
        //Prepare account
        User testUsr = OSM_DataFactory.createUser('Catindoy');
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        //Prepare parent territory
        OSM_Territory__c pterr = OSM_DataFactory.createTerritory('My ParentTerr');
        insert pterr;
        //Prepare territory
        OSM_Territory__c terr = OSM_DataFactory.createTerritory('My Terr');
        terr.OSM_Parent_Territory__c = pterr.ID;
        insert terr;
        
        
        //Prepare Sales Reps for TRSA
        List<User> sr = new List<User>();
        for(Integer a=0; a<40; a++){
            sr.add(OSM_DataFactory.createUser('srep'+a));
        }
        
        insert sr;
        
        //Prepare ATA 
        OSM_Account_Territory_Assignment__c ata = OSM_DataFactory.createATA(acct.ID, terr.ID);
        insert ata;
        //Insert TSRA
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<40; a++){
            if(a<10){
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, 'Read', null ));
            }
            else if (a<20){
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, 'Edit', null ));
            }
            else if(a<30){
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, 'Read', null ));
            }
            else{
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, 'Edit', null ));
            }
         }
            
        insert tsraList;
        
        test.startTest();
        System.runAs(testUsr){
            //Delete TSRA List
            delete tsraList;
        }
        Map<ID, User> userMap = new Map<ID,User>([SELECT ID, LastName, Alias from User where Alias != 'TestUser']);
        test.stopTest();
        
        /*Aman Comment Out
        List<AccountShare> aShareList = new List<AccountShare>();
        aShareList = [SELECT UserorGroupId from AccountShare Where AccountAccessLevel = 'Read'];
        for(AccountShare aShare : aShareList){
            //System.assertEquals(userMap.containsKey(aShare.UserorGroupId), true, 'error');
        }*/

    }
     
     static testmethod void testCreateContactShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Sales_Rep_Assignment_Trigger__c = true;
        triggerSwitch.Contact_Trigger__c = false;
        TriggerSwitch.GHI_Portal_UserTrigger__c = false;
        update triggerSwitch;
        //Prepare account
        User testUsr = OSM_DataFactory.createUser('Catindoy');
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        //Prepare parent territory
        OSM_Territory__c pterr = OSM_DataFactory.createTerritory('My ParentTerr');
        insert pterr;
        //Prepare territory
        OSM_Territory__c terr = OSM_DataFactory.createTerritory('My Terr');
        terr.OSM_Parent_Territory__c = pterr.ID;
        insert terr;
        //prepare hcp
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
        
        //Prepare Sales Reps for TRSA
        List<User> sr = new List<User>();
        for(Integer a=0; a<40; a++){
            sr.add(OSM_DataFactory.createUser('srep'+a));
        }
        
        insert sr;
        
        //Prepare HTA 
        OSM_HCP_Territory_Assignment__c hta = OSM_DataFactory.createHTA(hcp.ID, terr.ID);
        insert hta;
        test.startTest();
        System.runAs(testUsr){
            //Insert TSRA
            List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
            for(Integer a=0; a<40; a++){
                if(a<10){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, null ,'Read'));
                }
                else if (a<20){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, null ,'Edit' ));
                }
                else if(a<30){
                    tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, null ,'Read' ));
                }
                else{
                    tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID,  null, 'Edit' ));
                }
            }
            
            insert tsraList;
        }
        test.stopTest();
        
        /* Aman Comment Out
        Map<ID, User> userMap = new Map<ID,User>([SELECT ID, LastName, Alias from User where Alias != 'TestUser']);
        for(ContactShare cShare : [SELECT UserorGroupId from ContactShare Where ContactAccessLevel = 'Read']){
            //System.assertEquals(userMap.containsKey(cShare.UserorGroupId), true, 'error');
        }*/
    }
    
    static testmethod void testDeleteContactShare(){
        Record_Type__c recTypes = OSM_DataFactory.recordLists();
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Sales_Rep_Assignment_Trigger__c = true;
        triggerSwitch.Contact_Trigger__c = false;
        TriggerSwitch.GHI_Portal_UserTrigger__c = false;
        update triggerSwitch;
        //Prepare account
        User testUsr = OSM_DataFactory.createUser('Catindoy');
        Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
        insert acct;
        //Prepare parent territory
        OSM_Territory__c pterr = OSM_DataFactory.createTerritory('My ParentTerr');
        insert pterr;
        //Prepare territory
        OSM_Territory__c terr = OSM_DataFactory.createTerritory('My Terr');
        terr.OSM_Parent_Territory__c = pterr.ID;
        insert terr;
        //prepare hcp
        Contact hcp =  OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
        insert hcp;
        
        //Prepare Sales Reps for TRSA
        List<User> sr = new List<User>();
        for(Integer a=0; a<40; a++){
            sr.add(OSM_DataFactory.createUser('srep'+a));
        }
        
        insert sr;
        
        //Prepare HTA 
        OSM_HCP_Territory_Assignment__c hta = OSM_DataFactory.createHTA(hcp.ID, terr.ID);
        insert hta;
     
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<40; a++){
            if(a<10){
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, null ,'Read'));
            }
            else if (a<20){
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, pterr.ID, null ,'Edit' ));
            }
            else if(a<30){
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID, null ,'Read' ));
            }
            else{
                tsraList.add(OSM_DataFactory.createTerritorySRA(sr[a].ID, terr.ID,  null, 'Edit' ));
            }
        }
            
        insert tsraList;
       
        test.startTest();
        System.runAs(testUsr){
            delete tsraList;
        }
        
        //Map<ID, User> userMap = new Map<ID,User>([SELECT ID, LastName, Alias from User where Alias != 'TestUser']);
        test.stopTest();
        /* Aman Comment Out
        List<ContactShare> cShareList = new List<ContactShare>();
        cShareList = [SELECT UserorGroupId from ContactShare Where ContactAccessLevel = 'Read'];
        for(ContactShare cShare : cShareList){
            //System.assertEquals(userMap.containsKey(cShare.UserorGroupId), false, 'error');
 
        }*/
    }
}