/*
    @author: Daniel Quismorio
    @date: 27 Jan 2015
    @description: Territory Trigger Handler

*/
public class OSM_TerritoryTriggerHandler {
    
    
    /*
    @author: Daniel Quismorio
    @date: 27 January 2015
    @param: 
    @description: After Insert Method
    */
    public static void OnAfterInsert(List<OSM_Territory__c> newMap, Map<ID, OSM_Territory__c> oldMap){  
        
        List<OSM_Territory__c> territoryToLink = new List<OSM_Territory__c>();
        for(OSM_Territory__c loopTerritory : newMap){
            if(loopTerritory.Partner_Account__c != null){
                territoryToLink.add(loopTerritory);
            }
        }
        
        if(territoryToLink.size() > 0){
            realignPartnerAccount(territoryToLink);
        }
        
    }
    
    /*
    @author: Daniel Quismorio
    @date: 27 January 2015
    @param: 
    @description: After Update Method
    */
    public static void OnAfterUpdate(List<OSM_Territory__c> newTerritory, Map<ID, OSM_Territory__c> oldMap){
        
        List<OSM_Territory__c> territoryToLink = new List<OSM_Territory__c>();
        List<OSM_Territory__c> territoryCreate = new List<OSM_Territory__c>();
        List<OSM_Territory__c> territoryDelete = new List<OSM_Territory__c>(); 
        Set<Id> accTerritoryToRemove = new  Set<Id>();
        Set<Id> TerritoryToRemove = new  Set<Id>();
        for(OSM_Territory__c loopTerritory : newTerritory){
            if(oldMap.get(loopTerritory.Id).OSM_Parent_Territory__c != loopTerritory.OSM_Parent_Territory__c && loopTerritory.OSM_Parent_Territory__c != null ){
                territoryCreate.add(loopTerritory);
            }
            if(oldMap.get(loopTerritory.Id).OSM_Parent_Territory__c != null && loopTerritory.OSM_Parent_Territory__c == null ){
                territoryDelete.add(oldMap.get(loopTerritory.Id));
            }  
            if(loopTerritory.Partner_Account__c != null && loopTerritory.Partner_Account__c != oldMap.get(loopTerritory.Id).Partner_Account__c){
                territoryToLink.add(loopTerritory);
            }
            if(oldMap.get(loopTerritory.Id).Partner_Account__c != null && loopTerritory.Partner_Account__c == null ){
               accTerritoryToRemove.add(oldMap.get(loopTerritory.Id).Partner_Account__c); 
               TerritoryToRemove.add(loopTerritory.Id);
            }
            

        }
        if(territoryCreate.size() > 0){
            createShareRecords(territoryCreate);
        } 
        System.debug('**************territoryDeleteData' + territoryDelete);
        
        if(territoryDelete.size() > 0){
            removeShareRecords(territoryDelete);
        }
        system.debug('****accTerritoryToRemove' + accTerritoryToRemove);
        List<OSM_Account_Territory_Assignment__c> territoryLost = [Select Id From OSM_Account_Territory_Assignment__c Where OSM_Account_Name__c IN: accTerritoryToRemove And OSM_Territory_Name__c IN: TerritoryToRemove];
        if(territoryLost.size() > 0){
            delete territoryLost;
        }
       
  
        if(territoryToLink.size() > 0){
            realignPartnerAccount(territoryToLink);
        }
        
  
    }
      
    public static void OnBeforeUpdate(List<OSM_Territory__c> newTerritory, Map<ID, OSM_Territory__c> oldMap){
   
    }
    
     /*
        @author: Patrick Lorilla
        @date: 6 February 2015
        @param: addTerritory - territory list
        @description: Create Share Records for parent territory
    */ 
    
    public static void createShareRecords(List<OSM_Territory__c> addTerritory){
        Set<Id> terrIdsadd = new Set<Id>();
        Map<Id, Set<Id>> childGroupMap = getChildRecords('OSM_Parent_Territory__c', 'OSM_Territory__c');
        Map<Id, Map<Id,Id>> territoryChildMap = new Map<Id, Map<Id,Id>>();
        
         for(OSM_Territory__c tLoop : addTerritory){
            if(!territoryChildMap.containsKey(tLoop.Id)){
                territoryChildMap.put(tLoop.Id, new Map<Id,Id>());
            }
            if(childGroupMap.containsKey(tLoop.Id)){
                if(childGroupMap.get(tLoop.Id) != null){
                    String idStr = '';
                    Map <Id,Id> childTerrMap = new Map<Id,Id>();
                    for(Id chid: childGroupMap.get(tLoop.Id)){
                        childTerrMap.put(chid, null);
                    }
                    territoryChildMap.put(tLoop.Id,childTerrMap);
                } 
            }
        }
        
        Set<ID> childTerrIds = new Set<ID>();
        System.debug('\n\n\n CHILD RECORD EXTRACT');
        for(Id loopId : territoryChildMap.keySet()){
            System.debug('\n\nTerritory: ' + loopId);
            System.debug('\nChild Territories: ' + territoryChildMap.get(loopId));
            Map<Id,Id> childMap = territoryChildMap.get(loopId);
            for(Id cid: childMap.keySet()){
                childTerrIds.add(cid);
            }
            
        }
         Map<Id, OSM_Territory__c> childTerrMap = new Map<Id, OSM_Territory__c>([SELECT Id, OSM_Parent_Territory__c from OSM_Territory__c where Id IN: childTerrIds]);
        for(Id loopId : territoryChildMap.keySet()){
            for(Id loopId2: territoryChildMap.get(loopId).keySet()){
                Id pid = childTerrMap.get(loopId2).OSM_Parent_Territory__c; 
                territoryChildMap.get(loopId).put(loopId2,pid );    
            }
        }
       
        System.debug('\n\n\n TERRCHILDMAPPING' +territoryChildMap);
        
        Map<Id, Id> territoryLTMap = new Map<Id, Id>();
        for(Id loopId : territoryChildMap.keySet()){
            boolean finished = false;
            
            ID principal = null;
            if(territoryChildMap.get(loopId) != null && !territoryChildMap.get(loopId).isEmpty()){
                    do{
                        if(principal == null){
                            
                            for(Id loopId2 : territoryChildMap.get(loopId).keySet()){
                                if(loopId == territoryChildMap.get(loopId).get(loopId2)){
                                    principal = loopId2; 
                                }
                            }
                            if(!territoryChildMap.containsKey(loopId)){
                                finished = true;
                                territoryLTMap.put(loopId, loopId);        
                            }
                        }
                        else{
                            System.debug('\n\n\n CHKPT 1');
                            Integer ctr=0;
                            for(Id loopId2 : territoryChildMap.get(loopId).keySet()){
                                System.debug('\n\n\n CHKPT 2');
                                if(principal == territoryChildMap.get(loopId).get(loopId2)){
                                    principal = loopId2;
                                    ctr++;
                                }
                            }    
                            
                            if(ctr ==0){
                                finished = true;
                                territoryLTMap.put(loopId, principal);        
                            }
                        }
                        
                        
                    }while(!finished);
                }else{
                    territoryLTMap.put(loopId, loopId);          
                }
        }
        System.debug('\n\n\n TERRITORY TO LATEST CHILD MAP' + territoryLTMap ) ;  
        
        for(Id tLoopId : territoryLTMap.values()){
            terrIdsadd.add(tLoopId);
        }
        //Retrieve territory assignment records
        System.debug('**************updateShareRecordsEntry');
       
        List<OSM_Account_Territory_Assignment__c> addacctList = [Select Id, OSM_Territory_Name__c, OSM_Account_Name__c From OSM_Account_Territory_Assignment__c Where OSM_Territory_Name__c IN: terrIdsadd];
        List<OSM_HCP_Territory_Assignment__c> addhcpList = [Select Id, OSM_Territory_Name__c, OSM_HCP_Name__c From OSM_HCP_Territory_Assignment__c Where OSM_Territory_Name__c IN: terrIdsadd];
        List<OSM_Order_Territory_Assignment__c> addordrList = [Select Id, OSM_Territory_Name__c, OSM_OrderName__c From OSM_Order_Territory_Assignment__c Where OSM_Territory_Name__c IN: terrIdsadd];
        
        //Create share record using territory assignment records
        if(addordrList.size() > 0){
            OSM_OrderTerritoryAssignTriggerHandler.createOrderShare(addordrList,terrIdsadd);
        }
        if(addacctList.size() > 0){
            OSM_AccountTerritoryAssignTriggerHandler.createAccountShare(addacctList,terrIdsadd);
        }
        if(addhcpList.size() > 0){
            OSM_HCPTerritoryAssignmentTriggerHandler.createContactShare(addhcpList,terrIdsadd);
        }
    
    }
    


     /*
        @author: Patrick Lorilla
        @date: 6 February 2015
        @param: terrIdsdel - territory id set
        @description: Remove Share Records for parent territory
    */  
    public static void removeShareRecords(List<OSM_Territory__c> terrIdsdel){
        Set<Id> tdID= new Set<Id>();
        Set<Id> childIds = new Set<Id>();
        Map<Id, Set<Id>> territoryParentMap = new Map<Id, Set<Id>>();
        Map<Id, Map<Id,Id>> territoryChildMap = new Map<Id, Map<Id,Id>>();
        Map<Id, Set<Id>> parentGroupMap = getParentRecords('OSM_Parent_Territory__c', 'OSM_Territory__c');
        Map<Id, Set<Id>> childGroupMap = getChildRecords('OSM_Parent_Territory__c', 'OSM_Territory__c');
        String userString = '';
        for(OSM_Territory__c tLoop : terrIdsdel){
            territoryParentMap.put(tLoop.Id, new Set<Id>{tLoop.OSM_Parent_Territory__c});
            if(parentGroupMap.containsKey(tLoop.OSM_Parent_Territory__c)){
                if(parentGroupMap.get(tLoop.OSM_Parent_Territory__c) != null){
                    territoryParentMap.get(tLoop.Id).addAll(parentGroupMap.get(tLoop.OSM_Parent_Territory__c));
                } 
            }
        }
        
        for(OSM_Territory__c tLoop : terrIdsdel){
            if(!territoryChildMap.containsKey(tLoop.Id)){
                territoryChildMap.put(tLoop.Id, new Map<Id,Id>());
            }
            if(childGroupMap.containsKey(tLoop.Id)){
                if(childGroupMap.get(tLoop.Id) != null){
                    String idStr = '';
                    Map <Id,Id> childTerrMap = new Map<Id,Id>();
                    for(Id chid: childGroupMap.get(tLoop.Id)){
                        childTerrMap.put(chid, null);
                    }
                    territoryChildMap.put(tLoop.Id,childTerrMap);
                } 
            }
        }
        System.debug('\n\n\n CHILDGRPMAP' + childGroupMap);
        Set<ID> parentTerrIds = new Set<ID>();
        System.debug('\n\n\n PARENT RECORD EXTRACT');
        for(Id loopId : territoryParentMap.keySet()){
            System.debug('\n\nTerritory: ' + loopId);
            System.debug('\nParent Territories: ' + territoryParentMap.get(loopId));
            for(Id pid: territoryParentMap.get(loopId)){
                parentTerrIds.add(pid);
            }
        }
        
        List<OSM_Territory_Sales_Rep_Assignment__c> tsrpaList = [SELECT Id, OSM_Sales_Rep__c, OSM_Territory_ID__c from OSM_Territory_Sales_Rep_Assignment__c where OSM_Territory_ID__c IN:  parentTerrIds];
        Map<Id, Set<Id>> terrUsrMap = new Map<Id, Set<Id>>();
        for(Id loopId : territoryParentMap.keySet()){
            Set<ID> srIDS = new Set<ID>();
            for(Id pid: territoryParentMap.get(loopId)){
                 for(OSM_Territory_Sales_Rep_Assignment__c  tsrp: tsrpaList){
                     if(tsrp.OSM_Territory_ID__c == pid){
                        srIdS.add(tsrp.OSM_Sales_Rep__c); 
                        
                     }
                     
                 }
            }
            
            terrUsrMap.put(loopId,srIDS);
        }

        Set<ID> childTerrIds = new Set<ID>();
        System.debug('\n\n\n CHILD RECORD EXTRACT');
        for(Id loopId : territoryChildMap.keySet()){
            System.debug('\n\nTerritory: ' + loopId);
            System.debug('\nChild Territories: ' + territoryChildMap.get(loopId));
            Map<Id,Id> childMap = territoryChildMap.get(loopId);
            for(Id cid: childMap.keySet()){
                childTerrIds.add(cid);
            }
            
        }
        
        List<OSM_Territory_Sales_Rep_Assignment__c> tsrpaListChild = [SELECT Id, OSM_Sales_Rep__c, OSM_Territory_ID__c from OSM_Territory_Sales_Rep_Assignment__c where OSM_Territory_ID__c IN:  childTerrIds];
        Map<Id, Set<Id>> terrUsrMapChild = new Map<Id, Set<Id>>();
        for(Id loopId : territoryChildMap.keySet()){
            Set<ID> srIDS = new Set<ID>();
            for(Id cid: territoryChildMap.get(loopId).keySet()){
                for(OSM_Territory_Sales_Rep_Assignment__c  tsrp: tsrpaListChild){
                    if(tsrp.OSM_Territory_ID__c == cid){
                        srIdS.add(tsrp.OSM_Sales_Rep__c); 
                    }
                }
            }
            
            terrUsrMapChild.put(loopId,srIDS);
        }
        
        Map<Id, OSM_Territory__c> childTerrMap = new Map<Id, OSM_Territory__c>([SELECT Id, OSM_Parent_Territory__c from OSM_Territory__c where Id IN: childTerrIds]);
        for(Id loopId : territoryChildMap.keySet()){
            for(Id loopId2: territoryChildMap.get(loopId).keySet()){
                Id pid = childTerrMap.get(loopId2).OSM_Parent_Territory__c; 
                territoryChildMap.get(loopId).put(loopId2,pid );    
            }
        }
       
        System.debug('\n\n\n TERRCHILDMAPPING' +territoryChildMap);
       
        Map<Id, Id> territoryLTMap = new Map<Id, Id>();
        for(Id loopId : territoryChildMap.keySet()){
            boolean finished = false;
            
            ID principal = null;
            if(territoryChildMap.get(loopId) != null && !territoryChildMap.get(loopId).isEmpty()){
                    do{
                        if(principal == null){
                            
                            for(Id loopId2 : territoryChildMap.get(loopId).keySet()){
                                if(loopId == territoryChildMap.get(loopId).get(loopId2)){
                                    principal = loopId2; 
                                }
                            }
                            if(!territoryChildMap.containsKey(loopId)){
                                finished = true;
                                territoryLTMap.put(loopId, loopId);        
                            }
                        }
                        else{
                            System.debug('\n\n\n CHKPT 1');
                            Integer ctr=0;
                            for(Id loopId2 : territoryChildMap.get(loopId).keySet()){
                                System.debug('\n\n\n CHKPT 2');
                                if(principal == territoryChildMap.get(loopId).get(loopId2)){
                                    principal = loopId2;
                                    ctr++;
                                }
                            }    
                            
                            if(ctr ==0){
                                finished = true;
                                territoryLTMap.put(loopId, principal);        
                            }
                        }
                        
                        
                    }while(!finished);
                }else{
                    territoryLTMap.put(loopId, loopId);          
                }
        }
        System.debug('\n\n\n TERRITORY TO LATEST CHILD MAP' + territoryLTMap ) ;    
        
        //RETRIEVE ACCOUNTS
        Map<Id, OSM_Account_Territory_Assignment__c> terrATAMap = new Map<Id, OSM_Account_Territory_Assignment__c>([SELECT OSM_Territory_Name__c, OSM_Account_Name__c from OSM_Account_Territory_Assignment__c where OSM_Territory_Name__c IN: territoryLTMap.values()]);
        Map<Id, Set<Id>> territoryAcctMap = new Map<Id, Set<Id>>();
        Set<Id> territoryAcctIds = new Set<Id>();
        Set<Id> terrActsIDS = new Set<Id>();
        for(Id loopId : territoryLTMap.keySet()){
            Set<ID> acctsIDS = new Set<ID>();
            for(OSM_Account_Territory_Assignment__c ata: terrATAMap.values()){
                if(territoryLTMap.get(loopId) == ata.OSM_Territory_Name__c){
                    acctsIDS.add(ata.OSM_Account_Name__c);        
                } 
             
            }  
            if(!acctsIDS.isEmpty()){
                territoryAcctMap.put(loopId, acctsIDS);
                terrActsIDS.addAll(acctsIDS);
            }
        }
        
        System.debug('\n\n\n TERRITORY TO ACCOUNT MAP' + territoryAcctMap ) ;    
        System.debug('\n\n\n TERRITORY TO USERS' + territoryLTMap ) ;   
       
        List<AccountShare> asList = [SELECT ID, OpportunityAccessLevel, UserOrGroupId, AccountAccessLevel, AccountId from AccountShare where RowCause = 'Manual' AND  AccountId IN: terrActsIDS];
        //RETREIVE CONTACTS
        Map<Id, OSM_HCP_Territory_Assignment__c> terrHTAMap = new Map<Id, OSM_HCP_Territory_Assignment__c>([SELECT OSM_Territory_Name__c, OSM_HCP_Name__c from OSM_HCP_Territory_Assignment__c where OSM_Territory_Name__c IN: territoryLTMap.values()]);
        Map<Id, Set<Id>> territoryHCPMap = new Map<Id, Set<Id>>();
        Set<Id> territoryHCPIds = new Set<Id>();
        Set<Id> terrHCPIDS = new Set<Id>();
        for(Id loopId : territoryLTMap.keySet()){
            Set<ID> hcpIDS = new Set<ID>();
            for(OSM_HCP_Territory_Assignment__c hta: terrHTAMap.values()){
                if(territoryLTMap.get(loopId) == hta.OSM_Territory_Name__c){
                    hcpIDS.add(hta.OSM_HCP_Name__c);        
                } 
             
            }  
            if(!hcpIDS.isEmpty()){
                territoryHCPMap.put(loopId, hcpIDS);
                terrHCPIDS.addAll(hcpIDS);
            }
        }
        
        System.debug('\n\n\n TERRITORY TO CONTACT MAP' + territoryHCPMap ) ;    
 
        List<ContactShare> csList = [SELECT ID, UserOrGroupId, ContactAccessLevel, ContactId from ContactShare where RowCause = 'Manual' AND ContactId IN: terrHCPIDS];

        //RETREIVE ORDER
        Map<Id, OSM_Order_Territory_Assignment__c> terrOTAMap = new Map<Id, OSM_Order_Territory_Assignment__c>([SELECT OSM_OrderName__c, OSM_Territory_Name__c from OSM_Order_Territory_Assignment__c where OSM_Territory_Name__c IN: territoryLTMap.values()]);
        Map<Id, Set<Id>> territoryOrderMap = new Map<Id, Set<Id>>();
        Set<Id> territoryOrderIds = new Set<Id>();
        Set<Id> terrOrderIDS = new Set<Id>();
        for(Id loopId : territoryLTMap.keySet()){
            Set<ID> orderIDS = new Set<ID>();
            for(OSM_Order_Territory_Assignment__c ota: terrOTAMap.values()){
                if(territoryLTMap.get(loopId) == ota.OSM_Territory_Name__c){
                    orderIDS.add(ota.OSM_OrderName__c);        
                } 
             
            }  
            if(!orderIDS.isEmpty()){
                territoryOrderMap.put(loopId, orderIDS);
                terrOrderIDS.addAll(orderIDS);
            }
        }
       
        System.debug('\n\n\n TERRITORY TO ORDER MAP' + territoryOrderMap ) ;    
        //RECREATION
        List<OrderShare> osList = [SELECT ID, UserOrGroupId, OrderAccessLevel, OrderId from OrderShare where RowCause = 'Manual' AND OrderId IN: terrOrderIDS];
        if(!osList.isEmpty()){
            delete osList;
        }
        
        if(!csList.isEmpty()){
            delete csList;
        }
        
        if(!asList.isEmpty()){
            delete asList;
        }
         
         List<OSM_Territory__c> latestTerritory = [SELECT Id, OSM_Parent_Territory__c from OSM_Territory__c where Id IN:  territoryLTMap.values()];
         if(!latestTerritory.isEmpty()){
            createShareRecords(latestTerritory);
         }
    }
   
    /**
     * @author         Kristian Vegerano
     * @description    Method retrieves all parent records of the territory.
     * @history        03.DEC.2014 - Kristian Vegerano - Created  
     */
    public static Map<Id, Set<Id>> getParentRecords(String parentField, String sObjectAPIName){
        Map<Id, Set<Id>> childGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();
        
        String queryString = 'SELECT Id,' + parentField + ' FROM ' + sObjectAPIName + ' LIMIT 20000';
        for(sObject loopObject : Database.query(queryString)){
            childParentMap.put((Id)loopObject.get('Id'),(Id)loopObject.get(parentField));
            parentChildMap.put((Id)loopObject.get(parentField),(Id)loopObject.get('Id'));
            if(!childGroupMap.containsKey((Id)loopObject.get('Id'))){
                childGroupMap.put((Id)loopObject.get('Id'), new Set<Id>{(Id)loopObject.get(parentField)});
            }else{
                childGroupMap.get((Id)loopObject.get('Id')).add((Id)loopObject.get(parentField));
            }
        }
        for(Integer counter = 0; counter < 50; counter++){
           for(Id loopChildId : childGroupMap.keySet()){
               for(Id loopParentId : childGroupMap.get(loopChildId)){
                   if(childParentMap.get(loopParentId) != null){
                        childGroupMap.get(loopChildId).add(childParentMap.get(loopParentId));
                   }
                }
             }
        }
        
        return childGroupMap;
    }
    
     /**
     * @author         Kristian Vegerano
     * @description    Method retrieves all child records of the order.
     * @history        03.DEC.2014 - Kristian Vegerano - Created  
     */
    public static Map<Id, Set<Id>> getChildRecords(String parentField, String sObjectAPIName){
        Map<Id, Set<Id>> parentGroupMap = new Map<Id,Set<Id>>();
        Map<Id,Id> childParentMap = new Map<Id,Id>();
        Map<Id,Id> parentChildMap = new Map<Id,Id>();
        
        String queryString = 'SELECT Id,' + parentField + ' FROM ' + sObjectAPIName + ' LIMIT 20000';
        for(sObject loopObject : Database.query(queryString)){
            childParentMap.put((Id)loopObject.get('Id'), (Id)loopObject.get(parentField));
            parentChildMap.put((Id)loopObject.get(parentField), (Id)loopObject.get('Id'));
            if(!parentGroupMap.containsKey((Id)loopObject.get(parentField))){
                parentGroupMap.put((Id)loopObject.get(parentField), new Set<Id>{(Id)loopObject.get('Id')});
            }else{
                parentGroupMap.get((Id)loopObject.get(parentField)).add((Id)loopObject.get('Id'));
            }
        }
        
        for(Integer counter = 0; counter < 25; counter++){
            for(Id loopParentId : parentGroupMap.keySet()){
                for(Id loopChildId : parentGroupMap.get(loopParentId)){
                    if(parentChildMap.get(loopChildId) != null){
                        parentGroupMap.get(loopParentId).add(parentChildMap.get(loopChildId));
                    }
                }
            }
        }
        
        return parentGroupMap;
    }
    
    /*
    @author: Daniel Quismorio
    @date: 27 January 2015
    @param: 
    @description: After Insert Method
    */
    public static void OnBeforeDelete(List<OSM_Territory__c> oldTerritory){  
        
        Set<Id> terrIds = new Set<Id>();
        for(OSM_Territory__c tLoop : oldTerritory){
            terrIds.add(tLoop.Id);
        }
        
        system.debug('***terrIds' + terrIds);
        
        List<OSM_Account_Territory_Assignment__c> acctList = [Select Id From OSM_Account_Territory_Assignment__c Where OSM_Territory_Name__c IN: terrIds];
        List<OSM_HCP_Territory_Assignment__c> hcpList = [Select Id From OSM_HCP_Territory_Assignment__c Where OSM_Territory_Name__c IN: terrIds];
        List<OSM_Order_Territory_Assignment__c> ordrList = [Select Id From OSM_Order_Territory_Assignment__c Where OSM_Territory_Name__c IN: terrIds];
        List<OSM_Territory__c> terrList = [Select Id from OSM_Territory__c where OSM_Parent_Territory__c IN: terrIds];
        
        for(OSM_Territory__c tl: terrList){
            tl.OSM_Parent_Territory__c = null;    
        }
        
        if(terrList.size()>0){
            update terrList;
        }
        if(acctList.size() > 0){
            delete acctList;
        }
        if(hcpList.size() > 0){
            delete hcpList;
        }
        if(ordrList.size() > 0){
            delete ordrList;
        }
        
    }
    
    public static void realignPartnerAccount(List<OSM_Territory__c> newT){
        
        Map<Id, Id> fieldMapPartnerAccTerr = new Map<Id, Id>();
        for(OSM_Territory__c tLoop : newT){
            fieldMapPartnerAccTerr.put(tLoop.Partner_Account__c, tLoop.Id);   
        }
        
        List<OSM_Account_Territory_Assignment__c> ATAListInsert = new List<OSM_Account_Territory_Assignment__c>();
        for(Id accIdLoop : fieldMapPartnerAccTerr.keySet()){
            OSM_Account_Territory_Assignment__c newATA = new OSM_Account_Territory_Assignment__c();
            newATA.OSM_Account_Name__c = accIdLoop;
            newATA.OSM_Territory_Name__c = fieldMapPartnerAccTerr.get(accIdLoop);
            
            ATAListInsert.add(newATA);
        }
        
        if(ATAListInsert.size()>0){
            insert ATAListInsert;
        }
    }
    
}