/*
      @author: David Catindoy
      @date: 28 JAN 2015 - Created
      @description: Territory Trigger Handler Test Class
      @history: 28 JAN 2015 - Updated (PL)
      23-JUL-2015           Mark Cagurong            Added additional covering unit tests

*/
@isTest
private class OSM_TerritoryTriggerHandler_Test {    
  
    static testmethod void TerritoryTestMethod(){

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Order_Territory_Assignment_Trigger__c = false;
        triggerSwitch.HCP_Territory_Management_Trigger__c = false;
        insert triggerSwitch;

        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id PartnerRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();

        Account account = OSM_DataFactory.createAccount('New Account 1');
        account.RecordTypeId = PartnerRecType;
        insert account;

        Account account1 = OSM_DataFactory.createAccount('New Account 2');
        account1.RecordTypeId = PartnerRecType;
        insert account1;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Contact contact1 = OSM_DataFactory.createContact('David', 'Catindoy', HCPRecType);
        contact1.AccountId = account.Id;
        insert contact1;

        Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        insert order;

        OSM_Territory__c territory = OSM_DataFactory.createTerritory('New Territory');
        territory.Partner_Account__c = account.Id;

        Test.startTest();

        insert territory;
        territory.Partner_Account__c = account1.Id;
        update territory;

        territory.Partner_Account__c = null;
        update territory;

        territory.Partner_Account__c = account.Id;
        update territory;
        
        OSM_HCP_Territory_Assignment__c hcpTA = OSM_DataFactory.createHTA(contact1.Id, territory.Id);
        insert hcpTA;
        OSM_Order_Territory_Assignment__c orderTA = new OSM_Order_Territory_Assignment__c(OSM_OrderName__c = order.Id, OSM_Territory_Name__c = territory.Id);
        insert orderTA;
        List<OSM_Territory__c> parentTerritoryList = new List<OSM_Territory__c>();
        List<OSM_Territory__c> territoryList = new List<OSM_Territory__c>();
        List<OSM_Order_Territory_Assignment__c> otaList = new List<OSM_Order_Territory_Assignment__c>();
        List<Order> orderList = new List<Order>();
        OSM_Territory__c territor1 = OSM_DataFactory.createTerritory('Territory Number1');
        parentTerritoryList.add(territor1);
        OSM_Territory__c territor3 = OSM_DataFactory.createTerritory('Territory Number3');
        insert parentTerritoryList;
        OSM_Territory__c territor2 = OSM_DataFactory.createTerritory('Territory Number2');
        territor2.OSM_Parent_Territory__c = territor1.Id;
        territoryList.add(territor2);
        insert territoryList;
        territoryList.clear();
        territor2.OSM_Parent_Territory__c = territor3.Id;
        territoryList.add(territor2);
        update territoryList;
        
        delete territory;
        Test.stopTest();
        System.assert(territory.Partner_Account__c == account.Id);
        
        
    }
    @testSetup static void sharingRuleTestMethod() {
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id PartnerRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();

       
        List<Account> acctList = new List<Account>();
        for(Integer a=0; a<20; a++){
            Account account = OSM_DataFactory.createAccount('New Account'+a);
            account.RecordTypeId = PartnerRecType;
            acctList.add(account);        
        }     
        
        insert acctList;
       

        //Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact.Id);
        //contact.AccountId = account.Id;
        //insert contact;

     

        //Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        //insert order1;
        
        //Order order2 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        //insert order2;
        
        
        //Prepare youngest child territory
        List<OSM_Territory__c> yterrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<20; a++){
            yterrList.add(OSM_DataFactory.createTerritory('Terrtesty'));   
            
        }
        insert yterrList; 
        
        //Prepare territory
        List<OSM_Territory__c> oterrList = new List<OSM_Territory__c>();
        for(Integer a=0; a<60; a++){
            oterrList.add(OSM_DataFactory.createTerritory('Terrtest'));
        }
        
        insert oterrList;
        
        //Prepare Users
        List<User> shUsers = new List<User>();
        for(Integer a=0; a<60; a++){
            shUsers.add(OSM_DataFactory.createUser('lname'+a));
        }
        
        insert shUsers;
        
        
        //Prepare TSRA Account Access
        List<OSM_Territory_Sales_Rep_Assignment__c> tsraList = new List<OSM_Territory_Sales_Rep_Assignment__c>();
        for(Integer a=0; a<60; a++){
            tsraList.add(OSM_DataFactory.createTerritorySRA(shUsers[a].Id, oterrList[a].Id, 'Edit', null));
        }
        
        insert tsraList;
        
        //Prepare ATA
        List<OSM_Account_Territory_Assignment__c> ataList = new List<OSM_Account_Territory_Assignment__c>();
        List<OSM_Territory__c> oterrModify = new List<OSM_Territory__c>();
        for(Integer a=0; a<20; a++){
            ataList.add(OSM_DataFactory.createATA(acctList[a].Id,oterrList[0].Id));
        }
        
        insert ataList;
        /*
        for(Integer a=0; a<20; a++){
            if((a+1) <20){
                oterrList[a].OSM_Parent_Territory__c = oterrList[a+1].Id;
            }
            
        }
        update oterrList;*/
        Integer ctr = 0;
        for(OSM_Territory__c yt: yterrList){
            for(Integer a=0; a<2; a++){
                if(a==0){
                    yt.OSM_Parent_Territory__c = oterrList[a+ctr].Id;
                    oterrList[a+ctr].OSM_Parent_Territory__c =  oterrList[a+ctr+1].Id;   
                }
                    
            }
            ctr++;
        }
        
        update yterrList;    
    }
    static testmethod void TerritoryShareTestMethod1(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Order_Territory_Assignment_Trigger__c = false;
        triggerSwitch.HCP_Territory_Management_Trigger__c = false;
        insert triggerSwitch;

       
        List<OSM_Territory__c> yTerrList = [SELECT ID, OSM_Parent_Territory__c from OSM_Territory__c where Name = 'Terrtesty'];
        List<OSM_Territory__c> oTerrList = [SELECT ID, OSM_Parent_Territory__c from OSM_Territory__c where Name = 'Terrtest'];
        
        for(Integer a=0; a<oTerrList.size(); a++){
           
            for(OSM_Territory__c yt: yterrList){
                if(yt.OSM_Parent_Territory__c == oTerrList[a].Id){
                        yt.OSM_Parent_Territory__c = null;
                }
            }
            
        }
        update yTerrList;
    }
    
    
   
    static testmethod void TerritoryShareTestMethod2(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Order_Territory_Assignment_Trigger__c = false;
        triggerSwitch.HCP_Territory_Management_Trigger__c = false;
        insert triggerSwitch;

       
        List<OSM_Territory__c> yTerrList = [SELECT ID, OSM_Parent_Territory__c from OSM_Territory__c where Name = 'Terrtesty'];
         List<OSM_Territory__c> oterrList = [SELECT ID, OSM_Parent_Territory__c from OSM_Territory__c where Name = 'Terrtest'];
        Integer ctr = 0;
        
        for(OSM_Territory__c yt: yterrList){
            for(Integer a=0; a<2; a++){
                if(a==0){
                yt.OSM_Parent_Territory__c = null;
                yt.OSM_Parent_Territory__c = oterrList[a+ctr].Id;
                }
            }
            ctr++;
        }
        
        update yterrList;
    }
    
    static testmethod void TerritoryTestMethod2(){

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Order_Territory_Assignment_Trigger__c = false;
        triggerSwitch.HCP_Territory_Management_Trigger__c = false;
        insert triggerSwitch;

        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id PartnerRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
       
        List<Account> account = new List<Account>();
        for(Integer a=0; a<10; a++){
            account.add(OSM_DataFactory.createAccount('New Account 1'));
            account[a].RecordTypeId = PartnerRecType;
        }
        insert account;

        List<Contact> contact = new List<Contact>();
        for(Integer c=0; c<10; c++){
                contact.add(OSM_DataFactory.createContact('jats', 'matres', null));
                contact[c].AccountId = account[c].Id;
            if(c<5){
                contact[c].RecordTypeId = rtContact;
            } else {
                contact[c].RecordTypeId = HCPRecType;
            }
        }
        insert contact;


        List<Order> order = new List<Order>();
        for(Integer o=0; o<5; o++){
            order.add(OSM_DataFactory.createOrder('New Order', contact[o].Id, account[o].Id, System.Today(), 'Active'));   
        }
        insert order;

        List<OSM_Territory__c> territory = new List<OSM_Territory__c>();
        for(Integer a=0; a<10; a++){
            territory.add(OSM_DataFactory.createTerritory('New Territory'));
            territory[a].Partner_Account__c = account[a].Id;
        }
        //insert territory;

        System.debug('Tero: '+ territory);

        Test.startTest();
        insert territory;
        for(Integer b=0; b<10; b++){
            if(b<3){
                territory[b].Partner_Account__c = account[9-b].Id;
            } else if (b>3 && b<7){
                territory[b].Partner_Account__c = null;
            //} else {
            //    territory[b].Partner_Account__c = account[9-b].Id;
            }
            
        }
        update territory;

        System.debug('tero1: '+ territory);
        OSM_Order_Territory_Assignment__c orderTA = new OSM_Order_Territory_Assignment__c(OSM_OrderName__c = order[0].Id, OSM_Territory_Name__c = territory[0].Id);
        insert orderTA;

        delete territory;
        Test.stopTest();
        System.assert(territory[0].Partner_Account__c == account[9].Id);
    }

     private static final String TERRITORY_BATCH_MARKER = 'TerritoryBatch01';
     private static final String TERRITORY_PARENT_IDENTIFIER = 'TerParent01';
     private static final String TERRITORY_TEST_IDENTIFIER = 'Territory01';      

     /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering unit test to set the parent category after territory was previously created
        History
            <date>                <author>                <description>
            27-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
     static testmethod void updateTerritory_assignParent(){

        setupDefaultData(false);

        OSM_Territory__c parentTerritory = GetTerritory(TERRITORY_PARENT_IDENTIFIER);
        OSM_Territory__c territory = GetTerritory(TERRITORY_TEST_IDENTIFIER);

        Test.startTest();        

        territory.OSM_Parent_Territory__c = parentTerritory.Id;

        update territory;

        /*
        system.debug('**territory->' + JSON.serialize(territory));

        List<OSM_Territory__c> trys = [SELECT Id, Name, OSM_Parent_Territory__c, OSM_Parent_Territory__r.Name FROM OSM_Territory__c WHERE OSM_Territory_ID__c = :TERRITORY_BATCH_MARKER];
        system.debug('Territories.size: ' + trys.size());
        system.debug('Territoriesxx: ' + JSON.serialize(trys));

        Map<Id, Set<Id>> childRecords = OSM_TerritoryTriggerHandler.getChildRecords('OSM_Parent_Territory__c', 'OSM_Territory__c');
        system.debug('childRecords: ' + JSON.serialize(childRecords));
        */

        Test.stopTest();

        // rehydate from DB
        territory = GetTerritory(TERRITORY_TEST_IDENTIFIER);

        System.assertEquals(parentTerritory.Id, territory.OSM_Parent_Territory__c);
    }


    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering unit test to clear the parent territory
        History
            <date>                <author>                <description>
            27-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    static testmethod void updateTerritory_ClearParent(){

        setupDefaultData(true);

        OSM_Territory__c parentTerritory = GetTerritory(TERRITORY_PARENT_IDENTIFIER);
        OSM_Territory__c territory = GetTerritory(TERRITORY_TEST_IDENTIFIER);

        Test.startTest();        

        territory.OSM_Parent_Territory__c = null;

        update territory;

        Test.stopTest();

        // rehydate from DB
        territory = GetTerritory(TERRITORY_TEST_IDENTIFIER);

        System.assertEquals(null, territory.OSM_Parent_Territory__c);
    }


    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering unit test to delete the territories in parent to child order
        History
            <date>                <author>                <description>
            27-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    static testmethod void deleteParentThenChildTerritory(){

        setupDefaultData(true);

        OSM_Territory__c parentTerritory = GetTerritory(TERRITORY_PARENT_IDENTIFIER);
        OSM_Territory__c territory = GetTerritory(TERRITORY_TEST_IDENTIFIER);     

        Test.startTest();        

        delete parentTerritory;

        delete territory;

        Test.stopTest();

        parentTerritory = GetTerritory(TERRITORY_PARENT_IDENTIFIER);
        territory = GetTerritory(TERRITORY_TEST_IDENTIFIER);     

        System.assertEquals(null, parentTerritory);
        System.assertEquals(null, territory);
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Covering unit test to delete the territories in child to parent order
        History
            <date>                <author>                <description>
            27-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    static testmethod void deleteChildThenParentTerritory(){

        setupDefaultData(true);

        OSM_Territory__c parentTerritory = GetTerritory(TERRITORY_PARENT_IDENTIFIER);
        OSM_Territory__c territory = GetTerritory(TERRITORY_TEST_IDENTIFIER);     

        Test.startTest();

        delete territory;

        delete parentTerritory;        

        Test.stopTest();

        parentTerritory = GetTerritory(TERRITORY_PARENT_IDENTIFIER);
        territory = GetTerritory(TERRITORY_TEST_IDENTIFIER);     

        System.assertEquals(null, territory);
        System.assertEquals(null, parentTerritory);        
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Helper method to create order default test data
        History
            <date>                <author>                <description>
            27-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    private static void setupDefaultData(Boolean autoRelateParent){

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Territory_Trigger__c = true;
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Order_Territory_Assignment_Trigger__c = false;
        triggerSwitch.HCP_Territory_Management_Trigger__c = false;
        insert triggerSwitch;

        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id PartnerRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();

        Account account = OSM_DataFactory.createAccount('New Account 1');
        account.RecordTypeId = PartnerRecType;
        insert account;

        Account account1 = OSM_DataFactory.createAccount('New Account 2');
        account1.RecordTypeId = PartnerRecType;
        insert account1;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Contact contact1 = OSM_DataFactory.createContact('David', 'Catindoy', HCPRecType);
        contact1.AccountId = account.Id;
        insert contact1;

        Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        insert order;

        OSM_Territory__c parentTerritory = OSM_DataFactory.createTerritory('Parent Territory');
        parentTerritory.OSM_Territory_ID__c = TERRITORY_PARENT_IDENTIFIER;
        parentTerritory.Partner_Account__c = account.Id;

        insert parentTerritory;

        //system.debug('parentTerritory: ' + JSON.serialize(parentTerritory));

        OSM_Territory__c territory = OSM_DataFactory.createTerritory('Territory');
        territory.OSM_Territory_ID__c = TERRITORY_TEST_IDENTIFIER;
        territory.Partner_Account__c = account.Id;
        if(autoRelateParent){
            territory.OSM_Parent_Territory__c = parentTerritory.Id;
        }

        insert territory;

        //system.debug('territory: ' + JSON.serialize(territory));

        List<OSM_Territory__c> childTerritories = new List<OSM_Territory__c>();
        for(Integer ctr=0; ctr<5; ctr++){
            OSM_Territory__c childTrty = OSM_DataFactory.createTerritory('Child Territory' + ctr);
            childTrty.OSM_Territory_ID__c = TERRITORY_BATCH_MARKER;
            childTrty.OSM_Parent_Territory__c = territory.Id;

            childTerritories.add(childTrty);
        }

        insert childTerritories;

        OSM_Territory__c lastChildTerritory = childTerritories.get(childTerritories.size() -1);

        Date startDate = Date.today().addDays(3);
        Date endDate = startDate.addDays(3);

        // account territory assignments
        //Select Id, OSM_Territory_Name__c, OSM_Account_Name__c From OSM_Account_Territory_Assignment__c Where OSM_Territory_Name__c = 'a0CK0000009Nulm'
        OSM_Account_Territory_Assignment__c accttryAssign = OSM_DataFactory.createATA(account1.Id, lastChildTerritory.Id);
        accttryAssign.OSM_Manual_Assignment__c = true;
        accttryAssign.OSM_Effective_Start_Date__c = startDate;
        accttryAssign.OSM_Effective_Ending_Date__c = endDate;

        insert accttryAssign;        

        OSM_HCP_Territory_Assignment__c hcptryAssign = OSM_DataFactory.createHTA(contact1.Id, lastChildTerritory.Id);        
        hcptryAssign.OSM_Manual_Assignment__c = true;

        insert hcptryAssign;

        OSM_Order_Territory_Assignment__c orderTA = new OSM_Order_Territory_Assignment__c(OSM_OrderName__c = order.Id, OSM_Territory_Name__c = lastChildTerritory.Id);
        orderTA.OSM_Manual_Assignment__c = true;
        orderTA.OSM_Effective_Start_Date__c = startDate;
        orderTA.OSM_Effective_Ending_Date__c = endDate;

        insert orderTA;
    }

    /******************************************************************************
        Author         : Mark Cagurong
        Description    : Helper method to retrieve the Territory by OSM_Territory_ID__c
        History
            <date>                <author>                <description>
            27-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
    private static OSM_Territory__c GetTerritory(String markerId){
        List<OSM_Territory__c> results = [SELECT Id, Name, OSM_Parent_Territory__c, OSM_Parent_Territory__r.Name FROM OSM_Territory__c WHERE OSM_Territory_ID__c = :markerId];
        if(results.size() > 0) return results.get(0);
        return null;
    }

   

}