/********************************************************************
    @author         : Rescian Rey
    @description    : Therapak ordering utility class.
    @history:
        <date>                <author>                <description>
        MAY 25 2015           Rescian Rey             Created class
********************************************************************/
public with sharing class OSM_TherapakOrder {
    @TestVisible private OSM_Therapak_Ordering_Settings__c settings;
    @TestVisible private OSM_Kit_Order__c order;
    @TestVisible private User submitter;
    @TestVisible private Map<String, String> items;

    @TestVisible private static final Map<String, String> PREPRINT_MAP = new Map<String, String>{
        'Practice Preprinted' => 'PHYS',
        'Pathology Preprinted' => 'PATH',
        'Practice and Pathology Preprinted' => 'BOTH'
    };

    /********************************************************************
        @author         : Rescian Rey
        @description    : Initialization. Fetches the correct settings based on
                            the region. Sets the order and the submitter.
        @history:
            <date>                <author>                <description>
            MAY 25 2015           Rescian Rey             Created method
    ********************************************************************/
    public OSM_TherapakOrder(OSM_Kit_Order__c order, User submitter, OSM_Therapak_Ordering_Settings__c settings) {
        this.submitter = submitter;
        this.order = order;
        this.settings = settings;
        items = new Map<String, String>();
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Adds an item to the Item Map.
        @history:
            <date>                <author>                <description>
            MAY 25 2015           Rescian Rey             Created method.
    ********************************************************************/
    public void addItem(String variable, String quantity){
        items.put(variable, quantity);
    }


    /********************************************************************
        @author         : Rescian Rey
        @description    : Adds a requisition form to the Item Map.
        @history:
            <date>                <author>                <description>
            JUNE 1 2015           Rescian Rey             Created method.
    ********************************************************************/
    public void addRequisitionLocation(String accountInfo){
        String preprinting = 'BLNK';
        if(accountInfo != null && PREPRINT_MAP.containsKey(accountInfo)){
            preprinting = PREPRINT_MAP.get(accountInfo);
        }
        items.put('PreprintingReqformLocation', preprinting);
    }

    /********************************************************************
        @author         : Rescian Rey
        @description    : Sends the Http request to Therapak. Returns whether
                            the submission is successful or not.
        @history:
            <date>                <author>                <description>
    ********************************************************************/
    public Boolean submitOrder(){
        Boolean success = false;

        // Form/Post Data to send to Therapak
        Integer count = items.size();
        String rush = order.OSM_Rush_Order__c?'Yes':'No';
        Map<String, String> parameters = new Map<String, String>{
            'RequestorName'=> submitter.Name,
            'PhoneNo'=> submitter.Phone,
            'FaxNo'=> submitter.Fax,
            'Email'=> submitter.Email,
            'RUSH'=> rush,
            'ShippingMethod'=> order.OSM_Shipping_Method__c,
            'Samples'=> 'Yes',
            'ClientAcctNo' => order.OSM_Account_Number__c,
            'ClientName' => order.OSM_Account__r.Name,
            'ClientAddress1' => order.OSM_Main_Address_Line_1__c,
            'ClientAddress2' => order.OSM_Main_Address_Line_2__c,
            'ClientCity' => order.OSM_Main_City__c,
            'ClientState' => order.OSM_Main_State__c,
            'ClientZip' => order.OSM_Main_Zip__c,
            'ClientPhone' => order.OSM_Main_Phone__c,
            'ClientFax' => order.OSM_Main_Fax__c,
            'ClientCountry' => order.OSM_Main_Country__c,
            'ShipToCompany' => order.OSM_Account__r.Name,
            'ShipToName' => order.OSM_Ship_To_Address_Contact__c,
            'ShipToAddress1' => order.OSM_Ship_To_Address_Line_1__c,
            'ShipToAddress2' => order.OSM_Ship_To_Address_Line_2__c,
            'ShipToCity' => order.OSM_Ship_To_City__c,
            'ShipToState' => order.OSM_Ship_To_State__c,
            'ShipToFax' => order.OSM_Ship_To_Fax__c,
            'ShipToZip' =>  order.OSM_Ship_To_Zip__c,
            'ShipToPhone' =>  order.OSM_Ship_To_Phone__c,
            'ShipToCountry' =>  order.OSM_Ship_To_Country__c,
            'PO' => order.Name,
            'itemcount' => String.valueOf(count)
        };

        // Form body construction
        String boundary = '__8LA4YWxkTrZl0gW__';
        String delimiter = '\n';

        String formData = '';
        for(String k: parameters.keySet()){
            String value = parameters.get(k)==null?'':parameters.get(k);

            formData += '--' + boundary + delimiter;
            formData += 'Content-Disposition: form-data; name="'+ k +'"' + delimiter;
            formData += delimiter;
            formData += value + delimiter;
        }

        // Add order items in the order request
        for(String k: items.keySet()){
            String value = items.get(k)==null?'':items.get(k);

            formData += '--' + boundary + delimiter;
            formData += 'Content-Disposition: form-data; name="'+ k +'"' + delimiter;
            formData += delimiter;
            formData += value + delimiter;
        }

        formData += '--' + boundary + '--';

        // Login to Therapak site to obtain a session ID.
        String sessionID = login();
        if(sessionID == ''){ return success; }

        // Construct the request
        HttpRequest request = new HttpRequest();
        request.setEndpoint(settings.OSM_Therapak_Post_Order__c);
        //request.setEndpoint('http://requestb.in/y6erhay6');
        request.setMethod('POST');
        request.setHeader('Cookie', sessionID);
        request.setHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
        request.setTimeout(20000);

        request.setBody(formData);
        request.setHeader('Content-Length', String.valueof(formData.length()));

        HTTPResponse response;

        try{
            Http http = new Http();
            response = http.send(request);
            System.debug(response.getBody());

            // Parse for a thank you note to ensure ordering is successful
            if(response.getBody().contains('Thank you for your order')){
                success = true;
            }
        }catch(CalloutException ex){
            if(ex.getMessage() == 'Read timed out'){
                // It means it's already in process
                success = true;
            }else{
                System.debug(ex.getMessage());
            }
        }

        return success;
    }


    /********************************************************************
        @author: Rescian Rey
        @description: Logins to the Therapak site. Returns the session ID.
        @history:
            <date>                <author>                <description>
    ********************************************************************/
    private String login(){
        HttpRequest request = new HttpRequest();
        request.setEndpoint(settings.OSM_Therapak_Login__c);
        //request.setEndpoint();
        request.setMethod('POST');

        Map<String, String> parameters = new Map<String, String>{
            'Username'=> settings.OSM_Username__c,
            'Password'=> settings.OSM_Password__c,
            'login'=> '1'
        };

        List<String> parameterList = new List<String>();
        for(String k: parameters.keySet()){
            parameterList.add(k + '=' + parameters.get(k));
        }

        request.setBody(String.join(parameterList, '&'));

        String sessionID = '';
        HTTPResponse response;

        try{
            Http http = new Http();
            response = http.send(request);
            sessionID = response.getHeader('Set-Cookie').split(';')[0];
        }catch(CalloutException ex){
            System.debug(ex.getMessage());
        }catch(NullPointerException nexp){
            System.debug(nexp.getMessage());
        }

        return sessionID;
    }
}