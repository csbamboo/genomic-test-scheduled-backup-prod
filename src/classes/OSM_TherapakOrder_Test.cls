@isTest (seeAllData = true)
private class OSM_TherapakOrder_Test {

	private static testMethod void test() {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults(); 
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Kit_Order__c = true;
        triggerSwitch.Product2__c = false;
        update triggerSwitch;
        
        Account accVENDOR_THERAPAK = OSM_DataFactory.createAccount('Therapak');
        insert accVENDOR_THERAPAK;
        
        
        OSM_Kit_Order__c kitOrder = new OSM_Kit_Order__c(OSM_Account__c = accVENDOR_THERAPAK.Id);
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Service']; 
        User u = new User(Alias = 'standt', Email='standarduser123213@testo1231rg.co213m', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser3213@testorg123.com1232', Phone = '12314124', Fax = '12312412');
        OSM_Therapak_Ordering_Settings__c settings = OSM_Therapak_Ordering_Settings__c.getOrgDefaults();
        
       
        System.runAs(u){
        test.startTest();
        OSM_TherapakOrder a = new OSM_TherapakOrder(kitOrder, u  , settings);
        a.addItem('String variable','4');
        a.addRequisitionLocation('Practice Preprinted');    
        Test.setMock(HttpCalloutMock.class, new OSM_MockHttpResponseGenerator(200,'Complete','{"apex":"test"}'));
        a.submitOrder();
        test.stopTest();
        }
        
        
        
	}
    
        
}