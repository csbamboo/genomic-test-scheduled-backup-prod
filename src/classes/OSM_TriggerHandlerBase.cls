/*
    @author: Rescian Rey
    @description: Will be inherited by Trigger Handlers
    @history:
        MAY 11 2015     Rescian Rey    Created
        MAY 12 2015     Rescian Rey    Renamed class. Prepended 'OSM_'
*/
public virtual class OSM_TriggerHandlerBase {
    public class NotImplementedException extends Exception{} 
    public virtual void MainEntry(String TriggerObject, Boolean IsBefore, Boolean IsDelete, Boolean 
                                    IsAfter, Boolean IsInsert, Boolean IsUpdate, Boolean IsExecuting, List<SObject> newlist, 
                                    Map<ID, SObject> newmap, List<SObject> oldlist, Map<ID,SObject> oldmap){
        throw new NotImplementedException('Error Not Implemented');
    }
        
    public virtual void InProgressEntry(String TriggerObject, Boolean IsBefore, Boolean IsDelete, 
                                        Boolean IsAfter, Boolean IsInsert, Boolean IsUpdate, Boolean IsExecuting, 
                                        List<SObject> newlist, Map<ID, SObject> newmap, List<SObject> oldlist, 
                                        Map<ID,SObject> oldmap){

        //By default just call the main entry of the invoking classes controller object 
        String TypeName;
        TypeName = TriggerObject + 'Handler';
        Type t = Type.forName(TypeName);
        OSM_TriggerHandlerBase activeFunction = (OSM_TriggerHandlerBase)t.newInstance();

        //Update the dispatcher active function so that it references the new object
        OSM_CentralDispatcher.activeFunction = activeFunction;
        activeFunction.MainEntry(TriggerObject, IsBefore, IsDelete, IsAfter, IsInsert, IsUpdate,
                                    IsExecuting, newlist, newmap, oldlist, oldmap);
        OSM_CentralDispatcher.activeFunction = this;
    }
}