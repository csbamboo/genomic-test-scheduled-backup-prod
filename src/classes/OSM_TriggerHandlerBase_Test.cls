/*
    @author: Mark Cagurong
    @description: Unit test for OSM_TriggerHandlerBase coverage
    @history:
        AUG 6 2015     Mark Cagurong    Created
*/
@isTest
private class OSM_TriggerHandlerBase_Test {
    
    private static final String FAKE_TRIGGER_HANDLER_TYPE = 'OSM_FaxTrigger';
    private static final Boolean FAKE_PARAMETER = false;
	private static final List<SObject> fakeNewList = new List<SObject>();
	private static final Map<Id, SObject> fakeNewMap = new Map<Id, SObject>();
    private static final List<SObject> fakeOldList = new List<SObject>();
    private static final Map<Id, SObject> fakeOldMap = new Map<Id, SObject>();

    /*
        @author: Mark Cagurong
        @description: Unit test for inProgressEntry
        @history:
            AUG 6 2015     Mark Cagurong    Created
    */
	@isTest static void inProgressEntry() {
		
		OSM_TriggerHandlerBase tBase = new OSM_TriggerHandlerBase();
		
		Test.startTest();
		
		// invoke the order trigger handler
		tBase.inProgressEntry(FAKE_TRIGGER_HANDLER_TYPE, FAKE_PARAMETER, FAKE_PARAMETER, FAKE_PARAMETER, FAKE_PARAMETER, FAKE_PARAMETER, FAKE_PARAMETER, fakeNewList, fakeNewMap, fakeOldList, fakeOldMap);
		
		Test.stopTest();
		
		system.assert(OSM_CentralDispatcher.activefunction != null);
	}
	
	/*
        @author: Mark Cagurong
        @description: Unit test for MainEntry
        @history:
            AUG 6 2015     Mark Cagurong    Created
    */
	@isTest static void MainEntry() {
	    
		OSM_TriggerHandlerBase tBase = new OSM_TriggerHandlerBase();
		
		Test.startTest();
		
		try{
		    tBase.MainEntry(FAKE_TRIGGER_HANDLER_TYPE, FAKE_PARAMETER, FAKE_PARAMETER, FAKE_PARAMETER, FAKE_PARAMETER, FAKE_PARAMETER, FAKE_PARAMETER, fakeNewList, fakeNewMap, fakeOldList, fakeOldMap);    
		}
		catch(Exception e){
		    system.assertEquals('Error Not Implemented', e.getMessage());
		}
		
		Test.stopTest();
	}
	
	
	
}