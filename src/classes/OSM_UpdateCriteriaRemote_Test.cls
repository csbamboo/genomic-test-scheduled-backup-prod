/******************************************************************************
    Author         : Rescian Rey
    Description    : Unit tests for OSM_UpdateCriteriaRemote
    History
        <date>                <author>                <description>
        29-JUL-2015           Mark Cagurong             Created class        
********************************************************************************/
@isTest
private class OSM_UpdateCriteriaRemote_Test {
	
	private static final String TEST_ACCOUNT_NAME = 'Acct1234';
	private static final String TEST_PLAN_NAME = 'PLAN123';
	private static final String TEST_PLAN_STATUS = 'PLAN123';
	private static final String TEST_CRITERIA_NAME = 'CRIT123';

	private static final String PLAN_LOB_COMM = 'COMM';
	private static final String PLAN_LOB_MCRA = 'MCRA';
	private static final String PLAN_LOB_MAMC = 'MAMC';
	private static final String PLAN_LOB_BCBS = 'BCBS';
	private static final String PLAN_LOB_EXCH = 'EXCH';
	private static final String PLAN_LOB_GOVT = 'GOVT';

	private static final String CRITERIA_LOB_COMM = 'Commercial';	
	private static final String CRITERIA_LOB_MCRA = 'Medicare Advantage';
	private static final String CRITERIA_LOB_DOMESTIC_GOVT ='Domestic Government';
	private static final String CRITERIA_LOB_MANAGED_MEDICAID = 'Managed Medicaid';
	private static final String CRITERIA_LOB_INTL_GOVT ='International Government';


	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Test Initialization routine
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	@testSetup static void setup(){
		Record_Type__c recordLists = OSM_DataFactory.recordLists();
		insert recordLists;

		Account payorAccount = OSM_DataFactory.createPayorAccountWithBillingAddress(2,
                                                                               '2',
                                                                               'Private',
                                                                               'United States', 
                                                                               'Test Street', 
                                                                               'Test City', 
                                                                               'Alaska', 
                                                                               '123', 
                                                                               recordLists.Account_Payor_Record_Type__c);
		payorAccount.Name = TEST_ACCOUNT_NAME;

        insert payorAccount;
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Helper method to retrieve the test account
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	static Account getTestAccount(){
		return [SELECT Id FROM Account WHERE Name = :TEST_ACCOUNT_NAME LIMIT 1];
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Helper method to create criteria test data
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	static OSM_Criteria__c createCriteria(Account acct, String line_of_Business){
		Id ibcRTID = Schema.SObjectType.OSM_Criteria__c.getRecordTypeInfosByName().get('IBC').getRecordTypeId();

		OSM_Criteria__c criteria = OSM_DataFactory.createCriteria(TEST_CRITERIA_NAME, acct.ID, ibcRTID);
		criteria.OSM_Line_of_Business__c = line_of_Business;

		//insert criteria;
		return criteria;
	}
	
	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Helper class for the common  updateCriteria unit tests 
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	static void updateCriteriaFilterTest(String planLOB, String criteriaLOB) {	
		
		Account acct = getTestAccount();

		OSM_Plan__c testPlan1 = OSM_DataFactory.createPlan(acct.Id, TEST_PLAN_NAME, planLOB, TEST_PLAN_STATUS);
		insert testPlan1;
		
		OSM_Plan__c testPlan2 = OSM_DataFactory.createPlan(acct.Id, TEST_PLAN_NAME, planLOB, TEST_PLAN_STATUS);
		insert testPlan2;	
		
		
		OSM_Criteria__c criteria1 = createCriteria(acct, criteriaLOB);
		insert criteria1;

		OSM_Criteria__c criteria2 = createCriteria(acct, criteriaLOB);
		insert criteria2;

		/*
		List<OSM_Criteria__c> criterias = [SELECT Id, Name, OSM_Payor__c, OSM_Plan__c, OSM_Line_of_Business__c, RecordType.Name FROM OSM_Criteria__c];

		System.debug('0.criterias.size: ' + criterias.size());
		System.debug('0.criterias: ' + JSON.serialize(criterias));
		*/

		Test.startTest();

		//String idListStr = String.format('{0}::{1}', new String[]{testPlan1.Id, testPlan2.Id});
		String idListStr = String.format('{0}::{1}', new String[]{testPlan1.Id, testPlan2.Id});

		OSM_UpdateCriteriaRemote.updateCriteria(idListStr, acct.Id);

		Test.stopTest();

		List<OSM_Criteria__c> criterias = [SELECT Id, Name, OSM_Payor__c, OSM_Plan__c, OSM_Line_of_Business__c, RecordType.Name FROM OSM_Criteria__c WHERE OSM_Line_of_Business__c = :criteriaLOB];

		// now we should have 6 criterias, the other one should be a clone for the plan
		System.assertEquals(6, criterias.size());
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Helper class for the common  updatePricingToPlan unit tests 
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	static void updatePricingToPlan(String planLOB, String pricingSchema) {

		Account acct = getTestAccount();		
		
		//createPlan(ID payID, String nameplan, String financial, String status)
		
		OSM_Plan__c testPlan1 = OSM_DataFactory.createPlan(acct.Id, TEST_PLAN_NAME, planLOB, TEST_PLAN_STATUS);
		insert testPlan1;		

		OSM_Plan__c testPlan2 = OSM_DataFactory.createPlan(acct.Id, TEST_PLAN_NAME, planLOB, TEST_PLAN_STATUS);
		insert testPlan2;		

		String idListStr = String.format('{0}::{1}', new String[]{testPlan1.Id, testPlan2.Id});

		Date ptpStartDate = Date.today().addDays(10);
		OSM_Payor_Test_Price__c ptp = OSM_DataFactory.createPTP(acct.Id, null, false, null, null, null );
		ptp.OSM_Pricing_Schema__c = pricingSchema;
		ptp.OSM_Start_Date__c  = ptpStartDate;		
		insert ptp;

		Product2 test = OSM_DataFactory.createProduct('Test', true);
		insert test;

		OSM_Payor_Test_Price__c ptp2 = OSM_DataFactory.createPTP(acct.Id, null, true, null, null, null );
		ptp2.OSM_Pricing_Schema__c = pricingSchema;
		//ptp2.OSM_Currently_Active__c  -> true 
		ptp2.OSM_Test__c = test.Id;
		insert ptp2;		

		OSM_UpdateCriteriaRemote.updatePricingToPlan(idListStr, acct.Id);
		//OSM_PlanTriggerHandler.updatePricingToPlan(idListStr, acct.Id);

		List<OSM_Payor_Test_Price__c> ptps = [SELECT Id, Name, OSM_Payor__c, OSM_Pricing_Schema__c FROM OSM_Payor_Test_Price__c WHERE OSM_Pricing_Schema__c = :ptp.OSM_Pricing_Schema__c];

		// now we should have 2 criterias, the other one should be a clone for the plan
		System.assertEquals(6, ptps.size());
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and criteria LOB through updateCriteria
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updateCriteria_PLAN_LOB_COMM_2_CRITERIA_LOB_COMM() {	
		
		updateCriteriaFilterTest(PLAN_LOB_COMM, CRITERIA_LOB_COMM);
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and criteria LOB through updateCriteria
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updateCriteria_PLAN_LOB_MCRA_2_CRITERIA_LOB_MCRA() {	
		
		updateCriteriaFilterTest(PLAN_LOB_MCRA, CRITERIA_LOB_MCRA);
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and criteria LOB through updateCriteria
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updateCriteria_PLAN_LOB_MAMC_2_CRITERIA_LOB_MANAGED_MEDICAID() {	
		
		updateCriteriaFilterTest(PLAN_LOB_MAMC, CRITERIA_LOB_MANAGED_MEDICAID);
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and criteria LOB through updateCriteria
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updateCriteria_PLAN_LOB_BCBS_2_CRITERIA_LOB_COMM() {	
		
		updateCriteriaFilterTest(PLAN_LOB_BCBS, CRITERIA_LOB_COMM);
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and criteria LOB through updateCriteria
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updateCriteria_PLAN_LOB_EXCH_2_CRITERIA_LOB_COMM() {	
		
		updateCriteriaFilterTest(PLAN_LOB_EXCH, CRITERIA_LOB_COMM);
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and criteria LOB through updateCriteria
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updateCriteria_PLAN_LOB_GOVT_2_CRITERIA_LOB_DOMESTIC_GOVT() {	
		
		updateCriteriaFilterTest(PLAN_LOB_GOVT, CRITERIA_LOB_DOMESTIC_GOVT);
	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and criteria LOB through updateCriteria
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updateCriteria_PLAN_LOB_GOVT_2_CRITERIA_LOB_INTL_GOVT() {	
		
		updateCriteriaFilterTest(PLAN_LOB_GOVT, CRITERIA_LOB_INTL_GOVT);
	}	

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_COMM_2_Commercial() {

		updatePricingToPlan('COMM', 'Commercial');

	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_MCRA_2_Medicare_Advantage() {
		
		updatePricingToPlan('MCRA', 'Medicare Advantage');

	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_MAMC_2_Managed_Medicaid() {
		
		updatePricingToPlan('MAMC', 'Managed Medicaid');

	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_BCBS_2_Commercial() {
		
		updatePricingToPlan('BCBS', 'Commercial');

	}	

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_COMM_2_TriCare() {
		
		updatePricingToPlan('COMM', 'TriCare');

	}	

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_EXCH_2_TriCare() {
		
		updatePricingToPlan('EXCH', 'Commercial');

	}	

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_GOVT_2_Government() {
		
		updatePricingToPlan('GOVT', 'Government');

	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_GOVT_2_Medicare() {
		
		updatePricingToPlan('GOVT', 'Medicare');

	}

	/******************************************************************************
        Author         : Mark Cagurong
        Description    : Unit test to cover cloning criteria via plan LOB and pricing plan  through updatePricingToPlan
        History
            <date>                <author>                <description>
            29-JUL-2015           Mark Cagurong             Created test method
    ********************************************************************************/
	testmethod static void updatePricingToPlan_GOVT_2_Medicaid() {
		
		updatePricingToPlan('GOVT', 'Medicaid');

	}
    
}