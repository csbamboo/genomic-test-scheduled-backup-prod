/*
  @author:Patrick Lorilla
  @date: 1 SEPTEMBER 2015
  @description: Batch class for updating the update default payor test price
*/
global class OSM_UpdateDefaultPTPBatch implements Database.Batchable<sObject>, Database.Stateful  {
    
    global List<MyWrapper> wrapperList = new List<MyWrapper>();
    Map<Id, Account> myAcctScope = new Map<Id, Account>();
   Map<Id, OSM_Payor_Test_Price__c> myPTPScope = new Map<Id, OSM_Payor_Test_Price__c>();
    global Boolean isReRun {get;set;}
    public static Boolean batchIsRunnng = false;
    public static boolean batchSuccess = false;
    Integer batchCtr = 0;
    //wrapper to get the id, name and the error of the batch
    public Class MyWrapper{
        String id;
        String name;
        String errors;
        public MyWrapper(String id, String name, String errors){
            this.id = id;
            this.name = name;
            this.errors = errors;
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //declaration of variables
		String soqlString = '';
		Id payorRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
	
        if(!isReRun) {
            //for the query
	        //soqlString = 'SELECT Id, Name, OSM_Agreement_ID__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c, OSM_Payor__c FROM OSM_Payor_Test_Price__c';
              soqlString = 'SELECT ID, (Select Id from Plans__r), OSM_Payor_Category__c, CurrencyIsoCode, GHI_CLM_Agreement_ID__c from Account where RecordTypeId =:payorRTId and OSM_Status__c = \'Approved\' ';
        } else {
            Set<String> errorIds = new Set<String>();
            List<Error_in_Batch__c> eb = [Select Record_Ids__c From Error_in_Batch__c Where Object__c = 'Account' Order By CreatedDate DESC Limit 1];
            system.debug('test eb ' + eb);
            if(eb.size()>0) {
                for(String idLop : eb[0].Record_Ids__c.split(',')){
                    errorIds.add(idLop);
                }
                //for the query
		        //soqlString = 'SELECT Id, Name, OSM_Agreement_ID__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c, OSM_Payor__c FROM OSM_Payor_Test_Price__c WHERE Id IN: errorIds';
                soqlString = 'SELECT ID, (Select Id from Plans__r), OSM_Payor_Category__c, CurrencyIsoCode, GHI_CLM_Agreement_ID__c from Account where RecordTypeId =:payorRTId AND OSM_Status__c = \'Approved\' AND Id IN: errorIds';
                
            } else {
                //for the query
		        //soqlString = 'SELECT Id, Name, OSM_Agreement_ID__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c, OSM_Payor__c FROM OSM_Payor_Test_Price__c';
                 soqlString = 'SELECT ID, (Select Id from Plans__r), OSM_Payor_Category__c, CurrencyIsoCode, GHI_CLM_Agreement_ID__c from Account where RecordTypeId =:payorRTId and OSM_Status__c = \'Approved\' ';
                
            }
        }
        system.debug('test soqlString ' + soqlString);
        return Database.getQueryLocator(soqlString);
    }
    
    /*
      @author: Patrick Lorilla
      @date: 1 SEPT 2015
      @description: Batch class for updating the payor test price
      */
    global void execute(Database.BatchableContext BC,List<Account> scope){
        batchIsRunnng = true;
        List<Account> acctsWithPlans = new List<Account>();
        for(Account acct: scope){
            //if(acct.Plans__r.size() > 0){
                acctsWithPlans.add(acct);    
            //}
           
        }
        myAcctScope = new Map<Id, Account>(acctsWithPlans);
        for(OSM_Payor_Test_Price__c ptp: [SELECT Id, Name from OSM_Payor_Test_Price__c where OSM_Payor__c IN: myAcctScope.keySet()]){
            myPTPScope.put(ptp.Id, ptp);    
        }
        try{
            batchSuccess = false;
            updateDefaultPTPLP(acctsWithPlans);
            if(batchSuccess){
                batchCtr++;    
            }
        }
        catch(DMLException ex){
             for (Integer i = 0; i < ex.getNumDml(); i++) {
                 if(ex.getDmlId(i) != null && myPTPScope.containsKey(ex.getDmlId(i))){
                    wrapperList.add(new MyWrapper(ex.getDmlId(i), myPTPScope.get(ex.getDmlId(i)).Name, ex.getDmlMessage(i) ));     
                 }
                 else if (ex.getDmlId(i) != null && myAcctScope.containsKey(ex.getDmlId(i))){
                    wrapperList.add(new MyWrapper(ex.getDmlId(i), myAcctScope.get(ex.getDmlId(i)).Name, ex.getDmlMessage(i) ));         
                 }
                 else if(ex.getDmlId(i) != null && !myPTPScope.containsKey(ex.getDmlId(i))){
                    wrapperList.add(new MyWrapper(ex.getDmlId(i), 'Unable to retrieve name', ex.getDmlMessage(i) ));         
                 }
                 
             }

        }
        catch(Exception ex){
             wrapperList.add(new MyWrapper('N/A', 'N/A', ex.getMessage()));    
        }
    }
    
    public  void updateDefaultPTPLP(List<Account> myAccts){
        
        List<String> pbNameList = new List<String>();
        List<String> pbCatList = new List<String>();
        Map<Id,Account> accountMap = new Map<Id,Account>(myAccts);
        System.debug('\n\n\n ACCTKEYSET'+accountMap.keySet());
        Set<Id> accountIds = accountMap.keySet();
        Set<Id> planIds = new Set<Id>();
        for(Account act: myAccts){
               pbNameList.add(act.CurrencyIsoCode + ' ' + act.OSM_Payor_Category__c);
               pbCatList.add(act.OSM_Payor_Category__c);
               for(OSM_Plan__c pln: act.Plans__r){
                  planIds.add(pln.Id);    
               }
              
        }
        //retrieve price books and acct data
      
        List<PriceBookEntry> pbEntList = [SELECT Name, CurrencyIsoCode, Pricing_Category__c, UnitPrice, Pricebook2.Name, Product2.Name, ProductCode from PriceBookEntry where Pricing_Category__c IN : pbCatList AND Pricebook2.Name IN : pbNameList];
        
        Map<String, PriceBookEntry> pbTestMap = new Map<String, PriceBookEntry>();
        for(PriceBookEntry pbe: pbEntList){
             pbTestMap.put(pbe.Pricebook2.Name + '::'+ pbe.Product2.Name, pbe);
        }
        List<OSM_Payor_Test_Price__c> ptpList = [SELECT Id, OSM_Plan__r.OSM_Payor__c, OSM_Plan__r.OSM_Payor__r.OSM_Payor_Category__c, OSM_Payor__r.OSM_Payor_Category__c, OSM_Payor__c, OSM_List_Price__c, OSM_Test__r.Name, CurrencyIsoCode from OSM_Payor_Test_Price__c where (OSM_Payor__c IN: accountIds  OR OSM_Plan__c IN: planIds) AND OSM_Agreement_ID__r.Name =: Label.OSM_Dummy_Agreement_Name AND OSM_Newly_Generated__c = true]; 
     
        //populate fields
        for(OSM_Payor_Test_Price__c ptp: ptpList){
            if(ptp.OSM_Payor__c != null ){
                if(accountMap.containsKey(ptp.OSM_Payor__c)){
                    ptp.CurrencyIsoCode = accountMap.get(ptp.OSM_Payor__c).CurrencyIsoCode;
                }
                String curCat = ptp.CurrencyIsoCode + ' ' + ptp.OSM_Payor__r.OSM_Payor_Category__c +'::'+ptp.OSM_Test__r.Name;
                if(pbTestMap.containsKey(curCat)){
                    ptp.OSM_List_Price__c = pbTestMap.get(curCat).UnitPrice;
                }
            }
            else if (ptp.OSM_Plan__c != null){
                if(accountMap.containsKey(ptp.OSM_Plan__r.OSM_Payor__c)){
                    ptp.CurrencyIsoCode = accountMap.get(ptp.OSM_Plan__r.OSM_Payor__c).CurrencyIsoCode;
                } 
                String curCat = ptp.CurrencyIsoCode + ' ' + ptp.OSM_Plan__r.OSM_Payor__r.OSM_Payor_Category__c +'::'+ptp.OSM_Test__r.Name;
                if(pbTestMap.containsKey(curCat)){
                    ptp.OSM_List_Price__c = pbTestMap.get(curCat).UnitPrice;
                }
            }
            
        }
        
        if(!ptpList.isEmpty()){
            update ptpList;
            batchSuccess = true;
        }
    }
    global void finish(Database.BatchableContext BC){
        AsyncApexJob aaj = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        
        
        if(wrapperList.size() != 0){
          Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
          String[] toAddress = Label.OSM_PTP_Batch_Emails.split('::');
          String body = '<style>span.dropt:hover {text-decoration: none; background: #ffffff; z-index: 6; }span.dropt span {position: absolute; left: -9999px;margin: 20px 0 0 0px; padding: 1px 1px 1px 1px;border-style:solid; border-color:black; border-width:1px; z-index: 6;}span.dropt:hover span {left: 0%; background: #ffffff;} span.dropt span {position: absolute; left: -9999px;margin: 4px 0 0 0px; padding: 1px 1px 1px 1px; border-style:solid; border-color:black; border-width:1px;}span.dropt:hover span {margin: 10px 0 0 170px; background: #ffffff; z-index:6;} </style><table border="1" style="width:100%"><tr><th>Payor/PTP Name</th><th>Error</th></tr>';
          email.setSubject('Payor Test Price Failed Record');
          
          String errorIds = '';
          
          for(Integer i=0; i<wrapperList.size(); i++){
            if(!wrapperList.get(i).errors.contains('Trigger')){
                body += '<tr><td align="center" width="30%"><a href="'+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +wrapperList.get(i).id+'" target="_blank">'+wrapperList.get(i).name+'</a></td><td align="center">'+wrapperList.get(i).errors+'</td></tr>';
            } else {
                
                body += '<tr><td align="center" width="30%"><a href="'+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +parseErrorReturnId(wrapperList.get(i).errors)+'" target="_blank">'+'View Record'+'</a></td><td align="center">'+'<span class="dropt" title="Title for the pop-up">Apex Error'+'</td></tr>';
            }
            
            errorIds += wrapperList.get(i).id + ',';
          }
     
          Error_in_Batch__c eb = new Error_in_Batch__c();
          eb.Object__c = 'OSM_Payor_Test_Price__c';
          eb.Record_Ids__c = errorIds;
          insert eb;
          
          body += '</table><br/> Status'+aaj.Status+'<br/>Number Of Errors'+aaj.NumberofErrors+'<br/>JobItemsProcessed'+aaj.JobItemsProcessed+'<br/>TotalJobItems'+aaj.TotalJobItems;
          email.setHtmlBody(body);
          email.setToAddresses(toAddress);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
        }
        
        if(batchCtr !=0){
             Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
             String[] toAddress = Label.OSM_PTP_Batch_Emails.split('::');
             String body = '';//'<style>span.dropt:hover {text-decoration: none; background: #ffffff; z-index: 6; }span.dropt span {position: absolute; left: -9999px;margin: 20px 0 0 0px; padding: 1px 1px 1px 1px;border-style:solid; border-color:black; border-width:1px; z-index: 6;}span.dropt:hover span {left: 0%; background: #ffffff;} span.dropt span {position: absolute; left: -9999px;margin: 4px 0 0 0px; padding: 1px 1px 1px 1px; border-style:solid; border-color:black; border-width:1px;}span.dropt:hover span {margin: 10px 0 0 170px; background: #ffffff; z-index:6;} </style><table border="1" style="width:100%"><tr><th>Order Name</th><th>Status</th></tr>';
             email.setSubject('Payor Test Price Success Records');
             body += ''+wrapperList.size()+' '+'Number of Error Records';
            body += '\n'+batchCtr+' '+' Number of Success Records';
            email.setHtmlBody(body);
            email.setToAddresses(toAddress);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});   
        }
    }
    
      //method to get the error message per id
    public String parseErrorReturnId(String errorMsg){
        Integer indexCount = errorMsg.lastIndexOf('with id')+8;
        return errorMsg.substring(indexCount, indexCount+18);
    }
    

}