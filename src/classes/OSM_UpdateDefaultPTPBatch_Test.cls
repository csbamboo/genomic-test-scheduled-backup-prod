//@isTest(seeAllData = true)
@isTest
private class OSM_UpdateDefaultPTPBatch_Test {
    
    static testMethod void testBatch1() {
    
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
            triggerSwitch.Account_Trigger__c = false;
            triggerSwitch.Contact_Trigger__c = false;
            triggerSwitch.Order_Line_ItemTrigger__c = false;
            triggerSwitch.Order_Trigger__c = false;
            triggerSwitch.Validation_Builder__c = false;
            triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
            triggerSwitch.Product2__c = false;
            triggerSwitch.result_trigger__c = false;
            triggerSwitch.order_Role_Trigger__c = false;
            triggerSwitch.case_trigger__c = false;
            triggerSwitch.Payor_Test_Price_Trigger__c = false;
            triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id stdPB = test.getStandardPricebookId();
        
        Account payorAcc = OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType);
            payorAcc.OSM_Payor_Type__c = 'Private';
            payorAcc.OSM_Payor_Hierarchy__c = '2';
            payorAcc.OSM_Payor_Category__c = 'Private';
            payorAcc.OSM_Number_of_Plans__c = 1;
        insert payorAcc;
        
        /* Product */
        List<Product2> testProdList = new List<Product2>();
        Product2 testProd;
        testProd = OSM_DataFactory.createProduct('IBC', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('DCIS', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('Prostate', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('MMR', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('Colon', true);
        testProdList.add(testProd);
        insert testProdList;
        
        /* Pricebook2 */
        Pricebook2 testPB = OSM_DataFactory.createpb2('USD Private');
        testPB.isActive = true;
        insert testPB;
        
        /* Pricebook Entry */
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry pbe;
        for(Product2 prod2 : testProdList){
            pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, stdPB, 7);
            testPBEList.add(pbe);
            pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, testPB.Id, 7);
            pbe.CurrencyIsoCode = 'USD';
            pbe.Pricing_Category__c = 'Private';
            pbe.isActive = true;
            testPBEList.add(pbe);
        }
        insert testPBEList;
        
         OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payorAcc.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = false,
            OSM_Start_Date__c = System.Today() + 100,
            OSM_End_Date__c = System.Today() + 10,
            OSM_Pricing_Schema__c = 'Commercial'
        );
        insert ptp;
        
        String accid = acct.id;
        
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Account', Record_Ids__c = accid);

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        OSM_UpdateDefaultPTPBatch tr = new OSM_UpdateDefaultPTPBatch();
        
        tr.isReRun = false;
        
        OSM_UpdateDefaultPTPBatch.MyWrapper a = new OSM_UpdateDefaultPTPBatch.MyWrapper('Account',accid, 'error');
        
        tr.WrapperList.add(a);
        
        OSM_Plan__c plan = new OSM_Plan__c(OSM_Payor__c = payorAcc.Id);
            plan.OSM_QDX_Financial_Category__c = 'COMM';
        insert plan;
        
        List<Account> accList = new List<Account>();
        accList.add(acct);
        accList.add(payorAcc);

        Test.startTest();

            Database.executeBatch(tr);
            
            tr.updateDefaultPTPLP(accList);
            
            plan.OSM_QDX_Financial_Category__c = 'MCRA';
            
            update plan;
            
            ptp.OSM_Pricing_Schema__c = 'Medicare Advantage';
            
            update ptp;
            
            tr.updateDefaultPTPLP(accList);

        Test.stopTest();

    }
    
    static testMethod void testBatch2() {
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
            triggerSwitch.Account_Trigger__c = false;
            triggerSwitch.Contact_Trigger__c = false;
            triggerSwitch.Order_Line_ItemTrigger__c = false;
            triggerSwitch.Order_Trigger__c = false;
            triggerSwitch.Validation_Builder__c = false;
            triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
            triggerSwitch.Product2__c = false;
            triggerSwitch.result_trigger__c = false;
            triggerSwitch.order_Role_Trigger__c = false;
            triggerSwitch.case_trigger__c = false;
            triggerSwitch.Payor_Test_Price_Trigger__c = false;
            triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        
        Account payorAcc = OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType);
            payorAcc.OSM_Payor_Type__c = 'Private';
            payorAcc.OSM_Payor_Hierarchy__c = '2';
            payorAcc.OSM_Payor_Category__c = 'Private';
            payorAcc.OSM_Number_of_Plans__c = 1;
        insert payorAcc;
        
        String accid = acct.id;
        
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Account', Record_Ids__c = accid);
        insert errInBatch;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id stdPB = test.getStandardPricebookId();
        
        /* Product */
        List<Product2> testProdList = new List<Product2>();
        Product2 testProd;
        testProd = OSM_DataFactory.createProduct('IBC', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('DCIS', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('Prostate', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('MMR', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('Colon', true);
        testProdList.add(testProd);
        insert testProdList;
        
        /* Pricebook2 */
        Pricebook2 testPB = OSM_DataFactory.createpb2('USD Private');
        testPB.isActive = true;
        insert testPB;
        
        /* Pricebook Entry */
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry pbe;
        for(Product2 prod2 : testProdList){
            pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, stdPB, 7);
            testPBEList.add(pbe);
            pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, testPB.Id, 7);
            pbe.CurrencyIsoCode = 'USD';
            pbe.Pricing_Category__c = 'Private';
            pbe.isActive = true;
            testPBEList.add(pbe);
        }
        insert testPBEList;
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        OSM_UpdateDefaultPTPBatch tr = new OSM_UpdateDefaultPTPBatch();
        
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payorAcc.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = true,
            OSM_Start_Date__c = System.Today() + 100,
            OSM_End_Date__c = System.Today() + 10,
            OSM_Pricing_Schema__c = 'Commercial'
        );
        insert ptp;
        
        tr.isReRun = true;
        
        OSM_UpdateDefaultPTPBatch.MyWrapper a = new OSM_UpdateDefaultPTPBatch.MyWrapper('Account',accid, 'TheQuickBrownFoxJumpsOverTheLazyDog');
        
        tr.WrapperList.add(a);
        
        OSM_Plan__c plan = new OSM_Plan__c(OSM_Payor__c = payorAcc.Id);
            plan.OSM_QDX_Financial_Category__c = 'COMM';
        insert plan;
        
        List<Account> accList = new List<Account>();
        accList.add(acct);
        accList.add(payorAcc);
        
        Test.startTest();
            
            Database.executeBatch(tr);
            
            tr.updateDefaultPTPLP(accList);
            
            plan.OSM_QDX_Financial_Category__c = 'BCBS';
            
            update plan;
            
            tr.updateDefaultPTPLP(accList);

        Test.stopTest();
    }
    
    static testMethod void testBatch3() {
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
            triggerSwitch.Account_Trigger__c = false;
            triggerSwitch.Contact_Trigger__c = false;
            triggerSwitch.Order_Line_ItemTrigger__c = false;
            triggerSwitch.Order_Trigger__c = false;
            triggerSwitch.Validation_Builder__c = false;
            triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
            triggerSwitch.Product2__c = false;
            triggerSwitch.result_trigger__c = false;
            triggerSwitch.order_Role_Trigger__c = false;
            triggerSwitch.case_trigger__c = false;
            triggerSwitch.Payor_Test_Price_Trigger__c = false;
            triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id stdPB = test.getStandardPricebookId();
        
        Account payorAcc = OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType);
            payorAcc.OSM_Payor_Type__c = 'Private';
            payorAcc.OSM_Payor_Hierarchy__c = '2';
            payorAcc.OSM_Payor_Category__c = 'Private';
            payorAcc.OSM_Number_of_Plans__c = 1;
        insert payorAcc;
        
        /* Product */
        List<Product2> testProdList = new List<Product2>();
        Product2 testProd;
        testProd = OSM_DataFactory.createProduct('IBC', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('DCIS', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('Prostate', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('MMR', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('Colon', true);
        testProdList.add(testProd);
        insert testProdList;
        
        /* Pricebook2 */
        Pricebook2 testPB = OSM_DataFactory.createpb2('USD Private');
        testPB.isActive = true;
        insert testPB;
        
        /* Pricebook Entry */
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry pbe;
        for(Product2 prod2 : testProdList){
            pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, stdPB, 7);
            testPBEList.add(pbe);
            pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, testPB.Id, 7);
            pbe.CurrencyIsoCode = 'USD';
            pbe.Pricing_Category__c = 'Private';
            pbe.isActive = true;
            testPBEList.add(pbe);
        }
        insert testPBEList;
        
        String accid = acct.id;
        
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Account', Record_Ids__c = accid);
        insert errInBatch;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        OSM_UpdateDefaultPTPBatch tr = new OSM_UpdateDefaultPTPBatch();
        
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payorAcc.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = true,
            OSM_Start_Date__c = System.Today() + 100,
            OSM_End_Date__c = System.Today() + 10,
            OSM_Pricing_Schema__c = 'TriCare'
        );
        insert ptp;
        
        tr.isReRun = true;
        
        OSM_UpdateDefaultPTPBatch.MyWrapper a = new OSM_UpdateDefaultPTPBatch.MyWrapper('Account',accid, 'TheQuickBrownFoxJumpsOverTheLazyDog');
        
        tr.WrapperList.add(a);
        
        OSM_Plan__c plan = new OSM_Plan__c(OSM_Payor__c = payorAcc.Id);
            plan.OSM_QDX_Financial_Category__c = 'COMM';
        insert plan;
        
        List<Account> accList = new List<Account>();
        accList.add(acct);
        accList.add(payorAcc);
         
        Test.startTest();

            Database.executeBatch(tr);
            
            tr.updateDefaultPTPLP(accList);
            
            plan.OSM_QDX_Financial_Category__c = 'EXCH';
            
            update plan;
            
            ptp.OSM_Pricing_Schema__c = 'Commercial';
            
            update ptp;
            
            tr.updateDefaultPTPLP(accList);

        Test.stopTest();   
    }

    static testMethod void testBatch4() {
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
            triggerSwitch.Account_Trigger__c = false;
            triggerSwitch.Contact_Trigger__c = false;
            triggerSwitch.Order_Line_ItemTrigger__c = false;
            triggerSwitch.Order_Trigger__c = false;
            triggerSwitch.Validation_Builder__c = false;
            triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
            triggerSwitch.Product2__c = false;
            triggerSwitch.result_trigger__c = false;
            triggerSwitch.order_Role_Trigger__c = false;
            triggerSwitch.case_trigger__c = false;
            triggerSwitch.Payor_Test_Price_Trigger__c = false;
            triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id stdPB = test.getStandardPricebookId();
        
        /* Product */
        List<Product2> testProdList = new List<Product2>();
        Product2 testProd;
        testProd = OSM_DataFactory.createProduct('IBC', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('DCIS', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('Prostate', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('MMR', true);
        testProdList.add(testProd);
        testProd = OSM_DataFactory.createProduct('Colon', true);
        testProdList.add(testProd);
        insert testProdList;
        
        /* Pricebook2 */
        Pricebook2 testPB = OSM_DataFactory.createpb2('USD Private');
        testPB.isActive = true;
        insert testPB;
        
        /* Pricebook Entry */
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry pbe;
        for(Product2 prod2 : testProdList){
            pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, stdPB, 7);
            testPBEList.add(pbe);
            pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, testPB.Id, 7);
            pbe.CurrencyIsoCode = 'USD';
            pbe.Pricing_Category__c = 'Private';
            pbe.isActive = true;
            testPBEList.add(pbe);
        }
        insert testPBEList;
        
        Account payorAcc = OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType);
            payorAcc.OSM_Payor_Type__c = 'Private';
            payorAcc.OSM_Payor_Hierarchy__c = '2';
            payorAcc.OSM_Payor_Category__c = 'Private';
            payorAcc.OSM_Number_of_Plans__c = 1;
        insert payorAcc;
        
        String accid = acct.id;
        
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Account', Record_Ids__c = accid);
        insert errInBatch;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        OSM_UpdateDefaultPTPBatch tr = new OSM_UpdateDefaultPTPBatch();
        
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payorAcc.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = true,
            OSM_Start_Date__c = System.Today() + 100, 
            OSM_End_Date__c = System.Today() + 10,
            OSM_Pricing_Schema__c = 'Managed Medicaid'
        );
        insert ptp;
        
        tr.isReRun = true;
        
        OSM_UpdateDefaultPTPBatch.MyWrapper a = new OSM_UpdateDefaultPTPBatch.MyWrapper('Account',accid, 'TheQuickBrownFoxJumpsOverTheLazyDog');
        
        tr.WrapperList.add(a);
        
        OSM_Plan__c plan = new OSM_Plan__c(OSM_Payor__c = payorAcc.Id);
            plan.OSM_QDX_Financial_Category__c = 'MAMC';
        insert plan;
        
        List<Account> accList = new List<Account>();
        accList.add(acct);
        accList.add(payorAcc);
        
         
        Test.startTest();

            Database.executeBatch(tr);
            tr.updateDefaultPTPLP(accList);
            plan.OSM_QDX_Financial_Category__c = 'GOVT';
            update plan;
            ptp.OSM_Pricing_Schema__c = 'Government';
            update ptp;
            tr.updateDefaultPTPLP(accList);
            ptp.OSM_Pricing_Schema__c = 'Medicare';
            update ptp;
            tr.updateDefaultPTPLP(accList);
            ptp.OSM_Pricing_Schema__c = 'Medicaid';
            update ptp;
            tr.updateDefaultPTPLP(accList);
            tr.parseErrorReturnId('TheQuickBrownFoxJumpsOverTheLazyDog');

        Test.stopTest();   
    }
    
}