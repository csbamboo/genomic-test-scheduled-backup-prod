global class OSM_UpdateDefaultPTPRemote {
    
    webservice static void updateDefaultPTPLP(Id accountId){
      
        Account acct= [SELECT ID, OSM_Payor_Category__c, CurrencyIsoCode from Account where ID =: accountId LIMIT 1];
        List<PriceBookEntry> pbEntList = [SELECT Name, CurrencyIsoCode, Pricing_Category__c, UnitPrice, Pricebook2.Name, Product2.Name, ProductCode from PriceBookEntry where Pricing_Category__c =: acct.OSM_Payor_Category__c AND Pricebook2.Name =: acct.CurrencyIsoCode + ' '+acct.OSM_Payor_Category__c];
        
        List<OSM_Payor_Test_Price__c> ptpList = [SELECT Id, OSM_List_Price__c, OSM_Test__r.Name, CurrencyIsoCode from OSM_Payor_Test_Price__c where OSM_Payor__c =: accountId AND OSM_Agreement_ID__r.Name =: Label.OSM_Dummy_Agreement_Name AND OSM_Newly_Generated__c = true]; 
        System.debug('\n\n\n PTPLISTVAl' + ptpList);
        for(OSM_Payor_Test_Price__c ptp: ptpList){
            ptp.CurrencyIsoCode = acct.CurrencyIsoCode;
            for(PriceBookEntry pbe: pbEntList){
                    if(ptp.OSM_Test__r.Name == pbe.Product2.Name){
                        ptp.OSM_List_Price__c = pbe.UnitPrice;
                        
                        System.debug('\n\n\n ptpedit'+ ptp);
                    }   
            }
            
        }
        
        if(!ptpList.isEmpty()){
            System.debug('\n\n\n PTPLISTUPD1' + ptpList);
            update ptpList;
            System.debug('\n\n\n PTPLISTUPD2' + ptpList);
        }
        
    
    }

}