@isTest(SeeAllData=false) 
public class OSM_UpdateDefaultPTPRemote_Test {
    
      testmethod static void updateDefaultPTPLPTest(){
          
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        insert triggerSwitch;
        
        Account payor = OSM_DataFactory.createPayorAccountWithBillingAddress(0,'2', 'Private','United States', 'street', 'city','province','111', Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId());
        payor.OSM_Status__c = 'Draft';
        insert payor;
        
        payor.OSM_Status__c = 'Approved';
        update payor;
        Test.startTest();
        OSM_UpdateDefaultPTPRemote.updateDefaultPTPLP(payor.Id);
        Test.stopTest();
       
      }

}