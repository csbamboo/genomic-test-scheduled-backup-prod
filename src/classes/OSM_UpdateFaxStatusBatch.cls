/********************************************************************
    @author         : Rescian Rey
    @description    : Batch class for mass updating fax records' status. Batch size is
                        limited to 99 to avoid callout limit exception. This batch prioritizes
                        newly created fax.
    @history:
        <date>                <author>                <description>
        MAY 28 2015           Rescian Rey             Created class.
********************************************************************/
global with sharing class OSM_UpdateFaxStatusBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
	private static final Integer BATCH_SIZE = 99;
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id FROM Fax__c WHERE Status__c = \'Pending\' ORDER BY CreatedDate DESC LIMIT ' + BATCH_SIZE);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Fax__c> faxToUpdate = new List<Fax__c>();
        for(Fax__c fax: (List<Fax__c>)scope){
            faxToUpdate.add(OSM_FaxUtilities.updateFaxStatus(fax));
        }
        update faxToUpdate;
    }
    
    global void finish(Database.BatchableContext info){}
}