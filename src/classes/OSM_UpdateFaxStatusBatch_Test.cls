/********************************************************************
    @author         : Mark Cagurong
    @description    : OSM_UpdateFaxStatusBatch Test class
    @history:
        <date>                <author>                <description>
        July 22 2015           Mark Cagurong             Created class.
********************************************************************/
@isTest
private class OSM_UpdateFaxStatusBatch_Test {
    
    private static final String TEST_FAX_SUBJECT = 'Test Fax';
    private static final String FAX_NUMBER = '18622351052';
    private static final String BATCH_STATUS_FLAG = 'Pending';
    private static final Integer BATCH_SIZE = 20;

    /*
        @author: Mark Cagurong
        @description: initialization data
        @createdDate: July 22 2015
        @history:
        <date>     <author>    <change_description>
    */
    @testSetup static void Setup_Data(){
        
        List<Fax__c> faxes = new List<Fax__c>();

        for(Integer ctr=0; ctr<BATCH_SIZE; ctr++){
            Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');
            fax.Fax_Subject__c = TEST_FAX_SUBJECT;
            fax.Status__c = BATCH_STATUS_FLAG;

            faxes.add(fax);
        }

        insert faxes;
    }
    
    /*
        @author: Mark Cagurong
        @description: Test that all faxes are updated by by this batch job with the fake status, we don't need much variations in testing as we have separate unit tests for the fax utility simulating different statuses
        @createdDate: July 22 2015
        @history:
        <date>     <author>    <change_description>
    */
    @isTest static void executeBatch() {

        // force success status on the fax service mock call
        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = OSM_EasyLink_API_Services.QUERY_FAX_SUCCESS_MSG;
        
        Test.startTest();

        Database.executeBatch(new OSM_UpdateFaxStatusBatch());

        Test.stopTest();

        List<Fax__c> faxes = [SELECT Id, Fax_Number__c, Fax_Subject__c, Status__c FROM Fax__c WHERE Fax_Subject__c = :TEST_FAX_SUBJECT];

        Boolean allSuccess = true;
        for(Fax__c fax: faxes){
            if(false == 'Success'.equals(fax.Status__c)){
                allSuccess = false;
                break;
            }
        }

        system.assertEquals(BATCH_SIZE, faxes.size());
        system.assertEquals(true, allSuccess); 
    }
    
}