/*
    @author: Rescian Rey
    @description: Controller for the Update Status Button for Fax
    @createdDate: APR 20, 2015
    @history:
        APR 21, 2015  Added constructor and sendFax method
*/
public with sharing class OSM_UpdateFaxStatusController {
    private ApexPages.StandardController controller;
    private Fax__c faxRecord;

    /*
        @author: Rescian Rey
        @description: Initialization. Gets the Fax Record.
        @createdDate: APR 21, 2015
        @history:
    */
    public OSM_UpdateFaxStatusController(ApexPages.StandardController cx){
        controller = cx;
        faxRecord = [SELECT Id FROM Fax__c WHERE Id = :cx.getRecord().Id];

        // Bypass edit validations
        OSM_KitOrderTriggerHandler.bypassEditValidation = true;
        OSM_FaxTriggerHandler.bypassEditValidation = true;
    }

    /*
        @author: Rescian Rey
        @description: Calls the Easylink Fax Web Service to query fax.
        @createdDate: APR 21, 2015
        @history:
    */
    public PageReference updateStatus(){
        faxRecord = OSM_FaxUtilities.updateFaxStatus(faxRecord);
        update faxRecord;

        return controller.view();
    }
}