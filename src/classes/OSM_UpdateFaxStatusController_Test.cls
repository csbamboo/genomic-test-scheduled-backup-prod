/********************************************************************
    @author: Rescian Rey
    @description: Unit tests for OSM_UpdateFaxStatusController
    @history:
        <date>                <author>                <description>
        MAY 13 2015           Rescian Rey             Created class
        July 22 2015           Mark Cagurong             Added unit tests
********************************************************************/
@isTest
public class OSM_UpdateFaxStatusController_Test {
    
    private static final String TEST_FAX_SUBJECT = 'Test Fax';
    private static final String FAX_NUMBER = '18622351052';

    /*
        @author: Mark Cagurong
        @description: initialization of test data
        @createdDate: July 22, 2015
        @history:
    */
    @testsetup static void Setup_Data(){

        Fax__c fax = OSM_DataFactory.createFax(FAX_NUMBER, 'New');
        fax.Fax_Subject__c = TEST_FAX_SUBJECT;

        insert fax;
    }

   /*
        @author: Mark Cagurong
        @description: unit text for updatestatus, we have another set of unit tests just for the OSM_FaxUtilities so we have the variations there, here we'll just stick with the success verification
        @createdDate: July 22, 2015
        @history:
    */
    private static testmethod void updateStatus(){

        Fax__c fax = [SELECT Id, Status__c FROM Fax__c where Fax_Number__c = :FAX_NUMBER LIMIT 1];        

        Test.startTest();

        ApexPages.StandardController std = new ApexPages.StandardController(fax);
        OSM_UpdateFaxStatusController controller = new OSM_UpdateFaxStatusController(std);

        OSM_EasyLink_API_Services.FAX_STATUS_OVERRIDE = OSM_EasyLink_API_Services.QUERY_FAX_SUCCESS_MSG;

        // send fax
        controller.updateStatus();

        Test.stopTest();

        fax = [SELECT Id, Status__c FROM Fax__c where Fax_Number__c = :FAX_NUMBER LIMIT 1];        

        system.assertEquals('Success', fax.Status__c);
    }
    

}