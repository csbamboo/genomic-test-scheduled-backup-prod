/********************************************************************
    @author         : Rescian Rey
    @description    : Schedulable class for mass update of fax status
    @history:
        <date>                <author>                <description>
        MAY 28 2015           Rescian Rey             Created class
********************************************************************/
global class OSM_UpdateFaxStatusSchedulable implements Schedulable {

    global void execute(SchedulableContext sc) {
        OSM_UpdateFaxStatusBatch b = new OSM_UpdateFaxStatusBatch(); 
        database.executebatch(b);
    }

}