/********************************************************************
    @author         : Mark Cagurong
    @description    : OSM_UpdateFaxStatusSchedulable Test class
    @history:
        <date>                <author>                <description>
        July 22 2015           Mark Cagurong             Created class.
********************************************************************/
@isTest
private class OSM_UpdateFaxStatusSchedulable_Test {

	private static final String CRON_SCHEDULE = '0 0 3 3,27 * ?';
	
	/*
        @author: Mark Cagurong
        @description: Unit test setting up for CRON the batch status scheduler class
        @createdDate: July 22 2015
        @history:
        <date>     <author>    <change_description>
    */
	@isTest static void schedule_CRON() {
		
		Test.startTest();

		String jobId = System.schedule('OSM_UpdateFaxStatusSchedulable', CRON_SCHEDULE, new OSM_UpdateFaxStatusSchedulable());

		Test.stopTest();
		
		//check the schedule details
		List<CronTrigger> cronJob = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE (Id = :jobId)];
		if (!cronJob.isEmpty()) {
			CronTrigger schedJob = cronJob.get(0);
			System.assert(schedJob.CronExpression.equals(CRON_SCHEDULE));
			System.assert(schedJob.TimesTriggered == 0);
			System.assert(schedJob.NextFireTime.hour() == 3);
		}
	}
	
}