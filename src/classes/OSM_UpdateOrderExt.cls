/*
  @author: patrick lorilla
  @date: 14 OCT 2014
  @description: Update Order Extension
  @history: 14 OCT 2014 - Created (PL)

*/
public class OSM_UpdateOrderExt{
    private Order pageOrder = null;
    /*
      @author: patrick lorilla
      @date: 14 OCT 2014
      @description: Constructor
      @param: stdController - Standard Controller
    */
    public OSM_UpdateOrderExt(ApexPages.StandardController stdController){
        pageOrder = (Order)stdController.getRecord();

    }
    /*
      @author: patrick lorilla
      @date: 14 OCT 2014
      @description: Update Orders
    */
    public PageReference updateOrder(){
        Order updateOrder = [SELECT Id, OSM_Patient__c, OSM_Patient_First_Name__c, OSM_Patient_Last_Name__c, OSM_Patient_Gender__c, OSM_Patient_State__c, OSM_Patient_Country__c, OSM_Patient_Initials__c, AccountId, Pricebook2Id  FROM Order WHERE Id = :pageOrder.Id];
        //Call on create logic - copies account/contact details to order role fields
        update OSM_OrderTriggerHandler.populatePatientFields(new List<Order>{updateOrder}, false);
  
        return new PageReference('/' + pageOrder.Id);
    }
}