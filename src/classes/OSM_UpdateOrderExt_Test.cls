/*
  @author: David E. Catindoy 
  @date: 23 JAN 2015
  @description: Update Order Extension test
  @history: 23 JAN 2015 - Created (David E. Catindoy)

*/
@isTest
private class OSM_UpdateOrderExt_Test {
    
    static testMethod void testExt() {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        //Custome Standard Price Book
        Pricebook2 customPB = new Pricebook2(Name='Test Class Price Book', isActive=true);
        insert customPB;
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Order order = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        insert order;

        Test.startTest();
        PageReference pgRef = Page.OSM_UpdateOrder;
        Test.setCurrentPage(pgRef);
        ApexPages.StandardController standardController = new ApexPages.StandardController(order);
        OSM_UpdateOrderExt controller = new OSM_UpdateOrderExt(standardController);
        controller.updateOrder();
        Test.stopTest();
    }
    
}