/**
 * File Info
 * ----------------------------------
 * @filename       OSM_Update_Order_RoleExt.cls
 * @created        14.OCT.2014
 * @author         Kristian Vegerano
 * @description    Class extension for updat Order Role custom button. 
 * @history        14.OCT.2014 - Kristian Vegerano - Created  
 */
public class OSM_UpdateOrderRoleExt{
    private OSM_Order_Role__c pageOrderRole;
    /**
     * @author         Kristian Vegerano
     * @description    Constructor. 
     * @history        14.OCT.2014 - Kristian Vegerano - Created  
     */
    public OSM_UpdateOrderRoleExt(ApexPages.StandardController stdController){
        pageOrderRole = (OSM_Order_Role__c)stdController.getRecord();

    }
    
    public PageReference updateOrderRole(){
        OSM_Order_Role__c updateOrderRole = [SELECT Id, OSM_Account__c, OSM_Contact__c, OSM_Delegate__c, OSM_Phone_and_Fax_Locked__c, OSM_Address_Affiliation__c, OSM_Role__c, OSM_All_Roles__c, OSM_Order__c FROM OSM_Order_Role__c WHERE Id = :pageOrderRole.Id];
        //Call on create logic - copies account/contact details to order role fields
        OSM_OrderRoleTriggerHandler.runAccountContactCopyOnce = true;
        update OSM_OrderRoleTriggerHandler.copyAccountContactDetails(new List<OSM_Order_Role__c>{updateOrderRole}, false, true, null);
        return new PageReference('/' + pageOrderRole.Id);
    }
}