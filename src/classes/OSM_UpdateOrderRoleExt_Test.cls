@isTest
private class OSM_UpdateOrderRoleExt_Test {

    static testMethod void updateOrderRole() {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        triggerSwitch.order_Role_Trigger__c = false;
        insert triggerSwitch;
        
        Account acct = OSM_DataFactory.createAccount('test');
        insert acct;
        
        Id recType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCPRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
        Id orderRoleRecType = Schema.SObjectType.OSM_Order_Role__c.getRecordTypeInfosByName().get('Account & Contact').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
         
        Contact con = OSM_DataFactory.createContact('test', 'test', recType);
        insert con;
        
        Contact conHCP = OSM_DataFactory.createContact('test', 'test', HCPRecType);
        insert conHCP;
        
        Order ordr = OSM_DataFactory.createOrder('test', con.Id, acct.Id, system.today(), 'Active');
        ordr.PriceBook2Id = pb2Standard;
        insert ordr;
        
        OSM_Order_Role__c ordrRole = OSM_DataFactory.createOrderRole(ordr.Id, acct.Id, conHCP.Id, 'Ordering',orderRoleRecType);
        insert ordrRole;
        
        Test.startTest();
            PageReference pageRef = Page.OSM_UpdateOrderRole;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ordrRole);
            ApexPages.currentPage().getParameters().put('Id',ordrRole.id);
            OSM_UpdateOrderRoleExt cons = new OSM_UpdateOrderRoleExt(sc);
            cons.updateOrderRole();
        Test.stopTest();
        
    }
}