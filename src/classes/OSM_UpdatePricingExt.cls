/*
    @author: Michiel Patricia M. Robrigado
    @date: 24 March 2015
    @description: Class for a custom button in account (OSM_Update_Pricing)
*/
global  class OSM_UpdatePricingExt{
    public boolean success {get; set;}
    public boolean errMsg {get; set;}
    public OSM_UpdatePricingExt(ApexPages.StandardController controller) {
        
    }
    webService static String updatePricing(ID payorId){
        //success = false;
        String result = '';
        //declaration of variables
        List<OSM_Payor_Test_Price__c> ptpList = new List<OSM_Payor_Test_Price__c>();
        List<OSM_Payor_Test_Price__c> ptpListUpdate = new List<OSM_Payor_Test_Price__c>();
        Set<ID> dummyaliIds = new Set<ID>();
        Set<ID> updatedPTPIds = new Set<ID>();
        Set<ID> takenPTP = new Set<ID>();
        Set<ID> takenALI = new Set<ID>();
        //PageReference accPage;
        
        //get the id 
        Id accId = payorId;
        
        //get the agreement list that matches to the account that is used
        Map<Id, Apttus__APTS_Agreement__c> agreemenMap = new Map<Id, Apttus__APTS_Agreement__c>([SELECT Apttus__Account__c, Id  FROM Apttus__APTS_Agreement__c WHERE Apttus__Account__c =: accId AND Name !=: Label.OSM_Dummy_Agreement_Name]);
        
        system.debug('test agreemenMap ' + agreemenMap.values());
        Account myAcct = [SELECT ID, CurrencyIsoCode, OSM_Payor_Category__c, GHI_CLM_Agreement_ID__c from Account where ID =: accId];
        
        if(myAcct != null && myAcct.GHI_CLM_Agreement_ID__c != null){
            if(!agreemenMap.isEmpty()){
                
                //get the ali list that matches to the agreementId that is used
                Map<Id, Apttus__AgreementLineItem__c> aliMap = new Map<Id, Apttus__AgreementLineItem__c>([SELECT Id, GHI_CPQ_Price_Method__c, GHI_CPQ_Billing_Cycle__c, CurrencyIsoCode, GHI_CPQ_Pricing_Schema__c, Active_Rate__c, GHI_CPQ_Billing_Category__c, GHI_CPQ_CPT_Code__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  Apttus_CMConfig__StartDate__c, GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus__ListPrice__c FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c IN: agreemenMap.keySet()]);
                
                Map<Id, Apttus__APTS_Agreement__c> agMap = new Map<Id, Apttus__APTS_Agreement__c>([SELECT ID, Apttus__Subtype__c, RecordTypeId from  Apttus__APTS_Agreement__c where ID IN: agreemenMap.keySet() ]);
                Set<ID> rtIds = new Set<ID>();
                for(Apttus__APTS_Agreement__c rid: agMap.values() ){
                    rtIds.add(rid.RecordTypeId);    
                }
                
                system.debug('test aliMap ' + aliMap.values());
                 Apttus__APTS_Agreement__c dummyAgreement = [SELECT ID, RecordTypeId, Apttus__Subtype__c from Apttus__APTS_Agreement__c where Name =: Label.OSM_Dummy_Agreement_Name];
                rtIds.add(dummyAgreement.RecordTypeId);
                Map<Id, RecordType> rtMap = new Map<Id, RecordType>([SELECT ID, Name from RecordType where ID IN: rtIds]);
                //get the ptp list that matches to the agreement line item Id that is used
                ptpList = [SELECT OSM_Agreement_ID__c, OSM_Billing_Cycle__c, OSM_Billing_Category__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c, OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__c, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c =: accId];
                 //Create 1 ALI, click on PTP sync and then on Update pricing
                //Delete the ALI click on PTP to ALI sync, PTP is removed and no default is left
                List<OSM_Payor_Test_Price__c> ptpListUpd0 = new List<OSM_Payor_Test_Price__c>();
                List<Apttus__AgreementLineItem__c> aliList = [SELECT Id, GHI_CPQ_Price_Method__c, Apttus__ProductId__r.Name, Apttus__ProductId__c, GHI_CPQ_Pricing_Schema__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, GHI_CPQ_CPT_Code__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus_CMConfig__StartDate__c, Apttus__ListPrice__c, GHI_CPQ_Billing_Cycle__c, GHI_CPQ_Billing_Category__c, CurrencyISOCode FROM Apttus__AgreementLineItem__c WHERE Apttus__AgreementId__c =: myAcct.GHI_CLM_Agreement_ID__c];
                for(OSM_Payor_Test_Price__c ptp :ptpList){
                    if(ptp.OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c != dummyAgreement.Id){
                        takenALI.add(ptp.OSM_Agreement_Line_Item_ID__c);   
                    }
                }
                for(OSM_Payor_Test_Price__c ptp :ptpList){
                    if(ptp.OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c == dummyAgreement.Id){
                        for(Apttus__AgreementLineItem__c ali :aliList){
                            if(ptp.OSM_Test__c == ali.Apttus__ProductId__c && ptp.OSM_Agreement_Line_Item_ID__c != ali.Id && !takenPTP.contains(ptp.Id) && !takenALI.contains(ali.Id)){
                                ptp.OSM_Agreement_ID__c = myAcct.GHI_CLM_Agreement_ID__c;
                                ptp.OSM_Agreement_Line_Item_ID__c = ali.Id;
                                ptp.OSM_Net_Price__c = ali.Apttus__NetPrice__c;
                                ptp.OSM_CPT_Code__c = ali.GHI_CPQ_CPT_Code__c;
                                ptp.CurrencyIsoCode = ali.CurrencyISOCode;
                                ptp.OSM_Billing_Cycle__c = ali.GHI_CPQ_Billing_Cycle__c;
                                ptp.OSM_Pricing_Schema__c = ali.GHI_CPQ_Pricing_Schema__c;
                                ptp.OSM_Start_Date__c = ali.Apttus_CMConfig__StartDate__c;
                                ptp.OSM_Price_Method__c = ali.GHI_CPQ_Price_Method__c;
                                takenPTP.add(ptp.Id);
                                takenALI.add(ali.Id);
                                ptpListUpd0.add(ptp);
                            }
                        }
                    }
                }
                if(!ptpListUpd0.isEmpty()){
                    try{
                        update ptpListUpd0;
                    }
                    catch(Exception ex){
                        result = 'failed';
                        return result;
                    }
                }
                
                ptpList = [SELECT OSM_Agreement_ID__c, OSM_Billing_Cycle__c, OSM_Billing_Category__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c =: accId];
                Map<String, Apttus__AgreementLineItem__c> dummyaliMap = new Map<String, Apttus__AgreementLineItem__c>();
                for(Apttus__AgreementLineItem__c ali: [SELECT Id, GHI_CPQ_Price_Method__c, Apttus__ProductId__r.Name, GHI_CPQ_Pricing_Schema__c, Apttus__AgreementId__c, Apttus_CMConfig__SubType__c, GHI_CLM_Expected_Rate__c, Apttus_CMConfig__EndDate__c, Apttus__NetPrice__c, GHI_CPQ_CPT_Code__c, Apttus_CMConfig__PriceMethod__c, Apttus_CMConfig__PriceType__c,  GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_Currency__c, Apttus_CMConfig__StartDate__c, Apttus__ListPrice__c, GHI_CPQ_Billing_Cycle__c, GHI_CPQ_Billing_Category__c, CurrencyISOCode FROM Apttus__AgreementLineItem__c  WHERE Apttus__AgreementId__c =: dummyAgreement.Id]){
                    if(!dummyAliMap.containsKey(ali.Apttus__ProductId__r.Name)){
                        dummyAliMap.put(ali.Apttus__ProductId__r.Name, ali);  
                        dummyaliIds.add(ali.Id);
                    }
                }
                system.debug('test ptpList ' + ptpList);
                system.debug('test dummyAliMap ' + dummyAliMap);
                //get the id of the ali and put it in a set
                if(ptpList.size()>0){
                    for(OSM_Payor_Test_Price__c ptp : ptpList){
                        //copies the value from ali to ptp
                        //ptp.OSM_Agreement_ID__c =  aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).Apttus__AgreementId__c;
                        if(/*aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).Active_Rate__c &&*/ptp.OSM_Agreement_Line_Item_ID__c != null && aliMap.containsKey(ptp.OSM_Agreement_Line_Item_ID__c) && !dummyaliIds.contains(aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).Id)){
                            ptp.OSM_End_Date__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).Apttus_CMConfig__EndDate__c;
                            ptp.OSM_Agreement_Record_Type__c=  rtMap.get(agMap.get(ptp.OSM_Agreement_ID__c).RecordTypeId).Name;
                            ptp.OSM_Net_Price__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).Apttus__NetPrice__c;
                            ptp.OSM_Price_Method__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Price_Method__c;
                            ptp.OSM_Pricing_Schema__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Pricing_Schema__c;
                            ptp.OSM_Start_Date__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).Apttus_CMConfig__StartDate__c;
                            ptp.OSM_List_Price__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).Apttus__ListPrice__c;
                            ptp.OSM_Quantity_End_Range__c =aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_End_Range__c;
                            ptp.OSM_Quantity_Start_Range__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Start_Range__c;
                            ptp.OSM_Assay_Score__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Assay_Score__c;
                            //ptp.OSM_Criteria__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Criteria__c;
                           // ptp.OSM_Currency__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Currency__c;
                            ptp.OSM_List_Price__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).Apttus__ListPrice__c;
                            ptp.OSM_Last_Pricing_Update__c = system.today();
                            ptp.OSM_Agreement_Sub_Type__c =  agMap.get(ptp.OSM_Agreement_ID__c).Apttus__Subtype__c;
                            ptp.OSM_Expected_Rate__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CLM_Expected_Rate__c;
                            ptp.OSM_Billing_Category__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Billing_Category__c;
                             ptp.OSM_Billing_Cycle__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Billing_Cycle__c;
                            ptp.CurrencyIsoCode = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).CurrencyISOCode;
                            ptp.OSM_CPT_Code__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_CPT_Code__c;
                            System.debug('\n\n\n StatusUpdate'+agMap.get(ptp.OSM_Agreement_ID__c).Apttus__Subtype__c);
                            ptp.OSM_Newly_Generated__c = false;
                            System.debug('\n\n\n StatusUpdated'+ptp.OSM_Agreement_Sub_Type__c);
                            ptpListUpdate.add(ptp);
                            
                        }
                        else{
                            /*
                            ptp.OSM_Agreement_ID__c =   dummyAgreement.Id ;
                            ptp.OSM_Agreement_Line_Item_ID__c =  dummyAliMap.get(ptp.OSM_Test__r.Name).Id;
                            
                            ptp.OSM_End_Date__c =  dummyAliMap.get(ptp.OSM_Test__r.Name).Apttus_CMConfig__EndDate__c;
                            ptp.OSM_Agreement_Record_Type__c=  rtMap.get(dummyAgreement.RecordTypeId).Name;
                            ptp.OSM_Net_Price__c = dummyAliMap.get(ptp.OSM_Test__r.Name).Apttus__NetPrice__c;
                            ptp.OSM_Price_Method__c = dummyAliMap.get(ptp.OSM_Test__r.Name).Apttus_CMConfig__PriceMethod__c;
                            ptp.OSM_Pricing_Schema__c = dummyAliMap.get(ptp.OSM_Test__r.Name).Apttus_CMConfig__PriceType__c;
                            ptp.OSM_Start_Date__c = dummyAliMap.get(ptp.OSM_Test__r.Name).Apttus_CMConfig__StartDate__c;
                            ptp.OSM_List_Price__c = dummyAliMap.get(ptp.OSM_Test__r.Name).Apttus__ListPrice__c;
                            ptp.OSM_Quantity_End_Range__c =dummyAliMap.get(ptp.OSM_Test__r.Name).GHI_CPQ_End_Range__c;
                            ptp.OSM_Quantity_Start_Range__c = dummyAliMap.get(ptp.OSM_Test__r.Name).GHI_CPQ_Start_Range__c;
                            ptp.OSM_Assay_Score__c = dummyAliMap.get(ptp.OSM_Test__r.Name).GHI_CPQ_Assay_Score__c;
                            //ptp.OSM_Criteria__c = aliMap.get(ptp.OSM_Agreement_Line_Item_ID__c).GHI_CPQ_Criteria__c;
                            ptp.OSM_Currency__c = dummyAliMap.get(ptp.OSM_Test__r.Name).GHI_CPQ_Currency__c;
                            ptp.OSM_List_Price__c = dummyAliMap.get(ptp.OSM_Test__r.Name).Apttus__ListPrice__c;
                            ptp.OSM_Last_Pricing_Update__c = system.today();
                            ptp.OSM_Agreement_Sub_Type__c =  dummyAgreement.Apttus__Subtype__c;
                            ptp.OSM_Expected_Rate__c = Decimal.valueOf(dummyAliMap.get(ptp.OSM_Test__r.Name).GHI_CLM_Expected_Rate__c);*/
                        }
                          
                           
                           updatedPTPIds.add(ptp.Id);
                        
                    }
                }
                
              
                
                if(ptpListUpdate.size()>0){
                    try{
                        update ptpListUpdate;
                    }
                    catch(Exception ex){
                        result = 'failed';
                        return result;
                    }
                 
                }
                //success = true;
                Datetime today = Datetime.now();
                Account acct = [SELECT ID, OSM_Last_Pricing_Update__c from Account where ID =: accId];
                acct.OSM_Last_Pricing_Update__c = Date.parse(today.format('MM/dd/yyyy'));
                if(acct!=null){
                    update acct;
                }
               boolean isDeleted = false;
               
               ptpListUpdate = [SELECT OSM_Agreement_ID__c, OSM_Payor__r.CurrencyIsoCode, OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name, OSM_CPT_Code__c, OSM_Future_Effective__c, OSM_Plan__c, OSM_Billing_Cycle__c, OSM_Billing_Category__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE ID IN: updatedPTPIds];
                
                //8.7.2015 patrick l fix (allow price book entries)
                String pbname = myAcct.CurrencyIsoCode + ' ' + myAcct.OSM_Payor_Category__c;
                List<PriceBookEntry> pbEntList = [SELECT Name, CurrencyIsoCode, Pricing_Category__c, UnitPrice, Pricebook2.Name, Product2.Name, ProductCode from PriceBookEntry where Pricing_Category__c =: myAcct.OSM_Payor_Category__c AND Pricebook2.Name =: pbname];
                Map<String, PriceBookEntry> pbTestMap = new Map<String, PriceBookEntry>();
                for(PriceBookEntry pbe: pbEntList){
                   pbTestMap.put(pbe.Name, pbe);
                }
               
               Map<Id,OSM_Payor_Test_Price__c> ptpListDelete = new Map<Id, OSM_Payor_Test_Price__c>();
                Map<String, List<OSM_Payor_Test_Price__c>> ptpListNotDeletePerTest = new Map<String,  List<OSM_Payor_Test_Price__c>>();
              List<OSM_Payor_Test_Price__c>  ptpListNotDelete = new List<OSM_Payor_Test_Price__c> ();
               List<OSM_Payor_Test_Price_History__c> ptphList= new List<OSM_Payor_Test_Price_History__c>();
               List<OSM_Payor_Test_Price__c> ptpdList= new List<OSM_Payor_Test_Price__c>();
               List<OSM_Payor_Test_Price__c> fePTPList= new List<OSM_Payor_Test_Price__c>();
               Map<String, OSM_Payor_Test_Price__c> ptpDummyMap = new Map<String, OSM_Payor_Test_Price__c>();
               Set<ID> takenPTPs = new Set<ID>();
                 system.debug('test ptpListUpdate ' + ptpListUpdate);
                 /*
               for(OSM_Payor_Test_Price__c ptp: ptpListUpdate){
                   if(ptp.OSM_Future_Effective__c && !ptpListNotDelete.containsKey(ptp.Id)){
                       ptpListNotDelete.put(ptp.Id, ptp);      
                   }
               }*/
               for(Integer a=0; a<ptpListUpdate.size(); a++){
                  if(!ptpListUpdate[a].OSM_Currently_Active__c && !ptpListUpdate[a].OSM_Future_Effective__c ){
                      ptpListDelete.put(ptpListUpdate[a].Id, ptpListUpdate[a]);      
                  }
                  else if (ptpListUpdate[a].OSM_Currently_Active__c){
                     System.debug('\n\n\n currently active detected'+ptpListUpdate[a].OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name);
                     ptpListNotDelete =  new List<OSM_Payor_Test_Price__c> ();
                     if(ptpListNotDeletePerTest.containsKey(ptpListUpdate[a].OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name)){
                        ptpListNotDelete = ptpListNotDeletePerTest.get(ptpListUpdate[a].OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name);
                     }
                      ptpListNotDelete.add(ptpListUpdate[a]); 
                      ptpListNotDeletePerTest.put(ptpListUpdate[a].OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name,ptpListNotDelete);
                  }
                  else if (ptpListUpdate[a].OSM_Future_Effective__c){
                       fePTPList.add(ptpListUpdate[a]);    
                  }
                  
               }
               System.debug('\n\n\n DELETEDMAP'+ptpListNotDelete);
               Set<String> delProds = new Set<String>();
               //try{
                    for(OSM_Payor_Test_Price__c ptp: ptpListDelete.values()){
                        //delProds.add(ptp.OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name) ;  
                        String prodName = ptp.OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name;
                        if(!ptpDummyMap.containsKey(prodName) ){
                            if((ptpListNotDeletePerTest.containsKey(prodName) && ptpListNotDeletePerTest.get(prodName).size() == 0) || !ptpListNotDeletePerTest.containsKey(prodName) ){
                                OSM_Payor_Test_Price__c ptpDummy1 = new OSM_Payor_Test_Price__c(OSM_Payor__c = ptp.OSM_Payor__c,
                                                                                                        OSM_Test__c = ptp.OSM_Test__c,
                                                                                                        OSM_Start_Date__c = dummyAliMap.get(prodName).Apttus_CMConfig__StartDate__c,
                                                                                                        OSM_End_Date__c =  dummyAliMap.get(prodName).Apttus_CMConfig__EndDate__c,
                                                                                                        OSM_Agreement_ID__c =  dummyAgreement.Id ,
                                                                                                        OSM_Agreement_Line_Item_ID__c= dummyAliMap.get(prodName).Id,
                                                                                                        OSM_Agreement_Record_Type__c=  rtMap.get(dummyAgreement.RecordTypeId).Name,
                                                                                                        OSM_Agreement_Sub_Type__c =  dummyAgreement.Apttus__Subtype__c,
                                                                                                        //OSM_Net_Price__c =    dummyAliMap.get(ptpListUpdate[a].OSM_Test__r.Name).Apttus__NetPrice__c,
                                                                                                        OSM_List_Price__c =   pbTestMap.get(prodName).UnitPrice,//dummyAliMap.get(prodName).Apttus__ListPrice__c,
                                                                                                        OSM_Price_Method__c =   dummyAliMap.get(prodName).GHI_CPQ_Price_Method__c,
                                                                                                        OSM_Pricing_Schema__c =   dummyAliMap.get(prodName).GHI_CPQ_Pricing_Schema__c,
                                                                                                        OSM_Quantity_End_Range__c =  dummyAliMap.get(prodName).GHI_CPQ_End_Range__c,
                                                                                                        OSM_Quantity_Start_Range__c =   dummyAliMap.get(prodName).GHI_CPQ_Start_Range__c,
                                                                                                        OSM_Assay_Score__c =   dummyAliMap.get(prodName).GHI_CPQ_Assay_Score__c,
                                                                                                        //OSM_Criteria__c = agreementliList[a].GHI_CPQ_Criteria__c,
                                                                                                        CurrencyIsoCode =   ptp.OSM_Payor__r.CurrencyIsoCode, //dummyAliMap.get(prodName).CurrencyISOCode,
                                                                                                        OSM_Expected_Rate__c =  dummyAliMap.get(prodName).GHI_CLM_Expected_Rate__c,
                                                                                                        OSM_Billing_Category__c =  dummyAliMap.get(prodName).GHI_CPQ_Billing_Category__c,
                                                                                                        OSM_CPT_Code__c =  dummyAliMap.get(prodName).GHI_CPQ_CPT_Code__c,
                                                                                                        OSM_Newly_Generated__c = true,
                                                                                                        OSM_Billing_Cycle__c =  dummyAliMap.get(prodName).GHI_CPQ_Billing_Cycle__c
                                                                                                     );
                                ptpDummyMap.put(prodName, ptpDummy1 );  
                            }
                        }
                        
                        OSM_Payor_Test_Price_History__c ptpHistory1 = new OSM_Payor_Test_Price_History__c(OSM_Payor__c = ptp.OSM_Payor__c,
                                                                                                    OSM_Test__c = ptp.OSM_Test__c,
                                                                                                    OSM_Start_Date__c = ptp.OSM_Start_Date__c,
                                                                                                    OSM_End_Date__c = ptp.OSM_End_Date__c,
                                                                                                    OSM_Agreement_ID__c = ptp.OSM_Agreement_ID__c,
                                                                                                    OSM_Agreement_Line_Item_ID__c= ptp.OSM_Agreement_Line_Item_ID__c,
                                                                                                    OSM_Agreement_Record_Type__c=  ptp.OSM_Agreement_Record_Type__c,
                                                                                                    OSM_Agreement_Sub_Type__c =  ptp.OSM_Agreement_Sub_Type__c,
                                                                                                    OSM_Net_Price__c =   ptp.OSM_Net_Price__c,
                                                                                                    OSM_List_Price__c =  ptp.OSM_List_Price__c,
                                                                                                    OSM_Price_Method__c =  ptp.OSM_Price_Method__c,
                                                                                                    OSM_Pricing_Schema__c =  ptp.OSM_Pricing_Schema__c,
                                                                                                    OSM_Quantity_End_Range__c = ptp.OSM_Quantity_End_Range__c,
                                                                                                    OSM_Quantity_Start_Range__c = ptp.OSM_Quantity_Start_Range__c,
                                                                                                    OSM_Assay_Score__c =  ptp.OSM_Assay_Score__c,
                                                                                                    //OSM_Criteria__c = agreementliList[a].GHI_CPQ_Criteria__c,
                                                                                                    CurrencyIsoCode =  ptp.CurrencyIsoCode,
                                                                                                    OSM_Expected_Rate__c =ptp.OSM_Expected_Rate__c,
                                                                                                    OSM_Billing_Category__c = ptp.OSM_Billing_Category__c,
                                                                                                    OSM_Billing_Cycle__c = ptp.OSM_Billing_Cycle__c,
                                                                                                    OSM_CPT_Code__c = ptp.OSM_CPT_Code__c,
                                                                                                    OSM_Future_Effective__c = ptp.OSM_Future_Effective__c,
                                                                                                    OSM_Plan__c = ptp.OSM_Plan__c
                                                                                                
                                                                                                 );  
                          ptphList.add(ptpHistory1);
                        
                        
                    }
               /*}
               catch(Exception ex){
                    System.debug('****ERROR'+ex.getMessage());
                    result = 'failed';
                    return result;
                 
               }*/
               System.debug('!@#ListFromController ' + fePTPList);
               for(OSM_Payor_Test_Price__c ptp: fePTPList){
                   String prodName = ptp.OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name;
                   //9/1/2015 added for test class 
                   System.debug('!!!dummyAliMap' + dummyAliMap.get(prodName));
                   if(!ptpDummyMap.containsKey(prodName) && ((ptpListNotDeletePerTest.containsKey(prodName) && ptpListNotDeletePerTest.get(prodName).size() == 0) || !ptpListNotDeletePerTest.containsKey(prodName) )){
                        OSM_Payor_Test_Price__c ptpDummy1 = new OSM_Payor_Test_Price__c(OSM_Payor__c = ptp.OSM_Payor__c,
                                                                                                        OSM_Test__c = ptp.OSM_Test__c,
                                                                                                        OSM_Start_Date__c = dummyAliMap.get(prodName).Apttus_CMConfig__StartDate__c,
                                                                                                        OSM_End_Date__c =  dummyAliMap.get(prodName).Apttus_CMConfig__EndDate__c,
                                                                                                        OSM_Agreement_ID__c =  dummyAgreement.Id ,
                                                                                                        OSM_Agreement_Line_Item_ID__c= dummyAliMap.get(prodName).Id,
                                                                                                        OSM_Agreement_Record_Type__c=  rtMap.get(dummyAgreement.RecordTypeId).Name,
                                                                                                        OSM_Agreement_Sub_Type__c =  dummyAgreement.Apttus__Subtype__c,
                                                                                                        //OSM_Net_Price__c =    dummyAliMap.get(ptpListUpdate[a].OSM_Test__r.Name).Apttus__NetPrice__c,
                                                                                                        OSM_List_Price__c =   pbTestMap.get(prodName).UnitPrice,//dummyAliMap.get(prodName).Apttus__ListPrice__c,
                                                                                                        OSM_Price_Method__c =   dummyAliMap.get(prodName).GHI_CPQ_Price_Method__c,
                                                                                                        OSM_Pricing_Schema__c =   dummyAliMap.get(prodName).GHI_CPQ_Pricing_Schema__c,
                                                                                                        OSM_Quantity_End_Range__c =  dummyAliMap.get(prodName).GHI_CPQ_End_Range__c,
                                                                                                        OSM_Quantity_Start_Range__c =   dummyAliMap.get(prodName).GHI_CPQ_Start_Range__c,
                                                                                                        OSM_Assay_Score__c =   dummyAliMap.get(prodName).GHI_CPQ_Assay_Score__c,
                                                                                                        //OSM_Criteria__c = agreementliList[a].GHI_CPQ_Criteria__c,
                                                                                                        CurrencyIsoCode =   ptp.OSM_Payor__r.CurrencyIsoCode, //dummyAliMap.get(prodName).CurrencyISOCode,
                                                                                                        OSM_Expected_Rate__c =  dummyAliMap.get(prodName).GHI_CLM_Expected_Rate__c,
                                                                                                        OSM_Billing_Category__c =  dummyAliMap.get(prodName).GHI_CPQ_Billing_Category__c,
                                                                                                        OSM_CPT_Code__c =  dummyAliMap.get(prodName).GHI_CPQ_CPT_Code__c,
                                                                                                        OSM_Newly_Generated__c = true,
                                                                                                        OSM_Billing_Cycle__c =  dummyAliMap.get(prodName).GHI_CPQ_Billing_Cycle__c
                                                                                                     );
                                ptpDummyMap.put(prodName, ptpDummy1 );      
                   }
                  
                   
               }
                try{
                    delete ptpListDelete.values();
                    insert ptphList;
                    insert ptpDummyMap.values();
                    
                    //insert ptpdList;
                }
                catch(Exception ex){
                    System.debug('****ERROR'+ex.getMessage());
                    result = 'failed';
                    return result;
                 
                }
                
                List<OSM_Payor_Test_Price__c> defaultPTPS = [SELECT OSM_Agreement_ID__c, OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name, OSM_Billing_Cycle__c, OSM_Billing_Category__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_Payor__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Agreement_Record_Type__c, OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_Test__c, OSM_Test__r.Name, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c =: accId AND OSM_Agreement_ID__c =: dummyAgreement.Id];
                List<OSM_Payor_Test_Price__c> deleteDefaultPTPs = new List<OSM_Payor_Test_Price__c>();
                for(OSM_Payor_Test_Price__c ptp: defaultPTPS){
                    String prodName = ptp.OSM_Agreement_Line_Item_ID__r.Apttus__ProductId__r.Name;
                    if(ptpListNotDeletePerTest.containsKey(prodName) && ptpListNotDeletePerTest.get(prodName).size() > 1){
                        deleteDefaultPTPs.add(ptp);
                    }
                    
                }
                
                try{
                    delete deleteDefaultPTPs;
                 
                }
                catch(Exception ex){
                    System.debug('****ERROR'+ex.getMessage());
                    result = 'failed';
                    return result;
                 
                }
              
            }
             result = 'success';
             return result;
                
        }
        else{
            //success= false;
            result = 'failed';
            return result;
        }
        //accPage = new  PageReference('/' + accId);
        //return accPage;
        
      
    }
}