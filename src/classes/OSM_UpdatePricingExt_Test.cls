@isTest(SeeAllData=true) 
public class OSM_UpdatePricingExt_Test {
    
    testmethod static void updatePricingTestGoodData(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        triggerSwitch.order_Role_Trigger__c = false;
        triggerSwitch.case_trigger__c = false;
        triggerSwitch.Payor_Test_Price_Trigger__c = false;
        triggerSwitch.Agreement_Trigger__c = false;
        update triggerSwitch;
        
        Record_Type__c rec = Record_Type__c.getOrgDefaults();
        update rec;
        
        List<Apttus__APTS_Agreement__c> dummyAgreement = [SELECT Id from Apttus__APTS_Agreement__c where Name = 'Payor Pricing Agreement' LIMIT 1];
        List<Apttus__AgreementLineItem__c> dummyALIs =[SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement'];
        
        Account payor = OSM_DataFactory.createPayorAccountWithBillingAddress(0,'2', 'Private','United States', 'street', 'city','province','111', Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId());
        payor.OSM_Status__c = 'Draft';
        insert payor;
        
        Apttus__APTS_Agreement__c agreementTest = dummyAgreement[0].clone(false, true, false, false);
        agreementTest.Name = 'Test Agreement';
        agreementTest.Apttus__Account__c = payor.Id;
        agreementTest.Apttus__Status_Category__c = 'In Effect';
        agreementTest.Apttus__Contract_Start_Date__c = system.today();
        agreementTest.Apttus__Contract_End_Date__c = system.today()+10;
        
        insert agreementTest;
        
        payor.GHI_CLM_Agreement_ID__c = agreementTest.Id;
        payor.OSM_Status__c = 'Approved';
        update payor;
        
        List<Product2> testProd = [SELECT ID, name from Product2 where name = 'Colon'];
        
        Product2 prod2 = OSM_DataFactory.createProduct('Colon', true);
        insert prod2;
        
        Product2 prod3 = OSM_DataFactory.createProduct('MMR', true);
        insert prod3;
        
        Datetime today = Datetime.now();
        Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
        Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) + Integer.valueOf(1000 );   
        Apttus__AgreementLineItem__c realALI1 = dummyALIs[0].clone(false, true, false, false);
        
        realALI1.Apttus__AgreementId__c = agreementTest.Id;
        realALI1.Apttus__ProductId__c = testProd[0].Id;
        realALI1.Apttus__ListPrice__c = 1.00;
        realALI1.GHI_CPQ_Billing_Category__c = 'test billcat1';
        realALI1.GHI_CPQ_CPT_Code__c = '123';
        realALI1.CurrencyIsoCode = 'USD';
        realALI1.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        realALI1.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        realALI1.Apttus__NetPrice__c = 1.00;
        realALI1.Apttus_CMConfig__StartDate__c = system.today();
        realALI1.Apttus_CMConfig__EndDate__c = system.today();
        realALI1.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        insert realALI1;
        
        today = Datetime.now();
        defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(2000);        
        defDateEnd = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1250 );   
        
        Apttus__AgreementLineItem__c realALI2 = dummyALIs[0].clone(false, true, false, false);
        realALI2.Apttus__AgreementId__c = agreementTest.Id;
        realALI2.Apttus__ProductId__c = testProd[0].Id;
        realALI2.Apttus__ListPrice__c = 1.00;
        realALI2.GHI_CPQ_Billing_Category__c = 'test billcat1';
        realALI2.GHI_CPQ_CPT_Code__c = '123';
        realALI2.CurrencyIsoCode = 'USD';
        realALI2.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        realALI2.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        realALI2.Apttus__NetPrice__c = 1.00;
        realALI2.Apttus_CMConfig__StartDate__c = System.Today() + 100;
        realALI2.Apttus_CMConfig__EndDate__c = System.Today() + 10;
        realALI2.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        
        insert realALI2;
        
        Apttus__AgreementLineItem__c realALI3 = dummyALIs[0].clone(false, true, false, false);
        realALI3.Apttus__AgreementId__c = agreementTest.Id;
        realALI3.Apttus__ProductId__c = prod3.Id;
        realALI3.Apttus__ListPrice__c = 1.00;
        realALI3.GHI_CPQ_Billing_Category__c = 'test billcat1';
        realALI3.GHI_CPQ_CPT_Code__c = '123';
        realALI3.CurrencyIsoCode = 'USD';
        realALI3.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        realALI3.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        realALI3.Apttus__NetPrice__c = 1.00;
        realALI3.Apttus_CMConfig__StartDate__c = System.Today() + 100;
        realALI3.Apttus_CMConfig__EndDate__c = System.Today() + 10;
        realALI3.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        
        insert realALI3;
        
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payor.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = false,
            OSM_Start_Date__c = System.Today() + 100,
            OSM_End_Date__c = System.Today() + 10,
            OSM_Agreement_Line_Item_ID__c = realALI2.Id,
            OSM_Agreement_ID__c = agreementTest.Id,
            OSM_Test__c = testProd[0].Id
            
        );
        insert ptp;
        
         OSM_Payor_Test_Price__c ptp2 = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payor.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = false,
            OSM_Start_Date__c = System.Today(),
            OSM_End_Date__c = System.Today() + 10,
            OSM_Agreement_Line_Item_ID__c = realALI3.Id,
            OSM_Agreement_ID__c = agreementTest.Id,
            OSM_Test__c = testProd[0].Id
            
        );
        insert ptp2;
        
        /*OSM_Payor_Test_Price__c ptp3 = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payor.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = false,
            OSM_Start_Date__c = System.Today()-1,
            OSM_End_Date__c = System.Today() -1,
            OSM_Agreement_Line_Item_ID__c = realALI1.Id,
            OSM_Agreement_ID__c = agreementTest.Id,
            OSM_Test__c = testProd[0].Id
            
        );
        insert ptp3;*/
        
        OSM_Payor_Test_Price__c ptpAfterInsert = [SELECT Id, OSM_Start_Date__c, OSM_Future_Effective__c, OSM_Currently_Active__c FROM OSM_Payor_Test_Price__c WHERE Id = :ptp.Id];
        
        System.assertEquals(ptpAfterInsert.OSM_Future_Effective__c, true);
        
        
         // try{
            test.startTest();
            OSM_UpdatePricingExt.updatePricing(payor.Id);
            realALI2.Apttus_CMConfig__StartDate__c = defDateStart;
            realALI2.Apttus_CMConfig__EndDate__c = defDateEnd;
            update realALI2;
            OSM_UpdatePricingExt.updatePricing(payor.Id);
            test.stopTest();
            
        // }
        //  catch(Exception ex){
            //  OSM_PTPALISyncRemote.ptpALISync(agreementTest.Id, payor.Id);
            //  OSM_UpdatePricingExt.updatePricing(payor.Id);
         //}
        
        
    }
    
     testmethod static void updatePricingTestBadData(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        triggerSwitch.order_Role_Trigger__c = false;
        triggerSwitch.case_trigger__c = false;
        triggerSwitch.Payor_Test_Price_Trigger__c = false;
        triggerSwitch.Agreement_Trigger__c = false;
        update triggerSwitch;
        
        List<Apttus__APTS_Agreement__c> dummyAgreement = [SELECT Id from Apttus__APTS_Agreement__c where Name = 'Payor Pricing Agreement' LIMIT 1];
        List<Apttus__AgreementLineItem__c> dummyALIs =[SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement'];
        
        Account payor = OSM_DataFactory.createPayorAccountWithBillingAddress(0,'2', 'Private','United States', 'street', 'city','province','111', Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId());
        payor.OSM_Status__c = 'Draft';
        insert payor;
        
        
        Apttus__APTS_Agreement__c agreementTest = dummyAgreement[0].clone(false, true, false, false);
        agreementTest.Name = 'Test Agreement';
        agreementTest.Apttus__Account__c = payor.Id;
        agreementTest.Apttus__Perpetual__c = false;
        
        insert agreementTest;
        
        payor.GHI_CLM_Agreement_ID__c = agreementTest.Id;
        payor.OSM_Status__c = 'Approved';
        update payor;
        
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payor.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = true,
            OSM_Start_Date__c = System.Today() + 100,
            OSM_End_Date__c = System.Today() + 10
        );
        insert ptp;
        
        OSM_Payor_Test_Price__c ptpAfterInsert = [SELECT Id, OSM_Start_Date__c, OSM_Future_Effective__c, OSM_Currently_Active__c FROM OSM_Payor_Test_Price__c WHERE Id = :ptp.Id];
        
        
        System.assertEquals(ptpAfterInsert.OSM_Currently_Active__c, true);

        List<Product2> testProd = [SELECT ID, name from Product2 where name = 'Colon'];
        
        Datetime today = Datetime.now();
        Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
        Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) + Integer.valueOf(1000 );   
        Apttus__AgreementLineItem__c realALI1 = dummyALIs[0].clone(false, true, false, false);
        
        realALI1.Apttus__AgreementId__c = agreementTest.Id;
        realALI1.Apttus__ProductId__c = testProd[0].Id;
      
        
        insert realALI1;
        
        today = Datetime.now();
        defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(2000);        
        defDateEnd = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1250 );   
        
        Apttus__AgreementLineItem__c realALI2 = dummyALIs[0].clone(false, true, false, false);
        realALI2.Apttus__AgreementId__c = agreementTest.Id;
        realALI2.Apttus__ProductId__c = testProd[0].Id;
        
        
        
        insert realALI2;
        
        ApexPages.currentPage().getParameters().put('id',ptp.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ptp); 
        //OSM_PTPALISyncRemote.ptpALISync(agreementTest.Id, payor.Id);
        OSM_UpdatePricingExt a = new OSM_UpdatePricingExt(sc);
        a.errMsg = false;
        a.success = true;
        OSM_UpdatePricingExt.updatePricing(payor.Id);
        
    }

}