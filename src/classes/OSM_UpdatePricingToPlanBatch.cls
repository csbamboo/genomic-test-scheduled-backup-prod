/*
  @author: Michiel Patricia M. Robrigado
  @date: 31 AUG 2015
  @description: Batch class for updating the payor test price
*/
global class OSM_UpdatePricingToPlanBatch implements Database.Batchable<sObject>, Database.Stateful {

    

    //declaration of variables
    global List<MyWrapper> wrapperList = new List<MyWrapper>();
    global Boolean isReRun {get;set;}
    global final String Query;
    public static Boolean batchIsRunnng = false;
    
    //wrapper to get the id, name and the error of the batch
    public Class MyWrapper{
        String id;
        String name;
        String errors;
        public MyWrapper(String id, String name, String errors){
            this.id = id;
            this.name = name;
            this.errors = errors;
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //declaration of variables
        String soqlString = '';
        Id payorRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        system.debug('payorRTId ' + payorRTId);
        system.debug('test isReRun ' + isReRun);
        if(!isReRun) {
            //for the query
            //soqlString = 'SELECT Id, Name, OSM_Agreement_ID__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c, OSM_Payor__c FROM OSM_Payor_Test_Price__c';
            //   soqlString = 'SELECT Id, OSM_Payor_Category__c, CurrencyIsoCode, (SELECT Id FROM Plans__r) FROM Account WHERE OSM_Status__c =  \'Approved\' AND GHI_CLM_Agreement_ID__c != null';
              soqlString = 'SELECT Id, OSM_Payor_Category__c, CurrencyIsoCode, (SELECT Id FROM Plans__r) FROM Account WHERE OSM_Status__c =  \'Approved\' AND RecordType.Name = \'Payor\' ';
        } else {
            Set<String> errorIds = new Set<String>();
            List<Error_in_Batch__c> eb = [Select Record_Ids__c From Error_in_Batch__c Where Object__c = 'Account' Order By CreatedDate DESC Limit 1];
            system.debug('test eb ' + eb);
            if(eb.size()>0) {
                for(String idLop : eb[0].Record_Ids__c.split(',')){
                    errorIds.add(idLop);
                }
                //for the query
                //soqlString = 'SELECT Id, Name, OSM_Agreement_ID__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c, OSM_Payor__c FROM OSM_Payor_Test_Price__c WHERE Id IN: errorIds';
                // soqlString = 'SELECT Id, OSM_Payor_Category__c, CurrencyIsoCode, (SELECT Id FROM Plans__r) FROM Account WHERE OSM_Status__c =  \'Approved\' AND GHI_CLM_Agreement_ID__c != null AND Id IN: errorIds';
                soqlString = 'SELECT Id, OSM_Payor_Category__c, CurrencyIsoCode, (SELECT Id FROM Plans__r) FROM Account WHERE OSM_Status__c =  \'Approved\' AND RecordType.Name = \'Payor\' AND Id IN: errorIds';
                
            } else {
                //for the query
                //soqlString = 'SELECT Id, Name, OSM_Agreement_ID__c, OSM_Agreement_Sub_Type__c, OSM_Agreement_Line_Item_ID__c, OSM_End_Date__c, OSM_Net_Price__c, OSM_Price_Method__c, OSM_Pricing_Schema__c,  OSM_Start_Date__c, OSM_List_Price__c, OSM_Payor__c FROM OSM_Payor_Test_Price__c';
                //  soqlString = 'SELECT Id, OSM_Payor_Category__c, CurrencyIsoCode, (SELECT Id FROM Plans__r) FROM Account WHERE OSM_Status__c =  \'Approved\' AND GHI_CLM_Agreement_ID__c != null ';
                 soqlString = 'SELECT Id, OSM_Payor_Category__c, CurrencyIsoCode, (SELECT Id FROM Plans__r) FROM Account WHERE OSM_Status__c =  \'Approved\' AND RecordType.Name = \'Payor\' ';
                
            }
        }
        system.debug('test soqlString ' + soqlString);
        return Database.getQueryLocator(soqlString);
    }
    /*
      @author: Michie Patricia M. Robrigado
      @date: 31 AUG 2015
      @description: Batch class for updating the payor test price
      */
    global void execute(Database.BatchableContext BC,List<Account> scope){
        System.debug('\n\n\n PAYORS'+scope);
        updatePricingToPlanBatchClone(scope);
        
        List<Account> filtScope = new List<Account>();
        //updatePricingToPlanBatchClone(scope);  
        
        for(Account sc: scope){
            if(sc.Plans__r.size() >0){
                filtScope.add(sc);
            }
        }
        generateDefaultPTPToPLan(filtScope);
        updatePricingToPlanBatchClone(filtScope);
        
    }

    global void finish(Database.BatchableContext BC){
    }
    //method to get the error message per id
    public String parseErrorReturnId(String errorMsg){
        Integer indexCount = errorMsg.lastIndexOf('with id')+8;
        return errorMsg.substring(indexCount, indexCount+18);
    }
    
    //delete the existing real ptp in plan
    public void updatePricingToPlanBatchDelete(List<OSM_Plan__c> planList){
        system.debug('test enter del update pric');
      List<OSM_Payor_Test_Price__c> delPTPList = new List<OSM_Payor_Test_Price__c>();
      for(OSM_Payor_Test_Price__c ptp : [SELECT Id FROM OSM_Payor_Test_Price__c WHERE OSM_Plan__c IN: planList AND OSM_Pricing_Schema__c != 'default']){
        delPTPList.add(ptp);
      }
      system.debug('test delPTPList ' + delPTPList);
      delete delPTPList;
    }
    
    //generate default ptp in plan
    public void generateDefaultPTPToPLan(List<Account> accts){
        system.debug('test enter generateDefaultPTPToPLan batch');
        Set<Id> planIds = new Set<Id>();
        List<String> prodStrList = Label.OSM_Product_List.split('::');
        Map<String, Id> ptpProdMapPerPlan= new Map<String,Id>();
        List<String>pbCatList = new List<String>();
        List<String>pbNameList = new List<String>();
        for(Account act: accts){
           
            for(OSM_Plan__c pln: act.Plans__r){
                //planList.add(pln);    
                planIds.add(pln.Id);
            }
            pbCatList.add(act.OSM_Payor_Category__c);
            pbNameList.add(act.CurrencyIsoCode + ' ' + act.OSM_Payor_Category__c); 
        }
      List<Product2> prodList = [SELECT ID, name from Product2 where Name IN: prodStrList];
      Map<Id, RecordType> rtMap = new Map<Id, RecordType>([Select Id, Name from RecordType where SObjecttype = 'Apttus__APTS_Agreement__c']);
      //  List<OSM_Payor_Test_Price__c> ptpList = [SELECT Id, OSM_List_Price__c,  ]
      List<OSM_Plan__c> planList1 =  [SELECT Id, OSM_Payor__r.CurrencyIsoCode, OSM_Payor__r.OSM_Payor_Category__c, (Select Id from Payor_Test_Prices__r) from OSM_Plan__c where ID IN:planIds ];
      Apttus__APTS_Agreement__c dummyAgreement = [SELECT ID, Apttus__Subtype__c, RecordTypeId from Apttus__APTS_Agreement__c where Name =: Label.OSM_Dummy_Agreement_Name];
      List<Apttus__AgreementLineItem__c> aliList = [SELECT ID, Apttus__NetPrice__c, Apttus__ListPrice__c, Apttus_CMConfig__EndDate__c, GHI_CPQ_Price_Method__c, GHI_CPQ_Pricing_Schema__c, Apttus_CMConfig__StartDate__c,GHI_CPQ_End_Range__c, GHI_CPQ_Start_Range__c, GHI_CPQ_Assay_Score__c, GHI_CPQ_CPT_Code__c, GHI_CLM_Expected_Rate__c, GHI_CPQ_Billing_Category__c, GHI_CPQ_Billing_Cycle__c, Apttus__ProductId__r.Name from Apttus__AgreementLineItem__c where Apttus__AgreementId__c =: dummyAgreement.Id];
      Map<String, id> prodListByName = new Map<String,Id>();
      Map<String, Apttus__AgreementLineItem__c> aliListByProdName = new Map<String,Apttus__AgreementLineItem__c>();
      for(OSM_Payor_Test_Price__c ptp: [SELECT ID, OSM_Plan__c, OSM_Test__r.Name from OSM_Payor_Test_Price__c where OSM_Agreement_ID__c =: dummyAgreement.Id AND OSM_Plan__c IN: planIds AND OSM_Test__r.Name IN: prodStrList]){
        ptpProdMapPerPlan.put(ptp.OSM_Plan__c+'::'+ptp.OSM_Test__r.Name, ptp.Id);    
      }
      for(Product2 prd: prodList){
          prodListByName.put(prd.Name, prd.Id);
      }
      
      for(Apttus__AgreementLineItem__c ali: aliList){
          aliListByProdName.put(ali.Apttus__ProductId__r.Name, ali);
      }
      List<PriceBookEntry> pbEntList = [SELECT Name, CurrencyIsoCode, Pricing_Category__c, UnitPrice, Pricebook2.Name, Product2.Name, ProductCode from PriceBookEntry where Pricing_Category__c IN : pbCatList AND Pricebook2.Name IN : pbNameList];
        
      Map<String, PriceBookEntry> pbTestMap = new Map<String, PriceBookEntry>();
      for(PriceBookEntry pbe: pbEntList){
        pbTestMap.put(pbe.Pricebook2.Name + '::'+ pbe.Product2.Name, pbe);
      }
      List<OSM_Payor_Test_Price__c> ptpUpsert = new List<OSM_Payor_Test_Price__c>();
      for(OSM_Plan__c pln: planList1){
          for(String prd: prodStrList){
              OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c();
              String pbName = pln.OSM_Payor__r.CurrencyIsoCode + ' ' + pln.OSM_Payor__r.OSM_Payor_Category__c;
              ptp.Id = ptpProdMapPerPlan.containsKey(pln.Id+'::'+prd) ? ptpProdMapPerPlan.get(pln.Id+'::'+prd) : null;
              ptp.OSM_Test__c = prodListByName.containsKey(prd) ? prodListByName.get(prd): null;
              ptp.OSM_Agreement_ID__c = dummyAgreement.Id;
              ptp.OSM_Agreement_Line_Item_ID__c = aliListByProdName.get(prd).Id;
              ptp.OSM_Agreement_Record_Type__c = rtMap.containsKey(dummyAgreement.RecordTypeId) ? rtMap.get(dummyAgreement.RecordTypeId).Name : null;
              ptp.OSM_Agreement_Sub_Type__c = dummyAgreement.Apttus__Subtype__c;
              ptp.OSM_List_Price__c = pbTestMap.containsKey(pbName + '::'+prd) ?  pbTestMap.get(pbName + '::'+prd).UnitPrice : 0;
              ptp.OSM_End_Date__c = aliListByProdName.get(prd).Apttus_CMConfig__EndDate__c;
              ptp.OSM_Price_Method__c = aliListByProdName.get(prd).GHI_CPQ_Price_Method__c;
              ptp.OSM_Pricing_Schema__c = aliListByProdName.get(prd).GHI_CPQ_Pricing_Schema__c;
              ptp.OSM_Start_Date__c = aliListByProdName.get(prd).Apttus_CMConfig__StartDate__c;
              ptp.OSM_Quantity_End_Range__c = aliListByProdName.get(prd).GHI_CPQ_End_Range__c;
              ptp.OSM_Quantity_Start_Range__c = aliListByProdName.get(prd).GHI_CPQ_Start_Range__c;
              ptp.OSM_Assay_Score__c = aliListByProdName.get(prd).GHI_CPQ_Assay_Score__c;
              ptp.OSM_CPT_Code__c = aliListByProdName.get(prd).GHI_CPQ_CPT_Code__c;
              ptp.OSM_Expected_Rate__c = aliListByProdName.get(prd).GHI_CLM_Expected_Rate__c;
              ptp.OSM_Billing_Category__c = aliListByProdName.get(prd).GHI_CPQ_Billing_Category__c;
              ptp.OSM_Billing_Cycle__c = aliListByProdName.get(prd).GHI_CPQ_Billing_Cycle__c;
              if(pln.OSM_Payor__r.CurrencyIsoCode == null){
                 System.debug('\n\n\n PTPPlanCurrencyPayorId'+pln.OSM_Payor__c);    
              }
             
              ptp.CurrencyIsoCode = pln.OSM_Payor__r.CurrencyIsoCode;
              ptp.OSM_Newly_Generated__c = true;
              ptp.OSM_Plan__c = pln.Id;
              ptpUpsert.add(ptp);
          }
      }
      
      if(!ptpUpsert.isEmpty()){
          upsert ptpUpsert;
      }
      
      /*
      for(OSM_Plan__c plan: planList){
          OSM_Payor_Test_Price__c ptp =  new OSM_Payor_Test_Price__c ();
          p
      }   */ 
        
        
    }
    
    public void updatePricingToPlanBatchClone(List<Account> accList){
        system.debug('test enter update batch');
        Set<Id> accId = new Set<Id>();
        Map<Id, List<OSM_Payor_Test_Price__c> > accPTPMap = new Map<Id, List<OSM_Payor_Test_Price__c> >();
        Map<Id, List<OSM_Plan__c> > accPlanMap = new Map<Id, List<OSM_Plan__c> >();
        
        List<Account> accIdListCombine = [SELECT Id, (SELECT Id From Payor_Test_Prices__r), (SELECT Id From Plans__r) FROM Account WHERE Id IN: accList];
        
        system.debug('test accIdListCombine ' + accIdListCombine);
        for(Account acc : accIdListCombine){
            accId.add(acc.Id);
        }
        system.debug('test accId ' + accId);
      List<OSM_Payor_Test_Price__c> ptpList = [SELECT Id, OSM_Payor__c, OSM_Active_Tiered_Rate__c, OSM_Agreement_ID__c, OSM_Agreement_Line_Item_ID__c, OSM_End_Date__c,
                  OSM_Start_Date__c, OSM_Assay_Score__c, OSM_Billing_Category__c, OSM_Billing_Cycle__c, OSM_Net_Price__c, OSM_CPQ_Criteria__c, OSM_CPT_Code__c,
                  OSM_Currently_Active__c, OSM_Expected_Rate__c, OSM_Future_Effective__c, OSM_Last_Pricing_Update__c, OSM_List_Price__c, OSM_Newly_Generated__c,
                  OSM_Plan__c, OSM_Price_Method__c, OSM_Pricing_Schema__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Test__c, OSM_Test_ID__c,
                  OSM_Test__r.Name FROM OSM_Payor_Test_Price__c WHERE OSM_Payor__c IN: accList];

      List<OSM_Plan__c> planList = [SELECT Id, OSM_Payor__c, OSM_Price_Exception__c, OSM_QDX_Financial_Category__c FROM OSM_Plan__c WHERE OSM_Payor__c IN: accList];
      
      //delete the existing real ptp in plan
      updatePricingToPlanBatchDelete(planList);
      //generateDefaultPTPToPLan(accList);
      /*for(OSM_Payor_Test_Price__c ptp: ptpList){
          if(ptp.OSM_Plan__c == null){
              system.debug('test enter generate batch');
              generateDefaultPTPToPLan(accList);
          }
      }*/

    
    List<OSM_Payor_Test_Price__c> clonedPTP = new List<OSM_Payor_Test_Price__c>();
    List<OSM_Payor_Test_Price__c> deleteRecordOnPlan = new List<OSM_Payor_Test_Price__c>();
    List<OSM_Payor_Test_Price__c> clonedPTPNew = new List<OSM_Payor_Test_Price__c>();
    Set<ID> planIds2 = new Set<ID>();
    Map<Id, Set<String>> mapPlanIdTestIds = new  Map<Id, Set<String>>();

      /*for(OSM_Payor_Test_Price__c ptp : ptpList){
          if(accPTPMap.containsKey(ptp.OSM_Payor__c)){
              accPTPMap.get(ptp.OSM_Payor__c).add(ptp);
          }
          else{
              accPTPMap.put(ptp.OSM_Payor__c, new List<OSM_Payor_Test_Price__c>{ptp});
          }
      }

      for(OSM_Plan__c plan : planList){
          if(accPTPMap.containsKey(plan.OSM_Payor__c)){
              accPlanMap.get(plan.OSM_Payor__c).add(plan);
          }
          else{
              accPlanMap.put(plan.OSM_Payor__c, new List<OSM_Plan__c>{plan});
          }
      }*/
        system.debug('test ptpList ' + ptpList);
      for(OSM_Payor_Test_Price__c ptp: ptpList){
          system.debug('test enter loop');
             for(OSM_Plan__c plan: planList){
                 if(ptp.OSM_Payor__c == plan.OSM_Payor__c){
                     if(plan.OSM_Price_Exception__c == null || (plan.OSM_Price_Exception__c != null && !plan.OSM_Price_Exception__c.contains(ptp.OSM_Test__r.Name))){
                    //if(plan.OSM_Price_Exception__c == null){
                           if(plan.OSM_QDX_Financial_Category__c == 'COMM' &&  ptp.OSM_Pricing_Schema__c == 'Commercial' ){
                                system.debug('test enter 1');
                                system.debug('test mapPlanIdTestIds.containsKey(plan.Id) enter 1' + mapPlanIdTestIds.containsKey(plan.Id));
                                system.debug('test ptp.OSM_Future_Effective__c ' + ptp.OSM_Future_Effective__c);
                                
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 1');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 1');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                           else if (plan.OSM_QDX_Financial_Category__c == 'MCRA' &&  ptp.OSM_Pricing_Schema__c == 'Medicare Advantage'){
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 2');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 2');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                           else if (plan.OSM_QDX_Financial_Category__c == 'MAMC' &&  ptp.OSM_Pricing_Schema__c == 'Managed Medicaid'){
                               
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 3');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 3');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                           else if (plan.OSM_QDX_Financial_Category__c == 'BCBS' && ptp.OSM_Pricing_Schema__c == 'Commercial'){
                                
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 4');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 4');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                           else if(plan.OSM_QDX_Financial_Category__c == 'COMM' &&  ptp.OSM_Pricing_Schema__c == 'TriCare' ){
                                
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 5');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 5');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                           else if (plan.OSM_QDX_Financial_Category__c == 'EXCH' && ptp.OSM_Pricing_Schema__c == 'Commercial'){
                                
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 6');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 6');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                           //Mika: added filters for cr: 58343 Additional financial category on the plan requires mapping to PTPs
                           else if (plan.OSM_QDX_Financial_Category__c == 'GOVT' && ptp.OSM_Pricing_Schema__c == 'Government'){
                                
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 7');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 7');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                           else if (plan.OSM_QDX_Financial_Category__c == 'GOVT' &&  ptp.OSM_Pricing_Schema__c == 'Medicare'){
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 8');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 8');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                           else if (plan.OSM_QDX_Financial_Category__c == 'GOVT' &&  ptp.OSM_Pricing_Schema__c == 'Medicaid'){
                               
                                if(mapPlanIdTestIds.containsKey(plan.Id)){
                                    system.debug('test enter if 9');
                                    mapPlanIdTestIds.get(plan.Id).add(ptp.OSM_Test__r.Name);
                                    //testId.add(ptp.OSM_Test__c);
                                } else {
                                    system.debug('test enter else 9');
                                    mapPlanIdTestIds.put(plan.Id, new Set<String>{ptp.OSM_Test__r.Name});
                                    //testId.add(ptp.OSM_Test__c);
                                }
                                OSM_Payor_Test_Price__c clonedPTPRec = ptp.clone(false, true);
                                clonedPTPRec.OSM_Payor__c = null;
                                clonedPTPRec.OSM_Plan__c = plan.Id;
                                clonedPTP.add(clonedPTPRec);
                           }
                    }
                }
            
        }

    }

    Set<String> excludeTest = new Set<String>();
         Set<Id> excludePlanId = new Set<Id>();
         List<OSM_Payor_Test_Price__c> filteredPTPFE = new List<OSM_Payor_Test_Price__c>();
         List<OSM_Payor_Test_Price__c> filteredPTPCE = new List<OSM_Payor_Test_Price__c>();
         Boolean ceFlag = false;
         Map<Id, List<OSM_Payor_Test_Price__c>> ptpMapFE = new Map<Id, List<OSM_Payor_Test_Price__c>>();
         Map<Id, Map<String, Set<OSM_Payor_Test_Price__c>>> ptpMapCE = new Map<Id, Map<String, Set<OSM_Payor_Test_Price__c>>>();

         for(OSM_Payor_Test_Price__c ptp: clonedPTP){
            if(ptp.OSM_Future_Effective__c){
                    if(ptpMapFE.containsKey(ptp.OSM_Plan__c)){
                            ptpMapFE.get(ptp.OSM_Plan__c).add(ptp);
                        } else {
                            system.debug('test enter exclude id fe');
                      //filteredPTPFE.add(ptp);
                      ptpMapFE.put(ptp.OSM_Plan__c, new List<OSM_Payor_Test_Price__c>{ptp});
                        }
              
            }
            if(ptp.OSM_Currently_Active__c){
              //  if(ptpMapCE.containsKey(ptp.OSM_Plan__c)){
               //   system.debug('test enter other ce');
            //             ptpMapCE.get(ptp.OSM_Plan__c).add(ptp.OSM_Test__r.Name, ptp);
            //         } else {
            //         system.debug('test enter exclude id ce');
            //      //filteredPTPFE.add(ptp);
            //      ptpMapCE.put(ptp.OSM_Plan__c, new Map<String{ptp.OSM_Test__r.Name}, Set<OSM_Payor_Test_Price__c>{ptp}>);
            //         }
            
                    //if plan is null
            if(!ptpMapCE.containsKey(ptp.OSM_Plan__c)) {
              ptpMapCE.put(ptp.OSM_Plan__c, new Map<String, Set<OSM_Payor_Test_Price__c>>());

            }
            
            //if test name is null
            if(!ptpMapCE.get(ptp.OSM_Plan__c).containsKey(ptp.OSM_Test__r.Name)) {
              ptpMapCE.get(ptp.OSM_Plan__c).put(ptp.OSM_Test__r.Name, new Set<OSM_Payor_Test_Price__c>());
            }
                    ptpMapCE.get(ptp.OSM_Plan__c).get(ptp.OSM_Test__r.Name).add(ptp);

            }
         }
        
         /*if(filteredPTPFE.size()>0){
             system.debug('test enter filteredPTPFE');
            for(OSM_Payor_Test_Price__c ptp: filteredPTPFE){
                system.debug('test enter FE clone');
                clonedPTPNew.add(ptp);
            }
         }*/
         
         for(Id ptpFE: ptpMapFE.keySet()){
             if(ptpMapFE.get(ptpFE).size()>0){
                 system.debug('test ptpMapFE.keySet() ' + ptpMapFE.keySet());
                 clonedPTPNew.addAll(ptpMapFE.get(ptpFE));
             }
         }
         
         for(Id ptpCE: ptpMapCE.keySet()){  // iterate through plan
             
             for(String testName : ptpMapCE.get(ptpCE).keySet()) { // iterate through test in plan
                 if(ptpMapCE.get(ptpCE).get(testName).size() > 1) {
                    ceFlag = true;
                     for(OSM_Payor_Test_Price__c ptp: ptpMapCE.get(ptpCE).get(testName)) {  // get OSM_Payor_Test_Price__c in test
                        if(ptp.OSM_Price_Method__c != 'Tiered Rate'){
                            ceFlag = false;
                        }
                     }
                     if(ceFlag) {
                         clonedPTPNew.addAll(ptpMapCE.get(ptpCE).get(testName));
                     }
                 }
                  else {
                    clonedPTPNew.addAll(ptpMapCE.get(ptpCE).get(testName));
                  }
             }
         }
        system.debug('test clonedPTPNew' + clonedPTPNew);
         if(!clonedPTPNew.isEmpty()){
             insert clonedPTPNew;
         }
         
         set<Id> recIds = new set<Id>();
         for(OSM_Payor_Test_Price__c ptp : clonedPTPNew){
             planIds2.add(ptp.OSM_Plan__c);
             recIds.add(ptp.OSM_Test__c);
         }
         
         List<OSM_Payor_Test_Price__c> ptpListNew = [SELECT Id, OSM_Net_Price__c , OSM_Agreement_Line_Item_ID__r.Apttus__AgreementId__c, OSM_Billing_Category__c, OSM_Billing_Cycle__c, OSM_Expected_Rate__c, OSM_Assay_Score__c, CurrencyIsoCode, OSM_CPT_Code__c, OSM_Quantity_End_Range__c, OSM_Quantity_Start_Range__c, OSM_Price_Method__c, OSM_List_Price__c, OSM_Start_Date__c,OSM_End_Date__c,OSM_Agreement_ID__c, OSM_Agreement_Line_Item_ID__c, OSM_Agreement_Record_Type__c, OSM_Agreement_Sub_Type__c, OSM_Currently_Active__c, OSM_Test__r.Name, OSM_Future_Effective__c, OSM_Active_Tiered_Rate__c, OSM_Pricing_Schema__c, OSM_Test__c, OSM_Plan__c FROM OSM_Payor_Test_Price__c WHERE OSM_Plan__c IN: planIds2];
         
         Set<String> excludeDelTest = new Set<String>();
         Boolean ceCHeck = false;
         
         for(OSM_Payor_Test_Price__c ptpLoop : ptpListNew){
             if(ptpLoop.OSM_Pricing_Schema__c != 'default'){
                if(ptpLoop.OSM_Currently_Active__c){
                    ceCHeck = true;
                    break;
                }
                else{
                    ceCHeck = false;
                }
             }
         }
        if(ceCHeck){
            for(OSM_Payor_Test_Price__c ptpLoop : ptpListNew){
                    excludeDelTest.add(ptpLoop.OSM_Test__r.Name);
            }
        }   
        for(OSM_Payor_Test_Price__c ptpLoop : ptpListNew){
             if(excludeDelTest.size()>0){
                 if(excludeDelTest.contains(ptpLoop.OSM_Test__r.Name)){
                     if(mapPlanIdTestIds.get(ptpLoop.OSM_Plan__c)!= null && mapPlanIdTestIds.get(ptpLoop.OSM_Plan__c).contains(ptpLoop.OSM_Test__r.Name) && ptpLoop.OSM_Pricing_Schema__c == 'default'){
                     
                         if(!ptpLoop.OSM_Future_Effective__c){
                            deleteRecordOnPlan.add(ptpLoop);
                         }
                     }
                 }
             }
             else{
                 if(mapPlanIdTestIds.get(ptpLoop.OSM_Plan__c)!= null && mapPlanIdTestIds.get(ptpLoop.OSM_Plan__c).contains(ptpLoop.OSM_Test__r.Name) && ptpLoop.OSM_Pricing_Schema__c == 'default'){
                     
                         if(ptpLoop.OSM_Future_Effective__c){
                            deleteRecordOnPlan.add(ptpLoop);
                         }
                     }
             }
        }

        //PATRICK's ADDITIONAL CODES END
         if(deleteRecordOnPlan.size()>0){
            delete deleteRecordOnPlan;
            
         }
    }
}