@isTest(seeAllData = false)
private class OSM_UpdatePricingToPlanBatch_Test {
 
 static testMethod void testBatch1() {
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        triggerSwitch.order_Role_Trigger__c = false;
        triggerSwitch.case_trigger__c = false;
        triggerSwitch.Payor_Test_Price_Trigger__c = false;
        triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        //List<Apttus__AgreementLineItem__c> dummyALIs =[SELECT Id from Apttus__AgreementLineItem__c where Apttus__AgreementId__r.Name = 'Payor Pricing Agreement'];
        
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        
        acct.Name = 'dasdwe';
        update acct;
        
        Product2 prodProstate = OSM_DataFactory.createProduct('Prostate', true);
        insert prodProstate;
        Product2 prodDCIS = OSM_DataFactory.createProduct('DCIS', true);
        insert prodDCIS;
        Product2 prodIBC = OSM_DataFactory.createProduct('IBC', true);
        insert prodIBC;
        Product2 prodColon = OSM_DataFactory.createProduct('Colon', true);
        insert prodColon;
        Product2 prodMMR = OSM_DataFactory.createProduct('MMR', true);
        insert prodMMR;
        
        // List<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();
        
        // Datetime today = Datetime.now();
        // Date defDateStart = Date.parse(today.format('MM/dd/yyyy')) - Integer.valueOf(1000);        
        // Date defDateEnd = Date.parse(today.format('MM/dd/yyyy')) + Integer.valueOf(1000);  
        // agreementList.add(new Apttus__APTS_Agreement__c(Name = Label.OSM_Dummy_Agreement_Name, Apttus__Account__c = acct.Id, GHI_CLM_Agreement_SubType__c = 'Commercial'));
        // agreementList[0].GHI_CLM_Effective_Date__c = defDateStart;
        // agreementList[0].Apttus__Contract_End_Date__c = defDateEnd;
        // agreementList.add(new Apttus__APTS_Agreement__c(Name = 'Test Agreement2', Apttus__Account__c = acct.Id, GHI_CLM_Agreement_SubType__c = 'Commercial'));
        // agreementList[1].GHI_CLM_Effective_Date__c = defDateStart;
        // agreementList[1].Apttus__Contract_End_Date__c = defDateEnd;
        // insert agreementList;
        
        //Apttus__AgreementLineItem__c ALIProstate;
        //dummyALIs[0].clone(false, true, false, false);
        // ALIProstate.Apttus__AgreementId__c = agreementList[0].Id;
        // ALIProstate.Apttus__ProductId__c = testProd[0].Id;
        // ALIProstate.Apttus__ListPrice__c = 1.00;
        // ALIProstate.GHI_CPQ_Billing_Category__c = 'test billcat1';
        // ALIProstate.GHI_CPQ_CPT_Code__c = '123';
        // ALIProstate.CurrencyIsoCode = 'USD';
        // ALIProstate.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        // ALIProstate.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        // ALIProstate.Apttus__NetPrice__c = 1.00;
        // ALIProstate.Apttus_CMConfig__StartDate__c = defDateStart;
        // ALIProstate.Apttus_CMConfig__EndDate__c = defDateEnd;
        // ALIProstate.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        //insert ALIProstate;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        
        Account payorAcc = OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType);
        payorAcc.OSM_Payor_Type__c = 'Private';
        payorAcc.OSM_Payor_Hierarchy__c = '2';
        payorAcc.OSM_Payor_Category__c = 'Private';
        payorAcc.OSM_Number_of_Plans__c = 1;
        payorAcc.OSM_Status__c = 'Approved';
        
        insert payorAcc;
        
         OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payorAcc.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = false,
            OSM_Start_Date__c = System.Today() + 100,
            OSM_End_Date__c = System.Today() + 10,
            // OSM_Agreement_Line_Item_ID__c = realALI2.Id,
            // OSM_Agreement_ID__c = agreementTest.Id,
            OSM_Pricing_Schema__c = 'Commercial'
            
        );
        insert ptp;
        
        String accid = acct.id;
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Account', Record_Ids__c = accid);
        //insert errInBatch;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        OSM_UpdatePricingToPlanBatch tr = new OSM_UpdatePricingToPlanBatch();
        
        tr.isReRun = false;
        OSM_UpdatePricingToPlanBatch.MyWrapper a = new OSM_UpdatePricingToPlanBatch.MyWrapper('Account',accid, 'error');
        tr.WrapperList.add(a);
        
        OSM_Plan__c plan = new OSM_Plan__c(OSM_Payor__c = payorAcc.Id);
        plan.OSM_QDX_Financial_Category__c = 'COMM';
        insert plan;
        
        Apttus__APTS_Agreement__c agreementTest = new Apttus__APTS_Agreement__c();
        agreementTest.Name = 'Payor Pricing Agreement';
        agreementTest.Apttus__Account__c = payorAcc.Id;
        agreementTest.Apttus__Status_Category__c = 'In Effect';
        agreementTest.Apttus__Contract_Start_Date__c = system.today();
        agreementTest.Apttus__Contract_End_Date__c = system.today()+10;
        
        insert agreementTest;
        
        payorAcc.GHI_CLM_Agreement_ID__c = agreementTest.Id;
        update payorAcc;
        
        ptp.OSM_Agreement_ID__c = agreementTest.Id;
        update ptp;
        
        Apttus__AgreementLineItem__c ALIProstate = new Apttus__AgreementLineItem__c();
        
        ALIProstate.Apttus__AgreementId__c = agreementTest.Id;
        ALIProstate.Apttus__ProductId__c = prodProstate.Id;
        ALIProstate.Apttus__ListPrice__c = 1.00;
        ALIProstate.GHI_CPQ_Billing_Category__c = 'test billcat1';
        ALIProstate.GHI_CPQ_CPT_Code__c = '123';
        ALIProstate.CurrencyIsoCode = 'USD';
        ALIProstate.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        ALIProstate.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        ALIProstate.Apttus__NetPrice__c = 1.00;
        ALIProstate.Apttus_CMConfig__StartDate__c = system.today();
        ALIProstate.Apttus_CMConfig__EndDate__c = system.today();
        ALIProstate.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        insert ALIProstate;
        
        Apttus__AgreementLineItem__c ALIDCIS = new Apttus__AgreementLineItem__c();
        
        ALIDCIS.Apttus__AgreementId__c = agreementTest.Id;
        ALIDCIS.Apttus__ProductId__c = prodDCIS.Id;
        ALIDCIS.Apttus__ListPrice__c = 1.00;
        ALIDCIS.GHI_CPQ_Billing_Category__c = 'test billcat1';
        ALIDCIS.GHI_CPQ_CPT_Code__c = '123';
        ALIDCIS.CurrencyIsoCode = 'USD';
        ALIDCIS.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        ALIDCIS.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        ALIDCIS.Apttus__NetPrice__c = 1.00;
        ALIDCIS.Apttus_CMConfig__StartDate__c = system.today();
        ALIDCIS.Apttus_CMConfig__EndDate__c = system.today();
        ALIDCIS.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        insert ALIDCIS;
        
        Apttus__AgreementLineItem__c ALIIBC = new Apttus__AgreementLineItem__c();
        
        ALIIBC.Apttus__AgreementId__c = agreementTest.Id;
        ALIIBC.Apttus__ProductId__c = prodIBC.Id;
        ALIIBC.Apttus__ListPrice__c = 1.00;
        ALIIBC.GHI_CPQ_Billing_Category__c = 'test billcat1';
        ALIIBC.GHI_CPQ_CPT_Code__c = '123';
        ALIIBC.CurrencyIsoCode = 'USD';
        ALIIBC.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        ALIIBC.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        ALIIBC.Apttus__NetPrice__c = 1.00;
        ALIIBC.Apttus_CMConfig__StartDate__c = system.today();
        ALIIBC.Apttus_CMConfig__EndDate__c = system.today();
        ALIIBC.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        insert ALIIBC;
        
        Apttus__AgreementLineItem__c ALIColon = new Apttus__AgreementLineItem__c();
        
        ALIColon.Apttus__AgreementId__c = agreementTest.Id;
        ALIColon.Apttus__ProductId__c = prodColon.Id;
        ALIColon.Apttus__ListPrice__c = 1.00;
        ALIColon.GHI_CPQ_Billing_Category__c = 'test billcat1';
        ALIColon.GHI_CPQ_CPT_Code__c = '123';
        ALIColon.CurrencyIsoCode = 'USD';
        ALIColon.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        ALIColon.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        ALIColon.Apttus__NetPrice__c = 1.00;
        ALIColon.Apttus_CMConfig__StartDate__c = system.today();
        ALIColon.Apttus_CMConfig__EndDate__c = system.today();
        ALIColon.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        insert ALIColon;
        
        Apttus__AgreementLineItem__c ALIMMR = new Apttus__AgreementLineItem__c();
        
        ALIMMR.Apttus__AgreementId__c = agreementTest.Id;
        ALIMMR.Apttus__ProductId__c = prodMMR.Id;
        ALIMMR.Apttus__ListPrice__c = 1.00;
        ALIMMR.GHI_CPQ_Billing_Category__c = 'test billcat1';
        ALIMMR.GHI_CPQ_CPT_Code__c = '123';
        ALIMMR.CurrencyIsoCode = 'USD';
        ALIMMR.GHI_CPQ_Billing_Cycle__c = 'test billcyc1';
        ALIMMR.GHI_CPQ_Pricing_Schema__c = 'test pricschema';
        ALIMMR.Apttus__NetPrice__c = 1.00;
        ALIMMR.Apttus_CMConfig__StartDate__c = system.today();
        ALIMMR.Apttus_CMConfig__EndDate__c = system.today();
        ALIMMR.Apttus_CMConfig__PriceMethod__c = 'Flat Price';
        
        insert ALIMMR;
        
        List<Account> accList = new List<Account>();
        accList.add(acct);
        accList.add(payorAcc);
        
        //OSM_UpdatePricingToPlanBatch b = new OSM_UpdatePricingToPlanBatch();
        //b.generateDefaultPTPToPLan(accList);
        
        Test.startTest();
        Database.executeBatch(tr);
        tr.updatePricingToPlanBatchClone(accList);
        plan.OSM_QDX_Financial_Category__c = 'MCRA';
        update plan;
        ptp.OSM_Pricing_Schema__c = 'Medicare Advantage';
        update ptp;
        tr.updatePricingToPlanBatchClone(accList);
        Test.stopTest();

        //System.assert(OSM_TerritoryAlignmentBatchOrder.batchIsRunnng);
    }
    
        static testMethod void testBatch2() {
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        triggerSwitch.order_Role_Trigger__c = false;
        triggerSwitch.case_trigger__c = false;
        triggerSwitch.Payor_Test_Price_Trigger__c = false;
        triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        
        Account payorAcc = OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType);
        payorAcc.OSM_Payor_Type__c = 'Private';
        payorAcc.OSM_Payor_Hierarchy__c = '2';
        payorAcc.OSM_Payor_Category__c = 'Private';
        payorAcc.OSM_Number_of_Plans__c = 1;
        insert payorAcc;
        
        String accid = acct.id;
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Account', Record_Ids__c = accid);
        insert errInBatch;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        OSM_UpdatePricingToPlanBatch tr = new OSM_UpdatePricingToPlanBatch();
        
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payorAcc.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = true,
            OSM_Start_Date__c = System.Today() + 100,
            OSM_End_Date__c = System.Today() + 10,
            // OSM_Agreement_Line_Item_ID__c = realALI2.Id,
            // OSM_Agreement_ID__c = agreementTest.Id,
            OSM_Pricing_Schema__c = 'Commercial'
            
        );
        insert ptp;
        
        tr.isReRun = true;
        OSM_UpdatePricingToPlanBatch.MyWrapper a = new OSM_UpdatePricingToPlanBatch.MyWrapper('Account',accid, 'TriggerTriggerTriggerTriggerTriggerTriggerTriggerTriggerTrigger');
        tr.WrapperList.add(a);
        
        OSM_Plan__c plan = new OSM_Plan__c(OSM_Payor__c = payorAcc.Id);
        plan.OSM_QDX_Financial_Category__c = 'COMM';
        insert plan;
        
        
        
         List<Account> accList = new List<Account>();
        accList.add(acct);
        accList.add(payorAcc);
        
        
        Test.startTest();
        Database.executeBatch(tr);
        tr.updatePricingToPlanBatchClone(accList);
        plan.OSM_QDX_Financial_Category__c = 'BCBS';
        update plan;
        tr.updatePricingToPlanBatchClone(accList);
        Test.stopTest();

        


        //System.assert(OSM_TerritoryAlignmentBatchOrder.batchIsRunnng);
    }
    
    static testMethod void testBatch3() {
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        triggerSwitch.order_Role_Trigger__c = false;
        triggerSwitch.case_trigger__c = false;
        triggerSwitch.Payor_Test_Price_Trigger__c = false;
        triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        
        Account payorAcc = OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType);
        payorAcc.OSM_Payor_Type__c = 'Private';
        payorAcc.OSM_Payor_Hierarchy__c = '2';
        payorAcc.OSM_Payor_Category__c = 'Private';
        payorAcc.OSM_Number_of_Plans__c = 1;
        insert payorAcc;
        
        String accid = acct.id;
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Account', Record_Ids__c = accid);
        insert errInBatch;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        OSM_UpdatePricingToPlanBatch tr = new OSM_UpdatePricingToPlanBatch();
        
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payorAcc.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = true,
            OSM_Start_Date__c = System.Today() + 100,
            OSM_End_Date__c = System.Today() + 10,
            // OSM_Agreement_Line_Item_ID__c = realALI2.Id,
            // OSM_Agreement_ID__c = agreementTest.Id,
            OSM_Pricing_Schema__c = 'TriCare'
            
        );
        insert ptp;
        
        tr.isReRun = true;
        OSM_UpdatePricingToPlanBatch.MyWrapper a = new OSM_UpdatePricingToPlanBatch.MyWrapper('Account',accid, 'TriggerTriggerTriggerTriggerTriggerTriggerTriggerTriggerTrigger');
        tr.WrapperList.add(a);
        
        OSM_Plan__c plan = new OSM_Plan__c(OSM_Payor__c = payorAcc.Id);
        plan.OSM_QDX_Financial_Category__c = 'COMM';
        insert plan;
        
        
        
         List<Account> accList = new List<Account>();
        accList.add(acct);
        accList.add(payorAcc);
        
        
        Test.startTest();
        Database.executeBatch(tr);
        tr.updatePricingToPlanBatchClone(accList);
        plan.OSM_QDX_Financial_Category__c = 'EXCH';
        update plan;
        ptp.OSM_Pricing_Schema__c = 'Commercial';
        update ptp;
        tr.updatePricingToPlanBatchClone(accList);
        Test.stopTest();
        
       
        
        
    }
    static testMethod void testBatch4() {
        
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.result_trigger__c = false;
        triggerSwitch.order_Role_Trigger__c = false;
        triggerSwitch.case_trigger__c = false;
        triggerSwitch.Payor_Test_Price_Trigger__c = false;
        triggerSwitch.Agreement_Trigger__c = false;
        insert triggerSwitch;
        
        Account acct = OSM_DataFactory.createAccountWithBillingAddress(12,'USA', 'Hudson Street', 'Redwood', 'CA', '94121', null);
        insert acct;
        
        acct.Name = 'dasdwe';
        update acct;
        
        Id accPayorRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        
        Account payorAcc = OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', accPayorRecType);
        payorAcc.OSM_Payor_Type__c = 'Private';
        payorAcc.OSM_Payor_Hierarchy__c = '2';
        payorAcc.OSM_Payor_Category__c = 'Private';
        payorAcc.OSM_Number_of_Plans__c = 1;
        insert payorAcc;
        
        String accid = acct.id;
        Error_in_Batch__c errInBatch = new Error_in_Batch__c(Object__c = 'Account', Record_Ids__c = accid);
        insert errInBatch;

        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', PatientRecType);
        insert contact;

        OSM_UpdatePricingToPlanBatch tr = new OSM_UpdatePricingToPlanBatch();
        
        OSM_Payor_Test_Price__c ptp = new OSM_Payor_Test_Price__c(
            OSM_Payor__c = payorAcc.Id, 
            OSM_Expected_Rate__c = '20',
            OSM_Newly_Generated__c = true,
            OSM_Start_Date__c = System.Today() + 100, 
            OSM_End_Date__c = System.Today() + 10,
            // OSM_Agreement_Line_Item_ID__c = realALI2.Id,
            // OSM_Agreement_ID__c = agreementTest.Id,
            OSM_Pricing_Schema__c = 'Managed Medicaid'
            
        );
        insert ptp;
        
        tr.isReRun = true;
        OSM_UpdatePricingToPlanBatch.MyWrapper a = new OSM_UpdatePricingToPlanBatch.MyWrapper('Account',accid, 'TriggerTriggerTriggerTriggerTriggerTriggerTriggerTriggerTrigger');
        tr.WrapperList.add(a);
        
        OSM_Plan__c plan = new OSM_Plan__c(OSM_Payor__c = payorAcc.Id);
        plan.OSM_QDX_Financial_Category__c = 'MAMC';
        insert plan;
        
         List<Account> accList = new List<Account>();
        accList.add(acct);
        accList.add(payorAcc);
        
         
        Test.startTest();
        Database.executeBatch(tr);
        tr.updatePricingToPlanBatchClone(accList);
        plan.OSM_QDX_Financial_Category__c = 'GOVT';
        update plan;
        ptp.OSM_Pricing_Schema__c = 'Government';
        update ptp;
        tr.updatePricingToPlanBatchClone(accList);
        ptp.OSM_Pricing_Schema__c = 'Medicare';
        update ptp;
        tr.updatePricingToPlanBatchClone(accList);
        ptp.OSM_Pricing_Schema__c = 'Medicaid';
        update ptp;
        tr.updatePricingToPlanBatchClone(accList);
        tr.parseErrorReturnId('TriggerTriggerTriggerTriggerTriggerTriggerTriggerTriggerTrigger');
        Test.stopTest();
  
        
    }
    
}