global class OSM_Utilities {
    /*
        @author: Daniel Quismorio
        @date: 4 February 2015
        @description : Dynamic SQL
        @param: String Object Name, Set of Ids, String where caluse
        
    */
    public static List<SObject> getObjectRecord(String objName, Set<Id> ordrId, String whereClause){
		String query = 'SELECT';
		Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        for(String s : objectFields.keySet()) {
		   query += ' ' + s + ',';
		}
		if (query.subString(query.Length()-1,query.Length()) == ','){
		    query = query.subString(0,query.Length()-1);
		}
		query += ' FROM ' + objName;
		query += ' WHERE '+ whereClause + ' IN: ordrId';
		List<SObject> objectQuery = database.query(query);
        return objectQuery;
    }
    
    /*
        @author: Daniel Quismorio
        @date: 4 February 2015
        @description : Dynamic SQL with return with related fields
        @param: String Object Name, Set of Ids, String where caluse, List of extended fields
        
    */
    public static List<SObject> getObjectRecord2(String objName, Set<Id> ordrId, String whereClause, List<String> addFields){
        ordrId.remove(null);
        if(!ordrId.isEmpty()){
    		String query = 'SELECT';
    		Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
            for(String s : objectFields.keySet()) {
    		   query += ' ' + s + ',';
    		}
    		if(addFields.size() > 0){
        		for(String addField: addFields){
        		     query += ' ' + addField + ',';
        		}
    		}
    		if (query.subString(query.Length()-1,query.Length()) == ','){
    		    query = query.subString(0,query.Length()-1);
    		}
    		query += ' FROM ' + objName;
    		query += ' WHERE '+ whereClause + ' IN: ordrId';
    		List<SObject> objectQuery = database.query(query);
            return objectQuery;
        } else {
            List<SObject> objectQuery = new List<SObject>();
            return objectQuery;
        }
    }
    
    /*
        @author: Daniel Quismorio
        @date: 30 July 2015
        @description : Parse format base on the specification of GHI
        @param: String faxNumber
        
    */
    public static String parseFax(String faxNumber){
        if(faxNumber.contains('(') || faxNumber.contains(')') || faxNumber.contains('-')){
            faxNumber = faxNumber.remove('(');
            faxNumber = faxNumber.remove(')');
            faxNumber = faxNumber.remove('-');
            faxNumber = faxNumber.remove(' ');
            return '1'+faxNumber;
        } else {
            return faxNumber;
        }
    }
     
    public static Boolean generateXMLForOrderLineItemRunOnce = false;
    public static Boolean generateXMLForOrderRunOnce = false;
    public static Boolean generateXMLForWorkOrderRunOnce = false;
    public static Boolean callFutureXMLGenerationRunOnce = false;
    public static Boolean sendCaseDataMessageRunOnce = false;
    public static Boolean processSpciemenMessageRunOnce = false;
    public static Boolean processInboundRunOnce = false;
    public static Boolean hasRenderAndDistribtueRunOnce = false;
}