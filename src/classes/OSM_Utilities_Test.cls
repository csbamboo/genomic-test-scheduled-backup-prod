@isTest
private class OSM_Utilities_Test {

    private static testMethod void callGetObjectRecord() {
        Id rt = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();

        Account acct = OSM_DataFactory.createAccount('Francis Nasalita');
        insert acct;

        Contact ctct = OSM_DataFactory.createContact('Francis','Nasalita',rtContact);
        ctct.AccountId = acct.Id;
        insert ctct;

        Id recTypeOrdrId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Work Order').getRecordTypeId(); 
        Order order1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        //order1.RecordTypeId = recTypeOrdrId;
        order1.OSM_Credit_Card__c = true;
        order1.OSM_Bill_Type__c = 'Patient Pre-Pay';
        insert order1;
        
        Order od1 = OSM_DataFactory.createOrder('test', ctct.Id, acct.Id, system.today(), 'Active');
        insert od1;
        
        Set<Id> tempOrderIds = new Set<Id>{od1.Id};
        List<String> addFields = new List<String>();
        addFields.add('OSM_Patient__r.FirstName');
        List<Order> tempOrderList = OSM_Utilities.getObjectRecord('Order', tempOrderIds, 'Id');
        List<Order> tempOrderList2 = OSM_Utilities.getObjectRecord2('Order', tempOrderIds, 'Id',addFields);
        
        System.assertEquals(1, tempOrderList2.size());
        System.assertEquals(1, tempOrderList.size());
        String faxNum = '(454)0000222';
        String faxNum2 = '2222222222';
        OSM_Utilities.parseFax(faxNum);
        OSM_Utilities.parseFax(faxNum2);
    }

}