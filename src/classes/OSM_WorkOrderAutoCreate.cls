/*
 *  @author         Raus Kenneth Ablaza
 *  @User Story     32043
 *  @description    Creates Work Order and Work Order Line for every Order that is saved.
 *  @date           18 May 2015
 */

global with sharing class OSM_WorkOrderAutoCreate{
    public static boolean callFromOrderWOButton = false;
    
    public OSM_WorkOrderAutoCreate(){}
    
    public static void automateWorkOrder(List<Order> ordList){
        Set <Id> ordId = new Set <Id>();
        
        for(Order ord : ordList){
            ordId.add(ord.Id);
        }
        
        List <OSM_Work_Order__c> woChecker = [
            select Id 
            from OSM_Work_Order__c 
            where OSM_Order__c in :ordId
        ];
        
        if(woChecker.isEmpty()){
            String res = createWorkOrderWorkOrderLine(ordList);
        }
    }
    
    public static String createWorkOrderWorkOrderLine(List<Order> ordList){
        String result = '';
        List <OSM_Work_Order_Line_Item__c> woliList = new List <OSM_Work_Order_Line_Item__c>();
        Map <OrderItem, OSM_Work_Order__c> woMap = new Map <OrderItem, OSM_Work_Order__c>();
        Set <Id> ordIds = new Set <Id>();
        Set <Id> wOrdIds = new Set <Id>();
        Set <Id> oliIds = new Set <Id>();
        Map<Id,Order> parentOrderMap = new Map<Id,Order>();
        
        // Gets all Order Ids
        for(Order ord : ordList){
            ordIds.add(ord.Id);
            parentOrderMap.put(ord.Id, ord);
        }
        
        // Gets all the OLI related to the Orders
        Map<Id,OrderItem> oliQueryMap = new Map<Id,OrderItem>([
            SELECT Id, OrderId, Order.OSM_Status__c, Order.OSM_Triage_Outcome__c, OSM_Work_Order__c, OSM_Product_Name_f__c
            FROM OrderItem
            WHERE OrderId in :ordIds
        ]);
        
        // Gets all the OLI Ids
        for(OrderItem oli : oliQueryMap.values()){
            oliIds.add(oli.Id);
        }
        
        // Gets all the WOLI related to an Order
        List<OSM_Work_Order_Line_Item__c> woliQuery = [
            select Id, OSM_Order_Product__c, OSM_Bill_Account__c
            from OSM_Work_Order_Line_Item__c 
            where OSM_Order_Product__c in :oliIds 
                and OSM_Bill_Account__c in :ordIds 
        ];
        
        // Gets all the WOLI Ids
        for(OSM_Work_Order_Line_Item__c woli : woliQuery){
            wOrdIds.add(woli.OSM_Order_Product__c);
        }
        
        Map<Id,String> mmrColonPartnerMap = new Map<Id,String>();
        OSM_Work_Order__c createWO;
        String woType = '';
        Integer createdWorkOrders = 0;
        
        for(OrderItem ordr : oliQueryMap.values()){
            //Process Type
            if(ordr.Order.OSM_Triage_Outcome__c == 'Resubmission - Confirmed'){
                woType = 'Resubmission';
            }
            else if(ordr.Order.OSM_Triage_Outcome__c == 'Retest - Confirmed'){
                woType = 'Retest';
            }
            else if(ordr.Order.OSM_Triage_Outcome__c == 'Franchise Different - Confirmed' || 
                    ordr.Order.OSM_Triage_Outcome__c == 'New' || 
                    ordr.Order.OSM_Triage_Outcome__c == 'Other'){
                woType = 'New';
            }
            else{
                woType = null;
            }
             
            // Process WO Creation 
            // Checks if the OLI has a WOLI already
            if (!wOrdIds.contains(ordr.Id)){
                if((ordr.Order.OSM_Status__c == 'Processing') || callFromOrderWOButton){
                    if(parentOrderMap.get(ordr.OrderId).OSM_Product__c != 'MMR and Colon' && parentOrderMap.get(ordr.OrderId).OSM_Product__c != 'MMR Reflex to Colon'){
                        createWO = new OSM_Work_Order__c(
                            OSM_Order__c = ordr.OrderId, 
                            OSM_Type__c = woType,
                            OSM_Status__c = 'Open'
                        );
                        woMap.put(ordr, createWO);
                        createdWorkOrders += 1;
                    }else{
                        if(ordr.OSM_Product_Name_f__c != 'MMR' && ordr.OSM_Product_Name_f__c != 'Colon'){
                            createWO = new OSM_Work_Order__c(
                                OSM_Order__c = ordr.OrderId, 
                                OSM_Type__c = woType,
                                OSM_Status__c = 'Open'
                            );
                            woMap.put(ordr, createWO);
                            createdWorkOrders += 1;
                        }else{
                            if(mmrColonPartnerMap.containsKey(ordr.OrderId)){
                                mmrColonPartnerMap.put(ordr.OrderId, ordr.Id + ':::' + mmrColonPartnerMap.get(ordr.OrderId));
                            }else{
                                mmrColonPartnerMap.put(ordr.OrderId, ordr.Id);
                            }
                            
                            //Create 1 WO or mmr and colon
                            if(mmrColonPartnerMap.get(ordr.OrderId).contains(':::')){
                                createWO = new OSM_Work_Order__c(
                                    OSM_Order__c = ordr.OrderId, 
                                    OSM_Type__c = woType,
                                    OSM_Status__c = 'Open'
                                );
                                woMap.put(oliQueryMap.get(mmrColonPartnerMap.get(ordr.OrderId).split(':::')[0]), createWO);
                            }
                        }
                    }
                }
            }
        }
        
        insert woMap.values();
        //Create Link for MMR and Colon
        for(String loopString : mmrColonPartnerMap.values()){
            woMap.put(oliQueryMap.get(loopString.split(':::')[1]), woMap.get(oliQueryMap.get(loopString.split(':::')[0])));
        }
        createdWorkOrders += mmrColonPartnerMap.size();
        
        /*
        Map<Id, List<OSM_Work_Order_Line_Item__c>> woliMap = new Map<Id, List<OSM_Work_Order_Line_Item__c>>();
        List<OrderItem> oliUpdates = new List<OrderItem>();
        */
        
        OSM_Work_Order__c ordIter;
        List<OrderItem> updateOrderItemList = new List<OrderItem>();
        for(OrderItem wo : woMap.keySet()){
            ordIter = woMap.get(wo);
            updateOrderItemList.add(new OrderItem(Id = wo.Id, OSM_Work_Order__c = ordIter.Id));
            
            /*
            
            oliUpdates.add(wo);
            
            if((ordIter.OSM_Order__r.OSM_Product__c == 'MMR and Colon') &&
                (wo.OSM_Product_Name__c == 'MMR' || wo.OSM_Product_Name__c == 'Colon')){
                
                if(!woliMap.containsKey(ordIter.Id)){
                    List<OSM_Work_Order_Line_Item__c> listwoli = new List<OSM_Work_Order_Line_Item__c>();
                    listwoli.add(new OSM_Work_Order_Line_Item__c(
                        OSM_Order_Product__c = wo.Id, 
                        OSM_Bill_Account__c = ordIter.OSM_Order__c, 
                        OSM_Work_Order__c = ordIter.Id, 
                        OSM_Work_Order_ID__c = ordIter.Id
                    ));
                    woliMap.put(ordIter.Id, listwoli);
                }    
                else{
                    woliMap.get(ordIter.Id).add(new OSM_Work_Order_Line_Item__c(
                        OSM_Order_Product__c = wo.Id, 
                        OSM_Bill_Account__c = ordIter.OSM_Order__c, 
                        OSM_Work_Order__c = ordIter.Id, 
                        OSM_Work_Order_ID__c = ordIter.Id
                    ));
                }
                    
                woliMap.put(ordIter.Id, new List<OSM_Work_Order_Line_Item__c> {new OSM_Work_Order_Line_Item__c(
                    OSM_Order_Product__c = wo.Id, 
                    OSM_Bill_Account__c = ordIter.OSM_Order__c, 
                    OSM_Work_Order__c = ordIter.Id, 
                    OSM_Work_Order_ID__c = ordIter.Id
                )});
            }
            
            */
            
            woliList.add(new OSM_Work_Order_Line_Item__c(
                OSM_Order_Product__c = wo.Id, 
                OSM_Bill_Account__c = ordIter.OSM_Order__c, 
                OSM_Work_Order__c = ordIter.Id
            ));
            
            /*
            if(ordIter.OSM_Order__r.OSM_Product__c == 'MMR and Colon'){
                ordMmrColon = ordIter;
                if(wo.OSM_Product_Name__c == 'MMR' || wo.OSM_Product_Name__c == 'Colon'){
                    woliList.add(new OSM_Work_Order_Line_Item__c(
                        OSM_Order_Product__c = wo.Id, 
                        OSM_Bill_Account__c = ordMmrColon.OSM_Order__c, 
                        OSM_Work_Order__c = ordMmrColon.Id
                    ));
                }
                else{
                    woliList.add(new OSM_Work_Order_Line_Item__c(
                        OSM_Order_Product__c = wo.Id, 
                        OSM_Bill_Account__c = ordIter.OSM_Order__c, 
                        OSM_Work_Order__c = ordIter.Id
                    ));
                }
            }
            else{
                
            }
            */
        }
        
        /*
        System.debug('@@@@@@******************** debug of woliMap: ' + woliMap);
            
        for(List<OSM_Work_Order_Line_Item__c> loopWoli : woliMap.values()){
            for(OSM_Work_Order_Line_Item__c lwoli : loopWoli){
                woliList.add(lwoli);
            }
        }
        
        update oliUpdates;
        */
        
        insert woliList;
        update updateOrderItemList;
        
        // Returns messages based on the number of WOs created
        if(woMap.values().isEmpty()){
            result = 'Cannot create new work order. There is no new Order Line Item.';
        }
        else if(woMap.values().size() == 1){
            result = 'A Work Order has been added.';
        }
        else{
            result = createdWorkOrders == 1 ? createdWorkOrders + ' Work Order have been added.' : createdWorkOrders + ' Work Orders have been added.';
        }
        
        return result;
    }
    
    // Called by the "Create Work Orders and WOOLIs" button
    webService static String retraceWOLI(Id ordId){
        String result = '';
        callFromOrderWOButton = true;
        
        
        List <Order> ordList = [
            select Id, OSM_Product__c 
            from Order
            where id = :ordId
        ];
         
        Integer woCheckerCnt = [
            select count()
            from OSM_Work_Order__c 
            where OSM_Order__c = :ordId
        ];
        
        if(woCheckerCnt == 0){
             result = createWorkOrderWorkOrderLine(ordList);
        } else {
            result = 'Work Orders already present on Order, Cannot Create New Work Orders';
        }
        
        return result;
    }

}