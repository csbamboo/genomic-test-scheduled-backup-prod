@isTest
private class OSM_WorkOrderAutoCreate_Test {
    
    /*
        @author             Raus Kenneth Ablaza
        @description        Setup method for test data
        @date               18 September 2015
    */
    @testSetup
    private static void setup(){
        /* Initialization of variables */
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        
        /* Id's */
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id pb2Standard = Test.getStandardPricebookId();
        
        /* Custom Settings - TriggerSwitch */
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Trigger__c = true;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;

        /* Accounts */
        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;
        
        /* Contacts */
        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        /* Products */
        prodList.add(OSM_DataFactory.createProduct('IBC', true));
        prodList.add(OSM_DataFactory.createProduct('MMR and Colon', true));
        prodList.add(OSM_DataFactory.createProduct('MMR', true));
        prodList.add(OSM_DataFactory.createProduct('Colon', true));
        insert prodList;
        
        /* PricebookEntries */
        pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[0].Id, pb2Standard, 3));
        pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[1].Id, pb2Standard, 3));
        pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[2].Id, pb2Standard, 3));
        pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[3].Id, pb2Standard, 3));
        insert pbeList;

        /* Orders */
        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_Product__c = (i == 1 ? 'MMR and Colon' : 'IBC'), 
                Pricebook2Id = pb2Standard, 
                EffectiveDate = System.Today(), 
                OSM_Triage_Outcome__c = 'New',
                Status = 'New'
            ));        
        }
        insert ordList;
        
        /* Order Line Items */
        for(Integer i = 0; i < 2; i++){
            oliList.add(new OrderItem(
                OrderId = ordList[i].Id , 
                PricebookEntryId = pbeList[i].Id , 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        insert oliList;
        
        /* Work Orders */
        for(Integer i = 0; i < 2; i++){
            woList.add(OSM_DataFactory.createWorkOrder(ordList[i].Id));
        }
    }

    /*
     *  @author          Raus Kenneth Ablaza
     *  @description     Tests OSM_WorkOrderAutoCreate
     *  @date            30 June 2015
     *
     */
    private static testMethod void testWorkOrder1() {
        /* Initialization of variables */
        Order testOrd;
        OrderItem testOrdr;
        Set<Id> ordIds = new Set<Id>();
        List<Order> ordList = new List<Order>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        
        
        /* Getting orders, work orders, and pricebook entries */
        ordList = (List<Order>) OSM_DataFactory.getAllFields('Order', null, null);
        woList = (List<OSM_Work_Order__c>) OSM_DataFactory.getAllFields('OSM_Work_Order__c', null, null);
        pbeList = (List<PricebookEntry>) OSM_DataFactory.getAllFields('PricebookEntry', null, null);
        
        
        /* Extra Order */
        testOrd = new Order();
        testOrd = ordList[0].clone(false);
        insert testOrd;
        
        /* Extra Order Item */
        testOrdr = new OrderItem(
            OrderId = testOrd.Id , 
            PricebookEntryId = pbeList[0].Id , 
            OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
            UnitPrice = 2 , 
            Quantity = 1
        );
        insert testOrdr;
        
        testOrd.OSM_Product__c = 'MMR';
        testOrd.OSM_Triage_Outcome__c = 'New';
        testOrd.OSM_Status__c = 'Processing';
        update testOrd;
        
        for(Order ord : ordList){
            ord.OSM_Status__c = 'Processing';
            ordIds.add(ord.Id);
        }
        update ordList; 
        
        /* Test */
        Test.startTest();
        
        OSM_WorkOrderAutoCreate obj = new OSM_WorkOrderAutoCreate();
        
        List<Order> ordList2 = new List<Order>{ordList[1]};
        OSM_WorkOrderAutoCreate.automateWorkOrder(ordList2);

        OSM_WorkOrderAutoCreate.retraceWOLI(testOrd.Id);
        OSM_WorkOrderAutoCreate.retraceWOLI(ordList[0].Id);
        
        for(Order ord : ordList){
            OSM_WorkOrderAutoCreate.retraceWOLI(ord.Id);
        }
        
        for(Order ord : ordList){
            ord.OSM_Product__c = 'MMR and Colon';
        }
        update ordList;
        
        OSM_WorkOrderAutoCreate.createWorkOrderWorkOrderLine(ordList);
        
        insert woList;
        
        for(OSM_Work_Order__c wo : woList){
            wo.OSM_Status__c = 'Processing';
        }
        update woList;
        
        Test.stopTest();
        
    }
    
    
    /*
        @author             Raus Kenneth Ablaza
        @description        Added test method for OSM_WorkOrderAutoCreate_Test test class
        @date               21 September 2015
    */
    private static testMethod void testWorkOrder2(){
         /* Initialization of variables */
        Order testOrd;
        OrderItem testOrdr;
        Set<Id> ordIds = new Set<Id>();
        List<Order> ordList = new List<Order>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<Product2> prodList = new List<Product2>();
        
        
        /* Getting orders, work orders, and pricebook entries */
        ordList = (List<Order>) OSM_DataFactory.getAllFields('Order', null, null);
        woList = (List<OSM_Work_Order__c>) OSM_DataFactory.getAllFields('OSM_Work_Order__c', null, null);
        prodList = (List<Product2>) OSM_DataFactory.getAllFields('Product2', null, null);
        pbeList = (List<PricebookEntry>) OSM_DataFactory.getAllFields('PricebookEntry', null, null);
        
        
        /* Extra Order */
        testOrd = new Order();
        testOrd = ordList[1].clone(false);
        insert testOrd;
        
        /* Extra Order Item */
        testOrdr = new OrderItem(
            OrderId = testOrd.Id , 
            PricebookEntryId = pbeList[2].Id , 
            OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
            UnitPrice = 2 , 
            Quantity = 1
        );
        oliList.add(testOrdr);
        testOrdr = new OrderItem(
            OrderId = testOrd.Id , 
            PricebookEntryId = pbeList[3].Id , 
            OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
            UnitPrice = 2 , 
            Quantity = 1
        );
        oliList.add(testOrdr);
        insert oliList;
        
        // testOrd.OSM_Product__c = 'MMR';
        // testOrd.OSM_Triage_Outcome__c = 'New';
        // testOrd.OSM_Status__c = 'Processing';
        // update testOrd;
        
        for(Order ord : ordList){
            ord.OSM_Status__c = 'Processing';
            ordIds.add(ord.Id);
        }
        update ordList; 
        
        /* Test */
        Test.startTest();
        
        OSM_WorkOrderAutoCreate.automateWorkOrder(ordList);
        String dumpStr = OSM_WorkOrderAutoCreate.retraceWOLI(testOrd.Id);
        
        Test.stopTest();
        
        System.assert(!String.isEmpty(dumpStr));
    }

}