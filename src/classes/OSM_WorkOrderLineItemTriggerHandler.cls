public with sharing class OSM_WorkOrderLineItemTriggerHandler {
    //public static Boolean hasUpdateWOLIToProcessingRun = false;
    public static Boolean runWorkOrderUpdateOnce = false;
    public static Boolean runValidationBuilderOnce = false;
    public static Boolean runOrderItemUpdate = false;
    public static Boolean hasPopulateStateRun = false;
    
    public static void onAfterInsert(List<OSM_Work_Order_Line_Item__c> workOrderLineItem){
        if(!runValidationBuilderOnce){
            runValidationBuilderOnce = true;
            String workOrderLineItemJSON = JSON.serializePretty(workOrderLineItem);
            //OSM_ValidationBuilder.validateOrderRecords(workOrderLineItemJSON);
        }
        Set<Id> workOrderIds = new Set<Id>();
        for(OSM_Work_Order_Line_Item__c loopWOLI : workOrderLineItem){
            workOrderIds.add(loopWOLI.Id);
        }
        if(!runOrderItemUpdate){
            runOrderItemUpdate = true;
            orderItemUpdate(workOrderLineItem);
        }
        if(!runWorkOrderUpdateOnce){
            runWorkOrderUpdateOnce = true;
            workOrderUpdate(workOrderLineItem);
        }
    }
    
    public static void onAfterUpdate(List<OSM_Work_Order_Line_Item__c> newWorkOrderLineItem, Map<Id, OSM_Work_Order_Line_Item__c> oldnewWorkOrderLineItemMap){
        List<OSM_Work_Order_Line_Item__c> changedBillTypeWorkOrderLineItemList = new List<OSM_Work_Order_Line_Item__c>();
        
        for(OSM_Work_Order_Line_Item__c loopOrderWorkLineItem : newWorkOrderLineItem){
            if(loopOrderWorkLineItem.OSM_Order_Bill_Type__c != oldnewWorkOrderLineItemMap.get(loopOrderWorkLineItem.Id).OSM_Order_Bill_Type__c){
                changedBillTypeWorkOrderLineItemList.add(loopOrderWorkLineItem);
            }
            
        }
        Set<Id> workOrderIds = new Set<Id>();
        for(OSM_Work_Order_Line_Item__c loopWOLI : newWorkOrderLineItem){
            workOrderIds.add(loopWOLI.Id);
        }
        if(!runWorkOrderUpdateOnce){
            runWorkOrderUpdateOnce = true;
            workOrderUpdate(newWorkOrderLineItem);
        }
        if(!runValidationBuilderOnce){
            runValidationBuilderOnce = true;
            String workOrderLineItemJSON = JSON.serializePretty(newWorkOrderLineItem);
            //OSM_ValidationBuilder.validateOrderRecords(workOrderLineItemJSON);
        }
    }
    
    public static void onBeforeInsert(List<OSM_Work_Order_Line_Item__c> workOrderLineItemList){
        if(!hasPopulateStateRun){
            hasPopulateStateRun = true;
            populateState(workOrderLineItemList);
        }        
    }
    
    public static void onBeforeUpdate(List<OSM_Work_Order_Line_Item__c> workOrderLineItemList, Map<Id,OSM_Work_Order_Line_Item__c> oldWorkOrderLineItemMap){
        if(!hasPopulateStateRun){
            hasPopulateStateRun = true;
            populateState(workOrderLineItemList);
        }   
    }
    
    public static void populateState(List<OSM_Work_Order_Line_Item__c> workOrderLineItemList){
        Set<Id> oliIds = new Set<Id>();
        for(OSM_Work_Order_Line_Item__c loopWOLI : workOrderLineItemList){
            oliIds.add(loopWOLI.OSM_Order_Product__c);
        }
        
        //Code modiified - 06/18/2015 - As per  code refactorization - removing Queries
        Map<Id, OrderItem> oliMap = new Map<Id, OrderItem>(DatabaseQueryHelper.getOrderItemByIds(oliIds));
        
        for(OSM_Work_Order_Line_Item__c loopWOLI : workOrderLineItemList){
            loopWOLI.OSM_Order_Line_Item_State__c = oliMap.get(loopWOLI.OSM_Order_Product__c).OSM_State__c;
            loopWOLI.OSM_Order_Line_Item_Status__c = oliMap.get(loopWOLI.OSM_Order_Product__c).OSM_Lab_and_Report_Status__c;
            loopWOLI.OSM_Order_Product_Data_Entry_Status__c = oliMap.get(loopWOLI.OSM_Order_Product__c).OSM_Data_Entry_Status__c;
        }
    }
    
    /**
     * @author         Jerome Liwanag
     * @description    User Story 30548 - Lab and Report Status to Pre-Distribution
     *                 User Story 14672 - Lab and Report Status to Distributing
     *                 User Story 30547 - Lab and Report Status to Distributed
     *                 User Story 32061 - Lab and Report Status to Obsolete
     * @parameter      List of Work Order Records
     * @history        1.JUNE.2015 - Jerome Liwanag - Created  
     */    
    public static void workOrderUpdate(List<OSM_Work_Order_Line_Item__c> workOrderLineItemList){
        Set<Id> workOrderIds = new Set<Id>();
        Set<Id> workOrderUpdateIds = new Set<Id>();
        Map<Id,OSM_Work_Order__c> workOrderMap = new Map<Id,OSM_Work_Order__c>();
        List<OSM_Work_Order__c> woliList = new List<OSM_Work_Order__c>();
        for(OSM_Work_Order_Line_Item__c loopWOLI : workOrderLineItemList){
            workOrderIds.add(loopWOLI.OSM_Work_Order__c);
        }
        //Map work order to work order line item
        for(OSM_Work_Order__c loopWorkOrder : [SELECT Id,OSM_State__c FROM OSM_Work_Order__c WHERE Id IN :workOrderIds]){
            workOrderMap.put(loopWorkOrder.Id,loopWorkOrder);
        }
        //Update work order
        for(OSM_Work_Order_Line_Item__c loopWOLI : workOrderLineItemList){
            if(!workOrderUpdateIds.contains(loopWOLI.OSM_Work_Order__c)){
                workOrderMap.get(loopWOLI.OSM_Work_Order__c).Id = loopWOLI.OSM_Work_Order__c;
                woliList.add(workOrderMap.get(loopWoli.OSM_Work_Order__c));
                workOrderUpdateIds.add(loopWOLI.OSM_Work_Order__c);
            }
        }
        if(!woliList.isEmpty()){
            update woliList;
        }
    }
    
    /**
     * @author         Jerome Liwanag
     * @description    User Story 19469 - New Work Order - OLI Field  Reset
     * @parameter      List of Order Line Item records 
     * @history        29.MAY.2015 - Jerome Liwanag - Created 
     */    
    public static void orderItemUpdate(List<OSM_Work_Order_Line_Item__c> workOrderLineItemList){
        Set<Id> orderItemIds = new Set<Id>();
        Map<Id,OrderItem> orderItemMap = new Map<Id,OrderItem>();
        List<OrderItem> orderItemList = new List<OrderItem>();
        //Get order line item associated to work order line item
        for(OSM_Work_Order_Line_Item__c loopWOLI : workOrderLineItemList){
            orderItemIds.add(loopWOLI.OSM_Order_Product__c);
        }
        for(OrderItem loopOrderItem : [SELECT Id,OSM_Work_Order__c, OSM_Lab_and_Report_Status__c FROM OrderItem WHERE Id IN :orderItemIds]){
            orderItemMap.put(loopOrderItem.Id,loopOrderItem);
        }
        //Update order line item associated to work order if work order is changed
        for(OSM_Work_Order_Line_Item__c loopWOLI : workOrderLineItemList){
            orderItemMap.get(loopWOLI.OSM_Order_Product__c).OSM_Work_Order__c = loopWOLI.OSM_Work_Order__c;
            orderItemList.add(orderItemMap.get(loopWOLI.OSM_Order_Product__c));
        }
            
        if(!orderItemList.isEmpty()){
            update orderItemList;
        }
    }

}