/*
      @author: David Catindoy and Jats Xyvenn B. Matres
      @date: 23 JAN 2015 - Created
      @description: Study Trigger Handler
      @history: 23 JAN 2015 - Updated (David E. Catindoy)

*/
@isTest(seeAllData = true)
private class OSM_WorkOrderLineItemTriggerHandler_Test {
    
    static testmethod void testOnAfterInsert() {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        update triggerSwitch;
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id PartnerRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        
        List<Account> accountList = new List<Account>();
        Account acct = OSM_DataFactory.createAccount('New Account');
        acct.RecordTypeId = PartnerRecType;
        Account acct3 = OSM_DataFactory.createAccount('New Account3');
        acct3.RecordTypeId = PartnerRecType;
        Account acct2 = OSM_DataFactory.createAccount('New Account2');
        acct2.RecordTypeId = HCORecType;
        accountList.add(acct);
        accountList.add(acct2);
        accountList.add(acct3);
        Account newAcct = OSM_DataFactory.createAccountWithBillingAddress(1, 
                                                                               'United States', 
                                                                               'Test Street', 
                                                                               'Test City', 
                                                                               'Alaska', 
                                                                               '123', 
                                                                               HCORecType);
        accountList.add(newAcct);
        insert accountList;
        
        Contact con = OSM_DataFactory.createContact('First','Last', PatientRecType);
        con.AccountId = acct.Id;
        insert con;

        Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
        
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard.Id, 3);
        
        insert pbe;
        
        Order ord = new Order(OSM_Patient__c = con.Id, AccountId = newAcct.Id ,Pricebook2Id = pb2Standard.Id, EffectiveDate = System.Today(), Status = 'New');
        
        insert ord;
        
        OrderItem ordItem = new OrderItem(OrderId = ord.Id , PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1);
        
        insert ordItem;
        
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(ord.Id);
        
        insert workOrder;
        
        OSM_Work_Order_Line_Item__c workOrderLineItem = OSM_DataFactory.createWorkOrderLineItem(workOrder.Id, ordItem.Id);
        
        insert workOrderLineItem;
        
        
        Test.startTest();
        //OSM_WorkOrderLineItemTriggerHandler.hasUpdateWOLIToProcessingRun = false;   
        OSM_WorkOrderLineItemTriggerHandler.runWorkOrderUpdateOnce = false; 
        OSM_WorkOrderLineItemTriggerHandler.runValidationBuilderOnce = false;
        OSM_WorkOrderLineItemTriggerHandler.runValidationBuilderOnce = false;
        OSM_WorkOrderLineItemTriggerHandler.hasPopulateStateRun = false;
        
        update workOrderLineItem;
           
        Test.stopTest();
    }
    
}