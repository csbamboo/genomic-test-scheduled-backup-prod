/**
 * File Info
 * ----------------------------------
 * @filename       OSM_WorkOrderTriggerHandler.cls
 * @created        21.APR.2014
 * @author         Kristian Vegerano
 * @description    Class for Work Order Trigger changes. 
 * @history        21.APR.2014 - Kristian Vegerano - Created  
 */ 
public class OSM_WorkOrderTriggerHandler{
    public static Boolean hasUpdateOrderItemsRun = false;
    public static Boolean hasreopenOrdersRun = false;
    public static Boolean runLabReportStatusToProcessing = false;
    public static Boolean runLabReportStatusUpdate = false;
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for any changes that should happen after an OSM_Work_Order__c record is created. 
     * @history        21.APR.2014 - Kristian Vegerano - Created  
     */
    public static void onAfterInsert(List<OSM_Work_Order__c> workOrderList){
        if(!hasUpdateOrderItemsRun){
            hasUpdateOrderItemsRun = true;
            updateOrderItems(workOrderList);
        }
        if(!hasreopenOrdersRun){
            hasreopenOrdersRun = true;
            reopenOrders(workOrderList);
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method for any changes that should happen after an OSM_Work_Order__c record is updated. 
     * @history        21.APR.2014 - Kristian Vegerano - Created  
     */
    public static void onAfterUpdate(List<OSM_Work_Order__c> workOrderList, Map<Id, OSM_Work_Order__c> oldWorkOrderMap){
        List<OSM_Work_Order__c> changedParentOrder = new List<OSM_Work_Order__c>();
        //Filter for records where its parent order has been changed
        for(OSM_Work_Order__c loopWorkOrder : workOrderList){
            if(loopWorkOrder.OSM_Order__c != oldWorkOrderMap.get(loopWorkOrder.Id).OSM_Order__c){
                changedParentOrder.add(loopWorkOrder);
            }
        }
        if(!hasreopenOrdersRun){
            hasreopenOrdersRun = true;
            reopenOrders(changedParentOrder);
        }
        if(!hasUpdateOrderItemsRun){
            hasUpdateOrderItemsRun = true;
            updateOrderItems(workOrderList);
        }
        if(!runLabReportStatusToProcessing){
            runLabReportStatusToProcessing = true;
            Set<Id> statusToProcessingSet = new Set<Id>();
            for(OSM_Work_Order__c workOrderLoop : workOrderList){
                //if(oldWorkOrderMap.get(workOrderLoop.Id).OSM_Status__c != 'Processing' && workOrderLoop.OSM_Status__c == 'Processing'){
                    statusToProcessingSet.add(workOrderLoop.Id);
                //}
            }
            labReportStatusToProcessing(workOrderList);
        }
        if(!runLabReportStatusUpdate){
            runLabReportStatusUpdate = true;
            labReportStatusUpdate(workOrderList,oldWorkOrderMap);
        }
        
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    Method that updates all the order product records related to the work order record. 
     * @history        21.APR.2014 - Kristian Vegerano - Created  
     */
    public static void updateOrderItems(List<OSM_Work_Order__c> workOrderList){
        List<OrderItem> updateOrderItems = new List<OrderItem>();
        Set<Id> workOrderIds = new Set<Id>();
        
        //Get Work Order Ids
        for(OSM_Work_Order__c loopWorkOrder : workOrderList){
            workOrderIds.add(loopWorkOrder.Id);
        }
        
        //Code modiified - 06/18/2015 - As per  code refactorization - removing Queries
        //Getting Order item ids from database query class
        updateOrderItems = DatabaseQueryHelper.getOrderItemByWorkOrderIds(workOrderIds);
        
        //Put flag OSM_Utilities.callFutureXMLGenerationRunOnce to prevent lock row - Integ issue. DQ 7/23/2015
        //Update OrderItem records related to the work order records on the trigger
        if(updateOrderItems.size() > 0 && !OSM_Utilities.callFutureXMLGenerationRunOnce){
            update updateOrderItems;
        }
    }
    
    /**
     * @author         Kristian Vegerano
     * @description    23465 - Order Status- Re-Open
     * @parameter      List of Work Order records 
     * @history        29.APR.2015 - Kristian vegerano - Created  
     */
    public static void reopenOrders(List<OSM_Work_Order__c> workOrderList){
        
        //Initialize Variables
        Set<Id> orderIds = new Set<Id>();
        
        //List of Order to be updated
        List<Order> listOrderToBeUpdated = new List<Order>();
        
        //Loop throough Trigger.New and populate set of id with Order id related to work oredre
        for(OSM_Work_Order__c loopWorkOrder : workOrderList)
            orderIds.add(loopWorkOrder.OSM_Order__c);
       
        //Code modiified - 06/18/2015 - As per  code refactorization - removing Queries
        //Getting the Order list from Database query 
        listOrderToBeUpdated = DatabaseQueryHelper.getOrderByOrderIds(orderIds);
        
        //Populate OSM_Status__c field based from the conditions
        //Loop through List of order
        for(Order loopOrder : listOrderToBeUpdated) {
            
            //Checking for condiotions
            if(loopOrder.status == 'Closed') 
                loopOrder.OSM_Status__c = 'Processing';
        }
        
        //Update orders
        if(listOrderToBeUpdated.size() > 0)
            update listOrderToBeUpdated;
    }
    
    /**
     * @author         Jerome Liwanag
     * @description    32043 -  Lab and Report Status to Processing
     * @parameter      Set of Work Order Ids
     * @history        19.MAY.2015 - Jerome Liwanag - Created  
     */
    
    public static void labReportStatusToProcessing(List<OSM_Work_Order__c> workOrderList){
        Set<Id> workOrderIds = new Set<Id>();
        Map<Id,List<OrderItem>> orderItemMap = new Map<Id,List<OrderItem>>();
        List<OrderItem> orderItemList = new List<OrderItem>();
        
        for(OSM_Work_Order__c loopWorkOrder : workOrderList){
            workOrderIds.add(loopWorkOrder.Id);
        }
        
        for(OrderItem orderItemLoop : [SELECT Id, OSM_Lab_and_Report_Status__c,OSM_Work_Order__c FROM OrderItem WHERE OSM_Work_Order__c IN :workOrderIds]){
            if(!orderItemMap.containsKey(orderItemLoop.OSM_Work_Order__c)){
                orderItemMap.put(orderItemLoop.OSM_Work_Order__c, new List<OrderItem>{orderItemLoop});
            }else{
                orderItemMap.get(orderItemLoop.OSM_Work_Order__c).add(orderItemLoop);
            }
        }
        
        Map<Id,Integer> totalWOLI = new Map<Id,Integer>();
        Map<Id,Integer> processingWOLI = new Map<Id,Integer>();
        
        for(OSM_Work_Order_Line_Item__c loopWOLI : [SELECT Id, OSM_Work_Order__c, OSM_Order_Line_Item_Status__c, OSM_Order_Product_Data_Entry_Status__c, OSM_Order_Product__c, 
                                                        OSM_Order_Product__r.OSM_Data_Entry_Status__c, OSM_Order_Product__r.OSM_State__c, OSM_Order_Product__r.OSM_Lab_Ready__c, 
                                                        OSM_Order_Product__r.OSM_Report_Distribution_Ready__c
                                                    FROM OSM_Work_Order_Line_Item__c 
                                                    WHERE OSM_Work_Order__c IN :workOrderIds]){
            if(!totalWOLI.containsKey(loopWOLI.OSM_Work_Order__c)){
                totalWOLI.put(loopWOLI.OSM_Work_Order__c, 1);
            }else{
                totalWOLI.put(loopWOLI.OSM_Work_Order__c, totalWOLI.get(loopWOLI.OSM_Work_Order__c) + 1);
            }
            if(loopWOLI.OSM_Order_Product__r.OSM_Data_Entry_Status__c != 'Initial Data Entry' && 
               loopWOLI.OSM_Order_Product__r.OSM_Data_Entry_Status__c != 'Awaiting Lab Initiation Data' &&
               loopWOLI.OSM_Order_Product__r.OSM_Data_Entry_Status__c != 'Data Verification Complete' &&
               loopWOLI.OSM_Order_Product__r.OSM_Data_Entry_Status__c != '' && 
               loopWOLI.OSM_Order_Product__r.OSM_Data_Entry_Status__c != null){
                if(!processingWOLI.containsKey(loopWOLI.OSM_Work_Order__c)){
                    processingWOLI.put(loopWOLI.OSM_Work_Order__c, 1);
                }else{
                    processingWOLI.put(loopWOLI.OSM_Work_Order__c, processingWOLI.get(loopWOLI.OSM_Work_Order__c) + 1);
                }
            }
        }
        
        for(OSM_Work_Order__c workOrderLoop : workOrderList){
            //Check if work order status is Processing
            if((workOrderLoop.OSM_Status__c == 'Processing') || (totalWOLI.get(workOrderLoop.Id) == processingWOLI.get(workOrderLoop.Id)) || workOrderLoop.OSM_Override_Status_to_Active__c){
                if(orderItemMap.containsKey(workOrderLoop.Id)){
                    for(OrderItem loopOrderItem : orderItemMap.get(workOrderLoop.Id)){
                        //Set the status of the order line item to processing if it is not
                        if(loopOrderItem.OSM_Lab_and_Report_Status__c == 'Pre-Processing' || loopOrderItem.OSM_Lab_and_Report_Status__c == 'Specimen Received'){
                            loopOrderItem.OSM_Lab_and_Report_Status__c = 'Processing';
                            orderItemList.add(loopOrderItem);
                        }
                    }
                }
            }
        }
        
        if(!orderItemList.isEmpty() && !OSM_Utilities.callFutureXMLGenerationRunOnce){
            update orderItemList;
        }
    }
    
    /**
     * @author         Jerome Liwanag
     * @description    User Story 30548 - Lab and Report Status to Pre-Distribution
     *                 User Story 14672 - Lab and Report Status to Distributing
     *                 User Story 30547 - Lab and Report Status to Distributed
     *                 User Story 32061 - Lab and Report Status to Obsolete
     * @parameter      List of Work Order Records
     * @history        1.JUNE.2015 - Jerome Liwanag - Created  
     */    
    public static void labReportStatusUpdate(List<OSM_Work_Order__c> workOrderList, Map<Id,OSM_Work_Order__c> oldWorkOrderMap){
        Set<Id> workOrderIds = new Set<Id>();
        Map<Id,OrderItem> orderItemMap = new Map<Id,OrderItem>();
        List<OrderItem> orderItemList = new List<OrderItem>();
        //Get the associated work order 
        for(OSM_Work_Order__c loopWorkOrder : workOrderList){
            workOrderIds.add(loopWorkOrder.Id);
        }
        for(OrderItem loopOrderItem : [Select OSM_Work_Order__c,OSM_Data_Entry_Status__c From OrderItem WHERE OSM_Work_Order__c IN :workOrderIds]){
            orderItemMap.put(loopOrderItem.OSM_Work_Order__c,loopOrderItem);
        }
        for(OSM_Work_Order__c loopWorkOrder : workOrderList){
            if(orderItemMap.containsKey(loopWorkOrder.Id)){
                orderItemMap.get(loopWorkOrder.Id).OSM_Work_Order__c = loopWorkOrder.Id;
                //Check if work order field ready for specimen is true
                orderItemList.add(orderItemMap.get(loopWorkOrder.Id));
            }
        }
        
        if(!orderItemList.isEmpty() && !OSM_Utilities.callFutureXMLGenerationRunOnce){
            update orderItemList;
        }
    }
    
    /**
     *  @description    :   Use for Integration
     *
     *  @return         :
     *
     *  @args           :   List<OrderItem> newOrderItems , Map<Id, OrderItem> mapOldOrderItems
    **/
    public static void populateFieldsForWebService(List<OSM_Work_Order__c> newWorkOrderItems , Map<Id, OSM_Work_Order__c> mapOldWorkOrderItems) {
        
        if(OSM_Utilities.generateXMLForWorkOrderRunOnce){
            return;
        }
        
        for(OSM_Work_Order__c woItem : newWorkOrderItems) {
            
            //Checking  for Conditions to fire trigger
            if(string.isNotBlank(woItem.OSM_Status__c) && woItem.OSM_Status__c != 'Created'
                    && (mapOldWorkOrderItems == null 
                         || mapOldWorkOrderItems.get(woItem.Id).OSM_Status__c != woItem.OSM_Status__c)){
                            woItem.Send_Order_To_Lab__c = true;
            } else {
                //woItem.Send_Order_To_Lab__c = false; 
            }
            
        }
        
    }
    
}