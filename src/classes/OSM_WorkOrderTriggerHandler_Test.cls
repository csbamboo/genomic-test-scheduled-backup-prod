/*
  @author: Amanpreet Singh Sidhu 
  @date: 21 June 2015
  @description: Test Class for OSM_WorkOrderTriggerHandler apex class
  @history: 21 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest
public class OSM_WorkOrderTriggerHandler_Test 
{
  
    static testMethod void testExt() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = true;
        triggerSwitch.Work_Order_Trigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id pricebookStandard = Test.getStandardPricebookId();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        order1.PriceBook2Id = pricebookStandard;
        insert order1;
                
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(order1.id);
        insert workOrder;
        
        Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
        insert pbe;
        
        OrderItem ordItem = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , UnitPrice = 2 , Quantity = 1, OSM_Lab_and_Report_Status__c = 'Pre-Processing');
        insert ordItem;
        
        OrderItem ordItem2 = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , UnitPrice = 5 , Quantity = 2, OSM_Lab_and_Report_Status__c = 'Pre-Processing');
        insert ordItem2;
        
        List<OSM_Work_Order__c> lstworkOrder = new List<OSM_Work_Order__c>([Select id, OSM_Order__c from OSM_Work_Order__c where id =: workOrder.id]);
        
        OSM_Work_Order_Line_Item__c woli = new OSM_Work_Order_Line_Item__c(
            OSM_Order_Product__c = ordItem.Id, 
            OSM_Bill_Account__c = lstworkOrder[0].OSM_Order__c, 
            OSM_Work_Order__c = lstworkOrder[0].Id
        );
        insert woli;
        
        for(OSM_Work_Order__c wo : lstworkOrder)
        {
            wo.OSM_Status__c = 'Processing';
        }
        
        Test.startTest();
        OSM_WorkOrderTriggerHandler.runLabReportStatusToProcessing = false;   
        OSM_WorkOrderTriggerHandler.hasreopenOrdersRun = false; 
        OSM_WorkOrderTriggerHandler.hasUpdateOrderItemsRun = false;
        OSM_WorkOrderTriggerHandler.runLabReportStatusUpdate = false;
        
        update lstworkOrder;
           
        Test.stopTest();
  }
  
}