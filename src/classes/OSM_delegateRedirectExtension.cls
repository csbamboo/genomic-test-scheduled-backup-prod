/*
    @author:patrick lorilla
    @date: 2 DEC 2014
    @description: Delegate Redirect Extension
*/
public with sharing class OSM_delegateRedirectExtension {

    Id hcoId = null;
    String name = null;
    String retURL = null;
        
    /*
    @author:patrick lorilla
    @date: 2 DEC 2014
    @description: Constructor
    @param: controller - standard controller
    */
    public OSM_delegateRedirectExtension (ApexPages.StandardController controller) {
        //from related list
        if(ApexPages.currentPage().getParameters().get('hcoid') != null && ApexPages.currentPage().getParameters().get('hconame') != null){
            hcoId = ApexPages.currentPage().getParameters().get('hcoid');  
            name= ApexPages.currentPage().getParameters().get('hconame'); 
        }   
        retURL = ApexPages.currentPage().getParameters().get('retURL');       
    }
    /*
    @author:patrick lorilla
    @date: 2 DEC 2014
    @description: redirect to our desired page with the paramters.
    */
    public pageReference redirect(){
        Record_Type__c recTypes = Record_Type__c.getOrgDefaults();
        PageReference returnURL = new PageReference('/' + recTypes.ObjectId_Delegate__c + '/e?nooverride=1');
        returnURL.getParameters().put('Name', 'Delegate');
        //from related list
        if(ApexPages.currentPage().getParameters().get('hcoid') != null && ApexPages.currentPage().getParameters().get('hconame') != null){
            returnURL.getParameters().put('CF'+recTypes.CF_Delegate_HCO_Name__c+ '_lkid', hcoId);
            returnURL.getParameters().put('CF'+recTypes.CF_Delegate_HCO_Name__c, name);
        }
       
        returnURL.getParameters().put('retURL', retURL);
        
        returnURL.setRedirect(true);
        return returnURL;
    }

}