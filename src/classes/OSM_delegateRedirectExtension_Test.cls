/*
    @author: patrick lorilla
    @description: Test delegate redirect extension
    @date: 04 December 2014
*/
@isTest
public class OSM_delegateRedirectExtension_Test{
      /*
        @author: patrick lorilla
        @description: Test page redirect
        @date: 04 December 2014
      */
      static testMethod void pageRedirect1(){
          //prepare record types, account and delegate
          Record_Type__c recTypes = OSM_DataFactory.recordLists();
          insert recTypes;
          Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
          insert acct;
          Contact con = OSM_DataFactory.createContact(1,acct.ID,null);
          insert con;
          Contact hcp = OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
          insert hcp ;
          OSM_Delegate__c del = OSM_DataFactory.createDelegate(con.ID, acct.ID, hcp.ID, 'Create Orders');
          insert del;
          //prepare apex page
          PageReference pageRef = Page.OSM_delegateRedirect;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(del);
          ApexPages.currentPage().getParameters().put('hcoid',acct.ID);
          ApexPages.currentPage().getParameters().put('hconame',acct.ID);
          //run vf page extension
          OSM_delegateRedirectExtension cons = new OSM_delegateRedirectExtension(sc);
          //assert page redirection
          System.assert(cons.redirect() != null);
      }
      /*
        @author: patrick lorilla
        @description: Test page redirect
        @date: 04 December 2014
      */
      static testMethod void pageRedirect2(){
          //prepare record types, account and delegate
          Record_Type__c recTypes = OSM_DataFactory.recordLists();
          insert recTypes;
          Account acct  =  OSM_DataFactory.createAccountWithBillingAddress(0,'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', recTypes.Account_HCO_Record_Type__c) ;
          insert acct;
          Contact con = OSM_DataFactory.createContact(1,acct.ID,null);
          insert con;
          Contact hcp = OSM_DataFactory.createContact(1,acct.ID,recTypes.Contact_HCP_Record_Type__c);
          insert hcp ;
          OSM_Delegate__c del = OSM_DataFactory.createDelegate(con.ID, acct.ID, hcp.ID, 'Create Orders');
          insert del;
          //prepare apex page
          PageReference pageRef = Page.OSM_delegateRedirect;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(del);
   
          //run vf page extension
          OSM_delegateRedirectExtension cons = new OSM_delegateRedirectExtension(sc);
          //assert page redirection
          System.assert(cons.redirect() != null);
      }

}