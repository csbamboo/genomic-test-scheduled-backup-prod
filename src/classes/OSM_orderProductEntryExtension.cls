/*
    @author: Kristian Vegerano
    @description: Extension for Order Product Entry page
    @createdDate: 12 APR 2015
    @history: 
        14 APR 2015 - added class header   Rescian Rey
        16 APR 2015 - redirect to same page on save and cancel
                      handle redirect on page itself (for console).
                      (Rescian)
*/
public with sharing class OSM_orderProductEntryExtension {

    public boolean isWorkOrder {get;set;}
    public boolean isOrderItem {get;set;}
    public boolean hasChanged {get;set;}
    public boolean hasStateError {get;set;}
    public Order theOpp {get;set;}
    public String searchString {get;set;}
    public orderItem[] shoppingCart {get;set;}
    public priceBookEntry[] AvailableProducts {get;set;}
    public List<OrderItem> availableOrderItems {get;set;}
    public Pricebook2 theBook {get;set;}   

    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public Decimal Total {get;set;}
    
    public boolean overLimit {get;set;}
    public boolean multipleCurrencies {get; set;}
    
    private boolean isConstructor = true;
    private boolean forcePricebookSelection = false;
    private Id mainOrderId {get;set;}
    public Id workOrderId {get;set;}
    public Id orderItemId {get;set;}
    private String orderItemName {get;set;}
    private String workOrderName {get;set;}
    private String orderItemLabStatus {get;set;}
    private orderItem[] forDeletion = new orderItem[]{};
    private Set<Id> pricebookEntryIds = new Set<Id>();

    public List<OSM_Work_Order_Line_Item__c> shoppingWorkLineItemCart {get;set;}
    public List<OSM_Work_Order_Line_Item__c> deleteWorkLineItemCart {get;set;}
    public List<Order> availableOrder {get;set;}
    public List<OSM_Work_Order__c> availableWorkOrder {get;set;}
    
    public OSM_orderProductEntryExtension(ApexPages.StandardController controller) {
        isWorkOrder = false;    
        availableOrderItems = new List<OrderItem>(); 
        availableWorkOrder = new List<OSM_Work_Order__c>();
        AvailableProducts = new List<PriceBookEntry>(); 
        theBook = new PriceBook2(); 
        availableOrder = new List<Order>();
        shoppingWorkLineItemCart = new List<OSM_Work_Order_Line_Item__c>();
        deleteWorkLineItemCart = new List<OSM_Work_Order_Line_Item__c>();
        hasChanged = false;
        isConstructor = true;
        // Need to know if org has multiple currencies enabled
        multipleCurrencies = UserInfo.isMultiCurrencyOrganization();

        // Get information about the Order being worked on
        if(Order.sObjectType.getDescribe().getKeyPrefix() == String.valueOf(controller.getRecord().Id).LEFT(3)){
            theOpp = [select Id, Pricebook2Id, PriceBook2.Name, OSM_Product__c, RecordTypeId, OSM_Order__c, OSM_Work_Order_ID__c, CurrencyIsoCode
                      from Order 
                      where Id = :controller.getRecord().Id limit 1];
            isOrderItem = false;
            mainOrderId = theOpp.Id;
            isWorkOrder = false;
            
            // If products were previously selected need to put them in the "selected products" section to start with
            shoppingCart = [select Id, Quantity, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, 
            PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, 
            PriceBookEntry.PriceBook2Id from OrderItem where OrderId=:mainOrderId];
            
            
            // Check if Opp has a pricebook associated yet
            if(theOpp.Pricebook2Id == null){
                Pricebook2[] activepbs = [select Id, Name from Pricebook2 where isActive = true limit 2];
                if(activepbs.size() == 2){
                    forcePricebookSelection = true;
                    theBook = new Pricebook2();
                }
                else{
                    theBook = activepbs[0];
                }
            }
            else{
                theBook = theOpp.Pricebook2;
            }
            
            if(!forcePricebookSelection){
                updateAvailableList();
            }
        }else if(OSM_Work_Order__c.sObjectType.getDescribe().getKeyPrefix() == String.valueOf(controller.getRecord().Id).LEFT(3)){
            theOpp = [SELECT Id, Pricebook2Id, PriceBook2.Name, OSM_Product__c, RecordTypeId, OSM_Order__c, OSM_Work_Order_ID__c,CurrencyIsoCode
                      FROM Order 
                      WHERE Id IN (SELECT OSM_Order__c FROM OSM_Work_Order__c WHERE Id = :controller.getRecord().Id) LIMIT 1];
                      
            mainOrderId = theOpp.Id;
            workOrderId = controller.getRecord().Id;  
            workOrderName = theOpp.OSM_Work_Order_ID__c;   
            isWorkOrder = true;
            isOrderItem = false;
            for(OrderItem loopOrderItem : [SELECT Id, PricebookEntryId FROM OrderItem WHERE OrderId = :mainOrderId LIMIT 100]){
                pricebookEntryIds.add(loopOrderItem.PricebookEntryId);
            }
            
            if([SELECT Id, OSM_State__c FROM OSM_Work_Order__c WHERE Id = :workOrderId LIMIT 1].OSM_State__c.toLowerCase() == 'closed'){
                hasStateError = true;
            }else{
                hasStateError = false;
            }
            
            shoppingWorkLineItemCart = [SELECT Id, OSM_Order_Product__c, OSM_Order_Product__r.PricebookEntry.Name, OSM_Work_Order__c, OSM_Name_of_Test__c FROM OSM_Work_Order_Line_Item__c WHERE OSM_Work_Order__c = :workOrderId];
            
            // Check if Opp has a pricebook associated yet
            if(theOpp.Pricebook2Id == null){
                Pricebook2[] activepbs = [select Id, Name from Pricebook2 where isActive = true limit 2];
                if(activepbs.size() == 2){
                    forcePricebookSelection = true;
                    theBook = new Pricebook2();
                }
                else{
                    theBook = activepbs[0];
                }
            }
            else{
                theBook = theOpp.Pricebook2;
            }
            
            if(!forcePricebookSelection){
                updateWorkOrderAvailableList();
            }
        }else{
            orderItemId = controller.getRecord().Id;
            if([SELECT Id, OSM_State__c FROM OrderItem WHERE Id = :orderItemId LIMIT 1].OSM_State__c.toLowerCase() == 'closed'){
                hasStateError = true;
            }else{
                hasStateError = false;
            }

            /*
                @author         Raus Kenneth Ablaza
                @date           17 April 2015
                @detail         Optimized usage of SOQL
            */

            OrderItem orderItemQuery = [
                SELECT Id, OrderId, PricebookEntry.Name, OSM_Lab_and_Report_Status__c 
                FROM OrderItem 
                WHERE Id = :controller.getRecord().Id
            ];

            if(orderItemQuery == null){
                hasStateError = true;
            }
            else{
                hasStateError = false;
            }

            orderItemName = orderItemQuery.PricebookEntry.Name;
            orderItemLabStatus = orderItemQuery.OSM_Lab_and_Report_Status__c;
            Id parentOrderId = orderItemQuery.OrderId;
            /*
            orderItemName = [SELECT Id, OrderId, PricebookEntry.Name FROM OrderItem WHERE Id = :controller.getRecord().Id].PricebookEntry.Name;
            orderItemLabStatus = [SELECT Id, OSM_Lab_and_Report_Status__c FROM OrderItem WHERE Id = :controller.getRecord().Id].OSM_Lab_and_Report_Status__c;
            Id parentOrderId = [SELECT Id, OrderId FROM OrderItem WHERE Id = :controller.getRecord().Id].OrderId;
            */
            theOpp = [select Id, Pricebook2Id, PriceBook2.Name, OSM_Product__c, RecordTypeId, OSM_Order__c,CurrencyIsoCode
                      from Order 
                      where Id = :parentOrderId limit 1];
            isOrderItem = true;
            isWorkOrder = true;
            availableWorkOrder = [SELECT Id, Name, OSM_Order__c, OSM_Patient_DOB__c, OSM_Patient_Full_Name__c, OSM_Specimen_Group_ID__c, OSM_State__c, OSM_Status__c, OSM_Type__c FROM OSM_Work_Order__c WHERE OSM_Order__c = :theOpp.Id];
            shoppingWorkLineItemCart = [SELECT Id, Name, OSM_Work_Order_ID__c, OSM_Order_Product__c, OSM_Order_Product__r.PricebookEntry.Name, OSM_Work_Order__c, OSM_Name_of_Test__c FROM OSM_Work_Order_Line_Item__c WHERE OSM_Order_Product__c = :orderItemId];
        }
    }
    
    // this is the 'action' method on the page
    public PageReference priceBookCheck(){
        // if the user needs to select a pricebook before we proceed we 
        //send them to standard pricebook selection screen
        if(forcePricebookSelection){        
            return changePricebook();
        }
        else{
        
            //if there is only one active pricebook we go with it and save the opp
            if(theOpp.pricebook2Id != theBook.Id){
                try{
                    theOpp.Pricebook2Id = theBook.Id;
                    update(theOpp);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
        }
    }
       
    public String getChosenCurrency(){
    
        if(multipleCurrencies)
            return (String)theOpp.get('CurrencyIsoCode');
        else
            return '';
    }

    public void updateAvailableList() {
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, '
        + 'Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where '
        + 'IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
                
        if(multipleCurrencies)
            qstring += ' and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';
        
        // note that we are looking for the search string entered by the user in the name OR description
        // modify this to search other fields if desired
        if(searchString!=null){
            qString+= ' and (Product2.Name like \'%' + String.escapeSingleQuotes(searchString) + '%\' or ' + 
            'Product2.Description like \'%' + String.escapeSingleQuotes(searchString) + '%\')';
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        //6-11-2015 - Don Ollila - Removed to allow multiple Order Line Items with the same Test(Product)
        /*for(OrderItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }*/
                
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        }
               
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        AvailableProducts = database.query(qString);

        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(AvailableProducts.size()==101){
            AvailableProducts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
    }
    
    public void updateWorkOrderAvailableList() {
        //Get Work Order Line Items
        Set<Id> attachedLineItemIds = new Set<Id>();
        for(OSM_Work_Order_Line_Item__c loopLineItem : [SELECT Id, OSM_Order_Product__c, OSM_Name_of_Test__c FROM OSM_Work_Order_Line_Item__c WHERE OSM_Work_Order__c = :workOrderId]){
            attachedLineItemIds.add(loopLineItem.OSM_Order_Product__c);
        }
        
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'SELECT Id, OSM_Lab_and_Report_Status__c, PricebookEntry.Pricebook2Id, PricebookEntry.IsActive, PricebookEntry.Product2.Name, PricebookEntry.Product2.Family, PricebookEntry.Product2.IsActive, PricebookEntry.Product2.Description, PricebookEntry.UnitPrice, Quantity, UnitPrice, Description, OSM_State__c FROM OrderItem WHERE OrderId = \'' + mainOrderId + '\'';
        /*if(isConstructor){
            if(!isWorkOrder){
                qString += ' AND Id NOT IN :attachedLineItemIds';
            }else{
                qString += ' AND Id IN :attachedLineItemIds';
            }
        }else{
            qString += ' AND Id IN :attachedLineItemIds';
        }
        if(multipleCurrencies)
            qstring += ' and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';
        
        // note that we are looking for the search string entered by the user in the name OR description
        // modify this to search other fields if desired
        */
        if(searchString!=null){
            qString+= ' and (PricebookEntry.Product2.Name like \'%' + String.escapeSingleQuotes(searchString) + '%\' or ' + 
            'PricebookEntry.Product2.Description like \'%' + String.escapeSingleQuotes(searchString) + '%\')';
        }
        /*
        Set<Id> selectedEntries = new Set<Id>();
        for(orderItem d:shoppingCart){
            selectedEntries.add(d.Id);
        }
          
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';    
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            if(!isWorkOrder){
                qString+= extraFilter;
            }
        }
               
        qString+= ' order by PricebookEntry.Product2.Name';
        qString+= ' limit 101';*/
        availableOrderItems = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(availableOrderItems.size()==101){
            availableOrderItems.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
    }
    
    public void addToShoppingCart(){
        hasChanged = true;
        // This function runs when a user hits "select" button next to a product
        if(!isWorkOrder){  
            for(PricebookEntry d : AvailableProducts){
                if((String)d.Id==toSelect){
                    shoppingCart.add(new OrderItem(OrderId=mainOrderId, PriceBookEntry=d, 
                    PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice, Quantity=1, OSM_Work_Order__c=workOrderId));
                    break;
                }
            }
            updateAvailableList();  
        }else if(isOrderItem){
            for(OSM_Work_Order__c loopOrder : availableWorkOrder){
                if((String)loopOrder.Id==toSelect){
                    OSM_Work_Order_Line_Item__c newWOLI = new OSM_Work_Order_Line_Item__c(OSM_Order_Line_Item_Status__c = orderItemLabStatus, OSM_Order_Product__c = orderItemId, OSM_Work_Order__c = loopOrder.Id, OSM_Name_of_Test__c = orderItemName, OSM_Work_Order_ID__c = loopOrder.Name);
                    insert newWOLI;
                    update new OrderItem(Id = orderItemId, OSM_Work_Order__c = loopOrder.Id);
                    shoppingWorkLineItemCart.add(newWOLI);
                    break;
                }
            }
        }else{
            for(OrderItem loopOrderItem : availableOrderItems){
                if((String)loopOrderItem.Id==toSelect){
                    OSM_Work_Order_Line_Item__c newWOLI = new OSM_Work_Order_Line_Item__c(OSM_Order_Line_Item_Status__c = loopOrderItem.OSM_Lab_and_Report_Status__c, OSM_Order_Product__c = loopOrderItem.Id, OSM_Work_Order__c = workOrderId, OSM_Name_of_Test__c = loopOrderItem.PricebookEntry.Product2.Name, OSM_Work_Order_ID__c = workOrderName);
                    insert newWOLI;
                    update new OrderItem(Id = loopOrderItem.Id, OSM_Work_Order__c = workOrderId);
                    shoppingWorkLineItemCart.add(newWOLI);
                    break;
                }
            }
            updateWorkOrderAvailableList();  
        }
    }
    

    public PageReference removeFromShoppingCart(){
        hasChanged = true;
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
        Integer count = 0;
        
        if(!isWorkOrder){  
            for(OrderItem d : shoppingCart){
                if((String)d.PriceBookEntryId==toUnselect){
                
                    if(d.Id!=null)
                        forDeletion.add(d);
                
                    shoppingCart.remove(count);
                    break;
                }
                count++;
            }
            updateAvailableList();
        }else if(isOrderItem){
            for(OSM_Work_Order_Line_Item__c d : shoppingWorkLineItemCart){
                if((String)d.Id==toUnselect){
                    if(d.Id!=null)
                        delete d;
                        deleteWorkLineItemCart.add(d);
                
                    shoppingWorkLineItemCart.remove(count);
                    break;
                }
                count++;
            }
            
            Set<Id> workOrderIds = new Set<Id>();
            for(OSM_Work_Order_Line_Item__c d : shoppingWorkLineItemCart){
                workOrderIds.add(d.OSM_Work_Order__c);
            }
            List<OSM_Work_Order__c> closedWOList = new List<OSM_Work_Order__c>();
            closedWOList = [SELECT Id, OSM_State__c FROM OSM_Work_Order__c WHERE Id IN :workOrderIds AND OSM_State__c = 'Closed'];
            isConstructor = false;
        }else{
            for(OSM_Work_Order_Line_Item__c d : shoppingWorkLineItemCart){
                if((String)d.Id==toUnselect){
                    if(d.Id!=null)
                        delete d;
                        deleteWorkLineItemCart.add(d);
                
                    shoppingWorkLineItemCart.remove(count);
                    break;
                }
                count++;
            }
            
            Set<Id> orderItemIds = new Set<Id>();
            for(OSM_Work_Order_Line_Item__c d : shoppingWorkLineItemCart){
                orderItemIds.add(d.OSM_Order_Product__c);
            }
            List<OrderItem> closedOrderItemsList = new List<OrderItem>();
            closedOrderItemsList = [SELECT Id, OSM_State__c FROM OrderItem WHERE Id IN :orderItemIds AND OSM_State__c = 'Closed'];
            isConstructor = false;
            updateWorkOrderAvailableList();
        }
                
        return null;
    }
    
    public PageReference onSave(){
        // If previously selected products are now removed, we need to delete them
        if(forDeletion.size()>0){
            OSM_ValidationBuilder.runValidationBuilder = false;
            delete(forDeletion);
            OSM_ValidationBuilder.runValidationBuilder = true;
        }
        if(deleteWorkLineItemCart.size() > 0){
            try{
                delete deleteWorkLineItemCart;
            }
            catch(Exception ex){
                 Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.OP_Already_Deleted));
            }
        }
        // Previously selected products may have new quantities and amounts, and we may have new 
        //products listed, so we use upsert here
        try{
            if(isWorkOrder){
                Set<Id> existingOrderItemIds = new Set<Id>();
                Set<Id> orderItemIds = new Set<Id>();
                List<OSM_Work_Order_Line_Item__c> insertLineItems = new List<OSM_Work_Order_Line_Item__c>();
                insertLineItems = shoppingWorkLineItemCart;
                    
                if(insertLineItems.size() > 0){
                    upsert insertLineItems;
                }
            }
            
            if(!isWorkOrder){
                if(shoppingCart.size()>0){
                    upsert(shoppingCart);
                }
            }
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
        return null;
    }
    
    public PageReference onCancel(){
        // If user hits cancel we commit no changes and return them to the Order   
        return null;
    }
    
    public PageReference changePricebook(){
    
        // This simply returns a PageReference to the standard Pricebook selection screen
        // Note that is uses retURL parameter to make sure the user is sent back after they choose
        PageReference ref = new PageReference('/oppitm/choosepricebook.jsp');
        ref.getParameters().put('id',theOpp.Id);
        ref.getParameters().put('retURL','/apex/OSM_orderProductEntry?id=' + theOpp.Id);
        
        return ref;
    }
}