/*
      @author: David Catindoy
      @date: 28 JAN 2015 - Created
      @description: Order Product Entry Extension Test
      @history: 28 JAN 2015 - Updated (David Catindoy)

*/
@isTest(seeAllData=true)
private class OSM_orderProductEntryExtension_Test {

    public static void updateTriggers(){
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        update triggerSwitch;
    }
    
    public static testMethod void testOrderProductEntryExtensionOrder(){
        updateTriggers();
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        List<OSM_Work_Order_Line_Item__c> woliList = new List<OSM_Work_Order_Line_Item__c>();
        Set<Id> idList;
        PriceBook2 pb2Standard = [select Id from PriceBook2 where isStandard=true];
        System.debug('******************************pb2standard value: ' + pb2Standard);

        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;

        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        for(Integer i = 0; i < 2; i++){
            prodList.add(OSM_DataFactory.createProduct('IBC', true));
        }
        insert prodList;
        
        for(Integer i = 0; i < 2; i++){
            pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, pb2Standard.Id, 3));
        }
        insert pbeList;
        idList = new Set<Id>();
        for(PricebookEntry pbe : pbeList){
            idList.add(pbe.Id);
        }
        pbeList = (List<PricebookEntry>) getAllFields('PricebookEntry', idList, null);

        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_State__c = 'Active', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
                //CurrencyIsoCode = 'USD', 
                Pricebook2Id = pb2Standard.Id, 
                EffectiveDate = System.Today(), 
                Status = 'New'
            ));        
        }
        insert ordList;
        idList = new Set<Id>();
        for(Order ord : ordList){
            idList.add(ord.Id);
        }
        ordList = (List<Order>) getAllFields('Order', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            oliList.add(new OrderItem(
                OrderId = ordList[i].Id , 
                PricebookEntryId = pbeList[i].Id , 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                OSM_State__c = 'Active', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        insert oliList;
        idList = new Set<Id>();
        for(OrderItem oli : oliList){
            idList.add(oli.Id);
        }
        oliList = (List<OrderItem>) getAllFields('OrderItem', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            woList.add(OSM_DataFactory.createWorkOrder(ordList[i].Id));
        }
        insert woList;
        idList = new Set<Id>();
        for(OSM_Work_Order__c wo : woList){
            idList.add(wo.Id);
        }
        woList = (List<OSM_Work_Order__c>) getAllFields('OSM_Work_Order__c', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            woliList.add(new OSM_Work_Order_Line_Item__c(
                OSM_Bill_Account__c = ordList[i].Id, 
                OSM_Order_Product__c = oliList[i].Id, 
                OSM_Work_Order__c = woList[i].Id 
            ));
        }
        insert woliList;

        PageReference pageRef = Page.OSM_orderProductEntry;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc;
        OSM_orderProductEntryExtension cons;

        Test.startTest();

        for(Order ord : ordList){
            sc = new ApexPages.StandardController(ord);
            ApexPages.currentPage().getParameters().put('Id',ord.id);
            cons = new OSM_orderProductEntryExtension(sc);
            // cons.toSelect = pbeList[0].Id;
            // cons.toUnselect = pbeList[0].Id;
            cons.multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
            // cons.multipleCurrencies = false;
            cons.searchString = prodList[0].Name;
            cons.priceBookCheck();
            cons.getChosenCurrency();
            cons.toSelect = (String) pbeList[0].Id;
            cons.addToShoppingCart();
            cons.toUnselect = (String) pbeList[1].Id;
            cons.removeFromShoppingCart();
            cons.toSelect = (String) ordList[0].Id;
            cons.addToShoppingCart();
            cons.toUnselect = (String) ordList[1].Id;
            cons.removeFromShoppingCart();
            cons.onSave();
            cons.onCancel();
            cons.changePricebook();
        }
        
        
        // for Orders without Pricebook2Id
        
        List<Order> ordList2 = new List<Order>();
        for(Integer i = 0; i < 2; i++){
            ordList2.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_State__c = 'Active', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'),  
                EffectiveDate = System.Today(), 
                Status = 'New'
            ));        
        }
        insert ordList2;
        idList = new Set<Id>();
        for(Order ord : ordList2){
            idList.add(ord.Id);
        }
        ordList2 = (List<Order>) getAllFields('Order', idList, null);
        
        for(Order ord : ordList2){
            sc = new ApexPages.StandardController(ord);
            ApexPages.currentPage().getParameters().put('Id',ord.id);
            cons = new OSM_orderProductEntryExtension(sc);
        }

        Test.stopTest();
    }
    
    public static testMethod void testOrderProductEntryExtensionWO(){
    
        updateTriggers();
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        List<OSM_Work_Order_Line_Item__c> woliList = new List<OSM_Work_Order_Line_Item__c>();
        Set<Id> idList;
        PriceBook2 pb2Standard = [select Id from PriceBook2 where isStandard=true];
        System.debug('******************************pb2standard value: ' + pb2Standard);

        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;

        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        for(Integer i = 0; i < 2; i++){
            prodList.add(OSM_DataFactory.createProduct('IBC', true));
        }
        insert prodList;
        
        for(Integer i = 0; i < 2; i++){
            pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, pb2Standard.Id, 3));
        }
        insert pbeList;
        idList = new Set<Id>();
        for(PricebookEntry pbe : pbeList){
            idList.add(pbe.Id);
        }
        pbeList = (List<PricebookEntry>) getAllFields('PricebookEntry', idList, null);

        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_State__c = 'Active', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
                //CurrencyIsoCode = 'USD', 
                Pricebook2Id = pb2Standard.Id, 
                EffectiveDate = System.Today(), 
                Status = 'New'
            ));        
        }
        insert ordList;
        idList = new Set<Id>();
        for(Order ord : ordList){
            idList.add(ord.Id);
        }
        ordList = (List<Order>) getAllFields('Order', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            oliList.add(new OrderItem(
                OrderId = ordList[i].Id , 
                PricebookEntryId = pbeList[i].Id , 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                OSM_State__c = 'Active', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        insert oliList;
        idList = new Set<Id>();
        for(OrderItem oli : oliList){
            idList.add(oli.Id);
        }
        oliList = (List<OrderItem>) getAllFields('OrderItem', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            woList.add(OSM_DataFactory.createWorkOrder(ordList[i].Id));
        }
        insert woList;
        idList = new Set<Id>();
        for(OSM_Work_Order__c wo : woList){
            idList.add(wo.Id);
        }
        woList = (List<OSM_Work_Order__c>) getAllFields('OSM_Work_Order__c', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            woliList.add(new OSM_Work_Order_Line_Item__c(
                OSM_Bill_Account__c = ordList[i].Id, 
                OSM_Order_Product__c = oliList[i].Id, 
                OSM_Work_Order__c = woList[i].Id 
            ));
        }
        insert woliList;

        PageReference pageRef = Page.OSM_WorkOrderProductEntry;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc;
        OSM_orderProductEntryExtension cons;

        Test.startTest();
        
        for(OSM_Work_Order__c wo : woList){
            sc = new ApexPages.StandardController(wo);
            ApexPages.currentPage().getParameters().put('Id',wo.id);
            cons = new OSM_orderProductEntryExtension(sc);
            // cons.toSelect = wo.Id;
            // cons.toUnselect = wo.Id;
            cons.multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
            // cons.multipleCurrencies = false;
            cons.searchString = prodList[0].Name;
            cons.priceBookCheck();
            cons.getChosenCurrency();
            cons.toSelect = (String) pbeList[0].Id;
            cons.addToShoppingCart();
            cons.toUnselect = (String) pbeList[1].Id;
            cons.removeFromShoppingCart();
            cons.toSelect = (String) woList[0].Id;
            cons.addToShoppingCart();
            cons.toUnselect = (String) woList[1].Id;
            cons.removeFromShoppingCart();
            cons.onSave();
            cons.onCancel();
            cons.changePricebook();
        }
        
        // for Orders without Pricebook2Id 
        
        List<Order> ordList2 = new List<Order>();
        List<OSM_Work_Order__c> woList2 = new List<OSM_Work_Order__c>();
        
        for(Integer i = 0; i < 2; i++){
            ordList2.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_State__c = 'Active', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
                EffectiveDate = System.Today(), 
                Status = 'New'
            ));        
        }
        insert ordList2;
        
        for(Integer i = 0; i < 2; i++){
            woList2.add(OSM_DataFactory.createWorkOrder(ordList2[i].Id));
        }
        insert woList2;
        
        for(OSM_Work_Order__c wo : woList2){
            sc = new ApexPages.StandardController(wo);
            ApexPages.currentPage().getParameters().put('Id',wo.id);
            cons = new OSM_orderProductEntryExtension(sc);
        }
        
        Test.stopTest();
    }
    
    public static testMethod void testOrderProductEntryExtensionOLI(){
    
        updateTriggers();
        system.debug('test orderprod');
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        List<OSM_Work_Order_Line_Item__c> woliList = new List<OSM_Work_Order_Line_Item__c>();
        Set<Id> idList;
        PriceBook2 pb2Standard = [select Id from PriceBook2 where isStandard=true];
        System.debug('******************************pb2standard value: ' + pb2Standard);

        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;

        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        for(Integer i = 0; i < 2; i++){
            prodList.add(OSM_DataFactory.createProduct('IBC', true));
        }
        insert prodList;
        
        for(Integer i = 0; i < 2; i++){
            pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, pb2Standard.Id, 3));
        }
        insert pbeList;
        idList = new Set<Id>();
        for(PricebookEntry pbe : pbeList){
            idList.add(pbe.Id);
        }
        pbeList = (List<PricebookEntry>) getAllFields('PricebookEntry', idList, null);

        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_State__c = 'Active', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
                //CurrencyIsoCode = 'USD', 
                Pricebook2Id = pb2Standard.Id, 
                EffectiveDate = System.Today(), 
                Status = 'New'
            ));        
        }
        insert ordList;
        idList = new Set<Id>();
        for(Order ord : ordList){
            idList.add(ord.Id);
        }
        ordList = (List<Order>) getAllFields('Order', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            oliList.add(new OrderItem(
                OrderId = ordList[i].Id , 
                PricebookEntryId = pbeList[i].Id , 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                OSM_State__c = 'Active', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        insert oliList;
        idList = new Set<Id>();
        for(OrderItem oli : oliList){
            idList.add(oli.Id);
        }
        oliList = (List<OrderItem>) getAllFields('OrderItem', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            woList.add(OSM_DataFactory.createWorkOrder(ordList[i].Id));
        }
        insert woList;
        idList = new Set<Id>();
        for(OSM_Work_Order__c wo : woList){
            idList.add(wo.Id);
        }
        woList = (List<OSM_Work_Order__c>) getAllFields('OSM_Work_Order__c', idList, null);
        
        for(Integer i = 0; i < 2; i++){
            woliList.add(new OSM_Work_Order_Line_Item__c(
                OSM_Bill_Account__c = ordList[i].Id, 
                OSM_Order_Product__c = oliList[i].Id, 
                OSM_Work_Order__c = woList[i].Id 
            ));
        }
        insert woliList;
        
        PageReference pageRef = Page.OSM_orderProductEntry;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc;
        OSM_orderProductEntryExtension cons;

        Test.startTest();
        
        // for(OrderItem oli : oliList){
            sc = new ApexPages.StandardController(oliList[0]);
            ApexPages.currentPage().getParameters().put('Id',oliList[0].id);
            //Id tryId = ApexPages.currentPage().getParameters().get('Id');
            
            //system.debug('test tryId ' + tryId);
            
            //String tryIdStr = String.valueOf(tryID);
            cons = new OSM_orderProductEntryExtension(sc);
            // cons.toSelect = oliList[0].Id;
            // cons.toUnselect = oliList[0].Id;
            cons.multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
            // cons.multipleCurrencies = false;
            cons.searchString = prodList[0].Name;
            cons.priceBookCheck();
            cons.getChosenCurrency();
            cons.toSelect = (String) pbeList[0].Id;
            cons.addToShoppingCart();
            cons.toUnselect = (String) pbeList[1].Id;
            cons.removeFromShoppingCart();
            cons.toSelect = (String) oliList[0].Id;
            //cons.toSelect = tryId;
            cons.addToShoppingCart();
            cons.toUnselect = (String) oliList[0].Id;
            cons.removeFromShoppingCart();
            cons.onSave();
            cons.onCancel();
            cons.changePricebook();
        // }

        Test.stopTest();
    }
    
    
    public static List<SObject> getAllFields(String sObj, Set<Id> idList, Integer recLimit){
        String selects = '';
        
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(sObj).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ 
                Schema.DescribeFieldResult fd = ft.getDescribe(); 
                // if (fd.isCreateable()){ 
                    selectFields.add(fd.getName());
                // }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
        
        String queryBuilder = 'select ' + selects + ' from ' + sObj + (idList != null ? ' where Id in :idList ' : '') + (recLimit != null ? ' limit :recLimit ' : '');
        
        return Database.query(queryBuilder);
    }

    
    static testMethod void runTestCase1() {

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Product2__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = false;
        update triggerSwitch;

        Id recTypeCon = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        // Id recTypeOrdrId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Work Order').getRecordTypeId();
        Id orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Account acct = OSM_DataFactory.createAccount('New Account');
        insert acct;
          
        Contact con = OSM_DataFactory.createContact('David', 'Catindoy', recTypeCon);
        con.AccountId = acct.Id;
        insert con;

        Pricebook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        
        Record_Type__c recTypes = Record_Type__c.getOrgDefaults();
        recTypes.Order_Work_Order_Record_Type__c = orderRecType;
        update recTypes;

        Order od = OSM_DataFactory.createOrder('New Order 1', con.Id, acct.Id, System.today(), 'Active');
        od.RecordTypeId = orderRecType;
        od.OSM_Patient__c = con.Id;
        od.AccountId = acct.Id;
        od.OSM_Status__c = 'Order Intake';
        od.OSM_State__c = 'Active';
        od.OSM_Product__c = 'DCIS';
        od.Pricebook2Id = pb2Standard.Id;
        od.EffectiveDate = System.Today();
        od.Status = 'New';
        insert od;

        OSM_Work_Order__c rec = new OSM_Work_Order__c();
         rec.OSM_Order__c = od.Id;
        
        // Order od1 = OSM_DataFactory.createOrder('New Order 2', con.Id, acct.Id, System.today(), 'Active');
        // od1.Pricebook2Id = pb2Standard.Id;
        // od1.OSM_Order__c = od.Id;
        // od1.RecordTypeId = orderRecType;
        
        OSM_Work_Order__c od1 = OSM_DataFactory.createWorkOrder(od.Id);
        insert od1;

        Order od2 = OSM_DataFactory.createOrder('New Order 3', con.Id, acct.Id, System.today(), 'Active');
        od2.OSM_Order__c = od.Id;
        od2.RecordTypeId = orderRecType;
        od2.OSM_Patient__c = con.Id;
        od2.AccountId = acct.Id;
        od2.OSM_Status__c = 'Order Intake';
        od2.OSM_State__c = 'Active';
        od2.OSM_Product__c = 'DCIS';
        od2.Pricebook2Id = pb2Standard.Id;
        od2.EffectiveDate = System.Today();
        od2.Status = 'New';
        insert od2;
        
        List<Product2> prodList = new List<Product2>();
        for(Integer i =0; i<200; i++){
            prodList.add(OSM_DataFactory.createProduct('test', true));
        }
        insert prodList;
        
        List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>();
        for(Integer i=0; i<200; i++){
            priceBookEntryList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, pb2Standard.Id, 7));
            priceBookEntryList[i].isActive = true;
        }
        insert priceBookEntryList;
        
        List<OrderItem> ordrItmList = new List<OrderItem>();
        for(Integer i=0; i<200; i++){
            ordrItmList.add(OSM_DataFactory.createOrderItem(od.Id, priceBookEntryList[i].Id, 7, 7, System.today().addDays(2), rec.Id));
        }
        insert ordrItmList;

        List<OSM_Work_Order_Line_Item__c> wOrderList = new List<OSM_Work_Order_Line_Item__c>();
        for(Integer i=0 ; i<200; i++){
            wOrderList.add(new OSM_Work_Order_Line_Item__c(OSM_Order_Product__c = ordrItmList[i].Id, OSM_Work_Order__c = od1.Id));
        }
        //wOrderList.add(new OSM_Work_Order_Line_Item__c(OSM_Order_Product__c = ordrItmList[0].Id, OSM_Work_Order__c = od1.Id));
        insert wOrderList; 

        PageReference pageRef = Page.OSM_orderProductEntry;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(od1);
        ApexPages.currentPage().getParameters().put('Id',od1.id);
        OSM_orderProductEntryExtension cons = new OSM_orderProductEntryExtension(sc);
        cons.searchString = prodList[0].Name;
        Test.startTest();
            
            cons.priceBookCheck();
            cons.getChosenCurrency();
            cons.addToShoppingCart();
            cons.shoppingWorkLineItemCart.add(wOrderList[199]);
            cons.toUnselect = (String) wOrderList[199].Id;
            cons.removeFromShoppingCart();
            cons.onSave();
            cons.onCancel();
            cons.changePricebook();

            sc = new ApexPages.StandardController(od2);
            ApexPages.currentPage().getParameters().put('Id',od2.id);
            cons = new OSM_orderProductEntryExtension(sc);
            cons.searchString = prodList[0].Name;
            cons.toSelect = (String) ordrItmList[0].Id;
            cons.priceBookCheck();
            cons.getChosenCurrency();
            cons.updateWorkOrderAvailableList();
            cons.addToShoppingCart();
            cons.removeFromShoppingCart();
            cons.onSave();
            cons.onCancel();
            cons.changePricebook();

            List<Pricebook2> newPriceBook = [SELECT Id, Name from Pricebook2 where isActive = true];
            for(Pricebook2 priceBook2 : newPriceBook){
                priceBook2.isActive = false;
            }
            newPriceBook[0].isActive = true;
            update newPriceBook;

            sc = new ApexPages.StandardController(od2);
            ApexPages.currentPage().getParameters().put('Id',od2.id);
            cons = new OSM_orderProductEntryExtension(sc);

            // recTypes.Order_Work_Order_Record_Type__c = recTypeOrdrId;
            recTypes.Order_Work_Order_Record_Type__c = orderRecType;
            update recTypes;

            sc = new ApexPages.StandardController(od1);
            ApexPages.currentPage().getParameters().put('Id',od1.id);
            cons = new OSM_orderProductEntryExtension(sc);
            cons.searchString = prodList[0].Name;
            cons.priceBookCheck();
            cons.getChosenCurrency();
            cons.toSelect = (String) priceBookEntryList[101].Id;
            cons.addToShoppingCart();
            cons.toUnselect = (String) priceBookEntryList[100].Id;
            cons.removeFromShoppingCart();
            cons.onSave();
            cons.onCancel();
            cons.changePricebook();

            sc = new ApexPages.StandardController(ordrItmList[0]);
            ApexPages.currentPage().getParameters().put('Id',ordrItmList[0].id);
            cons = new OSM_orderProductEntryExtension(sc);
            cons.searchString = prodList[0].Name;
            cons.priceBookCheck();
            cons.getChosenCurrency();
            cons.toSelect = (String) od2.Id;
            cons.addToShoppingCart();
            cons.toUnselect = null;
            cons.removeFromShoppingCart();
            cons.toUnselect = (String) wOrderList[0].Id;
            cons.removeFromShoppingCart();
            cons.onSave();
            cons.onCancel();
            cons.changePricebook();

        Test.stopTest();

        System.assert(cons.isOrderItem);
        System.assert(cons.getChosenCurrency().equals('USD'));
    }
    
}