public with sharing class OSM_orderProductRedirectExtension {

    Id oppId;

    // we are extending the OpportunityLineItem controller, so we query to get the parent OpportunityId
    public OSM_orderProductRedirectExtension(ApexPages.StandardController controller) {
        oppId = [select Id, OrderId from OrderItem where Id = :controller.getRecord().Id 
        limit 1].OrderId;
    }
    
    // then we redirect to our desired page with the Opportunity Id in the URL
    public pageReference redirect(){
        return new PageReference('/apex/OSM_orderProductEntry?id=' + oppId);
    }

}