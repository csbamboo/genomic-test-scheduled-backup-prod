@isTest
private class OSM_orderProductRedirectExtension_Test {
    
    static testMethod void pageRedirect(){
    
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Order_Trigger__c = false;
        insert triggerSwitch;
        
        Id pb2Standard = Test.getStandardPricebookId();
         
        Account acct = OSM_DataFactory.createAccount('test');
        insert acct;
         
        Id recTypeCon = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId(); 
        Contact con = OSM_DataFactory.createContact('fName', 'lName', recTypeCon);
        insert con;

        Order od = OSM_DataFactory.createOrder('test', con.Id, acct.Id, system.today(), 'Active');
        od.pricebook2Id = pb2Standard;
        insert od;
        
        Product2 prod2 = OSM_DataFactory.createProduct('test', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pb2Standard, 7);
        insert pbe;
        
        OrderItem ordrItm = OSM_DataFactory.createOrderItem(od.Id, pbe.Id, 7, 7, system.today().addDays(5));
        insert ordrItm;
        
        Test.startTest();
            PageReference pageRef = Page.OSM_orderProductRedirect;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ordrItm);
            ApexPages.currentPage().getParameters().put('Id',ordrItm.id);
            OSM_orderProductRedirectExtension cons = new OSM_orderProductRedirectExtension(sc);
            cons.redirect();
        Test.stopTest();
        
    }

}