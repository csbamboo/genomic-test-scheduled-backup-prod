/**
 *  Purpose         :   Controller class for order  detail page for webservice callout on page load
 *
 *  Created By      :   
 * 
 *  Created Date    :   05/01/2015
 * 
 *  Revision Logs   :   V_1.0 - Created
 * 
 **/
public with sharing class OrderDetailPageController {
    
    //String to hold order id
    public String orderId;
    
    //Standard Controller
    public OrderDetailPageController(ApexPages.StandardController controller) {
        
        //Get order id from url
        orderId = ApexPages.currentPage().getParameters().get('id');
    }
    
    //Method Calling
    public void inIT() {
        
        //Query to get order record
        List<Order> orderList = [Select Send_Initial_Order_to_QDX__c, Send_Order_to_Lab__c From Order Where Id=: orderId];
        
        //This method for hitting the webservice for button - Send Initial Order to QDX
        //Check for flag value
        if(orderList[0].Send_Initial_Order_to_QDX__c) {
            
            //String to hold page name
            String pName = 'OSM_GenerateOrderXML';
            
            //Call webservice method
            String str = OSM_Messaging.MakeOrderXML(orderId, pName, '');
            
            //Set flag to false
            orderList[0].Send_Initial_Order_to_QDX__c = false;
            
            //Update order
            update orderList;
        }
        //This method for hitting the webservice for button - Send Order to Lab
        else if(orderList[0].Send_Order_to_Lab__c) {
            
            //String to hold page name
            String pName = 'OSM_OrderToLab';
            
            //Call webservice method
            String str = OSM_Messaging.MakeOrderXML(orderId, pName, '');
            
            //Set flag to false
            orderList[0].Send_Order_to_Lab__c = false;
            
            //Update order
            update orderList;
        }
    }
}