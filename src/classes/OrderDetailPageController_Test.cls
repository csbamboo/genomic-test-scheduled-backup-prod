/*
 *  @author         Raus Kenneth Ablaza
 *  @description    Test class for OrderDetailPageController
 *  @date           30 July 2015
 *
 */

@isTest(seeAllData = true)
public class OrderDetailPageController_Test {
  
    public static testMethod void testOrderDetailPage(){
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        update triggerSwitch;

		List<Account> accList = new List<Account>();
		List<Contact> conList = new List<Contact>();
		List<Order> ordList = new List<Order>();
		List<Product2> prodList = new List<Product2>();
		List<PricebookEntry> pbeList = new List<PricebookEntry>();
		List<OrderItem> oliList = new List<OrderItem>();
		List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
		PriceBook2 pb2Standard = [select Id from PriceBook2 where isStandard=true];
		System.debug('******************************pb2standard value: ' + pb2Standard);

		for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;

        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        for(Integer i = 0; i < 2; i++){
        	prodList.add(OSM_DataFactory.createProduct('IBC', true));
        }
        insert prodList;
        
        for(Integer i = 0; i < 2; i++){
        	pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, pb2Standard.Id, 3));
        }
        insert pbeList;

        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
           		OSM_Patient__c = conList[i].Id, 
           		AccountId = accList[i].Id ,
           		OSM_Status__c = 'Order Intake', 
           		OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
           		Pricebook2Id = pb2Standard.Id, 
           		EffectiveDate = System.Today(), 
           		OSM_Triage_Outcome__c = 'New',
           		Status = 'New'
           	));        
        }
        
        ordList[0].Send_Initial_Order_to_QDX__c = true;
        ordList[1].Send_Order_to_Lab__c = true;
        insert ordList;
        
        PageReference pageRef = Page.OrderDetailPage;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc;
        OrderDetailPageController cont;     
                
        Test.startTest();
        
        sc = new ApexPages.Standardcontroller(ordList[0]);
        ApexPages.currentPage().getParameters().put('Id',ordList[0].id);
        cont = new OrderDetailPageController(sc);   
        cont.inIT();
        
        sc = new ApexPages.Standardcontroller(ordList[1]);
        ApexPages.currentPage().getParameters().put('Id',ordList[1].id);
        cont = new OrderDetailPageController(sc);   
        cont.inIT();
                   
        Test.stopTest();
    
    }
}