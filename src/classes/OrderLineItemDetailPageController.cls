/**
 *  Purpose         :   Controller class for order line item detail page for webservice callout on page load
 *
 *  Created By      :   
 * 
 *  Created Date    :   05/01/2015
 * 
 *  Revision Logs   :   V_1.0 - Created
 * 
 **/
public with sharing class OrderLineItemDetailPageController {

    //String to hold order line item id
    public String orderLineItemId;
    
    //Standard Controller
    public OrderLineItemDetailPageController(ApexPages.StandardController controller) {
        
        //Get order line item id from url
        orderLineItemId = ApexPages.currentPage().getParameters().get('id');
    }
    
    //Method Calling
    public void inIT() {
        //Query to get order line item record
        List<OrderItem> odrItem = [Select Send_Order_Failure_to_QDX__c, Send_Order_Cancel_to_QDX__c, Send_Order_Update_to_QDX__c, OSM_Pre_Billing_Sent_Date_Current__c, Send_Order_To_Lab__c,
                                    Render_and_Distribute_Data_Message__c , OrderId, Send_Initial_Order_to_QDX__c, OSM_Is_Billing__c, OSM_Billing_Status__c, OSM_Billing_Void_Sent_Date_Current__c
                                    From OrderItem Where Id =: orderLineItemId];
    
        //This method hitting webservice callout for button - Send Order Failure to QDX
        //Check if flag is true
        if(odrItem[0].Send_Order_Failure_to_QDX__c)  {
            
            //String to hold page name
            String pName = 'OSM_OrderFailureToQDX';
            
            //Call webservice method
            String responseString = OSM_Messaging.MakeOrderXML(orderLineItemId, pName, '');
            
            System.debug('@@@@@@@@@@@@'+responseString);
            
            //String to hold action type
            String actionType = 'Failure';
            
            //Call webservice callout
            OSM_Messaging.MakeIntegrationTask(responseString, orderLineItemId, actionType, '');
            
            //Set flag to false
            odrItem[0].Send_Order_Failure_to_QDX__c = false;
            
            //Update record
            update odrItem;
        }
        //This method hitting webservice callout for button - Send Order Update to QDX
        //Check if flag is true
        if(odrItem[0].Send_Order_Update_to_QDX__c)  {
            
            //String to hold page name
            String pName = 'OSM_OrderUpdateToQDX';
            
            //Call webservice method
            String responseString = OSM_Messaging.MakeOrderXML(orderLineItemId, pName, '');
            
            //String to hold action type
            String actionType = 'Update';
            
            //Call webservice callout
            OSM_Messaging.MakeIntegrationTask(responseString, orderLineItemId, actionType, '');
            
            //Set flag to false
            odrItem[0].Send_Order_Update_to_QDX__c = false;
            
            //Update record
            update odrItem;
        }
        //This method hitting webservice callout for button - Send Order Cancel to QDX
        //Check if flag is true
        if(odrItem[0].Send_Order_Cancel_to_QDX__c)  {
            
            //String to hold page name
            String pName = 'OSM_CancelledOrderToQDX';
            
            //Call webservice method
            String responseString = OSM_Messaging.MakeOrderXML(orderLineItemId, pName, '');
            
            System.debug('@@@@@@@@@@@@'+responseString);
            
            //String to hold action type
            String actionType = 'Cancelled';
            
            //Call webservice callout
            OSM_Messaging.MakeIntegrationTask(responseString, orderLineItemId, actionType, '');
            
            //Set flag to false
            odrItem[0].Send_Order_Cancel_to_QDX__c = false;
            
            //Update record
            update odrItem;
        }
        
        if(odrItem[0].Render_and_Distribute_Data_Message__c) {
            
            //Call webservice method
            String responseString = OSM_Messaging.sendRenderAndDistributeDataMessage(odrItem[0].OrderId, orderLineItemId);
                        
            //Set flag to false
            odrItem[0].Render_and_Distribute_Data_Message__c = false;
            
            //Update record
            update odrItem;
        }
        
        if(odrItem[0].Send_Initial_Order_to_QDX__c) {
            
            //String to hold page name
            String pName = 'OSM_SendOrderToQDX';
            
            if(odrItem[0].OSM_Billing_Status__c == 'Pre-Billing Investigating'){
                List<Case> caseList = [SELECT Id, Status, OSM_Primary_Order_Line_Item__c, OSM_BI_Sent_Date__c, RecordType.Name FROM Case WHERE OSM_Primary_Order_Line_Item__c =: odrItem[0].Id AND RecordType.Name = 'Pre-Billing' AND Status = 'Open'];
                if(!caseList.isEmpty()){
                    String responseString = OSM_Messaging.MakeOrderXML(orderLineItemId, pName, '');
                    odrItem[0].OSM_Pre_Billing_Sent_Date_Current__c = system.now();
                    caseList[0].OSM_BI_Sent_Date__c = system.today();
                    update caseList;
                }
            }
            
            if(odrItem[0].OSM_Billing_Status__c == 'Billing Initiated'){
                List<Case> caseList = [SELECT Id, Status, OSM_Primary_Order_Line_Item__c, OSM_Billing_Request_Sent_Date__c, RecordType.Name FROM Case WHERE OSM_Primary_Order_Line_Item__c =: odrItem[0].Id AND RecordType.Name = 'Billing' AND Status = 'Open'];
                if(!caseList.isEmpty()){
                    String responseString = OSM_Messaging.MakeOrderXML(orderLineItemId, pName, '');
                    odrItem[0].OSM_Is_Billing__c = true;
                    caseList[0].OSM_Billing_Request_Sent_Date__c = system.now();
                    update caseList;
                }
            }
            
            if(odrItem[0].OSM_Billing_Status__c == 'Bill Voided'){
                List<Case> caseList = [SELECT Id, Status, OSM_Primary_Order_Line_Item__c, OSM_Billing_Request_Sent_Date__c, RecordType.Name FROM Case WHERE OSM_Primary_Order_Line_Item__c =: odrItem[0].Id AND RecordType.Name = 'Billing' AND Status = 'Closed' Order By LastModifiedDate DESC];
                if(!caseList.isEmpty()){
                    String responseString = OSM_Messaging.MakeOrderXML(orderLineItemId, pName, '');
                    odrItem[0].OSM_Is_Billing__c = false;
                    odrItem[0].OSM_Billing_Void_Sent_Date_Current__c = system.now();
                    caseList[0].OSM_Billing_Void_Sent_Date__c = system.now();
                    update caseList;
                }
                
                
            }
            
            if(odrItem[0].OSM_Billing_Status__c == 'Billing Not Required'){
                pName = 'OSM_CancelledOrderToQDX';
                String responseString = OSM_Messaging.MakeOrderXML(orderLineItemId, pName, '');
            }
            
            odrItem[0].Send_Initial_Order_to_QDX__c = false;
            update odrItem;
        }
        
        if(odrItem[0].Send_Order_To_Lab__c){
            //String to hold page name
            String pName = 'OSM_OrderToLab';
            
            //Call webservice method
            String responseString = OSM_Messaging.MakeOrderXML(odrItem[0].OrderId, pName, '');
            
            System.debug('@@@@@@@@@@@@'+responseString);
            
            //Set flag to false
            odrItem[0].Send_Order_To_Lab__c = false; 
            
            //Update record
            update odrItem;
        }
    }
}