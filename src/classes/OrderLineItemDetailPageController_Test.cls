/*
     *  @author          Raus Kenneth Ablaza
     *  @description     Test class for OrderLineItemDetailPageController
     *  @date            30 July 2015
     *
     */

@isTest
private class OrderLineItemDetailPageController_Test {

    private static testMethod void testOrderLineItemDetailPageController1() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id CasePreBillRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();
        Id CaseBillRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing').getRecordTypeId();

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;

        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<Case> caseList = new List<Case>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        // PriceBook2 pb2Standard = [select Id from PriceBook2 where isStandard=true];
        Id pb2Standard = Test.getStandardPricebookId();
        System.debug('******************************pb2standard value: ' + pb2Standard);

        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;

        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        for(Integer i = 0; i < 2; i++){
            prodList.add(OSM_DataFactory.createProduct('IBC', true));
        }
        insert prodList;
        
        for(Integer i = 0; i < 2; i++){
            pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, pb2Standard, 3));
        }
        insert pbeList;

        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
                Pricebook2Id = pb2Standard, 
                EffectiveDate = System.Today(), 
                OSM_Triage_Outcome__c = 'New',
                Status = 'New'
            ));        
        }
        insert ordList;
        
        for(Integer i = 0; i < 6; i++){
            oliList.add(new OrderItem(
                OrderId = (math.mod(i, 2) == 0 ? ordList[0].Id : ordList[1].Id), 
                PricebookEntryId = (math.mod(i, 2) == 0 ? pbeList[0].Id : pbeList[1].Id), 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        oliList[0].Send_Order_Failure_to_QDX__c = true;
        oliList[1].Send_Order_Update_to_QDX__c = true;
        oliList[2].Send_Order_Cancel_to_QDX__c = true;
        oliList[3].Render_and_Distribute_Data_Message__c = true;
        oliList[4].Send_Initial_Order_to_QDX__c = true;
        oliList[5].Send_Order_To_Lab__c = true;
        insert oliList;

        caseList.add(new Case(
            OSM_Primary_Order_Line_Item__c = oliList[4].Id, 
            OSM_BI_Sent_Date__c = System.Today(), 
            RecordTypeId = CasePreBillRecType, 
            Status = 'Open'
        ));
        caseList.add(new Case(
            OSM_Primary_Order_Line_Item__c = oliList[4].Id, 
            OSM_Billing_Request_Sent_Date__c = System.Today(), 
            RecordTypeId = CaseBillRecType, 
            Status = 'Open'
        ));
        caseList.add(new Case(
            OSM_Primary_Order_Line_Item__c = oliList[4].Id, 
            OSM_Billing_Request_Sent_Date__c = System.Today(), 
            RecordTypeId = CaseBillRecType, 
            Status = 'Closed'
        ));
        caseList.add(new Case(
            OSM_Primary_Order_Line_Item__c = oliList[4].Id, 
            OSM_BI_Sent_Date__c = System.Today(), 
            RecordTypeId = CaseBillRecType, 
            Status = 'Open'
        ));
        
        insert caseList;
        
        PageReference pageRef = Page.OrderDetailPage;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc;
        OrderLineItemDetailPageController cont;     
                
        Test.startTest();
        
        for(Integer i = 0; i < 6; i++){
            sc = new ApexPages.Standardcontroller(oliList[i]);
            ApexPages.currentPage().getParameters().put('Id',oliList[i].Id);
            cont = new OrderLineItemDetailPageController(sc);   
            cont.inIT();
        }
        
        Test.stopTest();
    }
    
    private static testMethod void testOrderLineItemDetailPageController2() {
        Id PatientRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Id HCORecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Id CasePreBillRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pre-Billing').getRecordTypeId();
        Id CaseBillRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing').getRecordTypeId();

        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = false;
        triggerSwitch.Work_Order_Line_ItemTrigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;

        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<Order> ordList = new List<Order>();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OrderItem> oliList = new List<OrderItem>();
        List<Case> caseList = new List<Case>();
        List<OSM_Work_Order__c> woList = new List<OSM_Work_Order__c>();
        // PriceBook2 pb2Standard = [select Id from PriceBook2 where isStandard=true];
        Id pb2Standard = Test.getStandardPricebookId();
        System.debug('******************************pb2standard value: ' + pb2Standard);

        for(Integer i = 0; i < 2; i++){
            accList.add(OSM_DataFactory.createAccountWithBillingAddress(1, 'Canada', 'Street 1', 'City 1', 'Northwest Territories', '123', HCORecType));
        }
        insert accList;

        for(Integer i = 0; i < 2; i++){
            conList.add(OSM_DataFactory.createContactWithMailingAddress(i, 'United States', 'Street 1', 'Los Angeles', 'California', '3500', PatientRecType));
        }
        insert conList;

        for(Integer i = 0; i < 2; i++){
            prodList.add(OSM_DataFactory.createProduct('IBC', true));
        }
        insert prodList;
        
        for(Integer i = 0; i < 2; i++){
            pbeList.add(OSM_DataFactory.createPricebookEntry(prodList[i].Id, pb2Standard, 3));
        }
        insert pbeList;

        for(Integer i = 0; i < 2; i++){
            ordList.add(new Order(
                OSM_Patient__c = conList[i].Id, 
                AccountId = accList[i].Id ,
                OSM_Status__c = 'Order Intake', 
                OSM_Product__c = (i == 0 ? 'MMR and Colon' : 'DCIS'), 
                Pricebook2Id = pb2Standard, 
                EffectiveDate = System.Today(), 
                OSM_Triage_Outcome__c = 'New',
                Status = 'New'
            ));        
        }
        insert ordList;
        
        for(Integer i = 0; i < 6; i++){
            oliList.add(new OrderItem(
                OrderId = (math.mod(i, 2) == 0 ? ordList[0].Id : ordList[1].Id), 
                PricebookEntryId = (math.mod(i, 2) == 0 ? pbeList[0].Id : pbeList[1].Id), 
                OSM_Lab_and_Report_Status__c = 'Pre-Processing', 
                UnitPrice = 2 , 
                Quantity = 1
            ));
        }
        oliList[0].Send_Initial_Order_to_QDX__c = true;
        oliList[1].Send_Initial_Order_to_QDX__c = true;
        oliList[2].Send_Initial_Order_to_QDX__c = true;
        oliList[3].Send_Initial_Order_to_QDX__c = true;
        
        oliList[0].OSM_Billing_Status__c = 'Pre-Billing Investigating';
        oliList[1].OSM_Billing_Status__c = 'Billing Initiated';
        oliList[2].OSM_Billing_Status__c = 'Bill Voided';
        oliList[2].OSM_Is_Billing__c = True;
        oliList[2].OSM_Billing_Void_Sent_Date_Current__c = System.Today();
        oliList[3].OSM_Billing_Status__c = 'Billing Not Required';
        
        insert oliList;

        caseList.add(new Case(
            OSM_Primary_Order_Line_Item__c = oliList[0].Id, 
            OSM_BI_Sent_Date__c = System.Today(), 
            RecordTypeId = CasePreBillRecType, 
            Status = 'Open'
        ));
        caseList.add(new Case(
            OSM_Primary_Order_Line_Item__c = oliList[1].Id, 
            OSM_Billing_Request_Sent_Date__c = System.Today(), 
            RecordTypeId = CaseBillRecType, 
            Status = 'Open'
        ));
        caseList.add(new Case(
            OSM_Primary_Order_Line_Item__c = oliList[2].Id, 
            OSM_Billing_Request_Sent_Date__c = System.Today(), 
            RecordTypeId = CaseBillRecType, 
            Status = 'Closed'
        ));
        caseList.add(new Case(
            OSM_Primary_Order_Line_Item__c = oliList[3].Id, 
            OSM_BI_Sent_Date__c = System.Today(), 
            RecordTypeId = CaseBillRecType, 
            Status = 'Open'
        ));
        
        insert caseList;
        
        PageReference pageRef = Page.OrderDetailPage;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc;
        OrderLineItemDetailPageController cont;     
                
        Test.startTest();
        
        for(Integer i = 0; i < 6; i++){
            sc = new ApexPages.Standardcontroller(oliList[i]);
            ApexPages.currentPage().getParameters().put('Id',oliList[i].Id);
            cont = new OrderLineItemDetailPageController(sc);   
            cont.inIT();
        }
        
        Test.stopTest();
    }

}