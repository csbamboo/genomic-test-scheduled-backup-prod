public class ReleaseAgreementHoldController {

    public String agreementId;
    
    public ReleaseAgreementHoldController(){
        
        agreementId = ApexPages.currentPage().getParameters().get('id');    
    }
    
    public PageReference releaseHold(){
        
        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c();
        
        agmt = [Select Id, Apttus__Status__c, Status_Staging_Field__c, Apttus__Status_Category__c, Status_Category_Staging_Field__c
                    FROM Apttus__APTS_Agreement__c WHERE Id =:agreementId];
        agmt.Apttus__Status__c = agmt.Status_Staging_Field__c;
        agmt.Apttus__Status_Category__c = agmt.Status_Category_Staging_Field__c;
        
        update agmt;
        
        PageReference pageRef = new PageReference('/'+agreementId);
        return pageRef;
    }
}