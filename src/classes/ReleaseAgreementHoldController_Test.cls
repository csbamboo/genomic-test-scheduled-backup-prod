/*
  @author: Amanpreet Singh Sidhu 
  @date: 29 June 2015
  @description: Test Class for ReleaseAgreementHoldController apex class
  @history: 29 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest
public class ReleaseAgreementHoldController_Test 
{
  
    static testMethod void apptusAgreement() 
    {
        Apttus__APTS_Agreement__c aggrement = new Apttus__APTS_Agreement__c();
        insert aggrement;
        
        PageReference pageRef = Page.APTS_ReleaseAgreementHold;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ordItem);
        ApexPages.currentPage().getParameters().put('Id',aggrement.id);
        ReleaseAgreementHoldController cont = new ReleaseAgreementHoldController();
        
        Test.startTest();
        cont.releaseHold();
        
        Test.stopTest();
     
    }
    
}