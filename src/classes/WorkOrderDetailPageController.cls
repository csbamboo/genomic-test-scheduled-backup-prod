/**
 *  Purpose         :   Controller class for work oreder detail page for webservice callout on page load
 *
 *  Created By      :   
 * 
 *  Created Date    :   05/21/2015
 * 
 *  Revision Logs   :   V_1.0 - Created
 * 
 **/
public with sharing class WorkOrderDetailPageController {

    //String to hold work order id
    public String workOrderId;
    
    //Standard Controller
    public WorkOrderDetailPageController(ApexPages.StandardController controller) {
        
        //Get work order id from url
        workOrderId = ApexPages.currentPage().getParameters().get('id');
    }
    
    
    //Method Calling
    public void inIT() {
        
        //Query to get order line item record
        List<OSM_Work_Order__c> wOrder = [Select Id, Send_Order_To_Lab__c, OSM_Order__c From OSM_Work_Order__c Where Id =: workOrderId];
        
        //This method hitting webservice callout for button - Send Order to lab
        //Check if flag is true
        if(wOrder[0].Send_Order_To_Lab__c)  {
            
            //String to hold page name
            String pName = 'OSM_OrderDataMessage';
            
            //Call webservice method
            String responseString = OSM_Messaging.MakeOrderXML(wOrder[0].OSM_Order__c, pName, '');
            
            System.debug('@@@@@@@@@@@@'+responseString);
            
            //Set flag to false
            wOrder[0].Send_Order_To_Lab__c = false; 
            
            //Update record
            update wOrder;
        }
    }
}