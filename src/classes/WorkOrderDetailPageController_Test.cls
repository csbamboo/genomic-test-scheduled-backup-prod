/*
  @author: Amanpreet Singh Sidhu 
  @date: 29 June 2015
  @description: Test Class for WorkOrderDetailPageController apex class
  @history: 29 June 2015 - Created (Amanpreet S Sidhu)

*/
@isTest
public class WorkOrderDetailPageController_Test 
{
  
    static testMethod void workOrderItem() 
    {
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        triggerSwitch.Account_Trigger__c = false;
        triggerSwitch.Contact_Trigger__c = false;
        triggerSwitch.Order_Line_ItemTrigger__c = false;
        triggerSwitch.Order_Trigger__c = false;
        triggerSwitch.Validation_Builder__c = true;
        triggerSwitch.Work_Order_Trigger__c = true;
        triggerSwitch.Product2__c = false;
        insert triggerSwitch;
        
        Id rtContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        //Id caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id pricebookStandard = Test.getStandardPricebookId();
        
        Set<Id> setOrder = new Set<Id>();
        //List<Case> lstCase = new List<Case>();
        List<OrderItem> lstOrderItem = new List<OrderItem>();
        
        Account account = OSM_DataFactory.createAccount('New Account');
        insert account;

        Contact contact = OSM_DataFactory.createContact('David', 'Catindoy', rtContact);
        contact.AccountId = account.Id;
        insert contact;

        Order order1 = OSM_DataFactory.createOrder('New Order', contact.Id, account.Id, System.Today(), 'Active');
        order1.PriceBook2Id = pricebookStandard;
        insert order1;
        
        setOrder.add(order1.Id);
                       
        OSM_Work_Order__c workOrder = OSM_DataFactory.createWorkOrder(order1.id);
        workOrder.Send_Order_To_Lab__c = True;
        insert workOrder;
        
        Product2 prod2 = OSM_DataFactory.createProduct('IBC', true);
        insert prod2;
        
        PricebookEntry pbe = OSM_DataFactory.createPricebookEntry(prod2.Id, pricebookStandard, 3);
        insert pbe;
        
        OrderItem ordItem = new OrderItem(OrderId = order1.Id, OSM_Work_Order__c = workOrder.Id, PricebookEntryId = pbe.Id , 
                                         UnitPrice = 2 , Quantity = 1, OSM_Lab_and_Report_Status__c = 'Pre-Processing', OSM_Order_Line_Item_Status__c = 'Processing');                
        insert ordItem ;
        
        PageReference pageRef = Page.WorkOrderDeatailPage;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(workOrder);
        ApexPages.currentPage().getParameters().put('Id',workOrder.id);
        WorkOrderDetailPageController cont = new WorkOrderDetailPageController(sc);     
                
        Test.startTest();
        
        cont.inIT();
                   
        Test.stopTest();
    
    }
}