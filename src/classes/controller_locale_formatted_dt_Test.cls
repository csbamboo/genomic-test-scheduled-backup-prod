/**
 *Author: David E. Catindoy
 *Date created: January 30, 2015
 *Description: controller_locale_formatted_dt_Test
 */
@isTest
private class controller_locale_formatted_dt_Test {
	
	static testmethod void controllerTestMethod(){
		controller_locale_formatted_datetime controller = new controller_locale_formatted_datetime();
		controller.date_time = DateTime.now();
		DateTime dt = System.now();
		String dTime = dt.format();
		Test.startTest();
        String timeZone = controller.getTimeZoneValue();
        Test.stopTest();
        System.assert(dTime==timeZone);
    }
}