<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Represents a billing plan associated with a product or service</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Apttus_Config2__BillingMethod__c</fullName>
        <deprecated>false</deprecated>
        <description>The billing method</description>
        <externalId>false</externalId>
        <inlineHelpText>The billing method</inlineHelpText>
        <label>Billing Method</label>
        <picklist>
            <picklistValues>
                <fullName>Percentage</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Amount</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__Description__c</fullName>
        <deprecated>false</deprecated>
        <description>The plan description</description>
        <externalId>false</externalId>
        <inlineHelpText>The plan description</inlineHelpText>
        <label>Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__NumOfInstallments__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of installments</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of installments</inlineHelpText>
        <label>Numner of Installments</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Apttus_Config2__OrderId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>ID of the associated order</description>
        <externalId>false</externalId>
        <inlineHelpText>ID of the associated order</inlineHelpText>
        <label>Order</label>
        <referenceTo>Apttus_Config2__Order__c</referenceTo>
        <relationshipLabel>Billing Plans</relationshipLabel>
        <relationshipName>BillingPlans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__PlanType__c</fullName>
        <deprecated>false</deprecated>
        <description>The billing plan type</description>
        <externalId>false</externalId>
        <inlineHelpText>The billing plan type</inlineHelpText>
        <label>Plan Type</label>
        <picklist>
            <picklistValues>
                <fullName>Fixed</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Milestone</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Apttus_QPConfig__ProposalId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>ID of the associated quote/proposal</description>
        <externalId>false</externalId>
        <inlineHelpText>ID of the associated quote/proposal</inlineHelpText>
        <label>Quote/Proposal</label>
        <referenceTo>Apttus_Proposal__Proposal__c</referenceTo>
        <relationshipLabel>Billing Plans</relationshipLabel>
        <relationshipName>BillingPlans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Billing Plan</label>
    <nameField>
        <label>Plan Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Billing Plans</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
