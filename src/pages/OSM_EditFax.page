<!--/******************************************************
 * Description      : Visual Force Page for Editing a Fax
 * @author          : Rulleth Decena
 * @since           : May 12, 2015
 * @History - 12 May 2015 - Rulleth Decena - created
 *            16 May 2015 - Rescian Rey    - Renamed, clean up 
 ******************************************************/-->
<apex:page standardController="Fax__c" extensions="OSM_EditFaxController" showHeader="true" sidebar="true"  tabStyle="Fax__c">
    <apex:sectionHeader title="Fax" subtitle="{!tabTitle}"/>

    <!-- Page Messages -->
    <apex:outputPanel id="pageMessages">
        <apex:pageMessages />
    </apex:outputPanel>
    <br  />
    <!-- END OF Page Messages -->

    <apex:form >
        <!-- Fax Details -->
        <apex:pageBlock mode="Edit">

            <!-- Block buttons -->
            <apex:pageblockButtons location="both">
                <apex:commandButton value="Save" action="{!save}" rerender="hiddenPanel,pageMessages" oncomplete="handleSave();"/>
                <apex:commandButton value="Cancel" onclick="handleCancel();"/>
            </apex:pageblockButtons>
            <!-- END of Block buttons -->

            <!-- Fax Fields -->
            <apex:pageBlockSection title="Fax Information" columns="2">
                <apex:inputField id="faxNumber" value="{!fax.Fax_Number__c}"/>
                <apex:inputField value="{!fax.Status__c}" />
                <apex:inputField id="sendElectronicStorage" value="{!fax.OSM_Send_Electronic_Storage__c}"/> 
                <apex:inputField value="{!fax.Account__c}"/>
                <apex:inputField id="storageFolder" value="{!fax.OSM_Storage_Folder__c}"/>
                <apex:inputField value="{!fax.Case__c}" id="relatedCase">
                    <apex:actionSupport event="focusout" rerender="faxURL" oncomplete="rerenderCaseSection();"/>
                    <apex:actionSupport event="onchange" rerender="faxURL" oncomplete="rerenderCaseSection();"/>
                </apex:inputField>
                <apex:outputText />
                <apex:inputField value="{!fax.Order__c}" id="relatedOrder">
                    <apex:actionSupport event="focusout" rerender="faxURL" oncomplete="rerenderOrderSection();"/>
                    <apex:actionSupport event="onchange" rerender="faxURL" oncomplete="rerenderOrderSection();"/>
                </apex:inputField>
            </apex:pageBlockSection>
            <!-- END of Fax Fields -->
        </apex:pageBlock>
        <!-- END of Fax Details -->

        <!-- Order Attachments -->
        <apex:pageBlock id="orderAttachmentsBlk" mode="Edit" title="Available Order Attachments">
            <apex:pageblockButtons location="top">
                <apex:commandButton value="Attach New File to Order" action="{!trackSelected}" id="attachOrderBtn" disabled="{!ISNULL(fax.Order__c)}" oncomplete="redirectToOrderAtt();"/>
            </apex:pageblockButtons>
            <apex:pageBlockSection columns="1">
                <apex:pageBlockTable value="{!orderAttachments}" var="orderAttachment" title="Order Attachments" rendered="{!hasOrderAttachments}">
                    <apex:column width="20px">
                        <apex:inputCheckbox value="{!orderAttachment.isSelected}" html-data-id="{!orderAttachment.file.Id}"/></apex:column>
                    <apex:column headerValue="Order Attachments">
                        <apex:outputlink value="/{!orderAttachment.file.Id}">{!orderAttachment.file.Name}</apex:outputlink>
                    </apex:column>
                </apex:pageBlockTable>  

                <!-- if there's an order, but no attachments found -->
                <apex:outputText rendered="{!AND(NOT(ISNULL(fax.Order__c)), NOT(hasOrderAttachments))}">No attachment records found for Order</apex:outputText>

                <!-- if there's no order -->
                <apex:outputText rendered="{!ISNULL(fax.Order__c)}">No Order selected. Please select an Order</apex:outputText> 
            </apex:pageBlockSection>
        </apex:pageBlock>
        <!-- END of Order Attachments -->

        <!-- Case Attachments -->
        <apex:pageBlock id="caseAttachmentsBlk" mode="Edit" title="Available Case Attachments">
            <apex:pageblockButtons location="top">
                <apex:commandButton value="Attach New File to Case" action="{!trackSelected}"
                id="attachCaseBtn" disabled="{!ISNULL(fax.Case__c)}" oncomplete="redirectToCaseAtt();"/>
            </apex:pageblockButtons>
            <apex:pageBlockSection columns="1">
                <apex:pageBlockTable value="{!caseAttachments}" var="caseAttachment" title="Case Attachments" rendered="{!hasCaseAttachments}">
                    <apex:column width="20px">
                        <apex:inputCheckbox value="{!caseAttachment.isSelected}" html-data-id="{!caseAttachment.file.Id}" /></apex:column>
                    <apex:column headerValue="Case Attachments">
                        <apex:outputlink value="/{!caseAttachment.file.Id}">{!caseAttachment.file.Name}</apex:outputlink>
                    </apex:column>  
                </apex:pageBlockTable>

                <!-- if there's a case, but no attachments found -->
                <apex:outputText rendered="{!AND(NOT(ISNULL(fax.Case__c)), NOT(hasCaseAttachments))}">No attachment records found for Case</apex:outputText>

                <!-- if there's no case -->
                <apex:outputText rendered="{!ISNULL(fax.Case__c)}">No Case selected. Please select a Case</apex:outputText>
            </apex:pageBlockSection>
        </apex:pageBlock>
        <br />
        <!-- END of Case Attachments -->

        <!-- FAX Urls -->
        <apex:outputPanel id="faxURL">
            <apex:inputHidden id="orderAttachmentURL" value="{!attachToOrderURL}" />
            <apex:inputHidden id="caseAttachmentURL" value="{!attachToCaseURL}" />
        </apex:outputPanel>

        <apex:outputPanel id="hiddenPanel" style="display:none">
            <span id="faxID">{!fax.Id}</span>
            <span id="faxName">{!fax.Name}</span>
        </apex:outputPanel>
        
        <!-- VF-JS functions -->
        <apex:actionFunction name="rerenderOrderAttSection" rerender="orderAttachmentsBlk" immediate="true" oncomplete="addCheckboxListener();"/>
        <apex:actionFunction name="rerenderCaseAttSection" rerender="caseAttachmentsBlk" immediate="true" oncomplete="addCheckboxListener();"/>
        <apex:actionFunction name="trackSelected" rerender="" immediate="true"/>
        <apex:actionFunction name="addToSelectedAttachment" action="{!addToSelectedAttachment}" reRender="" immediate="true">
            <apex:param name="attID" value=""/>
        </apex:actionFunction>
        <apex:actionFunction name="removeToSelectedAttachment" action="{!removeToSelectedAttachment}" reRender="" immediate="true">
            <apex:param name="attID" value=""/>
        </apex:actionFunction>
        <apex:actionFunction name="requeryAttachments" action="{!requeryAttachments}" reRender="orderAttachmentsBlk,caseAttachmentsBlk" oncomplete="addCheckboxListener();" immediate="true"/>
        <apex:actionFunction name="redirectToDetailPage" rerender="hiddenPanel" immediate="true" oncomplete="redirectToDetailPageCallback();"/>
        <!-- END of VF-JS functions -->
    </apex:form>

    <!-- Scripts -->
    <apex:includeScript value="{!URLFOR($Resource.jQuery_UI_1_10_2, 'js/jquery-1.9.1.min.js')}" />
    <apex:includeScript value="/support/console/33.0/integration.js"/>
    <script type="text/javascript">
        var isInConsole = sforce.console.isInConsole();
        var waiting = '<tr>'+
                      '<td class="data2Col  first " colspan="2" style="display: block;">'+
                      '<span id="order-processing-img" style="text-align:center;display: block;">' +
                      '<img alt="Processing..." height="32" src="/img/loading32.gif" title="Processing..." width="32"> Updating...</span>'+
                      '</td>' +
                      '</tr>';

        var ON_BASE_DOMESTIC = "{!domesticFax}";
        var ON_BASE_RWC = "{!rwcFax}";
        var ON_BASE_INT= "{!intFax}";

        // on load
        $(function(){
            // Set tab title and icon
            if(isInConsole){
                sforce.console.setTabTitle('{!tabTitle}');
                sforce.console.setTabIcon("/img/icon/cases16.png", null);
            }else{
                // Do non console stuff here.
            }

            // If send electronic is check, disable fax number input field
            var faxNumberInput = $('input[id$="faxNumber"]');
            var storageFolderSelect = $('select[id$="storageFolder"]');
            if($('input[type="checkbox"][id$="sendElectronicStorage"]').is(":checked")){
               disableField(faxNumberInput);
               enableField(storageFolderSelect);
            }else{
               enableField(faxNumberInput);
               disableField(storageFolderSelect);
            }

            // Deactivate attach button on selection of case
            $('input[id$="relatedCase"]').focus(function(){
                disableButton($('input[id$="attachCaseBtn"]'));
                previous.relatedCaseValue = $('input[id$="relatedCase"]').val();
            });

            // Enable button on focus out when related case is not changed (otherwise, it will reload)
            $('input[id$="relatedCase"]').focusout(function(){
                var currentValue = $('input[id$="relatedCase"]').val();
                if(currentValue != '' && previous.relatedCaseValue == currentValue){
                    enableButton($('input[id$="attachCaseBtn"]'));
                }
            });

            // Deactivate attach button on selection of order
            $('input[id$="relatedOrder"]').focus(function(){
                disableButton($('input[id$="attachOrderBtn"]'));
                previous.relatedOrderValue = $('input[id$="relatedOrder"]').val();
            });

            // Enable button on focus out when related order is not changed (otherwise, it will reload)
            $('input[id$="relatedOrder"]').focusout(function(){
                var currentValue = $('input[id$="relatedOrder"]').val();
                if(currentValue != '' && previous.relatedOrderValue == currentValue){
                    enableButton($('input[id$="attachOrderBtn"]'));
                }
            });

            storageFolderSelect.change(function(){
                if(storageFolderSelect.val() == 'Domestic'){
                    faxNumberInput.val(ON_BASE_DOMESTIC);
                }else if(storageFolderSelect.val() == 'RWC'){
                    faxNumberInput.val(ON_BASE_RWC);
                }else if(storageFolderSelect.val() == 'INT'){
                    faxNumberInput.val(ON_BASE_INT);
                }else{
                    faxNumberInput.val('');
                }
            });

            addCheckboxListener();
        });
        
        // Track selection of attachment
        function addCheckboxListener(){
            $('input[type="checkbox"][name*="orderAttachmentsBlk"]').change(function(){
                var attID = $(this).attr('data-id');
                if($(this).is(":checked")){
                   addToSelectedAttachment(attID);
                }else{
                   removeToSelectedAttachment(attID);
                }
            });

            $('input[type="checkbox"][name*="caseAttachmentsBlk"]').change(function(){
                var attID = $(this).attr('data-id');
                if($(this).is(":checked")){
                   addToSelectedAttachment(attID);
                }else{
                   removeToSelectedAttachment(attID);
                }
            });

            $('input[type="checkbox"][id$="sendElectronicStorage"]').change(function(){
                var inputField= $('input[id$="faxNumber"]');
                var storageFolderSelect = $('select[id$="storageFolder"]');
                inputField.val("");
                if($(this).is(":checked")){
                    disableField(inputField);
                    enableField(storageFolderSelect);
                }else{
                    enableField(inputField);
                    disableField(storageFolderSelect);
                    storageFolderSelect.val(null);
                }
            });
        }

        // Handles redirection to Order Attachment page
        function redirectToOrderAtt(){
            var orderAttachmentPage = $('input[id$="orderAttachmentURL"]').val();
            if(isInConsole){
                srcUp(orderAttachmentPage);
            }else{
                window.location = orderAttachmentPage;
            }
        }

        // Handles redirection to Case Attachment page
        function redirectToCaseAtt(){
            var caseAttachmentPage = $('input[id$="caseAttachmentURL"]').val();
            if(isInConsole){
                srcUp(caseAttachmentPage);
            }else{
                window.location = caseAttachmentPage;
            }
        }

        // Handle save redirection
        function handleSave(){
            var hasError = ($('div.errorM3').length > 0);
            if(!hasError){
                if(sforce.console.isInConsole()){
                    refreshPrimaryTab();
                    redirectToDetailPage();
                }else{
                    var faxID = $('span#faxID').text();
                    window.location = '/' + faxID;
                }
            }else{
                return false;
            }
        }

        // Handle cancel
        function handleCancel(){
            if(sforce.console.isInConsole()){
                closeTab();
            }else{
                window.location = '/' + "{!fax.Case__c}";
            }
        }

        // Redirects to detail page (for newly created fax)
        function redirectToDetailPageCallback(){
            var faxID = $('span#faxID').text();
            var faxName = $('span#faxName').text();

            // Reset tab
            sforce.console.setTabTitle(faxName);
            sforce.console.setTabIcon("/img/icon/cases16.png", null);

            window.location = '/' + faxID + '?isdtp=vw';
        }

        var previous = {
            relatedCaseValue: null,
            relatedOrderValue: null
        };

        function disableButton(button){
            button.attr("disabled", "disabled");
            button.removeClass('btn');
            button.addClass('btnDisabled');
        }

        function enableButton(button){
            button.removeAttr("disabled");
            button.removeClass('btnDisabled');
            button.addClass('btn');
        }

        function disableField(field){
            field.attr("disabled", "disabled");
        }

        function enableField(field){
            field.removeAttr("disabled");
        }

        function rerenderCaseSection(){
            $('div[id$="caseAttachmentsBlk"] div.pbSubsection table.list').hide();
            $('div[id$="caseAttachmentsBlk"] div.pbSubsection > table.detailList:first > tbody').prepend(waiting);
            rerenderCaseAttSection();
        }

        function rerenderOrderSection(){
            $('div[id$="orderAttachmentsBlk"] div.pbSubsection table.list').hide();
            $('div[id$="orderAttachmentsBlk"] div.pbSubsection > table.detailList:first > tbody').prepend(waiting);
            rerenderOrderAttSection();
        }
        var rerenderAttachmentSections = function (result) {
            $('div[id$="caseAttachmentsBlk"] div.pbSubsection table.list').hide();
            $('div[id$="caseAttachmentsBlk"] div.pbSubsection > table.detailList:first > tbody').prepend(waiting);

            $('div[id$="orderAttachmentsBlk"] div.pbSubsection table.list').hide();
            $('div[id$="orderAttachmentsBlk"] div.pbSubsection > table.detailList:first > tbody').prepend(waiting);
            requeryAttachments();
        };
        sforce.console.onFocusedSubtab(rerenderAttachmentSections);

        // closes this subtab. To be called when saving
        function closeTab() {
            //First find the ID of the current tab to close it
            sforce.console.getEnclosingTabId(closeTabCallback);
        }

        // refreshes main tab
        function refreshPrimaryTab(){
            sforce.console.getEnclosingPrimaryTabId(refreshPrimaryTabCallback);
        }

        var closeTabCallback = function(result){
            sforce.console.closeTab(result.id);
        }

        var refreshPrimaryTabCallback = function(result){
            sforce.console.refreshPrimaryTabById(result.id, true);
        }
    </script>
    <!-- END OF Scripts -->
</apex:page>