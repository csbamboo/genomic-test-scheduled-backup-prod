/*************************************************************
@Name: APTS_LineItem_Trigger
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 03/31/2015
@Description: Trigger for copying Agreement record type to Line item object
@UsedBy: Line Item (Apttus_Config2__LineItem__c) object
******************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
trigger APTS_LineItem_Trigger on Apttus_Config2__LineItem__c (before insert) {
        // Before Insert 
        if(Trigger.isInsert && Trigger.isBefore){
             for(Apttus_Config2__LineItem__c lineItem : Trigger.new) {
                lineItem.GHI_CPQ_Record_Type__c = lineItem.GHI_CPQ_Recordtype__c;
            }
        }          
}