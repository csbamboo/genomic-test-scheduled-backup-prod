/*************************************************************
@Name: APTS_RelAgmtTrigger
@Author: KrishnaRajani Yadlapalli,PS - Apttus
@CreateDate: 06/18/2015
@Description: Trigger to deep clone the agreement childs and grand child in amend and renew functionality
@UsedBy: APTS_RelAgmtTrigger
*****************************************************************
@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 

******************************************************************/
trigger APTS_RelAgmtTrigger on Apttus__APTS_Related_Agreement__c (after insert) {
 
        if(Trigger.isInsert && Trigger.isAfter) {
            APTS_RelAgmtTriggerHandler.onAfterInsert(Trigger.new);
        }  
    
}