trigger GHI_CLM_GetSpecimenRateList on Apttus__APTS_Agreement__c (before insert,before update) 
{
    List<GHI_CLM_Specimen_Rate_List__c> SpecimenRatelist = new List<GHI_CLM_Specimen_Rate_List__c>();
    Map<String,GHI_CLM_Specimen_Rate_List__c> SpecimenRateMap = new Map<String,GHI_CLM_Specimen_Rate_List__c> ();   
    set<string> Countrylist = new set<string>();
    
    SpecimenRatelist = [SELECT Id,Name,CurrencyIsoCode,GHI_CLM_Country_Region__c,GHI_CLM_Block_Rate__c,GHI_CLM_Slide_Rate__c,GHI_CLM_TAT__c
                        FROM GHI_CLM_Specimen_Rate_List__c];
    
    for (GHI_CLM_Specimen_Rate_List__c SpecimenRate :SpecimenRatelist)
    {
        SpecimenRateMap.put(SpecimenRate.GHI_CLM_Country_Region__c,SpecimenRate);
    }
    
     for(Apttus__APTS_Agreement__c Agreement : Trigger.new)
    {   
        if (SpecimenRateMap.containskey(Agreement.GHI_CLM_Country_Region__c))
        {
            Agreement.GHI_CLM_Specimen_TAT__c        = SpecimenRateMap.get(Agreement.GHI_CLM_Country_Region__c).GHI_CLM_TAT__c;
            Agreement.GHI_CLM_Slide_Rate__c          = SpecimenRateMap.get(Agreement.GHI_CLM_Country_Region__c).GHI_CLM_Slide_Rate__c;
            Agreement.GHI_CLM_Specimen_Block_Rate__c = SpecimenRateMap.get(Agreement.GHI_CLM_Country_Region__c).GHI_CLM_Block_Rate__c;          
        }
    }

}