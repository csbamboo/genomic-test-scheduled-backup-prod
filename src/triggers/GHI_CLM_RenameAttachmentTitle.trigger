trigger GHI_CLM_RenameAttachmentTitle on Attachment (before insert) {
    Set<Id> agreementIds = new Set<Id>();
    for(Attachment a : trigger.new){
        agreementIds.add(a.ParentId);
    }
    
    Map<Id,Apttus__APTS_Agreement__c> agreementMap = new Map<Id,Apttus__APTS_Agreement__c>([
                                                    Select Id, Name, RecordType.Name from Apttus__APTS_Agreement__c
                                                    where Id in : agreementIds]);
    
    //Set<String> agreementTypes = new Set<String>({});
    
    if(agreementMap.size()>0){
        try{
            for(Attachment a: trigger.new){
                String attName = a.Name;
                if(attName.contains('Contract Summary')){
                    a.Name = 'Contract Summary.docx';
                }
                if(agreementMap != null){
                    String recTypeName = agreementMap.get(a.ParentId).RecordType.Name;
                    if(recTypeName.contains('International Distributor') || recTypeName.contains('International Government') || 
                        recTypeName.contains('International Facility/Hospital') || recTypeName.contains('International Private') || 
                        recTypeName.contains('US Private') || recTypeName.contains('US Facilities/Hospital')
                        || recTypeName.contains('US Government')){
                        a.Name = 'Contract Summary.docx';
                    }
                }
            } 
        } catch(DMLException e){
            System.debug('The following error has occurred: '+e.getmessage());
        }
    }  
}