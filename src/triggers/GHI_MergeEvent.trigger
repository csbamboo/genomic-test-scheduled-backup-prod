trigger GHI_MergeEvent on Apttus__MergeEvent__c (before insert) {

	GHI_MergeEvent_Handler handler = new GHI_MergeEvent_Handler();

	/* Before Insert */
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }

    /* After Insert */
    //else if(Trigger.isInsert && Trigger.isAfter){
        //handler.OnAfterInsert(Trigger.new, Trigger.newMap);
    //    System.debug('******************trigger.new(Apttus__MergeEvent__c): '+Trigger.new);
    //}

    /* Before Update */
    //else if(Trigger.isUpdate && Trigger.isBefore){
        //handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);     
    //}

    // After Update */
    //else if(Trigger.isUpdate && Trigger.isAfter){
         //handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    //}

    /* Before Delete */
    //else if(Trigger.isDelete && Trigger.isBefore){
    //    System.debug('trigger.old for attachments' +trigger.old);
        //handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    //}

    /* After Delete */
    //else if(Trigger.isDelete && Trigger.isAfter){
        //handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
    //}

    /* After Undelete */
    //else if(Trigger.isUnDelete){
        //handler.OnUndelete(Trigger.new);
    //}

}