/*------------------------------------------------------------------------
    Author:        Andrew Castillo
    Company:       Cloud Sherpas
    Description:   Trigger Events for Lead object
                  
    Test Class:
    History
    <Date>      	<Authors Name>     		<Brief Description of Change>
    13.MAR.2015		Andrew Castillo      	Created
--------------------------------------------------------------------------*/

trigger GHI_Portal_LeadTrigger on Lead (after update) 
{
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    //Uncomment if adding INSERT event 
    /**if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }**/
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    //Uncomment if adding DELETE event 
    /**if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
	TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
	if(triggerSwitch.Lead_Trigger__c)
	{
		GHI_Portal_LeadTriggerHandler.onAfterInsert(trigger.new);
	}
}