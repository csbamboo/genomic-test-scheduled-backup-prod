/*------------------------------------------------------------------------
    Author:        Sairah Hadjinoor
    Company:       Cloud Sherpas
    Description:   Trigger Events for User object
                  
    Test Class:
    History
    <Date>          <Authors Name>          <Brief Description of Change>
    10.FEB.2015     Sairah Hadjinoor        Created
--------------------------------------------------------------------------*/


trigger GHI_Portal_UserTrigger on User (after delete, after insert, after undelete, 
                                        after update, before delete, before insert, before update) {

    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    /*LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        if(ls.Debug_Log_Level__c == 'FINE'){
            Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT -  TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
        }else{
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
        }
    }
    if(trigger.isDelete){
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    */
    
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.GHI_Portal_UserTrigger__c){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                //before insert
                GHI_Portal_UserTriggerHandler.onBeforeInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                //before update
                GHI_Portal_UserTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            } 
        }
        if(Trigger.isAfter){
            if(Trigger.isUpdate){
                //after update
                GHI_Portal_UserTriggerHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
            } 
            if(Trigger.isInsert){ 
                //after insert
                GHI_Portal_UserTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
            }       
        }
    }

}