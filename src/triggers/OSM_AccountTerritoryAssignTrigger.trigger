/*
    @author: patrick lorilla
    @date: 2 October 2014
    @description: Account Territory Assignment Trigger
*/
trigger OSM_AccountTerritoryAssignTrigger on OSM_Account_Territory_Assignment__c (after insert, after update, after delete, before delete, before update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    
    if(triggerSwitch.Account_Territory_Assignment_Trigger__c){
    //Check trigger condition and pass values
        if(Trigger.IsAfter){
            if(Trigger.IsInsert){
                OSM_AccountTerritoryAssignTriggerHandler.OnAfterInsert(Trigger.new, Trigger.newMap);
            }   
            if(Trigger.IsUpdate){
                OSM_AccountTerritoryAssignTriggerHandler.OnAfterUpdate(Trigger.new, Trigger.newMap,Trigger.oldMap);
            }
            if(Trigger.IsDelete){
                OSM_AccountTerritoryAssignTriggerHandler.OnAfterDelete(Trigger.old, Trigger.oldMap);
            }
        }
        if(Trigger.isBefore){
            if(Trigger.IsDelete){
                OSM_AccountTerritoryAssignTriggerHandler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
            }
             if(Trigger.isUpdate){
                OSM_AccountTerritoryAssignTriggerHandler.OnBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);    
            }
        }
    }
}