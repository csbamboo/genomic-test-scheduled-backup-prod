/*
    @author: patrick lorilla
    @date: 24 September 2014
    @description: Account Trigger 
*/
trigger OSM_AccountTrigger on Account (after insert, after update, before insert, before update) {

    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    //Uncomment if adding DELETE event 
    /**if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---

System.debug('FlagCheck'+OSM_DefaultPatientAccountforContact.bypassAccountTrigger);
if(OSM_DefaultPatientAccountforContact.bypassAccountTrigger == false)
{
System.debug('InsideAccountTrigger++');
//Custom settings
TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Account_Trigger__c){    
        if(Trigger.isAfter){
            if(Trigger.isUpdate){
                //after update
                OSM_AccountTriggerHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
            } 
            if(Trigger.isInsert){
                //after update
                OSM_AccountTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
            }       
        }
        if(Trigger.isBefore){
            if(Trigger.isUpdate){
                //after update
                OSM_AccountTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            } 
            if(Trigger.isInsert){
                //after update
                OSM_AccountTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
            }       
        }
    }
}
}