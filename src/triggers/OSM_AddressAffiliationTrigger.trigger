trigger OSM_AddressAffiliationTrigger on OSM_Address_Affiliation__c (Before Insert, After Insert, Before Update, After Update, Before Delete, After Delete) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Address_Affiliation_Trigger__c){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                system.debug('test trig before ins enter');
                //before insert
                OSM_AddressAffiliationTriggerHandler.onBeforeInsert(Trigger.new);
            }    
            if(Trigger.isUpdate){
                system.debug('test trig before up enter');
                //before update
                OSM_AddressAffiliationTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            }
            if(Trigger.isDelete){
                OSM_AddressAffiliationTriggerHandler.onBeforeDelete(Trigger.old);
            }
        }
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                system.debug('test trig after ins enter');
                //after insert
                OSM_AddressAffiliationTriggerHandler.onAfterInsert(Trigger.new);
            } 
            if(Trigger.isUpdate){
                system.debug('test trig after up enter');
                OSM_AddressAffiliationTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
            }
            if(Trigger.isDelete){
                OSM_AddressAffiliationTriggerHandler.onAfterDelete(Trigger.old);
            }
        }
    }
}