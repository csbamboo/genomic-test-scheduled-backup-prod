trigger OSM_ApptusAgreementTrigger on Apttus__APTS_Agreement__c (after update, after insert, after delete) {
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Agreement_Trigger__c){
        if(Trigger.isAfter){
            if(Trigger.isUpdate){
                OSM_ApptusAgreementTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
            }
            if(Trigger.isInsert){
                OSM_ApptusAgreementTriggerHandler.onAfterInsert(Trigger.new, Trigger.oldMap);
            }
            if(Trigger.isDelete){
                OSM_ApptusAgreementTriggerHandler.onAfterDelete(Trigger.oldMap);
            }
        }
        if(Trigger.isBefore){
             if(Trigger.isUpdate){
                OSM_ApptusAgreementTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            }
        }
    
    }
    

}