trigger OSM_DelegateTrigger on OSM_Delegate__c (after insert, after update) {

//Custom settings - Adding Trigger Switch Control to Trigger - Bug 70967 - Added By: Amanpreet Sidhu
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Delegate_Trigger__c){
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        if(ls.Debug_Log_Level__c == 'FINE'){
            Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT -  TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
        }else{
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
        }
    }
    //Uncomment if adding DELETE event 
    /**if(trigger.isDelete){
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
    Set<Id> delegateIdSet = new Set<Id>(); 
    Set<Id> sponsorIdSet = new Set<Id>(); 
    List<Contact> contactToUpdateList = new List<Contact>(); 
    
    for(OSM_Delegate__c currDelegate : trigger.new) { 
        //Get set of Delegate Ids
        if(currDelegate.OSM_HCP__c != null) { 
            delegateIdSet.add(currDelegate.OSM_HCP__c); 
        }
        //Get set of Sponsor Ids
        if(currDelegate.OSM_Contact__c != null) { 
            sponsorIdSet.add(currDelegate.OSM_Contact__c); 
        }
    }
    
    //Set Delegate flag to true 
    for(Contact currD : [SELECT Id, GHI_Portal_Delegate__c FROM Contact WHERE Id IN :delegateIdSet]) { 
        currD.GHI_Portal_Delegate__c = true; 
        contactToUpdateList.add(currD); 
    } 
    
    //Set Sponsor flag to true 
    for(Contact currS : [SELECT Id, GHI_Portal_Sponsor__c FROM Contact WHERE Id IN :sponsorIdSet]) { 
        currS.GHI_Portal_Sponsor__c = true; 
        contactToUpdateList.add(currS); 
    }
    
    if(contactToUpdateList.size() > 0) { 
       update contactToUpdateList;
    }
}
}