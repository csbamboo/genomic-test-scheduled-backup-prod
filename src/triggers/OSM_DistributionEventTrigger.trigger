/*
  @author: Karla Reytas/ Roden Songco
  @date: 29 MAY 2015
  @description:Distribution Event Trigger
  @history:29 MAY 2015 - Created (KR/RS)

*/
trigger OSM_DistributionEventTrigger on OSM_Distribution_Event__c (before update, before insert,after insert, after update) {

    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    //Uncomment if adding UPDATE event 
    /**if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }**/
    //Uncomment if adding DELETE event 
    /**
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    
        if(triggerSwitch.Distribution_Event_Trigger__c){
            if(trigger.isBefore){
               /* if(trigger.isInsert){
                    OSM_DistributionEventTriggerHandler.onBeforeInsert(trigger.new);
                }*/
                
               /* if(trigger.isUpdate){
                    OSM_DistributionEventTriggerHandler.onBeforeUpdate(trigger.new);
                }*/
            }
            
            if(trigger.isAfter){
                if(trigger.isInsert){
                    OSM_DistributionEventTriggerHandler.onAfterInsert(trigger.new);
                }
                
                /*if(trigger.isUpdate){ 
                    OSM_DistributionEventTriggerHandler.onAfterUpdate(trigger.new);
                }*/
            }
            
        }

}