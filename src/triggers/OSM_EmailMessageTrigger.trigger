/**
 * File Info
 * ----------------------------------
 * @filename       OSM_EmailMessageTrigger.trigger
 * @created        7.April.2015
 * @author         Paul Angelo
 * @description    Class contains custom application exceptions. 
 * @history        7.April.2015 - Paul Angelo- Created  
 */
trigger OSM_EmailMessageTrigger on EmailMessage (after insert) {
     if(Trigger.isAfter){
            if(Trigger.isInsert){
                OSM_EmailMessageTriggerHandler.onAfterInsert(Trigger.newMap);
            }
     }
}