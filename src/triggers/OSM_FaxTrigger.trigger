/********************************************************************
    @author         : Rescian Rey
    @description    : Fax Trigger
    @history:
        <date>                <author>                <description>
        MAY 29 2015           Rescian Rey              Created trigger.
********************************************************************/
trigger OSM_FaxTrigger on Fax__c (before insert, before delete, before update, after insert, after update) {

    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
    OSM_CentralDispatcher.MainEntry('OSM_FaxTrigger',
        trigger.IsBefore,
        trigger.IsDelete,
        trigger.IsAfter, 
        trigger.IsInsert,
        trigger.IsUpdate,
        trigger.IsExecuting,
        trigger.new,
        trigger.newmap,
        trigger.old,
        trigger.oldmap);
}