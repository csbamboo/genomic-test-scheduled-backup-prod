/*
    @author: patrick lorilla
    @date: 2 October 2014
    @description: HCP Territory Assignment Trigger
*/
trigger OSM_HCPTerritoryAssignmentTrigger on OSM_HCP_Territory_Assignment__c (after insert, after delete, after update, before delete, before update) {

    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
//Check trigger conditions and pass records
if(triggerSwitch.Account_Territory_Assignment_Trigger__c){

    if(Trigger.IsAfter){
        if(Trigger.IsInsert){
            OSM_HCPTerritoryAssignmentTriggerHandler.OnAfterInsert(Trigger.new, Trigger.newMap);
        }
        if(Trigger.IsUpdate){
            OSM_HCPTerritoryAssignmentTriggerHandler.OnAfterUpdate(Trigger.new, Trigger.newMap,Trigger.oldMap);
        }   
        if(Trigger.IsDelete){
            OSM_HCPTerritoryAssignmentTriggerHandler.OnAfterDelete(Trigger.new, Trigger.oldMap);
        } 
    }
    if(Trigger.isBefore){
        if(Trigger.IsDelete){
            OSM_HCPTerritoryAssignmentTriggerHandler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
        }
        if(Trigger.isUpdate){
            OSM_HCPTerritoryAssignmentTriggerHandler.OnBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);    
        }
    }
}
}