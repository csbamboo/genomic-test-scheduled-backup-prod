/*
  @author: Daniel Quismorio
  @date: 03 DEC 2014
  @description:Order Line Item Trigger
  @history:03 DEC 2014 - Created (DQ)
 
*/
trigger OSM_OrderLineItemTrigger on OrderItem (before update, before insert,after insert, after update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        if(ls.Debug_Log_Level__c == 'FINE'){
            Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT -  TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
        }else{
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
            //Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
        }
    }
    //Uncomment if adding DELETE event 
    /**if(trigger.isDelete){
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
        TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
        if(system.isbatch() && OSM_TerritoryAlignmentBatchOrder.batchIsRunnng){
            return;
        }
        //triggerSwitch.Case_Trigger__c = false;
        //update triggerSwitch;
        if(triggerSwitch.Order_Line_ItemTrigger__c){
            if(trigger.isBefore){
                if(trigger.isInsert){
                    
                    System.debug('@@@@@@@@ Queries Before Insert Event - Before ' + Limits.getQueries());
                    
                    OSM_OrderLineItemTriggerHandler.onBeforeInsert(trigger.new);
                    
                    System.debug('@@@@@@@@ Queries Before Insert Event - After ' + Limits.getQueries());
                }
                 
                if(trigger.isUpdate){
                    
                    System.debug('@@@@@@@@ Queries Before Update Event - Before - 0 ' + Limits.getQueries());
                    
                    OSM_OrderLineItemTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
                    
                    System.debug('@@@@@@@@ Queries Before Update Event - After  - 17' + Limits.getQueries());
                }
                
                //Checking for event type
                if(Trigger.isInsert || Trigger.isUpdate) {
                
                    //Calling helper class method
                    OSM_OrderLineItemTriggerHandler.populateFieldsForWebService(trigger.new, trigger.oldMap);
                }
                
                System.debug('@@@@@@@@ Queries Before Event Completed:: ' + Limits.getQueries());
            }
            
            if(trigger.isAfter){
                if(trigger.isInsert){
                    
                    System.debug('@@@@@@@@ Queries After Insert Event Before :: ' + Limits.getQueries());
                    
                    OSM_OrderLineItemTriggerHandler.onAfterInsert(trigger.new);
                    
                    System.debug('@@@@@@@@ Queries After Insert Event After :: ' + Limits.getQueries());
                }
                
                if(trigger.isUpdate){ 
                    
                    System.debug('@@@@@@@@ Queries After Update Event Before  - 17 :: ' + Limits.getQueries());
                    
                    OSM_OrderLineItemTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
                    
                    System.debug('@@@@@@@@ Queries After Update Event After - 24 :: ' + Limits.getQueries());
                }
                
                
                //Check for event type
                if(Trigger.isInsert || Trigger.isUpdate) {
                    
                    //Set<Id> newOliIds = new Set<Id>();
                    //for(OrderItem oli : trigger.new){
                    //    oliIds.add(oli.Id);
                    //}
                    
                    System.debug('@@@@@@@@ Queries Before generateXMLForOrderLineItem()  :: ' + Limits.getQueries());
                     
                    //Call helper class method 
                    //if(!OSM_Utilities.generateXMLForOrderLineItemRunOnce){
                       if(OSM_TerritoryAlignmentBatchOrder.batchIsRunnng) { //patrick l 8.14.2015 added flag when batch is running
                            return;
                        }
                        DataFactoryFutureHelper.generateXMLForOrderLineItem(trigger.new, trigger.oldMap);
                    //}
                   //if(!OSM_Utilities.generateXMLForOrderLineItemRunOnce){
                        
                        
                        
                        
                    //    DataFactoryFutureHelper.generateXMLForOrderLineItem(oliIds);
                    //}
                    
                    //if(!OSM_BatchForOLIXMLProcess.OSM_BatchForOLIXMLProcessIsRunning){
                        //Database.executeBatch(new OSM_BatchForOLIXMLProcess(Trigger.new, Trigger.oldMap), 1);
                    //}
                }
                
                System.debug('@@@@@@@@ Queries After Event Completed :: ' + Limits.getQueries());
                
            }            
        }
       
        //triggerSwitch.Case_Trigger__c = true;
        //update triggerSwitch;
}