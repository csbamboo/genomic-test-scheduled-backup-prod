trigger OSM_OrderRoleTrigger on OSM_Order_Role__c (before insert, before update, after insert, after update, after delete) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Order_Role_Trigger__c ){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                OSM_OrderRoleTriggerHandler.onBeforeInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                OSM_OrderRoleTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            }
        }
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                OSM_OrderRoleTriggerHandler.onAfterInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                OSM_OrderRoleTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
            }
            
            if(Trigger.isDelete){
                OSM_OrderRoleTriggerHandler.onAfterDelete(Trigger.old);
            } 
        }
    }
}