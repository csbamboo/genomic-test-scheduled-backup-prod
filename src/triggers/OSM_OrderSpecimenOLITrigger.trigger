/**
 * File Info
 * ----------------------------------
 * @filename       OSM_OrderSpecimenOLITrigger.trigger
 * @created        10.APR.2015
 * @author         Kristian Vegerano
 * @description    Class for Order Specimen OLI Trigger changes. 
 * @history        10.APR.20154 - Kristian Vegerano - Created  
 */
trigger OSM_OrderSpecimenOLITrigger on OSM_Order_Specimen_OLI__c (Before Insert, Before Update, After Insert, After Update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    //Uncomment if adding DELETE event 
    /**if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    //Check trigger switch and pass 
    if(triggerSwitch.OSM_Order_Specimen_OLI_Trigger__c){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                OSM_OrderSpecimenOLITriggerHandler.onBeforeInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                OSM_OrderSpecimenOLITriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            }
        }  
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                OSM_OrderSpecimenOLITriggerHandler.onAfterInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                OSM_OrderSpecimenOLITriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
            }
        }  
    }
}