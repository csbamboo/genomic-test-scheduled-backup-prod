/*
  @author: Karla Reytas/ Roden Songco
  @date: 20 APR 2015
  @description:Order Specimen Result Trigger 
  @history:20 APR 2015 - Created (KR/RS)
            - Commented out, transfer logic in OSM_ResultTrigger this trigger is obsolete (DQ)

*/
trigger OSM_OrderSpecimenResult_Trigger on OSM_Result__c (after delete) {
    /*
     TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    
        if(triggerSwitch.Result_Trigger__c){
            if(trigger.isBefore){
                if(trigger.isInsert){
                    OSM_OrderSpecimenResultTriggerHandler.onBeforeInsert(trigger.new);
                }
                
               /* if(trigger.isUpdate){
                    OSM_OrderSpecimenResultTriggerHandler.onBeforeUpdate(trigger.new);
                }
            }
            
            if(trigger.isAfter){
                system.debug('test entry trigger after');
                /*if(trigger.isInsert){
                    OSM_OrderSpecimenResultTriggerHandler.onAfterInsert(trigger.new);
                }
                
                if(trigger.isUpdate){ 
                    OSM_OrderSpecimenResultTriggerHandler.onAfterUpdate(trigger.new);
                }
            }
            
        }
*/
}