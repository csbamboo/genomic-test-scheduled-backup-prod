/**
 * File Info
 * ----------------------------------
 * @filename       OSM_SpecimenTrigger.trigger
 * @created        05.FEB.2015
 * @author         Kristian Vegerano
 * @description    Class contains OSM_Order_Specimen__c triggers. 
 * @history        05.FEB.2015 - Kristian Vegerano - Created  
 */
 trigger OSM_OrderSpecimenTrigger on OSM_Order_Specimen__c (Before insert, Before update, After insert, After update) {
     
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    //Uncomment if adding DELETE event 
    /** if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
    //Custom settings
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Specimen_Trigger__c){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                //before update
                OSM_OrderSpecimenTriggerHandler.onBeforeInsert(Trigger.new);
            }  
            if(Trigger.isUpdate){
                //before update
                OSM_OrderSpecimenTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            } 
        }
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                //after update
                OSM_OrderSpecimenTriggerHandler.onAfterInsert(Trigger.new);
            }  
            if(Trigger.isUpdate){
                //after update
                OSM_OrderSpecimenTriggerHandler.onAfterUpdate(Trigger.new,  Trigger.oldMap);
            } 
        }
    }
}