/*
  @author: patrick lorilla
  @date: 14 OCT 2014
  @description: Order Trigger
  @history: 14 OCT 2014 - Created (PL)

*/
trigger OSM_OrderTrigger on Order (before insert, before delete, before update, after insert, after update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        if(ls.Debug_Log_Level__c == 'FINE'){
            Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT -  TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
        }else{
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
        }
    }
    if(trigger.isDelete){
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(system.isbatch() && OSM_TerritoryAlignmentBatchOrder.batchIsRunnng){
        return;
    }
     system.debug('ordtrg test trig start');
    //Check trigger switch and pass 
    if(triggerSwitch.Order_Trigger__c){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                OSM_OrderTriggerHandler.onBeforeInsert(Trigger.new);
            }
            if(Trigger.isDelete){
                OSM_OrderTriggerHandler.onBeforeDelete(Trigger.old);
            }
            if(Trigger.isUpdate){
                OSM_OrderTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            }
            
            //Checking for request type
            //if(Trigger.isInsert || Trigger.isUpdate) {
                //Calling helper class method
            //   OSM_OrderTriggerHandler.sendOrderToLab(Trigger.New, Trigger.oldMap) ;
            //}
        } 
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                OSM_OrderTriggerHandler.onAfterInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                system.debug('test trig ord up');
                OSM_OrderTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
            }
            
            //Check for event type
            //if(Trigger.isInsert || Trigger.isUpdate) {
                 
                //Call helper class method
            //    DataFactoryFutureHelper.generateXMLForOrder(Trigger.new, Trigger.oldMap);
            //}
        }  
       
    }
}