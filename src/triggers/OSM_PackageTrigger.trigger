/**
 * File Info
 * ----------------------------------
 * @filename       OSM_PackageTrigger.trigger
 * @created        23.JAN.2014
 * @author         Kristian Vegerano
 * @description    Class contains OSM_Package__c triggers. 
 * @history        23.JAN.2014 - Kristian Vegerano - Created  
 */
trigger OSM_PackageTrigger on OSM_Package__c (after insert, after update, before insert, before update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    //Uncomment if adding DELETE event 
    /**
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
    //Custom settings
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Package_Trigger__c){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                //before update
                OSM_PackageTriggerHandler.onBeforeInsert(Trigger.new);
            }  
            if(Trigger.isUpdate){
                //before update
                OSM_PackageTriggerHandler.onBeforeUpdate(Trigger.new);
            } 
        }
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                //before update
                OSM_PackageTriggerHandler.onAfterInsert(Trigger.newMap);
            }  
            if(Trigger.isUpdate){
                //before update
                OSM_PackageTriggerHandler.onAfterUpdate(Trigger.newMap);
            } 
        }
    }
}