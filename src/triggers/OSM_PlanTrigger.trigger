/*
  @author: Daniel Quismorio
  @date: 14 NOV 2014
  @description: Plan Trigger
  @history: 14 NOV 2014 - Created (DQ)

*/
trigger OSM_PlanTrigger on OSM_Plan__c (after delete, after insert) {

//Custom settings - Adding Trigger Switch Control to Trigger - Bug 70967 - Added By: Amanpreet Sidhu
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Plan_Trigger__c){
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    //Uncomment if adding UPDATE event 
    /**if(trigger.isUpdate){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        if(ls.Debug_Log_Level__c == 'FINE'){
            Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT -  TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
        }else{
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
        }
    }**/
    if(trigger.isDelete){
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
    if(Trigger.isInsert){
        OSM_PlanTriggerHandler.onAfterInsert(trigger.new);
    }
     
    if(Trigger.isDelete){
        OSM_PlanTriggerHandler.onAfterDelete(trigger.old);
    }
}
}