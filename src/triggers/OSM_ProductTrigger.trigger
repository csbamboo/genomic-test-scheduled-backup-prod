/**
 * File Info
 * ----------------------------------
 * @filename       OSM_ProductTrigger.trigger
 * @created        06.JAN.2015
 * @author         Daniel Quismorio
 * @description    Class contains custom application exceptions. 
 * @history       06.JAN.2015 - aniel Quismorio - Created  
 */
trigger OSM_ProductTrigger on Product2 (after insert, after update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    //Uncomment if adding DELETE event 
    /**if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
	TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
	
	if(triggerSwitch.Product2__c){
		if(Trigger.IsAfter){
            if(Trigger.IsInsert){
                OSM_ProductTriggerHandler.afterInsert(Trigger.new);
            }   
            if(Trigger.IsUpdate){
                OSM_ProductTriggerHandler.afterUpdate(Trigger.new);
            }   
        }
	}
}