trigger OSM_ResultTrigger on OSM_Result__c (before insert,after insert, after update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    //Uncomment if adding DELETE event 
    /**if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    //Check trigger switch and pass 
    if(triggerSwitch.Result_Trigger__c){
        
        if(trigger.isBefore){
            if(trigger.isInsert){
                OSM_ResultTriggerHandler.onBeforeInsert(trigger.new);
            }
        }
         
        
        if(Trigger.isAfter){
            if(Trigger.isUpdate){
                OSM_ResultTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
            }
            if(Trigger.isInsert){
                OSM_ResultTriggerHandler.onAfterInsert(Trigger.new); 
            }
        }  
    }
}