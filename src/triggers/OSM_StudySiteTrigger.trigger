/*
  @author: Daniel Quismorio
  @date: 11 NOV 2014
  @description: Study Site Trigger
  @history: 11 NOV 2014 - Created (DQ)

*/

trigger OSM_StudySiteTrigger on OSM_Study_Site__c (after insert, after update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    //Uncomment if adding DELETE event 
    /**if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }**/
    //---END---
    
TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    //Check trigger switch and pass 
    if(triggerSwitch.Study_Site_Trigger__c){
            if(Trigger.isInsert){ 
                OSM_StudySiteTriggerHandler.onAfterInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                OSM_StudySiteTriggerHandler.onAfterUpdate(Trigger.new);
            }       
    }
}