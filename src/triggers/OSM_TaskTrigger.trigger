/**
 * File Info
 * ----------------------------------
 * @filename       OSM_TaskTrigger.trigger
 * @created        30.March.2015
 * @author         Paul Angelo
 * @description    Class contains custom application exceptions. 
 * @history        30.March.2015 - Paul Angelo - Created 
 *                 04.JUN.2015 - Rescian Rey - Added Before Update
 */

trigger OSM_TaskTrigger on Task (before delete, after insert, before update) {

//Custom settings - Adding Trigger Switch Control to Trigger - Bug 70967 - Added By: Amanpreet Sidhu
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Task_Trigger__c){

    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        if(ls.Debug_Log_Level__c == 'FINE'){
            Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT -  TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
        }else{
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
        }
    }
    if(trigger.isDelete){
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
    if(Trigger.isBefore){
        if(Trigger.isDelete){
            OSM_TaskTriggerHandler.onBeforeDelete(Trigger.oldMap);
        }

        if(Trigger.isUpdate){
            OSM_TaskTriggerHandler.onBeforeUpdate(Trigger.new);
        }
    }
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            OSM_TaskTriggerHandler.onAfterInsert(Trigger.newMap);
        }
        
        // if(Trigger.isUpdate){
        //     OSM_TaskTriggerHandler.onAfterUpdate(Trigger.newMap);
        // }
    }
    
}
}