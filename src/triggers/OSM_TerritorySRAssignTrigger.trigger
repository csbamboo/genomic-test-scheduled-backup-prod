/*
    @author: patrick lorilla
    @date: 7 October 2014
    @description: Territory SR Assign Trigger
*/
trigger OSM_TerritorySRAssignTrigger on OSM_Territory_Sales_Rep_Assignment__c (after insert, after update, before insert, before update, before delete ) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
    TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    if(triggerSwitch.Territory_Sales_Rep_Assignment_Trigger__c){
        //Check trigger condition and pass values
        if(Trigger.IsAfter){
            if(Trigger.IsInsert){
                OSM_TerritorySRAssignTriggerHandler.OnAfterInsert(Trigger.new, Trigger.newMap);
            } 
            if(Trigger.isUpdate){
                OSM_TerritorySRAssignTriggerHandler.OnAfterUpdate(Trigger.newMap, Trigger.oldMap);
            } 
        }  
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                OSM_TerritorySRAssignTriggerHandler.onBeforeInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                OSM_TerritorySRAssignTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            }
            if(Trigger.IsDelete){
                OSM_TerritorySRAssignTriggerHandler.OnBeforeDelete(Trigger.oldMap);
            }
            
            
        }
    }
}