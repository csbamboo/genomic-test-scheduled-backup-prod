/*
  @author: Dnaiel QUismorio
  @date: 27 OCT 2015
  @description: Territory Trigger
  @history: 27 JAN 2015 - Created (DQ)

*/
trigger OSM_TerritoryTrigger on OSM_Territory__c (after insert, after update, before delete, before update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
        String triggerNEW = JSON.serialize(trigger.new).left(100000);
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        if(ls.Debug_Log_Level__c == 'FINE'){
            Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT -  TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
        }else{
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
            Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
        }
    }
    if(trigger.isDelete){
        string triggerOLD = JSON.serialize(trigger.old).left(100000);
        Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
    //Check trigger switch and pass 
    if(triggerSwitch.Territory_Trigger__c){
       
        if(Trigger.isAfter){
           if(Trigger.isUpdate){
                
               OSM_TerritoryTriggerHandler.OnAfterUpdate(Trigger.new, Trigger.oldMap);
           } 
           if(Trigger.isInsert){
               OSM_TerritoryTriggerHandler.OnAfterInsert(Trigger.new, Trigger.oldMap);
           }
        } 
         if(Trigger.isBefore){
           if(Trigger.isUpdate){
               OSM_TerritoryTriggerHandler.OnBeforeUpdate(Trigger.new, Trigger.oldMap);
           } 
           if(Trigger.isDelete){
               OSM_TerritoryTriggerHandler.OnBeforeDelete(Trigger.old);
           }
           
        }
     

    }
}