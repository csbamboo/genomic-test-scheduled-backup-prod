/*
  @author: Daniel Quismorio
  @date: 05 NOV 2014
  @description: Work Order Line Item Trigger
  @history:05 NOV 2014 - Created (DQ)

*/
trigger OSM_WorkOrderLineItemTrigger on OSM_Work_Order_Line_Item__c (after delete, after insert, after update, before insert, before update) {
    
    /*-----------------------------------------------------------------------
    * Author        Paul Wittmeyer
    * History       SEPT-22-2015
    * Description   Generates log records for trigger firings 
    ----------------------------------------------------------------------- */ 
    //---START---
    LoggerSettings__c ls = LoggerSettings__c.getOrgDefaults();
    if(trigger.isInsert){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    }
    if(trigger.isUpdate){
    	String triggerNEW = JSON.serialize(trigger.new).left(100000);
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	if(ls.Debug_Log_Level__c == 'FINE'){
    		Logger.debugForAsync(system.logginglevel.FINE, 'Trigger.new & Trigger.old INSERT - 	TRIGGER.NEW*** ' + triggerNEW + ' - TRIGGER.OLD*** ');
    	}else{
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.new INSERT ' + triggerNEW);
    		Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old INSERT ' + triggerOLD);
    	}
    }
    if(trigger.isDelete){
    	string triggerOLD = JSON.serialize(trigger.old).left(100000);
    	Logger.debugForAsync(system.logginglevel.DEBUG, 'Trigger.old DELETE ' + triggerOLD);
    }
    //---END---
    
	TriggerSwitch__c triggerSwitch = TriggerSwitch__c.getOrgDefaults();
	
	if(triggerSwitch.Work_Order_Line_ItemTrigger__c){
		if(trigger.isAfter){
			if(trigger.isInsert){
				OSM_WorkOrderLineItemTriggerHandler.onAfterInsert(trigger.new);
			}
			
			if(trigger.isUpdate){ 
				OSM_WorkOrderLineItemTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
			}
		}
		if(trigger.isBefore){
			if(trigger.isInsert){
				OSM_WorkOrderLineItemTriggerHandler.onBeforeInsert(trigger.new);
			}
			
			if(trigger.isUpdate){ 
				OSM_WorkOrderLineItemTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
			}
		}
		
	}

}