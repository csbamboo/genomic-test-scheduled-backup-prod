<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IntlSFA_Account_Approval_Request_Approved</fullName>
        <description>Account Approval Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>OSM_Genomic_Health_Templates/IntlSFA_DE_UK_Request_Approved_Template</template>
    </alerts>
    <alerts>
        <fullName>IntlSFA_Account_Approval_Request_Rejected</fullName>
        <description>Account Approval Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>OSM_Genomic_Health_Templates/IntlSFA_DE_UK_Request_Rejected_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_GNAM</fullName>
        <field>OSM_Managed_Care__c</field>
        <literalValue>1</literalValue>
        <name>Check GNAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Oncology</fullName>
        <field>OSM_Oncology__c</field>
        <literalValue>1</literalValue>
        <name>Check Oncology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Urology</fullName>
        <field>OSM_Urology__c</field>
        <literalValue>1</literalValue>
        <name>Check Urology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deselect_GNAM</fullName>
        <field>OSM_Managed_Care__c</field>
        <literalValue>0</literalValue>
        <name>Deselect GNAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deselect_Oncology</fullName>
        <field>OSM_Oncology__c</field>
        <literalValue>0</literalValue>
        <name>Deselect Oncology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deselect_Urology</fullName>
        <field>OSM_Urology__c</field>
        <literalValue>0</literalValue>
        <name>Deselect Urology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deslect_Strategic_Account</fullName>
        <field>OSM_Strategic_Account__c</field>
        <literalValue>0</literalValue>
        <name>Deslect Strategic Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fax_email_Account</fullName>
        <field>Fax_email__c</field>
        <formula>IF( NOT( ISBLANK(Fax) ) , &apos;1&apos;+
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Fax, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;)+
&apos;@faxmail.com&apos;, Fax_email__c)</formula>
        <name>Fax email - Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GHI_Portal_Assign_Account_Source</fullName>
        <field>AccountSource</field>
        <literalValue>Portal</literalValue>
        <name>Assign Account Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GHI_Portal_Reassign_Account_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>system.admin@genomichealth.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Reassign Account Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IntlSFA_Update_Acct_Status_to_Approved</fullName>
        <field>OSM_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Account Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Account_Rep_preference_Field_Update</fullName>
        <field>OSM_Report_Delivery_Language_Preference__c</field>
        <literalValue>German</literalValue>
        <name>Account Report preference Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Account_Report_Field_Update_Eng</fullName>
        <field>OSM_Report_Delivery_Language_Preference__c</field>
        <literalValue>English</literalValue>
        <name>Account Report Field Update Eng</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_GNAM</fullName>
        <field>OSM_Managed_Care__c</field>
        <literalValue>1</literalValue>
        <name>Check GNAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_Oncology</fullName>
        <field>OSM_Oncology__c</field>
        <literalValue>1</literalValue>
        <name>Check Oncology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_Strategic</fullName>
        <field>OSM_Strategic_Account__c</field>
        <literalValue>1</literalValue>
        <name>Check Strategic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_Urology</fullName>
        <field>OSM_Urology__c</field>
        <literalValue>1</literalValue>
        <name>Check Urology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Clear_Account_Type_Field</fullName>
        <field>Type</field>
        <name>Clear Account Type Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Clear_Specialty_Field</fullName>
        <field>OSM_Specialty__c</field>
        <name>Clear Specialty Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Oncology_Checkbox</fullName>
        <field>OSM_Oncology__c</field>
        <literalValue>1</literalValue>
        <name>Update Oncology Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Urology_Checkbox</fullName>
        <field>OSM_Urology__c</field>
        <literalValue>1</literalValue>
        <name>Update Urology Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_Strategic_Account</fullName>
        <field>OSM_Strategic_Account__c</field>
        <literalValue>1</literalValue>
        <name>Select Strategic Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_GNAM</fullName>
        <field>OSM_Managed_Care__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck GNAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GHI Portal Account</fullName>
        <actions>
            <name>GHI_Portal_Assign_Account_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GHI_Portal_Reassign_Account_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.GHI_Portal_Location__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IntlSFA Untick Oncology Checkbox for HCO DE%2FUK</fullName>
        <actions>
            <name>Deselect_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>contains</operation>
            <value>HCO UK,HCO DE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Urology</value>
        </criteriaItems>
        <description>Unticks the Oncology checkbox for HCO DE/UK record types if Specialty values are one of the following below</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IntlSFA Untick Urology Checkbox for HCO DE%2FUK</fullName>
        <actions>
            <name>Deselect_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>contains</operation>
            <value>HCO UK,HCO DE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Oncology</value>
        </criteriaItems>
        <description>Unticks the Urology checkbox for HCO DE/UK record types if certain Specialty values are not met</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IntlSFA Update Oncology Checkbox for HCO DE%2FUK</fullName>
        <actions>
            <name>OSM_Check_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>contains</operation>
            <value>HCO UK,HCO DE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>contains</operation>
            <value>Cancer Center,Hospital,Oncology,Surgery,Pathology Lab,Research,Academic,GI Clinic,Other,Gynaecology</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>contains</operation>
            <value>Haematology and internist Oncology,Intensive Care /Focus Obstetrics and Gynaecology,Obstetrics,Obstetrics / Focus Haematology and internist Oncology,Obstetrics and Gynaecology,Radiology</value>
        </criteriaItems>
        <description>Ticks the Oncology checkbox for HCO DE/UK record types if Specialty values are one of the following below</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IntlSFA Update Urology Checkbox for HCO DE%2FUK</fullName>
        <actions>
            <name>OSM_Check_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>contains</operation>
            <value>HCO UK,HCO DE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>contains</operation>
            <value>Cancer Center,Hospital,Urology,Surgery,Pathology Lab,Research,Academic,GI Clinic,Other,Gynaecology</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>contains</operation>
            <value>Haematology and internist Oncology,Intensive Care /Focus Obstetrics and Gynaecology,Obstetrics,Obstetrics / Focus Haematology and internist Oncology,Obstetrics and Gynaecology,Radiology</value>
        </criteriaItems>
        <description>Ticks the Urology checkbox for HCO DE/UK record types if Specialty values are one of the following below</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Clear HCS%2FHCO Specialty Value</fullName>
        <actions>
            <name>OSM_Clear_Specialty_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Account Record Type =  HCO and Specialty value is &quot;Health Care System&quot; OR
when Account Record Type = HCS and Specialty value is blank or not &quot;Health Care System&quot;, clear the Specialty field.</description>
        <formula>OR( CONTAINS( RecordType.Name , &quot;HCO&quot;) &amp;&amp; ISPICKVAL(OSM_Specialty__c, &quot;Health Care System&quot;),  RecordType.Name = &quot;HCS&quot; &amp;&amp;  NOT(ISPICKVAL(OSM_Specialty__c, &quot;&quot;)) &amp;&amp; NOT(ISPICKVAL(OSM_Specialty__c, &quot;Health Care System&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Clear HCS%2FHCO Type Value</fullName>
        <actions>
            <name>OSM_Clear_Account_Type_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Account Record Type =  HCO and Type value is &quot;Health Care System&quot; OR
when Account Record Type = HCS and Type value is blank or not &quot;Health Care System&quot;, clear the Type field.</description>
        <formula>OR( CONTAINS( RecordType.Name , &quot;HCO&quot;) &amp;&amp; ISPICKVAL(  Type  , &quot;Health Care System&quot;),  RecordType.Name = &quot;HCS&quot; &amp;&amp;  NOT(ISPICKVAL(Type, &quot;&quot;)) &amp;&amp; NOT(ISPICKVAL(Type, &quot;Health Care System&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Clear Oncology%2FUrology when Record Type is HCS</fullName>
        <actions>
            <name>Deselect_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Deselect_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set Fax Email - Account</fullName>
        <actions>
            <name>Fax_email_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 17883 Updates the Fax Email field on Account</description>
        <formula>OR(    IF( ISCHANGED( Fax ) , true, false) ,      NOT(ISBLANK(Fax)),    ISNEW()  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update HCO Record</fullName>
        <actions>
            <name>Deselect_GNAM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Deslect_Strategic_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>contains</operation>
            <value>HCO</value>
        </criteriaItems>
        <description>When Record Type = HCO, Strategic Account checkbox and GNAM checkbox will be unchecked.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update Urology%2FOncology Checkboxes if Partner</fullName>
        <actions>
            <name>OSM_Update_Oncology_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Update_Urology_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>This workflow rule selects these two checkboxes when Record Type equals Partner</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Account_report _language</fullName>
        <actions>
            <name>OSM_Account_Rep_preference_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>Germany</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Report_Language_override__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>&quot;If country = Germany and  &quot;Report Language Override&quot; = FALSE, change Report Delivery Language (OSM_Report_Delivery_Language_Preference__c) value to German&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Account_report_language</fullName>
        <actions>
            <name>OSM_Account_Rep_preference_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>Germany</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Report_Language_override__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>&quot;If country = Germany and  &quot;Report Language Override&quot; = FALSE, change Report Delivery Language (OSM_Report_Delivery_Language_Preference__c) value to German&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Account_report_language_eng</fullName>
        <actions>
            <name>OSM_Account_Report_Field_Update_Eng</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>notEqual</operation>
            <value>Germany</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Report_Language_override__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>&quot;If country &lt;&gt; Germany and  &quot;Report Language Override&quot; = FALSE, change Report Delivery Language (OSM_Report_Delivery_Language_Preference__c) value to English&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Set_GNAM_False</fullName>
        <actions>
            <name>Uncheck_GNAM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Set_GNAM_Strategic_to_True</fullName>
        <actions>
            <name>OSM_Check_GNAM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_Strategic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Payor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Payor_Hierarchy__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>When Record Type = Payor and Payor Hierarchy = 1, set GNAM and Strategic to True</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Speciality_Derives_Market_1</fullName>
        <actions>
            <name>Check_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Check_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Cancer Center,Hospital,Surgery,Pathology Lab,Research,Academic,Other</value>
        </criteriaItems>
        <description>When Speciality = Cancer Center or Hospital or Surgery or Pathology Lab or or research or Academic or Other</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Speciality_Derives_Market_2</fullName>
        <actions>
            <name>Deselect_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Oncology,GI Clinic</value>
        </criteriaItems>
        <description>When Speciality = Oncology or GI Clinic</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Speciality_Derives_Market_3</fullName>
        <actions>
            <name>Deselect_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Urology</value>
        </criteriaItems>
        <description>When Speciality = Urology</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Speciality_Derives_Market_4</fullName>
        <actions>
            <name>Check_GNAM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Deselect_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Deselect_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Deslect_Strategic_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_GNAM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Clear_Specialty_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Payor</value>
        </criteriaItems>
        <description>When Record Type = Payor</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Speciality_Derives_Market_5</fullName>
        <actions>
            <name>OSM_Check_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>When Record Type = Partner</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Speciality_Derives_Market_6</fullName>
        <actions>
            <name>Select_Strategic_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCS</value>
        </criteriaItems>
        <description>When Record Type = HCS</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
