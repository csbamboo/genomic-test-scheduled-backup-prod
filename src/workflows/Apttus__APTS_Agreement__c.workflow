<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>APTS_Change_Status</fullName>
        <field>Apttus__Status__c</field>
        <literalValue>Author Contract</literalValue>
        <name>APTS Change Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>APTS_Change_Status_Category</fullName>
        <description>Change Agreement Status Category to in Authoring</description>
        <field>Apttus__Status_Category__c</field>
        <literalValue>In Authoring</literalValue>
        <name>APTS Change Status Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apttus__SearchFieldUpdate</fullName>
        <description>Update the account search field with Account Name</description>
        <field>Apttus__Account_Search_Field__c</field>
        <formula>Apttus__Account__r.Name  &amp;  Apttus__FF_Agreement_Number__c</formula>
        <name>Search Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apttus__SetAgreementNumber</fullName>
        <description>Set agreement number from the auto generated contract number</description>
        <field>Apttus__Agreement_Number__c</field>
        <formula>Apttus__Contract_Number__c</formula>
        <name>Set Agreement Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apttus__SetClonetriggertofalse</fullName>
        <description>Set Clone trigger to false</description>
        <field>Apttus__Workflow_Trigger_Created_From_Clone__c</field>
        <literalValue>0</literalValue>
        <name>Set Clone trigger to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Review_Comments_Link_Creation</fullName>
        <field>CLM_New_Review_and_Comments_Link__c</field>
        <formula>&apos;https://cs9.salesforce.com/a3g/e?CF00NK0000001bjTT=&apos;+ GHI_CLM_Agreement_Name_without_Spaces__c +&apos;&amp;CF00NK0000001bjTT_lkid=&apos;+Id</formula>
        <name>New Review Comments Link Creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Workflow_Trigger</fullName>
        <description>Reset Workflow Trigger Flag</description>
        <field>Apttus__Workflow_Trigger_Viewed_Final__c</field>
        <literalValue>0</literalValue>
        <name>Reset Workflow Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Category_Change_to_In_Sig</fullName>
        <description>Status category change to In signatures</description>
        <field>Apttus__Status_Category__c</field>
        <literalValue>In Signatures</literalValue>
        <name>Status Category Change to In Signatures</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Change_to_External_Signatures</fullName>
        <description>Status Change to Internal Signatures</description>
        <field>Apttus__Status__c</field>
        <literalValue>External Signatures</literalValue>
        <name>Status Change to External Signatures</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Change_to_Internal_Signatures</fullName>
        <field>Apttus__Status__c</field>
        <literalValue>Internal Signatures</literalValue>
        <name>Status Change to Internal Signatures</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Perpetual_As_Checked</fullName>
        <description>Update Perpetual As Checked when Contract auto renews was checked</description>
        <field>Apttus__Perpetual__c</field>
        <literalValue>1</literalValue>
        <name>Update Perpetual As Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>APTS_Agmnt_Default_Workflow_Rule</fullName>
        <actions>
            <name>APTS_Change_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>APTS_Change_Status_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Workflow_Trigger</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status_Category__c</field>
            <operation>equals</operation>
            <value>Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Workflow_Trigger_Viewed_Final__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Disable default tasks generated by Apttus.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Apttus__Reset Clone Trigger</fullName>
        <actions>
            <name>Apttus__SetClonetriggertofalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Workflow_Trigger_Created_From_Clone__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Reset Clone Trigger</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Apttus__Search Field Update</fullName>
        <actions>
            <name>Apttus__SearchFieldUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate an external Id search field with account name, so that side bar support can work with Account name search</description>
        <formula>or(not (isnull(Apttus__Account__r.Name)) ,not (isnull(Apttus__FF_Agreement_Number__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Apttus__Set Agreement Number</fullName>
        <actions>
            <name>Apttus__SetAgreementNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Agreement_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set agreement number for new agreements. The agreement number is auto generated.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Review Comments Link Creation</fullName>
        <actions>
            <name>New_Review_Comments_Link_Creation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status_Category__c</field>
            <operation>equals</operation>
            <value>Request,In Authoring,In Signatures</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status and Status Category change on click of Internal signatures Button</fullName>
        <actions>
            <name>Status_Category_Change_to_In_Sig</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_Change_to_Internal_Signatures</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Other Party Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status_Category__c</field>
            <operation>equals</operation>
            <value>In Authoring</value>
        </criteriaItems>
        <description>Status changed from Other Party Review to Internal Signatures on button click of Send for internal signatures  Status Category change from In authoring to In signatures</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status change on click of Send for External signatures Button</fullName>
        <actions>
            <name>Status_Change_to_External_Signatures</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Other Party Signatures</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status_Category__c</field>
            <operation>equals</operation>
            <value>In Signatures</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.GHI_CLM_External_signature__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Status changed from Other Party Signatures to External Signatures on button click of Send for External signatures......</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Perpetual based upon Contract Auto Renews Selection</fullName>
        <actions>
            <name>Update_Perpetual_As_Checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Auto_Renewal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
