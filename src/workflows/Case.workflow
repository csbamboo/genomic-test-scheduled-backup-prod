<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_to_Customer_Service_when_Document_is_Uploaded_to_Portal</fullName>
        <ccEmails>OSM_2@genomichealth.com</ccEmails>
        <description>Send Email to Customer Service when Document is Uploaded to Portal</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>aanguyen@genomichealth.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>OSM_Genomic_Health_Templates/A_New_Document_Has_Been_Uploaded_in_Portal</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Priority_Set_to_High</fullName>
        <field>Priority</field>
        <literalValue>3 - High</literalValue>
        <name>Case Priority Set to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Specimen_Request_Date_Time</fullName>
        <description>User Story 42500:Specimen Request Date - GHI to Request</description>
        <field>OSM_Specimen_Request_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Case Specimen Request Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_OSM_WIP_Date_Time</fullName>
        <field>OSM_WIP_ON_Date_Time__c</field>
        <name>Clear OSM WIP Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GHI_Portal_Case_Subject_Update</fullName>
        <description>This gets the In Regards To value entered when Case is created thru the Portal</description>
        <field>Subject</field>
        <formula>IF( TEXT(Origin) = &quot;Portal&quot;, 
    IF ( TEXT(GHI_Portal_In_Regards_To__c) = &quot;Other&quot; , 
         TEXT(GHI_Portal_In_Regards_To__c) + &quot; - &quot; + GHI_Portal_Subject_Other__c,
         TEXT(GHI_Portal_In_Regards_To__c)),
     Subject )</formula>
        <name>GHI Portal Case Subject Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>General_Case_Status_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>General Case Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Auto_False_Auto_Closed_by_Code</fullName>
        <field>OSM_Auto_Closed_by_Code__c</field>
        <literalValue>0</literalValue>
        <name>OSM Auto False Auto Closed by Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Case_Communication_Queued</fullName>
        <description>User Story #34973 Specimen Retrieval Form - Communication Ready - Automated Process</description>
        <field>Status</field>
        <literalValue>Communication Queued</literalValue>
        <name>OSM_Case_Status_Communication_Queued</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Clear_BI_Status</fullName>
        <field>OSM_BI_Status__c</field>
        <name>OSM_Clear_BI_Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Close_Status</fullName>
        <description>Close status when Case Result is selected</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_E2C_Set_Followup_DT</fullName>
        <description>Email to Case - Set Folllowu DT to NOW</description>
        <field>OSM_Follow_Up_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>OSM E2C Set Followup DT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_E2C_Set_Type</fullName>
        <description>Email to Case - Set Type to Customer Inquiry</description>
        <field>Type</field>
        <literalValue>Customer Inquiry</literalValue>
        <name>OSM E2C Set Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Populate_Closed_Reason_with_Received</fullName>
        <description>Populate closed reason field with &quot;Received&quot; value.</description>
        <field>OSM_Case_Results__c</field>
        <literalValue>Received</literalValue>
        <name>OSM_Populate Closed Reason with Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Remove_Addtl_Paperwork</fullName>
        <field>OSM_Additional_Paperwork__c</field>
        <name>Remove Addtl Paperwork</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Remove_PA_Number</fullName>
        <field>OSM_PA_Number__c</field>
        <name>Remove PA Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Remove_PA_Status</fullName>
        <field>OSM_PA_Status__c</field>
        <name>Remove PA Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_Case_Priority_to_Urgent</fullName>
        <description>Bug 64936*:Pre-billing Case Priority Not Being Updated</description>
        <field>Priority</field>
        <literalValue>4 - Urgent</literalValue>
        <name>Set Case Priority to Urgent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_Email_Inquiry_To_True</fullName>
        <field>OSM_New_Email_Inquiry__c</field>
        <literalValue>1</literalValue>
        <name>Set Email Inquiry To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_Owner_to_QDXUpdate_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>QDXUpdate</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner to QDXUpdate Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Specimen_Received_Date_and_Time_upda</fullName>
        <description>Auto populate with date and time when tracking number is populated</description>
        <field>OSM_Specimen_Received_Date_and_Time__c</field>
        <formula>now()</formula>
        <name>OSM Specimen Received Date and Time upda</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_BI_Complete_to_System_Date</fullName>
        <field>OSM_BI_Complete__c</field>
        <formula>NOW()</formula>
        <name>Update BI Complete to System Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_report_hold_to_true</fullName>
        <description>32085 - Set Cases to Lab and Report Hold</description>
        <field>OSM_Report_Hold__c</field>
        <literalValue>1</literalValue>
        <name>report hold to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Primary_Customer_Populated</fullName>
        <field>OSM_Primary_Customer_Populated__c</field>
        <literalValue>1</literalValue>
        <name>Update Primary Customer Populated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WIP_OFF_on_Case_Close_Clear_WIP_Picklist</fullName>
        <field>OSM_WIP_Pick__c</field>
        <literalValue>OFF</literalValue>
        <name>WIP OFF on Case Close Clear WIP Picklist</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WIP_OFF_on_Case_Close_Clear_WIP_User</fullName>
        <field>OSM_WIP_User__c</field>
        <name>WIP OFF on Case Close Clear WIP User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GHI Portal Case Subject</fullName>
        <actions>
            <name>GHI_Portal_Case_Subject_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Portal</value>
        </criteriaItems>
        <description>Standard Subject field gets value of In Regards To and Subject Other when Case is created thru Portal</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GHI Portal Follow Up Date%2FTime</fullName>
        <actions>
            <name>Case_Priority_Set_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_E2C_Set_Followup_DT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>New Portal Documents</value>
        </criteriaItems>
        <description>bug 59988</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM Auto False Auto Closed by Code</fullName>
        <actions>
            <name>OSM_Auto_False_Auto_Closed_by_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Turns OSM_Auto_Closed_by_Code__c field to false</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Close General Case</fullName>
        <actions>
            <name>General_Case_Status_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General</value>
        </criteriaItems>
        <description>Bug 63648 Upon Create of a Case
If[Case].[RecordType] Equals &apos;General&apos;
Set [Case].[Status]=&apos;Closed&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM Create Task when BI Status is Cancel%2FIncomplete</fullName>
        <actions>
            <name>OSM_E2C_Set_Followup_DT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Set_Case_Priority_to_Urgent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Canceled_Incomplete_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Creates task when BI Status is Canceled or Incomplete</description>
        <formula>AND(RecordType.Name = &quot;Pre-Billing&quot;, CONTAINS(TEXT(OSM_BI_Status__c), &quot;CANCEL&quot;) || CONTAINS(TEXT(OSM_BI_Status__c), &quot;INCOMPLETE&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Create Task when Data Transmission Error</fullName>
        <actions>
            <name>Data_Transmission_Error</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OSM_Data_Transmission_Status__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <description>Creates a task when Data Transmission Status = Error</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Create Task when PA Status is Incomplete%3A MD Non Compliant</fullName>
        <actions>
            <name>PA_INCOMPLETE_MD_Non_Compliant</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OSM_PA_Status__c</field>
            <operation>equals</operation>
            <value>INCOMPLETE: MD Non Compliant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pre-Billing</value>
        </criteriaItems>
        <description>Creates a task when PA Status is &quot;INCOMPLETE: MD Non Compliant&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Create Task when PA Status is Pending</fullName>
        <actions>
            <name>PA_Pending</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OSM_PA_Status__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pre-Billing</value>
        </criteriaItems>
        <description>Creates a task when PA Status is &quot;Pending&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Email to Case Defaulted Fields</fullName>
        <actions>
            <name>OSM_E2C_Set_Followup_DT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_E2C_Set_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>International,Customer Service,RUL Support,ROL Support,CE Team,MSteam,Kundendienst,European Support</value>
        </criteriaItems>
        <description>US 20232 - The case that is automatically created by email to case will be default the following fields:  
Type = Customer Inquiry
Follow-Up Date/Time = defaulted to the same day/time the email was received</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM Populate Closed Reason</fullName>
        <actions>
            <name>OSM_Populate_Closed_Reason_with_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Specimen Retrieval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OSM_Case_Results__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Populate Closed Reason field for Specimen Retrieval record type with a certain value if this field is blank/null.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Remove PA Status%2FNumber%2FAddtlPaperwork</fullName>
        <actions>
            <name>OSM_Remove_Addtl_Paperwork</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Remove_PA_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Remove_PA_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Removes the values of the following fields when Payor is modified.</description>
        <formula>RecordType.Name = &quot;Pre-Billing&quot; &amp;&amp; ISCHANGED( OSM_Payor__c ) &amp;&amp; PRIORVALUE(OSM_Payor__c)!=&quot;&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Specimen Received Date and Time</fullName>
        <actions>
            <name>OSM_Specimen_Received_Date_and_Time_upda</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OSM_Tracking_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Auto populate Specimen Received Date and Time once tracking number is populated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update Billing Case Status to Closed</fullName>
        <actions>
            <name>OSM_Close_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 43136:PB - Close Billing Case</description>
        <formula>AND(      RecordType.DeveloperName = &apos;OSM_Billing&apos;, ISBLANK( PRIORVALUE( OSM_Billing_Request_Sent_Date__c ) ) ,  NOT( ISBLANK(OSM_Billing_Request_Sent_Date__c ) )       )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update Case Status to Closed</fullName>
        <actions>
            <name>OSM_Close_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>The following criteria will close a case</description>
        <formula>OR(  AND( OSM_OLI_Bill_Type__c == &quot;Private Insurance&quot;,  OR( OSM_Order_Location__c == &quot;Domestic&quot;,  OSM_Order_Location__c == &quot;International&quot;),  CONTAINS(TEXT(OSM_BI_Status__c), &quot;PROCEED&quot;))  ||  AND(  OR(  ISPICKVAL( OSM_PA_Status__c , &quot;Cancelled&quot;),  ISPICKVAL( OSM_PA_Status__c , &quot;Complete&quot;)  ),  CONTAINS(TEXT(OSM_BI_Status__c), &quot;PROCEED&quot;))  ||  AND( CONTAINS(TEXT(OSM_BI_Status__c), &quot;PROCEED&quot;), CONTAINS(TEXT( OSM_ABN_Status__c ),&quot;Proceed&quot;))  ||  TEXT(OSM_Self_Pay_Status__c) != &quot;&quot;  ||  AND(  TEXT(OSM_Data_Transmission_Status__c)=&quot;Complete&quot;,  OR( OSM_OLI_Bill_Type__c == &quot;Genomic Health&quot;, OSM_OLI_Bill_Type__c == &quot;Bill Account&quot; )  )  ||  AND(  TEXT(OSM_Data_Transmission_Status__c)=&quot;Complete&quot;,  OSM_OLI_Bill_Type__c == &quot;Patient Post-DOS Pay&quot;,  OSM_Order_Location__c == &quot;International&quot;  )  ||  AND(  OSM_OLI_Bill_Type__c == &quot;Medicaid&quot;,  CONTAINS(TEXT(OSM_BI_Status__c),&quot;PROCEED&quot;)  )  ||  AND(  OSM_Case_Record_Type__c == &quot;Pre-Billing&quot;,  OSM_OLI_Product_Name__c = &quot;MMR&quot;,  TEXT( OSM_Data_Transmission_Status__c )= &quot;Complete&quot;)  ||  AND(  CONTAINS(TEXT(OSM_BI_Status__c),&quot;PROCEED&quot;),  OSM_OLI_Bill_Type__c == &quot;Medicare&quot;,  OSM_ABN_Required__c == false  )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update Pre-Billing Case Status to Closed</fullName>
        <actions>
            <name>OSM_Close_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Update_BI_Complete_to_System_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 41843:PB - Pre-Billing Case: Automatic closure business rules
7/6/14 - Story Logic Edited by CR Bug 53518
8/5/2014 - Story Logic Updated by CR Bug 59414</description>
        <formula>AND(      RecordType.DeveloperName = &quot;OSM_Benefits_Investigation&quot;,     OR(        AND(             NOT (ISBLANK( OSM_OLI_Bill_Type__c )) ,            OSM_Product__c =&quot;MMR&quot;,            NOT( ISBLANK( OSM_BI_Sent_Date__c ) )         ),        AND(            OSM_OLI_Bill_Type__c = &quot;Private Insurance&quot;,            OSM_Payor_Test_Hold__c = true,            OSM_Order_Location__c = &quot;Domestic&quot;,            CONTAINS( UPPER(TEXT(OSM_BI_Status__c )) ,&quot;PROCEED&quot;),            OR(               ISPICKVAL( OSM_PA_Status__c , &quot;Complete&quot;),               ISPICKVAL( OSM_PA_Status__c , &quot;Cancelled&quot;)             ),             NOT( ISBLANK(OSM_BI_Sent_Date__c ) )         ),         AND(            OSM_OLI_Bill_Type__c = &quot;Private Insurance&quot;,            OSM_Payor_Test_Hold__c = false,            OSM_Order_Location__c = &quot;Domestic&quot;,            CONTAINS( UPPER(TEXT(OSM_BI_Status__c )) ,&quot;PROCEED&quot;),            NOT( ISBLANK(OSM_BI_Sent_Date__c ) )           ),          AND(            OSM_OLI_Bill_Type__c = &quot;Private Insurance&quot;,            OSM_Order_Location__c = &quot;International&quot;,            NOT( ISBLANK( OSM_BI_Sent_Date__c ) )           ),          AND(            OSM_OLI_Bill_Type__c = &quot;Medicare&quot;,            OSM_ABN_Required__c = false,            CONTAINS( UPPER(TEXT(OSM_BI_Status__c )) ,&quot;PROCEED&quot;),            OR(                 NOT (CONTAINS(TEXT( OSM_ABN_Status__c ) ,&quot;Cancel&quot;)),                 ISBLANK(TEXT( OSM_ABN_Status__c ))             ),             NOT( ISBLANK(OSM_BI_Sent_Date__c ) )           ),          AND(            OSM_OLI_Bill_Type__c = &quot;Medicare&quot;,            OSM_ABN_Required__c = true,            CONTAINS( UPPER(TEXT(OSM_BI_Status__c )) ,&quot;PROCEED&quot;),            CONTAINS( UPPER(TEXT( OSM_ABN_Status__c )) ,&quot;PROCEED&quot;),            NOT( ISBLANK(OSM_BI_Sent_Date__c ) )           ),          AND(            OSM_OLI_Bill_Type__c = &quot;Medicaid&quot;,            CONTAINS( UPPER(TEXT(OSM_BI_Status__c )) ,&quot;PROCEED&quot;),            NOT( ISBLANK(OSM_BI_Sent_Date__c ) )           ),          AND(            OSM_OLI_Bill_Type__c = &quot;Patient Pre-Pay&quot;,            TEXT(OSM_Self_Pay_Status__c) = &quot;EHF Complete&quot;,            NOT( ISBLANK(OSM_BI_Sent_Date__c ) )           ),          AND(            OSM_OLI_Bill_Type__c = &quot;Patient Post-DOS Pay&quot;,            NOT( ISBLANK(OSM_BI_Sent_Date__c ) )           ),          AND(            OSM_OLI_Bill_Type__c = &quot;Genomic Health&quot;,            NOT( ISBLANK(OSM_BI_Sent_Date__c ) )           ),          AND(            OSM_OLI_Bill_Type__c = &quot;Bill Account&quot;,            NOT( ISBLANK(OSM_BI_Sent_Date__c ) )           )                           ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM WIP OFF on Case Close</fullName>
        <actions>
            <name>WIP_OFF_on_Case_Close_Clear_WIP_Picklist</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WIP_OFF_on_Case_Close_Clear_WIP_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM WIP Off after 1 hour</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.OSM_WIP_Pick__c</field>
            <operation>equals</operation>
            <value>ON</value>
        </criteriaItems>
        <description>User Story 22434:Time Based WIP Off</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Clear_OSM_WIP_Date_Time</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>WIP_OFF_on_Case_Close_Clear_WIP_Picklist</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>WIP_OFF_on_Case_Close_Clear_WIP_User</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.OSM_WIP_ON_Date_Time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OSM_Clear_BI_Status</fullName>
        <actions>
            <name>OSM_Clear_BI_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(TEXT(PRIORVALUE(Status)) = &apos;Closed&apos;, TEXT(Status) = &apos;Open&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Close status when Case Result is selected</fullName>
        <actions>
            <name>OSM_Close_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Case Result is selected, Case should automatically be closed</description>
        <formula>CONTAINS(Text(OSM_Case_Results__c), &quot;- Confirmed&quot;)   || TEXT(OSM_Case_Results__c)= &apos;other&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Create_SR_PDF_Form_Communication_Ready</fullName>
        <actions>
            <name>OSM_Case_Communication_Queued</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Specimen Retrieval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Specimen Retrieval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Communication Ready</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OSM_Specimen_Retrieval_Template__c</field>
            <operation>equals</operation>
            <value>Domestic Breast / Colon - GHI003,Domestic Prostate - GHI004</value>
        </criteriaItems>
        <description>User Story #34973 Specimen Retrieval Form - Communication Ready - Automated Process.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_New Clinical Experience Report Hold</fullName>
        <actions>
            <name>OSM_report_hold_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>32085 - Set Cases to Lab and Report Hold</description>
        <formula>OR (  	AND( ISNEW(), ISPICKVAL (Type,&quot;Clinical Experience&quot;)), 	AND(   	ISCHANGED(Type), 	ISPICKVAL(Type,&quot;Clinical Experience&quot;),  	NOT ISPICKVAL(PRIORVALUE(Type),&quot;Clinical Experience&quot;)) 	 	)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_SR_Form_Communication_Sent</fullName>
        <actions>
            <name>Case_Specimen_Request_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 42500:Specimen Request Date - GHI to Request</description>
        <formula>if ( AND(Text(Status) == &apos;Communication Sent&apos;, Text(Type) == &apos;Specimen Retrieval&apos;,  ISNULL(OSM_Specimen_Request_Date_Time__c )),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pre-Billing Case escalation</fullName>
        <actions>
            <name>OSM_Set_Email_Inquiry_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Set_Owner_to_QDXUpdate_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Bug 64926:CR Pre-Billing Case escalation when BI Status changes to incomplete or cancelled after the case is closed</description>
        <formula>RecordType.DeveloperName = &quot;OSM_Benefits_Investigation&quot; &amp;&amp; TEXT(Status) = &quot;Closed&quot; &amp;&amp; CONTAINS(UPPER(TEXT(PRIORVALUE(OSM_BI_Status__c))),&quot;PROCEED&quot;) &amp;&amp; NOT(CONTAINS(UPPER(TEXT(OSM_BI_Status__c)),&quot;PROCEED&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Primary Customer Field Populated</fullName>
        <actions>
            <name>Update_Primary_Customer_Populated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(       OSM_Primary_Customer_Populated__c = false,     NOT(ISBLANK( OSM_Primary_Customer__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email Everytime Document is Uploaded to Portal</fullName>
        <actions>
            <name>Send_Email_to_Customer_Service_when_Document_is_Uploaded_to_Portal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>New Portal Documents</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>4 - Urgent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Generic</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Canceled_Incomplete_Case</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>4 - Urgent</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Canceled/Incomplete Case</subject>
    </tasks>
    <tasks>
        <fullName>Data_Transmission_Error</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Data Transmission Error</subject>
    </tasks>
    <tasks>
        <fullName>PA_INCOMPLETE_MD_Non_Compliant</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>PA - INCOMPLETE: MD Non Compliant</subject>
    </tasks>
    <tasks>
        <fullName>PA_Pending</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>PA - Pending</subject>
    </tasks>
</Workflow>
