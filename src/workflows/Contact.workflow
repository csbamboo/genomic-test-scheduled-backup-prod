<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Record_Type_based_on_Speciality</fullName>
        <description>bug 61536</description>
        <field>RecordTypeId</field>
        <lookupValue>OSM_Other</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type based on Speciality</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Contact_Record_Type_on_Lead_Conv</fullName>
        <description>When a Lead is created through portal, the Contact record type will default to Other - Domestic</description>
        <field>RecordTypeId</field>
        <lookupValue>OSM_Other</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Default Contact Record Type on Lead Conv</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deselect_Contact_Oncology</fullName>
        <field>OSM_Oncology__c</field>
        <literalValue>0</literalValue>
        <name>Deselect Contact Oncology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deslect_Contact_Urology</fullName>
        <field>OSM_Urology__c</field>
        <literalValue>0</literalValue>
        <name>Deslect Contact Urology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fax_email_Contact</fullName>
        <field>OSM_Fax_email__c</field>
        <formula>IF( NOT( ISBLANK(Fax) ) , &apos;1&apos;+
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Fax, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;)+
&apos;@faxmail.com&apos;,  OSM_Fax_email__c )</formula>
        <name>Fax email - Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_Contact_Oncology</fullName>
        <field>OSM_Oncology__c</field>
        <literalValue>1</literalValue>
        <name>Check Contact Oncology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_Contact_Urology</fullName>
        <field>OSM_Urology__c</field>
        <literalValue>1</literalValue>
        <name>Check Contact Urology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Contact_Rep_preference_Field_Update</fullName>
        <field>OSM_Report_Delivery_Language_Preference__c</field>
        <literalValue>German</literalValue>
        <name>Contact Report preference Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Contact_Report_Pref_Field_Update_Eng</fullName>
        <field>OSM_Report_Delivery_Language_Preference__c</field>
        <literalValue>English</literalValue>
        <name>Contact Report Pref Field Update Eng</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Copy_Prtnr_Acct_to_Cntct_First_Name</fullName>
        <field>FirstName</field>
        <formula>Account.Name &amp; &quot; - &quot; &amp;  FirstName</formula>
        <name>Copy Partner Acct to Contact First Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Patient_Initials</fullName>
        <field>OSM_Patient_Initials__c</field>
        <formula>LEFT( FirstName, 1) +  LEFT(LastName, 1)</formula>
        <name>OSM_Update_Patient_Initials</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Speciality</fullName>
        <field>OSM_Specialty__c</field>
        <literalValue>Other</literalValue>
        <name>Update Contact Speciality</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Search_String</fullName>
        <description>US 23514</description>
        <field>OSM_Search_String__c</field>
        <formula>TEXT(MONTH(Birthdate))+&quot;/&quot; +TEXT(DAY(Birthdate))+&quot;/&quot; +TEXT(YEAR(Birthdate))</formula>
        <name>Update Search String</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Specialty Values if not equal to HCP</fullName>
        <actions>
            <name>Change_Record_Type_based_on_Speciality</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Clinical Coordinator,Histotech,Hospital Supervisor,Lab Technician,Medical Assistant,Other,Patient Proxy,Principal Investigator,Registered Nurse</value>
        </criteriaItems>
        <description>bug 61536</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GHI Portal Default Contact Record Type on Lead Conv</fullName>
        <actions>
            <name>Default_Contact_Record_Type_on_Lead_Conv</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Speciality</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Portal</value>
        </criteriaItems>
        <description>When a Lead is submitted through the Customer Provisioning Wizard and then converted to a Contact, the Contact Record Type will default to Other - Domestic</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IntlSFA Update Oncology Checkbox for HCP DE%2FUK%2C Other International</fullName>
        <actions>
            <name>OSM_Check_Contact_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP UK,HCP DE,Other - International</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>contains</operation>
            <value>Oncologist,NP,PA,Urologist,Pathologist,Radiation Oncologist,Gynecologist,Surgeon,Registered Nurse,Lab Technician,Medical Assistant,Histotech,Hospital Supervisor,Clinical Coordinator,Principal Investigator,Patient Proxy,Other</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IntlSFA Update Urology Checkbox for HCP DE%2FUK%2C Other International</fullName>
        <actions>
            <name>OSM_Check_Contact_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP UK,HCP DE,Other - International</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>contains</operation>
            <value>Oncologist,NP,PA,Urologist,Pathologist,Radiation Oncologist,Gynecologist,Surgeon,Registered Nurse,Lab Technician,Medical Assistant,Histotech,Hospital Supervisor,Clinical Coordinator,Principal Investigator,Patient Proxy,Other</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Copy Partner Account Name</fullName>
        <actions>
            <name>OSM_Copy_Prtnr_Acct_to_Cntct_First_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule copies the Partner Account Name to the Contact Name</description>
        <formula>IF(AND(CONTAINS(Account.RecordType.Name, &quot;Partner&quot;) &amp;&amp; CONTAINS(RecordType.Name, &quot;Partner&quot;) , OR( ISNEW(), ISCHANGED(FirstName), ISCHANGED(LastName))), true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Populate Search String</fullName>
        <actions>
            <name>Update_Search_String</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>US 23514:  Customer Service Global Search</description>
        <formula>OR(ISCHANGED(Birthdate), ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set Fax Email - Contact</fullName>
        <actions>
            <name>Fax_email_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>User Story 17883 Updates the Fax Email field on Contact</description>
        <formula>OR(    IF( ISCHANGED( Fax ) , true, false) ,      NOT(ISBLANK(Fax)),    ISNEW()  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Contact_Speciality_Derives_Market_1</fullName>
        <actions>
            <name>Deslect_Contact_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_Contact_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Oncologist</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Contact_Speciality_Derives_Market_2</fullName>
        <actions>
            <name>Deselect_Contact_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_Contact_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Urologist</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Contact_Speciality_Derives_Market_3</fullName>
        <actions>
            <name>OSM_Check_Contact_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_Contact_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>NP,PA,Pathologist,Surgeon,Other</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Contact_Speciality_Derives_Market_4</fullName>
        <actions>
            <name>Deslect_Contact_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Check_Contact_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Radiation Oncologist,Gynecologist</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Contact_report_language</fullName>
        <actions>
            <name>OSM_Contact_Rep_preference_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
            <value>Germany</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Report_Language_override__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Patient</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Contact_report_language_eng</fullName>
        <actions>
            <name>OSM_Contact_Report_Pref_Field_Update_Eng</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>notEqual</operation>
            <value>Germany</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.OSM_Report_Language_override__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Patient</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Populate_Patient_Initials</fullName>
        <actions>
            <name>OSM_Update_Patient_Initials</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.FirstName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Project : OSM
Sprint : Foundational Sprint (Sprint 1)
Description : Populate Patient Initials field on Contact</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Update_Contact_Oncology</fullName>
        <actions>
            <name>OSM_Check_Contact_Oncology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Oncologist,Pathologist,Radiation Oncologist,Surgeon,Registered Nurse,Lab Technician,Medical Assistant,Histotech,Hospital Supervisor,Clinical Coordinator,Principal Investigator,Patient Proxy,Other</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Update_Contact_Urology</fullName>
        <actions>
            <name>OSM_Check_Contact_Urology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.OSM_Specialty__c</field>
            <operation>equals</operation>
            <value>Urologist,Gynecologist,Surgeon,Registered Nurse,Lab Technician,Medical Assistant,Histotech,Hospital Supervisor,Clinical Coordinator,Principal Investigator,Patient Proxy,Other</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
