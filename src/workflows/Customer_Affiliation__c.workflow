<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IntlSFA_Update_Fax_with_Account_Fax</fullName>
        <field>IntlSFA_Fax__c</field>
        <formula>OSM_Account_1__r.Fax</formula>
        <name>Update Fax with Account Fax</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IntlSFA_Update_Mobile_with_Contact_Mobil</fullName>
        <field>IntlSFA_Mobile__c</field>
        <formula>OSM_Contact_1__r.MobilePhone</formula>
        <name>Update Mobile with Contact Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IntlSFA_Update_Phone_with_Account_Phone</fullName>
        <field>IntlSFA_Phone__c</field>
        <formula>OSM_Account_1__r.Phone</formula>
        <name>Update Phone with Account Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>IntlSFA Update Fax with Account Fax</fullName>
        <actions>
            <name>IntlSFA_Update_Fax_with_Account_Fax</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Affiliation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Affiliation UK,Customer Affiliation DE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Customer_Affiliation__c.IntlSFA_Fax__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates the Fax field with Account Fax Number if Customer Affiliation Record Type = Customer Affiliation DE or Customer Affiliation UK AND Fax field is blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IntlSFA Update Mobile with Contact Mobile</fullName>
        <actions>
            <name>IntlSFA_Update_Mobile_with_Contact_Mobil</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Affiliation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Affiliation UK,Customer Affiliation DE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Customer_Affiliation__c.IntlSFA_Mobile__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates the Mobile field with Contact Mobile Number if Customer Affiliation Record Type = Customer Affiliation DE or Customer Affiliation UK AND Mobile field is blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IntlSFA Update Phone with Account Phone</fullName>
        <actions>
            <name>IntlSFA_Update_Phone_with_Account_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Affiliation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Affiliation UK,Customer Affiliation DE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Customer_Affiliation__c.IntlSFA_Phone__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates the Phone field with Account Phone if Customer Affiliation Record Type = Customer Affiliation DE or Customer Affiliation UK AND Phone field is blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
