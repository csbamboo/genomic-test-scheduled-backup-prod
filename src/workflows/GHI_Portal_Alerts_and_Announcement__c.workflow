<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GHI_Portal_Published_Date_Update</fullName>
        <field>GHI_Portal_Published_Date__c</field>
        <formula>NOW()</formula>
        <name>GHI_Portal_Published_Date_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GHI_Portal_Set_Published_Date</fullName>
        <actions>
            <name>GHI_Portal_Published_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GHI_Portal_Alerts_and_Announcement__c.GHI_Portal_Published__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
