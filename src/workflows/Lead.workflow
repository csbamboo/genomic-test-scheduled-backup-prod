<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GHI_Portal_Update_Middle_Name</fullName>
        <description>This updates the value of the standard Middle Name from the Custom Middle Name</description>
        <field>MiddleName</field>
        <formula>GHI_Portal_Middle_Name__c</formula>
        <name>Update Middle Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Portal_Lead_Owner_is_Portal_Lead_Queue</fullName>
        <description>Changes the Lead Owner to the Portal Lead queue if the Lead is created from the Customer Provisioning Wizard</description>
        <field>OwnerId</field>
        <lookupValue>Portal_Lead</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Portal Lead Owner is Portal Lead Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GHI Portal Lead Owner is Portal Lead Queue</fullName>
        <actions>
            <name>Portal_Lead_Owner_is_Portal_Lead_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>GHI Portal SignUp Site Guest User</value>
        </criteriaItems>
        <description>When a Lead is created from the Customer Provisioning Page, the Lead Owner will default to the Portal Lead Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GHI Portal Update Middle Name</fullName>
        <actions>
            <name>GHI_Portal_Update_Middle_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Contacted,Open</value>
        </criteriaItems>
        <description>User Story - 15461
To populate the standard Middle name field from the custom Middle Name field in the Web-to-Lead form</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
