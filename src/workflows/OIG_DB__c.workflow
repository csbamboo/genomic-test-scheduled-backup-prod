<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_SetShortAdd1</fullName>
        <field>OSM_ShortAddress1__c</field>
        <formula>LEFT(OSM_Address1__c, 255)</formula>
        <name>SetShortAdd1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_SetShortBusName</fullName>
        <field>OSM_ShortBusName__c</field>
        <formula>LEFT(OSM_BusName__c, 255)</formula>
        <name>SetShortBusName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_SetShortFullName</fullName>
        <field>OSM_ShortFullName__c</field>
        <formula>LEFT(OSM_FullName__c, 255)</formula>
        <name>SetShortFullName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OSM_OIG_DB_Set_Short_Values</fullName>
        <actions>
            <name>OSM_SetShortAdd1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_SetShortBusName</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_SetShortFullName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
