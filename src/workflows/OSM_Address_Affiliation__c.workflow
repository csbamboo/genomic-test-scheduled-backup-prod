<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_Main_Address_False</fullName>
        <field>OSM_Main_Address__c</field>
        <literalValue>0</literalValue>
        <name>Main Address False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Private_Address_Update</fullName>
        <description>Update address name to Private if record is saved with Private Address checked.</description>
        <field>Name</field>
        <formula>&quot;Private&quot;</formula>
        <name>Private Address Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OSM_Default_Main_Address_Off</fullName>
        <actions>
            <name>OSM_Main_Address_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Address_Affiliation__c.OSM_Type__c</field>
            <operation>excludes</operation>
            <value>Main</value>
        </criteriaItems>
        <description>Unchecks the &quot;Main Address&quot; box for every new address affiliation with its type field not containing &quot;Main&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Private_Address</fullName>
        <actions>
            <name>OSM_Private_Address_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Address_Affiliation__c.OSM_Type__c</field>
            <operation>includes</operation>
            <value>Private Phone/Email</value>
        </criteriaItems>
        <description>Rename the record to &quot;Private&quot; if Private Address checkbox is ticked in the record.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
