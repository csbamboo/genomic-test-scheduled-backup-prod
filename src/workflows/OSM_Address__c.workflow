<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_Address_Update</fullName>
        <description>Update Address Name</description>
        <field>Name</field>
        <formula>IF(ISBLANK(OSM_Address_Line_1__c), &quot;&quot;, OSM_Address_Line_1__c) + IF(ISBLANK(OSM_City__c), &quot;&quot;, &quot; - &quot; + OSM_City__c) + IF(ISBLANK(OSM_State_Code__c), &quot;&quot;, &quot; - &quot; + OSM_State_Code__c) + IF(ISBLANK(OSM_Zip__c), &quot;&quot;, &quot; - &quot; + OSM_Zip__c) + IF(ISBLANK(OSM_Country_Code__c), &quot;&quot;, &quot; - &quot; + OSM_Country_Code__c)</formula>
        <name>OSM_Address Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OSM_Address Update</fullName>
        <actions>
            <name>OSM_Address_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update address field to Full Address</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
