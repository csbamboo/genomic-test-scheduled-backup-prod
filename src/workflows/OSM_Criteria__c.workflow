<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_Update_Last_AA_Update</fullName>
        <field>OSM_Last_AA_Update__c</field>
        <formula>NOW()</formula>
        <name>Update Last AA Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OSM Update Last AA Update</fullName>
        <actions>
            <name>OSM_Update_Last_AA_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Last AA Update field when Netsuite User updates the record</description>
        <formula>LastModifiedBy.Id  =  $Setup.Record_Type__c.OSM_User_Netsuite_User__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
