<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_Delegate_Name</fullName>
        <description>Update Delegate name</description>
        <field>Name</field>
        <formula>OSM_Contact__r.FirstName + &quot; &quot; + OSM_Contact__r.LastName + &quot; for &quot; + OSM_HCP__r.FirstName + &quot; &quot; + OSM_HCP__r.LastName + &quot; at &quot; + OSM_HCO__r.Name</formula>
        <name>OSM Delegate Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OSM_Full_Delegate_name</fullName>
        <actions>
            <name>OSM_Delegate_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
