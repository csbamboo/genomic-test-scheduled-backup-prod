<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>GHI_Portal_Send_Email_Notification_of_Result</fullName>
        <description>Send Email Notification of Result</description>
        <protected>false</protected>
        <recipients>
            <field>GHI_Portal_Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OSM_Genomic_Health_Templates/GHI_Portal_Notification_of_Result_2</template>
    </alerts>
    <alerts>
        <fullName>GHI_Portal_Send_Email_Notification_of_Result_Delegate</fullName>
        <description>Send Email Notification of Result to Delegate</description>
        <protected>false</protected>
        <recipients>
            <field>GHI_Portal_Delegate_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OSM_Genomic_Health_Templates/GHI_Portal_Notification_of_Result_2</template>
    </alerts>
    <fieldUpdates>
        <fullName>GHI_Portal_Email_of_Delegate</fullName>
        <description>Gets email of Delegate from Distribution Event object</description>
        <field>GHI_Portal_Delegate_Email__c</field>
        <formula>OSM_Order_Role__r.OSM_Delegate__r.OSM_Contact__r.Email</formula>
        <name>Email of Delegate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GHI_Portal_Email_of_Order_Role_Contact</fullName>
        <field>GHI_Portal_Customer_Email__c</field>
        <formula>OSM_Order_Role__r.OSM_Portal_Email__c</formula>
        <name>Email of Order Role Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>GHI Portal Notification of Result</fullName>
        <actions>
            <name>GHI_Portal_Send_Email_Notification_of_Result</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>GHI_Portal_Email_of_Order_Role_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story: 39251 This sends out an email notification to the customer of the Order</description>
        <formula>AND ( OSM_Order_Role__r.OSM_Contact__r.GHI_Portal_Receive_Patient_Report_Email__c = TRUE, ISPICKVAL(OSM_Distribution_Preference__c, &quot;Portal&quot;), ISPICKVAL( OSM_Distribution_Status__c , &quot;Success&quot;)     )</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GHI Portal Notification of Result to Delegate</fullName>
        <actions>
            <name>GHI_Portal_Send_Email_Notification_of_Result_Delegate</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>GHI_Portal_Email_of_Delegate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story: 48503 This sends out an email notification to the delegate of the Order</description>
        <formula>AND (   OSM_Order_Role__r.OSM_Delegate__r.OSM_HCP__r.GHI_Portal_Receive_Patient_Report_Email__c = TRUE, ISPICKVAL(OSM_Distribution_Preference__c, &quot;Portal&quot;), ISPICKVAL( OSM_Distribution_Status__c , &quot;Success&quot;)     )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
