<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Fax_email_Order_Role</fullName>
        <field>OSM_Order_Role_Fax_Email__c</field>
        <formula>IF( NOT( ISBLANK( OSM_Account_Fax__c ) ) , &apos;1&apos;+
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(OSM_Account_Fax__c , &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;)+
&apos;@faxmail.com&apos;, &apos;OSM_Order_Role_Fax_Email__c &apos;)</formula>
        <name>Fax email - Order Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Account_Listed_on_Report_UC</fullName>
        <description>26427 Initial Report Distribution Preference: Listed on Report</description>
        <field>OSM_Account_Listed_On_Report__c</field>
        <literalValue>0</literalValue>
        <name>Account Listed on Report Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_Contact_listed_on_Report</fullName>
        <description>26427 Initial Report Distribution Preference: Listed on Report</description>
        <field>OSM_Listed_On_Report__c</field>
        <literalValue>1</literalValue>
        <name>Check Contact listed on Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_ORole_Report_Dist_Ready</fullName>
        <description>19850 - Report Distribution Ready field- OLI
Check Report Distribution checkbox.
Unchecked will be summarized in Order.</description>
        <field>OSM_Report_Distribution_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Check ORole Report Distribution Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Fax_Email_to_Blank</fullName>
        <description>Fix for 26819</description>
        <field>OSM_Order_Role_Fax_Email__c</field>
        <name>Fax Email to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_ORole_AccountReceiveCopy_Check</fullName>
        <description>26625 Initial Report Distribution Preference: Receives Copy of Report</description>
        <field>OSM_Account_Receive_Copy_of_Report__c</field>
        <literalValue>1</literalValue>
        <name>ORole AccountReceiveCopy Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_ORole_AccountReceiveCopy_Uncheck</fullName>
        <description>26625 Initial Report Distribution Preference: Receives Copy of Report</description>
        <field>OSM_Account_Receive_Copy_of_Report__c</field>
        <literalValue>0</literalValue>
        <name>ORole AccountReceiveCopy Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_ORole_ContactReceiveCopy_Check</fullName>
        <description>26625 Initial Report Distribution Preference:  Receives Copy of Report</description>
        <field>OSM_Always_Receives_Copies_of_Reports__c</field>
        <literalValue>1</literalValue>
        <name>ORole ContactReceiveCopy Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_ORole_ContactReceiveCopy_Uncheck</fullName>
        <description>26625 Initial Report Distribution Preference: Receives Copy of Report</description>
        <field>OSM_Always_Receives_Copies_of_Reports__c</field>
        <literalValue>0</literalValue>
        <name>ORole ContactReceiveCopy Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Ordering_BI_Ready_Is_False</fullName>
        <field>OSM_BI_Ready__c</field>
        <literalValue>0</literalValue>
        <name>Ordering BI Ready Is False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Ordering_BI_Ready_Is_True</fullName>
        <field>OSM_BI_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Ordering BI Ready Is True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Reset_Order_Role_run_on_Code</fullName>
        <description>Sprint 29 
User Story - 26427 Initial Report Distribution Preference: Listed on Report 
User Story - 26625 Initial Report Distribution Preference: Receives Copy of Report</description>
        <field>OSM_Has_Run_from_Code__c</field>
        <literalValue>0</literalValue>
        <name>OSM Reset Order Role run on Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Role_Specimen_Not_Ready</fullName>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <field>OSM_Specimen_Retrieval_Ready__c</field>
        <literalValue>0</literalValue>
        <name>Role Specimen Not Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Role_Specimen_Ready</fullName>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <field>OSM_Specimen_Retrieval_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Role Specimen Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_SetOrderRoleName</fullName>
        <field>Name</field>
        <formula>LEFT(TEXT(OSM_Role__c)
&amp; &quot; - &quot; &amp; 
 IF(NOT(ISBLANK(OSM_Delegate_Contact__c)), 
  OSM_Delegate_Contact__r.FirstName &amp; &quot; &quot; &amp;
  OSM_Delegate_Contact__r.LastName  &amp;  &quot; for &quot;,
  &quot;&quot;) 
  &amp;
 IF(NOT(ISBLANK(OSM_Contact__c)), 
  OSM_Contact__r.FirstName &amp; &quot; &quot; &amp; OSM_Contact__r.LastName,
  &quot;&quot;) 
  &amp; 
 IF(AND(NOT(ISBLANK(OSM_Contact__c)), NOT(ISBLANK(OSM_Account__c))), 
  &quot; at &quot;,
  &quot;&quot;) 
  &amp; 
 IF(NOT(ISBLANK(OSM_Account__c)), 
  OSM_Account__r.Name,
  &quot;&quot;),80)</formula>
        <name>SetOrderRoleName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Uncheck_Contact_listed_on_Report</fullName>
        <description>26427 Initial Report Distribution Preference: Listed on Report</description>
        <field>OSM_Listed_On_Report__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Contact listed on Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Uncheck_ORole_Report_Dist_Ready</fullName>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <field>OSM_Report_Distribution_Ready__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck ORole Report Dist Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Count_Exclusions</fullName>
        <description>OM Feb Housekeeping - User Story 12050 - OIG - Updates</description>
        <field>OSM_Count_Exclusions__c</field>
        <formula>IF(
  AND(
   OSM_Exclusion_Match_Found__c,
   NOT(OSM_Verified_No_Match__c)
  )
 , 1, 0) + 
IF(
  AND(
   OSM_Account_Exclusion_Found__c,
   NOT(OSM_Account_Not_Exclusion__c)
  )
 , 1, 0)</formula>
        <name>Update Count Exclusions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Role_Specimen_Not_Ready</fullName>
        <field>OSM_Specimen_Retrieval_Ready__c</field>
        <literalValue>0</literalValue>
        <name>Role Specimen Not Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OSM Blank Fax Email</fullName>
        <actions>
            <name>OSM_Fax_Email_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Account_Fax__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Fix for bug: 26819 -  Order Role Table - Address fields are not stamped when only Contact is selected</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Reset Order Role run on Code</fullName>
        <actions>
            <name>OSM_Reset_Order_Role_run_on_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sprint 29 
User Story - 26427 Initial Report Distribution Preference: Listed on Report 
User Story - 26625 Initial Report Distribution Preference: Receives Copy of Report</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set Fax Email - Order Role</fullName>
        <actions>
            <name>Fax_email_Order_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 17883 Updates the Fax Email field on Order Role</description>
        <formula>OR(     AND(        IF(ISCHANGED(  OSM_Account_Fax__c ) , true, false),        NOT(ISBLANK(OSM_Account_Fax__c ))        ),      NOT(ISBLANK(OSM_Account_Fax__c )),    AND(ISNEW(), NOT(ISBLANK(OSM_Account_Fax__c ))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Count_Exclusions</fullName>
        <actions>
            <name>OSM_Update_Count_Exclusions</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>OM Feb Housekeeping - User Story 12050 - OIG - Updates</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_ORole_Check_Report_Distribution_Ready</fullName>
        <actions>
            <name>OSM_Check_ORole_Report_Dist_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <formula>IF(ISPICKVAL(OSM_Role__c, &quot;Ordering&quot;), 	IF(ISPICKVAL(OSM_Order__r.Order_Location__c, &quot;Domestic&quot;), 		NOT(ISBLANK(OSM_NPI__c)), 		TRUE	 	) 	&amp;&amp; 	NOT(ISBLANK(OSM_Account_Name__c)) 	&amp;&amp; 	NOT(ISBLANK( OSM_Account_Address_Line_1__c)) 	&amp;&amp; 	NOT(ISBLANK(OSM_City__c)) 	&amp;&amp; 	NOT(ISBLANK(OSM_Account_State__c)) 	&amp;&amp; 	NOT(ISBLANK(OSM_Account_Postal_Address__c)), 	TRUE ) &amp;&amp; IF(OSM_Always_Receives_Copies_of_Reports__c, 	NOT(ISBLANK(TEXT(OSM_Report_Delivery_Language_Preference__c))) &amp;&amp; NOT(ISBLANK(OSM_Delivery_method_of_report__c)), 	TRUE ) &amp;&amp; IF(OSM_Always_Receives_Copies_of_Reports__c &amp;&amp; INCLUDES(OSM_Delivery_method_of_report__c, &quot;Fax&quot;), 	NOT(ISBLANK( OSM_Account_Fax__c )), 	TRUE		 )  &amp;&amp; IF(INCLUDES(OSM_Delivery_method_of_report__c , &quot;Email&quot;), 	NOT(ISBLANK(OSM_Portal_Email__c)), 	TRUE ) &amp;&amp; NOT(ISBLANK(OSM_Contact_Full_Name__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_ORole_Uncheck_Report_Distribution_Ready</fullName>
        <actions>
            <name>OSM_Uncheck_ORole_Report_Dist_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <formula>NOT( IF(ISPICKVAL(OSM_Role__c, &quot;Ordering&quot;),  IF(ISPICKVAL(OSM_Order__r.Order_Location__c, &quot;Domestic&quot;),  NOT(ISBLANK(OSM_NPI__c)),  TRUE	 )  &amp;&amp;  NOT(ISBLANK(OSM_Account_Name__c))  &amp;&amp;  NOT(ISBLANK( OSM_Account_Address_Line_1__c))  &amp;&amp;  NOT(ISBLANK(OSM_City__c))  &amp;&amp;  NOT(ISBLANK(OSM_Account_State__c))  &amp;&amp;  NOT(ISBLANK(OSM_Account_Postal_Address__c)),  TRUE  )  &amp;&amp;  IF(OSM_Always_Receives_Copies_of_Reports__c,  NOT(ISBLANK(TEXT(OSM_Report_Delivery_Language_Preference__c))) &amp;&amp; NOT(ISBLANK(OSM_Delivery_method_of_report__c)),  TRUE  )  &amp;&amp;  IF(OSM_Always_Receives_Copies_of_Reports__c &amp;&amp; INCLUDES(OSM_Delivery_method_of_report__c, &quot;Fax&quot;),  NOT(ISBLANK( OSM_Account_Fax__c )),  TRUE	 ) &amp;&amp;  IF(INCLUDES(OSM_Delivery_method_of_report__c , &quot;Email&quot;),  NOT(ISBLANK(OSM_Portal_Email__c)),  TRUE  )  &amp;&amp;  NOT(ISBLANK(OSM_Contact_Full_Name__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OrderRole_Set_Name</fullName>
        <actions>
            <name>OSM_SetOrderRoleName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Role_Specimen_Not_Ready</fullName>
        <actions>
            <name>OSM_Role_Specimen_Not_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Role_Specimen_Not_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>((1 OR 2) OR (3 AND (4 OR 5)) OR (6 AND (7 OR 8)))</booleanFilter>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Role__c</field>
            <operation>notEqual</operation>
            <value>Ordering</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Contact_Full_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Role__c</field>
            <operation>notEqual</operation>
            <value>Specimen Submitting</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Account_Fax__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Account_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_All_Roles__c</field>
            <operation>excludes</operation>
            <value>Specimen Submitting</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Account_Fax__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Account_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Role_Specimen_Ready</fullName>
        <actions>
            <name>OSM_Role_Specimen_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(((1 OR 2) AND (3 AND 4)) OR (5 AND 6))</booleanFilter>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Role__c</field>
            <operation>equals</operation>
            <value>Specimen Submitting</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_All_Roles__c</field>
            <operation>includes</operation>
            <value>Specimen Submitting</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Account_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Account_Fax__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Role__c</field>
            <operation>equals</operation>
            <value>Ordering</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Role__c.OSM_Contact_Full_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Ordering_Role_BI_Not_Ready</fullName>
        <actions>
            <name>OSM_Ordering_BI_Ready_Is_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(         OSM_BI_Ready__c,      OR(            NOT(ISPICKVAL(OSM_Order__r.OSM_State__c ,&quot;Active&quot;)),          IF( AND( ISPICKVAL( OSM_Role__c ,&quot;Ordering&quot;),                  NOT(ISBLANK(OSM_Account__c)),                  NOT(ISBLANK(OSM_Contact__c)) ) ,            OR(                 ISBLANK(OSM_Account_Address_Line_1__c),                ISBLANK(OSM_City__c),                ISBLANK(OSM_Account_Country__c),                ISBLANK(OSM_Account_Phone__c), 	      ISBLANK(OSM_Contact_Full_Name__c),                 ISBLANK(OSM_Account_Name__c),               (IF(TEXT(OSM_Order__r.Order_Location__c) = &quot;Domestic&quot;,                 OR( (ISBLANK(OSM_Account_State__c)),                    (ISBLANK(OSM_Account_Postal_Address__c))                   ),  FALSE   )            )        ), TRUE)     ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Ordering_Role_BI_Ready</fullName>
        <actions>
            <name>OSM_Ordering_BI_Ready_Is_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(        NOT(OSM_BI_Ready__c),        IF( AND( ISPICKVAL( OSM_Role__c ,&quot;Ordering&quot;),              NOT(ISBLANK(OSM_Account__c)),                NOT(ISBLANK(OSM_Contact__c))             ) ,         AND( NOT(ISBLANK(OSM_Account_Address_Line_1__c)),              NOT(ISBLANK(OSM_City__c)),              NOT(ISBLANK(OSM_Account_Country__c)),               NOT(ISBLANK(OSM_Account_Phone__c)), 	     NOT(ISBLANK(OSM_Contact_Full_Name__c)),               NOT(ISBLANK(OSM_Account_Name__c))             )         , FALSE) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
