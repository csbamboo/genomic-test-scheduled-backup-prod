<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_BillingPolicyDataStatus_to_N_A</fullName>
        <description>Sprint 15
16855 - Billing Policy Data Status Check and Initial Qualification Status Check, Specimen</description>
        <field>OSM_Billing_Policy_Data_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>OSM_BillingPolicyDataStatus_to_N/A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OSM_Order_Specimen_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_BillingPolicyQualification_to_N_A</fullName>
        <description>Sprint 15
16855 - Billing Policy Data Status Check and Initial Qualification Status Check, Specimen</description>
        <field>OSM_Billing_Policy_Qualification_status__c</field>
        <literalValue>N/A</literalValue>
        <name>OSM_BillingPolicyQualification_to_N/A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OSM_Order_Specimen_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OSOLI_Bill_Policy_NA_TRUE</fullName>
        <description>User Story 16855:Billing Policy Data Status Check and Initial Qualification Status Check, Specimen</description>
        <field>OSM_Billing_policy_N_A__c</field>
        <literalValue>1</literalValue>
        <name>OSOLI Bill Policy NA TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OSOLI_Bill_Policy_Qual_TRUE</fullName>
        <description>Sprint 29 - 16856 Sets the Billing Policy Qualification ready to true on the OS OLI.</description>
        <field>OSM_Billing_Policy_Qualification_ready__c</field>
        <literalValue>1</literalValue>
        <name>OSOLI Bill Policy Qual TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OSM_BillingPolicyData_Rule</fullName>
        <actions>
            <name>OSM_OSOLI_Bill_Policy_Qual_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - 16856 Sets the Billing Policy Qualification ready flag on the Order Specimen OLI to true</description>
        <formula>AND(  ISPICKVAL(OSM_OLI_ID__r.Order.OSM_Status__c, &quot;Processing&quot;), OR(  NOT(ISBLANK(OSM_OLI_ID__r.OSM_First_Specimen_Received_Date__c )),  OSM_Order_Specimen_ID__r.OSM_Available_for_Processing__c = TRUE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_BillingPolicyData_Rule1</fullName>
        <actions>
            <name>OSM_BillingPolicyDataStatus_to_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_BillingPolicyQualification_to_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sprint 15
16855 - Billing Policy Data Status Check and Initial Qualification Status Check, Specimen</description>
        <formula>AND( NOT(ISBLANK(TEXT(OSM_OLI_ID__r.OSM_First_Specimen_Received_Date__c))), OR( ISPICKVAL(OSM_OLI_ID__r.OSM_Billing_Policy_Data_Status__c, &quot;Data Available&quot;),  ISPICKVAL(OSM_OLI_ID__r.OSM_Billing_Policy_Data_Status__c, &quot;N/A&quot;) ), ISPICKVAL(OSM_Order_Specimen_ID__r.OSM_Billing_Policy_Qualification_status__c, &quot;&quot;), ISPICKVAL(OSM_OLI_ID__r.OSM_Billing_Policy_Qualification_Status__c, &quot;N/A&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_BillingPolicyData_Rule2</fullName>
        <actions>
            <name>OSM_OSOLI_Bill_Policy_NA_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 15
16855 - Billing Policy Data Status Check and Initial Qualification Status Check, Specimen</description>
        <formula>AND(  ISPICKVAL(OSM_OLI_ID__r.Order.OSM_Status__c, &quot;Processing&quot;),  OR(  OSM_OLI_ID__r.Order.OSM_Billing_Policy_Override__c = true,  AND (  NOT ( ISPICKVAL(OSM_OLI_ID__r.Order.OSM_Bill_Type__c , &apos;&apos;)),  OSM_OLI_ID__r.Order.OSM_Billing_Policy_Payor__c = False  )),  OR(  NOT(ISBLANK(OSM_OLI_ID__r.OSM_First_Specimen_Received_Date__c )), OSM_Order_Specimen_ID__r.OSM_Available_for_Processing__c = TRUE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
