<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSMClear_Order_Specimen_Storage_Duration</fullName>
        <description>17721 – Calculation Fields on Specimen</description>
        <field>OSM_Specimen_Storage_Duration__c</field>
        <name>Clear Order Specimen Storage Duration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Bill_Policy_Qualification_Status_NA</fullName>
        <description>Sprint 29 - Story 16856 Set the Specimen Billing Policy Qualification Status to N/A</description>
        <field>OSM_Billing_Policy_Qualification_status__c</field>
        <literalValue>N/A</literalValue>
        <name>OSM_Specimen BP Qual Status NA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Billing_Policy_Data_Status_NA</fullName>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Data Status to N/A</description>
        <field>OSM_Billing_Policy_Data_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>OSM_Specimen BP Data Status NA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Billing_Policy_Qualification_Status</fullName>
        <description>Sprint 29 - Story 16856 Set the Specimen Billing Policy Qualification Status to Disqualified</description>
        <field>OSM_Billing_Policy_Qualification_status__c</field>
        <literalValue>Disqualified</literalValue>
        <name>OSM_Specimen BP Qual Status Disqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Billing_Policy_Qualified</fullName>
        <description>Sprint 29 - Story 16856 Set the Specimen Billing Policy Qualification Status to Qualified</description>
        <field>OSM_Billing_Policy_Qualification_status__c</field>
        <literalValue>Qualified</literalValue>
        <name>OSM_Specimen BP Qual Status Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Check_Specimen_Report_Dist_Ready</fullName>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <field>OSM_Report_Distribution_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Check Specimen Report Distribution Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_External_ID_to_Specimen_Name</fullName>
        <description>Feb Housekeeping: Populate the &apos;External Specimen ID&apos; into the &apos;Specimen Name&apos; field.

11789 – Housekeeping Config Guide Updates - Specimen</description>
        <field>Name</field>
        <formula>OSM_External_Specimen_ID__c</formula>
        <name>External ID to Specimen Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Null_External_ID_to_Specimen_Name</fullName>
        <description>OM Feb Housekeeping (11789 – Housekeeping Config Guide Updates - Specimen
) If the “external specimen id” is empty, then populate with the word “NULL</description>
        <field>Name</field>
        <formula>&quot;Null&quot;</formula>
        <name>Null External ID to Specimen Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OS_Billing_Policy_Data_to_Data_Avail</fullName>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Data Status to Data Available</description>
        <field>OSM_Billing_Policy_Data_Status__c</field>
        <literalValue>Data Available</literalValue>
        <name>OSM_Specimen BP Data Status Data Availab</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OSpecimen_Uncheck_Report_Dist_Ready</fullName>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <field>OSM_Report_Distribution_Ready__c</field>
        <literalValue>0</literalValue>
        <name>OSpecimen Uncheck Report Dist Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Specimen_Billing_Data_Missing</fullName>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Data Status to Data Missing</description>
        <field>OSM_Billing_Policy_Data_Status__c</field>
        <literalValue>Data Missing</literalValue>
        <name>OSM_Specimen BP Data Status Data Missing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_Order_Specimen_Discharge_Date</fullName>
        <description>Sprint 13: 17721 – Calculation Fields on Specimen</description>
        <field>OSM_Discharge_Date__c</field>
        <formula>OSM_Date_of_Collection__c</formula>
        <name>Set Order Specimen Discharge Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_Order_Specimen_Storage_Duration</fullName>
        <description>17721 – Calculation Fields on Specimen</description>
        <field>OSM_Specimen_Storage_Duration__c</field>
        <formula>OSM_Storage_Retrieval_Date__c -  OSM_Date_of_Collection__c</formula>
        <name>Set Order Specimen Storage Duration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_Specimen_Receipt_Date</fullName>
        <description>Sprint 38 &amp; 51: User Story 42859: Order Specimen - Populate Specimen Received Date</description>
        <field>OSM_Specimen_Receipt_Date__c</field>
        <formula>CreatedDate</formula>
        <name>OSM_Set_Specimen_Receipt_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Inherit_Discharge_Date</fullName>
        <description>Sprint 15</description>
        <field>Inherit_Discharge_Date__c</field>
        <literalValue>1</literalValue>
        <name>Set Inherit Discharge Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OSM OS Billing Policy NA</fullName>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>User Story 16855:Billing Policy Data Status Check and Initial Qualification Status Check, Specimen</description>
        <formula>AND ( NOT(OSM_Available_for_Processing__c ), ISPICKVAL(OSM_Billing_Policy_Data_Status__c ,&quot;&quot;), ISPICKVAL(OSM_Billing_Policy_Qualification_status__c ,&quot;&quot;), OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c , OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0, ISPICKVAL(OSM_Order__r.OSM_Status__c, &quot;Processing&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Calculate Billing Policy Days</fullName>
        <active>true</active>
        <formula>OR ( ISPICKVAL (OSM_Billing_Policy_Qualification_status__c , &quot;Disqualified&quot;),  AND (OR (ISPICKVAL(OSM_Billing_Policy_Data_Status__c, &quot;Data Available&quot;),ISPICKVAL ( OSM_Billing_Policy_Data_Status__c , &quot;Data Missing&quot;)), NOT ISBLANK ( OSM_Order__r.OSM_Signature_Date__c ), NOT ISBLANK (OSM_Discharge_Date__c ))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Clear Order Specimen Storage Duration</fullName>
        <actions>
            <name>OSMClear_Order_Specimen_Storage_Duration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.OSM_Date_of_Collection__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.OSM_Storage_Retrieval_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Clear storage duration when either OSM_Storage_Retrieval_Date__c or  OSM_Date_of_Collection__c is null</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OSpecimen_ReportDist_Ready</fullName>
        <actions>
            <name>OSM_Check_Specimen_Report_Dist_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <formula>IF( OSM_Available_for_Processing__c = True, AND(  NOT(ISBLANK(OSM_Date_of_Collection__c )),  NOT(ISBLANK(OSM_Specimen_Arrival_Case__c )) ) ,True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OSpecimen_Uncheck_ReportDist_Ready</fullName>
        <actions>
            <name>OSM_OSpecimen_Uncheck_Report_Dist_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <formula>NOT( IF( OSM_Available_for_Processing__c = True, AND(  NOT(ISBLANK(OSM_Date_of_Collection__c )),  NOT(ISBLANK(OSM_Specimen_Arrival_Case__c )) ) ,True ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Bill Qualification NA</fullName>
        <actions>
            <name>OSM_Bill_Policy_Qualification_Status_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>16856: :Billing Policy Qualification Status Check, Specimen</description>
        <formula>AND  ( OSM_Available_for_Processing__c = False, OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Data Available</fullName>
        <actions>
            <name>OSM_OS_Billing_Policy_Data_to_Data_Avail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>16855: Set to Data Available</description>
        <formula>AND ( OSM_Available_for_Processing__c, ISPICKVAL (OSM_Billing_Policy_Data_Status__c , &quot;&quot;), ISPICKVAL ( OSM_Billing_Policy_Qualification_status__c , &quot;&quot;), OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c, OR(  ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;LOA in place&quot; ),  AND(     OR(       ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;&quot; ),     ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;LOA refused&quot; )     ),     ISPICKVAL(OSM_Hospital_Status__c ,&quot;Non-Hospital&quot;)  ),  AND(  OR(  ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;&quot; ),  ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;LOA refused&quot; )  ),  OR(   ISPICKVAL(OSM_Hospital_Status__c , &quot;Inpatient&quot;),   ISPICKVAL(OSM_Hospital_Status__c , &quot;Outpatient&quot;)  ),   NOT(ISBLANK(OSM_Date_of_Collection__c )),   NOT(ISBLANK( OSM_Discharge_Date__c )),   NOT(ISBLANK( OSM_Storage_Retrieval_Date__c )) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Data Missing</fullName>
        <actions>
            <name>OSM_Order_Specimen_Billing_Data_Missing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND ( OSM_Available_for_Processing__c, ISPICKVAL (OSM_Billing_Policy_Data_Status__c , &quot;&quot;), ISPICKVAL ( OSM_Billing_Policy_Qualification_status__c , &quot;&quot;), OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c, OR(  ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;Follow-up required&quot; ),  AND(       ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;&quot; ),     ISPICKVAL(OSM_Hospital_Status__c ,&quot;&quot;)  ),  AND(  OR(  ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;&quot; ),  ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c,&quot;LOA refused&quot; )  ),  OR(   ISPICKVAL(OSM_Hospital_Status__c , &quot;Inpatient&quot;),   ISPICKVAL(OSM_Hospital_Status__c , &quot;Outpatient&quot;)  ),  OR(   ISBLANK(OSM_Date_of_Collection__c ),   ISBLANK( OSM_Discharge_Date__c ),   ISBLANK( OSM_Storage_Retrieval_Date__c )  ) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Data Status Data Available</fullName>
        <actions>
            <name>OSM_OS_Billing_Policy_Data_to_Data_Avail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Data Status to Data Available</description>
        <formula>AND  (  OSM_Available_for_Processing__c,  OSM_Total_Billing_Policy_NA__c = 0,  OSM_Total_Number_of_Order_Specimen_OLI__c = OSM_Number_of_OS_OLI_Billing_ready__c,  OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0,  NOT(ISBLANK(OSM_Potentially_Responsible_Account__c)), OR(    ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;LOA in place&quot; ),    AND(      OR(      ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;&quot; ),      ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;LOA refused&quot; )      ),    ISPICKVAL(OSM_Hospital_Status__c ,&quot;Non-Hospital&quot;)    ),    AND(      OR(      ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;&quot; ),      ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;LOA refused&quot; )      ),      OR(      ISPICKVAL(OSM_Hospital_Status__c , &quot;Inpatient&quot;),      ISPICKVAL(OSM_Hospital_Status__c , &quot;Outpatient&quot;)      ),    NOT(ISBLANK(OSM_Date_of_Collection__c )),    NOT(ISBLANK( OSM_Discharge_Date__c )),    NOT(ISBLANK( OSM_Storage_Retrieval_Date__c ))    )    )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Data Status Data Missing</fullName>
        <actions>
            <name>OSM_Order_Specimen_Billing_Data_Missing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Data Status to Data Missing</description>
        <formula>AND (  OSM_Available_for_Processing__c,  OSM_Total_Billing_Policy_NA__c = 0,  OSM_Total_Number_of_Order_Specimen_OLI__c = OSM_Number_of_OS_OLI_Billing_ready__c,  OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0,  NOT(ISBLANK(OSM_Potentially_Responsible_Account__c)), OR(  ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;Follow-up required&quot; ),    AND(    ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;&quot; ),    ISPICKVAL(OSM_Hospital_Status__c ,&quot;&quot;)    ),  AND(    OR(    ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;&quot; ),    ISPICKVAL(OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c,&quot;LOA refused&quot; )    ),    OR(    ISPICKVAL(OSM_Hospital_Status__c , &quot;Inpatient&quot;),    ISPICKVAL(OSM_Hospital_Status__c , &quot;Outpatient&quot;)    ),    OR(    ISBLANK(OSM_Date_of_Collection__c ),    ISBLANK( OSM_Discharge_Date__c ),    ISBLANK( OSM_Storage_Retrieval_Date__c )    )    )  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Data Status N%2FA 1A</fullName>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Data Status to N/A</description>
        <formula>AND( OSM_Total_Number_of_Order_Specimen_OLI__c = OSM_Total_Billing_Policy_NA__c, OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Data Status NA 1B</fullName>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Data Status to N/A</description>
        <formula>AND( NOT(OSM_Available_for_Processing__c ), OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c , OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Qual Status Disqualified</fullName>
        <actions>
            <name>OSM_Billing_Policy_Qualification_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16856 Set the Specimen Billing Policy Qualification Status to Disqualified</description>
        <formula>AND ( ISBLANK(TEXT(OSM_Billing_Policy_Qualification_status__c)), ISPICKVAL(OSM_Billing_Policy_Data_Status__c, &quot;Data Available&quot;), OSM_Available_for_Processing__c = TRUE, NOT (ISBLANK (OSM_Potentially_Responsible_Account__c )), OR (ISPICKVAL( OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c, &quot;LOA Refused&quot;),ISBLANK (TEXT( OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c))), OR ( ISPICKVAL (OSM_Hospital_Status__c , &quot;InPatient&quot;),ISPICKVAL (OSM_Hospital_Status__c , &quot;Outpatient&quot;)), NOT (ISBLANK (OSM_Date_of_Collection__c)), NOT (ISBLANK (OSM_Discharge_Date__c )), NOT (ISBLANK (OSM_Storage_Retrieval_Date__c )), OSM_Specimen_Storage_Duration__c &lt;= 30, OSM_Billing_Policy_Day_Calculation__c &lt; 14, OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c, OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Qual Status N%2FA 1A</fullName>
        <actions>
            <name>OSM_Bill_Policy_Qualification_Status_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Qualification to N/A</description>
        <formula>AND( OSM_Total_Number_of_Order_Specimen_OLI__c = OSM_Total_Billing_Policy_NA__c, OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0, ISPICKVAL(OSM_Billing_Policy_Qualification_status__c,&quot;&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Qual Status NA</fullName>
        <actions>
            <name>OSM_Bill_Policy_Qualification_Status_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16856 Update the Specimen Billing Policy Qualification Status to N/A</description>
        <formula>AND( ISPICKVAL(OSM_Billing_Policy_Data_Status__c, &quot;Data Available&quot;), ISBLANK(TEXT(OSM_Billing_Policy_Qualification_status__c)), OSM_Available_for_Processing__c = False, OSM_Total_Number_of_Order_Specimen_OLI__c = OSM_Number_of_OS_OLI_Billing_ready__c, OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Qual Status NA 1B</fullName>
        <actions>
            <name>OSM_Bill_Policy_Qualification_Status_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16855 Set the Specimen Billing Policy Qualification Status to N/A</description>
        <formula>AND( NOT(OSM_Available_for_Processing__c ), ISPICKVAL(OSM_Billing_Policy_Data_Status__c ,&quot;&quot;), ISPICKVAL(OSM_Billing_Policy_Qualification_status__c ,&quot;&quot;), OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c , OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Policy Qual Status Qualified</fullName>
        <actions>
            <name>OSM_Billing_Policy_Qualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 29 - Story 16856 Set the Specimen Billing Policy Qualification Status to N/A</description>
        <formula>AND( OSM_Available_for_Processing__c = TRUE, OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c, OSM_Total_Number_of_Order_Specimen_OLI__c &gt; 0, ISBLANK(TEXT(OSM_Billing_Policy_Qualification_status__c)),  ISPICKVAL(OSM_Billing_Policy_Data_Status__c, &quot;Data Available&quot;),  OR (  ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c , &quot;LOA in Place&quot;), AND (OR (ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c , &quot;LOA refused&quot;), ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c , &quot;&quot;)),  ISPICKVAL (OSM_Hospital_Status__c, &quot;Non-Hospital&quot;)), AND (OR (ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c , &quot;LOA refused&quot;), ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Letter_of_Agreement__c , &quot;&quot;)), OR ( ISPICKVAL (OSM_Hospital_Status__c, &quot;InPatient&quot;), ISPICKVAL (OSM_Hospital_Status__c, &quot;Outpatient&quot;)), NOT (ISBLANK  (OSM_Date_of_Collection__c )), NOT (ISBLANK ( OSM_Discharge_Date__c )), NOT (ISBLANK ( OSM_Storage_Retrieval_Date__c )), OR ( OSM_Specimen_Storage_Duration__c  &gt; 30,  AND (OSM_Specimen_Storage_Duration__c  &lt;= 30,  OSM_Billing_Policy_Day_Calculation__c &gt;= 14))) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Qualification</fullName>
        <actions>
            <name>OSM_Billing_Policy_Qualification_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND  ( OSM_Available_for_Processing__c = TRUE, NOT (ISBLANK (OSM_Potentially_Responsible_Account__c )), OR (ISPICKVAL( OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c , &quot;LOA Refused&quot;),ISBLANK (TEXT( OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c))), OR ( ISPICKVAL (OSM_Hospital_Status__c , &quot;InPatient&quot;),ISPICKVAL (OSM_Hospital_Status__c , &quot;Outpatient&quot;)), NOT (ISBLANK (OSM_Date_of_Collection__c)), NOT (ISBLANK (OSM_Discharge_Date__c )), NOT (ISBLANK (OSM_Storage_Retrieval_Date__c )), OSM_Specimen_Storage_Duration__c &lt; 30,  OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Specimen Billing Qualifiied</fullName>
        <actions>
            <name>OSM_Billing_Policy_Qualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND  (  OSM_Available_for_Processing__c = TRUE, OSM_Total_Number_of_Order_Specimen_OLI__c =  OSM_Number_of_OS_OLI_Billing_ready__c ,  OR (   ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c , &quot;LOA in Place&quot;),  AND (OR (ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c , &quot;LOA refused&quot;), ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c , &quot;&quot;)),  ISPICKVAL (OSM_Hospital_Status__c, &quot;Non-Hospital&quot;)),  AND (OR (ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c , &quot;LOA refused&quot;), ISPICKVAL ( OSM_Potentially_Responsible_Account__r.OSM_Account__r.OSM_Letter_of_Agreement__c , &quot;&quot;)), OR ( ISPICKVAL (OSM_Hospital_Status__c, &quot;InPatient&quot;), ISPICKVAL (OSM_Hospital_Status__c, &quot;Outpatient&quot;)), NOT (ISBLANK  (OSM_Date_of_Collection__c )), NOT (ISBLANK ( OSM_Discharge_Date__c )), NOT (ISBLANK ( OSM_Storage_Retrieval_Date__c )), OR ( OSM_Specimen_Storage_Duration__c  &gt;= 30,  AND (OSM_Specimen_Storage_Duration__c  &lt; 30))) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Populate External Specimen ID</fullName>
        <actions>
            <name>OSM_External_ID_to_Specimen_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.OSM_External_Specimen_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Feb Housekeeping - 11789 – Housekeeping Config Guide Updates - Specimen</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Populate Null External Specimen ID</fullName>
        <actions>
            <name>OSM_Null_External_ID_to_Specimen_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.OSM_External_Specimen_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Feb Housekeeping - 11789 – Housekeeping Config Guide Updates - Specimen</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Set Order Specimen Discharge Date</fullName>
        <actions>
            <name>OSM_Set_Order_Specimen_Discharge_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Inherit_Discharge_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR 3</booleanFilter>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.OSM_Hospital_Status__c</field>
            <operation>equals</operation>
            <value>Outpatient</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.OSM_Discharge_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.Inherit_Discharge_Date__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>17721 – Calculation Fields on Specimen</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Set Order Specimen Storage Duration</fullName>
        <actions>
            <name>OSM_Set_Order_Specimen_Storage_Duration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.OSM_Date_of_Collection__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Order_Specimen__c.OSM_Storage_Retrieval_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>17721 – Calculation Fields on Specimen</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Set_Specimen_Receipt_Date</fullName>
        <actions>
            <name>OSM_Set_Specimen_Receipt_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 38 &amp; 51: User Story 42859: Order Specimen - Populate Specimen Received Date</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
