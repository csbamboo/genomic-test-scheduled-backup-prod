<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_Tracking_Number_Update</fullName>
        <field>Name</field>
        <formula>&quot;HD&quot; &amp; 

IF(LEN(TEXT(MONTH( TODAY() ))) &lt; 2, 
&quot;0&quot; &amp; TEXT(MONTH( TODAY() )) , 
TEXT(MONTH( TODAY() ))) &amp; 

IF(LEN(TEXT(DAY( TODAY() ))) &lt; 2, 
&quot;0&quot; &amp; TEXT(DAY( TODAY() )) , 
TEXT(DAY( TODAY() ))) &amp; 

RIGHT(TEXT(YEAR(TODAY())),2) &amp;

RIGHT(LEFT(TEXT(SQRT(VALUE(
LEFT(RIGHT(TEXT(NOW()), 9),2) &amp;  LEFT(RIGHT(TEXT(NOW()), 6),2) &amp; LEFT(RIGHT(TEXT(NOW()), 3),2))  )),11),3)</formula>
        <name>OSM Tracking Number Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OSM Tracking Hand Delivery</fullName>
        <actions>
            <name>OSM_Tracking_Number_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Package__c.OSM_Carrier__c</field>
            <operation>equals</operation>
            <value>Hand-Delivery</value>
        </criteriaItems>
        <description>Updates the Tracking Number if the Carrier is Hand Delivery</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
