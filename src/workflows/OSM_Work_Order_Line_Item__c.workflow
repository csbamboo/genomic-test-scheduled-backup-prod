<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_Stamp_Order_Product_Data_Entry</fullName>
        <description>20210 - Work Order Status- Processing</description>
        <field>OSM_Order_Product_Data_Entry_Status__c</field>
        <formula>TEXT(OSM_Order_Product__r.OSM_Data_Entry_Status__c)</formula>
        <name>Stamp Order Product Data Entry Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OSM_Stamp_OrderProduct_DataEntryStatus</fullName>
        <actions>
            <name>OSM_Stamp_Order_Product_Data_Entry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>20210 - Work Order Status- Processing</description>
        <formula>NOT(ISNULL(OSM_Order_Product__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
