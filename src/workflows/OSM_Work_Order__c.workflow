<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_New_Work_Order_State_Active</fullName>
        <description>14531 - Work Order Status to State Mapping (UPDATES for new WO Object)</description>
        <field>OSM_State__c</field>
        <literalValue>Active</literalValue>
        <name>OSM_New Work Order State Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_New_Work_Order_State_Closed</fullName>
        <description>14531 - Work Order Status to State Mapping (UPDATES for new WO Object)</description>
        <field>OSM_State__c</field>
        <literalValue>Closed</literalValue>
        <name>OSM_New Work Order State Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Date_Time_Closed_to_Now</fullName>
        <description>User Story 30546: Work Order Status - Complete</description>
        <field>OSM_Date_Time_Closed__c</field>
        <formula>NOW()</formula>
        <name>Update Date Time Closed to Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_WO_Status_to_Complete</fullName>
        <description>User Story 30546: Work Order Status - Complete</description>
        <field>OSM_Status__c</field>
        <literalValue>Complete</literalValue>
        <name>Update WO Status to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_WO_Active_Date_when_State_is_Active</fullName>
        <description>27334 - KPI Incoming Specimen</description>
        <field>OSM_Active_Date__c</field>
        <formula>NOW()</formula>
        <name>WO Active Date when State is Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Work_Order_Cancellation</fullName>
        <description>14549 - State Status Update - WO Cancelation</description>
        <field>OSM_Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>OSM Work Order Cancellation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Work_Order_Status_to_Processing</fullName>
        <description>Change work order status to processing</description>
        <field>OSM_Status__c</field>
        <literalValue>Processing</literalValue>
        <name>OSM Work Order Status to Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OSM New Work Order State Active</fullName>
        <actions>
            <name>OSM_New_Work_Order_State_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Work_Order__c.OSM_Status__c</field>
            <operation>equals</operation>
            <value>Processing</value>
        </criteriaItems>
        <description>14531 - Work Order Status to State Mapping (UPDATES for new WO Object)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM New Work Order State Closed</fullName>
        <actions>
            <name>OSM_New_Work_Order_State_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Work_Order__c.OSM_Status__c</field>
            <operation>equals</operation>
            <value>Canceled,Complete</value>
        </criteriaItems>
        <description>14531 - Work Order Status to State Mapping (UPDATES for new WO Object)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set to Processing based on WOLI</fullName>
        <actions>
            <name>OSM_Work_Order_Status_to_Processing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>20210 - Work Order Status- Processing</description>
        <formula>OR(  AND(   OSM_Number_of_Work_Order_Line_Items__c &gt; 0,   OSM_Number_of_Work_Order_Line_Items__c =   OSM_Number_of_Processing_WOLI__c  ),  OSM_Override_Status_to_Active__c  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Populate Active Date when State is Active</fullName>
        <actions>
            <name>OSM_WO_Active_Date_when_State_is_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Work_Order__c.OSM_State__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>27334 - KPI Incoming Specimen</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Update Date Time when WO Status is Closed</fullName>
        <actions>
            <name>OSM_Update_Date_Time_Closed_to_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Work_Order__c.OSM_Status__c</field>
            <operation>equals</operation>
            <value>Canceled,Complete</value>
        </criteriaItems>
        <description>User Story 30546: Work Order Status - Complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Update Work order status to Complete</fullName>
        <actions>
            <name>OSM_Update_WO_Status_to_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OSM_Work_Order__c.OSM_Number_of_Work_Order_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Work_Order__c.OSM_Number_of_Incomplete_WOOLI__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>OSM_Work_Order__c.OSM_Status__c</field>
            <operation>notEqual</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>User Story 30546: Work Order Status - Complete</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Work Order Cancellation</fullName>
        <actions>
            <name>OSM_New_Work_Order_State_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Work_Order_Cancellation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Cancels work order if conditions are met.
14549 - State Status Update - WO Cancelation</description>
        <formula>AND (NOT ISPICKVAL(OSM_State__c,&apos;Closed&apos;), OSM_Number_of_Work_Order_Line_Items__c &gt; 0, OR (ISPICKVAL(OSM_Order__r.OSM_Status__c,&apos;Canceled&apos;), OSM_Number_of_Work_Order_Line_Items__c =  OSM_Number_of_Cancelled_WOLI__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
