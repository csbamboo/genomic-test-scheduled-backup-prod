<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>GHI_Portal_Order_Confirmation</fullName>
        <description>GHI Portal Order Confirmation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>aanguyen@genomichealth.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>OSM_Genomic_Health_Templates/GHI_Portal_Confirmation_Email</template>
    </alerts>
    <alerts>
        <fullName>OSM_Send_Wire_Transfer_Email_Order</fullName>
        <ccEmails>vnaidu@genomichealth.com</ccEmails>
        <description>Send Wire Transfer Email (Order)</description>
        <protected>false</protected>
        <recipients>
            <recipient>jsunkara@genomichealth.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pbhandari@genomichealth.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ssaxon@genomichealth.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OSM_Genomic_Health_Templates/Wired_Transfer_Order_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>GHI_Portal_Default_submission_type</fullName>
        <description>44430 - Default submission type to &quot;Resubmission - Confirmed&quot;</description>
        <field>OSM_Triage_Outcome__c</field>
        <literalValue>Resubmission - Confirmed</literalValue>
        <name>Default submission type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GHI_Portal_Snapshot_Update</fullName>
        <field>GHI_Portal_Snapshot__c</field>
        <formula>&quot;Primary Insurance Name: &quot;&amp; GHI_Portal_Primary_Insurance_Name__c &amp; BR() &amp;
&quot;Is the patient part of a medical group or Independent Practice Association (IPA)?: &quot;&amp; TEXT(GHI_Portal_Primary_Medical_Group_or_IPA__c) &amp; BR() &amp;
&quot;Primary Medical Group or IPA Name: &quot;&amp;  GHI_Portal_Primary_Med_Group_IPA_Name__c &amp; BR() &amp;
&quot;Primary Phone: &quot;&amp; GHI_Portal_Primary_Portal_Phone__c &amp; BR() &amp; 
&quot;Primary Address: &quot;&amp; GHI_Portal_Primary_Portal_Address__c &amp; BR() &amp;
&quot;Primary City: &quot;&amp; GHI_Portal_Primary_Portal_City__c &amp; BR() &amp;
&quot;Primary State: &quot;&amp; GHI_Portal_Primary_Portal_State__c &amp; BR() &amp;
&quot;Primary Zip Code: &quot;&amp; GHI_Portal_Primary_Portal_Zip_Code__c &amp; BR() &amp;
&quot;Primary Member ID: &quot;&amp; GHI_Portal_Primary_Member_ID__c &amp; BR() &amp;
&quot;Primary Portal Group Number: &quot;&amp; GHI_Portal_Primary_Portal_Group_Number__c &amp; BR() &amp;
&quot;Primary Portal PA Number: &quot;&amp; GHI_Portal_Primary_Portal_PA_Number__c &amp; BR() &amp;
&quot;Primary Subscriber Name: &quot;&amp; GHI_Portal_Primary_Subscriber_Name__c &amp; BR() &amp;
&quot;Primary Relationship to Subscriber: &quot;&amp; TEXT(GHI_Portal_Primary_Portal_Relation_Subs__c) &amp; BR() &amp;
&quot;Primary Relationship Gender: &quot;&amp; TEXT(GHI_Portal_Primary_Portal_Rel_Gender__c) &amp; BR() &amp;
&quot;Primary Relationship DOB: &quot;&amp; TEXT(GHI_Portal_Primary_Portal_Rel_DOB__c) &amp; BR() &amp;
&quot;Secondary Bill Type: &quot;&amp; TEXT(GHI_Portal_Secondary_Bill_Type__c) &amp; BR() &amp;
&quot;Secondary Insurance Name: &quot;&amp; GHI_Portal_Secondary_Insurance_Name__c &amp; BR() &amp;
&quot;Secondary Hospitalization Status: &quot;&amp; TEXT(GHI_Portal_Secondary_Hospitilization__c) &amp; BR() &amp;
&quot;Secondary Date of Discharge: &quot;&amp; TEXT(GHI_Portal_Sec_Date_of_Discharge__c) &amp; BR() &amp;
&quot;Secondary Medical Group or IPA Name: &quot;&amp; TEXT(GHI_Portal_Secondar_Med_Group_or_IPA__c) &amp; BR() &amp;
&quot;Secondary Phone: &quot;&amp; GHI_Portal_Secondary_Portal_Phone__c &amp; BR() &amp;
&quot;Secondary Address: &quot;&amp; GHI_Portal_Secondary_Portal_Address__c &amp; BR() &amp;
&quot;Secondary City: &quot;&amp; GHI_Portal_Secondary_Portal_City__c &amp; BR() &amp;
&quot;Secondary State: &quot;&amp; GHI_Portal_Secondary_Portal_State__c &amp; BR() &amp;
&quot;Secondary Zip Code: &quot;&amp; GHI_Portal_Secondary_Portal_Zip_Code__c &amp; BR() &amp;
&quot;Secondary Member ID: &quot;&amp; GHI_Portal_Secondary_Portal_Member_ID__c &amp; BR() &amp;
&quot;Secondary Group Number: &quot;&amp; GHI_Portal_Secondary_Group_Number__c &amp; BR() &amp;
&quot;Secondary Portal PA Number: &quot;&amp; GHI_Portal_Secondary_PA_Number__c &amp; BR() &amp;
&quot;Secondary Subscriber Name: &quot;&amp; GHI_Portal_Secondary_Subscriber_Name__c &amp; BR() &amp;
&quot;Secondary Relationship to Subscriber: &quot;&amp; TEXT(GHI_Portal_Secondary_Relation_to_Subs__c) &amp; BR() &amp;
&quot;Secondary Relationship Gender: &quot;&amp; TEXT(GHI_Portal_Secondary_Relationship_Gender__c) &amp; BR() &amp;
&quot;Secondary Relationship DOB: &quot;&amp; TEXT(GHI_Portal_Secondary_Relationship_DOB__c)</formula>
        <name>GHI Portal Snapshot Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_Data_Verification</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Order_Data_Verification</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Data Verification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_Data_Verification_Complete</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Order_Data_Verification_Complete</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Data Verification Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_Geneva_Order_Intake</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Geneva_Order_Intake</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Geneva Order Intake</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_Intl_RWC_Order_Intake</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Intl_RWC_Order_Intake</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Intl RWC - Order Intake</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_Order_Complete</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Order_Complete</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Order Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_Order_Intake_Complete</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Order_Intake_Complete</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Order Intake Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_Prostate_Order_Intake</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Prostate_Order_Intake</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Prostate - Order Intake</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Empty_Amount_Paid_Order</fullName>
        <field>OSM_Amount_Paid__c</field>
        <name>OSM Empty Amount Paid (Order)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Empty_Country_Order</fullName>
        <field>OSM_Country__c</field>
        <name>OSM Empty Country (Order)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Empty_Sending_Bank_Order</fullName>
        <field>OSM_Sending_Bank_Transfer_Number__c</field>
        <name>OSM Empty Sending Bank (Order)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Flag_Multiple_Primaries_Confirmed</fullName>
        <description>Sprint 9 - 10680 – Multiple Primaries Confirmed - Autofill</description>
        <field>OSM_Multiple_Primaries__c</field>
        <literalValue>1</literalValue>
        <name>Flag Multiple Primaries Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_BI_Ready_is_False</fullName>
        <field>OSM_BI_Ready__c</field>
        <literalValue>0</literalValue>
        <name>Order BI Ready is False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_BI_Ready_is_True</fullName>
        <field>OSM_BI_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Order BI Ready is True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Cancellation_from_OLI</fullName>
        <description>14533 - State Status Update - Cancel all OLIs on an Order Line Ite</description>
        <field>OSM_Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>OSM_Order Cancellation from OLI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Intake_Assigned_True</fullName>
        <description>User Story #29828 Order Assignment to Queues. Once Order intake assigned to Queue this Check box will be set to True</description>
        <field>OSM_Order_Intake_Assigned__c</field>
        <literalValue>1</literalValue>
        <name>Order Intake Assigned True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Patient_Contact_Not_Allowed</fullName>
        <description>User Story 43175:PB - Default &apos;Patient Contact&apos; for Prostate</description>
        <field>Patient_Contact__c</field>
        <literalValue>Not Allowed</literalValue>
        <name>Order Patient Contact Not Allowed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Prior_State</fullName>
        <field>OSM_Prior_State__c</field>
        <formula>TEXT(PRIORVALUE( Status ))</formula>
        <name>Order Prior State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Specimen_Not_Ready</fullName>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <field>OSM_Specimen_Retrieval_Ready__c</field>
        <literalValue>0</literalValue>
        <name>Order Specimen Not Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Specimen_Ready</fullName>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <field>OSM_Specimen_Retrieval_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Order Specimen Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_State_Set_to_Active</fullName>
        <description>Sprint 9 - 10863 - Order Status to State Mapping, 10866 - Work Order Status to State Mapping</description>
        <field>Status</field>
        <literalValue>Active</literalValue>
        <name>Order State Set to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_State_Set_to_Closed</fullName>
        <description>Sprint 9 - 10863 - Order Status to State Mapping, 10866 - Work Order Status to State Mapping</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Order State Set to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Owner_Domestic_Order_Intake</fullName>
        <description>User Story #29828 Order Assignment to Queues. Assign the Order to Domestic Order Intake Queue.</description>
        <field>OwnerId</field>
        <lookupValue>OSM_Domestic_Order_Intake</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Order Owner Domestic Order Intake</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_SetOrderName</fullName>
        <field>Name</field>
        <formula>RecordType.Name &amp; &quot; - &quot; &amp;  
OSM_Patient__r.FirstName &amp; &quot; &quot; &amp;
OSM_Patient__r.LastName</formula>
        <name>SetOrderName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_Order_Start_Date</fullName>
        <description>44199 - Order Effective Date Update</description>
        <field>EffectiveDate</field>
        <formula>TODAY()</formula>
        <name>Set Order Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_Triage_outcome_to_new</fullName>
        <field>OSM_Triage_Outcome__c</field>
        <literalValue>New</literalValue>
        <name>Set Triage outcome to new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Stamp_Order_Intake_User_Date_Time</fullName>
        <field>OSM_Order_Intake_User_Date_and_Time__c</field>
        <formula>NOW()</formula>
        <name>Stamp Order Intake User Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Unflag_Multiple_Primaries_Confirmed</fullName>
        <description>Sprint 9 - 10680 – Multiple Primaries Confirmed - Autofill</description>
        <field>OSM_Multiple_Primaries__c</field>
        <literalValue>0</literalValue>
        <name>Unflag Multiple Primaries Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_PreBillingSvcOpt_to_Automated</fullName>
        <field>OSM_Pre_Billing_Service_Option__c</field>
        <literalValue>Automated BI</literalValue>
        <name>Update Pre-BillingSvcOpt to Automated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Pre_BillingSvcOpt_to_No_BI</fullName>
        <field>OSM_Pre_Billing_Service_Option__c</field>
        <literalValue>No BI</literalValue>
        <name>Update Pre-BillingSvcOpt to No BI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSm_Empty_OLI_Order</fullName>
        <field>OSM_Order_Line_Items__c</field>
        <name>OSm Empty OLI (Order)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Portal_Order_Record_Type_to_Order</fullName>
        <description>Update Portal Order Record Type to Order Record Type if OSM_Order_Status__c is Order Intake or Closed</description>
        <field>RecordTypeId</field>
        <lookupValue>OSM_Order</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Portal Order Record Type to Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pre_BillingSvcOpt_to_Blank</fullName>
        <field>OSM_Pre_Billing_Service_Option__c</field>
        <name>Update Pre-BillingSvcOpt to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Signature_Date</fullName>
        <description>This updates the Signature Date once Order Status changes to &quot;Order Intake&quot;</description>
        <field>OSM_Signature_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Signature Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Single_Checkbox_to_True</fullName>
        <field>GHI_Portal_Single_Email__c</field>
        <literalValue>1</literalValue>
        <name>Update Single Checkbox to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>GHI_Portal_Conga_Create_Fax_Form</fullName>
        <apiVersion>34.0</apiVersion>
        <description>https://workflow.congamerge.com/OBMListener.ashx  Old Url</description>
        <endpointUrl>https://workflow.appextremes.com/apps/Conga/PMWorkflow.aspx</endpointUrl>
        <fields>GHI_Portal_Conga_Fax_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>system.admin@genomichealth.com</integrationUser>
        <name>GHI_Portal_Conga_Create_Fax_Form</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>GHI_Portal_Conga_Create_SR_Form</fullName>
        <apiVersion>34.0</apiVersion>
        <description>https://workflow.congamerge.com/OBMListener.ashx Old url version 8 
https://workflow.appextremes.com/apps/Conga/PMWorkflow.aspx version 7</description>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>GHI_Portal_Conga_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>system.admin@genomichealth.com</integrationUser>
        <name>GHI_Portal_Conga_Create_SR_Form</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>GHI Portal Change Order Record Type</fullName>
        <actions>
            <name>GHI_Portal_Order_Confirmation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Portal_Order_Record_Type_to_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GHI_Portal_Conga_Create_Fax_Form</name>
            <type>OutboundMessage</type>
        </actions>
        <actions>
            <name>GHI_Portal_Conga_Create_SR_Form</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When an Order is submitted in portal, the Portal Order record type will change to Order record type</description>
        <formula>RecordType.DeveloperName == &quot;GHI_Portal_Order_Layout&quot;  &amp;&amp; ISPICKVAL(OSM_Status__c , &quot;Order Intake&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GHI Portal Resubmitted Orders</fullName>
        <actions>
            <name>GHI_Portal_Default_submission_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.GHI_Portal_Resubmitted_Order__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Portal Order</value>
        </criteriaItems>
        <description>These are for orders resubmitted through portal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GHI Portal Signature Date upon Order Intake</fullName>
        <actions>
            <name>Update_Signature_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>43092 - Update Signature Date once status changes to Order Intake</description>
        <formula>AND (ISCHANGED(OSM_Status__c ),      TEXT(OSM_Status__c) = &quot;Order Intake&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GHI Portal Snapshot Update</fullName>
        <actions>
            <name>GHI_Portal_Snapshot_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(OSM_Channel__c, &quot;Portal&quot;), RecordType.DeveloperName == &quot;GHI_Portal_Order_Layout&quot;,  ISCHANGED( OSM_Status__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Empty Wired Section %28Order%29</fullName>
        <actions>
            <name>OSM_Empty_Amount_Paid_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Empty_Country_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Empty_Sending_Bank_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSm_Empty_OLI_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.OSM_Bill_Type__c</field>
            <operation>notEqual</operation>
            <value>Patient Pre-Pay</value>
        </criteriaItems>
        <description>Values in Wired Section should be blank if Bill Type is changed from Patient Pre-Pay to other Bill Types</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Send Wire Transfer Email %28Order%29</fullName>
        <actions>
            <name>OSM_Send_Wire_Transfer_Email_Order</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.OSM_Payment_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Bill_Type__c</field>
            <operation>equals</operation>
            <value>Patient Pre-Pay</value>
        </criteriaItems>
        <description>Send wire transfer email if Payment Received checkbox is true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update Pre-Billing Service Option to Automated BI</fullName>
        <actions>
            <name>OSM_Update_PreBillingSvcOpt_to_Automated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Order_Location__c</field>
            <operation>equals</operation>
            <value>Domestic</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Bill_Type__c</field>
            <operation>equals</operation>
            <value>Private Insurance,Medicare,Medicaid</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update Pre-Billing Service Option to Blank</fullName>
        <actions>
            <name>Update_Pre_BillingSvcOpt_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND (3 or 4))</booleanFilter>
        <criteriaItems>
            <field>Order.Order_Location__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Order_Location__c</field>
            <operation>equals</operation>
            <value>Domestic</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Bill_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Bill_Type__c</field>
            <operation>equals</operation>
            <value>Uninsured Patient</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update Pre-Billing Service Option to No BI</fullName>
        <actions>
            <name>OSM_Update_Pre_BillingSvcOpt_to_No_BI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Order.Order_Location__c</field>
            <operation>equals</operation>
            <value>International</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Bill_Type__c</field>
            <operation>equals</operation>
            <value>Bill Account,Patient Pre-Pay,Genomic Health</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Order_Location__c</field>
            <operation>equals</operation>
            <value>Domestic</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Data_Verification</fullName>
        <actions>
            <name>OSM_Assign_Data_Verification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND(  (RecordType.Name== &quot;Order&quot;),  (TEXT(OSM_Status__c) == &quot;Processing&quot;),  (OSM_OLI_Data_Verification_Count__c &gt;= 1)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Data_Verification_Complete</fullName>
        <actions>
            <name>OSM_Assign_Data_Verification_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND( (RecordType.Name== &quot;Order&quot;),  (TEXT(OSM_Status__c) == &quot;Processing&quot;), (OSM_OLI_Data_Verification_Comp_count__c ==  OSM_Order_Line_Item_Count__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Domestic_Order_Intake_Fax</fullName>
        <actions>
            <name>OSM_Order_Intake_Assigned_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Owner_Domestic_Order_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Usert Story #29828 Order Assignment to Queues. Default Queue</description>
        <formula>OR( AND ( !OSM_Order_Intake_Assigned__c,           (TEXT(OSM_Channel__c) != &quot;Paper&quot;),           (TEXT(OSM_Channel__c) !=  &quot;Portal&quot;),           (RecordType.Name== &quot;Order&quot;),           (TEXT(OSM_Status__c)== &quot;Order Intake&quot;),           !ISPICKVAL( OSM_Product__c , &quot;Prostate&quot;),           (TEXT($User.OSM_User_Team__c) == &quot;Domestic&quot;)         ),         AND( (RecordType.Name== &quot;Order&quot;),               OR( (TEXT(OSM_Product__c) == null),                   ISPICKVAL(OSM_Channel__c ,&quot;Unknown&quot;),                   (TEXT(OSM_Channel__c) ==  null)                )              )   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Domestic_Order_Intake_Online</fullName>
        <actions>
            <name>OSM_Order_Intake_Assigned_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Owner_Domestic_Order_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Usert Story #29828 Order Assignment to Queues.</description>
        <formula>AND ( !OSM_Order_Intake_Assigned__c,           ISPICKVAL(OSM_Channel__c ,&quot;Portal&quot;),           (RecordType.Name== &quot;Order&quot;),           (TEXT(OSM_Status__c)== &quot;Order Intake&quot;),           !ISPICKVAL( OSM_Product__c , &quot;Prostate&quot;),           (TEXT(OSM_Product__c )!= null),            IF (ISNULL(OSM_Ordering_HCO__r.OSM_Customer_Service_Team__c),        False,    OSM_Ordering_HCO__r.OSM_Customer_Service_Team__c  == &quot;Domestic&quot;  )            )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Geneva_Order_Intake_Fax</fullName>
        <actions>
            <name>OSM_Assign_Geneva_Order_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Order_Intake_Assigned_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND(  (RecordType.Name== &quot;Order&quot;),  (!OSM_Order_Intake_Assigned__c ), (TEXT(OSM_Status__c) == &quot;Order Intake&quot;), (TEXT(OSM_Channel__c) == &quot;Fax&quot;),  (TEXT(CreatedBy.OSM_User_Team__c)==&quot;Intl Geneva&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Geneva_Order_Intake_Online</fullName>
        <actions>
            <name>OSM_Assign_Geneva_Order_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Order_Intake_Assigned_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND(  (RecordType.Name== &quot;Order&quot;),  (!OSM_Order_Intake_Assigned__c ), (TEXT(OSM_Status__c) == &quot;Order Intake&quot;), (TEXT(OSM_Channel__c) == &quot;Portal&quot;),  IF (ISNULL(OSM_Ordering_HCO__r.OSM_Customer_Service_Team__c),        True,   OSM_Ordering_HCO__r.OSM_Customer_Service_Team__c == &quot;GVA&quot;   )   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Intl_RWC_Order_Intake_Fax</fullName>
        <actions>
            <name>OSM_Assign_Intl_RWC_Order_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Order_Intake_Assigned_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND ( (RecordType.Name== &quot;Order&quot;), (!OSM_Order_Intake_Assigned__c), (TEXT(OSM_Status__c) == &quot;Order Intake&quot;), (TEXT(OSM_Channel__c)== &quot;Fax&quot;), (TEXT(CreatedBy.OSM_User_Team__c) == &quot;Intl Redwood City&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Intl_RWC_Order_Intake_Online</fullName>
        <actions>
            <name>OSM_Assign_Intl_RWC_Order_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Order_Intake_Assigned_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND(      (RecordType.Name== &quot;Order&quot;),      (!OSM_Order_Intake_Assigned__c ),     (TEXT(OSM_Status__c) == &quot;Order Intake&quot;),     (TEXT(OSM_Channel__c) == &quot;Portal&quot;),      IF (ISNULL(OSM_Ordering_HCO__r.OSM_Customer_Service_Team__c),        True,   OSM_Ordering_HCO__r.OSM_Customer_Service_Team__c == &quot;RWC&quot;   ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Multiple Primaries confirmed Autofill</fullName>
        <actions>
            <name>OSM_Flag_Multiple_Primaries_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.OSM_Number_of_Multiple_Primaries__c</field>
            <operation>greaterThan</operation>
            <value>1</value>
        </criteriaItems>
        <description>Sprint 9 - 10680 – Multiple Primaries Confirmed - Autofill</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Multiple Primaries confirmed uncheck</fullName>
        <actions>
            <name>OSM_Unflag_Multiple_Primaries_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Order.OSM_Number_of_Multiple_Primaries__c</field>
            <operation>lessOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Number_of_Multiple_Primaries__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sprint 9 - 10680 – Multiple Primaries Confirmed - Autofill</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order Cancellation from OLI</fullName>
        <actions>
            <name>OSM_Order_Cancellation_from_OLI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>14533 - State Status Update - Cancel all OLIs on an Order Line Item.</description>
        <formula>AND  (NOT ISPICKVAL( OSM_Status__c,&apos;Closed&apos;), OSM_Order_Line_Item_Count__c  &gt; 0,  OSM_Order_Line_Item_Count__c =  OSM_Number_of_Cancelled_OLI__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_BI_Not_Ready</fullName>
        <actions>
            <name>OSM_Order_BI_Ready_is_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT (AND(       (NOT(ISNULL(Id))),       (TEXT(OSM_State__c) = &quot;Active&quot;),        NOT(OSM_BI_Ready__c),       (NOT(TEXT(OSM_Product__c) = &quot;Unknown&quot;)),       (NOT(ISBLANK(TEXT(OSM_Product__c)))),       (NOT(ISBLANK(OSM_Patient_Last_Name__c ))),      (OSM_Ordering_Role_BI_Ready_Code__c &gt; 0),      IF(TEXT(Order_Location__c) = &quot;Domestic&quot;,  	 AND(  	    NOT(ISBLANK(OSM_Patient_DOB__c)),  	    NOT(ISBLANK(TEXT(OSM_Patient_Gender__c)))  	    ), TRUE       )    ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_BI_Ready</fullName>
        <actions>
            <name>OSM_Order_BI_Ready_is_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(       (NOT(ISNULL(Id))),       (TEXT(OSM_State__c) = &quot;Active&quot;),        NOT(OSM_BI_Ready__c),       (NOT(TEXT(OSM_Product__c) = &quot;Unknown&quot;)),       (NOT(ISBLANK(TEXT(OSM_Product__c)))),       (NOT(ISBLANK(OSM_Patient_Last_Name__c ))),      ( OSM_Ordering_Role_BI_Ready_Code__c  &gt; 0),      IF(TEXT(Order_Location__c) = &quot;Domestic&quot;,  	 AND(  	    NOT(ISBLANK(OSM_Patient_DOB__c)),  	    NOT(ISBLANK(TEXT(OSM_Patient_Gender__c)))  	    ), TRUE       )    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Complete</fullName>
        <actions>
            <name>OSM_Assign_Order_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND(  (RecordType.Name== &quot;Order&quot;),  OR ((TEXT(OSM_Status__c) == &quot;Closed&quot;),      (TEXT(OSM_Status__c) == &quot;Canceled&quot;)     ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Default_Patient_Contact_for_Prostate</fullName>
        <actions>
            <name>OSM_Order_Patient_Contact_Not_Allowed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.OSM_Product__c</field>
            <operation>equals</operation>
            <value>Prostate</value>
        </criteriaItems>
        <description>User Story 43175:PB - Default &apos;Patient Contact&apos; for Prostate</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Intake_Complete</fullName>
        <actions>
            <name>OSM_Assign_Order_Intake_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND( (RecordType.Name== &quot;Order&quot;), (TEXT(OSM_Status__c) == &quot;Processing&quot;), (OSM_OLI_Data_Verification_Count__c==0) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Intake_User_Date_Time</fullName>
        <actions>
            <name>OSM_Stamp_Order_Intake_User_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.OSM_Order_Intake_User__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Prior_State</fullName>
        <actions>
            <name>OSM_Order_Prior_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Set_Name</fullName>
        <actions>
            <name>OSM_SetOrderName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Specimen_Not_Ready</fullName>
        <actions>
            <name>OSM_Order_Specimen_Not_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <formula>AND(  OSM_Specimen_Retrieval_Ready__c,  OR(   TEXT(OSM_Product__c) = &quot;Unknown&quot;,   ISBLANK(OSM_Patient_Last_Name__c),   ISBLANK(OSM_Patient_DOB__c),   TEXT(OSM_Patient_Gender__c) = &quot;Unknown&quot;,   TEXT(OSM_Patient_Gender__c) = &quot;&quot;,   (OSM_Specimen_Retrieval_Role__c  &lt; 2)  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Specimen_Ready</fullName>
        <actions>
            <name>OSM_Order_Specimen_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <formula>AND(  NOT(OSM_Specimen_Retrieval_Ready__c),  NOT(TEXT(OSM_Product__c) = &quot;Unknown&quot;),  NOT(ISBLANK(OSM_Patient_Last_Name__c)),  NOT(ISBLANK(OSM_Patient_DOB__c)),  OR(   NOT(TEXT(OSM_Patient_Gender__c) = &quot;Unknown&quot;),   NOT(TEXT(OSM_Patient_Gender__c) = &quot;&quot;)  ),  (OSM_Specimen_Retrieval_Role__c  &gt;= 2) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_State_Active</fullName>
        <actions>
            <name>OSM_Order_State_Set_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 9 - 10863 - Order Status to State Mapping</description>
        <formula>AND(  OSM_Custom_Order_Record_Type__c = &quot;Order&quot;,  OR(   ISCHANGED(OSM_Status__c) ),  TEXT(OSM_Status__c) = &quot;Processing&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_State_Closed</fullName>
        <actions>
            <name>OSM_Order_State_Set_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 9 - 10863 - Order Status to State Mapping</description>
        <formula>AND ( OR(TEXT(OSM_Status__c) = &quot;Canceled&quot;,     TEXT(OSM_Status__c) = &quot;Closed&quot;), RecordType.Name != &quot;Portal Order&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Prostate_Order_Intake_Fax</fullName>
        <actions>
            <name>OSM_Assign_Prostate_Order_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Order_Intake_Assigned_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND ( !OSM_Order_Intake_Assigned__c,        ISPICKVAL(OSM_Channel__c ,&quot;Fax&quot;),       (RecordType.Name== &quot;Order&quot;),       (TEXT(OSM_Status__c) == &quot;Order Intake&quot;),        ISPICKVAL( OSM_Product__c , &quot;Prostate&quot;),       (TEXT($User.OSM_User_Team__c) == &quot;Domestic&quot;)   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Prostate_Order_Intake_Online</fullName>
        <actions>
            <name>OSM_Assign_Prostate_Order_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Order_Intake_Assigned_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story - 29828 - Order Assignment to Queues.</description>
        <formula>AND ( !OSM_Order_Intake_Assigned__c,        ISPICKVAL(OSM_Channel__c ,&quot;Portal&quot;),       (RecordType.Name== &quot;Order&quot;),       (TEXT(OSM_Status__c) == &quot;Order Intake&quot;),        ISPICKVAL( OSM_Product__c , &quot;Prostate&quot;),       (IF (ISNULL (OSM_Ordering_HCO__r.OSM_Customer_Service_Team__c),   FALSE,  OSM_Ordering_HCO__r.OSM_Customer_Service_Team__c == &quot;Domestic&quot;  ))    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Set_Order_Start_Date</fullName>
        <actions>
            <name>OSM_Set_Order_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>44199 - Order Effective Date Update</description>
        <formula>OR( AND( ISNEW(), RecordType.DeveloperName = &quot;OSM_Order&quot; ), AND( ISCHANGED( RecordTypeId ), RecordType.DeveloperName = &quot;OSM_Order&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Triage Outcome - Default</fullName>
        <actions>
            <name>OSM_Set_Triage_outcome_to_new</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Patient number of Orders is &lt;=1, then Triage Outcome = New</description>
        <formula>AND (OSM_Patient_Number_of_Orders_F__c &lt;= 1,      NOT(ISPICKVAL(OSM_Channel__c, &quot;Portal&quot;))     )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Work_Order_State_Active</fullName>
        <actions>
            <name>OSM_Order_State_Set_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sprint 9 - 10866 - Work Order Status to State Mapping</description>
        <formula>AND(  OSM_Custom_Order_Record_Type__c = &quot;Work Order&quot;,  OR(   ISCHANGED(OSM_Status__c),   ISCHANGED(Status)  ),  TEXT(OSM_Status__c) = &quot;Processing&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Work_Order_State_Closed</fullName>
        <actions>
            <name>OSM_Order_State_Set_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 9 - 10866 - Work Order Status to State Mapping</description>
        <formula>AND(  OSM_Custom_Order_Record_Type__c = &quot;Work Order&quot;,  OR(   ISCHANGED(OSM_Status__c),   ISCHANGED(Status)  ),  OR(   TEXT(OSM_Status__c) = &quot;Canceled&quot;,   TEXT(OSM_Status__c) = &quot;Complete&quot;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WiredPayEmail</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Order.OSM_Signature_Verified__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
