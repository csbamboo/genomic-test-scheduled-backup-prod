<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>OSM_Send_Payor_Update_Needed_Email</fullName>
        <description>Send Payor Update Needed Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>pbhandari@genomichealth.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OSM_Genomic_Health_Templates/OSM_Plan_Payor_Update_Template</template>
    </alerts>
    <alerts>
        <fullName>OSM_Send_Wire_Transfer_Email_Order_Line_Item</fullName>
        <description>Send Wire Transfer Email (Order Line Item)</description>
        <protected>false</protected>
        <recipients>
            <recipient>jsunkara@genomichealth.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pbhandari@genomichealth.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vnaidu@genomichealth.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OSM_Genomic_Health_Templates/Wired_Transfer_OLI_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Lab_and_Report_to_Distributed</fullName>
        <field>OSM_Lab_and_Report_Status__c</field>
        <literalValue>Distributed</literalValue>
        <name>Lab and Report to Distributed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lab_and_Report_to_Obsolete</fullName>
        <field>OSM_Lab_and_Report_Status__c</field>
        <literalValue>Obsolete</literalValue>
        <name>Lab and Report to Obsolete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lab_and_Report_to_Pre_Distribution</fullName>
        <description>Sprint 33 - User Story 30548 - Lab and Report Status to Pre-Distribution</description>
        <field>OSM_Lab_and_Report_Status__c</field>
        <literalValue>Pre-Distribution</literalValue>
        <name>Lab and Report to Pre-Distribution</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Node_Determination_Status_equals_Form</fullName>
        <field>OSM_Nodal_Status_Determination_Source__c</field>
        <literalValue>Form</literalValue>
        <name>Node Determination Status equals Form</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OLI_Data_Missing</fullName>
        <field>OSM_OLI_Data_Missing__c</field>
        <literalValue>1</literalValue>
        <name>OLI Data Missing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Billing_Policy_Data_Status_To_DA</fullName>
        <field>OSM_Billing_Policy_Data_Status__c</field>
        <literalValue>Data Available</literalValue>
        <name>OSM Billing Policy Data Status To DA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Billing_Policy_Data_Status_To_DM</fullName>
        <field>OSM_Billing_Policy_Data_Status__c</field>
        <literalValue>Data Missing</literalValue>
        <name>OSM Billing Policy Data Status To DM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Billing_Policy_Data_Status_To_NA</fullName>
        <field>OSM_Billing_Policy_Data_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>OSM Billing Policy Data Status To NA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Billing_Policy_Rule_Override_FALSE</fullName>
        <field>OSM_Billing_Policy_Qualification_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>Billing Policy Rule Override - FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Billing_Policy_Rule_Override_TRUE</fullName>
        <field>OSM_Billing_Policy_Qualification_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>Billing Policy Rule Override - TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Cancel_Billing_Status</fullName>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>OSM Cancel Billing Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Cancel_Customer_Status</fullName>
        <field>OSM_Order_Line_Item_Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>OSM Cancel Customer Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Customer_Status_Closed</fullName>
        <description>User Story 19498:Customer Status - Status Automation</description>
        <field>OSM_Order_Line_Item_Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Customer Status Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Customer_Status_In_Lab</fullName>
        <description>User Story 19498:Customer Status - Status Automation</description>
        <field>OSM_Order_Line_Item_Status__c</field>
        <literalValue>In-Lab</literalValue>
        <name>Customer Status In Lab</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Empty_Amount_Paid_OLI</fullName>
        <field>OSM_Amount_Paid__c</field>
        <name>OSM Empty Amount Paid (OLI)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Empty_Country_OLI</fullName>
        <field>OSM_Country__c</field>
        <name>OSM Empty Country (OLI)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Empty_Sending_Bank_OLI</fullName>
        <field>OSM_Sending_Bank_Transfer_Number__c</field>
        <name>OSM Empty Sending Bank (OLI)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Lab_Report_Distributing</fullName>
        <description>User Story 14672:State Status Update - Lab and Report Status to Distributing</description>
        <field>OSM_Lab_and_Report_Status__c</field>
        <literalValue>Distributing</literalValue>
        <name>Lab Report Distributing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Policy_NA_Check</fullName>
        <field>OSM_Billing_policy_N_A__c</field>
        <literalValue>1</literalValue>
        <name>OLI Billing Policy NA Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Policy_Qualification</fullName>
        <field>OSM_OS_OLI_Billing_ready__c</field>
        <literalValue>1</literalValue>
        <name>OLI Billing Policy Qualification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Status_Await_PreBill</fullName>
        <description>User Story 17723: OLI Billing Status - Awaiting Pre-Billing Data</description>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Awaiting Pre-Billing Data</literalValue>
        <name>OLI Billing Status Await PreBill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Status_Bill_Initiated</fullName>
        <description>User Story 17724:OLI Billing Status - Billing Initiated</description>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Billing Initiated</literalValue>
        <name>OLI Billing Status Bill Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Status_Bill_Not_Req</fullName>
        <description>User Story 17725: OLI Billing Status - Billing Not Required</description>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Billing Not Required</literalValue>
        <name>OLI Billing Status Bill Not Req</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Status_Bill_Void</fullName>
        <description>User Story 17726:OLI Billing Status - Bill Voided</description>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Bill Voided</literalValue>
        <name>OLI Billing Status Bill Void</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Status_PreBill_Invest</fullName>
        <description>User Story 13633:OLI Billing Status - Pre-Billing Investigating</description>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Pre-Billing Investigating</literalValue>
        <name>OLI Billing Status PreBill Invest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Status_PreBill_Ready</fullName>
        <description>User Story 13325:OLI Billing Status - PreBilling Ready</description>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Pre-Billing Ready</literalValue>
        <name>OLI Billing Status PreBill Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Billing_Status_Pre_Bill_Complete</fullName>
        <description>User Story 17722:OLI Billing Status - PreBilling Complete</description>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Pre-Billing Complete</literalValue>
        <name>OLI Billing Status Pre-Bill Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Check_Report_Distribution_Ready</fullName>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <field>OSM_Report_Distribution_Ready__c</field>
        <literalValue>1</literalValue>
        <name>OLI Check Report Distribution Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Cust_Status_to_Processing</fullName>
        <description>User Story 13324:State Status Update - Order Status set to Processing</description>
        <field>OSM_Order_Line_Item_Status__c</field>
        <literalValue>Processing</literalValue>
        <name>OLI Cust Status to Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Data_Entry_to_Awaiting</fullName>
        <description>User Story 13324:State Status Update - Order Status set to Processing</description>
        <field>OSM_Data_Entry_Status__c</field>
        <literalValue>Awaiting Lab Initiation Data</literalValue>
        <name>OLI Data Entry to Awaiting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Data_Entry_to_Awaiting_Pat_Rep</fullName>
        <description>User Story 19505:Data Entry Status- Awaiting Patient Report Data</description>
        <field>OSM_Data_Entry_Status__c</field>
        <literalValue>Awaiting Patient Report Data</literalValue>
        <name>OLI Data Entry to Awaiting Pat Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Data_Entry_to_Data_V</fullName>
        <description>User Story 19508:Data Entry Status- Data Verification</description>
        <field>OSM_Data_Entry_Status__c</field>
        <literalValue>Data Verification</literalValue>
        <name>OLI Data Entry to Data V</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Lab_Not_ready</fullName>
        <description>OLI Lab Not ready</description>
        <field>OSM_Lab_Ready__c</field>
        <literalValue>0</literalValue>
        <name>OLI Lab Not ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_OLI_Uncheck_Report_Distrib_Ready</fullName>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <field>OSM_Report_Distribution_Ready__c</field>
        <literalValue>0</literalValue>
        <name>OLI Uncheck Report Distribution Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Line_BI_Ready_Is_False</fullName>
        <field>OSM_BI_Ready__c</field>
        <literalValue>0</literalValue>
        <name>Order Line BI Ready Is False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Line_BI_Ready_Is_True</fullName>
        <field>OSM_BI_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Order Line BI Ready Is True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Line_Item_Lab_Ready</fullName>
        <description>9547 - Lab Ready field- OLI</description>
        <field>OSM_Lab_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Order Line Item Lab Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Line_Item_State_New</fullName>
        <description>24160: OLI Status to State</description>
        <field>OSM_State__c</field>
        <literalValue>New</literalValue>
        <name>Order Line Item State New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Line_Specimen_Not_Ready</fullName>
        <field>OSM_Specimen_Retrieval_Ready__c</field>
        <literalValue>0</literalValue>
        <name>Order Line Specimen Not Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Line_State_Active</fullName>
        <description>User Story 13326:OLI Status to State</description>
        <field>OSM_State__c</field>
        <literalValue>Active</literalValue>
        <name>Order Line Item State Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Order_Line_State_Closed</fullName>
        <field>OSM_State__c</field>
        <literalValue>Closed</literalValue>
        <name>Order Line State Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Populate_Verification_User_Date_Time</fullName>
        <field>OSM_Verification_Last_Modified__c</field>
        <formula>NOW()</formula>
        <name>Populate Verification User Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_QTY_Set_1</fullName>
        <field>Quantity</field>
        <formula>1</formula>
        <name>QTY_Set_1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_OLI_Credit_Date_to_OLICreatdDate</fullName>
        <field>OSM_Credit_Date__c</field>
        <formula>OSM_OLI_Start_Date__c</formula>
        <name>Set OLI Credit Date to OLIStartDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_OLI_Credit_Date_to_OrdrCrtdDate</fullName>
        <field>OSM_Credit_Date__c</field>
        <formula>Order.EffectiveDate</formula>
        <name>Set OLI Credit Date to OrderStartDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_OLI_Creditable_to_False</fullName>
        <field>OSM_Creditable__c</field>
        <literalValue>0</literalValue>
        <name>Set OLI Creditable to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_OLI_Creditable_to_True</fullName>
        <field>OSM_Creditable__c</field>
        <literalValue>1</literalValue>
        <name>Set OLI Creditable to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Set_OLI_Crediting_Seq_No_to_Blank</fullName>
        <field>OSM_Crediting_Sequence_Number__c</field>
        <name>Set OLI Crediting Sequence No to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_UnitPriceSet</fullName>
        <field>UnitPrice</field>
        <formula>ListPrice</formula>
        <name>UnitPriceSet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Unset_Updated_By_Trigger</fullName>
        <field>OSM_Updated_by_Trigger__c</field>
        <literalValue>0</literalValue>
        <name>Unset Updated By Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Bill_Type_to_Bill_Account</fullName>
        <field>OSM_Bill_Type__c</field>
        <literalValue>Bill Account</literalValue>
        <name>Update Bill Type to Bill Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Billing_Status_to_Bill_Voided</fullName>
        <field>OSM_Billing_Status__c</field>
        <literalValue>Bill Voided</literalValue>
        <name>Update Billing Status to Bill Voided</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Update_Lab_and_Report_Status</fullName>
        <description>Update to Specimen Received</description>
        <field>OSM_Lab_and_Report_Status__c</field>
        <literalValue>Specimen Received</literalValue>
        <name>OSM Update Lab and Report Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Line_Specimen_Ready</fullName>
        <field>OSM_Specimen_Retrieval_Ready__c</field>
        <literalValue>1</literalValue>
        <name>Order Line Specimen Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Payor_PO_Number_Populate</fullName>
        <description>Payor PO Number Populate</description>
        <field>OSM_Purchase_Order_Number__c</field>
        <formula>OSM_Primary_Payor__r.OSM_Purchase_Order_Number__c</formula>
        <name>Payor PO Number Populate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ABN_Required_to_True</fullName>
        <field>OSM_ABN_Required__c</field>
        <literalValue>1</literalValue>
        <name>Update ABN Required to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OLI Node Determination Status Update</fullName>
        <actions>
            <name>Node_Determination_Status_equals_Form</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.OSM_Product__c</field>
            <operation>equals</operation>
            <value>IBC</value>
        </criteriaItems>
        <description>Node Determination Status will have a value of Form when an IBC order is created from Portal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OLI_Data_Entry_Status_and_Lab_and_Report_Status_Check</fullName>
        <actions>
            <name>OSM_OLI_Cust_Status_to_Processing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Data_Entry_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Lab Initiation Data</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>equals</operation>
            <value>Pre-Processing,Specimen Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Order_Line_Item_Status__c</field>
            <operation>notEqual</operation>
            <value>Processing</value>
        </criteriaItems>
        <description>User Story #33193 Customer status - Closed and Processing Updates</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Billing Policy Initial Status Checks on OLI Qualification Status</fullName>
        <actions>
            <name>OSM_Billing_Policy_Rule_Override_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(      ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Specimen Received&apos;),     ISPICKVAL(OSM_Billing_Policy_Qualification_Status__c,&apos;&apos;),     ISPICKVAL(Order.OSM_Status__c ,&apos;Processing&apos;),      OR(        Order.OSM_Billing_Policy_Override__c=true,        AND(            NOT(( ISPICKVAL(Order.OSM_Bill_Type__c, &apos;&apos;) ) )  ,            Order.OSM_Billing_Policy_Payor__c=false           )         )       )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Billing Policy Initial Status Checks on OLI Qualification Status False</fullName>
        <actions>
            <name>OSM_Billing_Policy_Rule_Override_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(      ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Specimen Received&apos;),     ISPICKVAL(Order.OSM_Status__c,&apos;Processing&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Billing Policy Qualification Status Ready</fullName>
        <actions>
            <name>OSM_OLI_Billing_Policy_Qualification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND  ( ISPICKVAL (OSM_Lab_and_Report_Status__c , &quot;Specimen Received&quot;), ISPICKVAL ( Order.OSM_Status__c , &quot;Processing&quot;), NOT ISBLANK (OSM_First_Specimen_Received_Date__c), OR (ISPICKVAL( OSM_Billing_Policy_Data_Status__c, &quot;Data Available&quot;),ISPICKVAL( OSM_Billing_Policy_Data_Status__c, &quot;N/A&quot;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Cancel OLIs</fullName>
        <actions>
            <name>OSM_Cancel_Billing_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Cancel_Customer_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>14533 - State Status Update - Cancel all OLIs on an Order Line Item.</description>
        <formula>ISPICKVAL(OSM_Lab_and_Report_Status__c, &quot;Canceled&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Empty Wired Section %28OLI%29</fullName>
        <actions>
            <name>OSM_Empty_Amount_Paid_OLI</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Empty_Country_OLI</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Empty_Sending_Bank_OLI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>notEqual</operation>
            <value>Patient Pre-Pay</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM OLI First Order Specimen Created</fullName>
        <actions>
            <name>OSM_Update_Lab_and_Report_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>14539 - State Status Update - First Order Specimen Created for OLI</description>
        <formula>AND( ISPICKVAL(Order.OSM_Status__c, &quot;Processing&quot;), NOT(ISNULL(OSM_First_Specimen_Received_Date__c)), ISPICKVAL(OSM_Lab_and_Report_Status__c,&quot;Pre-Processing&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM OLI Missing Data</fullName>
        <actions>
            <name>OLI_Data_Missing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND ( ISPICKVAL(Order.OSM_Status__c , &quot;Processing&quot;), ISPICKVAL (OSM_Lab_and_Report_Status__c , &quot;Specimen Received&quot;), NOT ISBLANK ( OSM_First_Specimen_Received_Date__c ), OR ( ISPICKVAL (OSM_Billing_Policy_Data_Status__c, &quot;Data Available&quot;),ISPICKVAL (OSM_Billing_Policy_Data_Status__c, &quot;N/A&quot;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM OLI NA</fullName>
        <actions>
            <name>OSM_OLI_Billing_Policy_NA_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (  ISPICKVAL (OSM_Lab_and_Report_Status__c, &quot;Specimen Received&quot;), NOT ISBLANK (OSM_First_Specimen_Received_Date__c ), OR (ISPICKVAL( OSM_Billing_Policy_Data_Status__c , &quot;Data Available&quot;),ISPICKVAL( OSM_Billing_Policy_Data_Status__c , &quot;N/A&quot;)), ISPICKVAL ( OSM_Billing_Policy_Qualification_Status__c, &quot;N/A&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Order Specimen Received And Processing Update Billing Policy Data Status to DA Rule E</fullName>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_To_DA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Task 15852 - As a system, if certain data is missing from the Order and OLI, then set the OLI Billing Policy Data Status:  Scenario E</description>
        <formula>AND(       OR(ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Specimen Received&apos;),  ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Processing&apos;)), ISPICKVAL(Order.OSM_Status__c ,&apos;Processing&apos;),      Order.OSM_Billing_Policy_Override__c = false,     NOT(ISPICKVAL( OSM_Bill_Type__c , &quot;&quot;)),     Order.OSM_Billing_Policy_Payor__c = true,     NOT(ISBLANK(Order.OSM_Signature_Date__c ))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Order Specimen Received And Processing Update Billing Policy Data Status to DM Rule B</fullName>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_To_DM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 15852*:Billing Policy - Initial Status Checks on OLI Data Status:  Scenario B</description>
        <formula>AND(       OR(ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Specimen Received&apos;), ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Processing&apos;)), ISPICKVAL(Order.OSM_Status__c ,&apos;Processing&apos;),      Order.OSM_Billing_Policy_Override__c = false,     ISPICKVAL( OSM_Bill_Type__c , &quot;&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Order Specimen Received And Processing Update Billing Policy Data Status to DM Rule C</fullName>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_To_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 15852*:Billing Policy - Initial Status Checks on OLI Data Status:  Scenario C</description>
        <formula>AND(       OR(ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Specimen Received&apos;), ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Processing&apos;)), ISPICKVAL(Order.OSM_Status__c ,&apos;Processing&apos;),      Order.OSM_Billing_Policy_Override__c = false,     NOT(ISPICKVAL( OSM_Bill_Type__c , &quot;&quot;)),     Order.OSM_Billing_Policy_Payor__c = false  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Order Specimen Received And Processing Update Billing Policy Data Status to DM Rule D</fullName>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_To_DM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 15852*:Billing Policy - Initial Status Checks on OLI Data Status:  Scenario D</description>
        <formula>AND( OR(ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Specimen Received&apos;), ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Processing&apos;)), ISPICKVAL(Order.OSM_Status__c ,&apos;Processing&apos;),      Order.OSM_Billing_Policy_Override__c = false,     NOT(ISPICKVAL( OSM_Bill_Type__c , &quot;&quot;)),     Order.OSM_Billing_Policy_Payor__c = true,     ISBLANK(Order.OSM_Signature_Date__c )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Order Specimen Received And Processing Update Billing Policy Data Status to NA Rule A</fullName>
        <actions>
            <name>OSM_Billing_Policy_Data_Status_To_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Billing Policy -  Initial Status Checks on OLI Data Status:  Scenario A</description>
        <formula>AND( OR(ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Specimen Received&apos;), ISPICKVAL(OSM_Lab_and_Report_Status__c,&apos;Processing&apos;)),       ISPICKVAL(Order.OSM_Status__c ,&apos;Processing&apos;),      Order.OSM_Billing_Policy_Override__c=true )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Populate PO Number from Payor</fullName>
        <actions>
            <name>Payor_PO_Number_Populate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Primary_Payor_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Populate PO Number from Payor</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Send Payor Update Needed Email</fullName>
        <actions>
            <name>OSM_Send_Payor_Update_Needed_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to a user that Payor field needs updating if the OLI Plan field has a value</description>
        <formula>AND(OSM_Plan__c != null,  OSM_Plan__r.OSM_Payor__c = null)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Send Wire Transfer Email %28Order Line Item%29</fullName>
        <actions>
            <name>OSM_Send_Wire_Transfer_Email_Order_Line_Item</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Payment_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>equals</operation>
            <value>Patient Pre-Pay</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set Crediting Sequence Number to Blank</fullName>
        <actions>
            <name>OSM_Set_OLI_Crediting_Seq_No_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OrderItem.OSM_Multiple_Primaries_Confirmed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sets the OLI Crediting Sequence Number to blank when Multiple Primaries Confirmed checkbox is False</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set OLI Credit Date to OLI Created Date</fullName>
        <actions>
            <name>OSM_Set_OLI_Credit_Date_to_OLICreatdDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.OSM_Order_Line_Item_Count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Sets the Credit Date to the OLI Start Date if new OLIs are created and there are existing OLIs already</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set OLI Credit Date to Order Created Date</fullName>
        <actions>
            <name>OSM_Set_OLI_Credit_Date_to_OrdrCrtdDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.OSM_Order_Line_Item_Count__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Sets the Credit Date to the Order Start Date the first time OLIs are created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set OLI Credit Date to Order Created Date2</fullName>
        <actions>
            <name>OSM_Set_OLI_Credit_Date_to_OrdrCrtdDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Credit Date to the Order Start Date the first time OLIs are created</description>
        <formula>OR(AND( Order.OSM_Order_Line_Item_Count__c = 0,  Order.RecordType.DeveloperName = &quot;OSM_Order&quot; ), AND(Order.RecordType.DeveloperName = &quot;OSM_Order&quot; ), ISPICKVAL(Order.OSM_Channel__c, &quot;Portal&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set OLI Creditable to False</fullName>
        <actions>
            <name>OSM_Set_OLI_Creditable_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the OLI Creditable field to false if Creditable Override is False or when the below criteria are met (Note: criteria to check Study Name = null was removed as there&apos;s a separate trigger that sets Creditable to False if Study Name is null.</description>
        <formula>TEXT(OSM_Creditable_Override__c)= &quot;Set Creditable to False&quot; || AND(TEXT(OSM_Creditable_Override__c)= &quot;&quot;, OR(TEXT( OSM_Submission_Status__c )!= &quot;New&quot;, AND(TEXT( OSM_Order_Line_Item_Status__c )!= &quot;Processing&quot;, TEXT( OSM_Order_Line_Item_Status__c )!= &quot;In-Lab&quot;, TEXT( OSM_Order_Line_Item_Status__c )!= &quot;Closed&quot;, TEXT( OSM_Order_Line_Item_Status__c )!= &quot;Canceled&quot;), Product2.OSM_Creditable__c = false)) || AND(TEXT(OSM_Creditable_Override__c)= &quot;&quot;, Order.OSM_Study_Name__c != &quot;&quot;, OR(Order.OSM_Study_Name__r.OSM_Credited_Study__c = false, TEXT(Order.OSM_Study_Name__r.OSM_Sample_Processing_Lab__c)= &quot;Development&quot;)) || AND(TEXT(OSM_Creditable_Override__c)= &quot;&quot;, Order.OSM_MockRunTrue__c = true, Order.OSM_Study_Name__c = &quot;&quot;) || AND(TEXT(OSM_Creditable_Override__c)= &quot;&quot;, Order.OSM_Study_Name__c = &quot;&quot;, OR(OSM_Multiple_Primaries_Confirmed__c = true, OSM_Multiple_Primaries_Confirmed__c = false), OSM_Crediting_Sequence_Number__c &gt; Product2.OSM_Max_Creditable_Primaries__c) || AND(TEXT(OSM_Creditable_Override__c)= &quot;&quot;, Order.OSM_Study_Name__c = &quot;&quot;, OSM_Multiple_Primaries_Confirmed__c = false, OSM_Crediting_Sequence_Number__c &lt;= Product2.OSM_Max_Creditable_Primaries__c) || AND(TEXT(OSM_Creditable_Override__c)= &quot;&quot;, Order.OSM_Study_Name__c = &quot;&quot;, OSM_Multiple_Primaries_Confirmed__c = true, ISBLANK(OSM_Crediting_Sequence_Number__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Set OLI Creditable to True</fullName>
        <actions>
            <name>OSM_Set_OLI_Creditable_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the OLI Creditable field to true when Creditable Override is True or when the below criteria are met</description>
        <formula>OR(TEXT(OSM_Creditable_Override__c)= &quot;Set Creditable to True&quot;, AND( TEXT(OSM_Creditable_Override__c)= &quot;&quot;, TEXT( OSM_Submission_Status__c )= &quot;New&quot;, OR(TEXT( OSM_Order_Line_Item_Status__c )= &quot;Processing&quot;, TEXT( OSM_Order_Line_Item_Status__c )= &quot;In-Lab&quot;, TEXT( OSM_Order_Line_Item_Status__c )= &quot;Closed&quot;, TEXT( OSM_Order_Line_Item_Status__c )= &quot;Canceled&quot;), Product2.OSM_Creditable__c = true) &amp;&amp; OR(AND(Order.OSM_Study_Name__c != &quot;&quot;, Order.OSM_Study_Name__r.OSM_Credited_Study__c = true, TEXT(Order.OSM_Study_Name__r.OSM_Sample_Processing_Lab__c)!= &quot;Development&quot;), AND(Order.OSM_MockRunTrue__c = false, Order.OSM_Study_Name__c = &quot;&quot;, OSM_Multiple_Primaries_Confirmed__c = false, ISBLANK(OSM_Crediting_Sequence_Number__c)), AND(Order.OSM_MockRunTrue__c = false, Order.OSM_Study_Name__c = &quot;&quot;, OSM_Multiple_Primaries_Confirmed__c = true, OSM_Crediting_Sequence_Number__c &lt;= Product2.OSM_Max_Creditable_Primaries__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update ABN Required to True</fullName>
        <actions>
            <name>Update_ABN_Required_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 or 5)</booleanFilter>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>equals</operation>
            <value>Medicare</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Nodal_Status__c</field>
            <operation>equals</operation>
            <value>Node Positive (4+ Nodes)</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_IBC_Clinical_Stage__c</field>
            <operation>equals</operation>
            <value>IV</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Colon_Clinical_Stage__c</field>
            <operation>equals</operation>
            <value>Stage I</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Colon_Clinical_Stage__c</field>
            <operation>equals</operation>
            <value>Stage IV</value>
        </criteriaItems>
        <description>Updates the ABN Required checkbox to true when record meets the following criteria</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update Billing Status to Bill Voided</fullName>
        <actions>
            <name>OSM_Update_Billing_Status_to_Bill_Voided</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Billing_Void_Sent_Date_Current__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled</value>
        </criteriaItems>
        <description>17726. Workflow: OLI.OSM_OLI_Bill_Status_BillVoided
User Story 43151:PB - Set OLI Billing Status to Billing Voided
Updates the Billing Status to Bill Voided if &quot;Results Disposition - Current&quot; field is changed from Results to Failure</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM Update OLI Bill Type to Bill Account</fullName>
        <actions>
            <name>OSM_Update_Bill_Type_to_Bill_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update OLI Bill Type to Bill Account if Order&apos;s Study is not NULL and Order&apos;s Study&apos;s Bill To field is &quot;Partner&quot;</description>
        <formula>AND(Order.OSM_Study_Name__c != null, ISPICKVAL(Order.OSM_Study_Name__r.OSM_Bill_To__c, &quot;Partner&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_DefaultValues</fullName>
        <actions>
            <name>OSM_QTY_Set_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_UnitPriceSet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI Lab Not ready</fullName>
        <actions>
            <name>OSM_OLI_Lab_Not_ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>9547 - Lab Ready field- OLI</description>
        <formula>NOT(  AND( NOT ISPICKVAL(Order.OSM_Product__c,&quot;Unknown&quot;),  NOT ISPICKVAL(Order.OSM_Product__c,&quot;&quot;), NOT ISBLANK(Order.Id), NOT ISBLANK(OSM_Patient_Last_Name__c), NOT ISPICKVAL(OSM_Bill_Type__c,&quot;&quot;),  IF(  AND(Contains(Product2.Name,&quot;IBC&quot;),  ISPICKVAL(OSM_Nodal_Status__c,&quot;&quot;)),  FALSE, TRUE),   IF(  AND(Product2.Name = &quot;IBC&quot;,  ISPICKVAL( OSM_ER_Status__c ,&quot;&quot; )),  FALSE, TRUE),  IF(AND(CONTAINS(Product2.Name,&quot;IBC&quot;), ISPICKVAL(OSM_IBC_Clinical_Stage__c ,&quot;&quot; )), FALSE, TRUE),  IF(AND(CONTAINS(Product2.Name,&quot;DCIS&quot;), ISPICKVAL(OSM_DCIS_Clinical_Stage__c ,&quot;&quot; )), FALSE, TRUE),  IF(AND(NOT(CONTAINS(Product2.Name,&quot;Prostate&quot;)),NOT(CONTAINS(Product2.Name,&quot;DCIS&quot;)), NOT(CONTAINS(Product2.Name,&quot;IBC&quot;)), ISPICKVAL(OSM_Colon_Clinical_Stage__c ,&quot;&quot; )), FALSE, TRUE),   IF(AND(CONTAINS(Product2.Name,&quot;Prostate&quot;), ISPICKVAL(  OSM_NCCN_Risk_Category__c ,&quot;&quot; )), FALSE, TRUE),  IF(AND(CONTAINS(Product2.Name,&quot;Prostate&quot;), ISPICKVAL(   OSM_Submitted_Gleason_Score__c  ,&quot;&quot; )), FALSE, TRUE),  IF (AND( OSM_Multiple_Primaries_Confirmed__c= True, ISPICKVAL(Order.OSM_How_to_Test_Multiple_Primaries__c, &quot;Test Sequentially - Highest Grade Tumor First&quot;),   ( (Text(OSM_Primary_Sequence_Number__c)= &quot;0&quot;)|| (TEXT(OSM_Primary_Sequence_Number__c) = &quot;&quot;))), FALSE,TRUE),  Order.OSM_Lab_Hold_Case_Open__c =False,  OR( ISPICKVAL(OSM_Billing_Policy_Qualification_Status__c,&quot;N/A&quot;), ISPICKVAL(OSM_Billing_Policy_Qualification_Status__c,&quot;OK to Proceed&quot;)),  NOT ISBLANK(OSM_First_Specimen_Received_Date__c),   Order.OSM_Order_Role_Lab_Ready__c = true ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Bill_Status_Await_PreBill</fullName>
        <actions>
            <name>OSM_OLI_Billing_Status_Await_PreBill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled,Obsolete,Distributed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>notEqual</operation>
            <value>Genomic Health</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_BI_Ready__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Pre_Billing_Complete_Date_Current__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Billing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Pre_Billing_Case_Open__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Results_Disposition_Current__c</field>
            <operation>notEqual</operation>
            <value>Failure</value>
        </criteriaItems>
        <description>User Story 17723: OLI Billing Status - Awaiting Pre-Billing Data
User Story 43095:PB - Set OLI Billing Status to Awaiting Pre-Billing Data
63677:Condition Update: Billing Status - Awaiting Pre-Billing Data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Bill_Status_BillInitiated</fullName>
        <actions>
            <name>OSM_OLI_Billing_Status_Bill_Initiated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Billing_Case_Open__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Billing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>User Story 17724:OLI Billing Status - Billing Initiated
User Story 43123:PB - Set OLI Billing Status to Billing Initiated</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Bill_Status_BillVoided</fullName>
        <actions>
            <name>OSM_OLI_Billing_Status_Bill_Void</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>equals</operation>
            <value>Distributed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Results_Disposition_Current__c</field>
            <operation>equals</operation>
            <value>Failure</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>notEqual</operation>
            <value>Genomic Health</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Pre_Billing_Sent_Date_Current__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Pre_Billing_Complete_Date_Current__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Billing_Request_Sent_Date_Current__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Billing_Void_Sent_Date_Current__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>User Story 17726:OLI Billing Status - Bill Voided</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Bill_Status_Bill_Not_Req</fullName>
        <actions>
            <name>OSM_OLI_Billing_Status_Bill_Not_Req</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Billing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>equals</operation>
            <value>Canceled,Obsolete</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>equals</operation>
            <value>Genomic Health</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Results_Disposition_Current__c</field>
            <operation>equals</operation>
            <value>Failure</value>
        </criteriaItems>
        <description>User Story 17725: OLI Billing Status - Billing Not Required
63674:Condition Update: Billing Status - Billing Not Required</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Bill_Status_Canceled</fullName>
        <actions>
            <name>OSM_Cancel_Billing_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>equals</operation>
            <value>Canceled</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Billing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Billing_Void_Sent_Date_Current__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>User Story 43173:PB - Set OLI Billing Status to Canceled</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Bill_Status_PreBillComplete</fullName>
        <actions>
            <name>OSM_OLI_Billing_Status_Pre_Bill_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>notContain</operation>
            <value>Canceled,Obsolete,Distributed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Billing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>notContain</operation>
            <value>Genomic Health</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Pre_Billing_Complete_Date_Current__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Pre_Billing_Case_Open__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Results_Disposition_Current__c</field>
            <operation>notEqual</operation>
            <value>Failure</value>
        </criteriaItems>
        <description>User Story 17722: OLI Billing Status - PreBilling Complete
User Story 44449:PB - OLI Billing Status to Pre-Billing Complete
63680:Condition Update: Billing Status - Pre-Billing Complete</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Bill_Status_PreBill_Invest</fullName>
        <actions>
            <name>OSM_OLI_Billing_Status_PreBill_Invest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Billing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Pre_Billing_Case_Open__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>notEqual</operation>
            <value>Genomic Health</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Results_Disposition_Current__c</field>
            <operation>notEqual</operation>
            <value>Failure</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled,Obsolete,Distributed</value>
        </criteriaItems>
        <description>User Story 13633:OLI Billing Status - Pre-Billing Investigating
User Story 43100:PB - Set OLI.Billing Status to PreBilling Investigating
63679:Condition Update: Billing Status - PreBilling Investigating</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Bill_Status_PreBill_Ready</fullName>
        <actions>
            <name>OSM_OLI_Billing_Status_PreBill_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 11 AND 12 AND (8 OR (9 AND 10))</booleanFilter>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Billing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Bill_Type__c</field>
            <operation>notEqual</operation>
            <value>Genomic Health</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_BI_Ready__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Is_Pre_Billing_Case_Open__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Pre_Billing_Complete_Date_Current__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Status__c</field>
            <operation>equals</operation>
            <value>Processing</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_State__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Billing_Policy_Payor__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Billing_Policy_Qualification_Status__c</field>
            <operation>equals</operation>
            <value>N/A,OK to Proceed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.OSM_Billing_Policy_Payor__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Results_Disposition_Current__c</field>
            <operation>notEqual</operation>
            <value>Failure</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled,Obsolete,Distributed</value>
        </criteriaItems>
        <description>User Story 13325:OLI Billing Status - PreBilling Ready
63678:Condition Update: Billing Status - PreBilling Ready</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Check_Report_Dist_Ready</fullName>
        <actions>
            <name>OSM_OLI_Check_Report_Distribution_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <formula>AND(  Order.OSM_Cnt_Order_Role_Report_Dist_Not_Ready__c = 0,  OSM_OSpecimen_Report_Distribution_Ready__c,  ISPICKVAL(OSM_State__c, &quot;Active&quot;),  OR(  NOT(ISPICKVAL(Order.OSM_Product__c, &quot;&quot;)),  NOT(ISPICKVAL(Order.OSM_Product__c,&quot;Unknown&quot;))  ),  NOT(ISNULL(OrderId)),  NOT(ISNULL(Order.OSM_Patient_Last_Name__c)),  NOT(ISNULL(Order.OSM_Patient_DOB__c)),  NOT(ISPICKVAL(Order.OSM_Patient_Gender__c, &quot;&quot;)),  NOT(ISNULL(Order.OSM_Signature_Date__c)),  NOT(ISPICKVAL(OSM_Bill_Type__c , &quot;&quot;)),  IF(  CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Post-DOS Pay&quot;),  NOT(ISBLANK(Order.OSM_Patient_City__c)),  TRUE  ),  IF(  CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Post-DOS Pay&quot;),  NOT(ISBLANK(Order.OSM_Patient_Country__c)),  TRUE  ),  IF(  CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Post-DOS Pay&quot;),  NOT(ISBLANK( Order.OSM_Patient_Street__c )),  TRUE  ),   IF(  CONTAINS(TEXT(OSM_Product_Name__c ),&quot;IBC&quot;),  NOT(ISPICKVAL(OSM_Nodal_Status__c,&quot;&quot;)),  TRUE  ),  IF(  CONTAINS(TEXT(OSM_Product_Name__c ),&quot;IBC&quot;),  NOT(ISPICKVAL(OSM_ER_Status__c,&quot;&quot;)),  TRUE  ),  IF(  CONTAINS(TEXT(OSM_Product_Name__c ),&quot;IBC&quot;),  NOT(ISPICKVAL(OSM_IBC_Clinical_Stage__c,&quot;&quot;)),  TRUE  ),  IF(  CONTAINS(TEXT(OSM_Product_Name__c ), &quot;DCIS&quot;),  NOT(ISPICKVAL(OSM_DCIS_Clinical_Stage__c,&quot;&quot;)),  TRUE  ),  IF(  AND(  NOT(CONTAINS(TEXT(OSM_Product_Name__c ),&quot;DCIS&quot;)),  NOT(CONTAINS(TEXT(OSM_Product_Name__c ),&quot;IBC&quot;)),  NOT(CONTAINS(TEXT(OSM_Product_Name__c ),&quot;Prostate&quot;))  ),  NOT(ISPICKVAL(OSM_Colon_Clinical_Stage__c,&quot;&quot;)),  TRUE  ),   IF(  CONTAINS(TEXT(OSM_Product_Name__c ),&quot;Prostate&quot;),  NOT(ISPICKVAL(OSM_Submitted_Gleason_Score__c,&quot;&quot;)),  TRUE  ),  IF(  CONTAINS(TEXT(OSM_Product_Name__c ),&quot;Prostate&quot;),  NOT(ISPICKVAL(OSM_NCCN_Risk_Category__c,&quot;&quot;)),  TRUE  ),  IF(  CONTAINS(TEXT(Order.Order_Location__c),&quot;International&quot;),  NOT(OSM_Submitting_Diagnosis__c = &quot;&quot;),  TRUE  ),  IF(  AND(  CONTAINS(TEXT(Order.Order_Location__c),&quot;Domestic&quot;),  AND(  NOT(CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Pre-Pay&quot;)),  NOT(CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Post-DOS Pay&quot;)),  NOT(CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Bill Account&quot;)),  NOT(CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Genomic Health&quot;))  )  ),  NOT(ISPICKVAL(OSM_ICD_Code__c,&quot;&quot;)),  TRUE  ),  NOT(Order.OSM_Report_Distribution_Hold_Case_Open__c),  Order.OSM_Has_Ordering_Role__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Cust_Status_Closed</fullName>
        <actions>
            <name>OSM_Customer_Status_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>equals</operation>
            <value>Obsolete,Distributed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Order_Line_Item_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>User Story 19498:Customer Status - Status Automation</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Cust_Status_InLab</fullName>
        <actions>
            <name>OSM_Customer_Status_In_Lab</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>equals</operation>
            <value>Processing,Pre-Distribution</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Order_Line_Item_Status__c</field>
            <operation>notEqual</operation>
            <value>In-Lab</value>
        </criteriaItems>
        <description>User Story 19498:Customer Status - Status Automation, User Story 33193-Customer Status-Closed and Processing Updates</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_DataEntStat_AwaitPatReport</fullName>
        <actions>
            <name>OSM_OLI_Data_Entry_to_Awaiting_Pat_Rep</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 19505:Data Entry Status- Awaiting Patient Report Data</description>
        <formula>OR(     AND(    TEXT( OSM_Work_Order__r.OSM_Status__c ) = &quot;Open&quot;,    TEXT(OSM_State__c) = &quot;Active&quot;,    OSM_Lab_Ready__c,    NOT(OSM_Report_Distribution_Ready__c),    TEXT(OSM_Data_Entry_Status__c) &lt;&gt; &quot;Awaiting Patient Report Data&quot;   ),   AND(    OR(     TEXT(OSM_Work_Order__r.OSM_Status__c) = &quot;Processing&quot;,     TEXT(OSM_Work_Order__r.OSM_Status__c) = &quot;Complete&quot;    ),    TEXT(OSM_State__c) = &quot;Active&quot;,    NOT(OSM_Lab_Ready__c),    NOT(OSM_Report_Distribution_Ready__c),    TEXT(OSM_Data_Entry_Status__c) &lt;&gt; &quot;Awaiting Patient Report Data&quot;   )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_Specimen_Retrieval_Not_Ready</fullName>
        <actions>
            <name>OSM_Order_Line_Specimen_Not_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sprint 9 - 8306 - Specimen Retrieval Readiness Field- OLI</description>
        <formula>NOT(Order.OSM_Specimen_Retrieval_Ready__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_OLI_UnCheck_Report_Dist_Ready</fullName>
        <actions>
            <name>OSM_OLI_Uncheck_Report_Distrib_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>19850 - Report Distribution Ready field- OLI</description>
        <formula>NOT( AND( Order.OSM_Cnt_Order_Role_Report_Dist_Not_Ready__c = 0,  OSM_OSpecimen_Report_Distribution_Ready__c,  ISPICKVAL(OSM_State__c, &quot;Active&quot;),  OR( NOT(ISPICKVAL(Order.OSM_Product__c, &quot;&quot;)),  	NOT(ISPICKVAL(Order.OSM_Product__c,&quot;Unknown&quot;)) ),	 NOT(ISNULL(OrderId)),  NOT(ISNULL(Order.OSM_Patient_Last_Name__c)),  NOT(ISNULL(Order.OSM_Patient_DOB__c)),  NOT(ISPICKVAL(Order.OSM_Patient_Gender__c, &quot;&quot;)),  NOT(ISNULL(Order.OSM_Signature_Date__c)),  NOT(ISPICKVAL(OSM_Bill_Type__c , &quot;&quot;)),  IF( CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Post-DOS Pay&quot;),  	NOT(ISBLANK(Order.OSM_Patient_City__c)), TRUE ),  IF( CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Post-DOS Pay&quot;),  	NOT(ISBLANK(Order.OSM_Patient_Country__c)), TRUE ),  IF( CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Post-DOS Pay&quot;),  	NOT(ISBLANK(Order.OSM_Patient_Street__c )), TRUE ),  IF( CONTAINS(TEXT(OSM_Product_Name__c ),&quot;IBC&quot;),  	NOT(ISPICKVAL(OSM_Nodal_Status__c,&quot;&quot;)), TRUE ),  IF( CONTAINS(TEXT(OSM_Product_Name__c ),&quot;IBC&quot;),  	NOT(ISPICKVAL(OSM_ER_Status__c,&quot;&quot;)), TRUE ),  IF( CONTAINS(TEXT(OSM_Product_Name__c ),&quot;IBC&quot;),  	NOT(ISPICKVAL(OSM_IBC_Clinical_Stage__c,&quot;&quot;)), TRUE ),  IF( CONTAINS(TEXT(OSM_Product_Name__c ), &quot;DCIS&quot;),  	NOT(ISPICKVAL(OSM_DCIS_Clinical_Stage__c,&quot;&quot;)), TRUE ),  IF( AND( NOT(CONTAINS(TEXT(OSM_Product_Name__c ),&quot;DCIS&quot;)),  		NOT(CONTAINS(TEXT(OSM_Product_Name__c ),&quot;IBC&quot;)),  		NOT(CONTAINS(TEXT(OSM_Product_Name__c ),&quot;Prostate&quot;)) ),  	NOT(ISPICKVAL(OSM_Colon_Clinical_Stage__c,&quot;&quot;)), TRUE ),  IF( CONTAINS(TEXT(OSM_Product_Name__c ),&quot;Prostate&quot;),  	NOT(ISPICKVAL(OSM_Submitted_Gleason_Score__c,&quot;&quot;)), TRUE ),  IF( CONTAINS(TEXT(OSM_Product_Name__c ),&quot;Prostate&quot;),  	NOT(ISPICKVAL(OSM_NCCN_Risk_Category__c,&quot;&quot;)), TRUE ),  IF( CONTAINS(TEXT(Order.Order_Location__c),&quot;International&quot;),  	NOT(OSM_Submitting_Diagnosis__c = &quot;&quot;), TRUE ),  IF( AND( CONTAINS(TEXT(Order.Order_Location__c),&quot;Domestic&quot;),  	AND( NOT(CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Pre-Pay&quot;)),  		NOT(CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Patient Post-DOS Pay&quot;)),  		NOT(CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Bill Account&quot;)),  		NOT(CONTAINS(TEXT(OSM_Bill_Type__c),&quot;Genomic Health&quot;)) ) ),  	NOT(ISPICKVAL(OSM_ICD_Code__c,&quot;&quot;)), TRUE ),  NOT(Order.OSM_Report_Distribution_Hold_Case_Open__c),  Order.OSM_Has_Ordering_Role__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Active</fullName>
        <actions>
            <name>OSM_Order_Line_State_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Data_Entry_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Lab Initiation Data</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_State__c</field>
            <operation>notEqual</operation>
            <value>Active</value>
        </criteriaItems>
        <description>User Story 13326:OLI Status to State</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Active2</fullName>
        <actions>
            <name>OSM_Order_Line_State_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_State__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Data_Entry_Status__c</field>
            <operation>notEqual</operation>
            <value>Initial Data Entry</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled,Obsolete,Distributed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Order_Line_Item_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled</value>
        </criteriaItems>
        <description>24160: OLI Status to State</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_BI_Not_Ready</fullName>
        <actions>
            <name>OSM_Order_Line_BI_Ready_Is_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OSM_BI_Ready__c,  OR(  Not(ISPICKVAL(OSM_State__c, &quot;Active&quot;)),  (ISBLANK(TEXT(OSM_Bill_Type__c))),  NOT(Order.OSM_BI_Ready__c),  (Order.OSM_Ordering_Role_BI_Ready_Code__c = 0),  (IF((ISPICKVAL(Order.Order_Location__c, &quot;Domestic&quot;)),  OR(  IF( TEXT(OSM_Product_Name__c) = &quot;Prostate&quot;, OR((ISBLANK(TEXT(OSM_Submitted_Gleason_Score__c))),  (ISBLANK(TEXT(OSM_NCCN_Risk_Category__c)))), FALSE ),  IF(  OR(  (TEXT(OSM_Bill_Type__c) = &quot;Patient Pre-Pay&quot;),  (TEXT(OSM_Bill_Type__c) = &quot;Patient Post-DOS Pay&quot;),  (TEXT(OSM_Bill_Type__c) = &quot;Genomic Health&quot;),  (TEXT(OSM_Bill_Type__c) = &quot;Bill Account&quot;)  ), FALSE, ISBLANK(TEXT(OSM_ICD_Code__c))  ),  IF(  OR(  (TEXT(OSM_Product_Name__c) = &quot;Prostate&quot;),  (TEXT(OSM_Product_Name__c) = &quot;IBC&quot;),  (TEXT(OSM_Product_Name__c) = &quot;DCIS&quot;)  ), FALSE, ISBLANK(TEXT(OSM_Colon_Clinical_Stage__c))  ),  IF( (TEXT(OSM_Product_Name__c) = &quot;DCIS&quot;), ISBLANK(TEXT(OSM_DCIS_Clinical_Stage__c)), FALSE  ),  IF( (TEXT(OSM_Product_Name__c) = &quot;IBC&quot;), OR(ISBLANK(TEXT(OSM_IBC_Clinical_Stage__c)),(ISBLANK(TEXT(OSM_Nodal_Status__c))),  (ISBLANK(TEXT(OSM_ER_Status__c)))   ), FALSE  )  ), FALSE  ))  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_BI_Ready</fullName>
        <actions>
            <name>OSM_Order_Line_BI_Ready_Is_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  (ISPICKVAL(OSM_State__c, &quot;Active&quot;)),  (NOT(ISBLANK(TEXT(OSM_Bill_Type__c)))),  NOT( OSM_BI_Ready__c ),  Order.OSM_BI_Ready__c,  IF( (ISPICKVAL(Order.Order_Location__c, &quot;Domestic&quot;)),  AND(  IF( TEXT(OSM_Product_Name__c) = &quot;Prostate&quot;  , AND ( (NOT(ISBLANK(TEXT(OSM_Submitted_Gleason_Score__c)))),  (NOT(ISBLANK(TEXT(OSM_NCCN_Risk_Category__c)))) ), TRUE  ),  IF(  OR(  (TEXT(OSM_Bill_Type__c) = &quot;Patient Pre-Pay&quot;),  (TEXT(OSM_Bill_Type__c) = &quot;Patient Post-DOS Pay&quot;),  (TEXT(OSM_Bill_Type__c) = &quot;Genomic Health&quot;),  (TEXT(OSM_Bill_Type__c) = &quot;Bill Account&quot;)  ), TRUE, (NOT(ISBLANK(TEXT(OSM_ICD_Code__c))))  ),  IF(  OR(  ( TEXT(OSM_Product_Name__c) = &quot;Prostate&quot;),  (TEXT(OSM_Product_Name__c) = &quot;IBC&quot;),  (TEXT(OSM_Product_Name__c) = &quot;DCIS&quot;)  ), TRUE, NOT(ISBLANK(TEXT(OSM_Colon_Clinical_Stage__c)))  ),  IF( (TEXT(OSM_Product_Name__c)= &quot;DCIS&quot;), NOT(ISBLANK(TEXT(OSM_DCIS_Clinical_Stage__c))), TRUE  ),  IF( (TEXT(OSM_Product_Name__c) = &quot;IBC&quot;), AND( NOT(ISBLANK(TEXT(OSM_IBC_Clinical_Stage__c))), (NOT(ISBLANK(TEXT(OSM_Nodal_Status__c)))), (NOT(ISBLANK(TEXT(OSM_ER_Status__c)))) ), TRUE  )  ), TRUE  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Closed</fullName>
        <actions>
            <name>OSM_Order_Line_State_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>equals</operation>
            <value>Canceled,Obsolete,Distributed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_State__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>User Story 13326:OLI Status to State</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Distributed</fullName>
        <actions>
            <name>Lab_and_Report_to_Distributed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 33 OM - User Story User Story 30547 - Lab and Report Status to Distributed</description>
        <formula>(ISPICKVAL(OSM_Work_Order__r.OSM_State__c, &quot;Closed&quot;) &amp;&amp;   !ISPICKVAL(OSM_Lab_and_Report_Status__c, &quot;Canceled&quot;) &amp;&amp;  OSM_Result_Current__r.OSM_Current__c = true &amp;&amp;  OSM_Result_Current__r.OSM_Distributed__c = true)   &amp;&amp;    ( ISPICKVAL(OSM_Result_Current__r.OSM_Result_Disposition__c, &quot;Results&quot;) ||  ISPICKVAL(OSM_Result_Current__r.OSM_Result_Disposition__c , &quot;Failure&quot; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Distributing</fullName>
        <actions>
            <name>OSM_Lab_Report_Distributing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 14672:State Status Update - Lab and Report Status to Distributing
54658 - added Billing Initiated to the condition</description>
        <formula>(ISPICKVAL(OSM_Work_Order__r.OSM_State__c, &quot;Closed&quot;) &amp;&amp;   ISPICKVAL(OSM_Lab_and_Report_Status__c, &quot;Pre-Distribution&quot;) &amp;&amp;   OSM_Result_Current__r.OSM_Current__c = true &amp;&amp;   OSM_Result_Current__r.OSM_Distributed__c = false)   &amp;&amp;   ( ISPICKVAL(OSM_Result_Current__r.OSM_Result_Disposition__c, &quot;Results&quot;) ||   ISPICKVAL(OSM_Result_Current__r.OSM_Result_Disposition__c , &quot;Failure&quot; ))   &amp;&amp;   ISPICKVAL(OSM_Data_Entry_Status__c, &quot;Data Verification Complete&quot;)  &amp;&amp;  ( ISPICKVAL(OSM_Billing_Status__c, &quot;Billing Not Required&quot;) || ISPICKVAL(OSM_Billing_Status__c, &quot;Pre-Billing Complete&quot;) || ISPICKVAL(OSM_Billing_Status__c, &quot;Billing Initiated&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Obsolete</fullName>
        <actions>
            <name>Lab_and_Report_to_Obsolete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 33 OM - User Story User Story 32061 - Lab and Report Status to Obsolete</description>
        <formula>(ISPICKVAL(OSM_Work_Order__r.OSM_State__c, &quot;Closed&quot;) &amp;&amp;   !ISPICKVAL(OSM_Lab_and_Report_Status__c, &quot;Canceled&quot;) &amp;&amp;  OSM_Result_Current__r.OSM_Current__c = true &amp;&amp;  OSM_Result_Current__r.OSM_Distributed__c = false)   &amp;&amp;    ISPICKVAL(OSM_Result_Current__r.OSM_Result_Disposition__c , &quot;Not Run&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Order_Active</fullName>
        <actions>
            <name>OSM_OLI_Cust_Status_to_Processing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_OLI_Data_Entry_to_Awaiting</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Order_Line_State_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story 13324: State Status Update - Order Status set to Processing</description>
        <formula>AND(	 ISPICKVAL(Order.Status, &quot;Active&quot;),  ISPICKVAL(OSM_State__c, &quot;New&quot;),  ISPICKVAL(OSM_Order_Line_Item_Status__c, &quot;Submitting&quot;),  ISPICKVAL(OSM_Data_Entry_Status__c, &quot;Initial Data Entry&quot;),  NOT(ISPICKVAL(OSM_Data_Entry_Status__c, &quot;Awaiting Patient Report Data&quot;)),  NOT(ISPICKVAL(OSM_Data_Entry_Status__c, &quot;Data Verification&quot;)),  NOT(ISPICKVAL(OSM_Data_Entry_Status__c, &quot;Data Verification Complete&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Pre-Distribution</fullName>
        <actions>
            <name>Lab_and_Report_to_Pre_Distribution</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sprint 33 OM - User Story 30548 - Lab and Report Status to Pre-Distribution</description>
        <formula>(ISPICKVAL(OSM_Work_Order__r.OSM_State__c, &quot;Closed&quot;) &amp;&amp;   (TEXT(PRIORVALUE(OSM_Lab_and_Report_Status__c)) != &quot;Canceled&quot; &amp;&amp;  TEXT(PRIORVALUE(OSM_Lab_and_Report_Status__c)) != &quot;Pre-Distribution&quot;) &amp;&amp;  OSM_Result_Current__r.OSM_Current__c = true &amp;&amp;  OSM_Result_Current__r.OSM_Distributed__c = false)   &amp;&amp;    ( ISPICKVAL(OSM_Result_Current__r.OSM_Result_Disposition__c, &quot;Results&quot;) ||  ISPICKVAL(OSM_Result_Current__r.OSM_Result_Disposition__c , &quot;Failure&quot; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Specimen_Not_Ready</fullName>
        <actions>
            <name>OSM_Order_Line_Specimen_Not_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.OSM_Specimen_Retrieval_Ready__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_Specimen_Ready</fullName>
        <actions>
            <name>Order_Line_Specimen_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.OSM_Specimen_Retrieval_Ready__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Order_Line_State_New</fullName>
        <actions>
            <name>OSM_Order_Line_Item_State_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_State__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Data_Entry_Status__c</field>
            <operation>equals</operation>
            <value>Initial Data Entry</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.OSM_Lab_and_Report_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled,Obsolete,Distributed</value>
        </criteriaItems>
        <description>24160: OLI Status to State</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Populate_Verification_User_Date_Time</fullName>
        <actions>
            <name>OSM_Populate_Verification_User_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OrderItem.OSM_Data_Entry_Status__c</field>
            <operation>equals</operation>
            <value>Data Verification Complete</value>
        </criteriaItems>
        <description>User story 34185 - update OLI Verification Date and Time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Unset_Updated_By_Trigger</fullName>
        <actions>
            <name>OSM_Unset_Updated_By_Trigger</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Bill_Type_Updated_by_Quadax</fullName>
        <assignedTo>Customer_Service</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Bill Type Updated by Quadax</subject>
    </tasks>
</Workflow>
