<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assign_to_Intl_RWC_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Intl_RWC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Intl RWC Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_14_Day_Rule_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_14_Day_Rule</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>OSM_Assign to 14 Day Rule Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_Default_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Default</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>OSM_Assign to Default Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_Intl_Geneva_Fax_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Intl_Geneva_Fax</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Intl Geneva Fax Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_Intl_Geneva_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Intl_Geneva</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Intl Geneva Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_Intl_RWC_Fax_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Intl_RWC_Fax</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Intl RWC Fax Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_New_Orders_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_New_Orders</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to New Orders Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_New_Owner_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_New_Orders</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to New Owner Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_Order_Complete_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Order_Complete</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Order Complete queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OSM_Assign_to_Prostate_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>OSM_Prostate</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Prostate Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OSM_Assign to 14 Day Rule Queue</fullName>
        <actions>
            <name>OSM_Assign_to_14_Day_Rule_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OSM_Channel__c</field>
            <operation>equals</operation>
            <value>portal,fax</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Ordering_Country__c</field>
            <operation>equals</operation>
            <value>United States,US,USA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Created_by_User_Group__c</field>
            <operation>equals</operation>
            <value>Domestic</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Product__c</field>
            <operation>equals</operation>
            <value>Breast,Colon</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Billing_Type__c</field>
            <operation>equals</operation>
            <value>Medicare</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to Default Queue</fullName>
        <actions>
            <name>OSM_Assign_to_Default_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OSM_Channel__c</field>
            <operation>equals</operation>
            <value>Fax,Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Ordering_Country__c</field>
            <operation>equals</operation>
            <value>USA,United States,US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Created_by_User_Group__c</field>
            <operation>equals</operation>
            <value>Domestic</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Product__c</field>
            <operation>notEqual</operation>
            <value>Prostate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Billing_Type__c</field>
            <operation>notEqual</operation>
            <value>Medicare</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to Initial Order Entry Complete Queue</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OSM_Order_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to Intl Geneva Fax Queue</fullName>
        <actions>
            <name>OSM_Assign_to_Intl_Geneva_Fax_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OSM_Created_by_User_Group__c</field>
            <operation>equals</operation>
            <value>International - Geneva</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Channel__c</field>
            <operation>equals</operation>
            <value>Fax</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to Intl Geneva Queue</fullName>
        <actions>
            <name>OSM_Assign_to_Intl_Geneva_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 or 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Order_Set__c.OSM_Channel__c</field>
            <operation>equals</operation>
            <value>portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Ordering_Country__c</field>
            <operation>equals</operation>
            <value>Albania,Andorra,Austria,Belarus,Belgium,Bosnia and Herzegovina,Bulgaria,Croatia,Denmark,Estonia,Finland,France,Iceland,Italy,Kosovo,Latvia,Liechtenstein,Lithuania,Luxembourg,Macedonia,Malta,Moldova,Monaco,Montenegro,Netherlands</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Ordering_Country__c</field>
            <operation>equals</operation>
            <value>Norway,Republic of Ireland,Russia,San Marino,Serbia,Sweden,Switzerland,Ukraine,United Kingdom,Vatican City</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Created_by_User_Group__c</field>
            <operation>equals</operation>
            <value>International - Geneva</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to Intl RWC Fax Queue</fullName>
        <actions>
            <name>OSM_Assign_to_Intl_RWC_Fax_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OSM_Created_by_User_Group__c</field>
            <operation>equals</operation>
            <value>International - RWC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Channel__c</field>
            <operation>equals</operation>
            <value>Fax</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to Intl RWC Queue</fullName>
        <actions>
            <name>Assign_to_Intl_RWC_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OSM_Channel__c</field>
            <operation>equals</operation>
            <value>portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Ordering_Country__c</field>
            <operation>equals</operation>
            <value>Cyprus,Czech Republic,Germany,Greece,Hungary,Poland,Portugal,Romania,Slovakia,Slovenia,Spain,Turkey</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Created_by_User_Group__c</field>
            <operation>equals</operation>
            <value>International - RWC</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to Order Complete Queue</fullName>
        <actions>
            <name>OSM_Assign_to_Order_Complete_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OSM_Order_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Assign new records to queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to Prostate Queue</fullName>
        <actions>
            <name>OSM_Assign_to_Prostate_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OSM_Channel__c</field>
            <operation>equals</operation>
            <value>fax,portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Ordering_Country__c</field>
            <operation>equals</operation>
            <value>USA,United States,US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Created_by_User_Group__c</field>
            <operation>equals</operation>
            <value>Domestic</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Set__c.OSM_Product__c</field>
            <operation>equals</operation>
            <value>Prostate</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OSM_Assign to a Queue</fullName>
        <actions>
            <name>OSM_Assign_to_New_Orders_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OSM_Assign_to_New_Owner_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Set__c.OwnerId</field>
            <operation>notEqual</operation>
            <value>New Order</value>
        </criteriaItems>
        <description>Assign new records to queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
