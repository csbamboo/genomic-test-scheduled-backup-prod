<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OSM_Inactive_Date_Field_update</fullName>
        <description>Update inactive date in User profile when active = false</description>
        <field>OSM_InactiveDate__c</field>
        <formula>TODAY()</formula>
        <name>Inactive Date Field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Must_Agree_Terms_and_Conditions</fullName>
        <field>GHI_Portal_Must_Agree_to_Terms__c</field>
        <literalValue>1</literalValue>
        <name>Update Must Agree Terms and Conditions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GHI Portal User must accept terms and conditions</fullName>
        <active>true</active>
        <description>Delegates must accept the terms and conditions upon first login and every after 90 days.</description>
        <formula>AND( AND (Contact.GHI_Portal_Delegate__c = TRUE,           Contact.GHI_Portal_Sponsor__c = FALSE),      NOT(ISBLANK(GHI_Portal_Authorization_Date__c)),      GHI_Portal_Must_Agree_to_Terms__c = FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Must_Agree_Terms_and_Conditions</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>User.GHI_Portal_Authorization_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OSM_User_InactiveDate</fullName>
        <actions>
            <name>OSM_Inactive_Date_Field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate inactive date once Active status is set to false</description>
        <formula>ISCHANGED(IsActive) &amp;&amp; IsActive = False</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
